package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ValueStep extends DefaultPathStep implements PathStep
{
    public ValueStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DocumentReferenceList)
        {
            return ((DocumentReferenceList)input).getReferenceIds();
        }
        else if (input instanceof AttachmentList)
        {
            return ((AttachmentList)input).getAttachmentIds();
        }
        else if (input instanceof DataRecord)
        {
            return evaluateValueStep(((DataRecord)input).getAllPropertyValues());
        }
        else if (input instanceof RecordProperty)
        {
            RecordProperty inputProperty;
            RecordValue inputPropertyValue;

            inputProperty = (RecordProperty)input;
            inputPropertyValue = inputProperty.getRecordValue();
            return inputPropertyValue != null ? evaluateValueStep(ArrayLists.newArrayList(inputPropertyValue)) : new ArrayList<>();
        }
        else if (input instanceof RecordValue)
        {
            RecordValue inputValue;

            inputValue = (RecordValue)input;
            if (inputValue.getSubValueCount() == 0) return evaluateValueStep(ArrayLists.typeSafeList(inputValue));
            else return evaluateValueStep(inputValue.getSubValues());
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateValueStep(List<RecordValue> recordValueList)
    {
        List<String> valueList;

        // If we have valid ID's to use for narrowing of the value list do it now.
        if (idList != null)
        {
            Iterator<RecordValue> valueIterator;

            valueIterator = recordValueList.iterator();
            while (valueIterator.hasNext())
            {
                RecordValue nextValue;

                nextValue = valueIterator.next();
                if (!idList.contains(nextValue.getValueRequirement().getRequirementType().toString()))
                {
                    valueIterator.remove();
                }
            }
        }

        // Now run through the remaining value list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<RecordValue> recordIterator;

            // Iterator over the records and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            recordIterator = recordValueList.iterator();
            while (recordIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                RecordValue nextValue;

                // Create a map containing the input parameters available to the predicate expression.
                nextValue = recordIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("VALUE", nextValue.getValue());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        recordIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        valueList = new ArrayList<String>();
        for (RecordValue recordValue : recordValueList)
        {
            if (recordValue instanceof DocumentReferenceList)
            {
                return ((DocumentReferenceList)recordValue).getReferenceIds();
            }
            else if (recordValue instanceof AttachmentList)
            {
                return ((AttachmentList)recordValue).getAttachmentIds();
            }
            else valueList.add(recordValue.getValue());
        }

        return valueList;
    }
}
