package com.pilog.t8.data.ontology.commission;

import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyChangeRequest implements Serializable
{
    private List<T8OntologyCode> codeChanges; // Contains a list of all Code changes.  The list may include new creations or changes to existing codes.
    private List<String> codeDeletions; // Contains a list of all Code ID's to be deleted.
    private Map<String, String> codeSupersessions; // Contains a map of code supersessions (Key: Old Code ID, Value: New Code ID).
    
    private List<T8OntologyDefinition> definitionChanges; // Contains a list of all definition changes.  The list may include new creations or changes to existing definitions.
    private List<String> definitionDeletions; // Contains a list of all Definition ID's to be deleted.
    private Map<String, String> definitionSupersessions; // Contains a map of definition supersessions (Key:  Old Definition ID, Value:  New Definition ID).
    
    private List<T8OntologyAbbreviation> abbreviationChanges; // Contains a list of all abbreviaton changes.  The list may include new creations or changes to existing abbreviations.
    private List<String> abbreviationDeletions;  // Contains a list of all abbreviation ID's to be deleted.
    private Map<String, String> abbreviationSupersessions; // Contains a map of abbreviation supersessions (Key:  Old Abbreviation ID, Value:  New Abbreviation ID).
    
    private List<T8OntologyTerm> termChanges; // Contains a list of all term changes.  The list may include new creations or changes to existing terms.
    private List<String> termDeletions; // Contains a list of all term ID's to be deleted.
    private Map<String, String> termSupersessions; // Contains a map of term supersessions (Key:  Old Term ID, Value:  New Term ID).
    
    private List<T8OntologyConcept> conceptChanges; // Contains a list of all concept changes.  The list may include new creations of changes to existing concepts.
    private List<String> conceptDeletions; // Contains a list of all concept ID's to be deleted.
    private Map<String, String> conceptSupersessions; // Contains a map of concept supersessions (Key:  Old Concept ID, Value:  New Concept ID).
    
    public T8OntologyChangeRequest()
    {
    }

    public List<T8OntologyCode> getCodeChanges()
    {
        return codeChanges;
    }

    public void setCodeChanges(List<T8OntologyCode> codeChanges)
    {
        this.codeChanges = codeChanges;
    }

    public List<String> getCodeDeletions()
    {
        return codeDeletions;
    }

    public void setCodeDeletions(List<String> codeDeletions)
    {
        this.codeDeletions = codeDeletions;
    }

    public Map<String, String> getCodeSupersessions()
    {
        return codeSupersessions;
    }

    public void setCodeSupersessions(Map<String, String> codeSupersessions)
    {
        this.codeSupersessions = codeSupersessions;
    }

    public List<T8OntologyDefinition> getDefinitionChanges()
    {
        return definitionChanges;
    }

    public void setDefinitionChanges(List<T8OntologyDefinition> definitionChanges)
    {
        this.definitionChanges = definitionChanges;
    }

    public List<String> getDefinitionDeletions()
    {
        return definitionDeletions;
    }

    public void setDefinitionDeletions(List<String> definitionDeletions)
    {
        this.definitionDeletions = definitionDeletions;
    }

    public Map<String, String> getDefinitionSupersessions()
    {
        return definitionSupersessions;
    }

    public void setDefinitionSupersessions(Map<String, String> definitionSupersessions)
    {
        this.definitionSupersessions = definitionSupersessions;
    }

    public List<T8OntologyAbbreviation> getAbbreviationChanges()
    {
        return abbreviationChanges;
    }

    public void setAbbreviationChanges(List<T8OntologyAbbreviation> abbreviationChanges)
    {
        this.abbreviationChanges = abbreviationChanges;
    }

    public List<String> getAbbreviationDeletions()
    {
        return abbreviationDeletions;
    }

    public void setAbbreviationDeletions(List<String> abbreviationDeletions)
    {
        this.abbreviationDeletions = abbreviationDeletions;
    }

    public Map<String, String> getAbbreviationSupersessions()
    {
        return abbreviationSupersessions;
    }

    public void setAbbreviationSupersessions(Map<String, String> abbreviationSupersessions)
    {
        this.abbreviationSupersessions = abbreviationSupersessions;
    }

    public List<T8OntologyTerm> getTermChanges()
    {
        return termChanges;
    }

    public void setTermChanges(List<T8OntologyTerm> termChanges)
    {
        this.termChanges = termChanges;
    }

    public List<String> getTermDeletions()
    {
        return termDeletions;
    }

    public void setTermDeletions(List<String> termDeletions)
    {
        this.termDeletions = termDeletions;
    }

    public Map<String, String> getTermSupersessions()
    {
        return termSupersessions;
    }

    public void setTermSupersessions(Map<String, String> termSupersessions)
    {
        this.termSupersessions = termSupersessions;
    }

    public List<T8OntologyConcept> getConceptChanges()
    {
        return conceptChanges;
    }

    public void setConceptChanges(List<T8OntologyConcept> conceptChanges)
    {
        this.conceptChanges = conceptChanges;
    }

    public List<String> getConceptDeletions()
    {
        return conceptDeletions;
    }

    public void setConceptDeletions(List<String> conceptDeletions)
    {
        this.conceptDeletions = conceptDeletions;
    }

    public Map<String, String> getConceptSupersessions()
    {
        return conceptSupersessions;
    }

    public void setConceptSupersessions(Map<String, String> conceptSupersessions)
    {
        this.conceptSupersessions = conceptSupersessions;
    }
}
