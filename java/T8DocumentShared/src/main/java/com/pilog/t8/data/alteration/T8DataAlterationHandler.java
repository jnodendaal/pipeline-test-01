package com.pilog.t8.data.alteration;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.T8DataTransaction;

/**
 * This interface defines classes that know how to handle specific types of alterations.
 * - Classes implementing this interface form the entry point from which clients gain access to maintain and/or apply steps in an alteration instance.
 * - An alteration handler is constructed from a factory method on client request.
 * - Classes implementing this interface will each handle inserts/updates/deletions to and from the applicable ALT_* entities.
 *
 * @author Bouwer du Preez
 */
public interface T8DataAlterationHandler
{
    /**
     * Returns the id of the alteration type to which this handler is applicable.
     * @return The meta id of the alteration type to which this handler is applicable.
     */
    public String getAlterationId();

    /**
     * De-serializes a {@code T8DataAlteration} object from the supplied JSON.
     * In order for this method to be successful, the supplied JSON must be of the correct type matching this handler.
     * @param jsonValue The JSON from which to deserialize the data alteration.
     * @return The {@code T8DataAlteration} deserialized from the supplied JSON.
     * @throws java.lang.Exception
     */
    public T8DataAlteration deserializeAlteration(JsonObject jsonValue) throws Exception;

    /**
     * Adjusts the steps in the specified alteration by the specified amount.
     * @param tx The transaction to use for this operation.
     * @param packageIid The unique instance id of the alteration package that will be updated.
     * @param startStep The first step to be adjusted.
     * @param endStep The last step to be adjusted.
     * @param increment The amount by which steps in the specified alteration
     * will be adjusted.
     * @throws Exception
     */
    public void adjustSteps(T8DataTransaction tx, String packageIid, int startStep, int endStep, int increment) throws Exception;

    /**
     * Deletes the specified data alteration.
     * @param tx The transaction to use for this operation.
     * @param packageIid The unique instance id of the alteration package to delete.
     * @param step The alteration step to delete.  If the value is negative, all
     * steps in the specified package will be deleted.
     * @return Returns the number of alteration steps successfully deleted.
     * @throws Exception
     */
    public int deleteAlteration(T8DataTransaction tx, String packageIid, int step) throws Exception;

    /**
     * Inserts the supplied data alteration in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param tx The transaction to use for this operation.
     * @param alteration The data alteration to persist.
     * @throws Exception
     */
    public void insertAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception;

    /**
     * Updates the supplied data alteration in the applicable tables.
     * @param tx The transaction to use for this operation.
     * @param alteration The alteration to persist.
     * @throws Exception
     */
    public void updateAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception;

    /**
     * This method returns an impact report indicating the data changes that would occur if the
     * specified alteration were applied.
     * @param tx The transaction to use for this operation.
     * @param alteration The data alteration for which to calculate an impact report.
     * @return The impact report, summarizing potential data changes resulting for the specified alteration.
     * @throws Exception
     */
    public T8DataAlterationImpact getAlterationImpact(T8DataTransaction tx, T8DataAlteration alteration) throws Exception;

    /**
     * Applies the specified data alteration deleting, inserting and updating
     * operational data as required.  This method returns an impact report
     * indicating the data changes that were applied.
     * @param tx The transaction to use for this operation.
     * @param alteration The data alteration to apply.
     * @return The impact report, summarizing applied data changes.
     * @throws Exception
     */
    public T8DataAlterationImpact applyAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception;

    /**
     * Retrieves the specified alteration from the persistence store.  The
     * alteration step specified will be used as a minimum value i.e. the next
     * alteration to return will have a step value equal to or greater than the
     * step value specified in order to accommodate skips in steps that could
     * potentially occur.
     * @param tx The transaction to use for this operation.
     * @param packageIid The instance id of the alteration package to retrieve.
     * @param step The minimum step to retrieve.  The alteration returned will
     * have a step value equal to or greater than this value.  If this value is
     * less than or equal to zero, the first step of the alteration will be
     * retrieved.
     * @return The specified alteration retrieved from the persistence store or
     * null if no such alteration exists.
     * @throws Exception
     */
    public T8DataAlteration retrieveAlteration(T8DataTransaction tx, String packageIid, int step) throws Exception;
}
