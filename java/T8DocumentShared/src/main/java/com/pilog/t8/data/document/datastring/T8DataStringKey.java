package com.pilog.t8.data.document.datastring;

import com.pilog.t8.data.document.datastring.T8DataStringFormat.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringKey
{
    private String dataKeyId;
    private String drId;
    private String drInstanceId;
    private String rootRecordId;
    private String recordId;
    private String propertyId;
    private String fieldId;
    private String dataTypeId;
    private Type formatType;
    private String languageId;
    private String dsTypeId;
    private String orgId;

    public T8DataStringKey(Type renderingFormatType)
    {
        this.formatType = renderingFormatType;
    }

    public Type getRenderingFormatType()
    {
        return formatType;
    }

    public void setRenderingFormatType(Type renderingFormatType)
    {
        this.formatType = renderingFormatType;
    }

    public String getDataKeyId()
    {
        return dataKeyId;
    }

    public void setDataKeyId(String keyId)
    {
        this.dataKeyId = keyId;
    }

    public String getDrId()
    {
        return drId;
    }

    public void setDrId(String drId)
    {
        this.drId = drId;
    }

    public String getDrInstanceId()
    {
        return drInstanceId;
    }

    public void setDrInstanceId(String drInstanceId)
    {
        this.drInstanceId = drInstanceId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getRootRecordId()
    {
        return rootRecordId;
    }

    public void setRootRecordId(String rootRecordId)
    {
        this.rootRecordId = rootRecordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public void setFieldId(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getDataTypeId()
    {
        return dataTypeId;
    }

    public void setDataTypeId(String dataTypeId)
    {
        this.dataTypeId = dataTypeId;
    }

    public String getLanguageID()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public String getDsTypeId()
    {
        return dsTypeId;
    }

    public void setDsTypeId(String dsTypeId)
    {
        this.dsTypeId = dsTypeId;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public boolean isKeySpecific()
    {
        return dataKeyId != null;
    }

    public boolean isDrSpecific()
    {
        return drId != null;
    }

    public boolean isDrInstanceSpecific()
    {
        return drInstanceId != null;
    }

    public boolean isRootRecordSpecific()
    {
        return rootRecordId != null;
    }

    public boolean isRecordSpecific()
    {
        return recordId != null;
    }

    public boolean isPropertySpecific()
    {
        return propertyId != null;
    }

    public boolean isFieldSpecific()
    {
        return fieldId != null;
    }

    public boolean isLanguageSpecific()
    {
        return languageId != null;
    }

    public boolean isDsTypeSpecific()
    {
        return dsTypeId != null;
    }

    public boolean isOrganizationSpecific()
    {
        return orgId != null;
    }

    public T8DataStringKey copy()
    {
        T8DataStringKey copy;

        copy = new T8DataStringKey(formatType);
        copy.setDataTypeId(dataTypeId);
        copy.setDrId(drId);
        copy.setDrInstanceId(drInstanceId);
        copy.setDataKeyId(dataKeyId);
        copy.setRootRecordId(rootRecordId);
        copy.setFieldId(fieldId);
        copy.setPropertyId(propertyId);
        copy.setRecordId(recordId);
        copy.setLanguageId(languageId);
        copy.setOrgId(orgId);
        copy.setDsTypeId(dsTypeId);

        return copy;
    }

    public List<String> getPrecedencyKeyStrings()
    {
        String formatTypeString;
        List<String> keys;

        // Create a new list to hold precedence keys.
        keys = new ArrayList<>();

        // Add the generic keys that do not specify a selection filter (ROOT_RECORD_ID/DATA_KEY_ID).
        formatTypeString = formatType.toString() + "|*";
        addDataLevelKeyStrings(keys, formatTypeString);

        // If a data key is specified, add it as the next level of more specific keys.
        if (dataKeyId != null)
        {
            formatTypeString = formatType.toString() + "|" + dataKeyId;
            addDataLevelKeyStrings(keys, formatTypeString);
        }

        // Lastly, add the root record id as the most specific set of keys.
        formatTypeString = formatType.toString() + "|" + rootRecordId;
        addDataLevelKeyStrings(keys, formatTypeString);

        // Reverse the order of the keys, so those with the highest precedency are first.
        Collections.reverse(keys);
        return keys;
    }

    private void addDataLevelKeyStrings(List<String> keys, String formatTypeString)
    {
        if (formatType == Type.DATA_TYPE)
        {
            addRenderingKeyStrings(keys, formatTypeString + "|*|*|*|*");
            addRenderingKeyStrings(keys, formatTypeString + "|*|*|*|" + dataTypeId);
            if (drId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drId + "*|*|*|" + dataTypeId);
            if (drInstanceId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drInstanceId + "*|*|*|" + dataTypeId);
            if (recordId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + recordId + "*|*|*|" + dataTypeId);

            if (fieldId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|*|" + propertyId + "|" + fieldId + "|" + dataTypeId);
                if (drId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drId + "|" + propertyId + "|" + fieldId + "|" + dataTypeId);
                if (drInstanceId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drInstanceId + "|" + propertyId + "|" + fieldId + "|" + dataTypeId);
                if (recordId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + recordId + "|" + propertyId + "|" + fieldId + "|" + dataTypeId);
            }
            else if (propertyId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|*|" + propertyId + "|*|" + dataTypeId);
                if (drId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drId + "|" + propertyId + "|*|" + dataTypeId);
                if (drInstanceId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drInstanceId + "|" + propertyId + "|*|" + dataTypeId);
                if (recordId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + recordId + "|" + propertyId + "|*|" + dataTypeId);
            }
        }
        else if (formatType == Type.FIELD)
        {
            addRenderingKeyStrings(keys, formatTypeString + "|*|" + propertyId + "|" + fieldId + "|*");
            if (drId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drId + "|" + propertyId + "|" + fieldId + "|*");
            if (drInstanceId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + drInstanceId + "|" + propertyId + "|" + fieldId + "|*");
            if (recordId != null) addRenderingKeyStrings(keys, formatTypeString + "|" + recordId + "|" + propertyId + "|" + fieldId + "|*");
        }
        else if (formatType == Type.PROPERTY)
        {
            addRenderingKeyStrings(keys, formatTypeString + "|*|*|*|*");
            addRenderingKeyStrings(keys, formatTypeString + "|*|" + propertyId + "|*|*");

            if (drId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|" + drId + "|*|*|*");
                addRenderingKeyStrings(keys, formatTypeString + "|" + drId + "|" + propertyId + "|*|*");
            }

            if (drInstanceId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|" + drInstanceId + "|*|*|*");
                addRenderingKeyStrings(keys, formatTypeString + "|" + drInstanceId + "|" + propertyId + "|*|*");
            }

            if (recordId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|" + recordId + "|*|*|*");
                addRenderingKeyStrings(keys, formatTypeString + "|" + recordId + "|" + propertyId + "|*|*");
            }
        }
        else // formatType == Type.RECORD.
        {
            // String matching all records.
            addRenderingKeyStrings(keys, formatTypeString + "|*|*|*|*");

            // String matching all records of a specific DR.
            if (drId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|" + drId + "|*|*|*");
            }

            // String matching all records of a specific DR Instance.
            if (drInstanceId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|" + drInstanceId + "|*|*|*");
            }

            // String matching a specific record.
            if (recordId != null)
            {
                addRenderingKeyStrings(keys, formatTypeString + "|" + recordId + "|*|*|*");
            }
        }
    }

    /**
     * Takes the data level precedence key string and adds rendering precedency keys to it in the
     * following order form least applicable, to most applicable:
     * L=Language
     * T=Data String Type
     * O=Organization
     *
     *      *|*|*
     *      L|*|*
     *      *|T|*
     *      *|*|O
     *      L|*|O
     *      *|T|O
     *      L|T|O
     *
     * @param keys
     * @param dataLevelKeyString
     */
    private void addRenderingKeyStrings(List<String> keys, String dataLevelKeyString)
    {
        keys.add(dataLevelKeyString + "|*|*|*");
        keys.add(dataLevelKeyString + "|" + languageId + "|*|*");
        keys.add(dataLevelKeyString + "|*|" + dsTypeId + "|*");
        keys.add(dataLevelKeyString + "|*|*|" + orgId);
        keys.add(dataLevelKeyString + "|" + languageId + "|*|" + orgId);
        keys.add(dataLevelKeyString + "|*|" + dsTypeId + "|" + orgId);
        keys.add(dataLevelKeyString + "|" + languageId + "|" + dsTypeId + "|" + orgId);
    }

    public String getFormatLevelId()
    {
        if (rootRecordId != null) return rootRecordId;
        else if (dataKeyId != null) return dataKeyId;
        else return null;
    }

    public String getDataLevelId()
    {
        if (recordId != null) return recordId;
        else if (drInstanceId != null) return drInstanceId;
        else if (drId != null) return drId;
        else return null;
    }

    public String getRenderingLevelID()
    {
        if (orgId != null) return orgId;
        else if (dsTypeId != null) return dsTypeId;
        else if (languageId != null) return languageId;
        else return null;
    }

    public String getKeyString()
    {
        // Create and return the key string.
        return createKeyString(getFormatLevelId(), getDataLevelId());
    }

    private String createKeyString(String formatLevelId, String dataLevelId)
    {
        StringBuilder keyString;

        // Create a string builder to use for concatenation.
        keyString = new StringBuilder(formatType.toString());
        keyString.append("|");

        // Append the root record filter.
        keyString.append(formatLevelId != null ? formatLevelId : "*");
        keyString.append("|");

        // First append the data link level.
        keyString.append(dataLevelId != null ? dataLevelId : "*");
        keyString.append("|");

        // Append the property selection.
        keyString.append(propertyId != null ? propertyId : "*");
        keyString.append("|");

        // Append the field selection.
        keyString.append(fieldId != null ? fieldId : "*");
        keyString.append("|");

        // Append the data type selection.
        keyString.append(dataTypeId != null ? dataTypeId : "*");
        keyString.append("|");

        // Append the language selection.
        keyString.append(languageId != null ? languageId : "*");
        keyString.append("|");

        // Append the data string type selection.
        keyString.append(dsTypeId != null ? dsTypeId : "*");
        keyString.append("|");

        // Append the organization selection.
        keyString.append(orgId != null ? orgId : "*");

        // Return the compiled key string.
        return keyString.toString();
    }
}
