package com.pilog.t8.data.document;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.filter.T8SubDataFilter;
import com.pilog.t8.definition.data.document.T8DocumentResources;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordFilterConstructor
{
    private String documentEntityIdentifier;
    private String docEntityIdentifier;
    private String classEntityIdentifier;
    private String propertyEntityIdentifier;
    private String valueEntityIdentifier;

    public T8DataRecordFilterConstructor()
    {
        documentEntityIdentifier = "@E_DATA_RECORD_DOCUMENT";
        docEntityIdentifier = "@E_DATA_RECORD_DOC";
        classEntityIdentifier = "@E_DATA_RECORD_CLASS";
        propertyEntityIdentifier = "@E_DATA_RECORD_PROPERTY";
        valueEntityIdentifier = "@E_DATA_RECORD_VALUE";
    }

    public String getDocumentEntityIdentifier()
    {
        return documentEntityIdentifier;
    }

    public void setDocumentEntityIdentifier(String documentEntityIdentifier)
    {
        this.documentEntityIdentifier = documentEntityIdentifier;
    }

    public String getDocEntityIdentifier()
    {
        return docEntityIdentifier;
    }

    public void setDocEntityIdentifier(String docEntityIdentifier)
    {
        this.docEntityIdentifier = docEntityIdentifier;
    }

    public String getClassEntityIdentifier()
    {
        return classEntityIdentifier;
    }

    public void setClassEntityIdentifier(String classEntityIdentifier)
    {
        this.classEntityIdentifier = classEntityIdentifier;
    }

    public String getPropertyEntityIdentifier()
    {
        return propertyEntityIdentifier;
    }

    public void setPropertyEntityIdentifier(String propertyEntityIdentifier)
    {
        this.propertyEntityIdentifier = propertyEntityIdentifier;
    }

    public String getValueEntityIdentifier()
    {
        return valueEntityIdentifier;
    }

    public void setValueEntityIdentifier(String valueEntityIdentifier)
    {
        this.valueEntityIdentifier = valueEntityIdentifier;
    }

    public void addPropertySubDataFilter(DataFilterConjunction conjunction, T8DataFilter dataFilter, String recordIDFieldIdentifier, String drIdentifier, List<PropertyValueStringCriterion> propertyCriteria)
    {
        dataFilter.addFilterCriterion(conjunction, recordIDFieldIdentifier, DataFilterOperator.IN, createPropertySubDataFilter(recordIDFieldIdentifier, drIdentifier, propertyCriteria), false);
    }

    private T8SubDataFilter createPropertySubDataFilter(String parentRecordIDFieldIdentifier, String drIdentifier, List<PropertyValueStringCriterion> propertyCriteria)
    {
        T8SubDataFilter subDataFilter;
        T8DataFilter dataFilter;
        Map<String, String> filterFieldMapping;
        PropertyValueStringCriterion nextCriteria;

        // Get the next property identifier and value string to use.
        nextCriteria = propertyCriteria.get(0);

        // Create the filter on the DR and Property Value String.
        dataFilter = new T8DataFilter(propertyEntityIdentifier);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, propertyEntityIdentifier + T8DocumentResources.EF_DR_ID, DataFilterOperator.EQUAL, drIdentifier, false);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, propertyEntityIdentifier + T8DocumentResources.EF_PROPERTY_ID, DataFilterOperator.EQUAL, nextCriteria.getPropertyIdentifier(), false);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, propertyEntityIdentifier + T8DocumentResources.EF_VALUE_STRING, DataFilterOperator.EQUAL, nextCriteria.getPropertyValueString(), false);

        // If there are more property criteria, add another sub-filter.
        if (propertyCriteria.size() > 1)
        {
            String filterFieldIdentifier;
            List<PropertyValueStringCriterion> remainingCriteria;

            remainingCriteria = propertyCriteria.subList(1, propertyCriteria.size());
            filterFieldIdentifier = propertyEntityIdentifier + T8DocumentResources.EF_RECORD_ID;
            dataFilter.addFilterCriterion(DataFilterConjunction.AND, filterFieldIdentifier, DataFilterOperator.IN, createPropertySubDataFilter(filterFieldIdentifier, drIdentifier, remainingCriteria), false);
        }

        // Create the sub-filter.
        filterFieldMapping = new HashMap<String, String>();
        filterFieldMapping.put(propertyEntityIdentifier + T8DocumentResources.EF_RECORD_ID, parentRecordIDFieldIdentifier);
        subDataFilter = new T8SubDataFilter(dataFilter);
        subDataFilter.setSubFilterFieldMapping(filterFieldMapping);
        return subDataFilter;
    }
}
