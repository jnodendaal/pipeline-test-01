package com.pilog.t8.data.document.dataquality;

/**
 * @author Bouwer du Preez
 */
public class T8FileDataQualityReport extends T8DataQualityMetrics
{
    private final String fileId;

    public T8FileDataQualityReport(String fileId)
    {
        this.fileId = fileId;
    }

    public String getFileId()
    {
        return fileId;
    }
}
