package com.pilog.t8.data.document;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasure;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasure;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer.duPreez
 */
public class DocumentHandler
{
    public static final HashSet<DataRecord> findNewRecords(DataRecord fromRootRecord, DataRecord toRootRecord)
    {
        HashSet<DataRecord> newRecords;

        newRecords = new HashSet<DataRecord>();
        for (DataRecord newRecord : toRootRecord.getDataRecords())
        {
            DataRecord existingRecord;

            existingRecord = fromRootRecord.findDataRecord(newRecord.getID());
            if (existingRecord == null)
            {
                newRecords.add(newRecord);
            }
        }

        return newRecords;
    }

    public static final HashSet<DataRecord> findUpdatedRecords(DataRecord fromRootRecord, DataRecord toRootRecord)
    {
        HashSet<DataRecord> updatedRecords;

        updatedRecords = new HashSet<DataRecord>();
        for (DataRecord record1 : fromRootRecord.getDataRecords())
        {
            DataRecord record2;

            record2 = toRootRecord.findDataRecord(record1.getID());
            if (record2 != null)
            {
                int hash1;
                int hash2;

                hash1 = record1.getContentHashCode(false, true);
                hash2 = record2.getContentHashCode(false, true);
                if (hash1 != hash2)
                {
                    updatedRecords.add(record2);
                }
            }
        }

        return updatedRecords;
    }

    public static final HashSet<DataRecord> findDeletedRecords(DataRecord fromRootRecord, DataRecord toRootRecord)
    {
        HashSet<DataRecord> deletedRecords;

        deletedRecords = new HashSet<DataRecord>();
        for (DataRecord deletedRecord : fromRootRecord.getDataRecords())
        {
            DataRecord existingRecord;

            existingRecord = toRootRecord.findDataRecord(deletedRecord.getID());
            if (existingRecord == null)
            {
                deletedRecords.add(deletedRecord);
            }
        }

        return deletedRecords;
    }

    /**
     * Returns all of the Concept ID's that are available in the Data Record
     * Instance supplied as well as the Data Requirement that it is based on.
     *
     * @param requirementInstance The Data Requirement Instance from which
     * concept ID's will be fetched.
     * @return The set of all Concept ID's available in the Data Requirement.
     */
    public static final HashSet<String> getAllConceptIds(DataRequirementInstance requirementInstance)
    {
        T8OntologyConcept ontology;
        HashSet<String> conceptIDs;

        // Add the concepts used in the ontology of the Data Requirement Instance.
        conceptIDs = new HashSet<String>();
        ontology = requirementInstance.getOntology();
        if (ontology != null) conceptIDs.addAll(ontology.getConceptIDSet());

        // Get all the DR Concept ID's, add the DR Instance ID to the set and return the set.
        conceptIDs.addAll(DocumentHandler.getAllConceptIds(requirementInstance.getDataRequirement()));
        conceptIDs.add(requirementInstance.getConceptID());

        // Add all comment type ID's.
        for (T8DataRequirementComment comment : requirementInstance.getComments())
        {
            conceptIDs.add(comment.getTypeId());
        }

        // Return the complete set of concept ID's used by this DR Instance.
        return conceptIDs;
    }

    /**
     * Returns all of the Concept ID's that are available in the Data Record
     * supplied as well as the Data Requirement that it is based on.
     *
     * @param requirement The Data Requirement from which concept ID's will be
     * fetched.
     * @return The set of all Concept ID's available in the Data Requirement.
     */
    public static final HashSet<String> getAllConceptIds(DataRequirement requirement)
    {
        HashSet<String> conceptIDs;
        T8OntologyConcept ontology;

        // Create a new Set to hold the ID's.
        conceptIDs = new HashSet<String>();

        // Add the concepts used in the ontology of the Data Requirement Instance.
        ontology = requirement.getOntology();
        if (ontology != null) conceptIDs.addAll(ontology.getConceptIDSet());

        // Add the DR concept itself.
        conceptIDs.add(requirement.getConceptID());

        // Add all of the concepts contained by the classes in the DR.
        for (SectionRequirement sectionRequirement : requirement.getSectionRequirements())
        {
            conceptIDs.add(sectionRequirement.getConceptID());
            for (PropertyRequirement propertyRequirement : sectionRequirement.getPropertyRequirements())
            {
                ArrayList<ValueRequirement> valueRequirements;

                conceptIDs.add(propertyRequirement.getConceptID());
                valueRequirements = propertyRequirement.getAllValueRequirements();
                for (ValueRequirement valueRequirement : valueRequirements)
                {
                    if (valueRequirement.getValue() != null)
                    {
                        if (valueRequirement.getRequirementType().isConceptRequirementValue())
                        {
                            conceptIDs.add(valueRequirement.getValue());
                        }
                    }
                }
            }
        }

        return conceptIDs;
    }

    /**
     * Returns all of the Concept ID's that are available in the Data Record
     * supplied as well as the Data Requirement that it is based on.
     *
     * @param dataRecord The Data Record from which concept ID's will be
     * fetched.
     * @return The set of all Concept ID's available in the Data Record and it's
     * Data Requirement.
     */
    public static final HashSet<String> getAllConceptIds(DataRecord dataRecord)
    {
        T8OntologyConcept ontology;
        HashSet<String> conceptIds;
        List<T8DataString> dataStrings;

        // Create a new Set to hold the ID's.
        conceptIds = new HashSet<String>();

        // Add the Record concept itself.
        conceptIds.add(dataRecord.getID());

        // Add the concepts used in the ontology for the record.
        ontology = dataRecord.getOntology();
        if (ontology != null) conceptIds.addAll(ontology.getConceptIDSet());

        // Add all the ID's used by the record's Data Requirement Instance.
        conceptIds.addAll(DocumentHandler.getAllConceptIds(dataRecord.getDataRequirementInstance()));

        // Add all data string concepts to the concept ID set.
        dataStrings = dataRecord.getDataStrings();
        if (dataStrings != null)
        {
            for (T8DataString dataString : dataStrings)
            {
                conceptIds.add(dataString.getInstanceID());
                conceptIds.add(dataString.getTypeID());
            }
        }

        // Add all of the ID's that are used only in the record and could not be extracted from the DR Instance.
        // This part of the method must be updated to use a more proper polymorhpic approach where each value types supplies
        // the concept ids it contains.
        for (RecordValue recordValue : getAllRecordValues(dataRecord))
        {
            RequirementType requirementType;

            requirementType = recordValue.getValueRequirement().getRequirementType();
            if (recordValue instanceof DocumentReferenceList)
            {
                conceptIds.addAll(((DocumentReferenceList)recordValue).getReferenceIds());
            }
            else if (requirementType.isConceptRecordValue())
            {
                String conceptId;

                conceptId = recordValue.getValue();
                if (!Strings.isNullOrEmpty(conceptId))
                {
                    conceptIds.add(conceptId);
                }
            }
        }

        return conceptIds;
    }

    public static final Set<String> getValueConceptIds(DataRecord dataRecord)
    {
        HashSet<String> conceptIds;

        conceptIds = new HashSet<String>();
        for (RecordValue value : dataRecord.getAllValues())
        {
            String conceptId;

            if (value instanceof ControlledConcept) conceptId = ((ControlledConcept)value).getConceptId();
            else if (value instanceof UnitOfMeasure) conceptId = ((UnitOfMeasure)value).getConceptId();
            else if (value instanceof QualifierOfMeasure) conceptId = ((QualifierOfMeasure)value).getConceptId();
            else continue;

            if (!Strings.isNullOrEmpty(conceptId))
            {
                conceptIds.add(conceptId);
            }
        }

        return conceptIds;
    }

    public static final HashSet<String> getAllClassRequirementIDs(DataRequirement requirement)
    {
        HashSet<String> conceptIDs;

        conceptIDs = new HashSet<String>();
        for (SectionRequirement classRequirement : requirement.getSectionRequirements())
        {
            conceptIDs.add(classRequirement.getConceptID());
        }

        return conceptIDs;
    }

    public static final HashSet<String> getAllPropertyRequirementIDs(DataRequirement requirement)
    {
        HashSet<String> conceptIDs;

        conceptIDs = new HashSet<String>();
        for (SectionRequirement classRequirement : requirement.getSectionRequirements())
        {
            for (PropertyRequirement propertyRequirement : classRequirement.getPropertyRequirements())
            {
                conceptIDs.add(propertyRequirement.getConceptID());
            }
        }

        return conceptIDs;
    }

    public static final HashSet<String> getAllValueRequirementIDs(DataRequirement requirement)
    {
        HashSet<String> conceptIDs;

        conceptIDs = new HashSet<String>();
        for (SectionRequirement classRequirement : requirement.getSectionRequirements())
        {
            for (PropertyRequirement propertyRequirement : classRequirement.getPropertyRequirements())
            {
                ArrayList<ValueRequirement> valueRequirements;

                valueRequirements = propertyRequirement.getAllValueRequirements();
                for (ValueRequirement valueRequirement : valueRequirements)
                {
                    if (valueRequirement.getValue() != null)
                    {
                        conceptIDs.add(valueRequirement.getValue());
                    }
                }
            }
        }

        return conceptIDs;
    }

    public static final PropertyRequirement findPropertyRequirement(DataRequirement requirement, String classID, String propertyID)
    {
        SectionRequirement classRequirement;

        classRequirement = requirement.getSectionRequirement(classID);
        if (classRequirement != null)
        {
            return classRequirement.getPropertyRequirement(propertyID);
        }
        else return null;
    }

    public static ArrayList<RecordValue> getAllRecordValues(DataRecord dataRecord)
    {
        ArrayList<RecordValue> recordValues;
        List<RecordProperty> recordProperties;

        recordValues = new ArrayList<RecordValue>();
        recordProperties = dataRecord.getRecordProperties();
        for (RecordProperty recordProperty : recordProperties)
        {
            recordValues.addAll(recordProperty.getAllRecordValues());
        }

        return recordValues;
    }

    public static ArrayList<PropertyRequirement> getAllPropertyRequirements(DataRequirement dataRequirement)
    {
        ArrayList<SectionRequirement> classRequirements;
        ArrayList<PropertyRequirement> propertyRequirements;

        propertyRequirements = new ArrayList<PropertyRequirement>();
        classRequirements = dataRequirement.getSectionRequirements();
        for (SectionRequirement classRequirement : classRequirements)
        {
            propertyRequirements.addAll(classRequirement.getPropertyRequirements());
        }

        return propertyRequirements;
    }

    public static ArrayList<ValueRequirement> getAllValueRequirements(DataRequirement dataRequirement)
    {
        ArrayList<SectionRequirement> classRequirements;
        ArrayList<ValueRequirement> valueRequirements;

        valueRequirements = new ArrayList<ValueRequirement>();
        classRequirements = dataRequirement.getSectionRequirements();
        for (SectionRequirement classRequirement : classRequirements)
        {
            ArrayList<PropertyRequirement> propertyRequirements;

            propertyRequirements = classRequirement.getPropertyRequirements();
            for (PropertyRequirement propertyRequirement : propertyRequirements)
            {
                valueRequirements.addAll(propertyRequirement.getAllValueRequirements());
            }
        }

        return valueRequirements;
    }

    public static RecordValue getFirstNumericSubValue(RecordValue value)
    {
        List<RecordValue> subValues;

        subValues = value.getSubValues();
        if (subValues != null)
        {
            List<RequirementType> numericDataTypes;

            numericDataTypes = RequirementType.getNumericDataTypes();
            for (RecordValue subValue : subValues)
            {
                RequirementType subValueDataType;

                subValueDataType = subValue.getValueRequirement().getRequirementType();
                if (numericDataTypes.contains(subValueDataType)) return subValue;
            }

            return null;
        }
        else return null;
    }
}
