package com.pilog.t8.data.document;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class PropertyValueStringCriterion implements Serializable
{
    private final String propertyIdentifier;
    private final String propertyValueString;

    public PropertyValueStringCriterion(String propertyIdentifier, String propertyValueString)
    {
        this.propertyIdentifier = propertyIdentifier;
        this.propertyValueString = propertyValueString;
    }

    public String getPropertyIdentifier()
    {
        return propertyIdentifier;
    }

    public String getPropertyValueString()
    {
        return propertyValueString;
    }
}
