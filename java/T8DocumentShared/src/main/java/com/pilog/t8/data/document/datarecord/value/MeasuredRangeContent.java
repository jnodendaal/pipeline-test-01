package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredRangeContent extends ValueContent
{
    private LowerBoundNumberContent lowerBoundNumber;
    private UpperBoundNumberContent upperBoundNumber;
    private UnitOfMeasureContent unitOfMeasure;
    private QualifierOfMeasureContent qualifierOfMeasure;

    public MeasuredRangeContent()
    {
        super(RequirementType.MEASURED_RANGE);
        this.lowerBoundNumber = new LowerBoundNumberContent();
        this.upperBoundNumber = new UpperBoundNumberContent();
        this.unitOfMeasure = new UnitOfMeasureContent();
        this.qualifierOfMeasure = new QualifierOfMeasureContent();
    }

    public LowerBoundNumberContent getLowerBoundNumber()
    {
        return lowerBoundNumber;
    }

    public void setLowerBoundNumber(LowerBoundNumberContent lowerBoundNumber)
    {
        this.lowerBoundNumber = lowerBoundNumber;
    }

    public UpperBoundNumberContent getUpperBoundNumber()
    {
        return upperBoundNumber;
    }

    public void setUpperBoundNumber(UpperBoundNumberContent upperBoundNumber)
    {
        this.upperBoundNumber = upperBoundNumber;
    }

    public UnitOfMeasureContent getUnitOfMeasure()
    {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasureContent unitOfMeasure)
    {
        this.unitOfMeasure = unitOfMeasure;
    }

    public QualifierOfMeasureContent getQualifierOfMeasure()
    {
        return qualifierOfMeasure;
    }

    public void setQualifierOfMeasure(QualifierOfMeasureContent qualifierOfMeasure)
    {
        this.qualifierOfMeasure = qualifierOfMeasure;
    }

    public boolean hasLowerBoundNumberContent()
    {
        return lowerBoundNumber != null && lowerBoundNumber.hasContent();
    }

    public boolean hasUpperBoundNumberContent()
    {
        return upperBoundNumber != null && upperBoundNumber.hasContent();
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof MeasuredRangeContent)
        {
            MeasuredRangeContent toMeasuredRangeContent;
            LowerBoundNumberContent toLowerBoundNumber;
            UpperBoundNumberContent toUpperBoundNumber;
            UnitOfMeasureContent toUnitOfMeasure;
            QualifierOfMeasureContent toQualifierOfMeasure;

            toMeasuredRangeContent = (MeasuredRangeContent)toContent;
            toLowerBoundNumber = toMeasuredRangeContent.getLowerBoundNumber();
            toUpperBoundNumber = toMeasuredRangeContent.getUpperBoundNumber();
            toUnitOfMeasure = toMeasuredRangeContent.getUnitOfMeasure();
            toQualifierOfMeasure = toMeasuredRangeContent.getQualifierOfMeasure();

            if (lowerBoundNumber == null)
            {
                if (toLowerBoundNumber != null && toLowerBoundNumber.hasContent()) return false;
            }
            else if (!lowerBoundNumber.isEqualTo(toLowerBoundNumber)) return false;

            if (upperBoundNumber == null)
            {
                if (toUpperBoundNumber != null && toUpperBoundNumber.hasContent()) return false;
            }
            else if (!upperBoundNumber.isEqualTo(toUpperBoundNumber)) return false;

            if (unitOfMeasure == null)
            {
                if (toUnitOfMeasure != null && toUnitOfMeasure.hasContent()) return false;
            }
            else if (!unitOfMeasure.isEqualTo(toUnitOfMeasure)) return false;

            if (qualifierOfMeasure == null)
            {
                if (toQualifierOfMeasure != null && toQualifierOfMeasure.hasContent()) return false;
            }
            else if (!qualifierOfMeasure.isEqualTo(toQualifierOfMeasure)) return false;

            return true;
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof MeasuredRangeContent)
        {
            MeasuredRangeContent toMeasuredRangeContent;
            LowerBoundNumberContent toLowerBoundNumber;
            UpperBoundNumberContent toUpperBoundNumber;
            UnitOfMeasureContent toUnitOfMeasure;
            QualifierOfMeasureContent toQualifierOfMeasure;

            toMeasuredRangeContent = (MeasuredRangeContent)toContent;
            toLowerBoundNumber = toMeasuredRangeContent.getLowerBoundNumber();
            toUpperBoundNumber = toMeasuredRangeContent.getUpperBoundNumber();
            toUnitOfMeasure = toMeasuredRangeContent.getUnitOfMeasure();
            toQualifierOfMeasure = toMeasuredRangeContent.getQualifierOfMeasure();

            if (lowerBoundNumber == null)
            {
                if (toLowerBoundNumber != null && toLowerBoundNumber.hasContent()) return false;
            }
            else if (!lowerBoundNumber.isEquivalentTo(toLowerBoundNumber)) return false;

            if (upperBoundNumber == null)
            {
                if (toUpperBoundNumber != null && toUpperBoundNumber.hasContent()) return false;
            }
            else if (!upperBoundNumber.isEquivalentTo(toUpperBoundNumber)) return false;

            if (unitOfMeasure == null)
            {
                if (toUnitOfMeasure != null && toUnitOfMeasure.hasContent()) return false;
            }
            else if (!unitOfMeasure.isEquivalentTo(toUnitOfMeasure)) return false;

            if (qualifierOfMeasure == null)
            {
                if (toQualifierOfMeasure != null && toQualifierOfMeasure.hasContent()) return false;
            }
            else if (!qualifierOfMeasure.isEquivalentTo(toQualifierOfMeasure)) return false;

            return true;
        }
        else return false;
    }
}
