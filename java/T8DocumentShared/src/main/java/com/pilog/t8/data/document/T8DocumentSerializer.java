package com.pilog.t8.data.document;

import com.pilog.json.JsonArray;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.json.WriterConfig;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarecord.value.AttachmentListContent;
import com.pilog.t8.data.document.datarecord.value.BooleanValue;
import com.pilog.t8.data.document.datarecord.value.BooleanValueContent;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.CompositeContent;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.ControlledConceptContent;
import com.pilog.t8.data.document.datarecord.value.DateTimeValue;
import com.pilog.t8.data.document.datarecord.value.DateTimeValueContent;
import com.pilog.t8.data.document.datarecord.value.DateValue;
import com.pilog.t8.data.document.datarecord.value.DateValueContent;
import com.pilog.t8.data.document.datarecord.value.DocumentReference;
import com.pilog.t8.data.document.datarecord.value.DocumentReference.DocumentReferenceType;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceContent;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceListContent;
import com.pilog.t8.data.document.datarecord.value.EpochValue;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarecord.value.FieldContent;
import com.pilog.t8.data.document.datarecord.value.IntegerValue;
import com.pilog.t8.data.document.datarecord.value.IntegerValueContent;
import com.pilog.t8.data.document.datarecord.value.LowerBoundNumberContent;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumberContent;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarecord.value.MeasuredRangeContent;
import com.pilog.t8.data.document.datarecord.value.NumberContent;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasure;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasureContent;
import com.pilog.t8.data.document.datarecord.value.RealValue;
import com.pilog.t8.data.document.datarecord.value.RealValueContent;
import com.pilog.t8.data.document.datarecord.value.StringValue;
import com.pilog.t8.data.document.datarecord.value.StringValueContent;
import com.pilog.t8.data.document.datarecord.value.TextValue;
import com.pilog.t8.data.document.datarecord.value.TextValueContent;
import com.pilog.t8.data.document.datarecord.value.TimeValue;
import com.pilog.t8.data.document.datarecord.value.TimeValueContent;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasure;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasureContent;
import com.pilog.t8.data.document.datarecord.value.UpperBoundNumberContent;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.DataDependency.DataDependencyType;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.AttachmentListRequirement;
import com.pilog.t8.data.document.datarequirement.value.BooleanRequirement;
import com.pilog.t8.data.document.datarequirement.value.CompositeRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.DateRequirement;
import com.pilog.t8.data.document.datarequirement.value.DateTimeRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.datarequirement.value.IntegerRequirement;
import com.pilog.t8.data.document.datarequirement.value.LowerBoundNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredRangeRequirement;
import com.pilog.t8.data.document.datarequirement.value.NumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.QualifierOfMeasureRequirement;
import com.pilog.t8.data.document.datarequirement.value.RealRequirement;
import com.pilog.t8.data.document.datarequirement.value.StringRequirement;
import com.pilog.t8.data.document.datarequirement.value.TextRequirement;
import com.pilog.t8.data.document.datarequirement.value.TimeRequirement;
import com.pilog.t8.data.document.datarequirement.value.UnitOfMeasureRequirement;
import com.pilog.t8.data.document.datarequirement.value.UpperBoundNumberRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.datatype.T8DtOntologyConcept;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DocumentSerializer
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DocumentSerializer.class);

    /**
     * Defines the way that Dr Instances will be serialized as part of a record structure.
     */
    public enum DrInstanceSerializationType
    {
        RECORD, // DR Instances for each record will be serialized and included in the record JSON.
        ROOT_RECORD, // DR Instances for each record will be serialized and included in a set on the root record JSON only.  All records will have drInstanceId references.
        EXCLUDED // DR Instances will not be serialized and all records will only contain a drInstanceId reference to the instance on which it is based.
    };

    private static final List<String> RECORD_STANDARD_NAMES = ArrayLists.typeSafeList
    (
        "recordId",
        "fft",
        "ontology",
        "subRecords",
        "drInstance",
        "drInstanceId",
        "drInstanceIrdi",
        "drId",
        "drIrdi",
        "classId",
        "classIrdi",
        "independent",
        "sections",
        "term",
        "abbreviation",
        "definition",
        "code"
    );

    private static final List<String> DR_INSTANCE_STANDARD_NAMES = ArrayLists.typeSafeList
    (
        "drInstanceId",
        "drInstanceIrdi",
        "drId",
        "drIrdi",
        "classId",
        "classIrdi",
        "independent",
        "ontology",
        "sections",
        "term",
        "abbreviation",
        "definition",
        "code"
    );

    private static final List<String> PROPERTY_REQUIREMENT_RESERVED_NAMES = ArrayLists.typeSafeList
    (
        "propertyId",
        "propertyIrdi",
        "characteristic",
        "term",
        "abbreviation",
        "definition",
        "code",
        "value"
    );

    private static final List<String> VALUE_REQUIREMENT_STANDARD_NAMES = ArrayLists.typeSafeList
    (
        "propertyId",
        "propertyIrdi",
        "fieldId",
        "fieldIrdi",
        "fields",
        "ontologyClassId",
        "term",
        "abbreviation",
        "definition",
        "code",
        "value",
        "number",
        "dependencies",
        "unitOfMeasure",
        "qualifiedOfMeasure",
        "lowerBoundNumber",
        "upperBoundNumber"
    );

    public static String toString(JsonValue value, boolean prettyPrint)
    {
        if (value != null)
        {
            // Write the JSON object to string output.
            return value.toString(prettyPrint ? WriterConfig.PRETTY_PRINT : WriterConfig.MINIMAL);
        }
        else return null;
    }

    public static JsonValue serializeDataRecord(DataRecord dataRecord, boolean includeDrInstance, boolean includeAccessLayer, boolean includeDescendants)
    {
        if (dataRecord != null)
        {
            T8OntologyConcept ontology;
            JsonObject recordObject;
            JsonArray sectionArray;

            // Create the section array.
            sectionArray = new JsonArray();
            for (RecordSection section : dataRecord.getRecordSections())
            {
                sectionArray.add(serializeRecordSection(section));
            }

            // Create the record JSON object.
            recordObject = new JsonObject();
            recordObject.add("recordId", dataRecord.getID());
            recordObject.add("drInstanceId", dataRecord.getDataRequirementInstanceId());

            // Add any other additional record attributes.
            addRecordAttributes(recordObject, dataRecord);

            // Include the record ontology.
            ontology = dataRecord.getOntology();
            if (ontology != null) recordObject.add("ontology", new T8DtOntologyConcept(null).serialize(ontology));

            // Now add the record content.
            recordObject.add("sections", sectionArray);
            recordObject.addIfNotNull("fft", dataRecord.getFFT());

            // Add sub-records if required.
            if (includeDescendants)
            {
                JsonArray subRecordArray;

                subRecordArray = new JsonArray();
                for (DataRecord subRecord : dataRecord.getSubRecords())
                {
                    subRecordArray.add(serializeDataRecord(subRecord, includeDrInstance, includeAccessLayer, includeDescendants));
                }

                recordObject.addIfNotNullOrEmpty("subRecords", subRecordArray);
            }

            // Include DR Instance.
            if (includeDrInstance)
            {
                recordObject.add("drInstance", serializeDrInstance(dataRecord.getDataRequirementInstance()));
            }

            // Include the access layer.
            if (includeAccessLayer)
            {
                recordObject.add("access", serializeRecordAccessLayer(dataRecord.getAccessLayer(), false));
            }

            // Return the completed record.
            return recordObject;
        }
        else return null;
    }

    public static JsonObject serializeRecordSection(RecordSection section)
    {
        JsonObject sectionObject;
        JsonArray propertyArray;

        // Create the property array.
        propertyArray = new JsonArray();
        for (RecordProperty property : section.getRecordProperties())
        {
            propertyArray.add(serializeRecordProperty(property));
        }

        // Create the section JSON object.
        sectionObject = new JsonObject();
        sectionObject.add("sectionId", section.getSectionID());
        sectionObject.add("properties", propertyArray);
        return sectionObject;
    }

    public static JsonObject serializeRecordProperty(RecordProperty property)
    {
        JsonObject propertyObject;

        // Create the property JSON object.
        propertyObject = new JsonObject();
        propertyObject.add("propertyId", property.getPropertyID());
        propertyObject.addIfNotNull("fft", property.getFFT());
        propertyObject.add("value", serializeRecordValue(property.getRecordValue()));
        return propertyObject;
    }

    public static JsonValue serializeRecordValue(RecordValue value)
    {
        if (value != null)
        {
            JsonObject valueObject;
            RequirementType requirementType;

            valueObject = new JsonObject();
            requirementType = value.getRequirementType();
            switch (requirementType)
            {
                case STRING_TYPE:
                {
                    valueObject.add("type", "STRING");
                    return valueObject.add("string", ((StringValue)value).getString());
                }
                case BOOLEAN_TYPE:
                {
                    valueObject.add("type", "BOOLEAN");
                    return valueObject.add("value", "true".equalsIgnoreCase(value.getValue()));
                }
                case TEXT_TYPE:
                {
                    valueObject.add("type", "TEXT");
                    return valueObject.add("text", value.getValue());
                }
                case INTEGER_TYPE:
                {
                    valueObject.add("type", "INTEGER");
                    return valueObject.add("value", ((IntegerValue)value).getIntegerValue());
                }
                case REAL_TYPE:
                {
                    valueObject.add("type", "REAL");
                    return valueObject.add("value", value.getValue()); // Using a numeric type here causes issues with format restrictions after deserialization
                }
                case DATE_TYPE:
                {
                    valueObject.add("type", "DATE");
                    valueObject.add("millis", ((EpochValue)value).getMilliseconds());
                    valueObject.add("date", value.getValue());
                    return valueObject;
                }
                case TIME_TYPE:
                {
                    valueObject.add("type", "TIME");
                    valueObject.add("millis", ((EpochValue)value).getMilliseconds());
                    valueObject.add("time", value.getValue());
                    return valueObject;
                }
                case DATE_TIME_TYPE:
                {
                    valueObject.add("type", "DATE_TIME");
                    valueObject.add("millis", ((EpochValue)value).getMilliseconds());
                    valueObject.add("dateTime", value.getValue());
                    return valueObject;
                }
                case CONTROLLED_CONCEPT:
                {
                    ControlledConcept controlledConcept;

                    controlledConcept = (ControlledConcept)value;
                    valueObject.add("type", "CONTROLLED_CONCEPT");
                    valueObject.add("conceptId", controlledConcept.getConceptId());
                    valueObject.addIfNotNull("term", controlledConcept.getTerm());
                    valueObject.addIfNotNull("definition", controlledConcept.getDefinition());
                    valueObject.addIfNotNull("code", controlledConcept.getCode());
                    valueObject.addIfNotNull("abbreviation", controlledConcept.getAbbreviation());

                    return valueObject;
                }
                case UNIT_OF_MEASURE:
                {
                    UnitOfMeasure uom;

                    uom = (UnitOfMeasure)value;
                    valueObject.add("type", "UNIT_OF_MEASURE");
                    valueObject.add("conceptId", uom.getConceptId());
                    valueObject.addIfNotNull("term", uom.getTerm());
                    valueObject.addIfNotNull("definition", uom.getDefinition());
                    valueObject.addIfNotNull("code", uom.getCode());
                    valueObject.addIfNotNull("abbreviation", uom.getAbbreviation());

                    return valueObject;
                }
                case COMPOSITE_TYPE:
                {
                    JsonArray fieldArray;

                    // Create the field array.
                    fieldArray = new JsonArray();
                    for (RecordValue field : value.getSubValues())
                    {
                        JsonObject fieldObject;
                        String fieldId;

                        fieldObject = new JsonObject();
                        fieldId = field.getFieldID();
                        fieldObject.add("fieldId", fieldId);
                        fieldObject.add("value", serializeRecordValue(field.getFirstSubValue()));
                        fieldArray.add(fieldObject);
                    }

                    // Create the Composite JSON object.
                    valueObject.add("type", "COMPOSITE");
                    return valueObject.add("fields", fieldArray);
                }
                case MEASURED_NUMBER:
                {
                    MeasuredNumber measuredNumber;
                    QualifierOfMeasure qom;
                    UnitOfMeasure uom;
                    String uomId;
                    String qomId;

                    measuredNumber = (MeasuredNumber)value;
                    uom = measuredNumber.getUom();
                    qom = measuredNumber.getQom();
                    uomId = measuredNumber.getUomId();
                    qomId = measuredNumber.getQomId();

                    valueObject.add("type", "MEASURED_NUMBER");
                    valueObject.add("number", measuredNumber.getNumber());
                    valueObject.add("unitOfMeasureId", uomId);
                    if (uom != null)
                    {
                        valueObject.addIfNotNull("unitOfMeasureTerm", uom.getTerm());
                        valueObject.addIfNotNull("unitOfMeasureDefinition", uom.getDefinition());
                        valueObject.addIfNotNull("unitOfMeasureCode", uom.getCode());
                    }

                    valueObject.add("qualifierOfMeasureId", qomId);
                    if (qom != null)
                    {
                        valueObject.addIfNotNull("qualifierOfMeasureTerm", qom.getTerm());
                        valueObject.addIfNotNull("qualifierOfMeasureDefinition", qom.getDefinition());
                        valueObject.addIfNotNull("qualifierOfMeasureCode", qom.getCode());
                    }

                    return valueObject;
                }
                case MEASURED_RANGE:
                {
                    MeasuredRange measuredRange;
                    QualifierOfMeasure qom;
                    UnitOfMeasure uom;
                    String uomId;
                    String qomId;

                    measuredRange = (MeasuredRange)value;
                    uom = measuredRange.getUom();
                    qom = measuredRange.getQom();
                    uomId = measuredRange.getUomId();
                    qomId = measuredRange.getQomId();

                    valueObject.add("type", "MEASURED_RANGE");
                    valueObject.add("lowerBoundNumber", measuredRange.getLowerBoundNumber());
                    valueObject.add("upperBoundNumber", measuredRange.getUpperBoundNumber());
                    valueObject.add("unitOfMeasureId", uomId);
                    if (uom != null)
                    {
                        valueObject.addIfNotNull("unitOfMeasureTerm", uom.getTerm());
                        valueObject.addIfNotNull("unitOfMeasureDefinition", uom.getDefinition());
                        valueObject.addIfNotNull("unitOfMeasureCode", uom.getCode());
                    }

                    valueObject.addIfNotNull("qualifierOfMeasureId", qomId);
                    if (qom != null)
                    {
                        valueObject.addIfNotNull("qualifierOfMeasureTerm", qom.getTerm());
                        valueObject.addIfNotNull("qualifierOfMeasureDefinition", qom.getDefinition());
                        valueObject.addIfNotNull("qualifierOfMeasureCode", qom.getCode());
                    }

                    return valueObject;
                }
                case DOCUMENT_REFERENCE:
                {
                    DocumentReferenceList referenceList;
                    JsonArray referenceArray;

                    // Create the reference array.
                    referenceList = (DocumentReferenceList)value;
                    referenceArray = new JsonArray();
                    for (DocumentReference reference : referenceList.getReferences())
                    {
                        JsonObject referenceObject;

                        // Add the reference object to the reference array.
                        referenceObject = new JsonObject();
                        referenceObject.add("referenceType", reference.getReferenceType().toString());
                        referenceObject.add("recordId", reference.getConceptId()); // This line is present in order to remain backwards compatible.
                        referenceObject.add("conceptId", reference.getConceptId());
                        referenceObject.add("code", reference.getCode());
                        referenceObject.add("term", reference.getTerm());
                        referenceObject.add("definition", reference.getDefinition());
                        referenceArray.add(referenceObject);
                    }

                    // Create the Document Reference JSON object.
                    valueObject.add("type", "DOCUMENT_REFERENCE");
                    valueObject.add("references", referenceArray);
                    return valueObject;
                }
                case ATTACHMENT:
                {
                    AttachmentList attachmentList;
                    JsonArray attachmentArray;

                    // Get the attachment list.
                    attachmentList = (AttachmentList)value;
                    attachmentArray = new JsonArray();
                    for (String attachmentId : attachmentList.getAttachmentIds())
                    {
                        T8AttachmentDetails attachmentDetails;
                        JsonObject jsonAttachment;

                        // Add the reference object to the reference array.
                        jsonAttachment = new JsonObject();
                        jsonAttachment.add("attachmentId", attachmentId);

                        // Add the optional attachment details.
                        attachmentDetails = attachmentList.getAttachmentDetails(attachmentId);
                        if (attachmentDetails != null)
                        {
                            jsonAttachment.add("fileContextId", attachmentDetails.getContextId());
                            jsonAttachment.add("fileContextIid", attachmentDetails.getContextIid());
                            jsonAttachment.add("filePath", attachmentDetails.getFilePath());
                            jsonAttachment.add("fileName", attachmentDetails.getFileName());
                            jsonAttachment.add("originalFileName", attachmentDetails.getOriginalFileName());
                            jsonAttachment.add("fileSize", attachmentDetails.getFileSize());
                            jsonAttachment.add("md5", attachmentDetails.getMD5Checksum());
                            jsonAttachment.add("mediaType", attachmentDetails.getMediaType());
                        }

                        // Add the attachment object to the array.
                        attachmentArray.add(jsonAttachment);
                    }

                    valueObject.add("type", "ATTACHMENT");
                    valueObject.add("attachments", attachmentArray);
                    return valueObject;
                }
                default:
                {
                    LOGGER.log(T8Logger.Level.WARNING, "Data Type [" + requirementType + "] not serialized.");
                    return JsonValue.NULL;
                }
            }
        }
        else return JsonValue.NULL;
    }

    public static JsonValue serializeValueContent(ValueContent value)
    {
        if (value != null)
        {
            JsonObject valueObject;
            RequirementType requirementType;

            valueObject = new JsonObject();
            requirementType = value.getType();
            switch (requirementType)
            {
                case STRING_TYPE:
                {
                    valueObject.add("type", "STRING");
                    return valueObject.add("string", ((StringValueContent)value).getString());
                }
                case NUMBER:
                {
                    valueObject.add("type", "NUMBER");
                    return valueObject.add("value", ((NumberContent)value).getValue());
                }
                case LOWER_BOUND_NUMBER:
                {
                    valueObject.add("type", "LOWER_BOUND_NUMBER");
                    return valueObject.add("value", ((LowerBoundNumberContent)value).getValue());
                }
                case UPPER_BOUND_NUMBER:
                {
                    valueObject.add("type", "UPPER_BOUND_NUMBER");
                    return valueObject.add("value", ((UpperBoundNumberContent)value).getValue());
                }
                case BOOLEAN_TYPE:
                {
                    valueObject.add("type", "BOOLEAN");
                    return valueObject.add("value", ((BooleanValueContent)value).getValue());
                }
                case TEXT_TYPE:
                {
                    valueObject.add("type", "TEXT");
                    return valueObject.add("text", ((TextValueContent)value).getText());
                }
                case INTEGER_TYPE:
                {
                    valueObject.add("type", "INTEGER");
                    return valueObject.add("value", ((IntegerValueContent)value).getValue());
                }
                case REAL_TYPE:
                {
                    valueObject.add("type", "REAL");
                    return valueObject.add("value", ((RealValueContent)value).getValue()); // Using a numeric type here causes issues with format restrictions after deserialization.
                }
                case DATE_TYPE:
                {
                    valueObject.add("type", "DATE");
                    return valueObject.add("date", ((DateValueContent)value).getDate());
                }
                case TIME_TYPE:
                {
                    valueObject.add("type", "TIME");
                    return valueObject.add("time", ((TimeValueContent)value).getTime());
                }
                case DATE_TIME_TYPE:
                {
                    valueObject.add("type", "DATE_TIME");
                    return valueObject.add("dateTime", ((DateTimeValueContent)value).getDateTime());
                }
                case CONTROLLED_CONCEPT:
                {
                    ControlledConceptContent controlledConcept;

                    controlledConcept = (ControlledConceptContent)value;
                    valueObject.add("type", "CONTROLLED_CONCEPT");
                    valueObject.add("conceptId", controlledConcept.getConceptId());
                    valueObject.addIfNotNull("termId", controlledConcept.getTermId());
                    valueObject.addIfNotNull("term", controlledConcept.getTerm());
                    valueObject.addIfNotNull("abbreviationId", controlledConcept.getAbbreviationId());
                    valueObject.addIfNotNull("abbreviation", controlledConcept.getAbbreviation());
                    valueObject.addIfNotNull("definitionId", controlledConcept.getDefinitionId());
                    valueObject.addIfNotNull("definition", controlledConcept.getDefinition());
                    valueObject.addIfNotNull("codeId", controlledConcept.getCodeId());
                    valueObject.addIfNotNull("code", controlledConcept.getCode());
                    return valueObject;
                }
                case UNIT_OF_MEASURE:
                {
                    UnitOfMeasureContent uomContent;

                    uomContent = (UnitOfMeasureContent)value;
                    valueObject.add("type", "UNIT_OF_MEASURE");
                    valueObject.add("conceptId", uomContent.getConceptId());
                    valueObject.addIfNotNull("termId", uomContent.getTermId());
                    valueObject.addIfNotNull("term", uomContent.getTerm());
                    valueObject.addIfNotNull("abbreviationId", uomContent.getAbbreviationId());
                    valueObject.addIfNotNull("abbreviation", uomContent.getAbbreviation());
                    valueObject.addIfNotNull("definitionId", uomContent.getDefinitionId());
                    valueObject.addIfNotNull("definition", uomContent.getDefinition());
                    valueObject.addIfNotNull("codeId", uomContent.getCodeId());
                    valueObject.addIfNotNull("code", uomContent.getCode());
                    return valueObject;
                }
                case QUALIFIER_OF_MEASURE:
                {
                    QualifierOfMeasureContent qomContent;

                    qomContent = (QualifierOfMeasureContent)value;
                    valueObject.add("type", "QUALIFIER_OF_MEASURE");
                    valueObject.add("conceptId", qomContent.getConceptId());
                    valueObject.addIfNotNull("termId", qomContent.getTermId());
                    valueObject.addIfNotNull("term", qomContent.getTerm());
                    valueObject.addIfNotNull("abbreviationId", qomContent.getAbbreviationId());
                    valueObject.addIfNotNull("abbreviation", qomContent.getAbbreviation());
                    valueObject.addIfNotNull("definitionId", qomContent.getDefinitionId());
                    valueObject.addIfNotNull("definition", qomContent.getDefinition());
                    valueObject.addIfNotNull("codeId", qomContent.getCodeId());
                    valueObject.addIfNotNull("code", qomContent.getCode());
                    return valueObject;
                }
                case COMPOSITE_TYPE:
                {
                    CompositeContent composite;
                    JsonArray fieldArray;

                    // Create the field array.
                    composite = (CompositeContent)value;
                    fieldArray = new JsonArray();
                    for (FieldContent field : composite.getFields())
                    {
                        JsonObject fieldObject;
                        String fieldId;

                        fieldObject = new JsonObject();
                        fieldId = field.getId();
                        fieldObject.add("fieldId", fieldId);
                        fieldObject.add("value", serializeValueContent(field.getValue()));
                        fieldArray.add(fieldObject);
                    }

                    // Create the Composite JSON object.
                    valueObject.add("type", "COMPOSITE");
                    return valueObject.add("fields", fieldArray);
                }
                case MEASURED_NUMBER:
                {
                    MeasuredNumberContent measuredNumber;

                    measuredNumber = (MeasuredNumberContent)value;
                    valueObject.add("type", "MEASURED_NUMBER");
                    valueObject.add("number", serializeValueContent(measuredNumber.getNumber()));
                    valueObject.add("unitOfMeasure", serializeValueContent(measuredNumber.getUnitOfMeasure()));
                    valueObject.add("qualifierOfMeasure", serializeValueContent(measuredNumber.getQualifierOfMeasure()));
                    return valueObject;
                }
                case MEASURED_RANGE:
                {
                    MeasuredRangeContent measuredRange;

                    measuredRange = (MeasuredRangeContent)value;
                    valueObject.add("type", "MEASURED_RANGE");
                    valueObject.add("lowerBoundNumber", serializeValueContent(measuredRange.getLowerBoundNumber()));
                    valueObject.add("upperBoundNumber", serializeValueContent(measuredRange.getUpperBoundNumber()));
                    valueObject.add("unitOfMeasure", serializeValueContent(measuredRange.getUnitOfMeasure()));
                    valueObject.add("qualifierOfMeasure", serializeValueContent(measuredRange.getQualifierOfMeasure()));
                    return valueObject;
                }
                case DOCUMENT_REFERENCE:
                {
                    DocumentReferenceListContent referenceList;
                    JsonArray referenceArray;

                    // Create the reference array.
                    referenceList = (DocumentReferenceListContent)value;
                    referenceArray = new JsonArray();
                    for (DocumentReferenceContent reference : referenceList.getReferences())
                    {
                        JsonObject referenceObject;
                        DocumentReferenceType referenceType;

                        referenceType = reference.getReferenceType();

                        // Add the reference object to the reference array.
                        referenceObject = new JsonObject();
                        referenceObject.addIfNotNull("referenceType", referenceType != null ? referenceType.toString() : null);
                        referenceObject.add("conceptId", reference.getConceptId());
                        referenceObject.add("code", reference.getCode());
                        referenceObject.add("term", reference.getTerm());
                        referenceObject.add("definition", reference.getDefinition());
                        referenceArray.add(referenceObject);
                    }

                    // Create the Document Reference JSON object.
                    valueObject.add("type", "DOCUMENT_REFERENCE");
                    valueObject.add("references", referenceArray);
                    return valueObject;
                }
                case ATTACHMENT:
                {
                    AttachmentListContent attachmentList;
                    JsonArray attachmentArray;

                    // Get the attachment list.
                    attachmentList = (AttachmentListContent)value;
                    attachmentArray = new JsonArray();
                    for (String attachmentId : attachmentList.getAttachmentIdList())
                    {
                        T8AttachmentDetails attachmentDetails;
                        JsonObject attachmentObject;

                        // Add the reference object to the reference array.
                        attachmentObject = new JsonObject();
                        attachmentObject.add("attachmentId", attachmentId);

                        // Add the optional attachment details.
                        attachmentDetails = attachmentList.getAttachmentDetails(attachmentId);
                        if (attachmentDetails != null)
                        {
                            attachmentObject.add("fileContextId", attachmentDetails.getContextId());
                            attachmentObject.add("fileContextIid", attachmentDetails.getContextIid());
                            attachmentObject.add("filePath", attachmentDetails.getFilePath());
                            attachmentObject.add("fileName", attachmentDetails.getFileName());
                            attachmentObject.add("originalFileName", attachmentDetails.getOriginalFileName());
                            attachmentObject.add("fileSize", attachmentDetails.getFileSize());
                            attachmentObject.add("md5", attachmentDetails.getMD5Checksum());
                            attachmentObject.add("mediaType", attachmentDetails.getMediaType());
                        }

                        // Add the attachment object to the array.
                        attachmentArray.add(attachmentObject);
                    }

                    valueObject.add("type", "ATTACHMENT");
                    valueObject.add("attachments", attachmentArray);
                    return valueObject;
                }
                default:
                {
                    LOGGER.log(T8Logger.Level.WARNING, "Data Type [" + requirementType + "] not serialized.");
                    return JsonValue.NULL;
                }
            }
        }
        else return JsonValue.NULL;
    }

    public static JsonValue serializeRecordAccessLayer(DataRecordAccessLayer recordAccess, boolean includeDescendants)
    {
        if (recordAccess != null)
        {
            JsonObject jsonRecordAccess;
            JsonArray jsonSectionArray;
            JsonArray subRecordArray;

            // Create the sub record array if specified to be included.
            if (includeDescendants)
            {
                subRecordArray = new JsonArray();
                for (DataRecordAccessLayer subRecordAccess : recordAccess.getSubRecordAccessLayers())
                {
                    subRecordArray.add(serializeRecordAccessLayer(subRecordAccess, includeDescendants));
                }
            }
            else subRecordArray = null;

            // Create the section array.
            jsonSectionArray = new JsonArray();
            for (SectionAccessLayer section : recordAccess.getSectionAccessLayers())
            {
                jsonSectionArray.add(serializeSectionAccessLayer(section));
            }

            // Create the record JSON object.
            jsonRecordAccess = new JsonObject();
            jsonRecordAccess.add("recordId", recordAccess.getRecordId());
            jsonRecordAccess.add("drInstanceId", recordAccess.getDataRequirementInstanceId());
            jsonRecordAccess.add("sections", jsonSectionArray);
            jsonRecordAccess.add("editable", recordAccess.isEditable());
            jsonRecordAccess.add("visible", recordAccess.isVisible());
            jsonRecordAccess.add("fftEditable", recordAccess.isFftEditable());

            // Add sub-records if required.
            if (includeDescendants)
            {
                jsonRecordAccess.addIfNotNullOrEmpty("subRecords", subRecordArray);
            }

            // Return the completed json.
            return jsonRecordAccess;
        }
        else return null;
    }

    public static JsonObject serializeSectionAccessLayer(SectionAccessLayer section)
    {
        JsonObject jsonSection;
        JsonArray jsonPropertyArray;

        // Create the property array.
        jsonPropertyArray = new JsonArray();
        for (PropertyAccessLayer property : section.getPropertyAccessLayers())
        {
            jsonPropertyArray.add(serializePropertyAccessLayer(property));
        }

        // Create the section JSON object.
        jsonSection = new JsonObject();
        jsonSection.add("sectionId", section.getSectionID());
        jsonSection.add("visible", section.isVisible());
        jsonSection.add("properties", jsonPropertyArray);
        return jsonSection;
    }

    public static JsonObject serializePropertyAccessLayer(PropertyAccessLayer property)
    {
        JsonObject jsonProperty;

        // Create the property JSON object.
        jsonProperty = new JsonObject();
        jsonProperty.add("propertyId", property.getPropertyId());
        jsonProperty.add("editable", property.isEditable());
        jsonProperty.add("visible", property.isVisible());
        jsonProperty.add("required", property.isRequired());
        jsonProperty.add("insertEnabled", property.isInsertEnabled());
        jsonProperty.add("updateEnabled", property.isUpdateEnabled());
        jsonProperty.add("deleteEnabled", property.isDeleteEnabled());
        jsonProperty.add("fftEditable", property.isFftEditable());
        return jsonProperty;
    }

    public static JsonObject serializeDrInstance(DataRequirementInstance drInstance)
    {
        if (drInstance != null)
        {
            JsonObject drInstanceObject;
            JsonArray sectionArray;

            // Create the section array.
            sectionArray = new JsonArray();
            for (SectionRequirement sectionRequirement : drInstance.getDataRequirement().getSectionRequirements())
            {
                sectionArray.add(serializeSectionRequirement(sectionRequirement));
            }

            // Create the record JSON object.
            drInstanceObject = new JsonObject();
            drInstanceObject.add("drInstanceId", drInstance.getConceptID());
            drInstanceObject.add("drId", drInstance.getDataRequirement().getConceptID());
            drInstanceObject.add("classId", drInstance.getDataRequirement().getClassConceptID());
            drInstanceObject.add("independent", drInstance.isIndependent());
            drInstanceObject.add("term", drInstance.getTerm());
            drInstanceObject.add("sections", sectionArray);
            addDrAttributes(drInstanceObject, drInstance.getDataRequirement());
            return drInstanceObject;
        }
        else return null;
    }

    public static JsonObject serializeDataRequirement(DataRequirement dr)
    {
        if (dr != null)
        {
            JsonObject drInstanceObject;
            JsonArray sectionArray;

            // Create the section array.
            sectionArray = new JsonArray();
            for (SectionRequirement sectionRequirement : dr.getSectionRequirements())
            {
                sectionArray.add(serializeSectionRequirement(sectionRequirement));
            }

            // Create the record JSON object.
            drInstanceObject = new JsonObject();
            drInstanceObject.add("drId", dr.getConceptID());
            drInstanceObject.add("classId", dr.getClassConceptID());
            drInstanceObject.add("term", dr.getTerm());
            drInstanceObject.add("sections", sectionArray);
            addDrAttributes(drInstanceObject, dr);
            return drInstanceObject;
        }
        else return null;
    }

    public static JsonObject serializeSectionRequirement(SectionRequirement section)
    {
        JsonObject sectionObject;
        JsonArray propertyArray;

        // Create the property array.
        propertyArray = new JsonArray();
        for (PropertyRequirement property : section.getPropertyRequirements())
        {
            propertyArray.add(serializePropertyRequirement(property));
        }

        // Create the section JSON object.
        sectionObject = new JsonObject();
        sectionObject.add("sectionId", section.getConceptID());
        sectionObject.add("term", section.getTerm());
        sectionObject.add("properties", propertyArray);
        return sectionObject;
    }

    public static JsonObject serializePropertyRequirement(PropertyRequirement property)
    {
        JsonObject propertyObject;

        // Create the property JSON object.
        propertyObject = new JsonObject();
        propertyObject.add("propertyId", property.getConceptID());
        propertyObject.add("term", property.getTerm());
        propertyObject.add("definition", property.getDefinition());
        propertyObject.add("characteristic", property.isCharacteristic());
        propertyObject.add("value", serializeValueRequirement(property.getValueRequirement()));
        addPropertyRequirementAttributes(propertyObject, property);
        return propertyObject;
    }

    public static JsonObject serializeValueRequirement(ValueRequirement valueRequirement)
    {
        if (valueRequirement != null)
        {
            JsonObject valueObject;
            RequirementType requirementType;

            valueObject = new JsonObject();
            requirementType = valueRequirement.getRequirementType();
            switch (requirementType)
            {
                case STRING_TYPE:
                {
                    StringRequirement stringRequirement;

                    stringRequirement = (StringRequirement)valueRequirement;
                    valueObject.add("type", "STRING");
                    addValueRequirementAttributes(valueObject, stringRequirement);
                    return valueObject;
                }
                case BOOLEAN_TYPE:
                {
                    valueObject.add("type", "BOOLEAN");
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case TEXT_TYPE:
                {
                    valueObject.add("type", "TEXT");
                    return valueObject;
                }
                case INTEGER_TYPE:
                {
                    IntegerRequirement integerRequirement;

                    integerRequirement = (IntegerRequirement)valueRequirement;
                    valueObject.add("type", "INTEGER");
                    addValueRequirementAttributes(valueObject, integerRequirement);
                    return valueObject;
                }
                case REAL_TYPE:
                {
                    RealRequirement realRequirement;

                    realRequirement = (RealRequirement)valueRequirement;
                    valueObject.add("type", "REAL");
                    addValueRequirementAttributes(valueObject, realRequirement);
                    return valueObject;
                }
                case NUMBER:
                {
                    NumberRequirement numberRequirement;

                    numberRequirement = (NumberRequirement)valueRequirement;
                    valueObject.add("type", "NUMBER");
                    addValueRequirementAttributes(valueObject, numberRequirement);
                    return valueObject;
                }
                case LOWER_BOUND_NUMBER:
                {
                    LowerBoundNumberRequirement lowerBoundRequirement;

                    lowerBoundRequirement = (LowerBoundNumberRequirement)valueRequirement;
                    valueObject.add("type", "LOWER_BOUND_NUMBER");
                    addValueRequirementAttributes(valueObject, lowerBoundRequirement);
                    return valueObject;
                }
                case UPPER_BOUND_NUMBER:
                {
                    UpperBoundNumberRequirement upperBoundRequirement;

                    upperBoundRequirement = (UpperBoundNumberRequirement)valueRequirement;
                    valueObject.add("type", "UPPER_BOUND_NUMBER");
                    addValueRequirementAttributes(valueObject, upperBoundRequirement);
                    return valueObject;
                }
                case DATE_TYPE:
                {
                    valueObject.add("type", "DATE");
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case TIME_TYPE:
                {
                    valueObject.add("type", "TIME");
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case DATE_TIME_TYPE:
                {
                    valueObject.add("type", "DATE_TIME");
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case CONTROLLED_CONCEPT:
                {
                    ControlledConceptRequirement conceptRequirement;
                    JsonArray dependencies;

                    // Cast to concept requirement.
                    conceptRequirement = (ControlledConceptRequirement)valueRequirement;

                    // Serialize any possible dependencies.
                    dependencies = serializeDataDependencies(valueRequirement);

                    // Create the Controlled Concept JSON object.
                    valueObject.add("type", "CONTROLLED_CONCEPT");
                    valueObject.add("ontologyClassId", conceptRequirement.getOntologyClassID());
                    if (!dependencies.isEmpty()) valueObject.add("dependencies", dependencies);
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case UNIT_OF_MEASURE:
                {
                    UnitOfMeasureRequirement uomRequirement;

                    // Cast to concept requirement.
                    uomRequirement = (UnitOfMeasureRequirement)valueRequirement;

                    // Create the Controlled Concept JSON object.
                    valueObject.add("type", "UNIT_OF_MEASURE");
                    valueObject.add("ontologyClassId", uomRequirement.getOntologyClassID());
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case QUALIFIER_OF_MEASURE:
                {
                    QualifierOfMeasureRequirement qomRequirement;

                    // Cast to concept requirement.
                    qomRequirement = (QualifierOfMeasureRequirement)valueRequirement;

                    // Create the Controlled Concept JSON object.
                    valueObject.add("type", "QUALIFIER_OF_MEASURE");
                    valueObject.add("ontologyClassId", qomRequirement.getOntologyClassID());
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case COMPOSITE_TYPE:
                {
                    CompositeRequirement compositeRequirement;
                    JsonArray fieldArray;

                    // Create the field array.
                    fieldArray = new JsonArray();
                    compositeRequirement = (CompositeRequirement)valueRequirement;
                    for (FieldRequirement fieldRequirement : compositeRequirement.getFieldRequirements())
                    {
                        JsonObject fieldObject;
                        String fieldId;

                        fieldObject = new JsonObject();
                        fieldId = fieldRequirement.getFieldID();
                        fieldObject.add("type", "FIELD");
                        fieldObject.add("fieldId", fieldId);
                        fieldObject.addIfNotNull("term", fieldRequirement.getTerm());
                        fieldObject.addIfNotNull("abbreviation", fieldRequirement.getAbbreviation());
                        fieldObject.addIfNotNull("definition", fieldRequirement.getDefinition());
                        fieldObject.add("value", serializeValueRequirement(fieldRequirement.getFirstSubRequirement()));
                        addValueRequirementAttributes(fieldObject, fieldRequirement);
                        fieldArray.add(fieldObject);
                    }

                    // Create the Composite JSON object.
                    valueObject.add("type", "COMPOSITE");
                    valueObject.add("fields", fieldArray);
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case MEASURED_NUMBER:
                {
                    MeasuredNumberRequirement measuredNumberRequirement;

                    // Cast to measured number requirement.
                    measuredNumberRequirement = (MeasuredNumberRequirement)valueRequirement;

                    // Create the Controlled Concept JSON object.
                    valueObject.add("type", "MEASURED_NUMBER");
                    valueObject.add("number", serializeValueRequirement(measuredNumberRequirement.getNumberRequirement()));
                    valueObject.addIfNotNull("unitOfMeasure", serializeValueRequirement(measuredNumberRequirement.getUomRequirement()));
                    valueObject.addIfNotNull("qualifierOfMeasure", serializeValueRequirement(measuredNumberRequirement.getQomRequirement()));
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case MEASURED_RANGE:
                {
                    MeasuredRangeRequirement measuredRangeRequirement;

                    // Cast to measured range requirement.
                    measuredRangeRequirement = (MeasuredRangeRequirement)valueRequirement;

                    // Create the Controlled Concept JSON object.
                    valueObject.add("type", "MEASURED_RANGE");
                    valueObject.add("lowerBoundNumber", serializeValueRequirement(measuredRangeRequirement.getLowerBoundNumberRequirement()));
                    valueObject.add("upperBoundNumber", serializeValueRequirement(measuredRangeRequirement.getUpperBoundNumberRequirement()));
                    valueObject.addIfNotNull("unitOfMeasure", serializeValueRequirement(measuredRangeRequirement.getUomRequirement()));
                    valueObject.addIfNotNull("qualifierOfMeasure", serializeValueRequirement(measuredRangeRequirement.getQomRequirement()));
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case DOCUMENT_REFERENCE:
                {
                    DocumentReferenceRequirement referenceRequirement;
                    JsonArray dependencies;

                    // Cast to reference requirement.
                    referenceRequirement = (DocumentReferenceRequirement)valueRequirement;

                    // Serialize any possible dependencies.
                    dependencies = serializeDataDependencies(valueRequirement);

                    // Create the Document Reference JSON object.
                    valueObject.add("type", "DOCUMENT_REFERENCE");
                    valueObject.add("ontologyClassId", referenceRequirement.getOntologyClassId());
                    if (!dependencies.isEmpty()) valueObject.add("dependencies", dependencies);
                    addValueRequirementAttributes(valueObject, valueRequirement);
                    return valueObject;
                }
                case ATTACHMENT:
                {
                    AttachmentListRequirement attachmentListRequirement;

                    // Cast the attachment requirement.
                    attachmentListRequirement = (AttachmentListRequirement)valueRequirement;

                    // Create the Document Reference JSON object.
                    valueObject.add("type", "ATTACHMENT");
                    addValueRequirementAttributes(valueObject, attachmentListRequirement);
                    return valueObject;
                }
                default:
                {
                    LOGGER.log(T8Logger.Level.WARNING, "DR Data Type [" + requirementType + "] not serialized.");
                    return null;
                }
            }
        }
        else return null;
    }

    public static JsonArray serializeDataDependencies(ValueRequirement valueRequirement)
    {
        JsonArray dependencyArray;

        dependencyArray = new JsonArray();
        for (DataDependency dependency : valueRequirement.getDataDependencies())
        {
            T8OntologyConceptType conceptType;
            JsonObject dependencyObject;

            conceptType = dependency.getConceptType();

            dependencyObject = new JsonObject();
            dependencyObject.add("type", dependency.getType().toString());
            dependencyObject.addIfNotNull("relationId", dependency.getRelationId());
            dependencyObject.addIfNotNull("drId", dependency.getDrId());
            dependencyObject.addIfNotNull("drInstanceId", dependency.getDrInstanceId());
            dependencyObject.addIfNotNull("propertyId", dependency.getPropertyId());
            dependencyObject.addIfNotNull("fieldId", dependency.getFieldId());
            if (conceptType != null) dependencyObject.add("conceptTypeId", conceptType.getConceptTypeID());

            dependencyArray.add(dependencyObject);
        }

        return dependencyArray;
    }

    public static List<DataDependency> deserializeDataDependencies(JsonArray dependencyArray)
    {
        List<DataDependency> dependencyList;

        dependencyList = new ArrayList<>();
        if (dependencyArray == null)
        {
            return dependencyList;
        }
        else
        {
            for (int index = 0; index < dependencyArray.size(); index++)
            {
                JsonObject dependencyObject;
                String drInstanceId;
                String drId;
                String propertyId;
                String fieldId;
                String relationId;
                String conceptTypeId;
                T8OntologyConceptType conceptType;
                DataDependencyType type;

                dependencyObject = dependencyArray.getJsonObject(index);
                type = DataDependencyType.valueOf(dependencyObject.getString("type", DataDependencyType.DR_INSTANCE.toString()));
                drInstanceId = dependencyObject.getString("drInstanceId");
                drId = dependencyObject.getString("drId");
                propertyId = dependencyObject.getString("propertyId");
                fieldId = dependencyObject.getString("fieldId");
                relationId = dependencyObject.getString("relationId");
                conceptTypeId = dependencyObject.getString("conceptTypeId");
                conceptType = conceptTypeId != null ? T8OntologyConceptType.getConceptType(conceptTypeId) : null;

                dependencyList.add(new DataDependency(type, drInstanceId, drId, propertyId, fieldId, relationId, conceptType));
            }

            return dependencyList;
        }
    }

    public static DataRecord deserializeDataRecord(String jsonString) throws Exception
    {
        if (jsonString != null) return deserializeDataRecord(JsonObject.readFrom(jsonString));
        else return null;
    }

    public static DataRecord deserializeDataRecord(JsonObject object) throws Exception
    {
        DataRequirementInstance drInstance;
        T8OntologyConcept ontology;
        DataRecord dataRecord;
        JsonArray sectionArray;
        JsonArray subRecordArray;
        String recordId;

        // Get the content from the object.
        recordId = object.getString("recordId");
        drInstance = deserializeDrInstance(object.getJsonObject("drInstance"));

        // Create the new Data Record.
        dataRecord = new DataRecord(drInstance);
        dataRecord.setID(recordId);
        dataRecord.setFFT(object.getString("fft"));

        // Add all the sections to the Data Record.
        sectionArray = object.getJsonArray("sections");
        if (sectionArray != null)
        {
            for (int sectionIndex = 0; sectionIndex < sectionArray.size(); sectionIndex++)
            {
                RecordSection section;

                section = deserializeRecordSection(drInstance.getDataRequirement(), sectionArray.getJsonObject(sectionIndex));
                dataRecord.setRecordSection(section);
            }
        }

        // Add all the sub-records to the Data Record.
        subRecordArray = object.getJsonArray("subRecords");
        if (subRecordArray != null)
        {
            for (int subRecordIndex = 0; subRecordIndex < subRecordArray.size(); subRecordIndex++)
            {
                DataRecord subRecord;

                subRecord = deserializeDataRecord(subRecordArray.getJsonObject(subRecordIndex));
                dataRecord.addSubRecord(subRecord);
            }
        }

        // Extract attributes.
        dataRecord.setAttributes(extractRecordAttributes(object));

        // Extract the ontology if present.
        ontology = new T8DtOntologyConcept(null).deserialize(object.getJsonObject("ontology"));
        if (ontology != null) dataRecord.setOntology(ontology);

        // Return the complete Data Record.
        return dataRecord;
    }

    public static RecordSection deserializeRecordSection(DataRequirement dr, JsonObject object) throws Exception
    {
        RecordSection section;
        JsonArray propertyArray;
        String sectionId;

        // Get the content from the object.
        sectionId = object.getString("sectionId");

        // Create the new Section.
        section = new RecordSection(dr.getSectionRequirement(sectionId));

        // Add all the properties to the section.
        propertyArray = object.getJsonArray("properties");
        if (propertyArray != null)
        {
            for (int propertyIndex = 0; propertyIndex < propertyArray.size(); propertyIndex++)
            {
                RecordProperty property;

                property = deserializeRecordProperty(dr, propertyArray.getJsonObject(propertyIndex));
                section.addRecordProperty(property);
            }
        }

        // Return the complete section.
        return section;
    }

    public static RecordProperty deserializeRecordProperty(DataRequirement dr, JsonObject object) throws Exception
    {
        String propertyId;

        // Get the content from the object.
        propertyId = object.getString("propertyId");
        if (!Strings.isNullOrEmpty(propertyId))
        {
            PropertyRequirement propertyRequirement;
            RecordProperty property;
            JsonObject valueObject;

            // Create the new Property.
            propertyRequirement = dr.getPropertyRequirement(propertyId);
            property = new RecordProperty(propertyRequirement);

            // Add the value to the property.
            valueObject = object.getJsonObject("value");
            if (valueObject != null)
            {
                property.setRecordValue(deserializeRecordValue(dr, propertyRequirement.getValueRequirement(), valueObject));
            }

            // Add FFT.
            property.setFFT(object.getString("fft"));

            // Return the complete property.
            return property;
        }
        else throw new Exception("Property ID missing from JSON object: " + object);
    }

    public static RecordValue deserializeRecordValue(DataRequirement dr, ValueRequirement valueRequirement, JsonObject object) throws Exception
    {
        if (object != null)
        {
            String typeID;

            typeID = object.getString("type");
            switch (typeID)
            {
                case "STRING":
                {
                    StringValue stringValue;

                    stringValue = new StringValue(valueRequirement);
                    stringValue.setString(object.getString("string"));
                    return stringValue;
                }
                case "BOOLEAN":
                {
                    BooleanValue booleanValue;

                    booleanValue = new BooleanValue(valueRequirement);
                    booleanValue.setBooleanValue(object.getBoolean("value"));
                    return booleanValue;
                }
                case "TEXT":
                {
                    TextValue textValue;

                    textValue = new TextValue(valueRequirement);
                    textValue.setText(object.getString("text"));
                    return textValue;
                }
                case "INTEGER":
                {
                    IntegerValue integerValue;

                    integerValue = new IntegerValue(valueRequirement);
                    integerValue.setValue(object.getInteger("value"));
                    return integerValue;
                }
                case "REAL":
                {
                    RealValue realValue;

                    realValue = new RealValue(valueRequirement);
                    realValue.setValue(object.getString("value"));
                    return realValue;
                }
                case "DATE":
                {
                    DateValue dateValue;

                    dateValue = new DateValue(valueRequirement);
                    dateValue.setDate(object.getLong("millis"));
                    return dateValue;
                }
                case "TIME":
                {
                    TimeValue timeValue;

                    timeValue = new TimeValue(valueRequirement);
                    timeValue.setTime(object.getLong("millis"));
                    return timeValue;
                }
                case "DATE_TIME":
                {
                    DateTimeValue dateTime;

                    dateTime = new DateTimeValue(valueRequirement);
                    dateTime.setDateTime(object.getLong("millis"));
                    return dateTime;
                }
                case "CONTROLLED_CONCEPT":
                {
                    ControlledConcept controlledConcept;
                    String conceptId;
                    String term;

                    conceptId = object.getString("conceptId");
                    term = object.getString("term");

                    // Construct the controlled concept.
                    controlledConcept = new ControlledConcept(valueRequirement);
                    controlledConcept.setConceptId(conceptId, false);
                    controlledConcept.setTerm(term);
                    return controlledConcept;
                }
                case "UNIT_OF_MEASURE":
                {
                    UnitOfMeasure uom;
                    String conceptId;

                    conceptId = object.getString("conceptId");

                    // Construct the controlled concept.
                    uom = new UnitOfMeasure(valueRequirement);
                    uom.setConceptId(conceptId, false);
                    uom.setTerm(object.getString("term"));
                    uom.setDefinition(object.getString("definition"));
                    uom.setCode(object.getString("code"));
                    uom.setAbbreviation(object.getString("abbreviation"));
                    return uom;
                }
                case "COMPOSITE":
                {
                    Composite composite;
                    JsonArray fieldArray;

                    // Create the new composite value.
                    composite = new Composite(valueRequirement);

                    // Create the field array.
                    fieldArray = object.getJsonArray("fields");
                    if (fieldArray != null)
                    {
                        for (int fieldIndex = 0; fieldIndex < fieldArray.size(); fieldIndex++)
                        {
                            ValueRequirement fieldValueRequirement;
                            ValueRequirement fieldRequirement;
                            JsonObject fieldValueObject;
                            JsonObject fieldObject;
                            String fieldId;
                            Field field;

                            // Create the new field.
                            fieldObject = fieldArray.getJsonObject(fieldIndex);
                            fieldId = fieldObject.getString("fieldId");
                            fieldRequirement = valueRequirement.getSubRequirement(RequirementType.FIELD_TYPE, fieldId, null);
                            field = new Field(fieldRequirement);
                            fieldValueRequirement = fieldRequirement.getFirstSubRequirement();

                            // Add the field value if any was found.
                            fieldValueObject = fieldObject.getJsonObject("value");
                            if (fieldValueObject != null)
                            {
                                field.setSubValue(deserializeRecordValue(dr, fieldValueRequirement, fieldValueObject));
                            }

                            // Add the field to the composite value.
                            composite.setField(field);
                        }
                    }

                    // Return the completed value.
                    return composite;
                }
                case "MEASURED_NUMBER":
                {
                    MeasuredNumber measuredNumber;

                    measuredNumber = new MeasuredNumber(valueRequirement);
                    measuredNumber.setNumber(object.getString("number"));
                    measuredNumber.setUomId(object.getString("unitOfMeasureId"));
                    measuredNumber.setQomId(object.getString("qualifierOfMeasureId"));
                    return measuredNumber;
                }
                case "MEASURED_RANGE":
                {
                    MeasuredRange measuredRange;

                    measuredRange = new MeasuredRange(valueRequirement);
                    measuredRange.setLowerBoundNumber(object.getString("lowerBoundNumber"));
                    measuredRange.setUpperBoundNumber(object.getString("upperBoundNumber"));
                    measuredRange.setUomId(object.getString("unitOfMeasureId"));
                    measuredRange.setQomId(object.getString("qualifierOfMeasureId"));
                    return measuredRange;
                }
                case "DOCUMENT_REFERENCE":
                {
                    DocumentReferenceList documentReferenceList;
                    JsonArray referenceArray;

                    // Create the document reference list.
                    documentReferenceList = new DocumentReferenceList(valueRequirement);

                    // Create the reference values.
                    referenceArray = object.getJsonArray("references");
                    if (referenceArray != null)
                    {
                        for (int referenceIndex = 0; referenceIndex < referenceArray.size(); referenceIndex++)
                        {
                            JsonObject referenceObject;
                            String conceptId;
                            String recordId;

                            // Get the details of the reference type.
                            referenceObject = referenceArray.getJsonObject(referenceIndex);
                            recordId = referenceObject.getString("recordId");
                            conceptId = referenceObject.getString("conceptId");
                            documentReferenceList.addReference(conceptId != null ? conceptId : recordId);
                        }
                    }

                    // Return the completed value.
                    return documentReferenceList;
                }
                case "ATTACHMENT":
                {
                    AttachmentList attachmentList;
                    JsonArray attachmentArray;

                    // Create the attachment list.
                    attachmentList = new AttachmentList(valueRequirement);

                    // Create the attachment type array.
                    attachmentArray = object.getJsonArray("attachments");
                    if (attachmentArray != null)
                    {
                        for (int attachmentIndex = 0; attachmentIndex < attachmentArray.size(); attachmentIndex++)
                        {
                            JsonObject attachmentObject;
                            String attachmentId;
                            String fileContextId;
                            String fileContextIid;
                            String originalFileName;
                            String filePath;
                            Long fileSize;
                            String md5;
                            String mediaType;

                            // Get the details of the reference type.
                            attachmentObject = attachmentArray.getJsonObject(attachmentIndex);
                            attachmentId = attachmentObject.getString("attachmentId");
                            fileContextId = attachmentObject.getString("fileContextId");
                            fileContextIid = attachmentObject.getString("fileContextIid");
                            originalFileName = attachmentObject.getString("originalFileName");
                            filePath = attachmentObject.getString("filePath");
                            fileSize = attachmentObject.getLong("fileSize");
                            md5 = attachmentObject.getString("md5");
                            mediaType = attachmentObject.getString("mediaType");

                            // Add the attachment reference.
                            attachmentList.addAttachment(attachmentId);

                            // Add attachment details if an id was found (the minimum amount of data required).
                            if (!Strings.isNullOrEmpty(attachmentId))
                            {
                                // Set default values for those not found.
                                if (fileSize == null) fileSize = -1l;

                                // Add the attachment details to the deserialized list.
                                attachmentList.addAttachmentDetails(attachmentId, new T8AttachmentDetails(attachmentId, fileContextIid, fileContextId, filePath, originalFileName, fileSize, md5, mediaType));
                            }
                        }
                    }

                    // Create the completed value.
                    return attachmentList;
                }
                default:
                {
                    LOGGER.log(T8Logger.Level.WARNING, "Type ID ["+typeID+"] has not been deserialized");
                    return null;
                }
            }
        }
        else return null;
    }

    public static ValueContent deserializeValueContent(JsonObject object)
    {
        if (object != null)
        {
            String typeID;

            typeID = object.getString("type");
            switch (typeID)
            {
                case "STRING":
                {
                    StringValueContent stringValue;

                    stringValue = new StringValueContent();
                    stringValue.setString(object.getString("string"));
                    return stringValue;
                }
                case "NUMBER":
                {
                    NumberContent numberContent;

                    numberContent = new NumberContent();
                    numberContent.setValue(object.getString("value"));
                    return numberContent;
                }
                case "LOWER_BOUND_NUMBER":
                {
                    LowerBoundNumberContent numberContent;

                    numberContent = new LowerBoundNumberContent();
                    numberContent.setValue(object.getString("value"));
                    return numberContent;
                }
                case "UPPER_BOUND_NUMBER":
                {
                    UpperBoundNumberContent numberContent;

                    numberContent = new UpperBoundNumberContent();
                    numberContent.setValue(object.getString("value"));
                    return numberContent;
                }
                case "BOOLEAN":
                {
                    BooleanValueContent booleanValue;

                    booleanValue = new BooleanValueContent();
                    booleanValue.setValue(object.getString("value"));
                    return booleanValue;
                }
                case "TEXT":
                {
                    TextValueContent textValue;

                    textValue = new TextValueContent();
                    textValue.setText(object.getString("text"));
                    return textValue;
                }
                case "INTEGER":
                {
                    IntegerValueContent integerValue;

                    integerValue = new IntegerValueContent();
                    integerValue.setValue(object.getString("value"));
                    return integerValue;
                }
                case "REAL":
                {
                    RealValueContent realValue;

                    realValue = new RealValueContent();
                    realValue.setValue(object.getString("value"));
                    return realValue;
                }
                case "DATE":
                {
                    DateValueContent dateValue;

                    dateValue = new DateValueContent();
                    dateValue.setDate(object.getString("date"));
                    return dateValue;
                }
                case "TIME":
                {
                    TimeValueContent timeValue;

                    timeValue = new TimeValueContent();
                    timeValue.setTime(object.getString("time"));
                    return timeValue;
                }
                case "DATE_TIME":
                {
                    DateTimeValueContent dateTime;

                    dateTime = new DateTimeValueContent();
                    dateTime.setDateTime(object.getString("dateTime"));
                    return dateTime;
                }
                case "CONTROLLED_CONCEPT":
                {
                    ControlledConceptContent controlledConcept;
                    String conceptId;

                    conceptId = object.getString("conceptId");

                    // Construct the controlled concept.
                    controlledConcept = new ControlledConceptContent();
                    controlledConcept.setConceptId(conceptId);
                    controlledConcept.setIrdi(object.getString("irdi"));
                    controlledConcept.setCodeId(object.getString("codeId"));
                    controlledConcept.setCode(object.getString("code"));
                    controlledConcept.setTermId(object.getString("termId"));
                    controlledConcept.setTerm(object.getString("term"));
                    controlledConcept.setDefinitionId(object.getString("definitionId"));
                    controlledConcept.setDefinition(object.getString("definition"));
                    controlledConcept.setAbbreviationId(object.getString("abbreviationId"));
                    controlledConcept.setAbbreviation(object.getString("abbreviation"));
                    return controlledConcept;
                }
                case "UNIT_OF_MEASURE":
                {
                    UnitOfMeasureContent uomContent;
                    String conceptId;

                    conceptId = object.getString("conceptId");

                    // Construct the controlled concept.
                    uomContent = new UnitOfMeasureContent();
                    uomContent.setConceptId(conceptId);
                    uomContent.setIrdi(object.getString("irdi"));
                    uomContent.setCodeId(object.getString("codeId"));
                    uomContent.setCode(object.getString("code"));
                    uomContent.setTermId(object.getString("termId"));
                    uomContent.setTerm(object.getString("term"));
                    uomContent.setDefinitionId(object.getString("definitionId"));
                    uomContent.setDefinition(object.getString("definition"));
                    uomContent.setAbbreviationId(object.getString("abbreviationId"));
                    uomContent.setAbbreviation(object.getString("abbreviation"));
                    return uomContent;
                }
                case "QUALIFIER_OF_MEASURE":
                {
                    QualifierOfMeasureContent qomContent;
                    String conceptId;

                    conceptId = object.getString("conceptId");

                    // Construct the controlled concept.
                    qomContent = new QualifierOfMeasureContent();
                    qomContent.setConceptId(conceptId);
                    qomContent.setIrdi(object.getString("irdi"));
                    qomContent.setCodeId(object.getString("codeId"));
                    qomContent.setCode(object.getString("code"));
                    qomContent.setTermId(object.getString("termId"));
                    qomContent.setTerm(object.getString("term"));
                    qomContent.setDefinitionId(object.getString("definitionId"));
                    qomContent.setDefinition(object.getString("definition"));
                    qomContent.setAbbreviationId(object.getString("abbreviationId"));
                    qomContent.setAbbreviation(object.getString("abbreviation"));
                    return qomContent;
                }
                case "COMPOSITE":
                {
                    CompositeContent composite;
                    JsonArray fieldArray;

                    // Create the new composite value.
                    composite = new CompositeContent();

                    // Create the field array.
                    fieldArray = object.getJsonArray("fields");
                    if (fieldArray != null)
                    {
                        for (int fieldIndex = 0; fieldIndex < fieldArray.size(); fieldIndex++)
                        {
                            JsonObject fieldValueObject;
                            JsonObject fieldObject;
                            FieldContent field;
                            String fieldId;

                            // Create the new field.
                            fieldObject = fieldArray.getJsonObject(fieldIndex);
                            fieldId = fieldObject.getString("fieldId");
                            field = new FieldContent(fieldId);

                            // Add the field value if any was found.
                            fieldValueObject = fieldObject.getJsonObject("value");
                            if (fieldValueObject != null)
                            {
                                field.setValue(deserializeValueContent(fieldValueObject));
                            }

                            // Add the field to the composite value.
                            composite.addField(field);
                        }
                    }

                    // Return the completed value.
                    return composite;
                }
                case "MEASURED_NUMBER":
                {
                    MeasuredNumberContent measuredNumber;

                    measuredNumber = new MeasuredNumberContent();
                    measuredNumber.setNumber((NumberContent)deserializeValueContent(object.getJsonObject("number")));
                    measuredNumber.setUnitOfMeasure((UnitOfMeasureContent)deserializeValueContent(object.getJsonObject("unitOfMeasure")));
                    measuredNumber.setQualifierOfMeasure((QualifierOfMeasureContent)deserializeValueContent(object.getJsonObject("qualifierOfMeasure")));
                    return measuredNumber;
                }
                case "MEASURED_RANGE":
                {
                    MeasuredRangeContent measuredRange;

                    measuredRange = new MeasuredRangeContent();
                    measuredRange.setLowerBoundNumber((LowerBoundNumberContent)deserializeValueContent(object.getJsonObject("lowerBoundNumber")));
                    measuredRange.setUpperBoundNumber((UpperBoundNumberContent)deserializeValueContent(object.getJsonObject("upperBoundNumber")));
                    measuredRange.setUnitOfMeasure((UnitOfMeasureContent)deserializeValueContent(object.getJsonObject("unitOfMeasure")));
                    measuredRange.setQualifierOfMeasure((QualifierOfMeasureContent)deserializeValueContent(object.getJsonObject("qualifierOfMeasure")));
                    return measuredRange;
                }
                case "DOCUMENT_REFERENCE":
                {
                    DocumentReferenceListContent documentReferenceList;
                    JsonArray referenceArray;

                    // Create the document reference list.
                    documentReferenceList = new DocumentReferenceListContent();

                    // Create the reference values.
                    referenceArray = object.getJsonArray("references");
                    if (referenceArray != null)
                    {
                        for (int referenceIndex = 0; referenceIndex < referenceArray.size(); referenceIndex++)
                        {
                            DocumentReferenceContent newReference;
                            DocumentReferenceType referenceType;
                            JsonObject referenceObject;
                            String conceptId;

                            // Get the details of the reference type.
                            referenceObject = referenceArray.getJsonObject(referenceIndex);
                            conceptId = referenceObject.getString("conceptId");
                            referenceType = DocumentReferenceType.valueOf(referenceObject.getString("referenceType"));
                            newReference = new DocumentReferenceContent(referenceType, conceptId);
                            newReference.setCode(referenceObject.getString("code"));
                            newReference.setCode(referenceObject.getString("term"));
                            newReference.setCode(referenceObject.getString("definition"));
                            documentReferenceList.addReference(newReference);
                        }
                    }

                    // Return the completed value.
                    return documentReferenceList;
                }
                case "ATTACHMENT":
                {
                    AttachmentListContent attachmentList;
                    JsonArray attachmentArray;

                    // Create the attachment list.
                    attachmentList = new AttachmentListContent();

                    // Create the attachment type array.
                    attachmentArray = object.getJsonArray("attachments");
                    if (attachmentArray != null)
                    {
                        for (int attachmentIndex = 0; attachmentIndex < attachmentArray.size(); attachmentIndex++)
                        {
                            JsonObject attachmentObject;
                            String attachmentId;
                            String fileContextId;
                            String fileContextIid;
                            String originalFileName;
                            String filePath;
                            Long fileSize;
                            String md5;
                            String mediaType;

                            // Get the details of the reference type.
                            attachmentObject = attachmentArray.getJsonObject(attachmentIndex);
                            attachmentId = attachmentObject.getString("attachmentId");
                            fileContextId = attachmentObject.getString("fileContextId");
                            fileContextIid = attachmentObject.getString("fileContextIid");
                            filePath = attachmentObject.getString("filePath");
                            originalFileName = attachmentObject.getString("originalFileName");
                            fileSize = attachmentObject.getLong("fileSize");
                            md5 = attachmentObject.getString("md5");
                            mediaType = attachmentObject.getString("mediaType");

                            // Add the attachment reference.
                            attachmentList.addAttachment(attachmentId);

                            // Add attachment details if an id was found (the minimum amount of data required).
                            if (!Strings.isNullOrEmpty(attachmentId))
                            {
                                // Set default values for those not found.
                                if (fileSize == null) fileSize = -1l;

                                // Add the attachment details to the deserialized list.
                                attachmentList.addAttachmentDetails(attachmentId, new T8AttachmentDetails(attachmentId, fileContextIid, fileContextId, filePath, originalFileName, fileSize, md5, mediaType));
                            }
                        }
                    }

                    // Create the completed value.
                    return attachmentList;
                }
                default:
                {
                    LOGGER.log(T8Logger.Level.WARNING, "Type ID ["+typeID+"] has not been deserialized");
                    return null;
                }
            }
        }
        else return null;
    }

    public static DataRequirementInstance deserializeDrInstance(String jsonString) throws Exception
    {
        JsonObject drInstanceObject;

        // Read the JSON object from the input string.
        drInstanceObject = JsonObject.readFrom(jsonString);

        // Construct the Data Requirement Instance from the JSON object.
        return deserializeDrInstance(drInstanceObject);
    }

    public static DataRequirementInstance deserializeDrInstance(JsonObject object) throws Exception
    {
        DataRequirementInstance drInstance;
        DataRequirement dataRequirement;
        JsonArray sectionArray;
        Boolean independent;
        String drInstanceId;
        String drId;
        String classId;

        // Get the content from the object.
        drInstanceId = object.getString("drInstanceId");
        drId = object.getString("drId");
        classId = object.getString("classId");
        independent = object.getBoolean("independent");

        // Create the new Data Requirement Instance.
        dataRequirement = new DataRequirement(drId);
        dataRequirement.setClassConceptID(classId);
        drInstance = new DataRequirementInstance(drInstanceId, dataRequirement);
        drInstance.setIndependent((independent != null) && independent);

        // Add all the sections to the Data Requirement.
        sectionArray = object.getJsonArray("sections");
        if (sectionArray != null)
        {
            for (int sectionIndex = 0; sectionIndex < sectionArray.size(); sectionIndex++)
            {
                SectionRequirement sectionRequirement;

                sectionRequirement = deserializeSectionRequirement(sectionArray.getJsonObject(sectionIndex));
                dataRequirement.addSectionRequirement(sectionRequirement);
            }
        }

        // Return the complete Data Requirement.
        return drInstance;
    }

    public static DataRequirement deserializeDataRequirement(String jsonString) throws Exception
    {
        if (!Strings.isNullOrEmpty(jsonString))
        {
            JsonObject dataRequirementObject;

            // Read the JSON object from the input string.
            dataRequirementObject = JsonObject.readFrom(jsonString);

            // Construct the Data Requirement from the JSON object.
            return deserializeDataRequirement(dataRequirementObject);
        }
        else return null;
    }

    public static DataRequirement deserializeDataRequirement(JsonObject object) throws Exception
    {
        DataRequirement dataRequirement;
        JsonArray sectionArray;
        String drId;
        String classId;

        // Get the content from the object.
        drId = object.getString("drId");
        classId = object.getString("classId");

        // Create the new Data Requirement.
        dataRequirement = new DataRequirement(drId);
        dataRequirement.setClassConceptID(classId);

        // Add all the sections to the Data Requirement.
        sectionArray = object.getJsonArray("sections");
        if (sectionArray != null)
        {
            for (int sectionIndex = 0; sectionIndex < sectionArray.size(); sectionIndex++)
            {
                SectionRequirement sectionRequirement;

                sectionRequirement = deserializeSectionRequirement(sectionArray.getJsonObject(sectionIndex));
                dataRequirement.addSectionRequirement(sectionRequirement);
            }
        }

        // Extract attributes.
        dataRequirement.setAttributes(extractDrAttributes(object));

        // Return the complete Data Requirement.
        return dataRequirement;
    }

    public static SectionRequirement deserializeSectionRequirement(JsonObject object) throws Exception
    {
        SectionRequirement sectionRequirement;
        JsonArray propertyArray;
        String sectionId;

        // Get the content from the object.
        sectionId = object.getString("sectionId");

        // Create the new Section Requirement.
        sectionRequirement = new SectionRequirement(sectionId);

        // Add all the properties to the section.
        propertyArray = object.getJsonArray("properties");
        if (propertyArray != null)
        {
            for (int propertyIndex = 0; propertyIndex < propertyArray.size(); propertyIndex++)
            {
                PropertyRequirement propertyRequirement;

                propertyRequirement = deserializePropertyRequirement(propertyArray.getJsonObject(propertyIndex));
                sectionRequirement.addPropertyRequirement(propertyRequirement);
            }
        }

        // Return the complete section.
        return sectionRequirement;
    }

    public static PropertyRequirement deserializePropertyRequirement(JsonObject object) throws Exception
    {
        PropertyRequirement propertyRequirement;
        JsonObject valueObject;
        String propertyId;
        String propertyTerm;
        String propertyDefinition;
        boolean isCharactaristic;

        // Get the content from the object.
        propertyId = object.getString("propertyId");
        isCharactaristic = object.getBoolean("characteristic", false);
        propertyTerm = object.getString("term");
        propertyDefinition = object.getString("definition");

        // Create the new Property Requirement.
        propertyRequirement = new PropertyRequirement(propertyId);

        // Add value requirement to the property.
        valueObject = object.getJsonObject("value");
        if (valueObject != null)
        {
            propertyRequirement.setValueRequirement(deserializeValueRequirement(valueObject));
        }

        // Deserialize remaining data.
        propertyRequirement.setAttributes(extractPropertyRequirementAttributes(object));
        propertyRequirement.setCharacteristic(isCharactaristic);
        propertyRequirement.setTerm(propertyTerm);
        propertyRequirement.setDefinition(propertyDefinition);

        // Return the complete property.
        return propertyRequirement;
    }

    public static ValueRequirement deserializeValueRequirement(JsonObject object) throws Exception
    {
        if (object != null)
        {
            String typeID;

            typeID = object.getString("type");
            switch (typeID)
            {
                case "STRING":
                {
                    StringRequirement stringRequirement;

                    stringRequirement = new StringRequirement();
                    stringRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return stringRequirement;
                }
                case "BOOLEAN":
                {
                    BooleanRequirement booleanRequirement;

                    booleanRequirement = new BooleanRequirement();
                    booleanRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return booleanRequirement;
                }
                case "TEXT":
                {
                    TextRequirement textRequirement;

                    textRequirement = new TextRequirement();
                    textRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return textRequirement;
                }
                case "INTEGER":
                {
                    IntegerRequirement integerRequirement;

                    integerRequirement = new IntegerRequirement();
                    integerRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return integerRequirement;
                }
                case "REAL":
                {
                    RealRequirement realRequirement;

                    realRequirement = new RealRequirement();
                    realRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return realRequirement;
                }
                case "DATE":
                {
                    DateRequirement dateRequirement;

                    dateRequirement = new DateRequirement();
                    dateRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return dateRequirement;
                }
                case "TIME":
                {
                    TimeRequirement timeRequirement;

                    timeRequirement = new TimeRequirement();
                    timeRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return timeRequirement;
                }
                case "DATE_TIME":
                {
                    DateTimeRequirement dateTimeRequirement;

                    dateTimeRequirement = new DateTimeRequirement();
                    dateTimeRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return dateTimeRequirement;
                }
                case "CONTROLLED_CONCEPT":
                {
                    ControlledConceptRequirement controlledConceptRequirement;

                    // Create the new value requirement.
                    controlledConceptRequirement = new ControlledConceptRequirement();
                    controlledConceptRequirement.setOntologyClassID(object.getString("ontologyClassId"));

                    // Add dependencies.
                    for (DataDependency dependency : deserializeDataDependencies(object.getJsonArray("dependencies")))
                    {
                        controlledConceptRequirement.addSubRequirement(dependency.createDependencyRequirement());
                    }

                    // Return the completed value requirement.
                    controlledConceptRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return controlledConceptRequirement;
                }
                case "UNIT_OF_MEASURE":
                {
                    UnitOfMeasureRequirement uomRequirement;

                    // Create the new value requirement.
                    uomRequirement = new UnitOfMeasureRequirement();
                    uomRequirement.setOntologyClassId(object.getString("ontologyClassId"));

                    // Return the completed value requirement.
                    uomRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return uomRequirement;
                }
                case "QUALIFIER_OF_MEASURE":
                {
                    QualifierOfMeasureRequirement qomRequirement;

                    // Create the new value requirement.
                    qomRequirement = new QualifierOfMeasureRequirement();
                    qomRequirement.setOntologyClassId(object.getString("ontologyClassId"));

                    // Return the completed value requirement.
                    qomRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return qomRequirement;
                }
                case "COMPOSITE":
                {
                    CompositeRequirement compositeRequirement;
                    JsonArray fieldArray;

                    // Create the composite requirement.
                    compositeRequirement = new CompositeRequirement();

                    // Create the field array.
                    fieldArray = object.getJsonArray("fields");
                    if (fieldArray != null)
                    {
                        for (int fieldIndex = 0; fieldIndex < fieldArray.size(); fieldIndex++)
                        {
                            JsonObject fieldObject;
                            JsonObject fieldValueRequirementObject;
                            FieldRequirement fieldRequirement;
                            String fieldId;

                            // Get the field details from the object.
                            fieldObject = fieldArray.getJsonObject(fieldIndex);
                            fieldId = fieldObject.getString("fieldId");
                            fieldValueRequirementObject = fieldObject.getJsonObject("value");

                            // Create the new field.
                            fieldRequirement = new FieldRequirement(fieldId);

                            // Add the field value requirement if any was found.
                            if (fieldValueRequirementObject != null)
                            {
                                fieldRequirement.addSubRequirement(deserializeValueRequirement(fieldValueRequirementObject));
                            }

                            // Add the field to the composite.
                            compositeRequirement.addSubRequirement(fieldRequirement);
                        }
                    }

                    // Return the completed value requirement.
                    compositeRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return compositeRequirement;
                }
                case "DOCUMENT_REFERENCE":
                {
                    DocumentReferenceRequirement referenceRequirement;

                    // Create the document reference requirement.
                    referenceRequirement = new DocumentReferenceRequirement();
                    referenceRequirement.setOntologyClassId(object.getString("ontologyClassId"));

                    // Add dependencies.
                    for (DataDependency dependency : deserializeDataDependencies(object.getJsonArray("dependencies")))
                    {
                        referenceRequirement.addSubRequirement(dependency.createDependencyRequirement());
                    }

                    // Return the completed requirement.
                    referenceRequirement.setAttributes(extractValueRequirementAttributes(object));
                    return referenceRequirement;
                }
                case "ATTACHMENT":
                {
                    AttachmentListRequirement attachmentListRequirement;

                    // Create the attachment requirement.
                    attachmentListRequirement = new AttachmentListRequirement();
                    attachmentListRequirement.setAttributes(extractValueRequirementAttributes(object));

                    // Create the completed requirement.
                    return attachmentListRequirement;
                }
                case "MEASURED_NUMBER":
                {
                    MeasuredNumberRequirement measuredNumberRequirement;
                    NumberRequirement numberRequirement;
                    JsonObject numberObject;
                    JsonObject uomObject;
                    JsonObject qomObject;

                    // Create the measured number requirement.
                    measuredNumberRequirement = new MeasuredNumberRequirement();

                    // Deserialize the numeric requirement.
                    numberObject = object.getJsonObject("number");
                    numberRequirement = new NumberRequirement();
                    numberRequirement.setAttributes(extractValueRequirementAttributes(numberObject));
                    measuredNumberRequirement.addSubRequirement(numberRequirement);

                    // Deserialize the uom requirement.
                    uomObject = object.getJsonObject("unitOfMeasure");
                    if (uomObject != null) measuredNumberRequirement.addSubRequirement(new UnitOfMeasureRequirement(uomObject.getString("ontologyClassId")));

                    // Deserialize the qom requirement.
                    qomObject = object.getJsonObject("qualifierOfMeasure");
                    if (qomObject != null) measuredNumberRequirement.addSubRequirement(new QualifierOfMeasureRequirement(qomObject.getString("ontologyClassId")));

                    // Return the final requiremet.
                    return measuredNumberRequirement;
                }
                case "MEASURED_RANGE":
                {
                    MeasuredRangeRequirement measuredRangeRequirement;
                    LowerBoundNumberRequirement lowerBoundRequirement;
                    UpperBoundNumberRequirement upperBoundRequirement;
                    JsonObject numberObject;
                    JsonObject uomObject;
                    JsonObject qomObject;

                    // Create the measured range requirement.
                    measuredRangeRequirement = new MeasuredRangeRequirement();

                    // Deserialize the lower bound numeric requirement.
                    numberObject = object.getJsonObject("lowerBoundNumber");
                    lowerBoundRequirement = new LowerBoundNumberRequirement();
                    lowerBoundRequirement.setAttributes(extractValueRequirementAttributes(numberObject));
                    measuredRangeRequirement.addSubRequirement(lowerBoundRequirement);

                    // Deserialize the upper bound numeric requirement.
                    numberObject = object.getJsonObject("upperBoundNumber");
                    upperBoundRequirement = new UpperBoundNumberRequirement();
                    upperBoundRequirement.setAttributes(extractValueRequirementAttributes(numberObject));
                    measuredRangeRequirement.addSubRequirement(upperBoundRequirement);

                    // Deserialize the uom requirement.
                    uomObject = object.getJsonObject("unitOfMeasure");
                    if (uomObject != null) measuredRangeRequirement.addSubRequirement(new UnitOfMeasureRequirement(uomObject.getString("ontologyClassId")));

                    // Deserialize the qom requirement.
                    qomObject = object.getJsonObject("qualifierOfMeasure");
                    if (qomObject != null) measuredRangeRequirement.addSubRequirement(new QualifierOfMeasureRequirement(qomObject.getString("ontologyClassId")));

                    // Return the final requirement.
                    return measuredRangeRequirement;
                }
                default:
                {
                    return null;
                }
            }
        }
        else return null;
    }

    private static void addRecordAttributes(JsonObject drObject, DataRecord dataRecord)
    {
        Map<String, Object> attributes;

        attributes = dataRecord.getAttributes();
        for (String attributeId : attributes.keySet())
        {
            drObject.addIfNotNull(T8IdentifierUtilities.toCamelCase(attributeId), attributes.get(attributeId));
        }
    }

    private static Map<String, Object> extractRecordAttributes(JsonObject drObject)
    {
        Map<String, Object> attributes;

        attributes = new HashMap<>();
        for (String name : drObject.names())
        {
            if (!RECORD_STANDARD_NAMES.contains(name))
            {
                Object value;

                value = drObject.getObject(name);
                attributes.put(T8IdentifierUtilities.toIdCase(name), value);
            }
        }

        return attributes;
    }

    private static void addDrAttributes(JsonObject drObject, DataRequirement dataRequirement)
    {
        Map<String, Object> attributes;

        attributes = dataRequirement.getAttributes();
        for (String attributeId : attributes.keySet())
        {
            drObject.addIfNotNull(T8IdentifierUtilities.toCamelCase(attributeId), attributes.get(attributeId));
        }
    }

    private static Map<String, Object> extractDrAttributes(JsonObject drObject)
    {
        Map<String, Object> attributes;

        attributes = new HashMap<>();
        for (String name : drObject.names())
        {
            if (!DR_INSTANCE_STANDARD_NAMES.contains(name))
            {
                Object value;

                value = drObject.getObject(name);
                attributes.put(T8IdentifierUtilities.toIdCase(name), value);
            }
        }

        return attributes;
    }

    private static void addPropertyRequirementAttributes(JsonObject propertyObject, PropertyRequirement propertyRequirement)
    {
        Map<String, Object> attributes;

        attributes = propertyRequirement.getAttributes();
        for (String attributeId : attributes.keySet())
        {
            propertyObject.addIfNotNull(T8IdentifierUtilities.toCamelCase(attributeId), attributes.get(attributeId));
        }
    }

    private static Map<String, Object> extractPropertyRequirementAttributes(JsonObject propertyRequirementObject)
    {
        Map<String, Object> attributes;

        attributes = new HashMap<>();
        for (String name : propertyRequirementObject.names())
        {
            if (!PROPERTY_REQUIREMENT_RESERVED_NAMES.contains(name))
            {
                Object value;

                value = propertyRequirementObject.getObject(name);
                attributes.put(T8IdentifierUtilities.toIdCase(name), value);
            }
        }

        return attributes;
    }

    private static void addValueRequirementAttributes(JsonObject drObject, ValueRequirement valueRequirement)
    {
        Map<String, Object> attributes;

        attributes = valueRequirement.getAttributes();
        for (String attributeId : attributes.keySet())
        {
            drObject.addIfNotNull(T8IdentifierUtilities.toCamelCase(attributeId), attributes.get(attributeId));
        }
    }

    private static Map<String, Object> extractValueRequirementAttributes(JsonObject valueRequirementObject)
    {
        Map<String, Object> attributes;

        attributes = new HashMap<>();
        for (String name : valueRequirementObject.names())
        {
            if (!VALUE_REQUIREMENT_STANDARD_NAMES.contains(name))
            {
                Object value;

                value = valueRequirementObject.getObject(name);
                attributes.put(T8IdentifierUtilities.toIdCase(name), value);
            }
        }

        return attributes;
    }
}
