package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.ValueAccessLayer;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ValueAccessStep extends DefaultPathStep implements PathStep
{
    public ValueAccessStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecordAccessLayer)
        {
            return evaluateFieldStep(((DataRecordAccessLayer)input).getValueAccessLayers());
        }
        else if (input instanceof SectionAccessLayer)
        {
            return evaluateFieldStep(((SectionAccessLayer)input).getValueAccessLayers());
        }
        else if (input instanceof PropertyAccessLayer)
        {
            return evaluateFieldStep(((PropertyAccessLayer)input).getValueAccessLayers());
        }
        else if (input instanceof FieldAccessLayer)
        {
            ValueAccessLayer valueAccessLayer;

            valueAccessLayer = ((FieldAccessLayer)input).getValueAccessLayer();
            if (valueAccessLayer != null)
            {
                return evaluateFieldStep(Arrays.asList(valueAccessLayer));
            }
            else return new ArrayList<ValueAccessLayer>();
        }
        else if (input instanceof List)
        {
            List<ValueAccessLayer> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<ValueAccessLayer>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<ValueAccessLayer>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Value access step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<ValueAccessLayer> evaluateFieldStep(List<ValueAccessLayer> accessLayerList)
    {
        // If we have ID's to use for narrowing of the list do it now.
        if (idList != null)
        {
            Iterator<ValueAccessLayer> accessLayerIterator;

            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                ValueAccessLayer nextAccessLayer;

                nextAccessLayer = accessLayerIterator.next();
                if (!idList.contains(nextAccessLayer.getRequirementType().getID()))
                {
                    accessLayerIterator.remove();
                }
            }
        }

        // Now run through the remaining items in the list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<ValueAccessLayer> accessLayerIterator;

            // Iterator over the access layers and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                ValueAccessLayer nextAccessLayer;

                // Create a map containing the input parameters available to the predicate expression.
                nextAccessLayer = accessLayerIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("ACCESS_LAYER", nextAccessLayer);
                inputParameters.put("REQUIREMENT_TYPE_ID", nextAccessLayer.getRequirementType().getID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        accessLayerIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return accessLayerList;
    }
}
