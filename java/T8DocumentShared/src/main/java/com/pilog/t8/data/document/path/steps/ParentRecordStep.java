package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ParentRecordStep extends DefaultPathStep implements PathStep
{
    public ParentRecordStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(((DataRecord)input).getParentRecord());
        }
        else if (input instanceof RecordProperty)
        {
            return evaluateStep(((RecordProperty)input).getParentDataRecord());
        }
        else if (input instanceof List)
        {
            List<DataRecord> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<DataRecord>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<DataRecord>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Class Id step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<DataRecord> evaluateStep(DataRecord record)
    {
        // No evaluation to do yet for this step type.
        return ArrayLists.newArrayList(record);
    }
}
