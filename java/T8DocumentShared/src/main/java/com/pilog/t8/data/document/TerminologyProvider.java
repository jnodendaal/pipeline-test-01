package com.pilog.t8.data.document;

import java.util.Collection;

/**
 * @author Bouwer du Preez
 */
public interface TerminologyProvider
{
    // Sets the default language to use when no language is specified.
    public void setLanguage(String languageId);

    // Convenience methods to return specific terminology.
    public String getCode(String conceptId);
    public String getTerm(String languageId, String conceptId);
    /**
     * Returns the abbreviation {@code String} value for the specified language
     * and concept ID.<br/>
     * <br/>
     * The implementation of this method should always return a value. If the
     * language ID and concept ID combination does not exist, {@code null}
     * should be returned.
     *
     * @param languageId The {@code String} language ID for the abbreviation to
     *      return
     * @param conceptId The {@code String} concept ID for the abbreviation to
     *      return
     *
     * @return The {@code String} abbreviation for the language/concept ID
     *      combination. {@code null} if no such combination exists
     */
    public String getAbbreviation(String languageId, String conceptId);
    public String getDefinition(String languageId, String conceptId);

    // Method to fetch the complete terminology for a concept.
    public T8ConceptTerminology getTerminology(String languageId, String conceptId);
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIdList, Collection<String> conceptIdList);
}
