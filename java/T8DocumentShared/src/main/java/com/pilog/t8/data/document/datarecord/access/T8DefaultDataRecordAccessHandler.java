package com.pilog.t8.data.document.datarecord.access;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordAccessHandler implements T8DataRecordAccessHandler
{
    private final String identifier;
    private final boolean editable;

    public T8DefaultDataRecordAccessHandler(String identifier, boolean editable)
    {
        this.identifier = identifier;
        this.editable = editable;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public void refreshDynamicState(DataRecord dataRecord)
    {
    }

    @Override
    public void setInitialState(DataRecord dataRecord)
    {
    }

    @Override
    public void setStaticState(DataRecord dataRecord)
    {
    }

    @Override
    public T8DataFileValidationReport validateState(DataRecord dataRecord)
    {
        return new T8DataFileValidationReport();
    }
}
