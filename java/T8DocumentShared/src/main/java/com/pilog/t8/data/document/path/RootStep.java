package com.pilog.t8.data.document.path;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.utilities.collections.Lists;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class RootStep implements PathStep
{
    @Override
    public List<PathRoot> evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return Lists.createSingular(new PathRoot((DataRecord)input));
        }
        else if (input instanceof List)
        {
            List<PathRoot> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<>(inputList.size());
            for (Object inputObject : inputList)
            {
                outputList.addAll(evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Cannot evaluate 'root' operator on root input: " + rootInput + " - and input: " + input);
    }

    @Override
    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        // Not needed here.
    }

    @Override
    public void setOntologyProvider(OntologyProvider ontologyProvider)
    {
        // Not needed here.
    }

    @Override
    public void setRecordProvider(DataRecordProvider recordProvider)
    {
        // Not needed here.
    }

    @Override
    public void setDataObjectProvider(T8DataObjectProvider dataObjectProvider)
    {
        // Not needed here.
    }

    @Override
    public String toString()
    {
        return "/";
    }
}
