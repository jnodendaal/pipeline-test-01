package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Date;
import java.util.Objects;

/**
 * Date-Time values are store as milliseconds since January 1, 1970, 00:00:00 GMT.
 * @author Bouwer du Preez
 */
public class DateTimeValueContent extends ValueContent
{
    private String dateTime;

    public DateTimeValueContent()
    {
        super(RequirementType.DATE_TIME_TYPE);
    }

    public void setDateTime(Date date)
    {
        setMilliseconds(date != null ? date.getTime() : null);
    }

    public String getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(String dateTime)
    {
        this.dateTime = dateTime;
    }

    public void setMilliseconds(Long millis)
    {
        this.dateTime = millis != null ? millis.toString() : null;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof DateTimeValueContent)
        {
            DateTimeValueContent toDateTimeContent;

            toDateTimeContent = (DateTimeValueContent)toContent;
            return Objects.equals(dateTime, toDateTimeContent.getDateTime());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof DateTimeValueContent)
        {
            DateTimeValueContent toDateTimeContent;

            toDateTimeContent = (DateTimeValueContent)toContent;
            return Objects.equals(dateTime, toDateTimeContent.getDateTime());
        }
        else return false;
    }
}
