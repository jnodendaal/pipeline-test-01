package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredNumberRequirement extends ValueRequirement implements PrescribedValueRequirement
{
    public MeasuredNumberRequirement()
    {
        super(RequirementType.MEASURED_NUMBER);
    }

    public NumberRequirement getNumberRequirement()
    {
        for (ValueRequirement subRequirement : getSubRequirements())
        {
            if (subRequirement instanceof NumberRequirement)
            {
                return (NumberRequirement)subRequirement;
            }
        }

        return null;
    }

    public void setNumberRequirement(NumberRequirement newNumberRequirement)
    {
        ValueRequirement existingNumberRequirement;

        // Get the first sub requirement.
        existingNumberRequirement = getNumberRequirement();
        if (existingNumberRequirement == null)
        {
            // If there are not sub requirements, just ad the new numeric requirement.
            addSubRequirement(newNumberRequirement, 0);
        }
        else
        {
            // Remove the existing number requirement first, then add the new one.
            removeSubRequirement(existingNumberRequirement);
            addSubRequirement(newNumberRequirement, 0);
        }
    }

    public UnitOfMeasureRequirement getUomRequirement()
    {
        return (UnitOfMeasureRequirement)getSubRequirement(RequirementType.UNIT_OF_MEASURE, null, null);
    }

    public QualifierOfMeasureRequirement getQomRequirement()
    {
        return (QualifierOfMeasureRequirement)getSubRequirement(RequirementType.QUALIFIER_OF_MEASURE, null, null);
    }

    @Override
    public boolean isPrescribed()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    @Override
    public boolean isRestrictToStandardValue()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    public String getUomOntologyClassId()
    {
        UnitOfMeasureRequirement uomRequirement;

        uomRequirement = getUomRequirement();
        return uomRequirement != null ? uomRequirement.getOntologyClassID() : null;
    }

    public String getQomOntologyClassId()
    {
        QualifierOfMeasureRequirement qomRequirement;

        qomRequirement = getQomRequirement();
        return qomRequirement != null ? qomRequirement.getOntologyClassID() : null;
    }
}
