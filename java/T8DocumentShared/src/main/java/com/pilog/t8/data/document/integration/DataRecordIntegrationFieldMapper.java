package com.pilog.t8.data.document.integration;

import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.api.T8Api;

/**
 * @author Andre Scheepers
 * @param <T> The Data Record Field Mapping Object
 */
public interface DataRecordIntegrationFieldMapper <T>
{
    public void initialize( T8Api orgApi,
                            T8LRUCachedTerminologyProvider terminologyProvider,
                            T8DataRecordPropertyValueStringGenerator valueStringGenerator);

    public T doFieldMapping(DataRecord dataRecord) throws Exception;
}
