package com.pilog.t8.data.document.datastring;

import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datastring.T8DataStringFormat.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringGeneratorContext
{
    private T8ExpressionEvaluator expressionEvaluator;
    private T8DataStringFormatProvider dataStringFormatProvider;
    private T8DataStringInstance lastGeneratedInstance;
    private final List<T8DataStringFormat> lastGenerationFormats;
    private T8OrganizationStructure orgStructure;

    public T8DataStringGeneratorContext()
    {
        this.lastGenerationFormats = new ArrayList<T8DataStringFormat>();
    }

    public T8OrganizationStructure getOrganizationStructure()
    {
        return orgStructure;
    }

    public void setOrganizationStructure(T8OrganizationStructure orgStructure)
    {
        this.orgStructure = orgStructure;
    }

    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return expressionEvaluator;
    }

    public void setExpressionEvaluator(T8ExpressionEvaluator expressionEvaluator)
    {
        this.expressionEvaluator = expressionEvaluator;
    }

    public T8DataStringFormatProvider getDataStringFormatProvider()
    {
        return dataStringFormatProvider;
    }

    public void setDataStringFormatProvider(T8DataStringFormatProvider dataStringFormatProvider)
    {
        this.dataStringFormatProvider = dataStringFormatProvider;
    }

    public T8DataStringInstance getLastGeneratedInstance()
    {
        return lastGeneratedInstance;
    }

    public List<T8DataStringFormat> getLastGenerationFormats()
    {
        return new ArrayList<T8DataStringFormat>(lastGenerationFormats);
    }

    public T8DataStringParticle generateDataString(T8DataStringInstance dsInstance, DataRecord documentValue, String dataKeyId)
    {
        lastGeneratedInstance = dsInstance;
        lastGenerationFormats.clear();
        return generateDataString(dsInstance, (Value)documentValue, dataKeyId);
    }

    T8DataStringParticle generateDataString(T8DataStringInstance dsInstance, Value documentValue, String dataKeyId)
    {
        T8DataStringFormat dataStringFormat;
        T8DataStringKey dataStringKey;

        // Create the rendering key.
        dataStringKey = createDataStringKey(dsInstance, documentValue, dataKeyId);

        // Ok now we need to find the data string format for this combination of parameters.
        dataStringFormat = dataStringFormatProvider.getDataStringFormat(dataStringKey);
        if (dataStringFormat != null)
        {
            // Add the data string format to the list of used formats (if it does not already exist in the list).
            if (!lastGenerationFormats.contains(dataStringFormat)) lastGenerationFormats.add(dataStringFormat);

            // Generate the particly and return the result.
            return dataStringFormat.generateParticle(this, dsInstance, documentValue, dataKeyId);
        }
        else
        {
            return null;
        }
    }

    public T8DataStringParticleSettingsList getDataStringParticleSettings(T8DataStringInstance dataStringIntance, List<Value> documentValues, String dataKeyId)
    {
        List<T8DataStringKey> keys;

        // Create the rendering keys.
        keys = new ArrayList<>();
        for (Value documentValue : documentValues)
        {
            keys.add(createDataStringKey(dataStringIntance, documentValue, dataKeyId));
        }

        // Fetch and return the particles matching the keys.
        return dataStringFormatProvider.getDataStringParticleSettings(keys);
    }

    public T8DataStringKey createDataStringKey(T8DataStringInstance dsInstance, Value documentValue, String dataKeyId)
    {
        T8DataStringKey key;

        // Depending on the type of the document object, set the key parameters.
        if (documentValue instanceof DataRecord)
        {
            DataRecord dataFile;
            DataRecord record;

            record = (DataRecord)documentValue;
            dataFile = record.getDataFile();
            key = new T8DataStringKey(Type.RECORD);
            key.setDrId(record.getDataRequirementID());
            key.setDrInstanceId(record.getDataRequirementInstanceID());
            key.setRootRecordId(dataFile.getID());
            key.setDataKeyId(dataKeyId);
            key.setRecordId(record.getID());
            key.setPropertyId(null);
            key.setFieldId(null);
            key.setDataTypeId(null);
        }
        else if (documentValue instanceof RecordProperty)
        {
            RecordProperty property;
            DataRecord dataFile;

            property = (RecordProperty)documentValue;
            dataFile = property.getParentDataRecord().getDataFile();
            key = new T8DataStringKey(Type.PROPERTY);
            key.setDrId(property.getDataRequirementID());
            key.setDrInstanceId(property.getDataRequirementInstanceID());
            key.setRootRecordId(dataFile.getID());
            key.setDataKeyId(dataKeyId);
            key.setRecordId(property.getRecordID());
            key.setPropertyId(property.getPropertyID());
            key.setFieldId(null);
            key.setDataTypeId(null);
        }
        else if (documentValue instanceof RecordValue)
        {
            DataRecord dataFile;
            RecordValue value;

            value = (RecordValue)documentValue;
            dataFile = value.getParentDataRecord().getDataFile();
            key = new T8DataStringKey(value.isField() ? Type.FIELD : Type.DATA_TYPE);
            key.setDrId(value.getDataRequirementID());
            key.setDrInstanceId(value.getDataRequirementInstanceID());
            key.setRootRecordId(dataFile.getID());
            key.setDataKeyId(dataKeyId);
            key.setRecordId(value.getRecordID());
            key.setPropertyId(value.getPropertyID());
            key.setFieldId(value.getFieldID());
            key.setDataTypeId(value.getRequirementTypeID());
        }
        else throw new RuntimeException("Invalid document object encountered: " + documentValue);

        // Set the rendering specification.
        key.setLanguageId(dsInstance.getLanguageID());
        key.setDsTypeId(dsInstance.getTypeID());
        key.setOrgId(dsInstance.getOrgId());

        // Return the complete key.
        return key;
    }
}
