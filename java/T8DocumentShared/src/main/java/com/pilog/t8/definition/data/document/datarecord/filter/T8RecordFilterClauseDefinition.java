package com.pilog.t8.definition.data.document.datarecord.filter;

import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterClause;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public abstract class T8RecordFilterClauseDefinition extends T8Definition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_RECORD_FILTER_CLAUSE";
    public static final String STORAGE_PATH = null;
    private enum Datum {CONJUNCTION};
    // -------- Definition Meta-Data -------- //

    public T8RecordFilterClauseDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONJUNCTION.toString(), "Conjunction", "The conjunction to use for this clause.", DataFilterConjunction.AND.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CONJUNCTION.toString().equals(datumIdentifier)) return createStringOptions(DataFilterConjunction.values());
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public abstract T8RecordFilterClause getRecordFilterClauseInstance(T8Context context);

    public DataFilterConjunction getConjunction()
    {
        String stringValue;

        stringValue = (String)getDefinitionDatum(Datum.CONJUNCTION.toString());
        if (Strings.isNullOrEmpty(stringValue)) return DataFilterConjunction.AND;
        else return DataFilterConjunction.valueOf(stringValue);
    }

    public void setConjunction(DataFilterConjunction conjunction)
    {
        setDefinitionDatum(Datum.CONJUNCTION.toString(), conjunction.toString());
    }
}
