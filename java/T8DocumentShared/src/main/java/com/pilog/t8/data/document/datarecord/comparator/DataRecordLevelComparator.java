package com.pilog.t8.data.document.datarecord.comparator;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.Comparator;

/**
 * @author Bouwer du Preez
 */
public class DataRecordLevelComparator implements Comparator
{
    @Override
    public int compare(Object o1, Object o2)
    {
        Integer level1;
        Integer level2;
        
        level1 = ((DataRecord)o1).getRecordLevel();
        level2 = ((DataRecord)o2).getRecordLevel();
        return level1.compareTo(level2);
    }
}
