package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class StripNonAlphanumericStep extends DefaultPathStep implements PathStep
{
    public StripNonAlphanumericStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        super.setTerminologyProvider(terminologyProvider);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input == null)
        {
            List<String> result;

            // DocPath steps always return lists.
            result = new ArrayList(1);
            result.add(null);
            return result;
        }
        else if (input instanceof CharSequence)
        {
            List<String> result;

            // DocPath steps always return lists.
            result = new ArrayList(1);
            result.add(Strings.stripNonAlphanumeric((CharSequence)input, true));
            return result;
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Step '" + stepString + "' cannot be executed from content object: " + input);
    }
}
