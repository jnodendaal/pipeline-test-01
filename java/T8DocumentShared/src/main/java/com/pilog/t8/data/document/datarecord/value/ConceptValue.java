package com.pilog.t8.data.document.datarecord.value;

/**
 * @author Bouwer du Preez
 */
public interface ConceptValue
{
    public String getConceptId();
    public void setConceptId(String conceptId, boolean clearDependentValues);
    public String getTerm();
    public void setTerm(String term);
}
