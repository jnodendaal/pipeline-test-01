package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class SectionAccessLayer implements Serializable
{
    private final String sectionId;
    private boolean editable;
    private boolean visible;
    private boolean required;
    private DataRecordAccessLayer parentAccessLayer;
    private final Map<String, PropertyAccessLayer> propertyAccessLayers;

    public SectionAccessLayer(String sectionId)
    {
        this.sectionId = sectionId;
        this.editable = true;
        this.visible = true;
        this.propertyAccessLayers = new HashMap<String, PropertyAccessLayer>();
    }

    void setParentAccessLayer(DataRecordAccessLayer recordAccessLayer)
    {
        this.parentAccessLayer = recordAccessLayer;
    }

    public DataRecordAccessLayer getParentAccessLayer()
    {
        return parentAccessLayer;
    }

    public DataRecord getParentDataRecord()
    {
        return parentAccessLayer != null ? parentAccessLayer.getDataRecord() : null;
    }

    public SectionAccessLayer copy()
    {
        SectionAccessLayer copy;

        copy = new SectionAccessLayer(sectionId);
        copy.setEditable(editable);
        copy.setVisible(visible);

        for (PropertyAccessLayer propertyAccessLayer : propertyAccessLayers.values())
        {
            copy.addPropertyAccessLayer(propertyAccessLayer.copy());
        }

        return copy;
    }

    public void setAccess(SectionAccessLayer newAccess)
    {
        setEditable(newAccess.isEditable());
        setVisible(newAccess.isVisible());

        for (PropertyAccessLayer propertyAccessLayer : propertyAccessLayers.values())
        {
            PropertyAccessLayer newPropertyAccess;

            newPropertyAccess = newAccess.getPropertyAccessLayer(propertyAccessLayer.getPropertyId());
            if (newPropertyAccess != null)
            {
                propertyAccessLayer.setAccess(newPropertyAccess);
            }
        }
    }

    public String getSectionID()
    {
        return sectionId;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setPathEditable()
    {
        this.editable = true;
        parentAccessLayer.setPathEditable();
    }

    public void setEditable(boolean editable)
    {
        // Set the editability.
        this.editable = editable;

        // Propagate the editability to parent access layers.
        if (editable) parentAccessLayer.setPathEditable();

        // Set the editability of all the properties.
        for (PropertyAccessLayer propertyAccessLayer : propertyAccessLayers.values())
        {
            propertyAccessLayer.setEditable(editable);
        }
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setPathVisible()
    {
        this.visible = true;
        parentAccessLayer.setPathVisible();
    }

    public void setVisible(boolean visible)
    {
        // Set the visibility of this access layer.
        this.visible = visible;

        // Propagate the visibility to parent access layers.
        if (visible) parentAccessLayer.setPathVisible();

        // Set the visibility of all the properties.
        for (PropertyAccessLayer propertyAccessLayer : propertyAccessLayers.values())
        {
            propertyAccessLayer.setVisible(visible);
        }
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public void setDescendentsRequired(boolean required)
    {
        // Set the required attribute of all the properties and descendents
        for (PropertyAccessLayer propertyAccessLayer : propertyAccessLayers.values())
        {
            propertyAccessLayer.setRequired(required);
            propertyAccessLayer.setDescendentsRequired(required);
        }
    }

    public PropertyAccessLayer getPropertyAccessLayer(String propertyID)
    {
        return propertyAccessLayers.get(propertyID);
    }

    public List<PropertyAccessLayer> getPropertyAccessLayers()
    {
        return new ArrayList<PropertyAccessLayer>(propertyAccessLayers.values());
    }

    public PropertyAccessLayer removePropertyAccessLayer(String propertyID)
    {
        PropertyAccessLayer propertyAccessLayer;

        propertyAccessLayer = propertyAccessLayers.get(propertyID);
        if (propertyAccessLayer != null) propertyAccessLayer.setParentAccessLayer(null);
        return propertyAccessLayer;
    }

    public void addPropertyAccessLayer(PropertyAccessLayer propertyAccessLayer)
    {
        propertyAccessLayer.setParentAccessLayer(this);
        propertyAccessLayers.put(propertyAccessLayer.getPropertyId(), propertyAccessLayer);
    }

    public List<FieldAccessLayer> getFieldAccessLayers()
    {
        List<FieldAccessLayer> fieldAccessLayers;

        fieldAccessLayers = new ArrayList<FieldAccessLayer>();
        for (PropertyAccessLayer propertyAccessLayer : propertyAccessLayers.values())
        {
            fieldAccessLayers.addAll(propertyAccessLayer.getFieldAccessLayers());
        }

        return fieldAccessLayers;
    }

    public List<ValueAccessLayer> getValueAccessLayers()
    {
        List<ValueAccessLayer> valueAccessLayers;

        valueAccessLayers = new ArrayList<ValueAccessLayer>();
        for (PropertyAccessLayer propertyAccessLayer : propertyAccessLayers.values())
        {
            valueAccessLayers.addAll(propertyAccessLayer.getValueAccessLayers());
        }

        return valueAccessLayers;
    }

    @Override
    public String toString()
    {
        return "SectionAccessLayer{" + "sectionID=" + sectionId + ", editable=" + editable + ", visible=" + visible + ", required=" + required + '}';
    }
}
