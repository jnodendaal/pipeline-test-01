package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.data.T8DataParameterResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordHistoryApiResource implements T8DefinitionResource
{
    public static final String OPERATION_RETRIEVE_FILE_HISTORY_SNAPSHOTS = "@OP_API_DRH_RETRIEVE_FILE_HISTORY_SNAPSHOTS";

    public static final String PARAMETER_FILE_ID = "$P_FILE_ID";
    public static final String PARAMETER_TRANSACTION_ID_LIST = "$P_TRANSACTION_ID_LIST";
    public static final String PARAMETER_SNAPSHOT_MAP = "$P_SNAPSHOT_MAP";

    // Entity fields defined by the API.
    public static final String EF_TX_IID = "$TX_IID";
    public static final String EF_RECORD_ID = "$RECORD_ID";
    public static final String EF_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    public static final String EF_EVENT = "$EVENT";
    public static final String EF_ROOT_RECORD_ID = "$ROOT_RECORD_ID";
    public static final String EF_TIME = "$TIME";
    public static final String EF_PARENT_RECORD_ID = "$PARENT_RECORD_ID";
    public static final String EF_FFT = "$FFT";
    public static final String EF_OLD_FFT = "$OLD_FFT";
    public static final String EF_DR_ID = "$DR_ID";
    public static final String EF_AGENT_ID = "$AGENT_ID";
    public static final String EF_AGENT_IID = "$AGENT_IID";
    public static final String EF_USER_ID = "$USER_ID";
    public static final String EF_SESSION_ID = "$SESSION_ID";
    public static final String EF_OPERATION_ID = "$OPERATION_ID";
    public static final String EF_OPERATION_IID = "$OPERATION_IID";
    public static final String EF_FLOW_ID = "$FLOW_ID";
    public static final String EF_FLOW_IID = "$FLOW_IID";
    public static final String EF_TASK_ID = "$TASK_ID";
    public static final String EF_TASK_IID = "$TASK_IID";
    public static final String EF_FUNCTIONALITY_ID = "$FUNCTIONALITY_ID";
    public static final String EF_FUNCTIONALITY_IID = "$FUNCTIONALITY_IID";
    public static final String EF_PROCESS_ID = "$PROCESS_ID";
    public static final String EF_PROCESS_IID = "$PROCESS_IID";
    public static final String EF_PROPERTY_ID = "$PROPERTY_ID";
    public static final String EF_SECTION_ID = "$SECTION_ID";
    public static final String EF_DATA_TYPE_ID = "$DATA_TYPE_ID";
    public static final String EF_DATA_TYPE_SEQUENCE = "$DATA_TYPE_SEQUENCE";
    public static final String EF_VALUE_SEQUENCE = "$VALUE_SEQUENCE";
    public static final String EF_PARENT_VALUE_SEQUENCE = "$PARENT_VALUE_SEQUENCE";
    public static final String EF_VALUE = "$VALUE";
    public static final String EF_OLD_VALUE = "$OLD_VALUE";
    public static final String EF_VALUE_ID = "$VALUE_ID";
    public static final String EF_OLD_VALUE_ID = "$OLD_VALUE_ID";
    public static final String EF_VALUE_CONCEPT_ID = "$VALUE_CONCEPT_ID";
    public static final String EF_OLD_VALUE_CONCEPT_ID = "$OLD_VALUE_CONCEPT_ID";
    public static final String EF_FIELD_ID = "$FIELD_ID";
    public static final String EF_TEXT = "$TEXT";
    public static final String EF_OLD_TEXT = "$OLD_TEXT";
    public static final String EF_ATTACHMENT_ID = "$ATTACHMENT_ID";
    public static final String EF_FILE_CONTEXT_ID = "$FILE_CONTEXT_ID";
    public static final String EF_FILE_NAME = "$FILE_NAME";
    public static final String EF_FILE_SIZE = "$FILE_SIZE";
    public static final String EF_MD5_CHECKSUM = "$MD5_CHECKSUM";
    public static final String EF_MEDIA_TYPE = "$MEDIA_TYPE";
    public static final String EF_ATTRIBUTE_ID = "$ATTRIBUTE_ID";
    public static final String EF_TYPE = "$TYPE";

    // HISTORY_DATA_RECORD_DOC.
    public static final String HISTORY_DATA_RECORD_DOC_DSID = "@DS_HISTORY_DATA_RECORD_DOC";
    public static final String HISTORY_DATA_RECORD_DOC_DEID = "@E_HISTORY_DATA_RECORD_DOC";

    // HISTORY_DATA_RECORD_PROPERTY.
    public static final String HISTORY_DATA_RECORD_PROPERTY_DSID = "@DS_HISTORY_DATA_RECORD_PROPERTY";
    public static final String HISTORY_DATA_RECORD_PROPERTY_DEID = "@E_HISTORY_DATA_RECORD_PROPERTY";

    // HISTORY_DATA_RECORD_VALUE.
    public static final String HISTORY_DATA_RECORD_VALUE_DSID = "@DS_HISTORY_DATA_RECORD_VALUE";
    public static final String HISTORY_DATA_RECORD_VALUE_DEID = "@E_HISTORY_DATA_RECORD_VALUE";

    // HISTORY_DATA_RECORD_ATTRIBUTE.
    public static final String HISTORY_DATA_RECORD_ATTRIBUTE_DSID = "@DS_HISTORY_DATA_RECORD_ATTRIBUTE";
    public static final String HISTORY_DATA_RECORD_ATTRIBUTE_DEID = "@E_HISTORY_DATA_RECORD_ATTRIBUTE";

    // HISTORY_DATA_RECORD_ATTACHMENT.
    public static final String HISTORY_DATA_RECORD_ATTACHMENT_DSID = "@DS_HISTORY_DATA_RECORD_ATTACHMENT";
    public static final String HISTORY_DATA_RECORD_ATTACHMENT_DEID = "@E_HISTORY_DATA_RECORD_ATTACHMENT";

    // Table Names.
    private static final String TABLE_HISTORY_DATA_RECORD_DOC = "HIS_DAT_REC";
    private static final String TABLE_HISTORY_DATA_RECORD_PROPERTY = "HIS_DAT_REC_PRP";
    private static final String TABLE_HISTORY_DATA_RECORD_VALUE = "HIS_DAT_REC_VAL";
    private static final String TABLE_HISTORY_DATA_RECORD_DOC_ATR = "HIS_DAT_REC_ATR";
    private static final String TABLE_HISTORY_DATA_RECORD_ATTACHMENT = "HIS_DAT_REC_ATC";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))definitions.addAll(getOperationDefinitions());
        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER))definitions.addAll(getDataSourceDefinitions(serverContext));
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER))definitions.addAll(getDataEntityDefinitions(serverContext));
        return definitions;
    }

    public List<T8Definition> getDataSourceDefinitions(T8ServerContext serverContext)
    {
        T8TableDataSourceDefinition tableDataSourceDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // HISTORY_DATA_RECORD_DOC.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_DATA_RECORD_DOC_DSID);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_DATA_RECORD_DOC);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TX_IID, "TX_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TIME, "TIME", true, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_RECORD_ID, "PARENT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FFT, "FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_FFT, "OLD_FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_AGENT_ID, "AGENT_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_AGENT_IID, "AGENT_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_USER_ID, "USER_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SESSION_ID, "SESSION_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OPERATION_ID, "OPERATION_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OPERATION_IID, "OPERATION_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FLOW_ID, "FLOW_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FLOW_IID, "FLOW_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TASK_ID, "TASK_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TASK_IID, "TASK_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FUNCTIONALITY_ID, "FUNCTIONALITY_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FUNCTIONALITY_IID, "FUNCTIONALITY_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROCESS_ID, "PROCESS_ID", false, false, T8DataType.DEFINITION_IDENTIFIER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROCESS_IID, "PROCESS_IID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // HISTORY_DATA_RECORD_PROPERTY.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_DATA_RECORD_PROPERTY_DSID);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_DATA_RECORD_PROPERTY);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TX_IID, "TX_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TIME, "TIME", true, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SECTION_ID, "SECTION_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FFT, "FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_FFT, "OLD_FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // HISTORY_DATA_RECORD_VALUE.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_DATA_RECORD_VALUE_DSID);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_DATA_RECORD_VALUE);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TX_IID, "TX_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TIME, "TIME", true, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PROPERTY_ID, "PROPERTY_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DR_ID, "DR_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_SECTION_ID, "SECTION_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_ID, "DATA_TYPE_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, "DATA_TYPE_SEQUENCE", true, true, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_SEQUENCE, "VALUE_SEQUENCE", true, true, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_PARENT_VALUE_SEQUENCE, "PARENT_VALUE_SEQUENCE", false, false, T8DataType.INTEGER));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FIELD_ID, "FIELD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_VALUE, "OLD_VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FFT, "FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_FFT, "OLD_FFT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_ID, "VALUE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_VALUE_ID, "OLD_VALUE_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE_CONCEPT_ID, "VALUE_CONCEPT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_VALUE_CONCEPT_ID, "OLD_VALUE_CONCEPT_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TEXT, "TEXT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_TEXT, "OLD_TEXT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // HISTORY_DATA_RECORD_DOC_ATR.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_DATA_RECORD_ATTRIBUTE_DSID);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_DATA_RECORD_DOC_ATR);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TX_IID, "TX_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TIME, "TIME", true, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTRIBUTE_ID, "ATTRIBUTE_ID", true, true, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TYPE, "TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_VALUE, "VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_OLD_VALUE, "OLD_VALUE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        // HISTORY_DATA_RECORD_ATTACHMENT.
        tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(HISTORY_DATA_RECORD_ATTACHMENT_DSID);
        tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
        tableDataSourceDefinition.setTableName(TABLE_HISTORY_DATA_RECORD_ATTACHMENT);
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TX_IID, "TX_IID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_TIME, "TIME", true, true, T8DataType.TIMESTAMP));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_EVENT, "EVENT", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_RECORD_ID, "RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ROOT_RECORD_ID, "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_ATTACHMENT_ID, "ATTACHMENT_ID", true, true, T8DataType.GUID));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_CONTEXT_ID, "FILE_CONTEXT_ID", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_NAME, "FILE_NAME", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_FILE_SIZE, "FILE_SIZE", false, false, T8DataType.LONG));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_MD5_CHECKSUM, "MD5_CHECKSUM", false, false, T8DataType.STRING));
        tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(EF_MEDIA_TYPE, "MEDIA_TYPE", false, false, T8DataType.STRING));
        tableDataSourceDefinition.setAutoGenerate(false);
        definitions.add(tableDataSourceDefinition);

        return definitions;
    }

    public List<T8Definition> getDataEntityDefinitions(T8ServerContext serverContext)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        // HISTORY_DATA_RECORD_DOC.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_DATA_RECORD_DOC_DEID);
        entityDefinition.setDataSourceIdentifier(HISTORY_DATA_RECORD_DOC_DSID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TX_IID, HISTORY_DATA_RECORD_DOC_DSID + EF_TX_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TIME, HISTORY_DATA_RECORD_DOC_DSID + EF_TIME, true, true, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT, HISTORY_DATA_RECORD_DOC_DSID + EF_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_ROOT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_RECORD_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_PARENT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_DR_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_INSTANCE_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_DR_INSTANCE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FFT, HISTORY_DATA_RECORD_DOC_DSID + EF_FFT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_FFT, HISTORY_DATA_RECORD_DOC_DSID + EF_OLD_FFT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_AGENT_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_AGENT_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_AGENT_IID, HISTORY_DATA_RECORD_DOC_DSID + EF_AGENT_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_USER_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_USER_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SESSION_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_SESSION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OPERATION_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_OPERATION_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OPERATION_IID, HISTORY_DATA_RECORD_DOC_DSID + EF_OPERATION_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FLOW_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_FLOW_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FLOW_IID, HISTORY_DATA_RECORD_DOC_DSID + EF_FLOW_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TASK_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_TASK_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TASK_IID, HISTORY_DATA_RECORD_DOC_DSID + EF_TASK_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FUNCTIONALITY_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_FUNCTIONALITY_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FUNCTIONALITY_IID, HISTORY_DATA_RECORD_DOC_DSID + EF_FUNCTIONALITY_IID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROCESS_ID, HISTORY_DATA_RECORD_DOC_DSID + EF_PROCESS_ID, false, false, T8DataType.DEFINITION_IDENTIFIER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROCESS_IID, HISTORY_DATA_RECORD_DOC_DSID + EF_PROCESS_IID, false, false, T8DataType.GUID));
        definitions.add(entityDefinition);

        // HISTORY_DATA_RECORD_PROPERTY.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_DATA_RECORD_PROPERTY_DEID);
        entityDefinition.setDataSourceIdentifier(HISTORY_DATA_RECORD_PROPERTY_DSID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TX_IID, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_TX_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TIME, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_TIME, true, true, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_ROOT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_PROPERTY_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_DR_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SECTION_ID, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_SECTION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FFT, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_FFT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_FFT, HISTORY_DATA_RECORD_PROPERTY_DSID + EF_OLD_FFT, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // HISTORY_DATA_RECORD_VALUE.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_DATA_RECORD_VALUE_DEID);
        entityDefinition.setDataSourceIdentifier(HISTORY_DATA_RECORD_VALUE_DSID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TX_IID, HISTORY_DATA_RECORD_VALUE_DSID + EF_TX_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TIME, HISTORY_DATA_RECORD_VALUE_DSID + EF_TIME, true, true, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT, HISTORY_DATA_RECORD_VALUE_DSID + EF_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_ROOT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PROPERTY_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_PROPERTY_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DR_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_DR_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_SECTION_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_SECTION_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_DATA_TYPE_ID, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_DATA_TYPE_SEQUENCE, HISTORY_DATA_RECORD_VALUE_DSID + EF_DATA_TYPE_SEQUENCE, true, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_SEQUENCE, HISTORY_DATA_RECORD_VALUE_DSID + EF_VALUE_SEQUENCE, true, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_PARENT_VALUE_SEQUENCE, HISTORY_DATA_RECORD_VALUE_DSID + EF_PARENT_VALUE_SEQUENCE, false, false, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FIELD_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_FIELD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, HISTORY_DATA_RECORD_VALUE_DSID + EF_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_VALUE, HISTORY_DATA_RECORD_VALUE_DSID + EF_OLD_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FFT, HISTORY_DATA_RECORD_VALUE_DSID + EF_FFT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_FFT, HISTORY_DATA_RECORD_VALUE_DSID + EF_OLD_FFT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_VALUE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_VALUE_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_OLD_VALUE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE_CONCEPT_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_VALUE_CONCEPT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_VALUE_CONCEPT_ID, HISTORY_DATA_RECORD_VALUE_DSID + EF_OLD_VALUE_CONCEPT_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TEXT, HISTORY_DATA_RECORD_VALUE_DSID + EF_TEXT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_TEXT, HISTORY_DATA_RECORD_VALUE_DSID + EF_OLD_TEXT, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // HISTORY_DATA_RECORD_DOC_ATR.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_DATA_RECORD_ATTRIBUTE_DEID);
        entityDefinition.setDataSourceIdentifier(HISTORY_DATA_RECORD_ATTRIBUTE_DSID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TX_IID, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_TX_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TIME, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_TIME, true, true, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_RECORD_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_ROOT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTRIBUTE_ID, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_ATTRIBUTE_ID, true, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TYPE, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_TYPE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_VALUE, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_VALUE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_OLD_VALUE, HISTORY_DATA_RECORD_ATTRIBUTE_DSID + EF_OLD_VALUE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        // HISTORY_DATA_RECORD_ATTACHMENT.
        entityDefinition = new T8DataEntityResourceDefinition(HISTORY_DATA_RECORD_ATTACHMENT_DEID);
        entityDefinition.setDataSourceIdentifier(HISTORY_DATA_RECORD_ATTACHMENT_DSID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TX_IID, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_TX_IID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_TIME, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_TIME, true, true, T8DataType.TIMESTAMP));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_EVENT, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_EVENT, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_RECORD_ID, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ROOT_RECORD_ID, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_ROOT_RECORD_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_ATTACHMENT_ID, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_ATTACHMENT_ID, true, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_CONTEXT_ID, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_FILE_CONTEXT_ID, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_NAME, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_FILE_NAME, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_FILE_SIZE, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_FILE_SIZE, false, false, T8DataType.LONG));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_MD5_CHECKSUM, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_MD5_CHECKSUM, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(EF_MEDIA_TYPE, HISTORY_DATA_RECORD_ATTACHMENT_DSID + EF_MEDIA_TYPE, false, false, T8DataType.STRING));
        definitions.add(entityDefinition);

        return definitions;
    }

    public List<T8Definition> getOperationDefinitions()
    {
        T8JavaServerOperationDefinition definition;
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        definition = new T8JavaServerOperationDefinition(OPERATION_RETRIEVE_FILE_HISTORY_SNAPSHOTS);
        definition.setMetaDisplayName("Retrieve File History Snapshots");
        definition.setMetaDescription("Retrieves shapshots of a data file at specific times in its history.");
        definition.setClassName("com.pilog.t8.api.T8DataRecordHistoryApiOperations$RetrieveFileHistorySnapshots");
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_FILE_ID, "File Id", "The record Id of the root record for which the history will be retrieved.", T8DataType.GUID));
        definition.addInputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_TRANSACTION_ID_LIST, "Transaction Id List", "The list of transaction ID's for which history will be retrieved, if no values are given the full history across all transactions will be retrieved", new T8DtList(T8DataType.GUID)));
        definition.addOutputParameterDefinition(new T8DataParameterResourceDefinition(PARAMETER_SNAPSHOT_MAP, "Snapshot Map", "A map containing the data record as at was represented at each transaction. The root record as it is currently will be in the map with a null key.", new T8DtMap(T8DataType.GUID, T8DataType.CUSTOM_OBJECT)));
        definitions.add(definition);

        return definitions;
    }
}
