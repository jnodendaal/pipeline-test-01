package com.pilog.t8.data.document.path;

import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;

/**
 * @author Bouwer du Preez
 */
public interface DocPathEvaluatorFactory
{
    public DocPathExpressionEvaluator getEvaluator();
    public void setLanguage(String languageId);

    public static DocPathEvaluatorFactory getFactory(T8Context context)
    {
        if (context.isServer())
        {
            return T8Reflections.getInstance("com.pilog.t8.data.document.path.T8ServerDocPathEvaluatorFactory", new Class<?>[]{T8Context.class}, context);
        }
        else
        {
            return T8Reflections.getInstance("com.pilog.t8.data.document.path.T8ClientDocPathEvaluatorFactory", new Class<?>[]{T8Context.class}, context);
        }
    }
}
