package com.pilog.t8.data.document.datarecord.filter;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Pieter Strydom
 */
public class T8OrganizationFilterCriterion implements Serializable
{
    private List<String> organizationTypeIdList;
    private Object filterValue;
    private List<Object> dataTypes;
    private DataFilterOperator operator;
    private String label;

    public T8OrganizationFilterCriterion(List<String>organizationTypeIdList, String label)
    {
        this.organizationTypeIdList = organizationTypeIdList;
        this.operator = DataFilterOperator.EQUAL;
        this.dataTypes = new ArrayList<Object>();
        this.label = label;
    }

    public T8OrganizationFilterCriterion(List<String>organizationTypeIdList, List<Object> dataTypes, DataFilterOperator operator, Object filterValue, String label)
    {
        this.organizationTypeIdList = organizationTypeIdList;
        this.dataTypes = new ArrayList<Object>();
        this.operator = operator;
        this.filterValue = filterValue;
        this.label = label;

        if (dataTypes != null) this.dataTypes.addAll(dataTypes);
    }

    public T8OrganizationFilterCriterion copy()
    {
        T8OrganizationFilterCriterion copiedCriterion;

        copiedCriterion = new T8OrganizationFilterCriterion(organizationTypeIdList, dataTypes, operator, filterValue, label);
        return copiedCriterion;
    }

    public boolean hasCriterion()
    {
        if (operator == DataFilterOperator.IS_NULL) return true; // This operator does not require a filter value.
        else if (operator == DataFilterOperator.IS_NOT_NULL) return true; // This operator does not require a filter value.
        else if (filterValue == null) return false; // All other operators require a filter value.
        else if (filterValue instanceof String) // If the filter value is a String type, make sure it is not empty.
        {
            return !Strings.isNullOrEmpty((String)filterValue);
        }
        else return true;
    }

    public List<String> getOrganizationTypeIdList()
    {
        return organizationTypeIdList;
    }

    public void setOrganizationTypeIdList(List<String> organizationTypeIdList)
    {
        this.organizationTypeIdList = organizationTypeIdList;
    }

    public Object getFilterValue()
    {
        return filterValue;
    }

    public void setFilterValue(Object filterValue)
    {
        this.filterValue = filterValue;
    }

    public List<Object> getDataTypes()
    {
        return dataTypes;
    }

    public void setDataTypes(List<Object> dataTypes)
    {
        this.dataTypes = dataTypes;
    }

    public DataFilterOperator getOperator()
    {
        return operator;
    }

    public void setOperator(DataFilterOperator operator)
    {
        this.operator = operator;
    }
    
    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }
}
