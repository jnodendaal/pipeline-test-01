package com.pilog.t8.definition.data.document.datarecord.access.layout;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout.PropertyHeaderLayoutType;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout.PropertyHeaderTextType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PropertyLayoutDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_ACCESS_PROPERTY_LAYOUT";
    public static final String GROUP_IDENTIFIER = "@DG_DATA_RECORD_ACCESS_PROPERTY_LAYOUT";
    public static final String DISPLAY_NAME = "Property Layout";
    public static final String DESCRIPTION = "A definition that specifies layout of a specific Data Record Property.";
    public static final String IDENTIFIER_PREFIX = "PROPERTY_LAYOUT_";
    public enum Datum {HEADER_LAYOUT_TYPE,
                       HEADER_TEXT_TYPE,
                       CONTROLLED_CONCEPT_LAYOUT_DEFINITION};
    // -------- Definition Meta-Data -------- //// -------- Definition Meta-Data -------- //
    
    public T8PropertyLayoutDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.HEADER_LAYOUT_TYPE.toString(), "Header Layout Type", "The layout type to apply to the property header.", PropertyHeaderLayoutType.LEFT.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.HEADER_TEXT_TYPE.toString(), "Header Text Type", "The type of text to display as the property header.", PropertyHeaderTextType.TERM.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(T8DefinitionDatumType.definitionType(Datum.CONTROLLED_CONCEPT_LAYOUT_DEFINITION.toString(), "Controlled Concept Layout", "The layout definition for controlled concept types."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.HEADER_LAYOUT_TYPE.toString().equals(datumIdentifier)) return createStringOptions(PropertyHeaderLayoutType.values());
        else if (Datum.HEADER_TEXT_TYPE.toString().equals(datumIdentifier)) return createStringOptions(PropertyHeaderTextType.values());
        else if (Datum.CONTROLLED_CONCEPT_LAYOUT_DEFINITION.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8ControlledConceptLayoutDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }
    
    public PropertyHeaderLayoutType getHeaderLayoutType()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.HEADER_LAYOUT_TYPE.toString());
        return value != null ? PropertyHeaderLayoutType.valueOf(value) : PropertyHeaderLayoutType.LEFT;
    }
    
    public void setHeaderLayoutType(PropertyHeaderLayoutType layoutType)
    {
        setDefinitionDatum(Datum.HEADER_LAYOUT_TYPE.toString(), layoutType.toString());
    }
    
    public PropertyHeaderTextType getHeaderTextType()
    {
        String value;
        
        value = (String)getDefinitionDatum(Datum.HEADER_TEXT_TYPE.toString());
        return value != null ? PropertyHeaderTextType.valueOf(value) : PropertyHeaderTextType.TERM;
    }
    
    public void setHeaderTextType(PropertyHeaderTextType textType)
    {
        setDefinitionDatum(Datum.HEADER_TEXT_TYPE.toString(), textType.toString());
    }
    
    public T8ControlledConceptLayoutDefinition getControlledConceptLayoutDefinition()
    {
        return (T8ControlledConceptLayoutDefinition)getDefinitionDatum(Datum.CONTROLLED_CONCEPT_LAYOUT_DEFINITION.toString());
    }
    
    public void setControlledConceptLayoutDefinition(T8ControlledConceptLayoutDefinition definition)
    {
        setDefinitionDatum(Datum.CONTROLLED_CONCEPT_LAYOUT_DEFINITION.toString(), definition);
    }
}
