package com.pilog.t8.data.document.datafile;

import com.pilog.t8.data.document.datarecord.DataRecord;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileStateChange
{
    private DataRecord fromState;
    private DataRecord toState;

    public T8DataFileStateChange(DataRecord fromState, DataRecord toState)
    {
        evaluateStateChange(fromState, toState);
    }

    private void evaluateStateChange(DataRecord fromStateFile, DataRecord toStateFile)
    {
        this.fromState = fromStateFile;
        this.toState = toStateFile;
        if (fromStateFile == null)
        {

        }
        else if (toStateFile == null)
        {

        }
        else
        {

        }
    }
}
