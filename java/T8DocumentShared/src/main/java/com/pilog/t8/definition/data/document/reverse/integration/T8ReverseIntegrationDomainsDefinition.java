package com.pilog.t8.definition.data.document.reverse.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Willem De Bruyn
 */
public class T8ReverseIntegrationDomainsDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_REVERSE_INTEGRATION_SERVICE_DOMAIN";
    public static final String GROUP_IDENTIFIER = "@DG_REVERSE_INTEGRATION_SERVICE_DOMAIN";
    public static final String IDENTIFIER_PREFIX = "XFACE_REV_DOM_";    
    public static final String DISPLAY_NAME = "Reverse Intergation Domain Definition";
    public static final String DESCRIPTION = "A definition that specifies the detail involved in an reverse interface domain.";
    
    
    public enum Datum
    {
        DOMAIN_IDENTIFIER,
        OPERATIONS
    }
    // -------- Definition Meta-Data -------- //
    
    public T8ReverseIntegrationDomainsDefinition(String identifier)
    {
        super(identifier);
    }
    
    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DOMAIN_IDENTIFIER.toString(), "Domain Identifier", "The domain identifier."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.OPERATIONS.toString(), "Operations",  "This will define the operations."));
        return datumTypes;
    }
    
    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.OPERATIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8ReverseIntegrationOperationDefinition.GROUP_IDENTIFIER));
        else return null;
    }
    
    @Override
    public void initializeDefinition(T8Context tsc1, Map<String, Object> map, Map<String, Object> map1) throws Exception
    {
    }
    
    public String getDomainIdentifier()
    {
        return (String)getDefinitionDatum(Datum.DOMAIN_IDENTIFIER.toString());
    }
    
    public void setDomainIdentifier(String domainIdentifier)
    {
        setDefinitionDatum(Datum.DOMAIN_IDENTIFIER.toString(), domainIdentifier);
    }
    
    public List<T8ReverseIntegrationOperationDefinition> getOperationDefinitions()
    {
        return (List<T8ReverseIntegrationOperationDefinition>) getDefinitionDatum(Datum.OPERATIONS.toString());
    }
    
    public void setOperationDefinition(List<T8ReverseIntegrationOperationDefinition> definitions)
    {
        setDefinitionDatum(Datum.OPERATIONS.toString(), definitions);
    }
}
