package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.utilities.cache.LRUCache;

/**
 * @author Bouwer du Preez
 */
public class T8LRUCachedDataRequirementInstanceProvider implements CachedDataRequirementInstanceProvider
{
    private final LRUCache<String, DataRequirementInstance> drInstanceCache;
    private final DataRequirementInstanceProvider drInstanceProvider;

    public T8LRUCachedDataRequirementInstanceProvider(DataRequirementInstanceProvider drInstanceProvider, int size)
    {
        this.drInstanceCache = new LRUCache<>(size);
        this.drInstanceProvider = drInstanceProvider;
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance(String drInstanceID, String languageID, boolean includeOntology, boolean includeTerminology)
    {
        if (drInstanceCache.containsKey(drInstanceID))
        {
            return drInstanceCache.get(drInstanceID);
        }
        else
        {
            DataRequirementInstance drInstance;

            drInstance = drInstanceProvider.getDataRequirementInstance(drInstanceID, languageID, includeOntology, includeTerminology);
            drInstanceCache.put(drInstanceID, drInstance);
            return drInstance;
        }
    }

    @Override
    public void clearCache()
    {
        drInstanceCache.clear();
    }
}
