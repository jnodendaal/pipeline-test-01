package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class DateValueContent extends ValueContent
{
    private String date;

    public DateValueContent()
    {
        super(RequirementType.DATE_TYPE);
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    @Override
    public boolean isEqualTo(ValueContent toContent)
    {
        if (toContent instanceof DateValueContent)
        {
            DateValueContent toDateContent;

            toDateContent = (DateValueContent)toContent;
            return Objects.equals(date, toDateContent.getDate());
        }
        else return false;
    }

    @Override
    public boolean isEquivalentTo(ValueContent toContent)
    {
        if (toContent instanceof DateValueContent)
        {
            DateValueContent toDateContent;

            toDateContent = (DateValueContent)toContent;
            return Objects.equals(date, toDateContent.getDate());
        }
        else return false;
    }
}
