package com.pilog.t8.data.document.datarecord.access;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordAccessHandler
{
    public String getIdentifier();

    public void setInitialState(DataRecord dataRecord); // Sets the initial state of the document i.e. set default values, remove non-visible date etc.
    public void setStaticState(DataRecord dataRecord); // Sets the static state of the document i.e. the access rules that will not change during record udpates.
    public void refreshDynamicState(DataRecord dataRecord); // Refreshes the state of the document based on its current state (this is called every time the record's content changes).
    public T8DataFileValidationReport validateState(DataRecord dataRecord); // Validates the state of the data record.
}
