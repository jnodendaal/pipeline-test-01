package com.pilog.t8.definition.data.document.integration.operation;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.document.integration.T8DataRecordFlagMappingDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceOperationMappingDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class T8IntegrationSetFlagsOperationDefinition extends T8IntegrationServiceOperationMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTEGRATION_SERVICE_OPERATION_MAPPING_SET_FLAGS";
        public static final String DISPLAY_NAME = "Set Flag Operation";
    public static final String DESCRIPTION = "An operation mapping that defines the Flags operation";

    public enum Datum
    {
        FLAG_MAPPING_IDENTIFIER
    };
// -------- Definition Meta-Data -------- //
    public T8IntegrationSetFlagsOperationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    protected List<T8DataParameterDefinition> getOperationInputParameters()
    {
        List<T8DataParameterDefinition> parameterDefinitions;

        parameterDefinitions = new ArrayList<>();
        parameterDefinitions.add(new T8DataParameterDefinition(P_DATA_RECORDS, "Data Records", "A List containing the data records", new T8DtList(T8DataType.CUSTOM_OBJECT)));
        parameterDefinitions.add(new T8DataParameterDefinition(P_OPERATION_IID, "Operation Instance ID", "The Call and Call Back unique Operation ID.", T8DataType.GUID));
        parameterDefinitions.add(new T8DataParameterDefinition(P_INPUT_PARAMETERS, "Input Paramters", "Input Parameters list that will be available in the Integration service.", new T8DtMap(T8DataType.STRING, T8DataType.CUSTOM_OBJECT)));

        return parameterDefinitions;
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FLAG_MAPPING_IDENTIFIER.toString(), "Flag Mapping Identifier", "The flag mapping that will be used for this operation."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {
        if(Datum.FLAG_MAPPING_IDENTIFIER.toString().equals(datumIdentifier)) return createLocalIdentifierOptionsFromDefinitions(getLocalDefinitionsOfType(T8DataRecordFlagMappingDefinition.TYPE_IDENTIFIER));
         return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public String getFlagMappingIdentifier()
    {
        return (String)getDefinitionDatum(Datum.FLAG_MAPPING_IDENTIFIER.toString());
    }


    public void setFlagMappingIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.FLAG_MAPPING_IDENTIFIER.toString(), identifier);
    }

}
