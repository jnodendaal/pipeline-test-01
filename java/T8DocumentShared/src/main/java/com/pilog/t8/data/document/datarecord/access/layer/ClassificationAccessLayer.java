package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class ClassificationAccessLayer implements Serializable
{
    private final String ontologyStructureID;
    private boolean editable;
    private boolean visible;
    private boolean required;
    private boolean insertEnabled;
    private boolean updateEnabled;
    private boolean deleteEnabled;
    private final Set<String> filterOntologClassIDSet;
    private DataRecordAccessLayer parentAccessLayer;

    public ClassificationAccessLayer(String ontologyStructureID)
    {
        this.ontologyStructureID = ontologyStructureID;
        this.editable = true;
        this.visible = true;
        this.required = false;
        this.insertEnabled = true;
        this.updateEnabled = true;
        this.deleteEnabled = true;
        this.filterOntologClassIDSet = new HashSet<>();
    }

    void setParentAccessLayer(DataRecordAccessLayer recordAccessLayer)
    {
        this.parentAccessLayer = recordAccessLayer;
    }

    public DataRecordAccessLayer getParentAccessLayer()
    {
        return parentAccessLayer;
    }

    public DataRecord getParentDataRecord()
    {
        return parentAccessLayer != null ? parentAccessLayer.getDataRecord() : null;
    }

    public String getOntologyStructureID()
    {
        return ontologyStructureID;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setEditable(boolean editable)
    {
        // Set the editability of this access layer.
        this.editable = editable;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        // Set the visibility on this access layer.
        this.visible = visible;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public boolean isInsertEnabled()
    {
        return insertEnabled;
    }

    public void setInsertEnabled(boolean insertEnabled)
    {
        this.insertEnabled = insertEnabled;
    }

    public boolean isUpdateEnabled()
    {
        return updateEnabled;
    }

    public void setUpdateEnabled(boolean updateEnabled)
    {
        this.updateEnabled = updateEnabled;
    }

    public boolean isDeleteEnabled()
    {
        return deleteEnabled;
    }

    public void setDeleteEnabled(boolean deleteEnabled)
    {
        this.deleteEnabled = deleteEnabled;
    }

    public void addFilterOntologyClassID(String ontologyClassID)
    {
        filterOntologClassIDSet.add(ontologyClassID);
    }

    public Set<String> getFilterOntologyClassIDSet()
    {
        return filterOntologClassIDSet;
    }

    public void setFilterOntologyClassIDSet(Collection<String> newOntologyClassIDSet)
    {
        this.filterOntologClassIDSet.clear();
        if (newOntologyClassIDSet != null)
        {
            this.filterOntologClassIDSet.addAll(newOntologyClassIDSet);
        }
    }

    @Override
    public String toString()
    {
        return "ClassificationAccessLayer{" + "ontologyStructureID=" + ontologyStructureID + ", editable=" + editable + ", visible=" + visible + ", required=" + required + ", insertEnabled=" + insertEnabled + ", updateEnabled=" + updateEnabled + ", deleteEnabled=" + deleteEnabled + ", filterOntologClassIDSet=" + filterOntologClassIDSet + '}';
    }
}
