package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class StandardValue implements Serializable
{
    private String id;
    private String rootOrgID;
    private String drID;
    private String drInstanceID;
    private String propertyID;
    private String fieldID;
    private String dataTypeID;
    private String value;
    private String lowerBoundValue;
    private String upperBoundValue;
    private String valueID;
    private String uomID;
    private String qomID;

    public StandardValue(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getRootOrgID()
    {
        return rootOrgID;
    }

    public void setRootOrgID(String rootOrgID)
    {
        this.rootOrgID = rootOrgID;
    }

    public String getDrID()
    {
        return drID;
    }

    public void setDrID(String drID)
    {
        this.drID = drID;
    }

    public String getDrInstanceID()
    {
        return drInstanceID;
    }

    public void setDrInstanceID(String drInstanceID)
    {
        this.drInstanceID = drInstanceID;
    }

    public String getPropertyID()
    {
        return propertyID;
    }

    public void setPropertyID(String propertyID)
    {
        this.propertyID = propertyID;
    }

    public String getFieldID()
    {
        return fieldID;
    }

    public void setFieldID(String fieldID)
    {
        this.fieldID = fieldID;
    }

    public String getDataTypeID()
    {
        return dataTypeID;
    }

    public void setDataTypeID(String dataTypeID)
    {
        this.dataTypeID = dataTypeID;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getLowerBoundValue()
    {
        return lowerBoundValue;
    }

    public void setLowerBoundValue(String lowerBoundValue)
    {
        this.lowerBoundValue = lowerBoundValue;
    }

    public String getUpperBoundValue()
    {
        return upperBoundValue;
    }

    public void setUpperBoundValue(String upperBoundValue)
    {
        this.upperBoundValue = upperBoundValue;
    }

    public String getValueID()
    {
        return valueID;
    }

    public void setValueID(String valueID)
    {
        this.valueID = valueID;
    }

    public String getUomID()
    {
        return uomID;
    }

    public void setUomID(String uomID)
    {
        this.uomID = uomID;
    }

    public String getQomID()
    {
        return qomID;
    }

    public void setQomID(String qomID)
    {
        this.qomID = qomID;
    }

    /**
     * Checks the supplied record value for equivalence to this standard value.
     * @param recordValue The record value to check.
     * @return true if the supplied record value is equivalent to this standard
     * value.
     */
    public boolean isEquivalentValue(RecordValue recordValue)
    {
        if (recordValue == null)
        {
            return false;
        }
        else
        {
            RequirementType requirementType;

            // Check the field for conformance.
            if (!recordValue.getFieldID().equals(fieldID)) return false;
            else if (!recordValue.getPropertyID().equals(propertyID)) return false;
            else if (!recordValue.getDataRequirementID().equals(drID)) return false;
            else if ((drInstanceID != null) && (!recordValue.getDataRequirementInstanceID().equals(drInstanceID))) return false;

            // Check the field value for conformance.
            requirementType = recordValue.getValueRequirement().getRequirementType();
            if (!requirementType.getID().equals(dataTypeID)) return false;
            else
            {
                // Check the conformance depending on the value data type.
                switch (requirementType)
                {
                    case ONTOLOGY_CLASS:
                    {
                        return recordValue.getValue().equals(valueID);
                    }
                    case MEASURED_NUMBER:
                    {
                        MeasuredNumber measuredNumber;

                        measuredNumber = (MeasuredNumber)recordValue;
                        if ((uomID != null) && (!uomID.equals(measuredNumber.getUomId()))) return false;
                        else if ((qomID != null) && (!qomID.equals(measuredNumber.getQomId()))) return false;
                        else if ((value != null) && (!value.equals(measuredNumber.getNumber()))) return false;
                        return true;
                    }
                    case MEASURED_RANGE:
                    {
                        MeasuredRange measuredRange;

                        measuredRange = (MeasuredRange)recordValue;
                        if ((uomID != null) && (!uomID.equals(measuredRange.getUomId()))) return false;
                        else if ((qomID != null) && (!qomID.equals(measuredRange.getQomId()))) return false;
                        else if ((lowerBoundValue != null) && (!lowerBoundValue.equals(measuredRange.getLowerBoundNumber()))) return false;
                        else if ((upperBoundValue != null) && (!upperBoundValue.equals(measuredRange.getUpperBoundNumber()))) return false;
                        return true;
                    }
                    default:
                    {
                        // The default conformance check for all values that are not handled specifically is just to compare the value variables.
                        return recordValue.getValue().equals(value);
                    }
                }
            }
        }
    }
}
