package com.pilog.t8.data.document.datastring;

import com.pilog.t8.definition.T8IdentifierUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringFormat extends T8DataStringElement
{
    private String formatId;
    private Type type;

    public enum Type
    {
        RECORD,
        PROPERTY,
        FIELD,
        DATA_TYPE
    }

    public T8DataStringFormat(String id)
    {
        this.formatId = id;
    }

    public String getDataStringFormatID()
    {
        return formatId;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    public void setType(String type)
    {
        this.type = Type.valueOf(type);
    }

    /**
     * This method assigns new id's to this element and all of its
     * descendants.
     */
    @Override
    public void assignNewIds()
    {
        super.assignNewIds();
        this.formatId = T8IdentifierUtilities.createNewGUID();
    }

    public void setRootElement(T8DataStringElement rootElement)
    {
        this.setID(rootElement.getID());
        this.setActive(rootElement.isActive());
        this.setPriority(rootElement.getPriority());
        this.setExpression(rootElement.getExpression());
        this.setPrefix(rootElement.getPrefix());
        this.setSuffix(rootElement.getSuffix());
        this.setSeparator(rootElement.getSeparator());
        this.setName(rootElement.getName());
        this.setDescription(rootElement.getDescription());
        this.setLengthRestriction(rootElement.getLengthRestriction());
        this.setTruncationType(rootElement.getTruncationType());
        this.setStrictParticleInclusion(rootElement.isStrictParticleInclusion());
        this.setDependencyElementID(rootElement.getDependencyElementID());
        this.setConditionExpression(rootElement.getConditionExpression());
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8DataStringFormat{");
        toStringBuilder.append("id=").append(this.formatId);
        toStringBuilder.append(",type=").append(this.type);
        toStringBuilder.append(",name=").append(this.getName());
        toStringBuilder.append(",description=").append(this.getDescription());
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}
