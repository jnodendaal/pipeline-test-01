package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class ControlledConceptAccessLayer extends ValueAccessLayer implements ConceptAccessLayer
{
    private boolean allowNewValue;

    public ControlledConceptAccessLayer()
    {
        super(RequirementType.CONTROLLED_CONCEPT);
        this.allowNewValue = true;
    }

    @Override
    public boolean isAllowNewValue()
    {
        return allowNewValue;
    }

    @Override
    public void setAllowNewValue(boolean allowNewValue)
    {
        this.allowNewValue = allowNewValue;
    }

    @Override
    public boolean isValueAccessEquivalent(ValueAccessLayer valueAccess)
    {
        if (valueAccess instanceof ControlledConceptAccessLayer)
        {
            ControlledConceptAccessLayer conceptAccess;

            conceptAccess = (ControlledConceptAccessLayer)valueAccess;
            if (!Objects.equals(allowNewValue, conceptAccess.isAllowNewValue())) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public ValueAccessLayer copy()
    {
        ControlledConceptAccessLayer copy;

        copy = new ControlledConceptAccessLayer();
        copy.setAllowNewValue(allowNewValue);
        return copy;
    }

    @Override
    public void setAccess(ValueAccessLayer newAccess)
    {
        ControlledConceptAccessLayer newConceptAccess;

        newConceptAccess = (ControlledConceptAccessLayer)newAccess;
        setAllowNewValue(newConceptAccess.isAllowNewValue());
    }
}
