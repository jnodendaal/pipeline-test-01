package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.path.PathRoot;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class RecordByInstanceStep extends DefaultPathStep implements PathStep
{
    public RecordByInstanceStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, DataRecordProvider recordProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, recordProvider, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof PathRoot)
        {
            return evaluateInstanceStep(ArrayLists.newArrayList(((PathRoot)input).getDataRecord()));
        }
        else if (input instanceof DataRecord)
        {
            return evaluateInstanceStep(((DataRecord)input).getSubRecords());
        }
        else if (input instanceof RecordProperty)
        {
            RecordProperty inputProperty;
            DataRecord parentRecord;

            inputProperty = (RecordProperty)input;
            parentRecord = inputProperty.getParentDataRecord();
            if (modifiers.isIndependent())
            {
                List<DataRecord> independentRecordList;
                List<String> references;

                independentRecordList = new ArrayList<>();
                references = parentRecord.getSubRecordIDsReferencedFromProperty(inputProperty.getPropertyID());
                for (String reference : references)
                {
                    DataRecord referencedRecord;

                    referencedRecord = recordProvider.getDataFileByContentRecord(reference, null, true, false, false, false, false, false);
                    if (referencedRecord != null)
                    {
                        independentRecordList.add(referencedRecord);
                    }
                }

                // Now evaluate the list of independent records and return the result.
                return evaluateInstanceStep(independentRecordList);
            }
            else
            {
                return evaluateInstanceStep(parentRecord.getSubRecordsReferencedFromProperty(inputProperty.getPropertyID()));
            }
        }
        else if (input instanceof List)
        {
            List<DataRecord> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<DataRecord>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<DataRecord>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<DataRecord> evaluateInstanceStep(List<DataRecord> recordList)
    {
        // If we have valid ID's to use for narrowing of the record list do it now.
        if (idList != null)
        {
            Iterator<DataRecord> recordIterator;

            recordIterator = recordList.iterator();
            while (recordIterator.hasNext())
            {
                DataRecord nextRecord;

                nextRecord = recordIterator.next();
                if (!idList.contains(nextRecord.getDataRequirementInstance().getConceptID()))
                {
                    recordIterator.remove();
                }
            }
        }

        // Now run through the remaining recordList and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<DataRecord> recordIterator;

            // Iterator over the records and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            recordIterator = recordList.iterator();
            while (recordIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                DataRecord nextRecord;

                // Create a map containing the input parameters available to the predicate expression.
                nextRecord = recordIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("RECORD", nextRecord);
                inputParameters.put("RECORD_ID", nextRecord.getID());
                inputParameters.put("DR_ID", nextRecord.getDataRequirementID());
                inputParameters.put("DR_INSTANCE_ID", nextRecord.getDataRequirementInstanceID());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        recordIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return recordList;
    }
}
