package com.pilog.t8.data.alteration;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataAlterationImpact implements Serializable
{
    private int affectedRecordCount;
    private int deletedPropertyCount;
    private int updatedPropertyCount;
    
    public void addImpact(T8DataAlterationImpact impact)
    {
    }
    
    public int getAffectedRecordCount()
    {
        return affectedRecordCount;
    }
    
    public void setAffectedRecordCount(int affectedRecordCount)
    {
        this.affectedRecordCount = affectedRecordCount;
    }
    
    public int getDeletedPropertyCount()
    {
        return deletedPropertyCount;
    }
    
    public void setDeletedPropertyCount(int deletedPropertyCount)
    {
        this.deletedPropertyCount = deletedPropertyCount;
    }
    
    public int getUpdatedPropertyCount()
    {
        return updatedPropertyCount;
    }
    
    public void setUpdatedPropertyCount(int updatedPropertyCount)
    {
        this.updatedPropertyCount = updatedPropertyCount;
    }
    
    // Convenience method
    public void getIncrementedPropertyCount()
    {
        updatedPropertyCount++;
    }
}
