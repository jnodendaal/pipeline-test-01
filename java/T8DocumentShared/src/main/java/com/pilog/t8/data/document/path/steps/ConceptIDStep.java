package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.path.steps.DefaultPathStep;
import com.pilog.t8.data.object.T8DataObjectState;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ConceptIDStep extends DefaultPathStep implements PathStep
{
    public ConceptIDStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(((DataRecord)input).getID());
        }
        else if (input instanceof RecordProperty)
        {
            return evaluateStep(((RecordProperty)input).getPropertyID());
        }
        else if (input instanceof RecordValue)
        {
            RecordValue recordValue;

            recordValue = (RecordValue)input;
            if (recordValue.isField())
            {
                return evaluateStep(recordValue.getFieldID());
            }
            else
            {
                return evaluateStep(recordValue.getValueConceptID());
            }
        }
        else if (input instanceof DataRequirementInstance)
        {
            return evaluateStep(((DataRequirementInstance)input).getConceptID());
        }
        else if (input instanceof DataRequirement)
        {
            return evaluateStep(((DataRequirement)input).getConceptID());
        }
        else if (input instanceof PropertyRequirement)
        {
            return evaluateStep(((PropertyRequirement)input).getConceptID());
        }
        else if (input instanceof T8DataObjectState)
        {
            return evaluateStep(((T8DataObjectState)input).getConceptId());
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(String conceptID)
    {
        List<String> conceptIDList;

        // Create a list to hold the codes.
        conceptIDList = new ArrayList<String>();
        conceptIDList.add(conceptID);
        return conceptIDList;
    }
}
