package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.TextValue;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class TextRequirement extends ValueRequirement
{
    public enum TextCase {UPPER, LOWER, ANY};

    public TextRequirement()
    {
        super(RequirementType.TEXT_TYPE);
    }

    public int getMaximumStringLength()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MAXIMUM_STRING_LENGTH.toString());
        return value != null ? value.intValue() : -1;
    }

    public TextCase getTextCase()
    {
        String value;

        value = (String)getAttribute(RequirementAttribute.CASE.toString());
        return value != null ? TextCase.valueOf(value) : TextCase.ANY;
    }

    @Override
    public ArrayList<RecordValue> buildRecordValues(List<Map<String, Object>> valueData, Integer parentValueSequence)
    {
        ArrayList<RecordValue> recordValues;
        ArrayList<Map<String, Object>> dataRows;
        HashMap<String, Object> filterValues;
        PropertyRequirement propertyRequirement;
        TextValue textValue;

        propertyRequirement = getParentPropertyRequirement();
        textValue = new TextValue(this);

        filterValues = new HashMap<>();
        filterValues.put("SECTION_ID", propertyRequirement.getParentSectionRequirement().getConceptID());
        filterValues.put("PROPERTY_ID", propertyRequirement.getConceptID());
        filterValues.put("DATA_TYPE_ID", getRequirementType().getID());
        filterValues.put("DATA_TYPE_SEQUENCE", getSequence());
        filterValues.put("PARENT_VALUE_SEQUENCE", parentValueSequence);
        dataRows = CollectionUtilities.getFilteredDataRows(valueData, filterValues);

        recordValues = new ArrayList<>();
        recordValues.add(textValue);
        if (!dataRows.isEmpty())
        {
            String text;

            text = (String)dataRows.get(0).get("TEXT");
            textValue.setText(text);
        }

        // Return the constructed value.
        return recordValues;
    }
}
