package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class RecordPropertyStep extends DefaultPathStep implements PathStep
{
    public RecordPropertyStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            DataRecord inputRecord;

            inputRecord = (DataRecord)input;
            if (modifiers.isIncludeNullValues())
            {
                List<RecordProperty> propertyList;
                DataRequirement inputDr;

                propertyList = new ArrayList<>();
                inputDr = inputRecord.getDataRequirement();
                for (String propertyId : inputDr.getPropertyRequirementIDList())
                {
                    propertyList.add(inputRecord.getRecordProperty(propertyId));
                }

                return evaluateStep(propertyList);
            }
            else
            {
                return evaluateStep(inputRecord.getRecordProperties());
            }
        }
        else if (input instanceof List)
        {
            List<RecordProperty> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<RecordProperty>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<RecordProperty>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Property step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<RecordProperty> evaluateStep(List<RecordProperty> propertyList)
    {
        // If we have valid ID's to use for narrowing of the property list do it now.
        if (idList != null)
        {
            Iterator<RecordProperty> propertyIterator;

            propertyIterator = propertyList.iterator();
            while (propertyIterator.hasNext())
            {
                RecordProperty nextProperty;

                nextProperty = propertyIterator.next();
                if (nextProperty != null)
                {
                    if (!idList.contains(nextProperty.getPropertyID()))
                    {
                        propertyIterator.remove();
                    }
                }
            }
        }

        // Now run through the remaining property list and evaluate the predicate expression (if any).
        if (predicateExpression != null)
        {
            Iterator<RecordProperty> propertyIterator;

            // Iterator over the properties and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            propertyIterator = propertyList.iterator();
            while (propertyIterator.hasNext())
            {
                RecordProperty nextProperty;

                // Create a map containing the input parameters available to the predicate expression.
                nextProperty = propertyIterator.next();
                if (nextProperty != null)
                {
                    Map<String, Object> inputParameters;

                    inputParameters = getPredicateExpressionParameters(nextProperty.getPropertyRequirement().getAttributes());
                    inputParameters.put("PROPERTY_ID", nextProperty.getPropertyID());

                    // Evaluate the predicate.
                    try
                    {
                        if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                        {
                            propertyIterator.remove();
                        }
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                    }
                }
                else
                {
                    // If any predicate expression is specified, it cannot be satisfied by a null value and therefore we remove the null from the output list.
                    propertyIterator.remove();
                }
            }
        }

        // Return the results.
        return propertyList;
    }
}
