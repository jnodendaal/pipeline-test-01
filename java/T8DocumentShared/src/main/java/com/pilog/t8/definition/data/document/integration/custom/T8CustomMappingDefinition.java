package com.pilog.t8.definition.data.document.integration.custom;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.data.document.integration.custom.property.T8CustomPropertyMappingDefinition;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.definition.data.document.integration.T8DataRecordPropertyMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8CustomMappingDefinition extends T8DataRecordPropertyMappingDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_RECORD_CUSTOM_PROPERTY";
    public static final String DISPLAY_NAME = "Custom Property Mapping";
    public static final String DESCRIPTION = "A Custom Data Record property mapping that defines how properties for this record will be mapped";

    public enum Datum
    {
        SPLIT_TYPE,
        SEPARATOR,
        PROPERTY_LIST,
        FIELD_LIST
    };
    // -------- Definition Meta-Data -------- //

    public static enum SplitTypes
    {
        CHARACTER,
        WORD
    }

    public T8CustomMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SPLIT_TYPE.toString(), "Split Type", "This setting will let the value string either be split per character or word.", SplitTypes.CHARACTER.toString(), T8DefinitionDatumOptionType.ENUMERATION));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.SEPARATOR.toString(), "Separator", "The Property Value String separator."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PROPERTY_LIST.toString(), "Property List", "The property list build up the concatenated string value."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_LIST.toString(), "Field List", "The field list the concatenated string value is mapped to."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (T8CustomMappingDefinition.Datum.SPLIT_TYPE.toString().equals(datumIdentifier))
        {
            return createStringOptions(T8CustomMappingDefinition.SplitTypes.values());
        }
        if (Datum.PROPERTY_LIST.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8CustomPropertyMappingDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.FIELD_LIST.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8CustomFieldMappingDefinition.GROUP_IDENTIFIER));
        }
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public <T> DataRecordIntegrationPropertyMapper<T> createNewPropertyMapperInstance(T8Context context)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.integration.property.mapping.CustomPropertyMapping").getConstructor(T8Context.class, T8CustomMappingDefinition.class);
            return (DataRecordIntegrationPropertyMapper<T>) constructor.newInstance(context, this);
        }
        catch(Exception ex)
        {
            T8Log.log("Failed to create new property mapper instance", ex);
            return null;
        }
    }

    public String getSplitType()
    {
        return (String)getDefinitionDatum(Datum.SPLIT_TYPE.toString());
    }

    public void setSplitType(String splitType)
    {
        setDefinitionDatum(Datum.SPLIT_TYPE.toString(), splitType);
    }
    public String getSeparator()
    {
        return (String)getDefinitionDatum(Datum.SEPARATOR.toString());
    }

    public void setSeparator(String separator)
    {
        setDefinitionDatum(Datum.SEPARATOR.toString(), separator);
    }

    public List<T8CustomPropertyMappingDefinition> getPropertyList()
    {
        return (List<T8CustomPropertyMappingDefinition>)getDefinitionDatum(Datum.PROPERTY_LIST.toString());
    }

    public void setPropertyList(List<T8CustomPropertyMappingDefinition> propertyList)
    {
        setDefinitionDatum(Datum.PROPERTY_LIST.toString(), propertyList);
    }

    public List<T8CustomFieldMappingDefinition> getFieldList()
    {
        return (List<T8CustomFieldMappingDefinition>)getDefinitionDatum(Datum.FIELD_LIST.toString());
    }

    public void setFieldList(List<T8CustomFieldMappingDefinition> fieldList)
    {
        setDefinitionDatum(Datum.FIELD_LIST.toString(), fieldList);
    }
}
