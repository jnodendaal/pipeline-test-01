package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarecord.DataRecord;

/**
 * @author Bouwer du Preez
 */
public class T8DtDataRecord extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@DATA_RECORD";

    public T8DtDataRecord()
    {
    }

    public T8DtDataRecord(T8DefinitionManager context)
    {
    }

    public T8DtDataRecord(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return DataRecord.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            DataRecord record;

            record = (DataRecord)object;
            return T8DocumentSerializer.serializeDataRecord(record, true, true, true);
        }
        else return JsonValue.NULL;
    }

    @Override
    public Object deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            try
            {
                return T8DocumentSerializer.deserializeDataRecord((JsonObject)jsonValue);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deserializing data record.", e);
            }
        }
        else return null;
    }
}
