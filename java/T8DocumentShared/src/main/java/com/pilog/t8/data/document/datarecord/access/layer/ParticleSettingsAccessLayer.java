package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class ParticleSettingsAccessLayer implements Serializable
{
    private String orgId;
    private String languageId;
    private String dsTypeId;
    private boolean editable;
    private boolean visible;
    private boolean required;
    private DataRecordAccessLayer parentAccessLayer;

    public ParticleSettingsAccessLayer(DataRecordAccessLayer parentAccessLayer)
    {
        this.parentAccessLayer = parentAccessLayer;
        this.editable = true;
        this.visible = true;
        this.required = false;
    }

    void setParentAccessLayer(DataRecordAccessLayer recordAccessLayer)
    {
        this.parentAccessLayer = recordAccessLayer;
    }

    public DataRecordAccessLayer getParentAccessLayer()
    {
        return parentAccessLayer;
    }

    public DataRecord getParentDataRecord()
    {
        return parentAccessLayer != null ? parentAccessLayer.getDataRecord() : null;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setEditable(boolean editable)
    {
        // Set the editability of this access layer.
        this.editable = editable;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        // Set the visibility on this access layer.
        this.visible = visible;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getRecordId()
    {
        return parentAccessLayer.getRecordId();
    }

    public String getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(String languageId)
    {
        this.languageId = languageId;
    }

    public String getDsTypeId()
    {
        return dsTypeId;
    }

    public void setDsTypeId(String dsTypeId)
    {
        this.dsTypeId = dsTypeId;
    }

    /**
     * Checks whether or not this access layer is applicable to the input particle settings object.
     * Applicability is determined by comparing the following values of the access layer with the settings object:
     *
     * recordId
     * propertyId
     * fieldId
     * dataTypeId
     * languageId
     * dsTypeId
     * orgId
     *
     * @param settings The settings object to check.
     * @return Boolean true if this access layer is applicable to the input particle settings.
     */
    public boolean isApplicableTo(T8DataStringParticleSettings settings)
    {
        if (settings == null)
        {
            return false;
        }
        else
        {
            String recordId;

            recordId = getRecordId();
            if (!Objects.equals(recordId, settings.getRecordId())) return false;
            else if (!Objects.equals(orgId, settings.getOrgId())) return false;
            else if (!Objects.equals(languageId, settings.getLanguageId())) return false;
            else if (!Objects.equals(dsTypeId, settings.getDsTypeId())) return false;
            else return true;
        }
    }

    @Override
    public String toString()
    {
        return "ParticleSettingsAccessLayer{" + "orgId=" + orgId + ", recordId=" + getRecordId() + ", languageId=" + languageId + ", dsTypeId=" + dsTypeId + ", editable=" + editable + ", visible=" + visible + ", required=" + required + '}';
    }
}
