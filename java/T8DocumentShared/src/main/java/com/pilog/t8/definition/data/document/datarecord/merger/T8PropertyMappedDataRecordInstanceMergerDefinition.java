/**
 * Created on Jun 28, 2017, 9:00:42 AM
 */
package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordInstanceMerger;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8PropertyMappedDataRecordInstanceMergerDefinition extends T8DefaultDataRecordInstanceMergerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_INSTANCE_MERGER_PROP_MAP";
    public static final String DISPLAY_NAME = "Property Mapped Record Instance Merger";
    public static final String DESCRIPTION = "A definition that specifies merging rules applicable to a data record of a specific instance type, allowing for specialized property mappings.";
    public enum Datum { PROPERTY_ID_MAPPING};
    // -------- Definition Meta-Data -------- //

    public T8PropertyMappedDataRecordInstanceMergerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(new T8DtMap(T8DataType.STRING, T8DataType.STRING), Datum.PROPERTY_ID_MAPPING.toString(), "Property Mapping: Source Property ID to Destination Property ID", "Property ID mapping between source and destination. Default behaviour apply to any properties not listed."));

        return datumTypes;
    }

    @Override
    public T8DataRecordInstanceMerger getNewMergerInstance(T8DataRecordMergeContext mergeContext)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.data.document.datarecord.merger.T8PropertyMappedDataRecordInstanceMerger", new Class<?>[]{this.getClass(), T8DataRecordMergeContext.class}, this, mergeContext);
    }

    public Map<String, String> getPropertyIdMapping()
    {
        return getDefinitionDatum(Datum.PROPERTY_ID_MAPPING);
    }

    public void setPropertyIdMapping(Map<String, String> propertyIdMapping)
    {
        setDefinitionDatum(Datum.PROPERTY_ID_MAPPING, propertyIdMapping);
    }
}