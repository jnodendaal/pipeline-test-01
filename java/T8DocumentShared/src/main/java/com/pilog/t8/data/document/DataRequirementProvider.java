package com.pilog.t8.data.document;

import com.pilog.t8.data.document.datarequirement.DataRequirement;

/**
 * @author Bouwer du Preez
 */
public interface DataRequirementProvider
{
    public DataRequirement getDataRequirement(String drId, String languageId, boolean includeOntology, boolean includeTerminology);
}
