package com.pilog.t8.definition.org;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.data.model.T8DataModelDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordAttachmentHandlerDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationSetupDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_ORGANIZATION_SETUP";
    public static final String GROUP_NAME = "Organization Setup";
    public static final String GROUP_DESCRIPTION = "Settings that are specific to an organization context.";
    public static final String TYPE_IDENTIFIER = "@DT_ORGANIZATION_SETUP";
    public static final String STORAGE_PATH = "/organization_setups";
    public static final String IDENTIFIER_PREFIX = "ORG_SETUP_";
    public static final String DISPLAY_NAME = "Organization Setup";
    public static final String DESCRIPTION = "A definition that specifies various settings for a specific organization.";
    public enum Datum {ORGANIZATION_ID_LIST,
                       DEFAULT_CONTENT_LANGUAGE_IDENTIFIER,
                       DATA_MODEL_IDENTIFIER,
                       ATTACHMENT_HANDLER_IDENTIFIER,
                       DATA_RECORD_MATCHER_IDENTIFIER,
                       DATA_RECORD_MATCHER_SERVICE_IDENTIFIER,
                       ORGANIZATION_OPERATIONAL_SETTINGS_IDENTIFIER};
    // -------- Definition Meta-Data -------- //

    public static final String RESOLUTION_PARAMETER_ORG_ID = "$P_ORG_ID";

    // These identfiers are hard-coded instead of being imported in order to
    // prevent a cyclic project dependency.  This will be resolved in future
    // when this setup definition has been moved to a proper location.
    public static final String RECORD_MATCHER_DEFINITION_GROUP_IDENTIFIER = "@DG_DATA_RECORD_MATCHER";

    public T8OrganizationSetupDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(new T8DtList(T8DataType.GUID), Datum.ORGANIZATION_ID_LIST.toString(), "Organizations", "The list of organizations to which this setup definition is applicable.  If left empty, this definition will be applied to all organization for which no other more specific setup can be found."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.GUID, Datum.DEFAULT_CONTENT_LANGUAGE_IDENTIFIER.toString(), "Default Content Langauge", "The default content language to use when no other more specific setting is available."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_MODEL_IDENTIFIER.toString(), "Data Model", "The data model to use for this organization's data."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ATTACHMENT_HANDLER_IDENTIFIER.toString(), "Attachment Handler", "The attachment handler to use for storing data record attachments created within organization to which this setup definition is applicable."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_MATCHER_IDENTIFIER.toString(), "Record Matcher", "The matcher to use when comparing data records."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_RECORD_MATCHER_SERVICE_IDENTIFIER.toString(), "Record Matcher Service", "The matcher service to use."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.ORGANIZATION_OPERATIONAL_SETTINGS_IDENTIFIER.toString(), "Organization Operational Settings", "The setup defining the organization operational settings."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ATTACHMENT_HANDLER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataRecordAttachmentHandlerDefinition.GROUP_IDENTIFIER));
        else if (Datum.DATA_RECORD_MATCHER_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), RECORD_MATCHER_DEFINITION_GROUP_IDENTIFIER), true, "Undefined");
        else if (Datum.DATA_RECORD_MATCHER_SERVICE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ServiceDefinition.GROUP_IDENTIFIER), true, "Undefined");
        else if (Datum.DATA_MODEL_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataModelDefinition.GROUP_IDENTIFIER));
        else if (Datum.ORGANIZATION_OPERATIONAL_SETTINGS_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8OrganizationOperationalSettingsDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    public static T8OrganizationSetupDefinition getOrganizationSetupDefinition(T8Context context) throws Exception
    {
        T8OrganizationSetupDefinition organizationSetupDefinition;

        organizationSetupDefinition = (T8OrganizationSetupDefinition)context.getServerContext().getDefinitionManager().getResolvedDefinition(context, T8OrganizationSetupDefinition.TYPE_IDENTIFIER, null);
        return organizationSetupDefinition;
    }

    public List<String> getOrganizationIDList()
    {
        return getDefinitionDatum(Datum.ORGANIZATION_ID_LIST);
    }

    public void setOrganizationIDList(List<String> organizationIDList)
    {
        setDefinitionDatum(Datum.ORGANIZATION_ID_LIST, organizationIDList);
    }

    public String getDefaultContentLanguageIdentifier()
    {
        return getDefinitionDatum(Datum.DEFAULT_CONTENT_LANGUAGE_IDENTIFIER);
    }

    public void setDefaultContentLanguageIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DEFAULT_CONTENT_LANGUAGE_IDENTIFIER, identifier);
    }

    public String getDataModelIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_MODEL_IDENTIFIER);
    }

    public void setDataModelIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_MODEL_IDENTIFIER, identifier);
    }

    public String getAttachmentHandlerIdentifier()
    {
        return getDefinitionDatum(Datum.ATTACHMENT_HANDLER_IDENTIFIER);
    }

    public void setAttachmentHandlerIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ATTACHMENT_HANDLER_IDENTIFIER, identifier);
    }

    public String getDataRecordMatcherIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_RECORD_MATCHER_IDENTIFIER);
    }

    public void setDataRecordMatcherIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_MATCHER_IDENTIFIER, identifier);
    }

    public String getDataRecordMatcherServiceIdentifier()
    {
        return getDefinitionDatum(Datum.DATA_RECORD_MATCHER_SERVICE_IDENTIFIER);
    }

    public void setDataRecordMatcheServicerIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.DATA_RECORD_MATCHER_SERVICE_IDENTIFIER, identifier);
    }

    public String getOrganizationOperationalSettingsIdentifier()
    {
        return getDefinitionDatum(Datum.ORGANIZATION_OPERATIONAL_SETTINGS_IDENTIFIER);
    }

    public void setOrganizationOperationalSettingsIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.ORGANIZATION_OPERATIONAL_SETTINGS_IDENTIFIER, identifier);
    }
}
