package com.pilog.t8.definition.data.source.datarequirement;

/**
 *
 * @author Pieter Strydom
 */
public class T8DataRequirementDataSourceResourceDefinition extends T8DataRequirementDataSourceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_SOURCE_DATA_REQUIREMENT_RESOURCE";
    public static final T8DefinitionLevel DEFINITION_LEVEL = T8DefinitionLevel.RESOURCE;
    // -------- Definition Meta-Data -------- //

    public T8DataRequirementDataSourceResourceDefinition(String identifier)
    {
        super(identifier);
    }
}