package com.pilog.t8.data.document.datarecord.structure;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class DataRecordStructure implements Serializable
{
    private DataRecordStructureNode rootNode;

    public DataRecordStructure()
    {
    }
    
    public DataRecordStructure(DataRecordStructureNode rootNode)
    {
        this.rootNode = rootNode;
    }

    public DataRecordStructureNode getRootNode()
    {
        return rootNode;
    }

    public void setRootNode(DataRecordStructureNode rootNode)
    {
        this.rootNode = rootNode;
    }

    public void setRootRecord(DataRecord rootRecord)
    {
        this.rootNode = createNewNode(rootRecord);
    }
    
    /**
     * This method applies the current structure to the records it contains,
     * setting each record's parent and sub-records.
     */
    public void applyStructureToRecords()
    {
        if (rootNode != null) rootNode.applyStructureToRecords();
    }

    public List<DataRecord> setRecords(String rootRecordID, Collection<DataRecord> dataRecords)
    {
        DataRecord root;

        root = null;
        for (DataRecord record : dataRecords)
        {
            if (record.getID().equals(rootRecordID))
            {
                root = record;
                break;
            }
        }

        // Make sure we found the root.
        if (root != null)
        {
            List<DataRecord> recordsToAdd;

            // Set the new root.
            setRootRecord(root);

            // Make sure to remove the root from the list of records to add (the root has already been added).
            recordsToAdd = new ArrayList<DataRecord>(dataRecords);
            recordsToAdd.remove(root);
            return addRecords(recordsToAdd);
        }
        else throw new IllegalArgumentException("Root '" + rootRecordID + "' not found in supplied list of records.");
    }

    public List<DataRecord> addRecords(List<DataRecord> dataRecords)
    {
        LinkedList<DataRecord> recordsToAdd;

        // This method requires that a root node must exist.
        if (rootNode == null) throw new RuntimeException("Data Records cannot be added to a structure without a root node.");
        
        recordsToAdd = new LinkedList<DataRecord>(dataRecords);
        while (recordsToAdd.size() > 0)
        {
            List<DataRecordStructureNode> parentNodes;
            Set<DataRecord> recordsAdded;
            boolean parentFound;

            // Find all nodes in the structure to which child nodes can be added.
            parentFound = false;
            parentNodes = findPossibleParentNodes();
            
            // For each parent, try to find a new child.
            recordsAdded = new HashSet<DataRecord>();
            for (DataRecordStructureNode possibleParent : parentNodes)
            {
                // Check each of the available nodes to see if they belong to the parent.
                for (DataRecord recordToAdd : recordsToAdd)
                {
                    Set<String> possibleSubRecordIDSet;
                    String recordID;
                    
                    // If the record belongs to the parent, add it.
                    recordID = recordToAdd.getID();
                    possibleSubRecordIDSet = possibleParent.getPossibleSubRecordIDSet();
                    if (possibleSubRecordIDSet.contains(recordID))
                    {
                        DataRecordStructureNode newNode;

                        // Create the new node.
                        newNode = createNewNode(recordToAdd);
                        possibleParent.addChildNode(newNode);
                        recordsAdded.add(recordToAdd);
                        parentFound = true;
                    }
                }
            }
            
            // Remove the records added, from the collection still to be added.
            recordsToAdd.removeAll(recordsAdded);

            // If we could not add any more nodes to parents, break the loop.
            if (!parentFound) break;
        }

        // Return a list of records not added.
        return recordsToAdd;
    }
    
    public List<DataRecordStructureNode> findPossibleParentNodes()
    {
        return rootNode != null ? rootNode.findPossibleParentNodes() : new ArrayList<DataRecordStructureNode>();
    }

    public List<DataRecordStructureNode> findPossibleParentNodes(String childRecordID)
    {
        return rootNode != null ? rootNode.findPossibleParentNodes(childRecordID) : new ArrayList<DataRecordStructureNode>();
    }

    public List<DataRecordStructureNode> findParentNodes(String childRecordID)
    {
        return rootNode != null ? rootNode.findParentNodes(childRecordID) : new ArrayList<DataRecordStructureNode>();
    }

    public boolean removeNode(DataRecordStructureNode node)
    {
        DataRecordStructureNode parentNode;

        parentNode = node.getParentNode();
        if (parentNode != null)
        {
            return parentNode.removeChildNode(node);
        }
        else if (node == rootNode)
        {
            setRootNode(null);
            return true;
        }
        else return false;
    }

    public DataRecordStructureDFIterator getDepthFirstIterator()
    {
        return new DataRecordStructureDFIterator(this);
    }

    /**
     * This method creates a new Instance of The Data Record Structure Node. This method exists to allow sub classes to provided their own implementations.
     * @param dataRecord The data record that must be used to create the new instance.
     * @return  An new instance of DataRecordStructureNode containing the provided data record.
     */
    protected DataRecordStructureNode createNewNode(DataRecord dataRecord)
    {
        return new DataRecordStructureNode(dataRecord);
    }
}
