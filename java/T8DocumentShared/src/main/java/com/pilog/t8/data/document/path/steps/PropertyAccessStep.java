package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class PropertyAccessStep extends DefaultPathStep implements PathStep
{
    public PropertyAccessStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecordAccessLayer)
        {
            return evaluatePropertyStep(((DataRecordAccessLayer)input).getPropertyAccessLayers());
        }
        else if (input instanceof SectionAccessLayer)
        {
            return evaluatePropertyStep(((SectionAccessLayer)input).getPropertyAccessLayers());
        }
        else if (input instanceof DataRecord)
        {
            return evaluatePropertyStep(((DataRecord)input).getAccessLayer().getPropertyAccessLayers());
        }
        else if (input instanceof List)
        {
            List<PropertyAccessLayer> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<PropertyAccessLayer>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<PropertyAccessLayer>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Property Access step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<PropertyAccessLayer> evaluatePropertyStep(List<PropertyAccessLayer> accessLayerList)
    {
        // If we have ID's to use for narrowing of the list do it now.
        if (idList != null)
        {
            Iterator<PropertyAccessLayer> accessLayerIterator;

            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                PropertyAccessLayer nextAccessLayer;

                nextAccessLayer = accessLayerIterator.next();
                if (!idList.contains(nextAccessLayer.getPropertyId()))
                {
                    accessLayerIterator.remove();
                }
            }
        }

        // Now run through the remaining items in the list and evaluate the predicate expression (if any).
        if (predicateExpression != null)
        {
            Iterator<PropertyAccessLayer> accessLayerIterator;

            // Iterator over the access layers and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            accessLayerIterator = accessLayerList.iterator();
            while (accessLayerIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                PropertyAccessLayer nextAccessLayer;

                // Create a map containing the input parameters available to the predicate expression.
                nextAccessLayer = accessLayerIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("PROPERTY_ID", nextAccessLayer.getPropertyId());

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        accessLayerIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        // Return the results.
        return accessLayerList;
    }
}
