package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordInstanceMerger;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordInstanceMergerDefinition extends T8DataRecordInstanceMergerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_INSTANCE_MERGER_DEFAULT";
    public static final String DISPLAY_NAME = "Default Record Instance Merger";
    public static final String DESCRIPTION = "A definition that specifies merging rules applicable to a data record of a specific instance type.";
    public enum Datum {DR_INSTANCE_ID,
                       MERGE_ONTOLOGY_CODES,
                       PROPERTY_MERGER_DEFINITIONS,
                       FIELD_MERGER_DEFINITIONS,
                       VALUE_MERGER_DEFINITIONS};
    // -------- Definition Meta-Data -------- //

    public T8DefaultDataRecordInstanceMergerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.DR_INSTANCE_ID.toString(), "Data Requirement Instance", "The Data Requirement Instance to which this merge configuration definition applies."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MERGE_ONTOLOGY_CODES.toString(), "Merge Ontology Codes", "If set to true, the ontology codes of records will be merged.", false));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PROPERTY_MERGER_DEFINITIONS.toString(), "Property Merge Configurations",  "The property merge configuration definitions applicable to this DR Instance."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_MERGER_DEFINITIONS.toString(), "Field Mergers",  "The Field merger definitions applicable within the context of this context."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.VALUE_MERGER_DEFINITIONS.toString(), "Value Mergers",  "The Value merger definitions applicable within the context of this context."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.PROPERTY_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordPropertyMergerDefinition.GROUP_IDENTIFIER));
        else if (Datum.FIELD_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordFieldMergerDefinition.GROUP_IDENTIFIER));
        else if (Datum.VALUE_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordValueMergerDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public T8DataRecordInstanceMerger getNewMergerInstance(T8DataRecordMergeContext mergeContext)
    {
        return T8Reflections.getFastFailInstance("com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordInstanceMerger", new Class<?>[]{this.getClass(), T8DataRecordMergeContext.class}, this, mergeContext);
    }

    public String getDataRequirementInstanceID()
    {
        return getDefinitionDatum(Datum.DR_INSTANCE_ID);
    }

    public void setDataRequirementInstanceID(String drInstanceID)
    {
        setDefinitionDatum(Datum.DR_INSTANCE_ID, drInstanceID);
    }

    public boolean isMergeOntologyCodes()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.MERGE_ONTOLOGY_CODES));
    }

    public void setMergeOntologyCodes(boolean mergeOntologyCodes)
    {
        setDefinitionDatum(Datum.MERGE_ONTOLOGY_CODES, mergeOntologyCodes);
    }

    public List<T8DataRecordPropertyMergerDefinition> getPropertyMergerDefinitions()
    {
        return getDefinitionDatum(Datum.PROPERTY_MERGER_DEFINITIONS);
    }

    public void setPropertyMergerDefinitions(List<T8DataRecordPropertyMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.PROPERTY_MERGER_DEFINITIONS, definitions);
    }

    public List<T8DataRecordFieldMergerDefinition> getFieldMergerDefinitions()
    {
        return getDefinitionDatum(Datum.FIELD_MERGER_DEFINITIONS);
    }

    public void setFieldMergerDefinitions(List<T8DataRecordFieldMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_MERGER_DEFINITIONS, definitions);
    }

    public List<T8DataRecordValueMergerDefinition> getValueMergerDefinitions()
    {
        return getDefinitionDatum(Datum.VALUE_MERGER_DEFINITIONS);
    }

    public void setValueMergerDefinitions(List<T8DataRecordValueMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.VALUE_MERGER_DEFINITIONS, definitions);
    }
}
