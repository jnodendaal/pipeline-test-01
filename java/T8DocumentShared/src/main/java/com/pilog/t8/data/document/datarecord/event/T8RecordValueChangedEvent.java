package com.pilog.t8.data.document.datarecord.event;

import com.pilog.t8.data.document.datarecord.RecordValue;

/**
 * @author Bouwer du Preez
 */
public class T8RecordValueChangedEvent extends T8DataRecordChangedEvent
{
    private final RecordValue recordValue;
    private final Object actor;

    public T8RecordValueChangedEvent(RecordValue recordValue, Object actor)
    {
        super(recordValue.getParentDataRecord());
        this.recordValue = recordValue;
        this.actor = actor;
    }

    public RecordValue getRecordValue()
    {
        return recordValue;
    }

    public Object getActor()
    {
        return actor;
    }

    @Override
    public String toString()
    {
        return "T8RecordValueChangedEvent{" + "recordValue=" + recordValue + "}";
    }
}
