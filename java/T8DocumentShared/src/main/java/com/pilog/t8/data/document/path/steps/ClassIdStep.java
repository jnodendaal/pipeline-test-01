package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ClassIdStep extends DefaultPathStep implements PathStep
{
    public ClassIdStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            return evaluateStep(((DataRecord)input).getDataRequirement().getClassConceptID());
        }
        else if (input instanceof DataRequirementInstance)
        {
            return evaluateStep(((DataRequirementInstance)input).getDataRequirement().getClassConceptID());
        }
        else if (input instanceof DataRequirement)
        {
            return evaluateStep(((DataRequirement)input).getClassConceptID());
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Class Id step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateStep(String classConceptId)
    {
        // No evaluation to do yet for this step type.
        return ArrayLists.newArrayList(classConceptId);
    }
}
