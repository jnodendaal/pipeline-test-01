package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordFieldMerger extends T8DataRecordMergeContext
{
    public void reset();
    public Map<String, String> getRecordIdMapping();
    public Map<String, String> getAttachmentIdentifierMapping();
    public Set<DataRecord> getNewDataRecordSet();
    public Set<DataRecord> getUpdatedDataRecordSet();

    public boolean isApplicable(RecordValue destinationField, RecordValue sourceField);
    public void mergeRecordFields(RecordValue destinationField, RecordValue sourceField);
}
