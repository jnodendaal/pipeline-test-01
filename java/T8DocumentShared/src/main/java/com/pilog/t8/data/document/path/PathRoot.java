package com.pilog.t8.data.document.path;

import com.pilog.t8.data.document.datarecord.DataRecord;

/**
 * @author Bouwer du Preez
 */
public class PathRoot
{
    private final DataRecord dataRecord;
    
    public PathRoot(DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
    }
    
    public DataRecord getDataRecord()
    {
        return dataRecord;
    }
    
    @Override
    public String toString()
    {
        return "PathRoot:" + dataRecord;
    }
}
