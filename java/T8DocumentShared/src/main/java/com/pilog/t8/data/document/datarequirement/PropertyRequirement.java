package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class PropertyRequirement implements Requirement, Serializable
{
    private String conceptId;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;
    private boolean characteristic;
    private SectionRequirement parent;
    private ValueRequirement valueRequirement;
    private final LinkedHashMap<String, Object> attributes;

    public PropertyRequirement()
    {
        this.parent = null;
        this.attributes = new LinkedHashMap<>();
    }

    public PropertyRequirement(String conceptID)
    {
        this();
        this.conceptId = conceptID;
    }

    void setParentClassRequirement(SectionRequirement classRequirement)
    {
        this.parent = classRequirement;
    }

    public SectionRequirement getParentSectionRequirement()
    {
        return parent;
    }

    public DataRequirementInstance getParentDataRequirementInstance()
    {
        return parent != null ? parent.getParentDataRequirementInstance() : null;
    }

    public DataRequirement getParentDataRequirement()
    {
        return parent != null ? parent.getParentDataRequirement() : null;
    }

    public ValueRequirement getValueRequirement()
    {
        return valueRequirement;
    }

    public void setValueRequirement(ValueRequirement valueRequirement)
    {
        this.valueRequirement = valueRequirement;
        if (valueRequirement != null)
        {
            valueRequirement.setParentPropertyRequirement(this);
        }
    }

    public ArrayList<ValueRequirement> getAllValueRequirements()
    {
        ArrayList<ValueRequirement> requirements;

        requirements = new ArrayList<>();
        if (valueRequirement != null)
        {
            requirements.add(valueRequirement);
            requirements.addAll(valueRequirement.getDescendentRequirements());
        }

        return requirements;
    }

    /**
     * Returns a copy of this object and its logical descendants.  No references to logical parents are copied.
     * @return A copy of this object and its logical descendants.
     */
    public PropertyRequirement copy()
    {
        PropertyRequirement newRequirement;

        newRequirement = new PropertyRequirement(conceptId);
        newRequirement.setCharacteristic(characteristic);
        newRequirement.setAttributes(attributes);
        newRequirement.setValueRequirement(valueRequirement != null ? valueRequirement.copy() : null);
        newRequirement.setTerm(term);
        newRequirement.setDefinition(definition);
        newRequirement.setCode(code);
        newRequirement.setAbbreviation(abbreviation);
        return newRequirement;
    }

    @Override
    public RequirementType getRequirementType()
    {
        return RequirementType.PROPERTY;
    }

    public String getDataRequirementID()
    {
        DataRequirement dataRequirement;

        dataRequirement = getParentDataRequirement();
        return dataRequirement != null ? dataRequirement.getConceptID() : null;
    }

    public String getDataRequirementInstanceID()
    {
        DataRequirementInstance drInstance;

        drInstance = getParentDataRequirementInstance();
        return drInstance != null ? drInstance.getConceptID() : null;
    }

    public String getId()
    {
        return conceptId;
    }

    public String getSectionId()
    {
        return parent != null ? parent.getConceptID() : null;
    }

    public String getConceptID()
    {
        return conceptId;
    }

    public void setConceptID(String conceptID)
    {
        this.conceptId = conceptID;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    /**
     * Adds the terminology of this Property Requirement to the supplied collector.
     * @param terminologyCollector The collector to which terminology will be added.
     */
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        terminologyCollector.addTerminology(languageId, T8OntologyConceptType.PROPERTY, conceptId, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);
    }

    /**
     * Sets the terminology of this Property Requirement and all of its content
     * elements using the supplied terminology provider.
     * @param terminologyProvider The terminology provider from which to fetch this Property Requirement's
     * and its contents' terminology.
     */
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        T8ConceptTerminology terminology;

        terminology = terminologyProvider.getTerminology(null, conceptId);
        if (terminology != null)
        {
            this.languageId = terminology.getLanguageId();
            this.codeId = terminology.getCodeId();
            this.code = terminology.getCode();
            this.termId = terminology.getTermId();
            this.term = terminology.getTerm();
            this.definitionId = terminology.getDefinitionId();
            this.definition = terminology.getDefinition();
            this.abbreviationId = terminology.getAbbreviationId();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.languageId = null;
            this.codeId = null;
            this.code = null;
            this.termId = null;
            this.term = null;
            this.definitionId = null;
            this.definition = null;
            this.abbreviationId = null;
            this.abbreviation = null;
        }

        // Set the terminology of the value requirement.
        if (valueRequirement != null) valueRequirement.setContentTerminology(terminologyProvider);
    }

    public boolean isUnique()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.UNIQUE.toString()));
    }

    public boolean isCharacteristic()
    {
        return characteristic;
    }

    public void setCharacteristic(boolean required)
    {
        this.characteristic = required;
    }

    public int getIndex()
    {
        if (parent != null)
        {
            return parent.getPropertyRequirementIndex(this);
        }
        else return -1;
    }

    public int getIndexInDataRequirement()
    {
        DataRequirement parentDataRequirement;

        parentDataRequirement = getParentDataRequirement();
        return parentDataRequirement != null ? parentDataRequirement.getPropertyRequirementIndex(conceptId) : -1;
    }

    public void incrementIndex()
    {
        if (parent != null)
        {
            parent.incrementPropertyRequirementIndex(this);
        }
    }

    public void decrementIndex()
    {
        if (parent != null)
        {
            parent.decrementPropertyRequirementIndex(this);
        }
    }

    public boolean containsFieldRequirement(String fieldID)
    {
        return getFieldRequirement(fieldID) != null;
    }

    public List<String> getFieldIDList()
    {
        List<String> idList;

        idList = new ArrayList<>();
        for (ValueRequirement fieldRequirement : getFieldRequirements())
        {
            idList.add(fieldRequirement.getValue());
        }

        return idList;
    }

    public FieldRequirement getFieldRequirement(String fieldID)
    {
        return valueRequirement != null ? (FieldRequirement)valueRequirement.getDescendentRequirement(RequirementType.FIELD_TYPE, fieldID, null) : null;
    }

    public List<FieldRequirement> getFieldRequirements()
    {
        return valueRequirement != null ? (List)valueRequirement.getDescendentRequirements(RequirementType.FIELD_TYPE, null, null) : new ArrayList<FieldRequirement>();
    }

    public boolean isComposite()
    {
        return valueRequirement != null && valueRequirement.getRequirementType() == RequirementType.COMPOSITE_TYPE;
    }

    @Override
    public boolean isEquivalentRequirement(Requirement requirement)
    {
        if (!(requirement instanceof PropertyRequirement))
        {
            return false;
        }
        else
        {
            PropertyRequirement propertyRequirement;

            propertyRequirement = (PropertyRequirement)requirement;
            if (!Objects.equals(conceptId, propertyRequirement.getConceptID())) return false;
            else if (valueRequirement == null) return propertyRequirement.getValueRequirement() == null;
            else return valueRequirement.isEquivalentRequirement(propertyRequirement.getValueRequirement());
        }
    }

    public Object getAttribute(String attributeIdentifier)
    {
        return attributes.get(attributeIdentifier);
    }

    public Object setAttribute(String attributeIdentifier, Object attributeValue)
    {
        return attributes.put(attributeIdentifier, attributeValue);
    }

    public boolean containsAttribute(String attributeIdentifier)
    {
        return attributes.containsKey(attributeIdentifier);
    }

    public Map<String, Object> getAttributes()
    {
        return new LinkedHashMap<>(attributes);
    }

    public void setAttributes(Map<String, Object> newAttributes)
    {
        attributes.clear();
        if (newAttributes != null)
        {
            attributes.putAll(newAttributes);
        }
    }

    /**
     * Returns the data dependencies specified on this requirement.
     * @return The data dependencies specified on this requirement.
     */
    public List<DataDependency> getDataDependencies()
    {
        if (valueRequirement != null)
        {
            return valueRequirement.getDataDependencies();
        }
        else return new ArrayList<>();
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("PropertyRequirement: [");
        builder.append("ID: ");
        builder.append(getConceptID());
        builder.append("]");
        return builder.toString();
    }
}
