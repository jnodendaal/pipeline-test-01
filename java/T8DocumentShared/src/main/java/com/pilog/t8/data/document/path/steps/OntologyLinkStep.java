package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.org.T8OntologyLink;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class OntologyLinkStep extends DefaultPathStep implements PathStep
{
    public OntologyLinkStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, ontologyProvider, null, null, stepString);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            List<T8OntologyLink> links;
            DataRecord dataRecord;

            dataRecord = (DataRecord)input;
            links = ontologyProvider.getOntologyLinks(null, dataRecord.getID());
            return evaluateStep(links);
        }
        else if (input instanceof DataRequirementInstance)
        {
            List<T8OntologyLink> links;
            DataRequirementInstance drInstance;

            drInstance = (DataRequirementInstance)input;
            links = ontologyProvider.getOntologyLinks(null, drInstance.getConceptID());
            return evaluateStep(links);
        }
        else if (input instanceof String)
        {
            List<T8OntologyLink> links;
            String conceptId;

            conceptId = (String)input;
            links = ontologyProvider.getOntologyLinks(null, conceptId);
            return evaluateStep(links);
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Instance step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<T8OntologyLink> evaluateStep(List<T8OntologyLink> links)
    {
        // If we have onstology structure id's to use for narrowing of the list do it now.
        if (idList != null)
        {
            Iterator<T8OntologyLink> linkIterator;

            linkIterator = links.iterator();
            while (linkIterator.hasNext())
            {
                T8OntologyLink nextLink;

                nextLink = linkIterator.next();
                if (!idList.contains(nextLink.getOntologyStructureID()))
                {
                    linkIterator.remove();
                }
            }
        }

        // Return the result list.
        return links;
    }
}
