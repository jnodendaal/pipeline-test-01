package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import java.util.ArrayList;
import java.util.List;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.data.document.path.PathRoot;
import com.pilog.t8.data.document.path.PathStep;

/**
 * @author Bouwer du Preez
 */
public class DescendantRecordStep implements PathStep
{
    private final boolean root;

    public DescendantRecordStep(boolean root)
    {
        this.root = root;
    }

    @Override
    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        // Not needed here.
    }

    @Override
    public void setOntologyProvider(OntologyProvider ontologyProvider)
    {
        // Not needed here.
    }

    @Override
    public void setRecordProvider(DataRecordProvider recordProvider)
    {
        // Not needed here.
    }

    @Override
    public void setDataObjectProvider(T8DataObjectProvider dataObjectProvider)
    {
        // Not needed here.
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input instanceof DataRecord)
        {
            if (root)
            {
                List<DataRecord> records;
                List<PathRoot> roots;

                // Add all descendant records of the root.
                records = ((DataRecord)input).getDescendantRecords();

                // Create a list of root records.
                roots = new ArrayList<PathRoot>();
                roots.add(new PathRoot((DataRecord)input)); // Add the root first.
                for (DataRecord record : records)
                {
                    roots.add(new PathRoot(record));
                }

                return roots;
            }
            else
            {
                return ((DataRecord)input).getDescendantRecords();
            }
        }
        else if (input instanceof RecordProperty)
        {
            List<DataRecord> propertyReferencedRecords;
            List<DataRecord> outputRecords;
            RecordProperty recordProperty;

            recordProperty = (RecordProperty)input;
            propertyReferencedRecords = recordProperty.getParentDataRecord().getSubRecordsReferencedFromProperty(recordProperty.getPropertyID());
            outputRecords = new ArrayList<DataRecord>(propertyReferencedRecords);
            for (DataRecord propertyReferencedRecord : propertyReferencedRecords)
            {
                outputRecords.addAll(propertyReferencedRecord.getDescendantRecords());
            }

            return outputRecords;
        }
        else if (input instanceof DataRecordAccessLayer)
        {
            return ((DataRecordAccessLayer)input).getDescendantRecordAccessLayers();
        }
        else if (input instanceof PropertyAccessLayer)
        {
            List<DataRecord> propertyReferencedRecords;
            List<DataRecordAccessLayer> outputRecordAccessLayers;
            PropertyAccessLayer propertyAccessLayer;

            propertyAccessLayer = (PropertyAccessLayer)input;
            propertyReferencedRecords = propertyAccessLayer.getParentDataRecord().getSubRecordsReferencedFromProperty(propertyAccessLayer.getPropertyId());
            outputRecordAccessLayers = new ArrayList<DataRecordAccessLayer>();
            for (DataRecord propertyReferencedRecord : propertyReferencedRecords)
            {
                DataRecordAccessLayer referencedRecordAccessLayer;

                referencedRecordAccessLayer = propertyReferencedRecord.getAccessLayer();
                outputRecordAccessLayers.add(referencedRecordAccessLayer);
                outputRecordAccessLayers.addAll(referencedRecordAccessLayer.getDescendantRecordAccessLayers());
            }

            return outputRecordAccessLayers;
        }
        else if (input instanceof List)
        {
            List<Object> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<Object>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<Object>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Cannot evaluate 'all records' operator on input: " + input);
    }

    @Override
    public String toString()
    {
        return "*";
    }
}
