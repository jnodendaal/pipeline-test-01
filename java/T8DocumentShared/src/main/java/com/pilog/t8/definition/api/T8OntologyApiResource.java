package com.pilog.t8.definition.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyApiResource implements T8DefinitionResource
{
    public static final String OPERATION_API_ONT_GET_ONTOLOGY_STRUCTURE = "@OP_API_ONT_GET_ONTOLOGY_STRUCTURE";
    public static final String OPERATION_API_ONT_REFRESH_ONTOLOGY_STRUCTURE_NESTED_INDICES = "@OP_API_ONT_REFRESH_ONTOLOGY_STRUCTURE_NESTED_INDICES";
    public static final String OPERATION_API_ONT_VALIDATE_CONCEPT_RELATIONS = "@OP_API_ONT_VALIDATE_CONCEPT_RELATIONS";
    public static final String OPERATION_API_ONT_INSERT_CONCEPT_RELATIONS = "@OP_API_ONT_INSERT_CONCEPT_RELATIONS";
    public static final String OPERATION_API_ONT_SAVE_CONCEPT_RELATIONS = "@OP_API_ONT_SAVE_CONCEPT_RELATIONS";
    public static final String OPERATION_API_ONT_DELETE_CONCEPT_RELATIONS = "@OP_API_ONT_DELETE_CONCEPT_RELATIONS";
    public static final String OPERATION_API_ONT_RETRIEVE_CONCEPT_CODES = "@OP_API_ONT_RETRIEVE_CONCEPT_CODES";
    public static final String OPERATION_API_ONT_DELETE_ONTOLOGY_CLASS = "@OP_API_ONT_DELETE_ONTOLOGY_CLASS";
    public static final String OPERATION_API_ONT_DELETE_DEFINITION = "@OP_API_ONT_DELETE_DEFINITION";
    public static final String OPERATION_API_ONT_DELETE_ABBREVIATION = "@OP_API_ONT_DELETE_ABBREVIATION";
    public static final String OPERATION_API_ONT_DELETE_CODE = "@OP_API_ONT_DELETE_CODE";
    public static final String OPERATION_API_ONT_DELETE_CONCEPT = "@OP_API_ONT_DELETE_CONCEPT";
    public static final String OPERATION_API_ONT_DELETE_TERM = "@OP_API_ONT_DELETE_TERM";
    public static final String OPERATION_API_ONT_RETRIEVE_DEFINITIONS = "@OP_API_ONT_RETRIEVE_DEFINITIONS";
    public static final String OPERATION_API_ONT_RETRIEVE_TERMS = "@OP_API_ONT_RETRIEVE_TERMS";
    public static final String OPERATION_API_ONT_RETRIEVE_CODES = "@OP_API_ONT_RETRIEVE_CODES";
    public static final String OPERATION_API_ONT_RETRIEVE_ABBREVIATIONS = "@OP_API_ONT_RETRIEVE_ABBREVIATIONS";
    public static final String OPERATION_API_ONT_RETRIEVE_TERM_ABBREVIATIONS = "@OP_API_ONT_RETRIEVE_TERM_ABBREVIATIONS";
    public static final String OPERATION_API_ONT_RETRIEVE_CONCEPT_DEFINITIONS = "@OP_API_ONT_RETRIEVE_CONCEPT_DEFINITIONS";
    public static final String OPERATION_API_ONT_RETRIEVE_CONCEPT_TERMS = "@OP_API_ONT_RETRIEVE_CONCEPT_TERMS";
    public static final String OPERATION_API_ONT_INSERT_DEFINITION = "@OP_API_ONT_INSERT_DEFINITION";
    public static final String OPERATION_API_ONT_INSERT_ABBREVIATION = "@OP_API_ONT_INSERT_ABBREVIATION";
    public static final String OPERATION_API_ONT_INSERT_CODE = "@OP_API_ONT_INSERT_CODE";
    public static final String OPERATION_API_ONT_INSERT_CONCEPT = "@OP_API_ONT_INSERT_CONCEPT";
    public static final String OPERATION_API_ONT_INSERT_TERM = "@OP_API_ONT_INSERT_TERM";
    public static final String OPERATION_API_ONT_SAVE_DEFINITION = "@OP_API_ONT_SAVE_DEFINITION";
    public static final String OPERATION_API_ONT_SAVE_ABBREVIATION = "@OP_API_ONT_SAVE_ABBREVIATION";
    public static final String OPERATION_API_ONT_SAVE_CODE = "@OP_API_ONT_SAVE_CODE";
    public static final String OPERATION_API_ONT_SAVE_CONCEPT = "@OP_API_ONT_SAVE_CONCEPT";
    public static final String OPERATION_API_ONT_SAVE_TERM = "@OP_API_ONT_SAVE_TERM";
    public static final String OPERATION_API_ONT_MOVE_ONTOLOGY_CLASS = "@OP_API_ONT_MOVE_ONTOLOGY_CLASS";
    public static final String OPERATION_API_ONT_RETRIEVE_CONCEPTS = "@OP_API_ONT_RETRIEVE_CONCEPTS";
    public static final String OPERATION_API_ONT_GET_ONTOLOGY_LINKS = "@OP_API_ONT_GET_ONTOLOGY_LINKS";
    public static final String OPERATION_API_ONT_RETRIEVE_CONCEPT_RELATION_PATH = "@OP_API_ONT_RETRIEVE_CONCEPT_RELATION_PATH";
    public static final String OPERATION_API_ONT_RECACHE_ONTOLOGY_STRUCTURE = "@OS_API_ONT_RECACHE_ONTOLOGY_STRUCTURE";

    public static final String PARAMETER_LANGUAGE_ID = "$P_LANGUAGE_ID";
    public static final String PARAMETER_CONCEPT_ID = "$P_CONCEPT_ID";
    public static final String PARAMETER_CONCEPT_CODES = "$P_CONCEPT_CODES";
    public static final String PARAMETER_OC_ID = "$P_OC_ID";
    public static final String PARAMETER_DELETION_ENABLED = "$P_DELETION_ENABLED";
    public static final String PARAMETER_DEFINITION_ID = "$P_DEFINITION_ID";
    public static final String PARAMETER_DEFINITION_ID_LIST = "$P_DEFINITION_ID_LIST";
    public static final String PARAMETER_TERM_ID = "$P_TERM_ID";
    public static final String PARAMETER_TERM_ID_LIST = "$P_TERM_ID_LIST";
    public static final String PARAMETER_ABBREVIATION_ID = "$P_ABBREVIATION_ID";
    public static final String PARAMETER_ABBREVIATION_ID_LIST = "$P_ABBREVIATION_ID_LIST";
    public static final String PARAMETER_CODE_ID = "$P_CODE_ID";
    public static final String PARAMETER_CODE_ID_LIST = "$P_CODE_ID_LIST";
    public static final String PARAMETER_PARENT_OC_ID = "$P_PARENT_OC_ID";
    public static final String PARAMETER_ONTOLOGY_LIST = "$P_ONTOLOGY_LIST";
    public static final String PARAMETER_ABBREVIATION_LIST = "$P_ABBREVIATION_LIST";
    public static final String PARAMETER_ABBREVIATION = "$P_ABBREVIATION";
    public static final String PARAMETER_TERM_LIST = "$P_TERM_LIST";
    public static final String PARAMETER_TERM = "$P_TERM";
    public static final String PARAMETER_DEFINITION_LIST = "$P_DEFINITION_LIST";
    public static final String PARAMETER_DEFINITION = "$P_DEFINITION";
    public static final String PARAMETER_CODE_LIST = "$P_CODE_LIST";
    public static final String PARAMETER_CODE = "$P_CODE";
    public static final String PARAMETER_CONCEPT = "$P_CONCEPT";
    public static final String PARAMETER_SUCCESS = "$P_SUCCESS";
    public static final String PARAMETER_CONCEPT_LIST = "$P_CONCEPT_LIST";
    public static final String PARAMETER_INCLUDE_ONTOLOGY = "$P_INCLUDE_ONTOLOGY";
    public static final String PARAMETER_ONTOLOGY_LINKS = "$P_ONTOLOGY_LINKS";
    public static final String PARAMETER_OS_ID_LIST = "$P_OS_ID_LIST";
    public static final String PARAMETER_CONCEPT_ID_LIST = "$P_CONCEPT_ID_LIST";
    public static final String PARAMETER_INCLUDE_TERMS = "$P_INCLUDE_TERMS";
    public static final String PARAMETER_INCLUDE_ABBREVIATIONS = "$P_INCLUDE_ABBREVIATIONS";
    public static final String PARAMETER_INCLUDE_DEFINITIONS = "$P_INCLUDE_DEFINITIONS";
    public static final String PARAMETER_INCLUDE_CODES = "$P_INCLUDE_CODES";
    public static final String PARAMETER_INCLUDE_ONTOLOGY_LINKS = "$P_INCLUDE_ONTOLOGY_LINKS";
    public static final String PARAMETER_ONTOLOGY_STRUCTURE = "$P_ONTOLOGY_STRUCTURE";
    public static final String PARAMETER_RELATION_IDS = "$P_RELATION_IDS";
    public static final String PARAMETER_CONCEPT_PAIRS = "$P_CONCEPT_PAIRS";
    public static final String PARAMETER_HEAD_TO_TAIL = "$P_HEAD_TO_TAIL";
    public static final String PARAMETER_INCLUDE_TERMINOLOGY = "$P_INCLUDE_TERMINOLOGY";

    public static final String DS_DATA_RECORD_CODE = "@DS_DATA_RECORD_CODE";
    public static final String E_DATA_RECORD_CODE = "@E_DATA_RECORD_CODE";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_GET_ONTOLOGY_STRUCTURE);
            definition.setMetaDisplayName("Get Ontology Structure");
            definition.setMetaDescription("Returns the entire default ontology structure for the current session.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiGetOntologyStructure");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ONTOLOGY_STRUCTURE, "Ontology Structure", "The ontology structure for the current session.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RECACHE_ONTOLOGY_STRUCTURE);
            definition.setMetaDisplayName("Recache Ontology Structure");
            definition.setMetaDescription("Reloads the Organization's Ontology Structure content from the persisted source.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRecacheOntologyStructure");
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ONTOLOGY_STRUCTURE, "Ontology Structure", "The Ontology Structure tree object used by the current organization.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_REFRESH_ONTOLOGY_STRUCTURE_NESTED_INDICES);
            definition.setMetaDisplayName("Refresh Ontology Structure Nested Indices");
            definition.setMetaDescription("Refreshes the indices of nested sets in the ontology structure.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRefreshOntologyStructureNestedIndices");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OS_ID_LIST, "Ontology Structure ID List", "The list of Ontology Structures ID's for which to fresh indices.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_VALIDATE_CONCEPT_RELATIONS);
            definition.setMetaDisplayName("Validate Concept Relations");
            definition.setMetaDescription("Validates the list of concept pairs against the respective relations to which they belong.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiValidateConceptRelations");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_PAIRS, "Concept Pair", "The list of ordered concept pairs to be validated.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_PAIRS, "Concept Pair", "The list of invalid concept pairs.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_INSERT_CONCEPT_RELATIONS);
            definition.setMetaDisplayName("Insert Concept Relations");
            definition.setMetaDescription("Inserts the list of concept pairs against the respective relations to which they belong.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiInsertConceptRelations");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_PAIRS, "Concept Pair", "The list of ordered concept pairs to be inserted.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_SAVE_CONCEPT_RELATIONS);
            definition.setMetaDisplayName("Save Concept Relations");
            definition.setMetaDescription("Inserts the list of concept pairs against the respective relations to which they belong if these pairs do not already exist in the respective relations.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiSaveConceptRelations");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_PAIRS, "Concept Pair", "The list of ordered concept pairs to be saved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_DELETE_CONCEPT_RELATIONS);
            definition.setMetaDisplayName("Delete Concept Relations");
            definition.setMetaDescription("Deletes the list of concept pairs from the respective relations to which they belong.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiDeleteConceptRelations");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_PAIRS, "Concept Pair", "The list of ordered concept pairs to be deleted.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_GET_ONTOLOGY_LINKS);
            definition.setMetaDisplayName("Retrives the ontology links for a given concept");
            definition.setMetaDescription("Retrives the ontology links for a given concept");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiGetOntologyLinks");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OS_ID_LIST,  "Ontology Structure ID List", "The list of Ontology Structures ID's for which to fresh indices.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept ID", "The Concept Id for which to retrieve Ontology Links", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ONTOLOGY_LINKS, "Ontology Links", "The return List Of Ontology links", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_CONCEPT_CODES);
            definition.setMetaDisplayName("Retrieve Concept Codes");
            definition.setMetaDescription("Retrieves the concept codes for a given concept ID");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveConceptCodes");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept ID", "The Concept Id for which to retrieve Ontology Codes", T8DataType.GUID));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_CODES, "Concept Codes", "The returned List Of Concept codes", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_DELETE_ONTOLOGY_CLASS);
            definition.setMetaDisplayName("Delete Ontology Class");
            definition.setMetaDescription("Delete the ontology class for the given OC id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiDeleteOntologyClass");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OC_ID, "Ontology Class Id", "The Ontology Class Id that needs to be deleted.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY, "Include Ontology", "A boolean indicator to indicate if the ontology for the given ontology class id should be deleted.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "A boolean indicator to show if the deletion was successfull.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_DELETE_DEFINITION);
            definition.setMetaDisplayName("Definition Id");
            definition.setMetaDescription("Delete the ontology definition for the given definition id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiDeleteDefinition");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID, "Definition Id", "The Ontology Definition Id that needs to be deleted.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_DELETE_ABBREVIATION);
            definition.setMetaDisplayName("Abbreviation Id");
            definition.setMetaDescription("Delete the ontology abbreviation for the given abbreviation id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiDeleteAbbreviation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ABBREVIATION_ID, "Abbreviation Id", "The Ontology Abbreviation Id that needs to be deleted.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_DELETE_CODE);
            definition.setMetaDisplayName("Code Id");
            definition.setMetaDescription("Delete the ontology code for the given code id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiDeleteCode");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CODE_ID, "Code Id", "The Ontology Code Id that needs to be deleted.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_DELETE_CONCEPT);
            definition.setMetaDisplayName("Concept Id");
            definition.setMetaDescription("Delete the ontology concept for the given concept id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiDeleteConcept");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept Id", "The Ontology Concept Id that needs to be deleted.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_DELETE_TERM);
            definition.setMetaDisplayName("Term Id");
            definition.setMetaDescription("Delete the ontology term for the given term id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiDeleteTerm");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERM_ID, "Term Id", "The Ontology Term Id that needs to be deleted.", T8DataType.STRING));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_SAVE_DEFINITION);
            definition.setMetaDisplayName("Save Ontology Definition");
            definition.setMetaDescription("Save the ontology Definition.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiSaveDefinition");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Ontology Definition", "The Ontology Definition that needs to be saved.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_SAVE_ABBREVIATION);
            definition.setMetaDisplayName("Save Ontology Abbreviation");
            definition.setMetaDescription("Save the ontology Abbreviation.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiSaveAbbreviation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ABBREVIATION, "Ontology Abbreviation", "The Ontology Abbreviation that needs to be saved.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_SAVE_CODE);
            definition.setMetaDisplayName("Save Ontology Code");
            definition.setMetaDescription("Save the ontology Code.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiSaveCode");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CODE, "Ontology Code", "The Ontology Code that needs to be saved.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_SAVE_CONCEPT);
            definition.setMetaDisplayName("Save Ontology Concept");
            definition.setMetaDescription("Save the ontology Concept.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiSaveConcept");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT, "Ontology Concept", "The Ontology concept that needs to be saved.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DELETION_ENABLED, "Deletion Enabled", "A Indicator to show if deletion of concept data is allowed.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMS, "Save Terms", "A Indicator to show if terms must also be saved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ABBREVIATIONS, "Save Abbreviations", "A Indicator to show if abbreviations must also be saved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DEFINITIONS, "Save Definitions", "A Indicator to show if definitions must also be saved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_CODES, "Save Codes", "A Indicator to show if codes must also be saved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Save Terminology", "A Indicator to show if terminology must also be saved.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_SAVE_TERM);
            definition.setMetaDisplayName("Save Ontology Term");
            definition.setMetaDescription("Save the ontology Term.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiSaveTerm");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERM, "Ontology Term", "The Ontology Term that needs to be saved.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DELETION_ENABLED, "Deletion Enabled", "A Indicator to show if deletion of concept data is allowed.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ABBREVIATIONS, "Include Abbreviations", "A Indicator to show if abbreviations must also be saved.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_INSERT_ABBREVIATION);
            definition.setMetaDisplayName("Insert Ontology Abbreviation");
            definition.setMetaDescription("Insert the ontology Abbreviation.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiInsertAbbreviation");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ABBREVIATION, "Ontology Abbreviation", "The Ontology Abbreviation that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_INSERT_CONCEPT);
            definition.setMetaDisplayName("Insert Ontology Concept");
            definition.setMetaDescription("Insert the ontology Concept.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiInsertConcept");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT, "Ontology Concept", "The Ontology concept that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMS, "Save Terms", "A Indicator to show if terms must also be inserted.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ABBREVIATIONS, "Save Abbreviations", "A Indicator to show if abbreviations must also be inserted.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DEFINITIONS, "Save Definitions", "A Indicator to show if definitions must also be inserted.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_CODES, "Save Codes", "A Indicator to show if codes must also be inserted.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Save Terminology", "A Indicator to show if terminology must also be inserted.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_INSERT_CODE);
            definition.setMetaDisplayName("Insert Ontology Code");
            definition.setMetaDescription("Insert the ontology Code.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiInsertCode");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CODE, "Ontology Code", "The Ontology Code that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_INSERT_TERM);
            definition.setMetaDisplayName("Insert Ontology Term");
            definition.setMetaDescription("Insert the ontology Term.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiInsertTerm");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERM, "Ontology Term", "The Ontology Term that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ABBREVIATIONS, "Include Abbreviations", "A Indicator to show if the abbreviations should be included in the insert.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_INSERT_DEFINITION);
            definition.setMetaDisplayName("Insert Ontology Definition");
            definition.setMetaDescription("Insert the ontology Definition.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiInsertDefinition");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION, "Ontology Definition", "The Ontology Definition that needs to be inserted.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_DEFINITIONS);
            definition.setMetaDisplayName("Retrieve Ontology Definitions");
            definition.setMetaDescription("Retrieve the ontology definitions for the given definition id's.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveDefinitions");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_ID_LIST, "Definition Id List", "The Ontology Definition Id List that needs to be retrieved.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_LIST, "Concept Definitions", "The list of concept definitions that was retrieved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_TERMS);
            definition.setMetaDisplayName("Retrieve Ontology Terms");
            definition.setMetaDescription("Retrieve the ontology Terms for the given term id's.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveTerms");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERM_ID_LIST, "Term Id List", "The Ontology Term Id List that needs to be retrieved.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ABBREVIATIONS, "Include Abbreviations", "A Indicator to show if the abbreviations should be included in the insert.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERM_LIST, "Concept Terms", "The list of concept terms that was retrieved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_ABBREVIATIONS);
            definition.setMetaDisplayName("Retrieve Ontology Abbreviations");
            definition.setMetaDescription("Retrieve the ontology abbreviations for the given abbreviation id's.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveAbbreviations");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ABBREVIATION_ID_LIST, "Abbreviation Id List", "The Ontology Abbreviation Id List that needs to be retrieved.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ABBREVIATION_LIST, "Concept Abbreviations", "The list of concept abbreviation that was retrieved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_CODES);
            definition.setMetaDisplayName("Retrieve Ontology Codes");
            definition.setMetaDescription("Retrieve the ontology Codes for the given codes id's.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveCodes");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CODE_ID_LIST, "Code Id List", "The Ontology Code Id List that needs to be retrieved.", T8DataType.LIST));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CODE_LIST, "Concept Codes", "The list of concept codes that was retrieved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_TERM_ABBREVIATIONS);
            definition.setMetaDisplayName("Retrieve Ontology Term Abbreviations");
            definition.setMetaDescription("Retrieve the ontology term abbreviations for the given term id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveTermAbbreviations");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERM_ID, "Term Id", "The Term id for which the abbreviations should be retrieved.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_ABBREVIATION_LIST, "Concept Abbreviations", "The list of concept abbreviation that was retrieved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_CONCEPT_DEFINITIONS);
            definition.setMetaDisplayName("Retrieve Ontology Concept Definitions");
            definition.setMetaDescription("Retrieve the ontology concept definitions for the given concept id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveConceptDefinitions");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept Id", "The concept Id for the concept definition that needs to be retrieved.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_DEFINITION_LIST, "Concept Definitions", "The list of concept definitions that was retrieved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_CONCEPT_TERMS);
            definition.setMetaDisplayName("Retrieve Ontology Concept Terms");
            definition.setMetaDescription("Retrieve the ontology concept terms for the given concept id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveConceptTerms");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept Id", "The concept Id for the concept term that needs to be retrieved.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ABBREVIATIONS, "Include Abbreviations", "A Indicator to show if the abbreviations should be included in the retrieve.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_TERM_LIST, "Concept Terms", "The list of concept terms that was retrieved.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_MOVE_ONTOLOGY_CLASS);
            definition.setMetaDisplayName("Move Ontology Class");
            definition.setMetaDescription("Move the ontology class for the given OC id.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiMoveOntologyClass");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_OC_ID, "Ontology Class Id", "The Ontology Class Id that needs to be moved.", T8DataType.STRING));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_PARENT_OC_ID, "Parent Ontology Class Id", "The new parent Ontology Class Id to where the class must be moved.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_SUCCESS, "Success", "A boolean indicator to show if the move was successfull.", T8DataType.BOOLEAN));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_CONCEPTS);
            definition.setMetaDisplayName("Retrieve Concepts");
            definition.setMetaDescription("Retrieves the specified concepts.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveConcepts");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID_LIST, "Concept Id List", "The list of concept id's to be retrieved.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMS, "Include Terms", "A boolean flag to indicate whether or not terms should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ABBREVIATIONS, "Include Abbreviations", "A boolean flag to indicate whether or not abbreviations should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_DEFINITIONS, "Include Terms", "A boolean flag to indicate whether or not definitions should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_CODES, "Include Codes", "A boolean flag to indicate whether or not codes should be retrieved.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_ONTOLOGY_LINKS, "Include Ontology Links", "A boolean flag to indicate whether or not ontology links should be retrieved.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_LIST, "Concept List", "The list of retrieved concepts.", T8DataType.LIST));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_ONT_RETRIEVE_CONCEPT_RELATION_PATH);
            definition.setMetaDisplayName("Retrieve Concept Relation Path");
            definition.setMetaDescription("Retrieves a path of concept ids, starting from the specified concept and moving across the list of relations specified using links from head-to-tail or tail-to-head depending on the input parameters.");
            definition.setClassName("com.pilog.t8.api.T8OntologyApiOperations$ApiRetrieveCrossRelationPath");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_RELATION_IDS, "Relation Ids", "The list of ids of the concept relations to traverse.", T8DataType.LIST));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept Id", "The concept from which the traversal will start.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_LANGUAGE_ID, "Language Id", "The terminology language to use.", T8DataType.GUID));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_HEAD_TO_TAIL, "Head-to-Tail", "If true, the list of concept graphs will be traversed in head-to-tail fashion, otherwise tail-to-head.", T8DataType.BOOLEAN));
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_INCLUDE_TERMINOLOGY, "Include Terminology", "If true, terminology for all concepts will be included in the pairs returned as output of this operation.", T8DataType.BOOLEAN));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_PAIRS, "Pair List", "The list of concept pairs, making up the path retrieved.", T8DataType.LIST));
            definitions.add(definition);
        }

        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER))
        {
            T8SQLDataSourceDefinition sqlDataSourceDefinition;

            sqlDataSourceDefinition = new T8SQLDataSourceResourceDefinition(DS_DATA_RECORD_CODE);
            sqlDataSourceDefinition.getLevel();
            sqlDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
            sqlDataSourceDefinition.setSQLSelectString
            (
                "SELECT   \n" +
                "  REC.ROOT_RECORD_ID,  \n" +
                "  REC.RECORD_ID,  \n" +
                "  REC.DR_ID,  \n" +
                "  REC.DR_INSTANCE_ID,  \n" +
                "  CDE.CODE_ID,  \n" +
                "  CDE.CODE,  \n" +
                "  CDE.CODE_TYPE_ID,  \n" +
                "  REC.FFT \n" +
                "FROM DAT_REC REC \n" +
                "INNER JOIN ONT_CDE CDE \n" +
                "ON REC.RECORD_ID = CDE.CONCEPT_ID"
            );
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$ROOT_RECORD_ID", "ROOT_RECORD_ID", false, false, T8DataType.GUID));
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_ID", "RECORD_ID", false, false, T8DataType.GUID));
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DR_ID", "DR_ID", false, false, T8DataType.GUID));
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DR_INSTANCE_ID", "DR_INSTANCE_ID", false, false, T8DataType.GUID));
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CODE_ID", "CODE_ID", false, false, T8DataType.GUID));
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CODE_TYPE_ID", "CODE_TYPE_ID", false, false, T8DataType.GUID));
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$CODE", "CODE", false, false, T8DataType.STRING));
            sqlDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$FFT", "FFT", false, false, T8DataType.STRING));
            definitions.add(sqlDataSourceDefinition);
        }

        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER))
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = new T8DataEntityResourceDefinition(E_DATA_RECORD_CODE);
            entityDefinition.setDataSourceIdentifier(DS_DATA_RECORD_CODE);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$ROOT_RECORD_ID", DS_DATA_RECORD_CODE + "$ROOT_RECORD_ID", false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$RECORD_ID", DS_DATA_RECORD_CODE + "$RECORD_ID", false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DR_ID", DS_DATA_RECORD_CODE + "$DR_ID", false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$DR_INSTANCE_ID", DS_DATA_RECORD_CODE + "$DR_INSTANCE_ID", false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CODE_ID", DS_DATA_RECORD_CODE + "$CODE_ID", false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CODE_TYPE_ID", DS_DATA_RECORD_CODE + "$CODE_TYPE_ID", false, false, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$CODE", DS_DATA_RECORD_CODE + "$CODE", false, false, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition("$FFT", DS_DATA_RECORD_CODE + "$FFT", false, false, T8DataType.STRING));
            definitions.add(entityDefinition);
        }

        return definitions;
    }
}
