package com.pilog.t8.definition.data.document.datarecord.access;

import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordAccessDefinition
{
    public static final String GROUP_IDENTIFIER = "@DG_DATA_RECORD_ACCESS";
    public static final String GROUP_NAME = "Data Record Access";
    public static final String GROUP_DESCRIPTION = "Handlers that apply access rights and business logic to data records as applicable in specific contexts.";
    public static final String STORAGE_PATH = "/data_record_access";
    public static final String IDENTIFIER_PREFIX = "ACCESS_";

    public T8DataRecordAccessHandler getNewDataRecordAccessHandlerInstance(T8Context context);
}
