/**
 * Created on 28 Jul 2015, 7:35:47 AM
 */
package com.pilog.t8.definition.data.org.concept;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Gavin Boshoff
 */
public class T8ConceptDataAPIResources implements T8DefinitionResource
{
    public static final String OPERATION_API_CD_GET_META_USAGE_COUNTS = "@OS_API_CD_GET_META_USAGE_COUNTS";
    public static final String OPERATION_API_CD_GET_USAGE_COUNTS = "@OS_API_CD_GET_USAGE_COUNTS";

    public static final String PARAMETER_CONCEPT_ID = "$P_CONCEPT_ID";
    public static final String PARAMETER_CONCEPT_USAGES = "$P_CONCEPT_USAGES";
    public static final String PARAMETER_CONCEPT_META_USAGES = "$P_CONCEPT_META_USAGES";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        if (typeIdentifiers.contains(T8JavaServerOperationDefinition.TYPE_IDENTIFIER))
        {
            T8JavaServerOperationDefinition definition;
            List<T8Definition> definitions;

            definitions = new ArrayList<>();

            definition = new T8JavaServerOperationDefinition(OPERATION_API_CD_GET_USAGE_COUNTS);
            definition.setMetaDisplayName("Retrieve Concept Usage Counts");
            definition.setMetaDescription("Retrieves the usage counts for a specific concept ID.");
            definition.setClassName("com.pilog.t8.data.org.concept.T8ConceptDataAPIOperations$APIOperationGetConceptUsageCounts");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept ID", "The concept ID for which usage counts should be determined.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_USAGES, "Concept Usages", "A representation of all the usages for a specific concept ID.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            definition = new T8JavaServerOperationDefinition(OPERATION_API_CD_GET_META_USAGE_COUNTS);
            definition.setMetaDisplayName("Retrieve Concept Meta Usage Counts");
            definition.setMetaDescription("Retrieves the usage counts for a specific concept ID within the meta.");
            definition.setClassName("com.pilog.t8.data.org.concept.T8ConceptDataAPIOperations$APIOperationGetConceptMetaUsageCounts");
            definition.addInputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_ID, "Concept ID", "The concept ID for which usage counts should be determined.", T8DataType.STRING));
            definition.addOutputParameterDefinition(new T8DataParameterDefinition(PARAMETER_CONCEPT_META_USAGES, "Concept Meta Usages", "A representation of all the usages for a specific concept ID within the Meta data.", T8DataType.CUSTOM_OBJECT));
            definitions.add(definition);

            return definitions;
        } else return null;
    }
}
