package com.pilog.t8.definition.data.document.datarecord.merger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeConfiguration;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordMergerDefinition extends T8DataRecordMergerDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_DATA_RECORD_MERGER";
    public static final String DISPLAY_NAME = "Data Record Merger";
    public static final String DESCRIPTION = "A definition that specifies the configuration of a Data Record Merger used to merge data from a source record into a destination record.";
    public enum Datum {INSTANCE_MERGER_DEFINITIONS,
                       PROPERTY_MERGER_DEFINITIONS,
                       FIELD_MERGER_DEFINITIONS,
                       VALUE_MERGER_DEFINITIONS,
                       MERGE_TAGS,
                       MERGE_ONTOLOGY_CODES,
                       MERGE_DESCENDANT_RECORDS,
                       MERGE_ASSIGN_NEW_RECORD_IDENTIFIERS,
                       MERGE_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS,
                       COPY_TAGS,
                       COPY_ONTOLOGY_CODES,
                       COPY_DESCENDANT_RECORDS,
                       COPY_ASSIGN_NEW_RECORD_IDENTIFIERS,
                       COPY_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS,
                       COPY_TAG_SOURCE_TO_COPIED_RECORD};
    // -------- Definition Meta-Data -------- //

    public T8DefaultDataRecordMergerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INSTANCE_MERGER_DEFINITIONS.toString(), "DR Instance Mergers",  "The Data Requirement Instance merger definitions applicable to this context."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PROPERTY_MERGER_DEFINITIONS.toString(), "Property Mergers",  "The property merge configuration definitions applicable to this context."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_MERGER_DEFINITIONS.toString(), "Field Mergers",  "The Field merger definitions applicable within the context of this context."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.VALUE_MERGER_DEFINITIONS.toString(), "Value Mergers",  "The Value merger definitions applicable within the context of this context."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MERGE_TAGS.toString(), "Merge: Include Tags",  "A Boolean setting to enabled/disable the merging of tags from source to destination record.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MERGE_ONTOLOGY_CODES.toString(), "Merge: Include Codes",  "A Boolean setting to enabled/disable the merging of ontology codes from source to destination record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MERGE_DESCENDANT_RECORDS.toString(), "Merge: Include Descendant Records",  "A Boolean setting to enabled/disable the merging of data records descending that are descendants of the input record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MERGE_ASSIGN_NEW_RECORD_IDENTIFIERS.toString(), "Merge: Assign New Record Identifiers",  "A Boolean setting to enabled/disable the assignment of new identifiers to copied/merged destination records.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.MERGE_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString(), "Merge: Assign New Attachment Identifiers",  "A Boolean setting to enabled/disable the assignment of new identifiers to copied/merged destination attachments.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COPY_TAGS.toString(), "Copy: Include Tags",  "A Boolean setting to enabled/disable the merging of tags from source to destination record.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COPY_ONTOLOGY_CODES.toString(), "Copy: Include Codes",  "A Boolean setting to enabled/disable the merging of ontology codes from source to destination record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COPY_DESCENDANT_RECORDS.toString(), "Copy: Include Descendant Records",  "A Boolean setting to enabled/disable the merging of data records descending that are descendants of the input record.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COPY_ASSIGN_NEW_RECORD_IDENTIFIERS.toString(), "Copy: Assign New Record Identifiers",  "A Boolean setting to enabled/disable the assignment of new identifiers to copied/merged destination records.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COPY_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString(), "Copy: Assign New Attachment Identifiers",  "A Boolean setting to enabled/disable the assignment of new identifiers to copied/merged destination attachments.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.COPY_TAG_SOURCE_TO_COPIED_RECORD.toString(), "Copy: Tag Source To Copied Record",  "A Boolean setting to enabled/disable the tagging of copied records with the ID of the source record from which the copy operation was performed.", false));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INSTANCE_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordInstanceMergerDefinition.GROUP_IDENTIFIER));
        else if (Datum.PROPERTY_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordPropertyMergerDefinition.GROUP_IDENTIFIER));
        else if (Datum.FIELD_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordFieldMergerDefinition.GROUP_IDENTIFIER));
        else if (Datum.VALUE_MERGER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordValueMergerDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context sessionContext, Map<String, Object> inputParameters, Map<String, Object> configurationParameters) throws Exception
    {
        super.initializeDefinition(sessionContext, inputParameters, configurationParameters);
    }

    @Override
    public T8DataRecordMerger getNewMergerInstance(T8DataRecordMergeContext mergeContext)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordMerger").getConstructor(T8DataRecordMergeContext.class, this.getClass());
            return (T8DataRecordMerger)constructor.newInstance(mergeContext, this);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing Data Record Merger.", e);
        }
    }

    @Override
    public T8DataRecordMergeConfiguration getNewMergeConfigurationInstance(T8DataRecordMergeContext mergeContext)
    {
        return createMergeConfiguration();
    }

    public T8DataRecordMergeConfiguration createMergeConfiguration()
    {
        T8DataRecordMergeConfiguration config;

        config = new T8DataRecordMergeConfiguration();
        config.setMergeAssignNewAttachmentIdentifiers(isMergeAssignNewAttachmentIdentifiers());
        config.setMergeAssignNewRecordIdentifiers(isMergeAssignNewRecordIdentifiers());
        config.setMergeDescendantRecords(isMergeDescendantRecords());
        config.setMergeOntologyCodes(isMergeOntologyCodes());
        config.setMergeTags(isMergeTags());
        config.setCopyAssignNewAttachmentIdentifiers(isCopyAssignNewAttachmentIdentifiers());
        config.setCopyAssignNewRecordIdentifiers(isCopyAssignNewRecordIdentifiers());
        config.setCopyDescendantRecords(isCopyDescendantRecords());
        config.setCopyOntologyCodes(isCopyOntologyCodes());
        config.setCopyTags(isCopyTags());
        config.setCopyTagSourceToCopiedRecord(isCopyTagSourceToCopiedRecord());
        return config;
    }

    public List<T8DataRecordInstanceMergerDefinition> getInstanceMergerDefinitions()
    {
        return (List<T8DataRecordInstanceMergerDefinition>)getDefinitionDatum(Datum.INSTANCE_MERGER_DEFINITIONS.toString());
    }

    public void setInstanceMergerDefinitions(List<T8DataRecordInstanceMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.INSTANCE_MERGER_DEFINITIONS.toString(), definitions);
    }

    public List<T8DataRecordPropertyMergerDefinition> getPropertyMergerDefinitions()
    {
        return (List<T8DataRecordPropertyMergerDefinition>)getDefinitionDatum(Datum.PROPERTY_MERGER_DEFINITIONS.toString());
    }

    public void setPropertyMergerDefinitions(List<T8DataRecordPropertyMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.PROPERTY_MERGER_DEFINITIONS.toString(), definitions);
    }

    public List<T8DataRecordFieldMergerDefinition> getFieldMergerDefinitions()
    {
        return (List<T8DataRecordFieldMergerDefinition>)getDefinitionDatum(Datum.FIELD_MERGER_DEFINITIONS.toString());
    }

    public void setFieldMergerDefinitions(List<T8DataRecordFieldMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_MERGER_DEFINITIONS.toString(), definitions);
    }

    public List<T8DataRecordValueMergerDefinition> getValueMergerDefinitions()
    {
        return (List<T8DataRecordValueMergerDefinition>)getDefinitionDatum(Datum.VALUE_MERGER_DEFINITIONS.toString());
    }

    public void setValueMergerDefinitions(List<T8DataRecordValueMergerDefinition> definitions)
    {
        setDefinitionDatum(Datum.VALUE_MERGER_DEFINITIONS.toString(), definitions);
    }

    public boolean isMergeTags()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.MERGE_TAGS.toString());
        return value != null ? value : false;
    }

    public void setMergeTags(boolean mergeTags)
    {
        setDefinitionDatum(Datum.MERGE_TAGS.toString(), mergeTags);
    }

    public boolean isMergeOntologyCodes()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.MERGE_ONTOLOGY_CODES.toString());
        return value != null ? value : false;
    }

    public void setMergeOntologyCodes(boolean mergeCodes)
    {
        setDefinitionDatum(Datum.MERGE_ONTOLOGY_CODES.toString(), mergeCodes);
    }

    public boolean isMergeDescendantRecords()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.MERGE_DESCENDANT_RECORDS.toString());
        return value != null ? value : true;
    }

    public void setMergeDescendantRecords(boolean mergeDescendantRecords)
    {
        setDefinitionDatum(Datum.MERGE_DESCENDANT_RECORDS.toString(), mergeDescendantRecords);
    }

    public boolean isMergeAssignNewRecordIdentifiers()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.MERGE_ASSIGN_NEW_RECORD_IDENTIFIERS.toString());
        return value != null ? value : true;
    }

    public void setMergeAssignNewRecordIdentifiers(boolean assignNewRecordIdentifiers)
    {
        setDefinitionDatum(Datum.MERGE_ASSIGN_NEW_RECORD_IDENTIFIERS.toString(), assignNewRecordIdentifiers);
    }

    public boolean isMergeAssignNewAttachmentIdentifiers()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.MERGE_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString());
        return value != null ? value : true;
    }

    public void setMergeAssignNewAttachmentIdentifiers(boolean assignNewAttachmentIdentifiers)
    {
        setDefinitionDatum(Datum.MERGE_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString(), assignNewAttachmentIdentifiers);
    }

    public boolean isCopyTags()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.COPY_TAGS.toString());
        return value != null ? value : false;
    }

    public void setCopyTags(boolean copyTags)
    {
        setDefinitionDatum(Datum.COPY_TAGS.toString(), copyTags);
    }

    public boolean isCopyOntologyCodes()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.COPY_ONTOLOGY_CODES.toString());
        return value != null ? value : false;
    }

    public void setCopyOntologyCodes(boolean copyCodes)
    {
        setDefinitionDatum(Datum.COPY_ONTOLOGY_CODES.toString(), copyCodes);
    }

    public boolean isCopyDescendantRecords()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.COPY_DESCENDANT_RECORDS.toString());
        return value != null ? value : true;
    }

    public void setCopyDescendantRecords(boolean copyDescendantRecords)
    {
        setDefinitionDatum(Datum.COPY_DESCENDANT_RECORDS.toString(), copyDescendantRecords);
    }

    public boolean isCopyAssignNewRecordIdentifiers()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.COPY_ASSIGN_NEW_RECORD_IDENTIFIERS.toString());
        return value != null ? value : true;
    }

    public void setCopyAssignNewRecordIdentifiers(boolean assignNewRecordIdentifiers)
    {
        setDefinitionDatum(Datum.COPY_ASSIGN_NEW_RECORD_IDENTIFIERS.toString(), assignNewRecordIdentifiers);
    }

    public boolean isCopyAssignNewAttachmentIdentifiers()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.COPY_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString());
        return value != null ? value : true;
    }

    public void setCopyAssignNewAttachmentIdentifiers(boolean assignNewAttachmentIdentifiers)
    {
        setDefinitionDatum(Datum.COPY_ASSIGN_NEW_ATTACHMENT_IDENTIFIERS.toString(), assignNewAttachmentIdentifiers);
    }

    public boolean isCopyTagSourceToCopiedRecord()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.COPY_TAG_SOURCE_TO_COPIED_RECORD.toString());
        return value != null ? value : true;
    }

    public void setCopyTagSourceToCopiedRecord(boolean tagSourceToCopiedRecord)
    {
        setDefinitionDatum(Datum.COPY_TAG_SOURCE_TO_COPIED_RECORD.toString(), tagSourceToCopiedRecord);
    }
}
