package com.pilog.t8.data.ontology;

/**
 * @author Bouwer du Preez
 */
public enum T8OntologyConceptType
{
    ATTRIBUTE("DDA31914E3294F788B58CDCE3F290380", "AAD1EEB27C6948368F40974ECBFF6527", "Attribute"),
    CONCEPT_TYPE("0F26B52CE763433D8B793DFA120E59AE", "6C4CC77A1F8D4B3DA669B5718C6C152A", "Concept Type"),
    CODE_TYPE("DE723BACCAF4488A9B10D42CCF3B13E3", "76C8864E692B46C8AFC5B4A5E2A57AE0", "Code Type"),
    CLASS("2A00A96C173342ABA2EBD2A697138CC5", "222131C6D9204E55BC0FBDFC6B9E660F", "Class"),
    SECTION("17C8D90DD5924E3AB31AB17AC61F91DF", "D1FDE5AF1A4E40D6BE0EE27A5742C9EE", "Section"),
    COMMENT_TYPE("51D5007DAFBF4530B6CAC7013E9FFDCE", "3DFA79973874433E83EFBD4670EBFF42", "Comment Type"),
    CONCEPT_RELATIONSHIP_GRAPH("92016888549E4D059C9197FE08025F05", "7B2973E5D29349A4A576A74213C1F65D", "Concept Relationship Graph"),
    CURRENCY("6362265F89E347B59A7AFFF206BDFDD4", "46A1D25715F74B9D8F0048E3B080B464", "Currency"),
    DATA_RECORD("35FBF4EC3F224825AD2CA873FF91E6CE", "7E13F778C7D149ABA22D134A290F1754", "Data Record"),
    DATA_REQUIREMENT("E9903DE2AF7242CD98222F323BBE168E", "9FCF20DD2C42457BBF6F269024D2093F", "Data Requirement"),
    DATA_REQUIREMENT_INSTANCE("97636A82A1F34654AF21B54A908CA1B4", "54EA4FDC1C9C4267A708EE3F4A90FE94", "Data Requirement Instance"),
    DATA_STRING_FORMAT("BA34CAB469C942EEB731EAB79F3BD1AC", "DA70E6220068497E84DE98A60D858CB7", "Data String Format"),
    DATA_STRING_INSTANCE("FE4C837B5A454525930639E67852FBFD", "2A78617D48C049BB99AD55BD2478C540", "Data String Instance"),
    DATA_STRING_TYPE("2B642CB206F240DF91BA7E8AC291B369", "6CF50D78B1C947309ED9152905B177F3", "Data String Type"),
    LANGUAGE("60989377F62D407DB635B3D89ACE85C8", "4972E8B5AC3546E7B54D898014B7C3C0", "Language"),
    ONTOLOGY_CLASS("1E2DA27EA5C6463880898E8BD05E25FC", "68A50FD2546C4F11AECD93F9F55D62A8", "Ontology Class"),
    ONTOLOGY_STRUCTURE("B6C0D0E62DB0430D84031B2E717F9BA4", "25BD559DAF1B417FB11D4F6147BFBBDB", "Ontology Structure"),
    ORGANIZATION("E88CF2A37EBC4C5BACB4E81788AD9D72", "7852B3431F8947F2B4A8EC4A93B6095C", "Organization"),
    ORGANIZATION_TYPE("FD78434E40A841139E272140037C4560", "02164AEE4E364775AF428195D33164CB", "Organization Type"),
    PROPERTY("3A05D5B39AE34F2C97B76D72965D7A1E", "B9017C1220E74530BB881B96C31A1E75", "Property"),
    QUALIFIER_OF_MEASURE("32A0DB944EBA4857BD1C63BAB0F59E4E", "59218B4D879249AD84B541D73BE8A0B5", "Qualifier of Measure"),
    UNIT_OF_MEASURE("E3FFDEB70F81414F92E23F966C1828E4", "34F7CFAC5CED4D8394EB2CCA9896B01D", "Unit of Measure"),
    VALUE("A197D00D2FB44ADDAFFA2AB180327F5B", "E31FD919F6C242CB8770D456A53CA32C", "Value"),
    STATE("B74CF744E7724F25A7C8769E89B71B66", "E8DA3FA29EDF4D78BB95152DD0D39C38", "State");

    private final String conceptTypeID;
    private final String dataTypeID;
    private final String displayName;

    T8OntologyConceptType(String conceptTypeID, String dataTypeID, String displayName)
    {
        this.conceptTypeID = conceptTypeID;
        this.dataTypeID = dataTypeID;
        this.displayName = displayName;
    }

    /**
     * Returns the ID of this concept type.
     * @return The ID of this concept type.
     */
    public String getConceptTypeID()
    {
        return conceptTypeID;
    }

    /**
     * Returns the root Data Type ID to which all concepts of this type belong.
     * @return The root Data Type ID to which all concepts of this type belong.
     */
    public String getDataTypeID()
    {
        return dataTypeID;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public static T8OntologyConceptType getConceptTypeFromDataType(String dataTypeID)
    {
        for (T8OntologyConceptType type : T8OntologyConceptType.values())
        {
            if (type.getDataTypeID().equals(dataTypeID))
            {
                return type;
            }
        }

        return null;
    }

    public static T8OntologyConceptType getConceptType(String conceptTypeID)
    {
        for (T8OntologyConceptType type : T8OntologyConceptType.values())
        {
            if (type.getConceptTypeID().equals(conceptTypeID))
            {
                return type;
            }
        }

        return null;
    }
}
