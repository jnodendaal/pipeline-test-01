package com.pilog.t8.data.source.datarecord;

import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentValidationResult;
import java.io.InputStream;
import java.util.Map;

/**
 * This interface defines the behavior of a standard attachment handler.
 * Because attachments operations can easily involve large file sizes, the
 * attachment handler uses a staged approach to file access.  This means that
 * attachment files are never accessed directly by clients.  Whenever an the
 * contents of an attachment is required, the content can be requested from the
 * attachment handler which will then export the requested content to a
 * dedicated file location where the requesting client can access the data
 * without affecting the rest of the system or the attachment persistence
 * source.
 *
 * @author Bouwer du Preez
 */
public interface T8AttachmentHandler
{
    public boolean isDatabaseStorageEnabled(String drInstanceId) throws Exception;
    public String getFileContextID(String drInstanceId) throws Exception;
    public T8FileDetails getTemporaryAttachmentFileDetails(String attachmentId) throws Exception;
    public T8AttachmentDetails retrieveAttachmentDetails(String attachmentId) throws Exception;
    public Map<String, T8AttachmentDetails> retrieveDataRecordAttachmentDetails(String recordId) throws Exception;
    public void deleteAttachment(String drInstanceId, String attachmentId) throws Exception;

    /**
     * Validates the specified attachment and returns a result object containing
     * the validation errors (if any) that were identified.
     * @param attachmentId The attachment to validate.
     * @return The result of the validation operation.
     * @throws Exception
     */
    public T8AttachmentValidationResult validateAttachment(String attachmentId) throws Exception;

    /**
     * Imports the specified attachment from a target source file.
     * @param drInstanceID The DR Instance to which the attachment belongs.
     * @param attachmentId The ID of the attachment to import.
     * @param attachmentDetails The details of the source file to import.
     * @throws Exception
     */
    public void importAttachment(String drInstanceID, String attachmentId, T8AttachmentDetails attachmentDetails) throws Exception;

    /**
     * Exports the specified attachment to the specified file location.
     * @param attachmentId The ID of the attachment to export.
     * @param contextIid The destination file context instance ID to
     * which the attachment will be exported.
     * @param contextId The destination file context ID to which the attachment
     * will be exported.
     * @param directoryPath The directory path (folder) within the specified
     * context to which the attachment will be exported.
     * @param filename The filename to use when exporting the attachment.  If
     * this parameter is null, the default name of the attachment (as it was
     * imported) will be used.
     * @return The details of the exported file.
     * @throws Exception
     */
    public T8AttachmentDetails exportAttachment(String attachmentId, String contextIid, String contextId, String directoryPath, String filename) throws Exception;

    /**
     * Gets an input stream for the attachment file regardless of storage location
     * @param attachmentID The attachment ID of the file
     * @return A direct input stream from the file
     * @throws Exception
     */
    public InputStream getFileInputStream(String attachmentID) throws Exception;
}
