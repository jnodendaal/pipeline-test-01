package com.pilog.t8.api.datarecordeditor;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataFilePropertyAccessUpdate implements Serializable
{
    private String recordId;
    private String propertyId;
    private boolean visible;
    private boolean required;
    private boolean editable;
    private boolean fftEditable;
    private boolean insertEnabled;
    private boolean updateEnabled;
    private boolean deleteEnabled;

    public T8DataFilePropertyAccessUpdate()
    {
    }

    public T8DataFilePropertyAccessUpdate(String recordId, String propertyId)
    {
        this();
        this.recordId = recordId;
        this.propertyId = propertyId;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getPropertyId()
    {
        return propertyId;
    }

    public void setPropertyId(String propertyId)
    {
        this.propertyId = propertyId;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public boolean isEditable()
    {
        return editable;
    }

    public void setEditable(boolean editable)
    {
        this.editable = editable;
    }

    public boolean isFftEditable()
    {
        return fftEditable;
    }

    public void setFftEditable(boolean fftEditable)
    {
        this.fftEditable = fftEditable;
    }

    public boolean isInsertEnabled()
    {
        return insertEnabled;
    }

    public void setInsertEnabled(boolean insertEnabled)
    {
        this.insertEnabled = insertEnabled;
    }

    public boolean isUpdateEnabled()
    {
        return updateEnabled;
    }

    public void setUpdateEnabled(boolean updateEnabled)
    {
        this.updateEnabled = updateEnabled;
    }

    public boolean isDeleteEnabled()
    {
        return deleteEnabled;
    }

    public void setDeleteEnabled(boolean deleteEnabled)
    {
        this.deleteEnabled = deleteEnabled;
    }
}
