package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.DateRequirement;
import com.pilog.t8.utilities.strings.Strings;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Bouwer du Preez
 */
public class DateValue extends EpochValue
{
    public DateValue(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
    }

    public void setDate(Date date)
    {
        setValue(date != null ? ((Long)date.getTime()).toString() : null);
    }

    public void setDate(Long date)
    {
        setValue(date != null ? date.toString() : null);
    }

    @Override
    public DateValueContent getContent()
    {
        DateValueContent content;

        content = new DateValueContent();
        content.setDate(getValue());
        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        DateValueContent content;
        String date;

        content = (DateValueContent)valueContent;
        date = content.getDate();
        if (date != null)
        {
            // If the input value is an interger (millisecond value), use it as-is.
            if (Strings.isInteger(date))
            {
                setValue(date);
            }
            else
            {
                // The input value is not an integer, so we try to parse it.
                try
                {
                    SimpleDateFormat format;

                    // Attempt to parse the date using the format specified by the date requirement.
                    format = new SimpleDateFormat(((DateRequirement)valueRequirement).getDateFormat());
                    setDate(format.parse(date));
                }
                catch (Exception e)
                {
                    // Set the unparseable value.
                    setValue(date);
                }
            }
        }
        else setValue(null);
    }
}
