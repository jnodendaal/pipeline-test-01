package com.pilog.t8.data.document.datarecord.access.logic;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;

/**
 * @author Bouwer du Preez
 */
public interface T8DataAccessLogic
{
    public T8DataFileValidationReport applyLogic(DataRecord dataRecord);
}
