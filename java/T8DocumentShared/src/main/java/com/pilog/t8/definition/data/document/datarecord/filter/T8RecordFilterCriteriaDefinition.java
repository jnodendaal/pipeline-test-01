package com.pilog.t8.definition.data.document.datarecord.filter;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterClause;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterCriteria;
import com.pilog.t8.security.T8Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8RecordFilterCriteriaDefinition extends T8RecordFilterClauseDefinition implements Serializable
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_RECORD_FILTER_CRITERIA";
    public static final String GROUP_IDENTIFIER = "@DG_RECORD_FILTER_CRITERIA";
    public static final String GROUP_NAME = "Record Filter Criteria";
    public static final String GROUP_DESCRIPTION = "A data filter criteria that contains functionality common to many different components and which is therefore defined on its own in order to be reusable.";
    public static final String STORAGE_PATH = "/record_filter_criteria";
    public static final String DISPLAY_NAME = "Record Filter Criteria";
    public static final String DESCRIPTION = "Criteria to be used when filtering data records.";
    public static final String IDENTIFIER_PREFIX = "RFC_";
    private enum Datum
    {
        ALLOW_CRITERIA_ADDITION,
        ALLOW_SUB_CRITERIA_ADDITION,
        CLAUSE_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8RecordFilterCriteriaDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_CRITERIA_ADDITION.toString(), "Allow Criteria Addition", "If enabled, allows the addition of criteria to this set.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ALLOW_SUB_CRITERIA_ADDITION.toString(), "Allow Sub-Criteria Addition", "If enabled, allows the addition of sub-criteria to this set.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_LIST, Datum.CLAUSE_DEFINITIONS.toString(), "Clauses", "The clauses of this criteria object."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.CLAUSE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8RecordFilterClauseDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8RecordFilterClause getRecordFilterClauseInstance(T8Context context)
    {
        return getRecordFilterCriteriaInstance(context);
    }

    public T8RecordFilterCriteria getRecordFilterCriteriaInstance(T8Context context)
    {
        T8RecordFilterCriteria criteria;

        criteria = new T8RecordFilterCriteria(getIdentifier());
        criteria.setAllowCriteriaAddition(isAllowCriteriaAddition());
        criteria.setAllowSubCriteriaAddition(isAllowSubCriteriaAddition());
        for (T8RecordFilterClauseDefinition clauseDefinition : getClauseDefinitions())
        {
            criteria.addFilterClause(clauseDefinition.getConjunction(), clauseDefinition.getRecordFilterClauseInstance(context));
        }

        return criteria;
    }

    public List<T8RecordFilterClauseDefinition> getClauseDefinitions()
    {
        return (List<T8RecordFilterClauseDefinition>)getDefinitionDatum(Datum.CLAUSE_DEFINITIONS.toString());
    }

    public void setClauseDefinitions(List<T8RecordFilterClauseDefinition> clauseDefinitions)
    {
        setDefinitionDatum(Datum.CLAUSE_DEFINITIONS.toString(), clauseDefinitions);
    }

    public boolean isAllowCriteriaAddition()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_CRITERIA_ADDITION.toString());
        return value != null && value;
    }

    public void setAllowCriteriaAddition(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_CRITERIA_ADDITION.toString(), false);
    }

    public boolean isAllowSubCriteriaAddition()
    {
        Boolean value;

        value = (Boolean)getDefinitionDatum(Datum.ALLOW_SUB_CRITERIA_ADDITION.toString());
        return value != null && value;
    }

    public void setAllowSubCriteriaAddition(Boolean allow)
    {
        setDefinitionDatum(Datum.ALLOW_SUB_CRITERIA_ADDITION.toString(), false);
    }
}