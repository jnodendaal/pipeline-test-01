package com.pilog.t8.data.document.valuestring;

import com.pilog.t8.data.document.TerminologyProvider;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordDefinitionGenerator extends T8DataRecordValueStringGenerator
{
    public T8DataRecordDefinitionGenerator(TerminologyProvider terminologyProvider)
    {
        super.setTerminologyProvider(terminologyProvider);
        super.setIncludeDataRequirementInstanceConcepts(true);
        super.setPropertySeparator(", ");
        super.setIncludeSectionConcepts(false);
        super.setSectionConceptRenderType(T8DataRecordValueStringGenerator.ConceptRenderType.TERM);
        super.setIncludePropertyConcepts(false);
        super.setIncludeFieldConcepts(false);
        super.setValueConceptRenderType(T8DataRecordValueStringGenerator.ConceptRenderType.TERM);
        super.setIncludeSubDocumentReferences(false);
        super.setIncludeSubDocumentValueStrings(false);
        super.setDateTimeFormatPattern(DEFAULT_DATE_TIME_FORMAT);
    }
}
