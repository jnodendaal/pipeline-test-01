package com.pilog.t8.data.document.datarecord.access.layer;

import com.pilog.t8.data.document.requirementtype.RequirementType;

/**
 * @author Bouwer du Preez
 */
public class MeasuredRangeAccessLayer extends ValueAccessLayer
{
    private final UnitOfMeasureAccessLayer uomAccessLayer;

    public MeasuredRangeAccessLayer()
    {
        super(RequirementType.MEASURED_RANGE);
        this.uomAccessLayer = new UnitOfMeasureAccessLayer();
    }

    public UnitOfMeasureAccessLayer getUomAccessLayer()
    {
        return uomAccessLayer;
    }

    public void setUomAccess(UnitOfMeasureAccessLayer newAccess)
    {
        uomAccessLayer.setAccess(newAccess);
    }

    @Override
    public boolean isValueAccessEquivalent(ValueAccessLayer valueAccess)
    {
        if (valueAccess instanceof MeasuredRangeAccessLayer)
        {
            UnitOfMeasureAccessLayer rangeUomAccess;
            MeasuredRangeAccessLayer rangeAccess;

            rangeAccess = (MeasuredRangeAccessLayer)valueAccess;
            rangeUomAccess = rangeAccess.getUomAccessLayer();
            if (uomAccessLayer == null)
            {
                if (rangeUomAccess != null) return false;
                else return true;
            }
            else if (!uomAccessLayer.isValueAccessEquivalent(rangeUomAccess)) return false;
            else return true;
        }
        else return false;
    }

    @Override
    public ValueAccessLayer copy()
    {
        MeasuredRangeAccessLayer copy;

        copy = new MeasuredRangeAccessLayer();
        copy.setUomAccess(uomAccessLayer);
        return copy;
    }

    @Override
    public void setAccess(ValueAccessLayer newAccess)
    {
        MeasuredRangeAccessLayer newMnAccess;

        newMnAccess = (MeasuredRangeAccessLayer)newAccess;
        uomAccessLayer.setAccess(newMnAccess.getUomAccessLayer());
    }
}
