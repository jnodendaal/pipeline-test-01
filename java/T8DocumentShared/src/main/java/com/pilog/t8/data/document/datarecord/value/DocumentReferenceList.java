package com.pilog.t8.data.document.datarecord.value;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarecord.value.DocumentReference.DocumentReferenceType;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8ConceptPair;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceList extends DependencyValue
{
    private final DocumentReferenceRequirement requirement;
    private final List<DocumentReference> references;

    public DocumentReferenceList(ValueRequirement valueRequirement)
    {
        super(valueRequirement);
        this.requirement = (DocumentReferenceRequirement)valueRequirement;
        this.references = new ArrayList<DocumentReference>();
    }

    @Override
    public DocumentReferenceListContent getContent()
    {
        DocumentReferenceListContent content;

        content = new DocumentReferenceListContent();
        content.setIndependent(this.isIndependent());
        for (DocumentReference reference : references)
        {
            content.addReference(reference.getContent());
        }

        return content;
    }

    @Override
    public void setContent(ValueContent valueContent)
    {
        DocumentReferenceListContent content;

        references.clear();
        content = (DocumentReferenceListContent)valueContent;
        for (DocumentReferenceContent referenceContent : content.getReferences())
        {
            DocumentReference reference;

            reference = new DocumentReference(referenceContent.getReferenceType(), referenceContent.getConceptId());
            reference.setContent(referenceContent);
            references.add(reference);
        }
    }

    public boolean isIndependent()
    {
        return Boolean.TRUE.equals(requirement.getAttribute(RequirementAttribute.INDEPENDENT.toString()));
    }

    public String getOntologyClassID()
    {
        return requirement.getOntologyClassId();
    }

    public boolean containsReference(String conceptId)
    {
        for (DocumentReference reference : references)
        {
            if (reference.getConceptId().equals(conceptId)) return true;
        }

        return false;
    }

    public void addReference(DocumentReference reference)
    {
        if (!containsReference(reference.getConceptId()))
        {
            references.add(reference);
        }
        else throw new IllegalArgumentException("Input list " + this + " already contains reference: " + reference);
    }

    public void addReference(String recordId)
    {
        addReference(DocumentReferenceType.RECORD, recordId);
    }

    public void addReference(DocumentReferenceType type, String conceptId)
    {
        if (conceptId != null)
        {
            if (!containsReference(conceptId))
            {
                // Add the new referfenced record ID to the list.
                references.add(new DocumentReference(type, conceptId));
            }
        }
        else throw new IllegalArgumentException("Cannot add null reference to reference list: " + this);
    }

    public void addReferences(Collection<String> newReferences)
    {
        if (!newReferences.contains(null))
        {
            for (String newReference : newReferences)
            {
                addReference(newReference);
            }
        }
        else throw new IllegalArgumentException("Input list " + newReferences + " contains a null reference that cannot be added to reference list: " + this);
    }

    public boolean replaceReference(String oldConceptId, String newConceptId)
    {
        DocumentReference oldReference;

        oldReference = getReference(oldConceptId);
        if (oldReference != null)
        {
            oldReference.setConceptId(newConceptId);
            return true;
        }
        else return false;
    }

    public DocumentReference getReference(String conceptId)
    {
        for (DocumentReference reference : references)
        {
            if (reference.getConceptId().equals(conceptId)) return reference;
        }

        return null;
    }

    public List<DocumentReference> getReferences()
    {
        return new ArrayList<DocumentReference>(references);
    }

    public List<String> getReferenceIds()
    {
        ArrayList<String> referenceIds;

        referenceIds = new ArrayList<>();
        for (DocumentReference reference : references)
        {
            referenceIds.add(reference.getConceptId());
        }

        return referenceIds;
    }

    public int getReferenceCount()
    {
        return references.size();
    }

    public List<RecordValue> removeReference(String conceptId, boolean clearDependentValues)
    {
        List<RecordValue> affectedValues;
        DocumentReference referenceToRemove;
        DataRecord parentRecord;

        // Create the list of removed values.
        affectedValues = new ArrayList<RecordValue>();
        referenceToRemove = getReference(conceptId);
        if (referenceToRemove != null)
        {
            // Clear dependent values if required.
            if (clearDependentValues)
            {
                List<DependencyValue> dependentValues;

                // Propagate the dependency if required.
                dependentValues = getDependentValues(conceptId);
                for (RecordValue dependentValue : dependentValues)
                {
                    RecordValue dependentParent;

                    dependentParent = dependentValue.getParentRecordValue();
                    if (dependentParent instanceof ControlledConcept)
                    {
                        affectedValues.addAll(((ControlledConcept)dependentParent).clearConcept(clearDependentValues));
                    }
                    else if (dependentParent instanceof DocumentReferenceList)
                    {
                        affectedValues.addAll(((DocumentReferenceList)dependentParent).clearReferences(clearDependentValues));
                    }
                    else throw new RuntimeException("Invalid dependent value type found: " + dependentValue);
                }
            }

            // Remove the sub-records referred to.
            parentRecord = getParentDataRecord();
            parentRecord.removeSubRecord(conceptId);

            // Remove the reference.
            references.remove(referenceToRemove);
            affectedValues.add(this);

            // Fire the event to signal the changed.
            fireRecordValueChangedEvent(this);
        }

        // Return the list of removed values.
        return affectedValues;
    }

    public List<RecordValue> clearReferences(boolean clearDependentValues)
    {
        List<RecordValue> affectedValues;
        DataRecord parentRecord;

        // Create the list of removed values.
        affectedValues = new ArrayList<RecordValue>();
        affectedValues.add(this);

        // Remove each of the sub-records referred to.
        parentRecord = getParentDataRecord();
        for (String referenceId : getReferenceIds())
        {
            parentRecord.removeSubRecord(referenceId);
        }

        // Clear the references.
        references.clear();

        // Fire the event to signal the changed.
        fireRecordValueChangedEvent(this);

        // Propagate the dependency if required.
        if (clearDependentValues)
        {
            List<DependencyValue> dependentValues;

            dependentValues = getDependentValues();
            for (RecordValue dependentValue : dependentValues)
            {
                RecordValue dependentParent;

                dependentParent = dependentValue.getParentRecordValue();
                if (dependentParent instanceof ControlledConcept)
                {
                    affectedValues.addAll(((ControlledConcept)dependentParent).clearConcept(clearDependentValues));
                }
                else if (dependentParent instanceof DocumentReferenceList)
                {
                    affectedValues.addAll(((DocumentReferenceList)dependentParent).clearReferences(clearDependentValues));
                }
                else throw new RuntimeException("Invalid dependent value type found: " + dependentValue);
            }
        }

        // Return the list of removed values.
        return affectedValues;
    }

    public List<RecordValue> clearDependentValues(String conceptId)
    {
        List<RecordValue> removedValues;
        List<DependencyValue> dependentValues;

        // Create the list of removed values.
        removedValues = new ArrayList<RecordValue>();
        dependentValues = getDependentValues(conceptId);
        for (RecordValue dependentValue : dependentValues)
        {
            RecordValue dependentParent;

            dependentParent = dependentValue.getParentRecordValue();
            if (dependentParent instanceof ControlledConcept)
            {
                removedValues.addAll(((ControlledConcept)dependentParent).clearConcept(true));
            }
            else if (dependentParent instanceof DocumentReferenceList)
            {
                removedValues.addAll(((DocumentReferenceList)dependentParent).clearReferences(true));
            }
            else throw new RuntimeException("Invalid dependent value type found: " + dependentValue);
        }

        // Return the list of removed values.
        return removedValues;
    }

    public List<DependencyValue> getDependentValues(String referencedConceptId)
    {
        return getParentDataRecord().getDependentValues(this, true);
    }

    /**
     * Returns the list of concept graph edges defining the concept links
     * between this value and its dependants.
     * @param referencedConceptId
     * @return The list of concept graph edges defining the concept links
     * between this value and its dependants.
     */
    public List<T8ConceptPair> getDependantLinks(String referencedConceptId)
    {
        List<DependencyValue> dependentValues;
        List<T8ConceptPair> links;

        // Create a list of all paths linking this value to its dependants.
        links = new ArrayList<T8ConceptPair>();
        dependentValues = getDependentValues(referencedConceptId);
        for (DependencyValue dependentValue : dependentValues)
        {
            DataDependency dependency;
            T8ConceptPair link;
            String tailConceptId;
            List<String> headConceptIDList;

            // Get the head and tail concept ID's.
            tailConceptId = dependentValue.findDependencyConceptId(this, dependentValue, null);
            headConceptIDList = dependentValue.findDependentConceptIdList(this, null);

            // Get the dependency linking this value to the dependant.
            dependency = dependentValue.getDataDependencyOn(this);
            for (String headConceptId : headConceptIDList)
            {
                link = new T8ConceptPair(dependency.getRelationId(), tailConceptId, headConceptId);
                links.add(link);
            }
        }

        // Return the list of results.
        return links;
    }

    /**
     * Returns the concept id's from this record value, that are dependent on
     * the specified parent value (head concepts).
     * @param parentValue The record value on which the returned concept ID's
     * are dependent.
     * @param recordProvider The record provider to use in case a required
     * document cannot be found in the current object model.
     * @return The concept id's from this record value, that are dependent on
     * the specified parent value (head concepts).
     */
    @Override
    public List<String> findDependentConceptIdList(RecordValue parentValue, DataRecordProvider recordProvider)
    {
        List<String> conceptIdList;
        DataRecord parentRecord;

        // For each of the references in the list, find the concept ID involved in the dependency.
        parentRecord = getParentDataRecord();
        conceptIdList = new ArrayList<String>();
        for (String conceptId : getReferenceIds())
        {
            DataRecord localDocument;

            // First try to find the Record from the record graph.
            localDocument = parentRecord.findDataRecord(conceptId);
            if (localDocument != null)
            {
                conceptIdList.add(localDocument.getDataRequirementInstance().getConceptID());
            }
            else
            {
                // We couldn't find the document within the local context so we have to retrieve.
                conceptIdList.add(recordProvider.getRecordDrInstanceId(conceptId));
            }
        }

        // Return the list of concept ID's.
        return conceptIdList;
    }

    @Override
    public boolean hasContent(boolean includeFFT)
    {
        return references.size() > 0;
    }

    @Override
    public RecordValue copy()
    {
        DocumentReferenceList copy;

        copy = (DocumentReferenceList)RecordValue.createValue(valueID, valueRequirement, value);
        copy.setFFT(fft);

        for (DocumentReference reference : references)
        {
            copy.addReference(reference.copy());
        }

        return copy;
    }

    @Override
    public RecordValue copy(ValueRequirement valueRequirement)
    {
        DocumentReferenceList copy;

        copy = (DocumentReferenceList)RecordValue.createValue(valueRequirement);
        copy.setFFT(fft);

        for (DocumentReference reference : references)
        {
            copy.addReference(reference.copy());
        }

        return copy;
    }

    @Override
    public List<HashMap<String, Object>> getDataRows()
    {
        ArrayList<HashMap<String, Object>> dataRows;
        ArrayList<RecordValue> recordValues;
        int valueSequence;

        recordValues = getDescendantValues();
        recordValues.add(this);

        valueSequence = 0;
        dataRows = new ArrayList<>();
        for (String conceptId : getReferenceIds())
        {
            HashMap<String, Object> dataRow;
            RecordSection recordSection;
            RecordProperty recordProperty;
            RecordValue parentRecordValue;
            RequirementType requirementType;
            DataRecord record;

            // Get the values from which data will be extracted.
            parentRecordValue = getParentRecordValue();
            requirementType = RequirementType.DOCUMENT_REFERENCE;
            recordProperty = getParentRecordProperty();
            recordSection = recordProperty.getParentRecordSection();
            record = recordSection.getParentDataRecord();

            // Extract the data row.
            dataRow = new HashMap<>();
            dataRow.put("RECORD_ID", record.getID());
            dataRow.put("ROOT_RECORD_ID", record.getDataFileID());
            dataRow.put("DR_ID", record.getDataRequirement().getConceptID());
            dataRow.put("SECTION_ID", recordSection.getSectionRequirement().getConceptID());
            dataRow.put("PROPERTY_ID", recordProperty.getPropertyRequirement().getConceptID());
            dataRow.put("DATA_TYPE_ID", requirementType.toString());
            dataRow.put("DATA_TYPE_SEQUENCE", getValueRequirement().getSequence());
            dataRow.put("VALUE_SEQUENCE", valueSequence++);
            dataRow.put("PARENT_VALUE_SEQUENCE", parentRecordValue != null ? parentRecordValue.getSequence() : null);
            dataRow.put("VALUE", conceptId);
            dataRow.put("VALUE_CONCEPT_ID", conceptId);
            dataRow.put("FFT", getFFT());
            dataRow.put("VALUE_ID", getPrescribedValueID());

            // Add the extracted data row to the list.
            dataRows.add(dataRow);
        }

        return dataRows;
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public RecordValue getSubValue(RequirementType requirementType, String conceptID, Integer index)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public ArrayList<Value> getSubValues(RequirementType requirementType, String requirementConceptID, Integer valueIndex)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public ArrayList<Value> removeSubValues(RequirementType requirementType, String requirementConceptID, Integer valueIndex)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public void replaceSubValue(RecordValue newValue)
    {
        throw new RuntimeException("Invalid method.");
    }

    // ONLY FOR DEBUGGING PURPOSES - WILL BE REMOVED.
    @Override
    public void setSubValue(Value newValue)
    {
        throw new RuntimeException("Invalid method.");
    }

    @Override
    public String toString()
    {
        return "DocumentReferenceList{" + "references=" + references + '}';
    }

    /**
     * Adds the terminology of this value and of all its descendants to the
     * supplied list.
     * @param terminologyCollector The list to which terminology will be added.
     */
    @Override
    public void addContentTerminology(TerminologyCollector terminologyCollector)
    {
        // Do the super implementation first.
        super.addContentTerminology(terminologyCollector);
        for (DocumentReference reference : references)
        {
            reference.addContentTerminology(terminologyCollector);
        }
    }

    /**
     * Sets the terminology of this value and all of its descendants by retrieval from the
     * supplied terminology provider.
     * @param terminologyProvider The terminology provider form which to fetch terminology.
     */
    @Override
    public void setContentTerminology(TerminologyProvider terminologyProvider)
    {
        super.setContentTerminology(terminologyProvider);
        for (DocumentReference reference : references)
        {
            reference.setContentTerminology(terminologyProvider);
        }
    }
}
