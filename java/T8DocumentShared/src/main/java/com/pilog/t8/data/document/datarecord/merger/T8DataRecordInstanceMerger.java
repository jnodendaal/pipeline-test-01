package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer
 */
public interface T8DataRecordInstanceMerger extends T8DataRecordMergeContext
{
    public void reset();
    public Map<String, String> getRecordIdMapping();
    public Map<String, String> getAttachmentIdentifierMapping();
    public Set<DataRecord> getNewDataRecordSet();
    public Set<DataRecord> getUpdatedDataRecordSet();

    public boolean isApplicable(DataRecord destinationRecord);
    public void mergeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord);
}
