package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.AttachmentDetailSource;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.AttachmentMatchType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.MergeConflictPolicy;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.RecordMatchType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.ConceptRenderType;
import java.io.Serializable;
import java.util.EnumSet;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordMergeConfiguration implements Serializable
{
    private MergeConflictPolicy conflictPolicy;
    private RecordMatchType recordMatchType;
    private AttachmentMatchType attachmentMatchType;
    private AttachmentDetailSource attachmentDetailSource;
    private boolean mergeTags;
    private boolean mergeOntologyCodes;
    private boolean mergeDescendantRecords;
    private boolean mergeAssignNewRecordIdentifiers;
    private boolean mergeAssignNewAttachmentIdentifiers;
    private boolean copyTags;
    private boolean copyOntologyCodes;
    private boolean copyDescendantRecords;
    private boolean copyAssignNewRecordIdentifiers;
    private boolean copyAssignNewAttachmentIdentifiers;
    private boolean copyTagSourceToCopiedRecord;
    private DataRecordProvider sourceRecordProvider;
    private DataRecordProvider destinationRecordProvider;
    private final EnumSet<RequirementType> excludedRequirementTypes;
    private TerminologyProvider terminologyProvider;
    private DataRecordProvider dataRecordProvider;
    private String fftLanguageIdentifier;

        public T8DataRecordMergeConfiguration()
    {
        this.excludedRequirementTypes = EnumSet.noneOf(RequirementType.class);
        this.recordMatchType = RecordMatchType.DEFINITION;
        this.attachmentMatchType = AttachmentMatchType.CHECKSUM;
        this.attachmentDetailSource = AttachmentDetailSource.DATA_RECORD_PROVIDER;
        this.conflictPolicy = MergeConflictPolicy.USE_DESTINATION_DATA_ADD_FFT;
        this.mergeTags = false;
        this.mergeAssignNewRecordIdentifiers = true;
        this.mergeAssignNewAttachmentIdentifiers = true;
        this.mergeDescendantRecords = true;
        this.mergeOntologyCodes = true;
        this.copyTags = false;
        this.copyAssignNewRecordIdentifiers = true;
        this.copyAssignNewAttachmentIdentifiers = true;
        this.copyDescendantRecords = true;
        this.copyOntologyCodes = true;
        this.copyTagSourceToCopiedRecord = false;
    }

    public T8DataRecordMergeConfiguration(T8DataRecordMergeConfiguration config)
    {
        this();
        this.recordMatchType = config.getRecordMatchType();
        this.attachmentMatchType = config.getAttachmentMatchType();
        this.attachmentDetailSource = config.getAttachmentDetailSource();
        this.conflictPolicy = config.getConflictPolicy();
        this.destinationRecordProvider = config.getDestinationRecordProvider();
        this.sourceRecordProvider = config.getSourceRecordProvider();
        this.mergeTags = config.isMergeTags();
        this.mergeDescendantRecords = config.isMergeDescendantRecords();
        this.mergeOntologyCodes = config.isMergeOntologyCodes();
        this.mergeAssignNewAttachmentIdentifiers = config.isMergeAssignNewAttachmentIdentifiers();
        this.mergeAssignNewRecordIdentifiers = config.isMergeAssignNewRecordIdentifiers();
        this.copyTags = config.isCopyTags();
        this.copyDescendantRecords = config.isCopyDescendantRecords();
        this.copyOntologyCodes = config.isCopyOntologyCodes();
        this.copyAssignNewAttachmentIdentifiers = config.isCopyAssignNewAttachmentIdentifiers();
        this.copyAssignNewRecordIdentifiers = config.isCopyAssignNewRecordIdentifiers();
        this.copyTagSourceToCopiedRecord = config.isCopyTagSourceToCopiedRecord();
        setExcludedRequirementTypes(config.getExcludedRequirementTypes());
    }

    public MergeConflictPolicy getConflictPolicy()
    {
        return conflictPolicy;
    }

    public void setConflictPolicy(MergeConflictPolicy conflictPolicy)
    {
        this.conflictPolicy = conflictPolicy;
    }

    public RecordMatchType getRecordMatchType()
    {
        return recordMatchType;
    }

    public void setRecordMatchType(RecordMatchType recordMatchType)
    {
        this.recordMatchType = recordMatchType;
    }

    public AttachmentMatchType getAttachmentMatchType()
    {
        return attachmentMatchType;
    }

    public void setAttachmentMatchType(AttachmentMatchType attachmentMatchType)
    {
        this.attachmentMatchType = attachmentMatchType;
    }

    public AttachmentDetailSource getAttachmentDetailSource()
    {
        return attachmentDetailSource;
    }

    public void setAttachmentDetailSource(AttachmentDetailSource attachmentDetailSource)
    {
        this.attachmentDetailSource = attachmentDetailSource;
    }

    public boolean isMergeTags()
    {
        return mergeTags;
    }

    public void setMergeTags(boolean mergeTags)
    {
        this.mergeTags = mergeTags;
    }

    public boolean isMergeOntologyCodes()
    {
        return mergeOntologyCodes;
    }

    public void setMergeOntologyCodes(boolean mergeOntologyCodes)
    {
        this.mergeOntologyCodes = mergeOntologyCodes;
    }

    public boolean isMergeDescendantRecords()
    {
        return mergeDescendantRecords;
    }

    public void setMergeDescendantRecords(boolean mergeDescendantRecords)
    {
        this.mergeDescendantRecords = mergeDescendantRecords;
    }

    public boolean isMergeAssignNewRecordIdentifiers()
    {
        return mergeAssignNewRecordIdentifiers;
    }

    public void setMergeAssignNewRecordIdentifiers(boolean assignNewRecordIdentifiers)
    {
        this.mergeAssignNewRecordIdentifiers = assignNewRecordIdentifiers;
    }

    public boolean isMergeAssignNewAttachmentIdentifiers()
    {
        return mergeAssignNewAttachmentIdentifiers;
    }

    public void setMergeAssignNewAttachmentIdentifiers(boolean assignNewAttachmentIdentifiers)
    {
        this.mergeAssignNewAttachmentIdentifiers = assignNewAttachmentIdentifiers;
    }

    public boolean isCopyTags()
    {
        return copyTags;
    }

    public void setCopyTags(boolean copyTags)
    {
        this.copyTags = copyTags;
    }

    public boolean isCopyOntologyCodes()
    {
        return copyOntologyCodes;
    }

    public void setCopyOntologyCodes(boolean copyOntologyCodes)
    {
        this.copyOntologyCodes = copyOntologyCodes;
    }

    public boolean isCopyDescendantRecords()
    {
        return copyDescendantRecords;
    }

    public void setCopyDescendantRecords(boolean copyDescendantRecords)
    {
        this.copyDescendantRecords = copyDescendantRecords;
    }

    public boolean isCopyAssignNewRecordIdentifiers()
    {
        return copyAssignNewRecordIdentifiers;
    }

    public void setCopyAssignNewRecordIdentifiers(boolean assignNewRecordIdentifiers)
    {
        this.copyAssignNewRecordIdentifiers = assignNewRecordIdentifiers;
    }

    public boolean isCopyAssignNewAttachmentIdentifiers()
    {
        return copyAssignNewAttachmentIdentifiers;
    }

    public void setCopyAssignNewAttachmentIdentifiers(boolean assignNewAttachmentIdentifiers)
    {
        this.copyAssignNewAttachmentIdentifiers = assignNewAttachmentIdentifiers;
    }

    public boolean isCopyTagSourceToCopiedRecord()
    {
        return copyTagSourceToCopiedRecord;
    }

    public void setCopyTagSourceToCopiedRecord(boolean tagSourceToCopiedRecord)
    {
        this.copyTagSourceToCopiedRecord = tagSourceToCopiedRecord;
    }

    public DataRecordProvider getSourceRecordProvider()
    {
        return sourceRecordProvider;
    }

    public void setSourceRecordProvider(DataRecordProvider sourceRecordProvider)
    {
        this.sourceRecordProvider = sourceRecordProvider;
    }

    public DataRecordProvider getDestinationRecordProvider()
    {
        return destinationRecordProvider;
    }

    public void setDestinationRecordProvider(DataRecordProvider destinationRecordProvider)
    {
        this.destinationRecordProvider = destinationRecordProvider;
    }

    public EnumSet<RequirementType> getExcludedRequirementTypes()
    {
        return excludedRequirementTypes;
    }

    public void setExcludedRequirementTypes(EnumSet<RequirementType> types)
    {
        excludedRequirementTypes.clear();
        if (types != null) excludedRequirementTypes.addAll(types);
    }

    public TerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
    }

    public T8DataRecordValueStringGenerator getFFTGenerator()
    {
        T8DataRecordValueStringGenerator generator;

        generator = new T8DataRecordValueStringGenerator();
        generator.setIncludeSubDocumentValueStrings(true);
        generator.setDataRecordProvider(dataRecordProvider);
        generator.setTerminologyProvider(terminologyProvider);
        generator.setIncludeRecordFFT(true);
        generator.setIncludeSectionConcepts(true);
        generator.setSectionConceptRenderType(ConceptRenderType.TERM);
        generator.setIncludePropertyFFT(true);
        generator.setIncludePropertyConcepts(true);
        generator.setPropertyConceptRenderType(ConceptRenderType.TERM);
        generator.setValueConceptRenderType(ConceptRenderType.TERM);
        generator.setLanguageID(fftLanguageIdentifier);
        return generator;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;
        toStringBuilder = new StringBuilder("T8DataRecordMergeConfiguration{");
        toStringBuilder.append("conflictPolicy=").append(this.conflictPolicy);
        toStringBuilder.append(",recordMatchType=").append(this.recordMatchType);
        toStringBuilder.append(",attachmentMatchType=").append(this.attachmentMatchType);
        toStringBuilder.append(",attachmentDetailSource=").append(this.attachmentDetailSource);
        toStringBuilder.append(",mergeTags=").append(this.mergeTags);
        toStringBuilder.append(",mergeOntologyCodes=").append(this.mergeOntologyCodes);
        toStringBuilder.append(",mergeDescendantRecords=").append(this.mergeDescendantRecords);
        toStringBuilder.append(",mergeAssignNewRecordIdentifiers=").append(this.mergeAssignNewRecordIdentifiers);
        toStringBuilder.append(",mergeAssignNewAttachmentIdentifiers=").append(this.mergeAssignNewAttachmentIdentifiers);
        toStringBuilder.append(",copyTags=").append(this.copyTags);
        toStringBuilder.append(",copyOntologyCodes=").append(this.copyOntologyCodes);
        toStringBuilder.append(",copyDescendantRecords=").append(this.copyDescendantRecords);
        toStringBuilder.append(",copyAssignNewRecordIdentifiers=").append(this.copyAssignNewRecordIdentifiers);
        toStringBuilder.append(",copyAssignNewAttachmentIdentifiers=").append(this.copyAssignNewAttachmentIdentifiers);
        toStringBuilder.append(",copyTagSourceToCopiedRecord=").append(this.copyTagSourceToCopiedRecord);
        toStringBuilder.append(",sourceRecordProvider=").append(this.sourceRecordProvider);
        toStringBuilder.append(",destinationRecordProvider=").append(this.destinationRecordProvider);
        toStringBuilder.append(",excludedRequirementTypes=").append(this.excludedRequirementTypes);
        toStringBuilder.append(",terminologyProvider=").append(this.terminologyProvider);
        toStringBuilder.append(",dataRecordProvider=").append(this.dataRecordProvider);
        toStringBuilder.append(",fftLanguageIdentifier=").append(this.fftLanguageIdentifier);
        toStringBuilder.append('}');
        return toStringBuilder.toString();
    }
}
