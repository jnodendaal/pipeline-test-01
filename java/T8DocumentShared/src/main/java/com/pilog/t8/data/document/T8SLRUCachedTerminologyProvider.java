/**
 * Created on 22 Dec 2015, 3:49:03 PM
 */
package com.pilog.t8.data.document;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.utilities.cache.SLRUCache;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * The {@code T8SLRUCachedTerminologyProvider} uses an underlying SLRU caching
 * data structure to ensure efficient caching of both recently accessed data
 * values, as well as more often accessed data values.<br/>
 * <br/>
 * Internally a customized key is used to ensure proper hash dispersal across
 * the underlying hash data structure buckets. A backing
 * {@code TerminologyProvider} is specified to allow for the data retrieval of
 * entries not found within the cache.
 *
 * @author Gavin Boshoff
 */
public class T8SLRUCachedTerminologyProvider implements CachedTerminologyProvider
{
    private final TerminologyProvider terminologyProvider;
    private final int cacheSize;

    private SLRUCache<TerminologyKey, T8ConceptTerminology> terminologyCache;

    /**
     * Creates a new {@code T8SLRUCachedTerminologyProvider} with the specified
     * cache and a backing {@code TerminologyProvider} which will be used for
     * retrieval of items not yet in the cache.
     *
     * @param terminologyProvider The {@code TerminologyProvider} which will
     *      enable non-cached terminology to be retrieved if they exist
     * @param cacheSize The {@code int} size of the cache to create
     */
    public T8SLRUCachedTerminologyProvider(TerminologyProvider terminologyProvider, int cacheSize)
    {
        this.terminologyProvider = terminologyProvider;
        this.cacheSize = cacheSize;

        this.terminologyCache = new SLRUCache<>(this.cacheSize);
    }

    @Override
    public void clearCache()
    {
        // The fastest way to clear the cache is to break the reference and let
        // the garbage collector handle the cleanup
        this.terminologyCache = new SLRUCache<>(this.cacheSize);
    }

    @Override
    public void addTerminology(T8ConceptTerminology terminology)
    {
        TerminologyKey key;

        key = new TerminologyKey(terminology);
        if (!this.terminologyCache.isCached(key))
        {
            this.terminologyCache.cacheNew(key, terminology);
        }
    }

    @Override
    public void addTerminology(List<T8ConceptTerminology> terminology)
    {
        terminology.forEach(this::addTerminology);
    }

    @Override
    public void cacheTerminology(Collection<String> languageIDList, Collection<String> conceptIDList)
    {
        throw new UnsupportedOperationException("Bulk caching not implemented yet.");
    }

    @Override
    public void setLanguage(String languageID) {}

    @Override
    public String getCode(String conceptID)
    {
        return getTerminology(null, conceptID).getCode();
    }

    @Override
    public String getTerm(String languageID, String conceptID)
    {
        return getTerminology(languageID, conceptID).getTerm();
    }

    @Override
    public String getAbbreviation(String languageID, String conceptID)
    {
        return getTerminology(languageID, conceptID).getAbbreviation();
    }

    @Override
    public String getDefinition(String languageID, String conceptID)
    {
        return getTerminology(languageID, conceptID).getDefinition();
    }

    @Override
    public T8ConceptTerminology getTerminology(String languageID, String conceptID)
    {
        TerminologyKey key;

        key = new TerminologyKey(languageID, conceptID);

        if (this.terminologyCache.isCached(key))
        {
            // Found in cache
            return this.terminologyCache.getCached(key);
        }
        else
        {
            T8ConceptTerminology conceptTerminology;

            // Not found in cache
            // Retrieve the terminology from the provider (most likely the database)
            conceptTerminology = terminologyProvider.getTerminology(languageID, conceptID);

            // If no terminology exists for the concept, we create an empty
            // concept object to add to the cache
            if (conceptTerminology == null)
            {
                conceptTerminology = new T8ConceptTerminology(T8OntologyConceptType.VALUE, conceptID);
                conceptTerminology.setLanguageId(languageID);
            }

            // Cache the terminology item
            this.terminologyCache.cacheNew(key, conceptTerminology);

            return conceptTerminology;
        }
    }

    @Override
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIDList, Collection<String> conceptIDList)
    {
        T8ConceptTerminologyList terminologyList;

        terminologyList = new T8ConceptTerminologyList();
        languageIDList.forEach(languageID -> conceptIDList.stream().map(conceptID -> getTerminology(languageID, conceptID)).forEach(terminologyList::add));

        return terminologyList;
    }

    @Override
    public void addTerminology(String languageId, T8OntologyConceptType conceptType, String conceptId, String codeId, String code, String termId, String term, String abbreviationId, String abbreviation, String definitionId, String definition)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * A serializable key object value with its hash code and equals contracts
     * applied in order to be used in any hashing collection structure.
     */
    private class TerminologyKey implements Serializable
    {
        private static final long serialVersionUID = 3890786928552356550L;

        private final String languageID;
        private final String conceptID;

        private TerminologyKey(T8ConceptTerminology terminology)
        {
            this(terminology.getLanguageId(), terminology.getConceptId());
        }

        private TerminologyKey(String languageID, String conceptID)
        {
            this.languageID = languageID;
            this.conceptID = conceptID;
        }

        @Override
        public int hashCode()
        {
            int hash = 7;
            hash = 19 * hash + Objects.hashCode(this.languageID);
            hash = 19 * hash + Objects.hashCode(this.conceptID);
            return hash;
        }

        @Override
        @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
        public boolean equals(Object obj)
        {
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;

            final TerminologyKey other = (TerminologyKey) obj;
            if (!Objects.equals(this.languageID, other.languageID)) return false;
            return Objects.equals(this.conceptID, other.conceptID);
        }
    }
}