package com.pilog.t8.data.document.datarecord.view;

import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datarecord.DataRecord;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordView
{
    public void updateView(DataRecord dataRecord, T8DataFileAlteration alteration);
}
