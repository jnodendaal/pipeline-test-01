package com.pilog.t8.data.ontology;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import java.io.Serializable;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptPair implements Serializable
{
    private String relationId; // The id of the relation to which this pair belongs.
    private String leftConceptId; // The left projection.
    private String rightConceptId; // The right projection.
    private final T8ConceptTerminologyList terminology;

    public T8ConceptPair(String relationId, String leftConceptId, String rightConceptId)
    {
        this.relationId = relationId;
        this.leftConceptId = leftConceptId;
        this.rightConceptId = rightConceptId;
        this.terminology = new T8ConceptTerminologyList();
    }

    public String getRelationId()
    {
        return relationId;
    }

    public void setRelationId(String relationId)
    {
        this.relationId = relationId;
    }

    public String getLeftConceptId()
    {
        return leftConceptId;
    }

    public void setLeftConceptId(String leftConceptId)
    {
        this.leftConceptId = leftConceptId;
    }

    public String getRightConceptId()
    {
        return rightConceptId;
    }

    public void setRightConceptId(String rightConceptId)
    {
        this.rightConceptId = rightConceptId;
    }

    public T8ConceptTerminologyList getTerminology()
    {
        return terminology;
    }

    public void setTerminology(List<T8ConceptTerminology> terminology)
    {
        this.terminology.setTerminology(terminology);
    }

    public void addTerminology(List<T8ConceptTerminology> terminology)
    {
        this.terminology.addTerminology(terminology);
    }

    @Override
    public String toString()
    {
        StringBuilder buffer;

        buffer = new StringBuilder("T8ConceptPair{");
        buffer.append("relationId=");
        buffer.append(relationId);
        buffer.append(", leftConceptId=");
        buffer.append(leftConceptId);
        buffer.append(", rightConceptId=");
        buffer.append(rightConceptId);
        buffer.append('}');
        return buffer.toString();
    }
}
