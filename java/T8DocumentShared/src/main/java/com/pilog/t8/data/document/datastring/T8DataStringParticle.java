package com.pilog.t8.data.document.datastring;

import com.pilog.t8.data.document.datastring.T8DataStringElement.TruncationType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.List;

import static com.pilog.t8.data.document.datastring.T8DataStringElement.TruncationType.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringParticle implements Serializable
{
    private final String name;
    private final String elementId;
    private final StringBuilder content;
    private String dependencyElementId;
    private double priority;
    private double sequence;
    private String prefix;
    private String suffix;
    private String separator;
    private boolean active;
    private String groupId;
    private int lengthRestriction;
    private TruncationType truncationType;
    private T8DataStringParticle parentParticle;
    private final List<T8DataStringParticle> subParticles;

    public T8DataStringParticle(String name, T8DataStringElement element, CharSequence characters, List<T8DataStringParticle> subParticles)
    {
        this.name = name;
        this.elementId = element.getID();
        this.active = true;
        this.dependencyElementId = element.getDependencyElementID();
        this.priority = element.getPriority();
        this.lengthRestriction = element.getLengthRestriction();
        this.truncationType = element.getTruncationType();
        this.prefix = element.getPrefix();
        this.suffix = element.getSuffix();
        this.separator = element.getSeparator();
        this.content = characters != null ? new StringBuilder(characters) : null;
        this.subParticles = new ArrayList<>();
        this.groupId = element.getGroupId();
        addSubParticles(subParticles);
    }

    void setParent(T8DataStringParticle parent)
    {
        this.parentParticle = parent;
    }

    public T8DataStringParticle getParent()
    {
        return parentParticle;
    }

    public final void addSubParticles(List<T8DataStringParticle> particles)
    {
        if (particles != null)
        {
            for (T8DataStringParticle particle : particles)
            {
                particle.setParent(this);
                this.subParticles.add(particle);
            }
        }
    }

    public String getElementId()
    {
        return elementId;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getDependencyElementId()
    {
        return dependencyElementId;
    }

    public void setDependencyElementId(String dependencyElementId)
    {
        this.dependencyElementId = dependencyElementId;
    }

    public double getPriority()
    {
        return priority;
    }

    public void setPriority(double priority)
    {
        this.priority = priority;
    }

    public double getSequence()
    {
        return sequence;
    }

    public void setSequence(double sequence)
    {
        this.sequence = sequence;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }

    public String getSuffix()
    {
        return suffix;
    }

    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }

    public String getSeparator()
    {
        return separator;
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public int getLengthRestriction()
    {
        return lengthRestriction;
    }

    public void setLengthRestriction(int lengthRestriction)
    {
        this.lengthRestriction = lengthRestriction;
    }

    public TruncationType getTruncationType()
    {
        return truncationType;
    }

    public void setTruncationType(TruncationType truncationType)
    {
        this.truncationType = truncationType;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public void setLength(int length)
    {
        content.setLength(length);
    }

    public StringBuilder generateDataString()
    {
        StringBuilder string;

        // Construct the output particle.
        string = generateParticleString();
        if (string != null)
        {
            // Apply transormations.
            applyTransformations(string);

            // Check the length restriction and truncate the string if required.
            if ((lengthRestriction > 0) && (string.length() > lengthRestriction))
            {
                switch (truncationType)
                {
                    case CHARACTER:
                    {
                        string.setLength(lengthRestriction);
                        break;
                    }
                    case WORD:
                    {
                        string.setLength(lengthRestriction);
                        break;
                    }
                    case PARTICLE:
                    {
                        LinkedList<T8DataStringParticle> queue;
                        T8DataStringParticle nextParticle;
                        StringBuilder validString;

                        // The following steps are followed for this truncation type.
                        // - Deactivate all descendant particles.
                        // - We will then proceed to reactivate one descendant particle at a time, until the length limit is reached.
                        // - Descendant particles are reactivated in sequence of their priority, from highest to lowest.
                        // - If a particle is activated and this results in the length limit being exceeded, the particle is deactivated again,
                        //   and subsequent particles with a lower priority are then evaluated for inclusion.
                        setDescendantParticlesActive(false);

                        // Create the processing queue.
                        queue = new LinkedList<>();
                        queue.add(this);
                        addDescendantsToPriorityQueue(queue);

                        // Add particles to the string according to priority, until the length limit is reached.
                        string = null;
                        validString = null;
                        while ((!queue.isEmpty()) && ((string == null) || (string.length() <= lengthRestriction)))
                        {
                            // Find the particle with the next highest priority.
                            nextParticle = queue.removeFirst();
                            if (nextParticle != null)
                            {
                                // Activate the next particle and generate a new string with it included.
                                nextParticle.setActive(true);

                                // Construct the output particle.
                                string = generateParticleString();

                                // Apply transformations and re-check length.
                                if (string != null)
                                {
                                    // Transformations may have an effect on the length of the string so apply them first.
                                    applyTransformations(string);

                                    // If adding this particle resulted in a string that exceeds the limit, remove the particle again and revert to the last valid string.
                                    if (string.length() > lengthRestriction)
                                    {
                                        List<T8DataStringParticle> deactivatedParticles;

                                        // Create a list to hold all particle that will now be deactivated.
                                        deactivatedParticles = new ArrayList();

                                        // Deactivate the particle and all of its dependents.
                                        nextParticle.setActive(false);
                                        nextParticle.deactivateDependencies(deactivatedParticles);

                                        // Remove all deactivated particles from the processing queue because they can no longer be activated.
                                        queue.removeAll(deactivatedParticles);

                                        // Restore the generated string to the last valid state.
                                        string = validString;
                                    }
                                    else
                                    {
                                        // Store the valid string before adding another particle.
                                        validString = string;
                                    }
                                }
                            }
                            else
                            {
                                // No more particle to be activated.
                                break;
                            }
                        }

                        // Use the last valid string before the length was exceeded.
                        string = validString;
                    }
                }
            }
        }

        // Return the final string.
        return string; // Non-active particles return null strings.
    }

    private StringBuilder generateParticleString()
    {
        // Check the number of active subParticles and continue if we have at least one, or if the content of this element is not empty.
        if ((isDependencyActive()) && (hasActiveContent()))
        {
            Iterator<T8DataStringParticle> particleIterator;
            T8DataStringParticle lastIncludedParticle;
            StringBuilder particleString;
            int includedParticleCount;

            // Construct the new particle.
            particleString = new StringBuilder();

            // Add the prefix if one is specified.
            if (prefix != null) particleString.append(prefix);

            // Append the content of this particle.
            if (content != null) particleString.append(content);

            // Add all particles in the list, with the specified separator in between particles.
            includedParticleCount = 0;
            lastIncludedParticle = null;
            particleIterator = subParticles.iterator();
            while (particleIterator.hasNext())
            {
                T8DataStringParticle nextParticle;
                StringBuilder subParticleString;

                // Append the next particle.
                nextParticle = particleIterator.next();
                subParticleString = nextParticle.generateDataString();
                if (subParticleString != null)
                {
                    // Append the separator if applicable.  A separator is only inserted between two particles of the same group (groupId).
                    if ((separator != null) && (lastIncludedParticle != null) && (Objects.equals(lastIncludedParticle.getGroupId(), nextParticle.getGroupId())))
                    {
                        particleString.append(separator);
                    }

                    // Append the subParticle string.
                    particleString.append(subParticleString);
                    includedParticleCount++;
                    lastIncludedParticle = nextParticle;
                }
            }

            // Add the suffix if one is specified.
            if (suffix != null) particleString.append(suffix);

            // Return the new particle.
            if ((includedParticleCount > 0) || (content != null))
            {
                return particleString;
            }
            else return null;
        }
        else return null;
    }

    /**
     * This method recursively deactivates the dependency chain, starting from this particle and moving up the particle tree.
     * At each level in the particle tree, all dependencies are checked and deactivated until no particle remains
     * active with an unsatisfied dependency.  Once this condition is met, the parent particle is checked for active
     * sub-particles.  If none are found, the parent particle is deactivated and the dependency check continues recursively
     * with its dependent particles.
     */
    private void deactivateDependencies(List<T8DataStringParticle> deactivatedParticles)
    {
        if (parentParticle != null)
        {
            List<T8DataStringParticle> dependentParticles;

            dependentParticles = parentParticle.findContentParticlesByDependencyElementId(elementId);
            for (T8DataStringParticle dependentParticle : dependentParticles)
            {
                if ((dependentParticle != this) && (!deactivatedParticles.contains(dependentParticle)))
                {
                    dependentParticle.setActive(false);
                    deactivatedParticles.add(dependentParticle);
                    dependentParticle.deactivateDependencies(deactivatedParticles);
                }
            }
        }
    }

    /**
     * Checks the dependency of this particle (if any) and returns a value indicating
     * whether or not it is active.
     * @return
     * - If no dependency is specified for this particle, true is returned.
     * - If a dependency is specified, true is returned if the dependency particle found and is active.
     * - If the preceding conditions are not met, false is returned.
     */
    private boolean isDependencyActive()
    {
        String elementIdToFind;

        // Get the element ID on which this particle is dependent.
        elementIdToFind = getDependencyElementId();
        if (elementIdToFind != null)
        {
            if (parentParticle != null)
            {
                T8DataStringParticle dependencyParticle;

                // Find at least one particle generated from the element we are looking for.
                dependencyParticle = parentParticle.findContentParticleByElementId(elementIdToFind);
                return ((dependencyParticle != null) && (dependencyParticle.hasActiveContent()));
            }
            else return false;
        }
        else return true;
    }

    /**
     * Returns the number of sub-particles of this particle that are flagged as 'active'.
     * @return The number of sub-particles of this particle that are flagged as 'active'.
     */
    private int getActiveSubParticleCount()
    {
        int count;

        count = 0;
        for (T8DataStringParticle particle : subParticles)
        {
            if (particle.isActive()) count++;
        }

        return count;
    }

    /**
     * Returns the first active sub-particle that was generated from the specified element and
     * contains active content i.e. either a String literal, or an active descendant that contains a String literal.
     * @param particles The list of particles to evaluate.
     * @param elementId The id of the element to check for.
     * @return The first particle from the supplied list, to have the specified elementId.
     */
    private T8DataStringParticle findContentParticleByElementId(String elementId)
    {
        // Check all particles in the list.
        for (T8DataStringParticle particle : subParticles)
        {
            // Make sure the particle was generated from the required element and is active.
            if ((particle.isActive()) && (particle.getElementId().equals(elementId)))
            {
                // Make sure the particle contains active content.
                if (particle.hasActiveContent())
                {
                    return particle;
                }
            }
        }

        return null;
    }

    /**
     * Returns a list of all sub-particles that are dependent on the specified element.
     * @param elementId The id of the element to check for.
     * @return The list of sub-particles dependent on the specified element.
     */
    private List<T8DataStringParticle> findContentParticlesByDependencyElementId(String elementId)
    {
        List<T8DataStringParticle> particleList;

        // Check all particles in the subParticles collection.
        particleList = new ArrayList<>();
        for (T8DataStringParticle particle : subParticles)
        {
            String depElementId;

            depElementId = particle.getDependencyElementId();
            if (elementId.equals(depElementId))
            {
                particleList.add(particle);
            }
        }

        return particleList;
    }

    /**
     * Returns true if one of this particle's sub-particles are active.
     * @return True if one of this particle's sub-particles are active.
     */
    private boolean hasActiveSubParticle()
    {
        for (T8DataStringParticle particle : subParticles)
        {
            if (particle.isActive())
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns true if this particle or one of its descendants contains active content i.e. a particle that is
     * both active and contains a String literal as content.
     * @return true if this particle or one of its descendants contains active content.
     */
    private boolean hasActiveContent()
    {
        if (active)
        {
            if (content != null)
            {
                return true;
            }
            else
            {
                for (T8DataStringParticle particle : subParticles)
                {
                    if (particle.hasActiveContent())
                    {
                        return true;
                    }
                }

                return false;
            }
        }
        else return false;
    }

    /**
     * Sets that active indicator of all descendant particles according to the input value.
     * @param active The value to which all descendant particle 'active' flags will be set.
     */
    private void setDescendantParticlesActive(boolean active)
    {
        for (T8DataStringParticle particle : subParticles)
        {
            particle.setActive(active);
            particle.setDescendantParticlesActive(active);
        }
    }

    /**
     * Adds all descendants of this particle to the input queue ordered in the sequence
     * of highest priority to lowest.  It is important to note that this does not
     * mean the final queue will contain a sequence of particles with priority values
     * that are directly comparable to each other because each particle's priority is relative to its
     * parent's.
     * @param queue The queue to which the the descendant particles will be added.
     */
    private void addDescendantsToPriorityQueue(LinkedList<T8DataStringParticle> queue)
    {
        for (T8DataStringParticle subParticle : getSubParticlePriorityList())
        {
            queue.add(subParticle);
            subParticle.addDescendantsToPriorityQueue(queue);
        }
    }

    /**
     * Returns the sub-particles of this particle, sorted according to priority, from
     * highest to lowest.  Not that higher priority particles have a lower numeric priority value.
     * @return The sub-particle collection of this particle, sorted from highest to lowest priority.
     */
    private List<T8DataStringParticle> getSubParticlePriorityList()
    {
        List<T8DataStringParticle> priorityList;

        priorityList = new ArrayList<>(subParticles);
        Collections.sort(priorityList, new T8DataStringParticlePriorityComparator());
        return priorityList;
    }

    /**
     * Applies all transformations linked to this element, to the supplied
     * particle.  The input parameter is therefore mutated by this method.
     * @param inputParticle The input particle to which transformations will
     * be applied.
     */
    private void applyTransformations(StringBuilder particleString)
    {
    }

    @Override
    public String toString()
    {
        return "T8DataStringParticle{" + "name=" + name + ", elementId=" + elementId + ", content=" + content + ", dependencyElementId=" + dependencyElementId + ", priority=" + priority + ", sequence=" + sequence + ", prefix=" + prefix + ", suffix=" + suffix + ", separator=" + separator + ", active=" + active + ", groupId=" + groupId + ", lengthRestriction=" + lengthRestriction + ", truncationType=" + truncationType + '}';
    }
}
