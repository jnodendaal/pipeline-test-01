package com.pilog.t8.data.document.datarecord.attachment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8AttachmentValidationReport implements Serializable
{
    public Map<String, List<T8AttachmentValidationResult>> recordResults;

    public T8AttachmentValidationReport()
    {
        recordResults = new HashMap<String, List<T8AttachmentValidationResult>>();
    }

    /**
     * Returns a single boolean value indicating whether or not all of the
     * results in the report are positive (valid).  If this report contains any
     * validation errors, a boolean 'false' value will be returned.
     * @return A boolean value indicating whether or not all of the  results in
     * the report are positive (valid).
     */
    public boolean isValid()
    {
        for (List<T8AttachmentValidationResult> resultList : recordResults.values())
        {
            for (T8AttachmentValidationResult result : resultList)
            {
                if (!result.isValid()) return false;
            }
        }

        return true;
    }

    public void addValidationResult(String recordID, T8AttachmentValidationResult result)
    {
        List<T8AttachmentValidationResult> results;

        // Find the list of results for the specified record.
        results = recordResults.get(recordID);
        if (results == null)
        {
            results = new ArrayList<T8AttachmentValidationResult>();
            recordResults.put(recordID, results);
        }

        // Add the supplied result to the list if it does not contain the result already.
        if (!results.contains(result))
        {
            results.add(result);
        }
    }

    public List<T8AttachmentValidationResult> getValidationResults()
    {
        List<T8AttachmentValidationResult> completeResultList;

        // Find the results for the requested record.
        completeResultList = new ArrayList<T8AttachmentValidationResult>();
        for (List<T8AttachmentValidationResult> resultList : recordResults.values())
        {
            completeResultList.addAll(resultList);
        }

        return completeResultList;
    }

    public List<T8AttachmentValidationResult> getValidationResults(String recordID)
    {
        List<T8AttachmentValidationResult> resultList;

        // Find the results for the requested record.
        resultList = recordResults.get(recordID);
        if (resultList != null)
        {
            return new ArrayList<T8AttachmentValidationResult>(resultList);
        }
        else
        {
            return new ArrayList<T8AttachmentValidationResult>();
        }
    }
}
