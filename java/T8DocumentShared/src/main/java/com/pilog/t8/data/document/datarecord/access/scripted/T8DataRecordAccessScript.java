package com.pilog.t8.data.document.datarecord.access.scripted;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;

/**
 * @author Bouwer du Preez
 */
public interface T8DataRecordAccessScript
{
    public T8DataFileValidationReport applyScript(DataRecord dataRecord);
}
