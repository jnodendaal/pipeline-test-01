package com.pilog.t8.data.document.path.steps;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.path.PathStep;
import com.pilog.t8.data.document.path.StepModifiers;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ValueStringStep extends DefaultPathStep implements PathStep
{
    private final T8DataRecordValueStringGenerator valueStringGenerator;

    public ValueStringStep(StepModifiers modifiers, TerminologyProvider terminologyProvider, String stepString)
    {
        super(modifiers, terminologyProvider, null, null, null, stepString);
        this.valueStringGenerator = new T8DataRecordValueStringGenerator();
        this.valueStringGenerator.setTerminologyProvider(terminologyProvider);
        this.valueStringGenerator.setIncludePropertyConcepts(false);
        this.valueStringGenerator.setIncludeFieldConcepts(false);
    }

    @Override
    public void setTerminologyProvider(TerminologyProvider terminologyProvider)
    {
        super.setTerminologyProvider(terminologyProvider);
        this.valueStringGenerator.setTerminologyProvider(terminologyProvider);
    }

    @Override
    public Object evaluateStep(Object rootInput, Object input)
    {
        if (input == null)
        {
            List<String> result;

            // DocPath steps always return lists.
            result = new ArrayList(1);
            result.add(null);
            return result;
        }
        else if (input instanceof Value)
        {
            return evaluateValueStep((Value)input);
        }
        else if (input instanceof List)
        {
            List<String> outputList;
            List<Object> inputList;

            inputList = (List)input;
            outputList = new ArrayList<String>();
            for (Object inputObject : inputList)
            {
                outputList.addAll((List<String>)evaluateStep(rootInput, inputObject));
            }

            return outputList;
        }
        else throw new RuntimeException("Value String step '" + stepString + "' cannot be executed from content object: " + input);
    }

    private List<String> evaluateValueStep(Value value)
    {
        List<String> valueStrings;

        // Create a list to hold the value strings.
        valueStrings = new ArrayList<String>();

        // Generate the value string for the value object.
        if (value instanceof DataRecord)
        {
            StringBuilder valueString;

            valueString = valueStringGenerator.generateValueString((DataRecord)value);
            if (valueString != null) valueStrings.add(valueString.toString());
            else if (modifiers.isIncludeNullValues()) valueStrings.add(null);
        }
        else if (value instanceof RecordProperty)
        {
            StringBuilder valueString;

            valueString = valueStringGenerator.generateValueString((RecordProperty)value);
            if (valueString != null) valueStrings.add(valueString.toString());
            else if (modifiers.isIncludeNullValues()) valueStrings.add(null);
        }
        else if (value instanceof RecordValue)
        {
            StringBuilder valueString;

            valueString = valueStringGenerator.generateValueString((RecordValue)value);
            if (valueString != null) valueStrings.add(valueString.toString());
            else if (modifiers.isIncludeNullValues()) valueStrings.add(null);
        }

        // Now run through the remaining value string list and evaluate the predicate expression (if any).
        if (predicateExpressionEvaluator != null)
        {
            Iterator<String> valueStringIterator;

            // Iterator over the value strings and evaluate the predicate expression on each one, retaining only those for which the predicate applies.
            valueStringIterator = valueStrings.iterator();
            while (valueStringIterator.hasNext())
            {
                Map<String, Object> inputParameters;
                String nextValueString;

                // Create a map containing the input parameters available to the predicate expression.
                nextValueString = valueStringIterator.next();
                inputParameters = new HashMap<String, Object>();
                inputParameters.put("VALUE_STRING", nextValueString);

                // Evaluate the predicate.
                try
                {
                    if (!predicateExpressionEvaluator.evaluateBooleanExpression(inputParameters, null))
                    {
                        valueStringIterator.remove();
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while evaluating predicate expression in path step: " + stepString, e);
                }
            }
        }

        return valueStrings;
    }
}
