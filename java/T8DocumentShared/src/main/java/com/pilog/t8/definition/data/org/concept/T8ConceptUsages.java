/**
 * Created on 28 Jul 2015, 9:38:52 AM
 */
package com.pilog.t8.definition.data.org.concept;

import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.io.Serializable;

/**
 * @author Gavin Boshoff
 */
public class T8ConceptUsages implements Serializable
{
    private static final long serialVersionUID = -8778172994906750699L;

    private final T8OntologyConceptType conceptType;
    private final String conceptID;

    private int languageOntologyDefinitionUsageCount;
    private int organizationOrgStructureUsageCount;
    private int conceptGraphRelationalUsageCount;
    private int dataTypeDataStructureUsageCount;
    private int dataRequirementRecordUsageCount;
    private int languageOntologyTermUsageCount;
    private int languageTerminologyUsageCount;
    private int dataRequirementDRUsageCount;
    private int drInstanceRecordUsageCount;
    private int uomRecordValueUsageCount;
    private int propertyRecordUsageCount;
    private int drInstanceDRUsageCount;
    private int valueRecordUsageCount;
    private int propertyDRUsageCount;
    private int qomRecordUsageCount;
    private int classRecordUsages;
    private int classDRUsageCount;
    private int valueSVUsageCount;
    private int uomsvUsageCount;
    private int qomsvUsageCount;

    public T8ConceptUsages(String conceptID, T8OntologyConceptType conceptType)
    {
        this.conceptID = conceptID;
        this.conceptType = conceptType;
    }

    public T8OntologyConceptType getConceptType()
    {
        return this.conceptType;
    }

    public String getConceptID()
    {
        return this.conceptID;
    }

    /**
     * A convenience method which will simply determine if any of the count
     * values are non-zero (!= 0).
     *
     * @return {@code true} if there are any non-zero counts. {@code false}
     *      otherwise
     */
    public boolean hasUsages()
    {
        return ((this.languageOntologyDefinitionUsageCount != 0)
                || (this.organizationOrgStructureUsageCount != 0)
                || (this.conceptGraphRelationalUsageCount != 0)
                || (this.dataTypeDataStructureUsageCount != 0)
                || (this.dataRequirementRecordUsageCount != 0)
                || (this.languageOntologyTermUsageCount != 0)
                || (this.languageTerminologyUsageCount != 0)
                || (this.dataRequirementDRUsageCount != 0)
                || (this.drInstanceRecordUsageCount != 0)
                || (this.uomRecordValueUsageCount != 0)
                || (this.propertyRecordUsageCount != 0)
                || (this.drInstanceDRUsageCount != 0)
                || (this.valueRecordUsageCount != 0)
                || (this.propertyDRUsageCount != 0)
                || (this.qomRecordUsageCount != 0)
                || (this.classRecordUsages != 0)
                || (this.classDRUsageCount != 0)
                || (this.valueSVUsageCount != 0)
                || (this.uomsvUsageCount != 0)
                || (this.qomsvUsageCount != 0));
    }

    /**
     * Returns the sum of all ontology usage counts for the specified concept.
     *
     * @return The {@code int} sum of all ontology usages
     */
    public int getOntologyUsageCount()
    {
        return (this.languageOntologyTermUsageCount+this.languageOntologyDefinitionUsageCount);
    }

    public void setClassDRUsages(int classDRUsageCount)
    {
        this.classDRUsageCount = classDRUsageCount;
    }

    public int getClassDRUsageCount()
    {
        return this.classDRUsageCount;
    }

    public void setClassRecordUsages(int classRecordUsageCount)
    {
        this.classRecordUsages = classRecordUsageCount;
    }

    public int getClassRecordUsages()
    {
        return this.classRecordUsages;
    }

    public void setConceptGraphRelationalUsages(int conceptGraphRelationalUsageCount)
    {
        this.conceptGraphRelationalUsageCount = conceptGraphRelationalUsageCount;
    }

    public int getConceptGraphRelationalUsageCount()
    {
        return this.conceptGraphRelationalUsageCount;
    }

    public void setDataRequirementDRUsages(int dataRequirementDRUsageCount)
    {
        this.dataRequirementDRUsageCount = dataRequirementDRUsageCount;
    }

    public int getDataRequirementDRUsageCount()
    {
        return this.dataRequirementDRUsageCount;
    }

    public void setDataRequirementRecordUsages(int dataRequirementRecordUsageCount)
    {
        this.dataRequirementRecordUsageCount = dataRequirementRecordUsageCount;
    }

    public int getDataRequirementRecordUsageCount()
    {
        return this.dataRequirementRecordUsageCount;
    }

    public void setDRInstanceDRUsages(int drInstanceDRUsageCount)
    {
        this.drInstanceDRUsageCount = drInstanceDRUsageCount;
    }

    public int getDRInstanceDRUsageCount()
    {
        return this.drInstanceDRUsageCount;
    }

    public void setDRInstanceRecordUsages(int drInstanceRecordUsageCount)
    {
        this.drInstanceRecordUsageCount = drInstanceRecordUsageCount;
    }

    public int getDRInstanceRecordUsageCount()
    {
        return this.drInstanceRecordUsageCount;
    }

    public void setDataTypeDataStructureUsages(int dataTypeDataStructureUsageCount)
    {
        this.dataTypeDataStructureUsageCount = dataTypeDataStructureUsageCount;
    }

    public int getDataTypeDataStructureUsageCount()
    {
        return this.dataTypeDataStructureUsageCount;
    }

    public void setLanguageTerminologyUsages(int languageTerminologyUsageCount)
    {
        this.languageTerminologyUsageCount = languageTerminologyUsageCount;
    }

    public int getLanguageTerminologyUsageCount()
    {
        return this.languageTerminologyUsageCount;
    }

    public void setLanguageOntologyTermUsages(int languageOntologyTermUsageCount)
    {
        this.languageOntologyTermUsageCount = languageOntologyTermUsageCount;
    }

    public int getLanguageOntologyTermUsageCount()
    {
        return this.languageOntologyTermUsageCount;
    }

    public void setLanguageOntologyDefinitionUsages(int languageOntologyDefinitionUsageCount)
    {
        this.languageOntologyDefinitionUsageCount = languageOntologyDefinitionUsageCount;
    }

    public int getLanguageOntologyDefinitionUsageCount()
    {
        return this.languageOntologyDefinitionUsageCount;
    }

    public void setOrganizationOrgStructureUsages(int organizationOrgStructureUsageCount)
    {
        this.organizationOrgStructureUsageCount = organizationOrgStructureUsageCount;
    }

    public int getOrganizationOrgStructureUsageCount()
    {
        return this.organizationOrgStructureUsageCount;
    }

    public void setPropertyDRUsages(int propertyDRUsageCount)
    {
        this.propertyDRUsageCount = propertyDRUsageCount;
    }

    public int getPropertyDRUsageCount()
    {
        return this.propertyDRUsageCount;
    }

    public void setPropertyRecordUsages(int propertyRecordUsageCount)
    {
        this.propertyRecordUsageCount = propertyRecordUsageCount;
    }

    public int getPropertyRecordUsageCount()
    {
        return this.propertyRecordUsageCount;
    }

    public void setQOMRecordValueUsages(int qomRecordUsageCount)
    {
        this.qomRecordUsageCount = qomRecordUsageCount;
    }

    public int getQOMRecordUsageCount()
    {
        return this.qomRecordUsageCount;
    }

    public void setQOMSVUsages(int qomsvUsageCount)
    {
        this.qomsvUsageCount = qomsvUsageCount;
    }

    public int getQOMSVUsageCount()
    {
        return this.qomsvUsageCount;
    }

    public void setUOMRecordValueUsages(int uomRecordValueUsageCount)
    {
        this.uomRecordValueUsageCount = uomRecordValueUsageCount;
    }

    public int getUOMRecordValueUsageCount()
    {
        return this.uomRecordValueUsageCount;
    }

    public void setUOMSVUsages(int uomsvUsageCount)
    {
        this.uomsvUsageCount = uomsvUsageCount;
    }

    public int getUOMSVUsageCount()
    {
        return this.uomsvUsageCount;
    }

    public void setValueRecordUsages(int valueRecordUsageCount)
    {
        this.valueRecordUsageCount = valueRecordUsageCount;
    }

    public int getValueRecordUsageCount()
    {
        return this.valueRecordUsageCount;
    }

    public void setValueSVUsages(int valueSVUsageCount)
    {
        this.valueSVUsageCount = valueSVUsageCount;
    }

    public int getValueSVUsageCount()
    {
        return this.valueSVUsageCount;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("T8ConceptUsages{");
        toStringBuilder.append("conceptType=").append(this.conceptType);
        toStringBuilder.append(",conceptID=").append(this.conceptID);
        toStringBuilder.append(",languageOntologyDefinitionUsageCount=").append(this.languageOntologyDefinitionUsageCount);
        toStringBuilder.append(",organizationOrgStructureUsageCount=").append(this.organizationOrgStructureUsageCount);
        toStringBuilder.append(",conceptGraphRelationalUsageCount=").append(this.conceptGraphRelationalUsageCount);
        toStringBuilder.append(",dataTypeDataStructureUsageCount=").append(this.dataTypeDataStructureUsageCount);
        toStringBuilder.append(",dataRequirementRecordUsageCount=").append(this.dataRequirementRecordUsageCount);
        toStringBuilder.append(",languageOntologyTermUsageCount=").append(this.languageOntologyTermUsageCount);
        toStringBuilder.append(",languageTerminologyUsageCount=").append(this.languageTerminologyUsageCount);
        toStringBuilder.append(",dataRequirementDRUsageCount=").append(this.dataRequirementDRUsageCount);
        toStringBuilder.append(",drInstanceRecordUsageCount=").append(this.drInstanceRecordUsageCount);
        toStringBuilder.append(",uomRecordValueUsageCount=").append(this.uomRecordValueUsageCount);
        toStringBuilder.append(",propertyRecordUsageCount=").append(this.propertyRecordUsageCount);
        toStringBuilder.append(",drInstanceDRUsageCount=").append(this.drInstanceDRUsageCount);
        toStringBuilder.append(",valueRecordUsageCount=").append(this.valueRecordUsageCount);
        toStringBuilder.append(",propertyDRUsageCount=").append(this.propertyDRUsageCount);
        toStringBuilder.append(",qomRecordUsageCount=").append(this.qomRecordUsageCount);
        toStringBuilder.append(",classRecordUsages=").append(this.classRecordUsages);
        toStringBuilder.append(",classDRUsageCount=").append(this.classDRUsageCount);
        toStringBuilder.append(",valueSVUsageCount=").append(this.valueSVUsageCount);
        toStringBuilder.append(",uomsvUsageCount=").append(this.uomsvUsageCount);
        toStringBuilder.append(",qomsvUsageCount=").append(this.qomsvUsageCount);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}