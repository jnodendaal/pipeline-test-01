package com.pilog.t8.data.document.datarequirement.value;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class AttachmentListRequirement extends ValueRequirement
{
    public AttachmentListRequirement()
    {
        super(RequirementType.ATTACHMENT);
    }

    public boolean isAllowDuplicateFilenames()
    {
        return Boolean.TRUE.equals(getAttribute(RequirementAttribute.ALLOW_DUPLICATE_FILENAMES.toString()));
    }

    public int getMaximumAttachmentCount()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.MAXIMUM_ATTACHMENTS.toString());
        return value != null ? value.intValue() : -1;
    }

    public long getFileSizeRestriction()
    {
        BigDecimal value;

        value = (BigDecimal)getAttribute(RequirementAttribute.FILE_SIZE_RESTRICTION.toString());
        return value != null ? value.longValue() : -1;
    }

    public String getFileTypeRestriction()
    {
        return (String)getAttribute(RequirementAttribute.FILE_TYPE_RESTRICTION.toString());
    }

    @Override
    public ArrayList<RecordValue> buildRecordValues(List<Map<String, Object>> valueData, Integer parentValueSequence)
    {
        ArrayList<RecordValue> recordValues;
        ArrayList<Map<String, Object>> dataRows;
        HashMap<String, Object> filterValues;
        PropertyRequirement propertyRequirement;
        AttachmentList attachmentList;

        propertyRequirement = getParentPropertyRequirement();
        attachmentList = new AttachmentList(this);

        filterValues = new HashMap<>();
        filterValues.put("SECTION_ID", propertyRequirement.getParentSectionRequirement().getConceptID());
        filterValues.put("PROPERTY_ID", propertyRequirement.getConceptID());
        filterValues.put("DATA_TYPE_ID", getRequirementType().getID());
        filterValues.put("DATA_TYPE_SEQUENCE", getSequence());
        filterValues.put("PARENT_VALUE_SEQUENCE", parentValueSequence);
        dataRows = CollectionUtilities.getFilteredDataRows(valueData, filterValues);

        recordValues = new ArrayList<>();
        recordValues.add(attachmentList);
        for (Map<String, Object> dataRow : dataRows)
        {
            String value;

            value = (String)dataRow.get("VALUE");
            if (!Strings.isNullOrEmpty(value))
            {
                attachmentList.addAttachment(value);
            }
        }

        // Return the constructed value.
        return recordValues;
    }
}
