package com.pilog.t8.data.document;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8LRUCachedTerminologyProvider implements CachedTerminologyProvider
{
    private final TerminologyProvider terminologyProvider;
    private final T8LRUTerminologyCache terminologyCache;
    private String defaultLanguageId;
    private boolean logCacheMisses;
    private boolean cacheNonExistence;
    private boolean restrictToCache;

    public T8LRUCachedTerminologyProvider(TerminologyProvider terminologyProvider, int size)
    {
        this.terminologyCache = new T8LRUTerminologyCache(size);
        this.terminologyProvider = terminologyProvider;
        this.logCacheMisses = false;
        this.cacheNonExistence = true;
        this.restrictToCache = false;
    }

    /**
     * If enabled, a log message will be printed every time a requested concept
     * is not found in the cache.
     * @param enabled The flag indicating whether or not log printouts are
     * enabled.
     */
    public void setDebugLogCacheMisses(boolean enabled)
    {
        this.logCacheMisses = enabled;
    }

    /**
     * If enabled, when a requested concept is not found in the local cache nor
     * the terminology provider, an empty concept terminology entry will be
     * created and added to the local cache for the concept in order to prevent
     * subsequent requests to the underlying terminology provider.
     * @param cacheNonExistence Flag to indicate whether or not cache entries
     * should be created for unfound requests.
     */
    public void setCacheNonExistence(boolean cacheNonExistence)
    {
        this.cacheNonExistence = cacheNonExistence;
    }

    /**
     * If this settings is enabled, no requests will be forwarded to the
     * underlying terminology provider and all requests will be serviced from
     * the local cache only.
     * @param restrictToCache Flag to indicate whether or not requests will be
     * limited to the local cache.
     */
    public void setRestrictToCache(boolean restrictToCache)
    {
        this.restrictToCache = restrictToCache;
    }

    @Override
    public void setLanguage(String languageId)
    {
        defaultLanguageId = languageId;
        terminologyCache.setLanguage(languageId);
    }

    @Override
    public void clearCache()
    {
        terminologyCache.clearCache();
    }

    @Override
    public String getCode(String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(null, conceptID);
        return terminology != null ? terminology.getCode() : null;
    }

    @Override
    public String getTerm(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getTerm() : null;
    }

    @Override
    public String getAbbreviation(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getAbbreviation() : null;
    }

    @Override
    public String getDefinition(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getDefinition() : null;
    }

    @Override
    public void addTerminology(T8ConceptTerminology conceptTerminology)
    {
        terminologyCache.addConceptTerminology(conceptTerminology);
    }

    @Override
    public void addTerminology(List<T8ConceptTerminology> conceptTerminologyList)
    {
        for (T8ConceptTerminology conceptTerminology : conceptTerminologyList)
        {
            if (conceptTerminology.getLanguageId() == null)
            {
                T8ConceptTerminology terminologyCopy;

                terminologyCopy = conceptTerminology.copy();
                terminologyCopy.setLanguageId(defaultLanguageId);
                terminologyCache.addConceptTerminology(terminologyCopy);
            }
            else
            {
                terminologyCache.addConceptTerminology(conceptTerminology);
            }
        }
    }

    @Override
    public void addTerminology(String languageId, T8OntologyConceptType conceptType, String conceptId, String codeId, String code, String termId, String term, String abbreviationId, String abbreviation, String definitionId, String definition)
    {
        terminologyCache.addTerminology(languageId != null ? languageId : defaultLanguageId, conceptType, conceptId, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);
    }

    @Override
    public T8ConceptTerminology getTerminology(String languageId, String conceptId)
    {
        String resolvedLanguageId;

        // Make sure the input parameters are valid.
        if (conceptId == null) throw new IllegalArgumentException("Null Concept ID.");

        // Resolve the language ID to use.
        resolvedLanguageId = (languageId != null ? languageId : defaultLanguageId);
        if (terminologyCache.isTerminologyCached(resolvedLanguageId, conceptId))
        {
            return terminologyCache.getTerminology(resolvedLanguageId, conceptId);
        }
        else if (!restrictToCache)
        {
            T8ConceptTerminology conceptTerminology;

            // Retrieve the terminology, add it to the cache and return it.
            if (logCacheMisses) T8Log.log("Terminology not cached for concept: " + conceptId + " in language: " + languageId);
            conceptTerminology = terminologyProvider.getTerminology(resolvedLanguageId, conceptId);
            if (conceptTerminology != null)
            {
                terminologyCache.setConceptTerminology(conceptTerminology);
                return conceptTerminology;
            }
            else if (cacheNonExistence)
            {
                T8ConceptTerminology emptyTerminology;

                // We have to cache the null values to prevent subsequent calls to this method from trying to fetch the same terminology from the database.
                T8Log.log("Caching unfound request: " + conceptId);
                emptyTerminology = new T8ConceptTerminology(T8OntologyConceptType.VALUE, conceptId);
                emptyTerminology.setLanguageId(resolvedLanguageId);
                terminologyCache.setConceptTerminology(emptyTerminology);
                return emptyTerminology;
            }
            else return null;
        }
        else if (cacheNonExistence)
        {
            T8ConceptTerminology emptyTerminology;

            // We have to cache the null values to prevent subsequent calls to this method from trying to fetch the same terminology from the database.
            System.out.println("Caching unfound request: " + conceptId);
            emptyTerminology = new T8ConceptTerminology(T8OntologyConceptType.VALUE, conceptId);
            emptyTerminology.setLanguageId(resolvedLanguageId);
            terminologyCache.setConceptTerminology(emptyTerminology);
            return emptyTerminology;
        }
        else return null;
    }

    @Override
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIDCollection, Collection<String> conceptIDSet)
    {
        T8ConceptTerminologyList resultList;
        Set<String> languageIDSet;

        // First make sure we some valid languages set.
        if (languageIDCollection == null)
        {
            if (defaultLanguageId == null) throw new RuntimeException("No terminology languages specified and no default language set.");
            else
            {
                languageIDSet = new HashSet<String>();
                languageIDSet.add(defaultLanguageId);
            }
        }
        else languageIDSet = new HashSet<String>(languageIDCollection);

        // Cache all required terminology.
        cacheTerminology(languageIDSet, conceptIDSet);

        // Get all the terminologies we need.
        resultList = new T8ConceptTerminologyList();
        for (String conceptID : conceptIDSet)
        {
            for (String languageID : languageIDSet)
            {
                resultList.add(terminologyCache.getTerminology(languageID, conceptID));
            }
        }

        // Return the results.
        return resultList;
    }


    @Override
    public void cacheTerminology(Collection<String> languageIDCollection, Collection<String> conceptIDSet)
    {
        List<T8ConceptTerminology> terminologyList;
        Set<String> languageIdSet;
        Set<String> conceptsToCache;

        // First make sure we some valid languages set.
        if (languageIDCollection == null)
        {
            if (defaultLanguageId == null) throw new RuntimeException("No languages specified for caching and no default language set.");
            else
            {
                languageIdSet = new HashSet<String>();
                languageIdSet.add(defaultLanguageId);
            }
        }
        else languageIdSet = new HashSet<String>(languageIDCollection);

        // Create a list of concepts for which the required terminology has not already been cached.
        conceptsToCache = new HashSet<String>();
        for (String conceptID : conceptIDSet)
        {
            for (String languageID : languageIdSet)
            {
                if (!terminologyCache.isTerminologyCached(languageID, conceptID))
                {
                    conceptsToCache.add(conceptID);
                }
            }
        }

        // Now retrieve the rest.
        terminologyList = terminologyProvider.getTerminology(languageIdSet, conceptsToCache);
        for (T8ConceptTerminology retrievedTerminology : terminologyList)
        {
            // Add the retrieved terminology to the cache.
            terminologyCache.setConceptTerminology(retrievedTerminology);
        }

        // We have to cache dummy objects for all terminology that we could not retrieve to ensure that the retrieval is not performed again.
        for (String conceptID : conceptIDSet)
        {
            for (String languageID : languageIdSet)
            {
                if (!terminologyCache.isTerminologyCached(languageID, conceptID))
                {
                    T8ConceptTerminology emptyTerminology;

                    emptyTerminology = new T8ConceptTerminology(T8OntologyConceptType.VALUE, conceptID);
                    emptyTerminology.setLanguageId(languageID);
                    terminologyCache.setConceptTerminology(emptyTerminology);
                }
            }
        }
    }
}

