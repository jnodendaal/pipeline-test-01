package com.pilog.t8.data.document.datarecord.attachment;

import com.pilog.t8.file.T8FileDetails;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8AttachmentDetails extends T8FileDetails implements Serializable
{
    private String attachmentId;
    private String originalFileName;

    public T8AttachmentDetails()
    {
    }

    public T8AttachmentDetails(String attachmentId, T8FileDetails fileDetails, String originalFileName)
    {
        this(attachmentId, fileDetails.getContextIid(), fileDetails.getContextId(), fileDetails.getFilePath(), originalFileName, fileDetails.getFileSize(), fileDetails.getMD5Checksum(), fileDetails.getMediaType());
    }

    public T8AttachmentDetails(String attachmentId, String contextIid, String contextId, String filePath, String originalFileName, long fileSize, String md5Checksum, String mediaType)
    {
        super(contextIid, contextId, filePath, fileSize, false, md5Checksum, mediaType);
        this.attachmentId = attachmentId;
        this.originalFileName = originalFileName;
    }

    public String getAttachmentId()
    {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId)
    {
        this.attachmentId = attachmentId;
    }

    public String getOriginalFileName()
    {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName)
    {
        this.originalFileName = originalFileName;
    }

    @Override
    public T8AttachmentDetails copy()
    {
        T8AttachmentDetails copy;

        copy = new T8AttachmentDetails();
        copy.setAttachmentId(attachmentId);
        copy.setDirectory(directory);
        copy.setContextId(contextId);
        copy.setContextIid(contextIid);
        copy.setMediaType(mediaType);
        copy.setOriginalFileName(originalFileName);
        copy.setFilePath(filePath);
        copy.setFileSize(fileSize);
        copy.setMD5Checksum(md5Checksum);
        return copy;
    }

    @Override
    public String toString()
    {
        StringBuffer buffer;

        buffer = new StringBuffer();
        buffer.append("T8FileDetails[");
        buffer.append("attachmentId=");
        buffer.append(attachmentId);
        buffer.append("fileContextInstanceID=");
        buffer.append(contextIid);
        buffer.append(", fileContextID=");
        buffer.append(contextId);
        buffer.append(", originalFileName=");
        buffer.append(originalFileName);
        buffer.append(", filePath=");
        buffer.append(filePath);
        buffer.append(", md5Checksum=");
        buffer.append(md5Checksum);
        buffer.append("]");

        return buffer.toString();
    }
}
