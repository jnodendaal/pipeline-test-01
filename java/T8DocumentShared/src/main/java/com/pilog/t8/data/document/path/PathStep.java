package com.pilog.t8.data.document.path;

import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.object.T8DataObjectProvider;

/**
 * @author Bouwer du Preez
 */
public interface PathStep
{
    public void setTerminologyProvider(TerminologyProvider terminologyProvider);
    public void setOntologyProvider(OntologyProvider ontologyProvider);
    public void setRecordProvider(DataRecordProvider recordProvider);
    public void setDataObjectProvider(T8DataObjectProvider dataObjectProvider);
    public Object evaluateStep(Object rootInput, Object input);
}
