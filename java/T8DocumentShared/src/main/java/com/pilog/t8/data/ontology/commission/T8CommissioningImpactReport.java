package com.pilog.t8.data.ontology.commission;

import java.io.Serializable;

/**
 * This class functions as a report that details the impact of a proposed
 * ontology commissioning on the existing data records in the system.
 * 
 * @author Bouwer du Preez
 */
public class T8CommissioningImpactReport implements Serializable
{
    private int affectedRecordCount; // The number of records affected by the proposed commissioning.
    private int affectedPropertyCount; // The number of properties (per record) affected by the proposed commissioning.
    private int dataTypeChangeCount; // The number of data types changes that will result from the proposed commissioning.
    
    public T8CommissioningImpactReport()
    {
        affectedRecordCount = -1;
        affectedPropertyCount = -1;
        dataTypeChangeCount = -1;
    }

    public int getAffectedRecordCount()
    {
        return affectedRecordCount;
    }

    public void setAffectedRecordCount(int affectedRecordCount)
    {
        this.affectedRecordCount = affectedRecordCount;
    }

    public int getAffectedPropertyCount()
    {
        return affectedPropertyCount;
    }

    public void setAffectedPropertyCount(int affectedPropertyCount)
    {
        this.affectedPropertyCount = affectedPropertyCount;
    }

    public int getDataTypeChangeCount()
    {
        return dataTypeChangeCount;
    }

    public void setDataTypeChangeCount(int dataTypeChangeCount)
    {
        this.dataTypeChangeCount = dataTypeChangeCount;
    }
}
