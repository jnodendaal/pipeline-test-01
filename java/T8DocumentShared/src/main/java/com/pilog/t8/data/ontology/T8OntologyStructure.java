package com.pilog.t8.data.ontology;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyStructure implements Serializable
{
    private String structureID;
    private final T8OntologyClass ontology;

    public T8OntologyStructure(String ontologyID, T8OntologyClass structure)
    {
        this.structureID = ontologyID;
        this.ontology = structure;
    }

    public String getID()
    {
        return structureID;
    }

    public void setID(String structureID)
    {
        this.structureID = structureID;
    }

    public String getRootClassID()
    {
        return ontology.getID();
    }

    /**
     * Returns true if the specified data type is a descendant of the parent
     * data type supplied.
     * @param dataTypeID The parent data type from which descendants will
     * be checked.
     * @param possibleDescendantDataTypeID The data type to compare to the
     * descendants of the specified parent.
     * @return
     */
    public boolean isDescendant(String dataTypeID, String possibleDescendantDataTypeID)
    {
        T8OntologyClass parentNode;

        parentNode = getNode(dataTypeID);
        if (parentNode != null)
        {
            return parentNode.getDescendantClassIDList().contains(possibleDescendantDataTypeID);
        }
        else throw new IllegalArgumentException("Parent data type not found: " + dataTypeID);
    }

    /**
     * This method finds the concept type of the data contained by the specified
     * data type by traversing the data structure towards the root until the
     * primary data type is found.
     * @param dataTypeID The data type for which to determine the content
     * concept type.
     * @return The concept type of the concepts contained by the specified
     * data type group.
     */
    public T8OntologyConceptType getConceptType(String dataTypeID)
    {
        // Iterate over the path ID list until we find the primary data type.
        for (String pathID : getClassPathIDList(dataTypeID))
        {
            T8OntologyConceptType conceptType;

            conceptType = T8OntologyConceptType.getConceptTypeFromDataType(pathID);
            if (conceptType != null) return conceptType;
        }

        // No concept type found for the specified data type.
        return null;
    }

    /**
     * Finds and returns the primary data type to which the specified data type
     * belongs.
     * @param dataTypeID The data type ID for which to find the primary data
     * type.
     * @return The ID of the primary data type to which the specified data type
     * belongs.  Returns null if the specified data type was not found in the
     * data structure.
     */
    public String getPrimaryDataTypeID(String dataTypeID)
    {
        List<String> pathIDList;

        // Get the lis of data types ID's from the root of the data structure to the type specified.
        pathIDList = getClassPathIDList(dataTypeID);

        // We have now got to iterate over the path ID list from the bottom up (from the type we specified) until we find the first standard type.
        for (int index = pathIDList.size()-1; index >= 0; index--)
        {
            String pathID;

            pathID = pathIDList.get(index);
            if (T8OntologyConceptType.getConceptTypeFromDataType(pathID) != null)
            {
                // If we find a concept type that corresponds to the data type ID we are at, we know the data type ID is a standard type.
                return pathID;
            }
        }

        // No concept type found for the specified data type.
        return null;
    }

    public boolean containsNode(String dataTypeID)
    {
        return getNode(dataTypeID) != null;
    }

    public List<String> getClassIDList()
    {
        List<T8OntologyClass> dataTypes;

        dataTypes = getNodes();
        if (dataTypes != null)
        {
            List<String> idList;

            idList = new ArrayList<String>();
            for (T8OntologyClass dataType : dataTypes)
            {
                idList.add(dataType.getID());
            }

            return idList;
        }
        else return null;
    }

    public List<T8OntologyClass> getNodes()
    {
        if (ontology != null)
        {
            List<T8OntologyClass> dataTypes;

            dataTypes = ontology.getDescendantNodes();
            dataTypes.add(0, ontology);
            return dataTypes;
        }
        else return null;
    }

    public T8OntologyClass getNode(String ontologyClassID)
    {
        LinkedList<T8OntologyClass> nodeList;

        nodeList = new LinkedList<T8OntologyClass>();
        nodeList.add(ontology);
        while (nodeList.size() > 0)
        {
            T8OntologyClass nextNode;

            nextNode = nodeList.pop();
            if (nextNode.getID().equals(ontologyClassID))
            {
                return nextNode;
            }
            else
            {
                nodeList.addAll(nextNode.getChildNodes());
            }
        }

        return null;
    }

    /**
     * This method returns the list of data type ID's from the root to the
     * specified node.
     * @param ontologyClassID The data type ID for which to retrieve the path ID set.
     * @return The list of path data type ID's to the specified data type.
     */
    public List<String> getClassPathIDList(String ontologyClassID)
    {
        T8OntologyClass node;
        List<String> idList;

        idList = new ArrayList<String>();
        node = getNode(ontologyClassID);
        if (node != null)
        {
            for (T8OntologyClass pathNode : node.getPathNodes())
            {
                idList.add(pathNode.getID());
            }
        }

        return idList;
    }

    public List<String> getDescendantClassIDList(String ontologyClassID)
    {
        T8OntologyClass node;

        node = getNode(ontologyClassID);
        if (node != null) return node.getDescendantClassIDList();
        else throw new RuntimeException("Node not found: " + ontologyClassID);
    }

    public List<String> getChildClassIDList(String ontologyClassID)
    {
        T8OntologyClass node;

        node = getNode(ontologyClassID);
        if (node != null) return node.getChildDataTypeIDList();
        else throw new RuntimeException("Node not found: " + ontologyClassID);
    }

    public int getSize()
    {
        if (ontology != null)
        {
            return ontology.countDescendants() + 1;
        }
        else return 0;
    }

    public List<String> getNestedIDList()
    {
        List<String> nestedList;

        nestedList = new ArrayList<String>();
        if (ontology != null) ontology.addNestedIDList(nestedList);
        return nestedList;
    }
}
