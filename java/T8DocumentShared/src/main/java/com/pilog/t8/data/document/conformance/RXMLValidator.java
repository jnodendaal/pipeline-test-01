package com.pilog.t8.data.document.conformance;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.transformation.DataRequirementTransformer;
import com.pilog.t8.utilities.xml.dom.DOMHandler;
import com.pilog.t8.utilities.xml.iso.ISOConstants;
import com.pilog.t8.utilities.xml.iso.RXMLHandler;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class RXMLValidator
{
    private DataRequirement dataRequirement;

    public RXMLValidator(Document ixmlDocument)
    {
        DataRequirementTransformer transformer;

        transformer = new DataRequirementTransformer();
        dataRequirement = transformer.transform(ixmlDocument);
    }

    public RXMLValidator(DataRequirement dataRequirement)
    {
        this.dataRequirement = dataRequirement;
    }

    public void validate(Document rxmlDocument) throws Exception
    {
        ArrayList<Element> itemElements;

        itemElements = RXMLHandler.getItemElements(rxmlDocument);
        for (Element itemElement : itemElements)
        {
            validateItem(itemElement);
        }
    }

    private void validateItem(Element itemElement) throws Exception
    {
        String classRef;
        String localID;

        classRef = itemElement.getAttribute(ISOConstants.R_ATTRIBUTE_CLASS_REF).trim();
        if (classRef.length() == 0) classRef = null;
        localID = itemElement.getAttribute(ISOConstants.R_ATTRIBUTE_LOCAL_ID).trim();
        if (localID.length() == 0) localID = null;

        if (classRef != null)
        {
            SectionRequirement classRequirement;

            classRequirement = dataRequirement.getSectionRequirement(classRef);
            if (classRequirement != null)
            {
                ArrayList<Element> propertyElements;

                propertyElements = RXMLHandler.getPropertyElements(itemElement);
                for (Element propertyElement : propertyElements)
                {
                    PropertyRequirement propertyRequirement;
                    String propertyRef;

                    propertyRef = propertyElement.getAttribute(ISOConstants.R_ATTRIBUTE_PROPERTY_REF);
                    propertyRequirement = classRequirement.getPropertyRequirement(propertyRef);
                    if (propertyRequirement != null)
                    {
                        validateProperty(propertyElement, propertyRequirement);
                    }
                    else throw new Exception("Invalid property found.  Class ref: " + classRef + ", Property ref: " + propertyRef);
                }
            }
            else throw new Exception("Invalid item found.  Class ref: " + classRef + ", Local ID: " + localID);
        }
        else throw new Exception("Missing class ref on item element.");
    }

    private void validateProperty(Element propertyElement, PropertyRequirement propertyRequirement) throws Exception
    {
        String propertyRef;

        propertyRef = propertyElement.getAttribute(ISOConstants.R_ATTRIBUTE_PROPERTY_REF).trim();
        if (propertyRef != null)
        {
            ArrayList<Element> valueElements;

            valueElements = DOMHandler.getChildElements(propertyElement);
            if (valueElements.size() == 1)
            {
                if (!matchElement(valueElements.get(0), propertyRequirement.getValueRequirement()))
                {
                    throw new Exception("Data Type mismatch.  Class ref: " + propertyRequirement.getParentSectionRequirement().getRequirementType() + ", Property ref: " + propertyRef);
                }
            }
            else if (valueElements.size() > 1) throw new Exception("Too many value elements in property: " + propertyRef);
            else throw new Exception("No value element found in property: " + propertyRef);
        }
        else throw new Exception("Missing property ref on property element in class: " +  RXMLHandler.getPropertyClassRef(propertyElement));
    }

    private boolean matchElement(Element valueElement, ValueRequirement valueRequirement)
    {
        String localName;
        RequirementType requirementType;

        localName = valueElement.getLocalName();
        requirementType = valueRequirement.getRequirementType();
        if (requirementType.equals(RequirementType.BAG_TYPE))
        {
            if (!ISOConstants.R_ELEMENT_BAG_VALUE.equals(localName))
            {
                return false;
            }
            else
            {
                return matchAnyRequirement(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.BOOLEAN_TYPE))
        {
            return (ISOConstants.R_ELEMENT_BOOLEAN_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.CHOICE_TYPE))
        {
            ArrayList<Element> elements;

            elements = new ArrayList<Element>();
            elements.add(valueElement);
            return matchAnyRequirement(elements, valueRequirement.getSubRequirements());
        }
        else if (requirementType.equals(RequirementType.COMPLEX_TYPE))
        {
            return (ISOConstants.R_ELEMENT_COMPLEX_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.COMPOSITE_TYPE))
        {
            if (!ISOConstants.R_ELEMENT_COMPOSITE_VALUE.equals(localName))
            {
                return false;
            }
            else
            {
                return matchAllRequirements(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.CONTROLLED_CONCEPT))
        {
            if (!ISOConstants.R_ELEMENT_CONTROLLED_VALUE.equals(localName))
            {
                return false;
            }
            else
            {
                String valueRef;

                valueRef = valueElement.getAttribute(ISOConstants.R_ATTRIBUTE_VALUE_REF);
                if (valueRef == null)
                {
                    return false;
                }
                else
                {
                    return valueRequirement.getSubRequirements(null, valueRef, null).size() > 0;
                }
            }
        }
        else if (requirementType.equals(RequirementType.CURRENCY_TYPE))
        {
            return (ISOConstants.R_ELEMENT_CURRENCY_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.DATE_TIME_TYPE))
        {
            return (ISOConstants.R_ELEMENT_DATE_TIME_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.DATE_TYPE))
        {
            return (ISOConstants.R_ELEMENT_DATE_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.FIELD_TYPE))
        {
            String propertyRef;

            propertyRef = valueRequirement.getValue();
            if (!ISOConstants.R_ELEMENT_FIELD.equals(localName))
            {
                return false;
            }
            else if (!propertyRef.equals(valueElement.getAttribute(ISOConstants.R_ATTRIBUTE_PROPERTY_REF)))
            {
                return false;
            }
            else
            {
                return matchAllRequirements(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.FILE_TYPE))
        {
            return (ISOConstants.R_ELEMENT_FILE_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.IMAGINARY_PART_TYPE))
        {
            return true;
        }
        else if (requirementType.equals(RequirementType.INTEGER_TYPE))
        {
            return (ISOConstants.R_ELEMENT_INTEGER_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.DOCUMENT_REFERENCE))
        {
            return (ISOConstants.R_ELEMENT_ITEM_REFERENCE_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.LANGUAGE_TYPE))
        {
            return true;
        }
        else if (requirementType.equals(RequirementType.LOCALIZED_TEXT_TYPE))
        {
            return true;
        }
        else if (requirementType.equals(RequirementType.LOWER_BOUND_NUMBER))
        {
            if (!ISOConstants.R_ELEMENT_LOWER_VALUE.equals(localName))
            {
                return false;
            }
            else
            {
                return matchAllRequirements(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.MEASURED_RANGE))
        {
            String uomRef;

            uomRef = valueElement.getAttribute(ISOConstants.R_ATTRIBUTE_UOM_REF);
            if (!ISOConstants.R_ELEMENT_MEASURE_RANGE_VALUE.equals(localName))
            {
                return false;
            }
            else if (valueRequirement.getDescendentRequirements(null, uomRef, null).size() == 0)
            {
                return false;
            }
            else
            {
                return matchAllRequirements(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.MEASURED_NUMBER))
        {
            String uomRef;

            uomRef = valueElement.getAttribute(ISOConstants.R_ATTRIBUTE_UOM_REF);
            if (!ISOConstants.R_ELEMENT_MEASURE_SINGLE_NUMBER_VALUE.equals(localName))
            {
                return false;
            }
            else if (valueRequirement.getDescendentRequirements(null, uomRef, null).size() == 0)
            {
                return false;
            }
            else
            {
                return matchAllRequirements(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.PRESCRIBED_CURRENCY_TYPE))
        {
            return (ISOConstants.R_ELEMENT_CURRENCY_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.QUALIFIER_OF_MEASURE))
        {
            return true;
        }
        else if (requirementType.equals(RequirementType.UNIT_OF_MEASURE))
        {
            return true;
        }
        else if (requirementType.equals(RequirementType.RATIONAL_TYPE))
        {
            return (ISOConstants.R_ELEMENT_RATIONAL_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.REAL_PART_TYPE))
        {
            return true;
        }
        else if (requirementType.equals(RequirementType.REAL_TYPE))
        {
            return (ISOConstants.R_ELEMENT_REAL_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.SEQUENCE_TYPE))
        {
            if (!ISOConstants.R_ELEMENT_SEQUENCE_VALUE.equals(localName))
            {
                return false;
            }
            else
            {
                return matchAnyRequirement(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.SET_TYPE))
        {
            if (!ISOConstants.R_ELEMENT_SET_VALUE.equals(localName))
            {
                return false;
            }
            else
            {
                return matchAnyRequirement(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.STRING_TYPE))
        {
            return (ISOConstants.R_ELEMENT_STRING_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.TIME_TYPE))
        {
            return (ISOConstants.R_ELEMENT_TIME_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.UPPER_BOUND_NUMBER))
        {
            if (!ISOConstants.R_ELEMENT_UPPER_VALUE.equals(localName))
            {
                return false;
            }
            else
            {
                return matchAllRequirements(DOMHandler.getChildElements(valueElement), valueRequirement.getSubRequirements());
            }
        }
        else if (requirementType.equals(RequirementType.ONTOLOGY_CLASS))
        {
            return true;
        }
        else if (requirementType.equals(RequirementType.YEAR_MONTH_TYPE))
        {
            return (ISOConstants.R_ELEMENT_YEAR_MONTH_VALUE.equals(localName));
        }
        else if (requirementType.equals(RequirementType.YEAR_TYPE))
        {
            return (ISOConstants.R_ELEMENT_YEAR_VALUE.equals(localName));
        }
        else return false;
    }

    private boolean matchAllRequirements(ArrayList<Element> elements, ArrayList<ValueRequirement> valueRequirements)
    {
        for (ValueRequirement valueRequirement : valueRequirements)
        {
            boolean matched;

            matched = false;
            for (Element element : elements)
            {
                if (matchElement(element, valueRequirement))
                {
                    matched = true;
                    break;
                }
            }

            if (!matched) return false;
        }

        return true;
    }

    private boolean matchAnyRequirement(ArrayList<Element> elements, ArrayList<ValueRequirement> valueRequirements)
    {
        for (Element element : elements)
        {
            for (ValueRequirement valueRequirement : valueRequirements)
            {
                if (matchElement(element, valueRequirement))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
