package com.pilog.t8.data.document.datarecord.structure;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class DataRecordStructureNode implements Serializable
{
    private DataRecord dataRecord;
    private DataRecordStructureNode parentNode;
    private List<DataRecordStructureNode> childNodes;

    public DataRecordStructureNode(DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
        this.parentNode = null;
        this.childNodes = new ArrayList<DataRecordStructureNode>();
    }

    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    public DataRecordStructureNode getParentNode()
    {
        return parentNode;
    }

    void setParentNode(DataRecordStructureNode parent)
    {
        this.parentNode = parent;
    }

    public String getRecordID()
    {
        return dataRecord != null ? dataRecord.getID() : null;
    }

    public List<DataRecordStructureNode> getChildNodes()
    {
        return childNodes;
    }

    public DataRecordStructureNode getChildNodeAt(int index)
    {
        return childNodes.get(index);
    }

    public int getChildNodeCount()
    {
        return childNodes.size();
    }

    public int getChildIndex(DataRecordStructureNode node)
    {
        return childNodes.indexOf(node);
    }

    public void applyStructureToRecords()
    {
        dataRecord.clearSubRecords();
        for (DataRecordStructureNode node : childNodes)
        {
            dataRecord.addSubRecord(node.getDataRecord());
            node.applyStructureToRecords();
        }
    }

    public List<DataRecord> getDescendantDataRecords()
    {
        List<DataRecord> descendantRecords;

        descendantRecords = new ArrayList<DataRecord>();
        for (DataRecordStructureNode node : childNodes)
        {
            descendantRecords.add(node.getDataRecord());
            descendantRecords.addAll(node.getDescendantDataRecords());
        }

        return descendantRecords;
    }

    public void addChildNode(DataRecordStructureNode node)
    {
        if (node == this) throw new IllegalArgumentException("Attempt to add a node to itself: " + node.getRecordID());
        if (childNodes.contains(node)) throw new IllegalArgumentException("Attempt to add a node twice to the same parent: " + node.getRecordID());

        node.setParentNode(this);
        childNodes.add(node);
    }

    public void clearChildNodes()
    {
        // Unset all parent references.
        for (DataRecordStructureNode childNode : childNodes)
        {
            childNode.setParentNode(null);
        }

        // Clear the child node collection.
        childNodes.clear();
    }

    public boolean removeChildNode(DataRecordStructureNode node)
    {
        if (childNodes.remove(node))
        {
            node.setParentNode(null);
            return true;
        }
        else return false;
    }

    public DataRecordStructureNode findNode(String recordID)
    {
        if ((dataRecord != null) && (Objects.equals(dataRecord.getID(), recordID)))
        {
            return this;
        }
        else
        {
            for (DataRecordStructureNode childNode : childNodes)
            {
                DataRecordStructureNode foundNode;

                foundNode = childNode.findNode(recordID);
                if (foundNode != null) return foundNode;
            }

            return null;
        }
    }

    public Set<String> getChildNodeRecordIDSet()
    {
        Set<String> recordIDSet;

        recordIDSet = new HashSet<String>();
        for (DataRecordStructureNode childNode : childNodes)
        {
            recordIDSet.add(childNode.getRecordID());
        }

        return recordIDSet;
    }

    /**
     * Returns the Set of sub-record ID's that can be added to this node.  That
     * means that all of the sub-record ID's referenced by the data record
     * contained by this node, minus the sub-record ID's for which a child node
     * has already been added.
     * @return The set of sub-record ID's that can still be added to this node.
     */
    public Set<String> getPossibleSubRecordIDSet()
    {
        Set<String> subRecordIDSet;

        subRecordIDSet = dataRecord.getSubRecordReferenceIDSet(true, true);
        subRecordIDSet.removeAll(getChildNodeRecordIDSet());
        return subRecordIDSet;
    }

    /**
     * Returns a list of all descendants of this node to which a record can be
     * added as a child.  If a record can be added to this node, then this node
     * is also included in the returned list.
     * @return Returns a list of all descendants of this node to which a record can be
     * added as a child.
     */
    public List<DataRecordStructureNode> findPossibleParentNodes()
    {
        List<DataRecordStructureNode> parentNodes;

        parentNodes = new ArrayList<DataRecordStructureNode>();
        if (getPossibleSubRecordIDSet().size() > 0)
        {
            parentNodes.add(this);
        }

        for (DataRecordStructureNode childNode : childNodes)
        {
            parentNodes.addAll(childNode.findPossibleParentNodes());
        }

        return parentNodes;
    }

    /**
     * Returns a list of all descendants of this node to which the specified
     * record can be added as a child.  If the record can be added to this node,
     * then this node is also included in the returned list.
     * @param childRecordID The ID of the record for which possible parents are
     * to be found.
     * @return The list of all nodes descendent from this node to which the
     * specified record can still be added as a child node.
     */
    public List<DataRecordStructureNode> findPossibleParentNodes(String childRecordID)
    {
        List<DataRecordStructureNode> parentNodes;

        parentNodes = new ArrayList<DataRecordStructureNode>();
        if (getPossibleSubRecordIDSet().contains(childRecordID))
        {
            parentNodes.add(this);
        }

        for (DataRecordStructureNode childNode : childNodes)
        {
            parentNodes.addAll(childNode.findPossibleParentNodes(childRecordID));
        }

        return parentNodes;
    }

    public List<DataRecordStructureNode> findParentNodes(String childRecordID)
    {
        List<DataRecordStructureNode> parentNodes;

        parentNodes = new ArrayList<DataRecordStructureNode>();
        if ((dataRecord != null) && (Objects.equals(dataRecord.getID(), childRecordID)) && (parentNode != null))
        {
            parentNodes.add(parentNode);
        }

        for (DataRecordStructureNode childNode : childNodes)
        {
            parentNodes.addAll(childNode.findParentNodes(childRecordID));
        }

        return parentNodes;
    }
}
