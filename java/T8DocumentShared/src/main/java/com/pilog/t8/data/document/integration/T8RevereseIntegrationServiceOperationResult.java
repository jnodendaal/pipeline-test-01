package com.pilog.t8.data.document.integration;

import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8RevereseIntegrationServiceOperationResult
{
    private String operationID;
    private String organizationID;
    private String domainType;
    private String operationType;
    private String externalReference;
    private String requestNumber;
    private Map<String, String> inputParameters;

    public T8RevereseIntegrationServiceOperationResult()
    {
    }

    public T8RevereseIntegrationServiceOperationResult(String operationID, String organizationID, String domainType, String operationType, String externalReference, String requestNumber, Map<String, String> inputParameters)
    {
        this.operationID = operationID;
        this.organizationID = organizationID;
        this.domainType = domainType;
        this.operationType = operationType;
        this.externalReference = externalReference;
        this.requestNumber = requestNumber;
        this.inputParameters = inputParameters;
    }

    /**
     * @return the opertionID
     */
    public String getOperationID()
    {
        return operationID;
    }

    /**
     * @param opertionID the opertionID to set
     */
    public void setOperationID(String opertionID)
    {
        this.operationID = opertionID;
    }

    /**
     * @return the oraganizationID
     */
    public String getOrganizationID()
    {
        return organizationID;
    }

    /**
     * @param oraganizationID the oraganizationID to set
     */
    public void setOrganizationID(String oraganizationID)
    {
        this.organizationID = oraganizationID;
    }

    /**
     * @return the domainType
     */
    public String getDomainType()
    {
        return domainType;
    }

    /**
     * @param domainType the domainType to set
     */
    public void setDomainType(String domainType)
    {
        this.domainType = domainType;
    }

    /**
     * @return the operationType
     */
    public String getOperationType()
    {
        return operationType;
    }

    /**
     * @param operationType the operationType to set
     */
    public void setOperationType(String operationType)
    {
        this.operationType = operationType;
    }

    /**
     * @return the interanlReference
     */
    public String getExternalReference()
    {
        return externalReference;
    }

    /**
     * @param externalReference the interanlReference to set
     */
    public void setExternalReference(String externalReference)
    {
        this.externalReference = externalReference;
    }

    /**
     * @return the requestNumber
     */
    public String getRequestNumber()
    {
        return requestNumber;
    }

    /**
     * @param requestNumber the requestNumber to set
     */
    public void setRequestNumber(String requestNumber)
    {
        this.requestNumber = requestNumber;
    }

    /**
     * @return the inputParameters
     */
    public Map<String, String> getInputParameters()
    {
        return inputParameters;
    }

    /**
     * @param inputParameters the inputParameters to set
     */
    public void setInputParameters(Map<String, String> inputParameters)
    {
        this.inputParameters = inputParameters;
    }
}
