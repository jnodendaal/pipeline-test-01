package com.pilog.t8.data.document.datarecord.event;

import com.pilog.t8.data.document.datarecord.RecordValue;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8RecordValueStructureChangedEvent extends T8DataRecordChangedEvent
{
    private final RecordValue recordValue;
    private final List<RecordValue> valuesAdded;
    private final List<RecordValue> valuesRemoved;
    private final Object actor;
    
    public T8RecordValueStructureChangedEvent(RecordValue recordValue, List<RecordValue> valuesAdded, List<RecordValue> valuesRemoved, Object actor)
    {
        super(recordValue.getParentDataRecord());
        this.recordValue = recordValue;
        this.valuesAdded = valuesAdded;
        this.valuesRemoved = valuesRemoved;
        this.actor = actor;
    }
    
    public RecordValue getRecordValue()
    {
        return recordValue;
    }

    public List<RecordValue> getValuesAdded()
    {
        return valuesAdded;
    }

    public List<RecordValue> getValuesRemoved()
    {
        return valuesRemoved;
    }

    public Object getActor()
    {
        return actor;
    }

    @Override
    public String toString()
    {
        return "T8RecordValueStructureChangedEvent{" + "recordValue=" + recordValue + ", valuesAdded=" + valuesAdded + ", valuesRemoved=" + valuesRemoved + '}';
    }
}
