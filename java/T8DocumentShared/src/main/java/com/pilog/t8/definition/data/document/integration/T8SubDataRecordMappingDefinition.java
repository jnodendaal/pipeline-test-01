package com.pilog.t8.definition.data.document.integration;

import com.pilog.t8.definition.data.document.integration.organization.T8DataRecordOrganizationMappingDefinition;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class T8SubDataRecordMappingDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_INTERFACE_MAPPING_SUB_DATA_RECORD";
    public static final String TYPE_IDENTIFIER = "@DT_INTERFACE_MAPPING_SUB_DATA_RECORD";
    public static final String DISPLAY_NAME = "Sub Data Record Mapping";
    public static final String DESCRIPTION = "Specifies how this sub data record should be mapped to an interface";
    public static final String IDENTIFIER_PREFIX = "SRM_";

    public enum Datum
    {
        RECORD_PATH,
        RECORD_INTERFACE_TERM,
        LOAD_INDEPENDENT_RECORDS,
        INTERFACE_ORGANIZATION_MAPPINGS,
        PROPERTY_DEFINITIONS,
        FIELD_DEFINITIONS,
        SUB_RECORD_DEFINITIONS,
        FLAG_MAPPING_DEFINITIONS
    };
// -------- Definition Meta-Data -------- //
    public T8SubDataRecordMappingDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.RECORD_PATH.toString(), "Record Path", "The record path that points to this record."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.RECORD_INTERFACE_TERM.toString(), "Record Interface Term", "The term that will be used to map this record to the interface."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.LOAD_INDEPENDENT_RECORDS.toString(), "Load Independent Records", "Load all child Indpendent records."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INTERFACE_ORGANIZATION_MAPPINGS.toString(), "Interface Organization Mappings", "The list that will be used for the organization mappings of the target system."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.PROPERTY_DEFINITIONS.toString(), "Property Mappings", "The mappings between the data record properties and the property terms in the target system."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FIELD_DEFINITIONS.toString(), "Field Mappings", "The mappings between the data record fields and the property terms in the target system."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.SUB_RECORD_DEFINITIONS.toString(), "Sub Records", "All the sub records to be evaluated."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.FLAG_MAPPING_DEFINITIONS.toString(), "Flag Mappings", "The mapping for the flag definitions for the data record."));


        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(
                                                              T8DefinitionContext definitionContext,
                                                              String datumIdentifier) throws Exception
    {        
        if (Datum.INTERFACE_ORGANIZATION_MAPPINGS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordOrganizationMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.SUB_RECORD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8SubDataRecordMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.PROPERTY_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordPropertyMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.FIELD_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordFieldMappingDefinition.GROUP_IDENTIFIER));
        else if(Datum.FLAG_MAPPING_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataRecordFlagMappingDefinition.GROUP_IDENTIFIER));
        else return null;
    }

    public String getRecordPathExpression()
    {
        return (String)getDefinitionDatum(T8DataRecordMappingDefinition.Datum.RECORD_PATH.toString());
    }

    public void setRecordPathExpression(String expression)
    {
        setDefinitionDatum(T8DataRecordMappingDefinition.Datum.RECORD_PATH.toString(), expression);
    }

    public String getRecordInterfaceTerm()
    {
        return (String)getDefinitionDatum(Datum.RECORD_INTERFACE_TERM.toString());
    }

    public void setRecordInterfaceTerm(String term)
    {
        setDefinitionDatum(Datum.RECORD_INTERFACE_TERM.toString(), term);
    }

    public List<T8DataRecordOrganizationMappingDefinition> getInterfaceOrganizationDefinitions()
    {
        return (List<T8DataRecordOrganizationMappingDefinition>)getDefinitionDatum(Datum.INTERFACE_ORGANIZATION_MAPPINGS.toString());
    }

    public void setInterfaceOrganizationDefinitions(List<T8DataRecordOrganizationMappingDefinition> dataRecordsOrganizationMappingDefinitions)
    {
        setDefinitionDatum(Datum.INTERFACE_ORGANIZATION_MAPPINGS.toString(), dataRecordsOrganizationMappingDefinitions);
    }

    public List<T8DataRecordPropertyMappingDefinition> getPropertyDefinitions()
    {
        return (List<T8DataRecordPropertyMappingDefinition>) getDefinitionDatum(Datum.PROPERTY_DEFINITIONS.toString());
    }

    public void setPropertyDefinitions(List<T8DataRecordPropertyMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.PROPERTY_DEFINITIONS.toString(), definitions);
    }

    public List<T8DataRecordFieldMappingDefinition> getFieldDefinitions()
    {
        return (List<T8DataRecordFieldMappingDefinition>) getDefinitionDatum(Datum.FIELD_DEFINITIONS.toString());
    }

    public void setFieldDefinitions(List<T8DataRecordFieldMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.FIELD_DEFINITIONS.toString(), definitions);
    }

    public List<T8SubDataRecordMappingDefinition> getSubRecordDefinitions()
    {
        return (List<T8SubDataRecordMappingDefinition>)getDefinitionDatum(Datum.SUB_RECORD_DEFINITIONS.toString());
    }

    public void setSubRecordDefinitions(List<T8SubDataRecordMappingDefinition> definitions)
    {
        setDefinitionDatum(Datum.SUB_RECORD_DEFINITIONS.toString(), definitions);
    }

    public List<T8DataRecordFlagMappingDefinition> getFlagDefinitions()
    {
        return (List<T8DataRecordFlagMappingDefinition>)getDefinitionDatum(Datum.FLAG_MAPPING_DEFINITIONS.toString());
    }

    public void setFlagDefinitions(List<T8DataRecordFlagMappingDefinition> dataRecordFlagKeyMappingDefinitions)
    {
        setDefinitionDatum(Datum.FLAG_MAPPING_DEFINITIONS.toString(), dataRecordFlagKeyMappingDefinitions);
    }

    public Boolean isLoadIndependentRecords()
    {
        Boolean enabled;

        enabled = (Boolean) getDefinitionDatum(Datum.LOAD_INDEPENDENT_RECORDS.toString());
        return ((enabled != null) && enabled);
    }

    public void setLoadIndependentRecords(Boolean loadIndependentRecords)
    {
        setDefinitionDatum(Datum.LOAD_INDEPENDENT_RECORDS.toString(), loadIndependentRecords);
    }
}
