package com.pilog.t8.data.document.datafile;

import com.pilog.t8.data.document.datarecord.DataRecord;

/**
 * @author Bouwer du Preez
 */
public interface T8DataFileInputStream
{
    public DataRecord read(String dataFileID) throws Exception;
}
