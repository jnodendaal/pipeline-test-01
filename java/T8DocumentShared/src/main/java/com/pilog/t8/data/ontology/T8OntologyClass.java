package com.pilog.t8.data.ontology;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyClass implements Serializable
{
    private String id;
    private String displayName;
    private T8OntologyClass parentNode;
    private final List<T8OntologyClass> childNodes;

    public T8OntologyClass(String id, String displayName)
    {
        this.id = id;
        this.displayName = displayName;
        this.childNodes = new ArrayList<>();
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    void setParentNode(T8OntologyClass parentNode)
    {
        this.parentNode = parentNode;
    }

    public T8OntologyClass getParentNode()
    {
        return parentNode;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public int countDescendants()
    {
        int descendants;

        descendants = childNodes.size();
        for (T8OntologyClass childNode : childNodes)
        {
            descendants += childNode.countDescendants();
        }

        return descendants;
    }

    /**
     * Returns the list of data type ID's from the root to this node.
     * @return The list of data type ID's from the root to this node.
     */
    public List<String> getDataTypeIDPath()
    {
        T8OntologyClass parent;
        List<String> idList;

        idList = new ArrayList<String>();
        idList.add(getID());

        parent = this;
        while ((parent = parent.getParentNode()) != null)
        {
            idList.add(parent.getID());
        }

        Collections.reverse(idList);
        return idList;
    }

    public List<String> getDescendantClassIDList()
    {
        List<T8OntologyClass> descendants;
        List<String> idList;

        idList = new ArrayList<String>();
        descendants = getDescendantNodes();
        for (T8OntologyClass descendant : descendants)
        {
            idList.add(descendant.getID());
        }

        return idList;
    }

    public List<String> getChildDataTypeIDList()
    {
        List<T8OntologyClass> children;
        List<String> idList;

        idList = new ArrayList<String>();
        children = getChildNodes();
        for (T8OntologyClass child : children)
        {
            idList.add(child.getID());
        }

        return idList;
    }

    public List<T8OntologyClass> getChildNodes()
    {
        ArrayList<T8OntologyClass> nodes;

        nodes = new ArrayList<T8OntologyClass>();
        nodes.addAll(childNodes);
        return nodes;
    }

    /**
     * Returns a list of all the nodes from the root of the tree to this node
     * in sequence of traversal.  This node is therefore the last node in the
     * returned list and if this node is the root it will be the only node in
     * the returned list.
     *
     * @return The list of nodes from the root of the structure to this node.
     */
    public List<T8OntologyClass> getPathNodes()
    {
        List<T8OntologyClass> pathNodes;
        T8OntologyClass node;

        pathNodes = new ArrayList<T8OntologyClass>();
        pathNodes.add(this);
        node = this;
        while ((node = node.getParentNode()) != null)
        {
            pathNodes.add(node);
        }

        // The order of the list returned is very important.  We have to return nodes in order, from the root to thise node.
        Collections.reverse(pathNodes);
        return pathNodes;
    }

    public List<T8OntologyClass> getDescendantNodes()
    {
        LinkedList<T8OntologyClass> nodeQueue;
        List<T8OntologyClass> descendants;

        descendants = new ArrayList<T8OntologyClass>();
        nodeQueue = new LinkedList<T8OntologyClass>();
        nodeQueue.add(this);
        while(nodeQueue.size() > 0)
        {
            T8OntologyClass nextNode;
            List<T8OntologyClass> childNodesList;

            nextNode = nodeQueue.pop();
            childNodesList = nextNode.getChildNodes();

            descendants.addAll(childNodesList);
            nodeQueue.addAll(childNodesList);
        }

        return descendants;
    }

    public T8OntologyClass findDescendantByID(String id)
    {
        if (this.id.equals(id)) return this;
        else
        {
            for (T8OntologyClass child : childNodes)
            {
                T8OntologyClass descendant;

                descendant = child.findDescendantByID(id);
                if (descendant != null) return descendant;
            }

            return null;
        }
    }

    public void addChildNode(T8OntologyClass node)
    {
        node.setParentNode(this);
        childNodes.add(node);
    }

    public void addChildNodes(Collection<T8OntologyClass> nodes)
    {
        for (T8OntologyClass node : nodes)
        {
            addChildNode(node);
        }
    }

    public boolean removeChildNode(T8OntologyClass node)
    {
        boolean deleted;

        deleted = childNodes.remove(node);
        if (deleted) node.setParentNode(null);
        return deleted;
    }

    void addNestedIDList(List<String> indexList)
    {
        indexList.add(id);
        for (T8OntologyClass childNode : childNodes)
        {
            childNode.addNestedIDList(indexList);
        }
        indexList.add(id);
    }
}
