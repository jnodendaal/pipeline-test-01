package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.document.dataquality.T8RecordDataQualityReport;
import com.pilog.t8.data.T8HierarchicalSetType;
import com.pilog.t8.data.state.T8StateStore;
import com.pilog.t8.data.state.T8Stateful;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyCollector;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.dataquality.T8FileDataQualityReport;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.event.T8DataRecordChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8DataRecordChangeListener;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyAddedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyValueChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueStructureChangedEvent;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarecord.value.DependencyValue;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.ontology.T8ConceptGraphPath;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class DataRecord implements Value, T8Stateful<DataRecord>, Serializable
{
    private T8OntologyConcept ontology;
    private String id;
    private String codeId;
    private String code;
    private String termId;
    private String term;
    private String definitionId;
    private String definition;
    private String abbreviationId;
    private String abbreviation;
    private String languageId;
    private String fft;
    private String updatedById;
    private String updatedByIid;
    private T8Timestamp updatedAt;
    private String insertedById;
    private String insertedByIid;
    private T8Timestamp insertedAt;
    private String dataFileId; // This value is always set to null whenever a parent is added to this record.  When this happens the owner record ID is determined via the record chain.
    private String parentRecordId; // This value is always set to null whenever a parent is added to this record.  When this happens the parent record ID is determined via the record chain.
    private boolean adjusting; // A boolean flag to indicate that this record is in a state of adjustment where a sequence of changes are being performed.
    private final DataRequirementInstance drInstance;
    private final DataRecordAccessLayer accessLayer;
    private final List<RecordSection> recordSections;
    private final List<T8DataString> dataStrings;
    private final List<DataRecord> referrerRecords;
    private final List<DataRecord> subRecords;
    private final LinkedHashMap<String, Object> attributes;
    private final List<T8DataStringParticleSettings> dataStringParticleSettings;
    private final T8StateStore<DataRecord> stateStore;

    private transient EventListenerList definitionListeners;

    public DataRecord(DataRequirementInstance drInstance)
    {
        this.drInstance = drInstance;
        this.accessLayer = new DataRecordAccessLayer(this);
        this.recordSections = new ArrayList<>();
        this.dataStrings = new ArrayList<>();
        this.referrerRecords = new ArrayList<>();
        this.subRecords = new ArrayList<>();
        this.dataStringParticleSettings = new ArrayList<>();
        this.adjusting = false;
        this.ontology = new T8OntologyConcept(null, T8OntologyConceptType.DATA_RECORD, false, true);
        this.attributes = new LinkedHashMap<>();
        this.stateStore = new T8StateStore<DataRecord>();
    }

    public T8OntologyConcept getOntology()
    {
        return ontology;
    }

    public void setOntology(T8OntologyConcept ontology)
    {
        this.ontology = ontology;
    }

    public List<T8OntologyLink> getOntologyLinks()
    {
        if (ontology != null)
        {
            return ontology.getOntologyLinks();
        }
        else return new ArrayList<T8OntologyLink>();
    }

    /**
     * @return The content terminology of this record.
     * @Deprecated use getContentTerminology.
     */
    public T8ConceptTerminologyList getTerminology()
    {
        return getContentTerminology(true);
    }

    public T8ConceptTerminologyList getContentTerminology(boolean includeDescendantRecords)
    {
        T8ConceptTerminologyList terminologyList;

        terminologyList = new T8ConceptTerminologyList();
        addContentTerminology(terminologyList, includeDescendantRecords);
        return terminologyList;
    }

    public void addContentTerminology(TerminologyCollector terminologyCollector, boolean includeDescendantRecords)
    {
        // Add this record's terminology.
        terminologyCollector.addTerminology(languageId, T8OntologyConceptType.DATA_RECORD, id, codeId, code, termId, term, abbreviationId, abbreviation, definitionId, definition);

        // Add the terminology of the DR Instance on which this record is based.
        drInstance.addContentTerminology(terminologyCollector);

        // Add terminology of all sections.
        for (RecordSection recordSection : recordSections)
        {
            recordSection.addContentTerminology(terminologyCollector);
        }

        // Add terminology of all descendant records if specified.
        if (includeDescendantRecords)
        {
            for (DataRecord subRecord : subRecords)
            {
                subRecord.addContentTerminology(terminologyCollector, includeDescendantRecords);
            }
        }
    }

    public void setContentTerminology(TerminologyProvider terminologyProvider, boolean includeDescendantRecords)
    {
        T8ConceptTerminology terminology;

        // Set this record's terminology.
        terminology = terminologyProvider.getTerminology(null, id);
        if (terminology != null)
        {
            this.languageId = terminology.getLanguageId();
            this.codeId = terminology.getCodeId();
            this.code = terminology.getCode();
            this.termId = terminology.getTermId();
            this.term = terminology.getTerm();
            this.definitionId = terminology.getDefinitionId();
            this.definition = terminology.getDefinition();
            this.abbreviationId = terminology.getAbbreviationId();
            this.abbreviation = terminology.getAbbreviation();
        }
        else
        {
            this.languageId = null;
            this.codeId = null;
            this.code = null;
            this.termId = null;
            this.term = null;
            this.definitionId = null;
            this.definition = null;
            this.abbreviationId = null;
            this.abbreviation = null;
        }

        // Set the terminology of the DR Instance on which this record is based.
        drInstance.setContentTerminology(terminologyProvider);

        // Set the terminology of all sections.
        for (RecordSection recordSection : recordSections)
        {
            recordSection.setContentTerminology(terminologyProvider);
        }

        // Set the terminology of descendant records if specified.
        if (includeDescendantRecords)
        {
            for (DataRecord subRecord : subRecords)
            {
                subRecord.setContentTerminology(terminologyProvider, includeDescendantRecords);
            }
        }
    }

    public T8FileDataQualityReport getFileDataQualityReport()
    {
        DataRecord file;

        file = getDataFile();
        if (file != null)
        {
            T8FileDataQualityReport report;

            report = new T8FileDataQualityReport(file.getID());
            for (DataRecord record : file.getDataFileRecords())
            {
                report.addMetrics(record.getDataQualityReport());
            }

            return report;
        }
        else return null;
    }

    public T8RecordDataQualityReport getDataQualityReport()
    {
        T8RecordDataQualityReport report;

        report = new T8RecordDataQualityReport(id);
        report.setPropertyCount(drInstance.getPropertyCount());
        report.setPropertyDataCount(getPropertyDataCount());
        report.setPropertyValidCount(getPropertyValidCount());
        report.setPropertyVerificationCount(getPropertyValidCount());
        report.setPropertyAccurateCount(getPropertyValidCount()); // Until a property validity measurement is implemented, use the correctness count.
        report.setCharacteristicCount(drInstance.getCharacteristicCount());
        report.setCharacteristicDataCount(getCharacteristicDataCount());
        report.setCharacteristicValidCount(getCharacteristicValidCount());
        report.setCharacteristicVerificationCount(getCharacteristicValidCount());
        report.setCharacteristicAccurateCount(getCharacteristicValidCount()); // Until a property validity measurement is implemented, use the correctness count.
        return report;
    }

    public int getPropertyDataCount()
    {
        int count;

        count = 0;
        for (RecordProperty property : getRecordProperties())
        {
            if (property.hasContent(true)) count++;
        }

        return count;
    }

    public int getPropertyValidCount()
    {
        int count;

        count = 0;
        for (RecordProperty property : getRecordProperties())
        {
            if (property.hasContent(false)) count++;
        }

        return count;
    }

    public int getCharacteristicDataCount()
    {
        int count;

        count = 0;
        for (RecordProperty property : getRecordProperties())
        {
            if ((property.isCharacteristic()) && (property.hasContent(true))) count++;
        }

        return count;
    }

    public int getCharacteristicValidCount()
    {
        int count;

        count = 0;
        for (RecordProperty property : getRecordProperties())
        {
            if ((property.isCharacteristic()) && (property.hasContent(false))) count++;
        }

        return count;
    }

    /**
     * Returns the adjusting flag of this document. This flag indicates that
     * a single data operation consisting of multiple individual updates is
     * taking place.
     * @return The value of the adjusting flag.
     */
    public boolean isAdjusting()
    {
        return adjusting;
    }

    /**
     * Sets the adjusting flag on this document. This flag indicates that
     * a single data operation consisting of multiple individual updates is
     * taking place. An event is fired to notify all listeners when this flag is
     * changed.
     * @param adjusting The value of the adjusting flag.  An event is fired to
     * notify all listeners when this flag is changed.
     */
    public void setAdjusting(boolean adjusting)
    {
        setAdjusting(adjusting, true);
    }

    /**
     * Sets the adjusting flag on this document. This flag indicates that
     * a single data operation consisting of multiple individual updates is
     * taking place.  A second parameter van be set to indicate whether or not
     * an event should be fired to notify listeners of the flag changes.
     * @param adjusting The value of the adjusting flag.
     * @param fireEvent A boolean value to indicate whether or not an event
     * should be fired to notify listeners of the flag changes.
     */
    public void setAdjusting(boolean adjusting, boolean fireEvent)
    {
        // We only need to handle an actualy change.
        if (this.adjusting != adjusting)
        {
            // Change this value.
            this.adjusting = adjusting;

            // If the adjusting sequence is over, we fire a last changed event to indicate this fact.
            if ((!this.adjusting) && (fireEvent))
            {
                fireDataRecordChangeEventPropagated(new T8DataRecordChangedEvent(this), null);
            }
        }
    }

    /**
     * Sets the adjusting flag of all documents in the current document model.
     * This flag indicates that a single data operation consisting of multiple
     * individual updates is taking place.  A second parameter van be set to
     * indicate whether or not an event should be fired to notify listeners of
     * the flag changes.
     * @param adjusting The value of the adjusting flag.
     * @param fireEvent A boolean value to indicate whether or not an event
     * should be fired to notify listeners of the flag changes.
     */
    public void setModelAdjusting(boolean adjusting, boolean fireEvent)
    {
        // Set the flag on all individual documents.
        for (DataRecord dataRecord : getDataRecords())
        {
            dataRecord.setAdjusting(adjusting, false);
        }

        // If the adjusting sequence is over, we fire a last changed event to indicate this fact.
        if ((!this.adjusting) && (fireEvent))
        {
            fireDataRecordChangeEventPropagated(new T8DataRecordChangedEvent(this), null);
        }
    }

    public void addChangeListener(T8DataRecordChangeListener listener)
    {
        // Lazy initialization of definition listener list.
        if (definitionListeners == null) definitionListeners = new EventListenerList();
        definitionListeners.add(T8DataRecordChangeListener.class, listener);
    }

    public void removeChangeListener(T8DataRecordChangeListener listener)
    {
        if (definitionListeners != null)
        {
            definitionListeners.remove(T8DataRecordChangeListener.class, listener);
        }
    }

    public void clearChangeListeners()
    {
        definitionListeners = null;
    }

    /**
     * Fires the specified event on all listeners added to this document.
     * @param event The event to fire.
     * @param actor The actor responsible for the event.
     */
    void fireDataRecordChangeEventIsolated(T8DataRecordChangedEvent event, Object actor)
    {
        // Only fire the event on this record if there are actually some listeners registered.
        if ((definitionListeners != null) && (definitionListeners.getListenerCount() > 0))
        {
            for (T8DataRecordChangeListener listener : definitionListeners.getListeners(T8DataRecordChangeListener.class))
            {
                if (event instanceof T8RecordPropertyAddedEvent) listener.recordPropertyAdded((T8RecordPropertyAddedEvent)event);
                else if (event instanceof T8RecordPropertyValueChangedEvent) listener.recordPropertyValueChanged((T8RecordPropertyValueChangedEvent)event);
                else if (event instanceof T8RecordValueChangedEvent) listener.recordValueChanged((T8RecordValueChangedEvent)event);
                else if (event instanceof T8RecordValueStructureChangedEvent) listener.recordValueStructureChanged((T8RecordValueStructureChangedEvent)event);
                else listener.dataRecordChanged(event); // The default event type.
            }
        }
    }

    void fireDataRecordChangeEventPropagated(T8DataRecordChangedEvent event, Object actor)
    {
        Set<DataRecord> dataRecords;

        // Set the adjusting value for the event.
        event.setAdjusting(adjusting);

        // Fire the event on all records in the record graph.
        dataRecords = getDataRecords();
        for (DataRecord record : dataRecords)
        {
            record.fireDataRecordChangeEventIsolated(event, actor);
        }
    }

    public void dataRecordChanged()
    {
        fireDataRecordChangeEventPropagated(new T8DataRecordChangedEvent(this), this);
    }

    public void resetAccessLayer(boolean includeDescendants)
    {
        accessLayer.reset();
        if (includeDescendants)
        {
            for (DataRecord subRecord : subRecords)
            {
                subRecord.resetAccessLayer(includeDescendants);
            }
        }
    }

    public DataRecordAccessLayer getAccessLayer()
    {
        return accessLayer;
    }

    /**
     * Sets the access layer of this record and its descendants (if specified) to the equivalent settings as the supplied access layer.
     * This method does not update the actual access layer object reference i.e. the existing access layer objects of this document model are simply
     * updated to reflect the same settings as those in the input model.  If any record, section, property, field or value access layers in
     * the current model are not present in the input model, those access layers are not updated and remain unchanged.
     *
     * @param newAccess The new access settings to which all those in this object model will be set.
     */
    public void setAccess(DataRecordAccessLayer newAccess, boolean includeDescendants)
    {
        this.accessLayer.setAccess(newAccess);
        if (includeDescendants)
        {
            for (DataRecord subRecord : subRecords)
            {
                DataRecordAccessLayer newSubAccess;

                newSubAccess = newAccess.getSubRecordAccessLayer(subRecord.getId());
                subRecord.setAccess(newSubAccess, includeDescendants);
            }
        }
    }

    public DataRequirementInstance getDataRequirementInstance()
    {
        return drInstance;
    }

    public DataRequirement getDataRequirement()
    {
        return drInstance != null ? drInstance.getDataRequirement() : null;
    }

    public String getFFT()
    {
        return fft;
    }

    public void setFFT(String fft)
    {
        this.fft = fft;
    }

    public void appendFFT(String fft)
    {
        if (this.fft == null)
        {
            this.fft = fft;
        }
        else
        {
            this.fft += fft;
        }
    }

    /**
     * Appends the specified FFT to the already existing record FFT after
     * performing required checks to ensure that the existing FFT remains valid.
     * The following checks are applied:
     *  -   The supplied FFT will not be added if the existing FFT already
     *      contains the string to be added.
     *  -   The specified separator will be used only if needed i.e. there is
     *      existing FFT to which the newly supplied FFT will be added.
     *  -   The supplied FFT will be trimmed before appended.
     *  -   If the supplied FFT is null or an empty string (whitespace only) it
     *      will not be appended.
     * @param fft The FFT to append to this record.
     * @param separator The separator insert between existing record FFT and the
     * new FFT supplied.
     */
    public void appendFFTChecked(String fft, String separator)
    {
        if (!Strings.isNullOrEmpty(fft))
        {
            String newFFT;

            newFFT = fft.trim();
            if (Strings.isNullOrEmpty(this.fft))
            {
                this.fft = newFFT;
            }
            else if (!this.fft.toUpperCase().contains(newFFT.toUpperCase()))
            {
                this.fft += (separator + newFFT);
            }
        }
    }

    public boolean hasFFT()
    {
        return ((fft != null) && (fft.trim().length() > 0));
    }

    public String getId()
    {
        return id;
    }

    /**
     * Sets the {@code String} ID for the record. This method is intended for
     * use when manually manipulating the {@code DataRecord} object.
     *
     * @param id The {@code String} ID to be set
     */
    public void setId(String id)
    {
        this.id = id;
        if (ontology != null) ontology.setID(id);

        // Set the new id on the data string particle settings.
        for (T8DataStringParticleSettings settings : dataStringParticleSettings)
        {
            settings.setRecordId(id);
        }
    }

    @Deprecated
    public String getID()
    {
        return getId();
    }

    @Deprecated
    public void setID(String id)
    {
        setId(id);
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public Object getAttribute(String attributeIdentifier)
    {
        return attributes.get(attributeIdentifier);
    }

    public Object setAttribute(String attributeId, Object attributeValue)
    {
        if (attributeId != null)
        {
            return attributes.put(attributeId, attributeValue);
        }
        else throw new RuntimeException("Invalid attribute id: " + attributeId);
    }

    public Object removeAttribute(String attributeId)
    {
        if (attributeId != null)
        {
            return attributes.remove(attributeId);
        }
        else throw new RuntimeException("Invalid attribute id: " + attributeId);
    }

    public boolean containsAttribute(String attributeId)
    {
        return attributes.containsKey(attributeId);
    }

    public Map<String, Object> getAttributes()
    {
        return new LinkedHashMap<>(attributes);
    }

    public void setAttributes(Map<String, Object> newAttributes)
    {
        attributes.clear();
        if (newAttributes != null)
        {
            for (String attributeId : newAttributes.keySet())
            {
                setAttribute(attributeId, newAttributes.get(attributeId));
            }
        }
    }

    public RecordContent getContent(boolean includeDescendants)
    {
        RecordContent content;

        // Set the content of the record object.
        content = new RecordContent();
        content.setId(id);
        content.setDrInstanceId(drInstance.getConceptID());
        content.setFft(fft);

        // Set property content.
        for (RecordProperty property : getRecordProperties())
        {
            content.addProperty(property.getContent());
        }

        // Add the content of descendants if required.
        if (includeDescendants)
        {
            for (DataRecord subRecord : subRecords)
            {
                content.addSubRecord(subRecord.getContent(includeDescendants));
            }
        }

        // Return the complete content of the record.
        return content;
    }

    /**
     * This method clear all data regarded as document content from the object:
     * - Property values (excluding DOCUMENT_REFERENCE, COMPOSITE and FIELD types).
     * - Record FFT.
     * - Record alteration data.
     * - Property FFT.
     * - Data Strings.
     * - Particle Settings.
     */
    public void clearContent()
    {
        fft = null;
        dataStrings.clear();
        dataStringParticleSettings.clear();

        // Clear the content from properties and fields.
        for (RecordProperty recordProperty : getRecordProperties())
        {
            recordProperty.clearContent();
        }
    }

    public void setContent(RecordContent content)
    {
        // Set this object's content.
        this.fft = content.getFft();

        // Set all property content.
        for (PropertyContent propertyContent : content.getProperties())
        {
            RecordProperty property;

            property = getOrAddRecordProperty(propertyContent.getId());
            property.setContent(propertyContent);
        }
    }

    /**
     * @deprecated {@link #getDataRequirementId()}
     */
    @Deprecated
    public String getDataRequirementID()
    {
        DataRequirement dataRequirement;

        dataRequirement = getDataRequirement();
        return dataRequirement.getConceptID();
    }

    public String getDataRequirementId()
    {
        return getDataRequirement().getId();
    }

    /**
     * @deprecated {@link #getDataRequirementInstanceId()}
     */
    @Deprecated
    public String getDataRequirementInstanceID()
    {
        DataRequirementInstance dataRequirementInstance;

        dataRequirementInstance = getDataRequirementInstance();
        return dataRequirementInstance.getConceptID();
    }

    public String getDataRequirementInstanceId()
    {
        return getDataRequirementInstance().getId();
    }

    /**
     * This method returns the id of the parent record to which this record belongs.
     * If no parent record document can be found, null is returned.  A document is
     * considered a parent record, if it is has a dependent reference to the child
     * record in question.  If this document is linked to parents, the data
     * file is determined via the parent document chain.  If this document is
     * not linked to parents, the local variable <code>parentRecordId</code> is
     * used.
     * @return The ID of the parent record of this record or null if no valid
     * parent record is found.
     */
    public String getParentRecordId()
    {
        DataRecord parentRecord;

        parentRecord = getParentRecord();
        if (parentRecord != null) return parentRecord.getID();
        else return parentRecordId;
    }

    /**
     * Sets the parent record ID of this document.  This method can only be used
     * on records that have no parents.  If a record has a parent or is itself
     * a valid owner, this method will throw an IllegalStateException.
     * @param parentRecordId The record ID of this document's owner.
     */
    public void setParentRecordId(String parentRecordId)
    {
        if (referrerRecords.isEmpty())
        {
            this.parentRecordId = parentRecordId;
        }
        else throw new IllegalStateException("Cannot set parent ID on a record that has parent documents.");
    }

    /**
     * This method returns the ID of the data file to which this record belongs.
     * If no data file document can be found, null is returned.  A document is
     * considered a data file, if it is based on an independent Data
     * Requirement Instance.  If this document is linked to parents, the data
     * file is determined via the parent document chain.  If this document is
     * not linked to parents, the local variable <code>dataFileId</code> is
     * used.
     * @return The ID of the owner record of this record or null if no valid
     * owner is found.
     */
    public String getDataFileID()
    {
        DataRecord dataFile;

        dataFile = getDataFile();
        if (dataFile != null) return dataFile.getID();
        else return dataFileId;
    }

    /**
     * Sets the owner record ID of this document.  This method can only be used
     * on records that have no parents.  If a record has a parent or is itself
     * a valid owner, this method will throw an IllegalStateException.
     * @param recordID The record ID of this document's owner.
     */
    public void setOwnerRecordID(String recordID)
    {
        if (isIndependentRecord())
        {
            // If the new ID is the same as the existing one, calling this method is a NOOP but if they differ it is a problem.
            if (!Objects.equals(recordID, id))
            {
                throw new IllegalStateException("Cannot set different owner ID on a record that is itself an owner (independent).");
            }
        }
        else
        {
            if (referrerRecords.isEmpty())
            {
                this.dataFileId = recordID;
            }
            else throw new IllegalStateException("Cannot set owner ID on a record that has parent documents.");
        }
    }

    /**
     * Returns true if this record is based on a Data Requirement Instance that
     * is independent.
     * @return true if this record is based on a Data Requirement Instance that
     * is independent.
     */
    public boolean isIndependentRecord()
    {
        return drInstance.isIndependent();
    }

    public int getRecordSectionIndex(RecordSection recordSection)
    {
        return recordSections.indexOf(recordSection);
    }

    public List<RecordSection> getRecordSections()
    {
        return new ArrayList<>(recordSections);
    }

    public RecordSection getRecordSection(String sectionId)
    {
        for (RecordSection recordSection : recordSections)
        {
            if (recordSection.getSectionID().equals(sectionId))
            {
                return recordSection;
            }
        }

        return null;
    }

    public RecordSection getOrAddRecordSection(String sectionId)
    {
        RecordSection section;

        section = getRecordSection(sectionId);
        if (section != null)
        {
            return section;
        }
        else
        {
            SectionRequirement sectionRequirement;

            sectionRequirement = drInstance.getDataRequirement().getSectionRequirement(sectionId);
            if (sectionRequirement != null)
            {
                RecordSection newSection;

                newSection = new RecordSection(sectionRequirement);
                setRecordSection(newSection);
                return newSection;
            }
            else throw new IllegalArgumentException("Section not found in Data Requirement Instance '" + drInstance + "': " + sectionId);
        }
    }

    public List<RecordProperty> getRecordProperties()
    {
        List<RecordProperty> recordProperties;

        recordProperties = new ArrayList<>();
        for (RecordSection recordSection : recordSections)
        {
            recordProperties.addAll(recordSection.getRecordProperties());
        }

        return recordProperties;
    }

    public RecordProperty getRecordProperty(String propertyId)
    {
        for (RecordSection recordSection : recordSections)
        {
            RecordProperty recordProperty;

            recordProperty = recordSection.getRecordProperty(propertyId);
            if (recordProperty != null) return recordProperty;
        }

        return null;
    }

    public RecordProperty getOrAddRecordProperty(String propertyId)
    {
        RecordProperty property;

        property = getRecordProperty(propertyId);
        if (property != null)
        {
            return property;
        }
        else
        {
            PropertyRequirement propertyRequirement;

            propertyRequirement = drInstance.getDataRequirement().getPropertyRequirement(propertyId);
            if (propertyRequirement != null)
            {
                RecordProperty newProperty;
                RecordSection section;

                newProperty = new RecordProperty(propertyRequirement);
                section = getOrAddRecordSection(propertyRequirement.getSectionId());
                section.addRecordProperty(newProperty);
                return newProperty;
            }
            else throw new IllegalArgumentException("Property not found in Data Requirement Instance '" + drInstance + "': " + propertyId);
        }
    }

    /**
     * Returns all record values that are fields (i.e. that have FIELD_TYPE
     * requirements).
     * @return All record values that are fields (i.e. that have FIELD_TYPE
     * requirements).
     */
    public List<RecordValue> getRecordFields()
    {
        List<RecordValue> recordFields;

        recordFields = new ArrayList<>();
        for (RecordProperty recordProperty : getRecordProperties())
        {
            recordFields.addAll(recordProperty.getFields());
        }

        return recordFields;
    }

    public RecordValue getRecordPropertyField(String propertyID, String fieldID)
    {
        RecordProperty recordProperty;

        recordProperty = getRecordProperty(propertyID);
        return recordProperty.getField(fieldID);
    }

    public RecordProperty removeRecordProperty(String propertyID)
    {
        RecordProperty oldProperty;

        // Get the property property and remove it if found.
        oldProperty = getRecordProperty(propertyID);
        if (oldProperty != null) removeRecordProperty(oldProperty);
        return oldProperty;
    }

    public boolean removeRecordProperty(RecordProperty property)
    {
        for (RecordSection recordSection : recordSections)
        {
            if (recordSection.removeRecordProperty(property))
            {
                return true;
            }
        }

        return false;
    }

    public void setRecordProperty(RecordProperty newProperty)
    {
        RecordSection matchingSection;
        RecordProperty oldProperty;
        String sectionID;

        // Get the record section or add it if it does not exist.
        sectionID = newProperty.getPropertyRequirement().getParentSectionRequirement().getConceptID();
        matchingSection = getRecordSection(sectionID);
        if (matchingSection == null)
        {
            SectionRequirement sectionRequirement;

            sectionRequirement = drInstance.getDataRequirement().getSectionRequirement(sectionID);
            if (sectionRequirement != null)
            {
                matchingSection = new RecordSection(sectionRequirement);
                setRecordSection(matchingSection);
            }
            else throw new RuntimeException("Section not found: Section ID:" + sectionID + " in DR:" + getDataRequirement().getConceptID());
        }

        // Remove the old property if it is not the same as the new one.
        oldProperty = matchingSection.getRecordProperty(newProperty.getPropertyID());
        if ((oldProperty != null) && (oldProperty != newProperty))
        {
            matchingSection.removeRecordProperty(oldProperty);
        }

        // Now set the new property if it is not the same as the old one.
        if (oldProperty != newProperty)
        {
            matchingSection.addRecordProperty(newProperty);
        }
    }

    public RecordValue getOrAddRecordValue(String propertyId, String fieldId)
    {
        RecordProperty property;

        // Get the target property and see if it has a value.
        property = getOrAddRecordProperty(propertyId);
        if (fieldId != null) // A new composite, field and field value must be created and returned.
        {
            return property.getOrAddValue(fieldId);
        }
        else return property.getOrAddValue(); // Create a new value for the property and return it.
    }

    public RecordValue getRecordValue(String propertyId)
    {
        RecordProperty recordProperty;

        recordProperty = getRecordProperty(propertyId);
        return recordProperty != null ? recordProperty.getRecordValue() : null;
    }

    public List<RecordValue> getAllPropertyValues()
    {
        List<RecordValue> propertyValues;

        propertyValues = new ArrayList<>();
        for (RecordProperty recordProperty : getRecordProperties())
        {
            RecordValue propertyValue;

            propertyValue = recordProperty.getRecordValue();
            if (propertyValue != null) propertyValues.add(propertyValue);
        }

        return propertyValues;
    }

    public List<RecordValue> getAllFieldValues()
    {
        List<RecordValue> fieldValues;

        fieldValues = new ArrayList<>();
        for (RecordValue recordField : getRecordFields())
        {
            RecordValue fieldValue;

            fieldValue = recordField.getFirstSubValue();
            if (fieldValue != null) fieldValues.add(fieldValue);
        }

        return fieldValues;
    }

    public List<RecordValue> getAllValues()
    {
        List<RecordValue> values;

        values = new ArrayList<>();
        for (RecordProperty recordProperty : getRecordProperties())
        {
            RecordValue propertyValue;

            propertyValue = recordProperty.getRecordValue();
            if (propertyValue != null)
            {
                values.add(propertyValue);
                values.addAll(propertyValue.getDescendantValues());
            }
        }

        return values;
    }

    /**
     * Returns the first value in the Data Record matching the specified value
     * requirement.
     * @param valueRequirement
     * @return The first value in the Data Record matching the specified value
     * requirement.
     */
    public RecordValue getRecordValue(ValueRequirement valueRequirement)
    {
        List<RecordValue> valueList;

        valueList = getRecordValues(valueRequirement);
        return valueList.size() > 0 ? valueList.get(0) : null;
    }

    /**
     * Returns all values in the Data Record that match the specified value
     * requirement.
     * @param valueRequirement The value requirement to match.
     * @return The list of values matching the value requirement.
     */
    public List<RecordValue> getRecordValues(ValueRequirement valueRequirement)
    {
        SectionRequirement sectionRequirement;
        PropertyRequirement propertyRequirement;
        List<ValueRequirement> valueRequirementPath;
        RecordSection matchingSection;
        List<RecordValue> valueList;

        // Get the higher-level requirements from the supplied value requirement.
        propertyRequirement = valueRequirement.getParentPropertyRequirement();
        sectionRequirement = propertyRequirement.getParentSectionRequirement();
        valueRequirementPath = valueRequirement.getPathValueRequirements();

        // Create a new list to hold the values found.
        valueList = new ArrayList<>();

        // Get the record section.
        matchingSection = getRecordSection(sectionRequirement.getConceptID());
        if (matchingSection != null)
        {
            RecordProperty property;

            // Get the record property.
            property = matchingSection.getRecordProperty(propertyRequirement.getConceptID());
            if (property != null)
            {
                // Return the values identified by the requirement path.
                return property.getRecordValues(valueRequirementPath);
            }
            else return valueList;
        }
        else return valueList;
    }

    public void setRecordValue(String propertyID, RecordValue value)
    {
        PropertyRequirement propertyRequirement;

        propertyRequirement = drInstance.getDataRequirement().getPropertyRequirement(propertyID);
        if (propertyRequirement != null)
        {
            SectionRequirement sectionRequirement;
            RecordSection matchingSection;
            RecordProperty property;

            // Get the record section or add it if it does not exist.
            sectionRequirement = propertyRequirement.getParentSectionRequirement();
            matchingSection = getRecordSection(sectionRequirement.getConceptID());
            if (matchingSection == null)
            {
                matchingSection = new RecordSection(sectionRequirement);
                setRecordSection(matchingSection);
            }

            // Get the record property or add it if it does not exist.
            property = matchingSection.getRecordProperty(propertyID);
            if (property != null)
            {
                property.setRecordValue(value);
            }
            else if (value != null) // Property does not exist in the record, so add it and its new value.
            {
                property = new RecordProperty(propertyRequirement);
                property.setRecordValue(value);
                matchingSection.addRecordProperty(property);
            }
        }
        else throw new RuntimeException("Property not found: ID:" + propertyID + " in DR:" + getDataRequirement().getRequirementType());
    }

    /**
     * Sets the plain value for the specified property.  This method can only
     * be used for properties that are of a RequirementType that only needs one
     * String value for valid entry.
     * @param propertyId The ID of the property for which the value will be set.
     * @param fieldId The ID of the field for which the value will be set.  Null
     * can be used in the case of non-composite properties.
     * @param value The plain value to set on the property.
     * @return The record value on which the new value was set.
     * @deprecated Use setValue().
     */
    @Deprecated
    public RecordValue setPlainValue(String propertyId, String fieldId, String value)
    {
        return setValue(propertyId, fieldId, value, false);
    }

    /**
     * Sets the plain value for the specified property.  This method can only
     * be used for properties that are of a RequirementType that only needs one
     * String value for valid entry.
     * @param propertyId The ID of the property for which the value will be set.
     * @param fieldId The ID of the field for which the value will be set.  Null
     * can be used in the case of non-composite properties.
     * @param value The plain value to set on the property.
     * @param clearDependants A boolean flag to indicate whether or not values
     * that are dependent on the altered value must be cleared or not.
     * @return The record value on which the new value was set.
     */
    public RecordValue setValue(String propertyId, String fieldId, String value, boolean clearDependants)
    {
        PropertyRequirement propertyRequirement;

        // Find the target property requirement.
        propertyRequirement = drInstance.getDataRequirement().getPropertyRequirement(propertyId);
        if (propertyRequirement != null)
        {
            SectionRequirement sectionRequirement;
            RecordSection targetSection;
            RecordProperty property;

            // Get the target section on which the value will be set.
            sectionRequirement = propertyRequirement.getParentSectionRequirement();
            targetSection = getRecordSection(sectionRequirement.getConceptID());
            if (targetSection == null)
            {
                targetSection = new RecordSection(sectionRequirement);
                setRecordSection(targetSection);
            }

            // Get the record property or add it if it does not exist.
            property = targetSection.getRecordProperty(propertyId);
            if (property != null) // Property exists, just change the value.
            {
                return property.setValue(fieldId, value, clearDependants);
            }
            else // Property does not exist in the record, so add it and its new value.
            {
                RecordValue newValue;

                property = new RecordProperty(propertyRequirement);
                newValue = property.setValue(fieldId, value, clearDependants);
                targetSection.addRecordProperty(property);
                return newValue;
            }
        }
        else throw new RuntimeException("Property not found: ID:" + propertyId + " in DR:" + getDataRequirement());
    }

    /**
     * Replaces all references to the specified old attachment with references
     * to the supplied new attachment and updates attachment details
     * accordingly.
     * @param oldAttachmentId The existing attachment ID to replace.
     * @param newAttachmentId The new attachment ID to replace the old id's.
     * @param newAttachmentFileDetails The new attachment details to replace the
     * old one.
     */
    public void replaceAttachmentDetails(String oldAttachmentId, String newAttachmentId, T8AttachmentDetails newAttachmentFileDetails)
    {
        for (RecordValue value : getAllValues())
        {
            if (value instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)value;
                if (attachmentList.containsAttachment(oldAttachmentId))
                {
                    attachmentList.replaceAttachment(oldAttachmentId, newAttachmentId, newAttachmentFileDetails);
                }
            }
        }
    }

    /**
     * Replaces all references to the specified old record ID with references to
     * the specified new record ID.
     * @param oldRecordId The existing record ID to replace.
     * @param newRecordId The new record ID to replace the old references.
     */
    public void replaceDocumentReference(String oldRecordId, String newRecordId)
    {
        // We first replace the ID references in the values
        for (RecordValue value : getAllValues())
        {
            RequirementType requirementType;

            requirementType = value.getValueRequirement().getRequirementType();
            if (requirementType == RequirementType.DOCUMENT_REFERENCE)
            {
                String referenceID;

                referenceID = value.getValue();
                if (oldRecordId.equals(referenceID))
                {
                    value.setValue(newRecordId);
                }
            }
        }

        // Now we want to replace the ID references in the reference Lists
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                for (RecordValue recordValue : recordProperty.getAllRecordValues())
                {
                    if (recordValue instanceof DocumentReferenceList)
                    {
                        ((DocumentReferenceList)recordValue).replaceReference(oldRecordId, newRecordId);
                    }
                }
            }
        }
    }

    /**
     * Replaces all occurrences of references to those specified as keys in the
     * supplied map, with the new references mapped to the keys in the map.
     * @param referenceMapping The record ID reference map containing the
     * following key-value pairs:  (key=oldRecordID, value=newRecordID).
     */
    public void replaceDocumentReferences(Map<String, String> referenceMapping)
    {
        for (RecordValue value : getAllValues())
        {
            RequirementType requirementType;

            requirementType = value.getValueRequirement().getRequirementType();
            if (requirementType == RequirementType.DOCUMENT_REFERENCE)
            {
                String referenceID;
                String newReferenceID;

                referenceID = value.getValue();
                newReferenceID = referenceMapping.get(referenceID);
                if (newReferenceID != null)
                {
                    value.setValue(newReferenceID);
                }
            }
        }
    }

    public RecordValue addDocumentReference(String propertyID, String fieldID, String drInstanceID, String drGroupID, String recordID)
    {
        PropertyRequirement propertyRequirement;

        // Find the target property requirement.
        propertyRequirement = drInstance.getDataRequirement().getPropertyRequirement(propertyID);
        if (propertyRequirement != null)
        {
            SectionRequirement sectionRequirement;
            RecordSection targetSection;
            RecordProperty property;

            sectionRequirement = propertyRequirement.getParentSectionRequirement();
            targetSection = getRecordSection(sectionRequirement.getConceptID());
            if (targetSection == null)
            {
                targetSection = new RecordSection(sectionRequirement);
                setRecordSection(targetSection);
            }

            // Get the record property or add it if it does not exist.
            property = targetSection.getRecordProperty(propertyID);
            if (property != null) // Property exists, just change the value.
            {
                return property.addDocumentReference(fieldID, drInstanceID, drGroupID, recordID);
            }
            else // Property does not exist in the record, so add it and its new value.
            {
                property = new RecordProperty(propertyRequirement);
                targetSection.addRecordProperty(property);
                return property.addDocumentReference(fieldID, drInstanceID, drGroupID, recordID);
            }
        }
        else throw new RuntimeException("Property not found: ID:" + propertyID + " in DR:" + getDataRequirement());
    }

    /**
     * Returns the plain value from the root RecordValue object (if any) of
     * this RecordProperty.
     * @param propertyID The ID of the property for which to fetch the plain
     * value.
     * @return The plain value from the root RecordValue object (if any) of
     * the specified RecordProperty.
     */
    public String getPlainValue(String propertyID)
    {
        RecordProperty recordProperty;

        recordProperty = getRecordProperty(propertyID);
        return recordProperty != null ? recordProperty.getPlainValue() : null;
    }

    public String getPlainValue(String propertyID, String fieldID)
    {
        RecordProperty recordProperty;

        recordProperty = getRecordProperty(propertyID);
        if (recordProperty != null)
        {
            RecordValue field;

            field = recordProperty.getField(fieldID);
            return field != null ? field.getPlainValue() : null;
        }
        else return null;
    }

    public List<String> getPlainValueList(String propertyID)
    {
        RecordProperty recordProperty;

        recordProperty = getRecordProperty(propertyID);
        return recordProperty != null ? recordProperty.getPlainValueList() : null;
    }

    /**
     * Returns all <code>DocumentReference</code> objects in this DataRecord
     * that refers to the specified record.
     * @param recordID The record ID to which the references sought, must refer.
     * @return All <code>DocumentReference</code> objects in this DataRecord
     * that refers to the specified record.
     */
    public List<DocumentReferenceList> getReferences(String recordID)
    {
        List<DocumentReferenceList> references;

        references = new ArrayList<DocumentReferenceList>();
        for (RecordValue value : this.getAllValues())
        {
            if (value instanceof DocumentReferenceList)
            {
                DocumentReferenceList referenceList;

                referenceList = (DocumentReferenceList)value;
                if (referenceList.containsReference(recordID))
                {
                    references.add((DocumentReferenceList)value);
                }
            }
        }

        return references;
    }

    /**
     * Finds all <code>DocumentReference</code> objects in the context record
     * object model that refers to the specified record.
     * @param recordID The record ID to which the references sought, must refer.
     * @return All <code>DocumentReference</code> objects in the context record
     * object model that refers to the specified record.
     */
    public List<DocumentReferenceList> findReferences(String recordID)
    {
        List<DocumentReferenceList> references;

        references = new ArrayList<DocumentReferenceList>();
        for (DataRecord record : getDataRecords())
        {
            references.addAll(record.getReferences(recordID));
        }

        return references;
    }

    /**
     * Returns the dependent <code>DocumentReferenceList</code> in this object
     * model that references the specified record.
     * @param recordID The record ID to which the reference sought, must refer.
     * @return The dependent <code>DocumentReferenceList</code> in this object
     * model that references the specified the specified record.
     */
    public DocumentReferenceList findDependentReference(String recordID)
    {
        for (DataRecord record : getDataRecords())
        {
            for (DocumentReferenceList reference : record.getReferences(recordID))
            {
                if (!reference.isIndependent())
                {
                    return reference;
                }
            }
        }

        return null;
    }

    /**
     * Returns all independent references from this record or any of the records
     * in the same context as this record that are available in the current
     * record object model.
     * @return All independent references reachable from this record's content
     * in the object model.
     */
    public Set<String> getAllIndependentReferences()
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (DataRecord record : getDataFileRecords())
        {
            idSet.addAll(record.getSubRecordReferenceIDSet(false, true));
        }

        return idSet;
    }

    /**
     * Returns a set of all the Record ID's referenced from from this record or
     * one of its descendant records.  This means that the set may contain
     * references to records that are externally persisted but not part of the
     * current object model.
     * @param includeDependentReferences
     * @param includeIndependentReferences
     * @return The set of Record ID references contained by one or more of the
     * descendant records of this record.
     */
    public Set<String> getDescendantRecordReferenceIDSet(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (String subRecordID : getSubRecordReferenceIDSet(includeDependentReferences, includeIndependentReferences))
        {
            DataRecord subRecord;

            idSet.add(subRecordID);
            subRecord = getSubRecord(subRecordID);
            if (subRecord != null)
            {
                idSet.addAll(subRecord.getDescendantRecordReferenceIDSet(includeDependentReferences, includeIndependentReferences));
            }
        }

        return idSet;
    }

    /**
     * Returns a set of all the Record ID's referenced from records in the
     * current record object graph.  This means that the set may contain
     * references to records that are externally persisted but not part
     * of the current object model.
     * @param includeDependentReferences
     * @param includeIndependentReferences
     * @return The set of Record ID references contained by any of the records
     * in the object graph of which this record is a part.
     */
    public Set<String> getRecordReferenceIDSet(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        Set<String> recordReferences;
        Set<DataRecord> dataRecords;

        recordReferences = new HashSet<>();
        dataRecords = getDataRecords();
        for (DataRecord dataRecord : dataRecords)
        {
            recordReferences.addAll(dataRecord.getSubRecordReferenceIDSet(includeDependentReferences, includeIndependentReferences));
        }

        return recordReferences;
    }

    /**
     * Returns a set of all the Record ID's referenced from this record.  This
     * means that the set may contain references to records that are externally
     * persisted but not part of the current object model.
     * @param includeDependentReferences
     * @param includeIndependentReferences
     * @return The set of Record ID references by this record.
     */
    public Set<String> getSubRecordReferenceIDSet(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        HashSet<String> subRecordIDSet;

        subRecordIDSet = new HashSet<>();
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                for (RecordValue recordValue : recordProperty.getAllRecordValues())
                {
                    if (recordValue instanceof DocumentReferenceList)
                    {
                        DocumentReferenceList referenceList;

                        referenceList = (DocumentReferenceList)recordValue;
                        if (referenceList.isIndependent())
                        {
                            if (includeIndependentReferences)
                            {
                                subRecordIDSet.addAll(referenceList.getReferenceIds());
                            }
                        }
                        else if (includeDependentReferences)
                        {
                            subRecordIDSet.addAll(referenceList.getReferenceIds());
                        }
                    }
                }
            }
        }

        return subRecordIDSet;
    }

    public List<DataRecord> getSubRecordsReferencedFromProperty(String propertyID)
    {
        ArrayList<DataRecord> subRecordList;
        RecordProperty recordProperty;

        subRecordList = new ArrayList<>();
        recordProperty = this.getRecordProperty(propertyID);
        if (recordProperty != null)
        {
            RecordValue recordValue;

            recordValue = recordProperty.getRecordValue();
            if (recordValue instanceof DocumentReferenceList)
            {
                for (String referencedRecordID : ((DocumentReferenceList)recordValue).getReferenceIds())
                {
                    DataRecord referencedRecord;

                    referencedRecord = getSubRecord(referencedRecordID);
                    if (referencedRecord != null) subRecordList.add(referencedRecord);
                }
            }
        }

        return subRecordList;
    }

    public List<String> getSubRecordIDsReferencedFromProperty(String propertyID)
    {
        ArrayList<String> subRecordIdList;
        RecordProperty recordProperty;

        subRecordIdList = new ArrayList<>();
        recordProperty = this.getRecordProperty(propertyID);
        if (recordProperty != null)
        {
            for (RecordValue recordValue : recordProperty.getAllRecordValues())
            {
                if (recordValue instanceof DocumentReferenceList)
                {
                    subRecordIdList.addAll(((DocumentReferenceList)recordValue).getReferenceIds());
                }
            }
        }

        return subRecordIdList;
    }

    /**
     * Returns the first property from this record that contains a reference to the specified record.
     * @param referencedRecordID The referenced ID to search for.
     * @return The first RecordProperty object containing the specified reference.
     */
    public RecordProperty getPropertyContainingReference(String referencedRecordID)
    {
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                RecordValue recordValue;

                recordValue = recordProperty.getRecordValue();
                if (recordValue instanceof DocumentReferenceList)
                {
                    DocumentReferenceList referenceList;

                    referenceList = (DocumentReferenceList)recordValue;
                    if (referenceList.containsReference(referencedRecordID))
                    {
                        return recordProperty;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Returns the properties from this record that contain a reference to the specified record.
     * @param referencedRecordID The referenced ID to search for.
     * @return The list of RecordProperty objects containing the specified reference.
     */
    public List<RecordProperty> getPropertiesContainingReference(String referencedRecordID)
    {
        List<RecordProperty> propertyList;

        propertyList = new ArrayList<RecordProperty>();
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                RecordValue recordValue;

                recordValue = recordProperty.getRecordValue();
                if (recordValue instanceof DocumentReferenceList)
                {
                    DocumentReferenceList referenceList;

                    referenceList = (DocumentReferenceList)recordValue;
                    if (referenceList.containsReference(referencedRecordID))
                    {
                        propertyList.add(recordProperty);
                    }
                }
            }
        }

        return propertyList;
    }

    public boolean containsReference(String referencedRecordID)
    {
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                RecordValue recordValue;

                recordValue = recordProperty.getRecordValue();
                if (recordValue instanceof DocumentReferenceList)
                {
                    DocumentReferenceList referenceList;

                    referenceList = (DocumentReferenceList)recordValue;
                    if (referenceList.containsReference(referencedRecordID)) return true;
                }
            }
        }

        return false;
    }

    public boolean containsReference(String subRecordID, boolean independent)
    {
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                RecordValue recordValue;

                recordValue = recordProperty.getRecordValue();
                if (recordValue instanceof DocumentReferenceList)
                {
                    DocumentReferenceList referenceList;

                    referenceList = (DocumentReferenceList)recordValue;
                    if (!(independent ^ (referenceList.isIndependent()))) // Only if both dependent or both independent.
                    {
                        if (referenceList.containsReference(subRecordID)) return true;
                    }
                }
            }
        }

        return false;
    }

    public List<RecordProperty> getDataRecordReferenceProperties(String subRecordId)
    {
        List<RecordProperty> propertyList;

        propertyList = new ArrayList<>();
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                for (RecordValue recordValue : recordProperty.getAllRecordValues())
                {
                    RequirementType requirementType;

                    requirementType = recordValue.getValueRequirement().getRequirementType();
                    if (requirementType == RequirementType.DOCUMENT_REFERENCE)
                    {
                        if (subRecordId.equals(recordValue.getValue()))
                        {
                            propertyList.add(recordProperty);
                        }
                    }
                }
            }
        }

        return propertyList;
    }

    public void setRecordSection(RecordSection recordSection)
    {
        SectionRequirement thisSectionRequirement;
        SectionRequirement newSectionRequirement;

        newSectionRequirement = recordSection.getSectionRequirement();
        thisSectionRequirement = drInstance.getDataRequirement().getEquivalentSectionRequirement(newSectionRequirement);
        if (thisSectionRequirement != null)
        {
            // Remove the existing section.
            removeRecordSection(thisSectionRequirement.getConceptID());

            // Set the new section.
            recordSection.setParent(this);
            recordSections.add(recordSection);
        }
        else throw new IllegalArgumentException("Cannot add RecordSection " + recordSection + " to Data Record '" + id + "' because no equivalent Section Requirement was found.");

    }

    public boolean removeRecordSection(String sectionID)
    {
        RecordSection matchingSection;

        matchingSection = getRecordSection(sectionID);
        if (matchingSection != null)
        {
            return removeRecordSection(matchingSection);
        }
        else return false;
    }

    public boolean removeRecordSection(RecordSection recordSection)
    {
        if (recordSections.remove(recordSection))
        {
            recordSection.setParent(null);
            return true;
        }
        else return false;
    }

    public String getUpdatedByID()
    {
        return updatedById;
    }

    public void setUpdatedByID(String updatedByID)
    {
        this.updatedById = updatedByID;
    }

    public String getUpdatedByIid()
    {
        return this.updatedByIid;
    }

    public void setUpdatedByIid(String updatedByIid)
    {
        this.updatedByIid = updatedByIid;
    }

    public void setUpdatedAt(T8Timestamp updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public T8Timestamp getUpdatedAt()
    {
        return updatedAt;
    }

    /**
     * Returns the last time at which any of the records in this data file
     * was updated.
     * @return The last time at which any of the records in this data file
     * was updated.  Returns null if no updates to this data file have been
     * made since its creation.
     */
    public T8Timestamp getDataFileUpdatedAt()
    {
        T8Timestamp maxUpdatedAt;

        maxUpdatedAt = updatedAt;
        for (DataRecord dataRecord : getDataFileRecords())
        {
            T8Timestamp recordUpdatedAt;

            recordUpdatedAt = dataRecord.getUpdatedAt();
            if (recordUpdatedAt != null)
            {
                if ((maxUpdatedAt == null) || (maxUpdatedAt.getMilliseconds() < recordUpdatedAt.getMilliseconds()))
                {
                    maxUpdatedAt = recordUpdatedAt;
                }
            }
        }

        return maxUpdatedAt;
    }

    /**
     * Returns the last user to updated one of the records in this data file and
     * also the time at which the updated was persisted.
     * @return The last user to updated one of the records in this data file and
     * also the time at which the updated was persisted.  Returns null if no
     * updates to this data file have been made since its creation.
     */
    public Pair<String, T8Timestamp> getDataFileUpdatedBy()
    {
        T8Timestamp maxUpdatedAt;
        String maxUpdatedBy;

        // Find the last updated date in the date file.
        maxUpdatedBy = null;
        maxUpdatedAt = updatedAt;
        for (DataRecord dataRecord : getDataFileRecords())
        {
            T8Timestamp recordUpdatedAt;

            recordUpdatedAt = dataRecord.getUpdatedAt();
            if (recordUpdatedAt != null)
            {
                if ((maxUpdatedAt == null) || (maxUpdatedAt.getMilliseconds() < recordUpdatedAt.getMilliseconds()))
                {
                    maxUpdatedAt = recordUpdatedAt;
                    maxUpdatedBy = dataRecord.getUpdatedByID();
                }
            }
        }

        // If we found an updated date, return it or else null.
        if (maxUpdatedAt != null)
        {
            return new Pair<>(maxUpdatedBy, maxUpdatedAt);
        }
        else
        {
            return null;
        }
    }

    /**
     * @see #getDataFileUpdatedAt()
     *
     * @return Last updated {@code DataRecord} instead of only the
     *      {@code T8Timestamp}
     */
    public DataRecord getDataFileLastUpdatedRecord()
    {
        T8Timestamp recordUpdatedAt;
        DataRecord maxUpdateRecord;
        long maxUpdatedAt;

        // Find the last updated date in the date file.
        if (updatedAt == null)
        {
            maxUpdateRecord = null;
            maxUpdatedAt = 0;
        }
        else
        {
            maxUpdateRecord = this;
            maxUpdatedAt = updatedAt.getMilliseconds();
        }

        for (DataRecord dataRecord : getDataFileRecords())
        {
            if ((recordUpdatedAt = dataRecord.getUpdatedAt()) != null)
            {
                if (maxUpdatedAt < recordUpdatedAt.getMilliseconds())
                {
                    maxUpdatedAt = recordUpdatedAt.getMilliseconds();
                    maxUpdateRecord = dataRecord;
                }
            }
        }

        return maxUpdateRecord;
    }

    public String getInsertedByID()
    {
        return insertedById;
    }

    public void setInsertedByID(String insertedByID)
    {
        this.insertedById = insertedByID;
    }

    public String getInsertedByIid()
    {
        return this.insertedByIid;
    }

    public void setInsertedByIid(String insertedByIid)
    {
        this.insertedByIid = insertedByIid;
    }

    public T8Timestamp getInsertedAt()
    {
        return insertedAt;
    }

    public void setInsertedAt(T8Timestamp insertedAt)
    {
        this.insertedAt = insertedAt;
    }

    public void clearDataStrings()
    {
        this.dataStrings.clear();
    }

    public T8DataString getDataString(String dsInstanceId)
    {
        for (T8DataString dataString : dataStrings)
        {
            if (dataString.getInstanceId().equals(dsInstanceId))
            {
                return dataString;
            }
        }

        return null;
    }

    public List<T8DataString> getDataStrings()
    {
        return new ArrayList<>(dataStrings);
    }

    public void setDataStrings(List<T8DataString> dataStrings)
    {
        this.dataStrings.clear();
        if (dataStrings != null)
        {
            this.dataStrings.addAll(dataStrings);
        }
    }

    public void addDataString(T8DataString dataString)
    {
        if (dataString != null)
        {
            this.dataStrings.add(dataString);
        }
    }

    public void addDataStrings(List<T8DataString> dataStrings)
    {
        this.dataStrings.addAll(dataStrings);
    }

    public boolean removeDataString(T8DataString dataString)
    {
        return dataStrings.remove(dataString);
    }

    public boolean removeDataString(String dsInstanceId)
    {
        Iterator<T8DataString> dsIterator;
        boolean result;

        result = false;
        dsIterator = dataStrings.iterator();
        while (dsIterator.hasNext())
        {
            if (dsIterator.next().getInstanceId().equals(dsInstanceId))
            {
                dsIterator.remove();
                result = true;
            }
        }

        return result;
    }

    public void clearDataStringParticleSettings()
    {
        dataStringParticleSettings.clear();
    }

    public T8DataStringParticleSettings getDataStringParticleSettings(String id)
    {
        for (T8DataStringParticleSettings settings : dataStringParticleSettings)
        {
            if (id.equals(settings.getId()))
            {
                return settings;
            }
        }

        return null;
    }

    public List<T8DataStringParticleSettings> getDataStringParticleSettings()
    {
        return new ArrayList<T8DataStringParticleSettings>(dataStringParticleSettings);
    }

    public void setDataStringParticleSettings(List<T8DataStringParticleSettings> particleSettings)
    {
        dataStringParticleSettings.clear();
        if (particleSettings != null)
        {
            for (T8DataStringParticleSettings settings : particleSettings)
            {
                addDataStringParticleSettings(settings);
            }
        }
    }

    public void addDataStringParticleSettings(T8DataStringParticleSettings settings)
    {
        if (settings != null)
        {
            settings.setRecordId(id);
            dataStringParticleSettings.add(settings);
        }
        else throw new IllegalArgumentException("Null particle settings object cannot be added to data record.");
    }

    public boolean containsDataStringParticleSettings(T8DataStringParticleSettings settings)
    {
        return dataStringParticleSettings.contains(settings);
    }

    public boolean removeDataStringParticleSettings(T8DataStringParticleSettings settings)
    {
        return dataStringParticleSettings.remove(settings);
    }

    public void clearAttachmentDetails()
    {
        for (RecordValue value : getAllValues())
        {
            if (value instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)value;
                attachmentList.clearAttachmentDetails();
            }
        }
    }

    public Map<String, T8AttachmentDetails> getAttachmentDetails()
    {
        Map<String, T8AttachmentDetails> attachmentDetails;

        attachmentDetails = new HashMap<String, T8AttachmentDetails>();
        for (RecordValue value : getAllValues())
        {
            if (value instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)value;
                attachmentDetails.putAll(attachmentList.getAttachmentDetails());
            }
        }

        return attachmentDetails;
    }

    public T8AttachmentDetails getAttachmentDetails(String attachmentID)
    {
        for (RecordValue value : getAllValues())
        {
            if (value instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)value;
                if (attachmentList.containsAttachment(attachmentID))
                {
                    return attachmentList.getAttachmentDetails(attachmentID);
                }
            }
        }

        return null;
    }

    public void setAttachmentDetails(Map<String, T8AttachmentDetails> attachmentDetails)
    {
        for (RecordValue value : getAllValues())
        {
            if (value instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)value;
                attachmentList.clearAttachmentDetails();
                for (String attachmentId : attachmentDetails.keySet())
                {
                    if (attachmentList.containsAttachment(attachmentId))
                    {
                        attachmentList.addAttachmentDetails(attachmentId, attachmentDetails.get(attachmentId));
                    }
                }
            }
        }
    }

    public void addAttachmentDetails(String attachmentId, T8AttachmentDetails attachmentDetails)
    {
        for (RecordValue value : getAllValues())
        {
            if (value instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)value;
                if (attachmentList.containsAttachment(attachmentId))
                {
                    attachmentList.addAttachmentDetails(attachmentId, attachmentDetails);
                }
            }
        }
    }

    public void addAttachmentDetails(Map<String, T8AttachmentDetails> attachmentDetails)
    {
        for (RecordValue value : getAllValues())
        {
            if (value instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)value;
                for (String attachmentId : attachmentDetails.keySet())
                {
                    if (attachmentList.containsAttachment(attachmentId))
                    {
                        attachmentList.addAttachmentDetails(attachmentId, attachmentDetails.get(attachmentId));
                    }
                }
            }
        }
    }

    /**
     * Returns the ID of this record's direct parent.  A parent is defined as the owner of a dependent sub-record.
     * If no parent record exists, such as when this record is independent or not linked to a
     * complete record structure, null is returned.  No record can have more than one parent.  Independent
     * root records have no parent.
     * @return The ID of the direct parent record of this record.
     */
    public String getParentRecordID()
    {
        DataRecord parentRecord;

        parentRecord = getParentRecord();
        return parentRecord != null ? parentRecord.getID() : null;
    }

    /**
     * Returns this record's direct parent.  A parent is defined as the owner of a dependent sub-record.
     * If no parent record exists, such as when this record is independent or not linked to a
     * complete record structure, null is returned.  No record can have more than one parent.  Independent
     * root records have no parent.
     * @return The direct parent record of this record.
     */
    public DataRecord getParentRecord()
    {
        if (isIndependentRecord())
        {
            return null;
        }
        else if (referrerRecords.isEmpty())
        {
            return null;
        }
        else
        {
            for (DataRecord referrerRecord : referrerRecords)
            {
                if (referrerRecord.getSubRecordReferenceIDSet(true, false).contains(id))
                {
                    return referrerRecord;
                }
            }

            return null;
        }
    }

    /**
     * Returns this record's direct parent property i.e. the property in this record's parent from which this record is
     * referenced as dependant.
     * @return The direct parent record of this record.
     */
    public String getParentPropertyId()
    {
        DataRecord parentRecord;

        parentRecord = getParentRecord();
        if (parentRecord != null)
        {
            return parentRecord.getParentPropertyId(id);
        }
        else return null;
    }

    /**
     * Returns the id of the property in this record that contains the dependent reference to the specified sub-record (child).
     * If the specified sub-record is not dependently referenced from this record, null is returned.
     * @param subRecordId The id of the sub-record for which the parent property will be returned.
     * @return Returns the id of the parent property of the specified sub-record or null if not found.
     */
    public String getParentPropertyId(String subRecordId)
    {
        RecordProperty parentProperty;

        parentProperty = getParentProperty(subRecordId);
        return parentProperty != null ? parentProperty.getId() : null;
    }

    /**
     * Returns the property in this record that contains the dependent reference to the specified sub-record (child).
     * If the specified sub-record is not dependently referenced from this record, null is returned.
     * @param subRecordId The id of the sub-record for which the parent property will be returned.
     * @return Returns the parent property of the specified sub-record or null if not found.
     */
    public RecordProperty getParentProperty(String subRecordId)
    {
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                for (RecordValue recordValue : recordProperty.getAllRecordValues())
                {
                    if (recordValue instanceof DocumentReferenceList)
                    {
                        DocumentReferenceList referenceList;

                        referenceList = (DocumentReferenceList)recordValue;
                        if ((!referenceList.isIndependent()) && (referenceList.containsReference(subRecordId)))
                        {
                           return recordProperty;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Returns the collection of records in the object model that refer to this
     * record.
     * @return The referrer record collection of this record.
     */
    public List<DataRecord> getReferrerRecords()
    {
        return new ArrayList<>(referrerRecords);
    }

    /**
     * Returns all Data Files in the current object model.  The Data File of a
     * record is its independent ancestor (of which each document can have only
     * one).
     * @return A list of all Data Files in the current object model.
     */
    public List<DataRecord> getDataFiles()
    {
        List<DataRecord> dataFiles;

        dataFiles = new ArrayList<>();
        for (DataRecord dataRecord: getDataRecords())
        {
            DataRecord dataFile;

            dataFile = dataRecord.getDataFile();
            if (!dataFiles.contains(dataFile)) dataFiles.add(dataFile);
        }

        return dataFiles;
    }

    /**
     * Returns the Data File of this document.  The Data File of a record is its
     * independent ancestor (of which each document can have only one).
     * @return The Data File document of this record.
     */
    public DataRecord getDataFile()
    {
        if (isIndependentRecord()) return this;
        else
        {
            DataRecord parentRecord;

            parentRecord = getParentRecord();
            if (parentRecord != null) return parentRecord.getDataFile();
            else return null;
        }
    }

    /**
     * Returns the highest level record in the current structure i.e. the root
     * of the linked record hierarchy to which this record is belongs.
     * @return The highest level record in the current structure.
     */
    public DataRecord getRootRecord()
    {
        // Although each data record can have multiple parents, we always build the hierarchy from one single root.
        // Therefore, we can use any of the parent records to reach the root.
        if (referrerRecords.size() > 0)
        {
            return referrerRecords.get(0).getRootRecord();
        }
        else return this;
    }

    /**
     * Returns the level of this record from the root of the data file to which
     * it belongs.  The root of a data file is at level 0.  If this record does
     * not belong to a data file, -1 is returned.
     * @return The level of this record in the data file to which it belongs or
     * -1 if this record does not belong to a data file.
     */
    public int getRecordLevel()
    {
        if (isIndependentRecord()) return 0;
        else
        {
            DataRecord parentRecord;

            parentRecord = getParentRecord();
            if (parentRecord != null) return parentRecord.getRecordLevel() + 1;
            else return -1;
        }
    }

    /**
     * Adds the specified record as a referrer to this data record.
     * @param referrerRecord The data record to add as a referrer to
     * this record.
     */
    private void addReferrerRecord(DataRecord referrerRecord)
    {
        if (!referrerRecords.contains(referrerRecord))
        {
            referrerRecords.add(referrerRecord);
        }
    }

    private boolean removeReferrerRecord(DataRecord record)
    {
        return referrerRecords.remove(record);
    }

    public Set<DataRecord> getSubRecordSet(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        Set<DataRecord> recordSet;

        recordSet = new HashSet<>();
        for (String subRecordID : this.getSubRecordReferenceIDSet(includeDependentReferences, includeIndependentReferences))
        {
            DataRecord subRecord;

            subRecord = getSubRecord(subRecordID);
            if (subRecord != null)
            {
                recordSet.add(subRecord);
            }
        }

        return recordSet;
    }

    public List<DataRecord> getSubRecords()
    {
        return new ArrayList<>(subRecords);
    }

    public DataRecord getSubRecord(String subRecordID)
    {
        for (DataRecord subRecord : subRecords)
        {
            if (subRecord.getID().equals(subRecordID))
            {
                return subRecord;
            }
        }

        return null;
    }

    @Deprecated
    public void clearSubRecords()
    {
        subRecords.clear();
    }

    /**
     * Adds the specified data record to the sub-record collection of this
     * data record, updating is parent reference to reflect the new hierarchy.
     * @param dataRecord The data record to add to this record as a sub-record.
     */
    public void addSubRecord(DataRecord dataRecord)
    {
        if (!containsSubRecord(dataRecord.getID()))
        {
            this.subRecords.add(dataRecord);
            dataRecord.addReferrerRecord(this);
            accessLayer.addSubRecordAccessLayer(dataRecord.getAccessLayer());
        }
        else throw new IllegalArgumentException("Cannot add record '" + dataRecord + "' as sub-record since it already exists in sub-record collection: " + subRecords);
    }

    public boolean containsSubRecord(String recordID)
    {
        for (DataRecord subRecord : subRecords)
        {
            if (subRecord.getID().equals(recordID)) return true;
        }

        return false;
    }

    /**
     * Returns true if a record with the specified ID exists within the current
     * object model.
     * @param recordID The ID of the record to find.
     * @return true if a record with the specified ID exists within the current
     * object model.
     */
    public boolean containsDataRecord(String recordID)
    {
        return findDataRecord(recordID) != null;
    }

    public void setSubRecords(List<DataRecord> subRecords)
    {
        removeAllSubRecords();
        if (subRecords != null)
        {
            for (DataRecord subRecord : subRecords)
            {
                addSubRecord(subRecord);
            }
        }
    }

    /**
     * Removes this document from all of its parents.  The references in the
     * parent record properties will still remain.
     */
    public void removeFromReferrerRecords()
    {
        removeFromReferrerRecords(false);
    }

    public void removeFromReferrerRecords(boolean removeReferences)
    {
        List<DataRecord> referrerRecordList;

        referrerRecordList = getReferrerRecords();
        for (DataRecord referrerRecord : referrerRecordList)
        {
            referrerRecord.removeSubRecord(this);
            if (removeReferences) referrerRecord.removeDocumentReference(id);
        }
    }

    /**
     * Removes the document reference for the specific section of the record,
     * based on the specified set of ID's which define where the reference
     * should be removed from.
     * If the reference should simply be removed, regardless of where it is
     * located, then the {@link #removeDocumentReference(java.lang.String)}
     * method would be more appropriate.
     *
     * @param propertyID The {@code String} ID of the property to which the
     *      document reference should be linked
     * @param fieldID The {@code String} ID of the field to which the document
     *      reference should be linked
     * @param drInstanceID The {@code String} DR Instance ID at which the
     *      document reference should be linked
     * @param drGroupID The {@code String} DR group ID to which the document
     *      reference should be linked
     * @param recordID The {@code String} record ID defining the document
     *      reference to be removed
     */
    public void removeDocumentReference(String propertyID, String fieldID, String drInstanceID, String drGroupID, String recordID)
    {
        PropertyRequirement propertyRequirement;

        // Find the target property requirement.
        propertyRequirement = drInstance.getDataRequirement().getPropertyRequirement(propertyID);
        if (propertyRequirement != null)
        {
            SectionRequirement sectionRequirement;
            RecordSection targetSection;
            RecordProperty property;

            sectionRequirement = propertyRequirement.getParentSectionRequirement();
            targetSection = getRecordSection(sectionRequirement.getConceptID());
            if (targetSection == null)
            {
                targetSection = new RecordSection(sectionRequirement);
                setRecordSection(targetSection);
            }

            // Get the record property
            property = targetSection.getRecordProperty(propertyID);
            if (property != null) // Property exists, so we can remove the reference
            {
                property.removeDocumentReference(fieldID, drInstanceID, recordID);
            } else throw new RuntimeException("Property not found: ID ["+propertyID+"] in Record ["+getID()+"]");
        } else throw new RuntimeException("Property not found: ID:" + propertyID + " in DR:" + getDataRequirement());
    }

    /**
     * Removes all references to the specified record ID.
     *
     * @param recordID The record ID for which references should be removed
     */
    public void removeDocumentReference(String recordID)
    {
        for (RecordSection recordSection : recordSections)
        {
            for (RecordProperty recordProperty : recordSection.getRecordProperties())
            {
                for (RecordValue recordValue : recordProperty.getAllRecordValues())
                {
                    if (recordValue instanceof DocumentReferenceList)
                    {
                        ((DocumentReferenceList)recordValue).removeReference(recordID, false);
                    }
                }
            }
        }
    }

    public void removeSubRecord(DataRecord subRecord)
    {
        // Remove the record from this record's list of sub-records.
        if (subRecords.remove(subRecord))
        {
            // If we successfully remove the sub-record, make sure this record is no longer listed as its parent.
            subRecord.removeReferrerRecord(this);
            subRecord.getAccessLayer().removeFromParent();
        }
    }

    public DataRecord removeSubRecord(String subRecordId)
    {
        DataRecord recordToRemove;

        recordToRemove = getSubRecord(subRecordId);
        if (recordToRemove != null)
        {
            removeSubRecord(recordToRemove);
            return recordToRemove;
        }
        else return null;
    }

    /**
     * Removes all sub-records from this DataRecord objects subRecords collection that are not referenced by one
     * or more of its properties.
     */
    public void removeUnreferencedSubRecords()
    {
        Iterator<DataRecord> subRecordIterator;
        Set<String> references;

        references = this.getSubRecordReferenceIDSet(true, false);
        subRecordIterator = subRecords.iterator();
        while (subRecordIterator.hasNext())
        {
            if (!references.contains(subRecordIterator.next().getId()))
            {
                subRecordIterator.remove();
            }
        }
    }

    public void removeAllSubRecords()
    {
        // First remove this record as a parent from all sub-records.
        for (DataRecord subRecord : subRecords)
        {
            subRecord.removeReferrerRecord(this);
        }

        // Clear the collection of sub-records.
        subRecords.clear();
    }

    public Set<String> getContextDataRequirementInstanceIDSet()
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (DataRecord contextRecord : getDataFileRecords())
        {
            idSet.add(contextRecord.getDataRequirementInstance().getConceptID());
        }

        return idSet;
    }

    public Set<String> getContextDataRequirementIDSet()
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (DataRecord contextRecord : getDataFileRecords())
        {
            idSet.add(contextRecord.getDataRequirement().getConceptID());
        }

        return idSet;
    }

    public Set<String> getDescendantDataRequirementInstanceIDSet(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (DataRecord record : getDescendantRecordSet(includeDependentReferences, includeIndependentReferences))
        {
            idSet.add(record.getDataRequirementInstance().getConceptID());
        }

        return idSet;
    }

    public Set<String> getDescendantDataRequirementIDSet(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        Set<String> idSet;

        idSet = new HashSet<>();
        for (DataRecord record : getDescendantRecordSet(includeDependentReferences, includeIndependentReferences))
        {
            idSet.add(record.getDataRequirement().getConceptID());
        }

        return idSet;
    }

    public List<String> getPathRecordIDList()
    {
        List<String> idList;

        idList = new ArrayList<>();
        for (DataRecord record : getAncestorRecords())
        {
            idList.add(record.getID());
        }

        idList.add(id); // This record is the last in the path.
        return idList;
    }

    public List<String> getAncestorRecordIDList()
    {
        List<String> idList;

        idList = new ArrayList<>();
        for (DataRecord record : getAncestorRecords())
        {
            idList.add(record.getID());
        }

        return idList;
    }

    public List<DataRecord> getAncestorRecords()
    {
        List<DataRecord> ancestors;

        ancestors = new ArrayList<>(referrerRecords);
        for (DataRecord parentRecord : referrerRecords)
        {
            ancestors.addAll(parentRecord.getAncestorRecords());
        }

        return ancestors;
    }

    /**
     * Returns a list of records ID's created using depth-first traversal starting from this record and including all descendants.
     * That means the first record ID in the result list will be the ID of this record.
     * All descendants of this record currently available in the object model will be included in the result list.
     * @return A list of records ID's created using depth-first traversal starting from this record.
     */
    public List<String> getDepthFirstRecordIDList()
    {
        LinkedList<DataRecord> stack;
        List<String> idList;

        // Create a result list and processing stack.
        idList = new ArrayList<>();
        stack = new LinkedList<DataRecord>();
        stack.push(this);
        while (!stack.isEmpty())
        {
            DataRecord nextRecord;

            // Get the next record from the stack.
            nextRecord = stack.pop();
            idList.add(nextRecord.getID());

            // Push all sub-records onto the stack.
            for (DataRecord subRecord : nextRecord.getSubRecords())
            {
                stack.push(subRecord);
            }
        }

        // Return the result.
        return idList;
    }

    /**
     * Returns a list of records ID's created using breadth-first traversal starting from this record and including all descendants.
     * That means the first record ID in the result list will be the ID of this record.
     * All descendants of this record currently available in the object model will be included in the result list.
     * @return A list of records ID's created using depth-first traversal starting from this record.
     */
    public List<String> getBreadthFirstRecordIDList()
    {
        LinkedList<DataRecord> queue;
        List<String> idList;

        // Create a result list and processing queue.
        idList = new ArrayList<>();
        queue = new LinkedList<DataRecord>();
        queue.add(this);
        while (!queue.isEmpty())
        {
            DataRecord nextRecord;

            // Get the next record from the queue.
            nextRecord = queue.removeFirst();
            idList.add(nextRecord.getID());

            // Add all sub-records at the end of the queue.
            for (DataRecord subRecord : nextRecord.getSubRecords())
            {
                queue.add(subRecord);
            }
        }

        // Return the result.
        return idList;
    }

    public List<String> getDescendantRecordIDList()
    {
        List<String> idList;

        idList = new ArrayList<>();
        for (DataRecord record : getDescendantRecords())
        {
            idList.add(record.getID());
        }

        return idList;
    }

    public List<DataRecord> getDescendantRecords()
    {
        ArrayList<DataRecord> descendants;

        descendants = new ArrayList<>(subRecords);
        for (DataRecord subRecord : subRecords)
        {
            descendants.addAll(subRecord.getDescendantRecords());
        }

        return descendants;
    }

    /**
     * Returns all independent root records from the current object model.
     * @return A list of all independent root records from the current object
     * model.
     */
    public List<DataRecord> getIndependentRootRecords()
    {
        List<DataRecord> recordList;

        recordList = new ArrayList<>();
        for (DataRecord record : getDataRecords())
        {
            if (record.isIndependentRecord())
            {
                recordList.add(record);
            }
        }

        return recordList;
    }

    /**
     * Returns all independent root records that are descendants of this record.
     * @return A list of all independent root records that are descendants of
     * this record.
     */
    public List<DataRecord> getIndependentDescendantRootRecords()
    {
        ArrayList<DataRecord> recordList;

        recordList = new ArrayList<>();
        for (DataRecord subRecord : subRecords)
        {
            if (subRecord.isIndependentRecord())
            {
                recordList.add(subRecord);
            }

            recordList.addAll(subRecord.getIndependentDescendantRootRecords());
        }

        return recordList;
    }

    public Set<DataRecord> getDescendantRecordSet(boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        Set<DataRecord> recordSet;

        recordSet = new HashSet<>();
        for (String subRecordID : this.getSubRecordReferenceIDSet(includeDependentReferences, includeIndependentReferences))
        {
            DataRecord subRecord;

            subRecord = getSubRecord(subRecordID);
            if (subRecord != null)
            {
                recordSet.add(subRecord);
                recordSet.addAll(subRecord.getDescendantRecordSet(includeDependentReferences, includeIndependentReferences));
            }
        }

        return recordSet;
    }

    public List<String> getLineageRecordIDList()
    {
        List<String> idList;

        idList = new ArrayList<>();
        for (DataRecord record : getLineageRecords())
        {
            idList.add(record.getID());
        }

        return idList;
    }

    public List<DataRecord> getLineageRecords()
    {
        List<DataRecord> lineage;

        lineage = new ArrayList<>();
        lineage.addAll(getAncestorRecords());
        lineage.add(this);
        lineage.addAll(getDescendantRecords());

        return lineage;
    }

    /**
     * Returns all data records that are in the data record model and in the
     * same file as this data record.
     * @return All data records in the context of the this record
     */
    public Set<DataRecord> getDataFileRecords()
    {
        DataRecord dataFile;

        dataFile = getDataFile();
        if (dataFile != null)
        {
            Set<DataRecord> contextRecords;

            contextRecords = new HashSet<DataRecord>();
            contextRecords.add(dataFile);
            contextRecords.addAll(dataFile.getDescendantRecordSet(true, false));
            return contextRecords;
        }
        else throw new RuntimeException("No owner found for record: " + this);
    }

    /**
     * Returns the set of all data records that are part of the record object
     * model to which this data record belongs.
     * @return A set of all data records that are part of the record object
     * model to which this data record belongs.
     */
    public Set<DataRecord> getDataRecords()
    {
        Map<String, DataRecord> dataRecords;
        LinkedList<DataRecord> recordQueue;

        // Create a collection to hold all of the required records.
        dataRecords = new HashMap<>();
        dataRecords.put(getID(), this);

        // No travers the hierarchy and add all records found to the map (avoiding cyclic traversals).
        recordQueue = new LinkedList<>();
        recordQueue.add(this);
        while (recordQueue.size() > 0)
        {
            DataRecord nextRecord;

            // Get the next record from the queue.
            nextRecord = recordQueue.pop();

            // Add all parent records that we have not found yet.
            for (DataRecord referrerRecord : nextRecord.getReferrerRecords())
            {
                if (!dataRecords.containsKey(referrerRecord.getID()))
                {
                    dataRecords.put(referrerRecord.getID(), referrerRecord);
                    recordQueue.add(referrerRecord);
                }
            }

            // Add all sub-records that we have not found yet.
            for (DataRecord subRecord : nextRecord.getSubRecords())
            {
                if (!dataRecords.containsKey(subRecord.getID()))
                {
                    dataRecords.put(subRecord.getID(), subRecord);
                    recordQueue.add(subRecord);
                }
            }
        }

        // Return the set of data records found.
        return new HashSet<>(dataRecords.values());
    }

    /**
     * Returns the set of ID's of all records currently in the record object
     * model.
     * @return The set of ID's of all records currently in the record object
     * model.
     */
    public Set<String> getDataRecordIDSet()
    {
        Set<String> recordIDSet;
        Set<DataRecord> dataRecords;

        recordIDSet = new HashSet<>();
        dataRecords = getDataRecords();
        for (DataRecord dataRecord : dataRecords)
        {
            recordIDSet.add(dataRecord.getID());
        }

        return recordIDSet;
    }

    /**
     * Returns the set of ID's of all Data Requirements currently in the record
     * object model.
     * @return The set of ID's of all Data Requirements currently in the record
     * object model.
     */
    public Set<String> getDataRequirementIDSet()
    {
        Set<String> drIDSet;
        Set<DataRecord> dataRecords;

        drIDSet = new HashSet<>();
        dataRecords = getDataRecords();
        for (DataRecord dataRecord : dataRecords)
        {
            drIDSet.add(dataRecord.getDataRequirementID());
        }

        return drIDSet;
    }

    /**
     * Returns the set of ID's of all Data Requirement Instances currently in
     * the record object model.
     * @return The set of ID's of all Data Requirement Instances currently in
     * the record object model.
     */
    public Set<String> getDataRequirementInstanceIDSet()
    {
        Set<String> drInstanceIDSet;
        Set<DataRecord> dataRecords;

        drInstanceIDSet = new HashSet<>();
        dataRecords = getDataRecords();
        for (DataRecord dataRecord : dataRecords)
        {
            drInstanceIDSet.add(dataRecord.getDataRequirementInstanceID());
        }

        return drInstanceIDSet;
    }

    /**
     * Returns the first data record found, that references the specified child
     * record.  If specified child record is an independent record, multiple
     * references to the record may exist in the object model and only the first
     * parent will be returned.  In the event that the specified child record is
     * dependent, this method is more safe because only one parent can ever
     * exist for a dependent child.
     * @param childRecordID The ID of the child record for which to return a
     * parent record.
     * @return The first parent found, that contains a reference to the
     * specified child record.
     */
    public DataRecord findParentRecord(String childRecordID)
    {
        for (DataRecord dataRecord : getDataRecords())
        {
            if (dataRecord.getSubRecordReferenceIDSet(true, true).contains(childRecordID))
            {
                return dataRecord;
            }
        }

        return null;
    }

    /**
     * Returns a list of all data records in the object model that reference
     * the specified child record.
     * @param childRecordID The child record for which parents will be found.
     * @return The list of all records that reference the specified child.
     */
    public List<DataRecord> findParentRecords(String childRecordID)
    {
        List<DataRecord> records;

        records = new ArrayList<>();
        for (DataRecord dataRecord : getDataRecords())
        {
            if (dataRecord.getSubRecordReferenceIDSet(true, true).contains(childRecordID))
            {
                records.add(dataRecord);
            }
        }

        return records;
    }

    /**
     * Finds the record with the specified ID in the record graph to which this
     * record belongs.  If the record is not found, null is returned.
     * @param recordID The ID of the record to find.
     * @return The record, if found or else null.
     */
    public DataRecord findDataRecord(String recordID)
    {
        for (DataRecord record : getDataRecords())
        {
            if (record.getID().equals(recordID))
            {
                return record;
            }
        }

        return null;
    }

    public DataRecord findDescendantDataRecord(String recordID)
    {
        for (DataRecord subRecord : subRecords)
        {
            DataRecord matchingDescendant;

            // Check the Record ID of the sub record.
            if (subRecord.getID().equals(recordID))
            {
                return subRecord;
            }

            // Try to find a matching descendant.
            matchingDescendant = subRecord.findDescendantDataRecord(recordID);
            if (matchingDescendant != null) return matchingDescendant;
        }

        return null;
    }

    public DataRecord findAncestorDataRecord(String recordID)
    {
        for (DataRecord parentRecord : referrerRecords)
        {
            DataRecord matchingAncestor;

            // Check the Record ID of the parent record.
            if (parentRecord.getID().equals(recordID))
            {
                return parentRecord;
            }

            // Try to find a matching ancestor.
            matchingAncestor = parentRecord.findAncestorDataRecord(recordID);
            if (matchingAncestor != null) return matchingAncestor;
        }

        return null;
    }

    /**
     * Returns the first data record property matching the specified DR
     * Instance ID and Property ID and belonging to the same record graph as
     * this record.
     * @param drInstanceID The DR Instance ID to match.
     * @param propertyID The ID of the Property to match.
     * @return The first data record properties matching the specified DR
     * Instance ID and Property ID and belonging to the same record graph as
     * this record.
     */
    public RecordProperty findRecordPropertyByDRInstance(String drInstanceID, String propertyID)
    {
        Set<DataRecord> dataRecords;

        // Get all data records in the record graph.
        dataRecords = getDataRecords();

        // Add all record properties fitting the DR Instance ID and Property ID.
        for (DataRecord dataRecord : dataRecords)
        {
            if (dataRecord.getDataRequirementInstance().getConceptID().equals(drInstanceID))
            {
                RecordProperty property;

                property = dataRecord.getRecordProperty(propertyID);
                if (property != null)
                {
                    return property;
                }
            }
        }

        // Property not found.
        return null;
    }

    /**
     * Returns the set of all data record properties matching the specified DR
     * Instance ID and Property ID and belonging to the same record graph as
     * this record.
     * @param drInstanceID The DR Instance ID to match.
     * @param propertyID The ID of the Property to match.
     * @return The set of all data record properties matching the specified DR
     * Instance ID and Property ID and belonging to the same record graph as
     * this record.
     */
    public Set<RecordProperty> findRecordPropertiesByDRInstance(String drInstanceID, String propertyID)
    {
        Set<RecordProperty> matchedProperties;
        Set<DataRecord> dataRecords;

        // Get all data records in the record graph.
        dataRecords = getDataRecords();

        // Add all record properties fitting the DR Instance ID and Property ID.
        matchedProperties = new HashSet<>();
        for (DataRecord dataRecord : dataRecords)
        {
            if (dataRecord.getDataRequirementInstance().getConceptID().equals(drInstanceID))
            {
                RecordProperty property;

                property = dataRecord.getRecordProperty(propertyID);
                if (property != null)
                {
                    matchedProperties.add(property);
                }
            }
        }

        // Return the set of matched records.
        return matchedProperties;
    }

    /**
     * Returns the set of all data records matching the specified DR Instance ID
     * and belonging to the same record graph as this record.
     * @param drInstanceId The DR Instance ID to match.
     * @return The set of all data records matching the specified DR Instance ID
     * and belonging to the same record graph as this record.
     */
    public Set<DataRecord> findDataRecordsByDrInstance(String drInstanceId)
    {
        Set<DataRecord> matchedRecords;
        Set<DataRecord> dataRecords;

        // Get all data records in the record graph.
        dataRecords = getDataRecords();

        // Add all records fitting the DR Instance ID.
        matchedRecords = new HashSet<>();
        for (DataRecord dataRecord : dataRecords)
        {
            if (dataRecord.getDataRequirementInstance().getConceptID().equals(drInstanceId))
            {
                matchedRecords.add(dataRecord);
            }
        }

        // Return the set of matched records.
        return matchedRecords;
    }

    public Set<DataRecord> findDescendantDataRecordsByDRInstance(String drInstanceID)
    {
        Set<DataRecord> dataRecords;

        dataRecords = new HashSet<>();
        for (DataRecord descendantRecord : getDescendantRecords())
        {
            // Check the DR Instance of the descendant record.
            if (descendantRecord.getDataRequirementInstance().getConceptID().equals(drInstanceID))
            {
                dataRecords.add(descendantRecord);
            }
        }

        return dataRecords;
    }

    /**
     * Returns the specified set of data records from the current object model.
     * This method uses only dependent references.
     * @param setType The type of set to retrieve.
     * @return The set of record requested.
     */
    public Set<DataRecord> getDataRecordSet(T8HierarchicalSetType setType)
    {
        Set<DataRecord> recordSet;

        recordSet = new HashSet<DataRecord>();
        switch (setType)
        {
            case LINEAGE:    // From a specific node, all ancestors, all descendants including the node itself.
            {
                DataRecord currentRecord;

                // Add all ancestors and this record itself.
                currentRecord = this;
                recordSet.add(currentRecord);
                while (currentRecord.getParentRecord() != null)
                {
                    currentRecord = currentRecord.getParentRecord();
                    recordSet.add(currentRecord);
                }

                // Add all descendants.
                recordSet.addAll(getDescendantRecordSet(true, false));

                // Return the result.
                return recordSet;
            }
            case ANCESTORS:  // From a specific node, all ancestors, excluding the node itself.
            {
                DataRecord currentRecord;

                currentRecord = this;
                while (currentRecord.getParentRecord() != null)
                {
                    currentRecord = currentRecord.getParentRecord();
                    recordSet.add(currentRecord);
                }

                return recordSet;
            }
            case LINE:       // From a specific node, all ancestors, including the node itself.
            {
                DataRecord currentRecord;

                currentRecord = this;
                recordSet.add(currentRecord);
                while (currentRecord.getParentRecord() != null)
                {
                    currentRecord = currentRecord.getParentRecord();
                    recordSet.add(currentRecord);
                }

                return recordSet;
            }
            case SINGLE:     // From a specific node, only the specific node itself.
            {
                recordSet.add(this);
                return recordSet;
            }
            case HOUSE:      // From a specific node, all nodes with the same parent, including the node itself and its parent.
            {
                DataRecord parentRecord;

                parentRecord = getParentRecord();
                if (parentRecord != null)
                {
                    recordSet = parentRecord.getSubRecordSet(true, false);
                    recordSet.add(parentRecord);
                    return recordSet;
                }
                else return recordSet;
            }
            case SIBLINGS:   // From a specific node, all nodes with the same parent, excluding the node itself.
            {
                DataRecord parentRecord;

                parentRecord = getParentRecord();
                if (parentRecord != null)
                {
                    recordSet = parentRecord.getSubRecordSet(true, false);
                    recordSet.remove(this);
                    return recordSet;
                }
                else return recordSet;
            }
            case BROOD:      // From a specific node, all nodes with the same parent, including the node itself.
            {
                DataRecord parentRecord;

                parentRecord = getParentRecord();
                if (parentRecord != null)
                {
                    return parentRecord.getSubRecordSet(true, false);
                }
                else return recordSet;
            }
            case FAMILY:     // From a specific node, all descendants, including the node itself.
            {
                recordSet.add(this);
                recordSet.addAll(getDescendantRecordSet(true, false));
                return recordSet;
            }
            case DESCENDANTS: // From a specific node, all descendants, excluding the node itself.
            {
                return getDescendantRecordSet(true, false);
            }
            default:
                throw new IllegalArgumentException("Unsupported set type: " + setType);
        }
    }

    public Set<DataRecord> findDataRecordsByDrInstance(T8HierarchicalSetType setType, String drInstanceId)
    {
        Set<DataRecord> dataRecords;

        dataRecords = new HashSet<>();
        for (DataRecord ancestorRecord : getDataRecordSet(setType))
        {
            // Check the DR Instance of the ancestor record.
            if (ancestorRecord.getDataRequirementInstance().getConceptID().equals(drInstanceId))
            {
                dataRecords.add(ancestorRecord);
            }
        }

        return dataRecords;
    }

    public Set<DataRecord> findDataRecordsByDr(T8HierarchicalSetType setType, String drId)
    {
        Set<DataRecord> dataRecords;

        dataRecords = new HashSet<>();
        for (DataRecord ancestorRecord : getDataRecordSet(setType))
        {
            // Check the DR of the ancestor record.
            if (ancestorRecord.getDataRequirement().getConceptID().equals(drId))
            {
                dataRecords.add(ancestorRecord);
            }
        }

        return dataRecords;
    }

    /**
     * Fills this data record ensuring that all sections, properties and fields specified by the Data Requirement
     * exist as objects in the record model.
     * @param fillDescendants A boolean flag to indicate whether or not descendant records must also by filled.
     */
    public void fill(boolean fillDescendants)
    {
        // Fill all record sections.
        for (SectionRequirement sectionRequirement : drInstance.getDataRequirement().getSectionRequirements())
        {
            RecordSection recordSection;

            recordSection = getOrAddRecordSection(sectionRequirement.getConceptID());
            recordSection.fill();
        }

        // Fill descendants if required.
        if (fillDescendants)
        {
            for (DataRecord subRecord : subRecords)
            {
                subRecord.fill(fillDescendants);
            }
        }
    }

    public void normalize(boolean normalizeDescendants)
    {
        // Set the FFT to null if it contains no useful text.
        if ((fft != null) && (fft.trim().length() == 0)) fft = null;

        // Normalize each section.
        for (RecordSection recordSection : recordSections)
        {
            // Noramlize the section.
            recordSection.normalize();
        }

        // Normalize descendants if required.
        if (normalizeDescendants)
        {
            for (DataRecord subRecord : subRecords)
            {
                subRecord.normalize(normalizeDescendants);
            }
        }
    }

    @Override
    public void normalize()
    {
        normalize(false);
    }

    @Override
    public boolean hasContent(boolean includeFFT)
    {
        if ((!Strings.isNullOrEmpty(fft)) && (includeFFT)) return true;
        else
        {
            for (RecordSection recordSection : recordSections)
            {
                if (recordSection.hasContent(includeFFT)) return true;
            }

            return false;
        }
    }

    @Override
    public boolean isEquivalentValue(Value comparedValue)
    {
        if (!(comparedValue instanceof DataRecord))
        {
            return false;
        }
        else
        {
            DataRecord dataRecord;

            dataRecord = (DataRecord)comparedValue;
            if (drInstance.isEquivalentRequirement(dataRecord.getDataRequirementInstance()))
            {
                for (RecordSection recordSection : recordSections)
                {
                    RecordSection comparedSection;

                    comparedSection = getRecordSection(recordSection.getSectionID());
                    if (comparedSection == null)
                    {
                        return false;
                    }
                    else if (!recordSection.isEquivalentValue(comparedSection))
                    {
                        return false;
                    }
                }

                return true;
            }
            else return false;
        }
    }

    @Override
    public Value getSubValue(RequirementType requirementType, String conceptID, Integer index)
    {
        if ((requirementType == null) || (requirementType == RequirementType.SECTION))
        {
            for (RecordSection recordSection : recordSections)
            {
                if ((conceptID == null) || (recordSection.getSectionID().equals(conceptID)))
                {
                    if ((index == null) || (recordSection.getIndex() == index))
                    {
                        return recordSection;
                    }
                }
            }

            return null;
        }
        else return null;
    }

    @Override
    public List<Value> getSubValues(RequirementType requirementType, String conceptID, Integer index)
    {
        ArrayList<Value> values;

        values = new ArrayList<>();
        if ((requirementType == null) || (requirementType == RequirementType.SECTION))
        {
            for (RecordSection recordSection : recordSections)
            {
                if ((conceptID == null) || (recordSection.getSectionID().equals(conceptID)))
                {
                    if ((index == null) || (recordSection.getIndex() == index))
                    {
                        values.add(recordSection);
                    }
                }
            }
        }

        return values;
    }

    @Override
    public Value getDescendantValue(RequirementType requirementType, String conceptID, Integer index)
    {
        for (RecordSection recordSection : recordSections)
        {
            Value descendantValue;

            if ((requirementType == null) || (requirementType == RequirementType.SECTION))
            {
                if ((conceptID == null) || (recordSection.getSectionID().equals(conceptID)))
                {
                    if ((index == null) || (recordSection.getIndex() == index))
                    {
                        return recordSection;
                    }
                }
            }

            descendantValue = recordSection.getDescendantValue(requirementType, conceptID, index);
            if (descendantValue != null) return descendantValue;
        }

        return null;
    }

    @Override
    public List<Value> getDescendantValues(RequirementType requirementType, String conceptID, Integer index)
    {
        ArrayList<Value> values;

        values = new ArrayList<>();
        for (RecordSection recordSection : recordSections)
        {
            if ((requirementType == null) || (requirementType == RequirementType.SECTION))
            {
                if ((conceptID == null) || (recordSection.getSectionID().equals(conceptID)))
                {
                    if ((index == null) || (recordSection.getIndex() == index))
                    {
                        values.add(recordSection);
                    }
                }
            }

            values.addAll(recordSection.getDescendantValues(requirementType, conceptID, index));
        }

        return values;
    }

    @Override
    public void setSubValue(Value subValue)
    {
        if (subValue instanceof RecordSection)
        {
            RecordSection sectionToRemove;
            RecordSection newSection;

            // Find the section to replace (if any).
            newSection = (RecordSection)subValue;
            sectionToRemove = getRecordSection(newSection.getSectionID());
            if (sectionToRemove != null)
            {
                // Remove the existing section.
                removeRecordSection(sectionToRemove);
            }

            // Add the new section.
            setRecordSection(newSection);
        }
        else throw new IllegalArgumentException("Cannot add value to to Data Record: " + subValue);
    }

    @Override
    public boolean removeSubValue(Value subValue)
    {
        if (subValue instanceof RecordSection)
        {
            return removeRecordSection((RecordSection)subValue);
        }
        else throw new IllegalArgumentException("Incompatible value type: " + subValue);
    }

    @Override
    public int getSubValueCount()
    {
        return recordSections.size();
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("DataRecord: [");
        builder.append("ID: ");
        builder.append(getID());
        builder.append(",DataRequirement: ");
        builder.append(getDataRequirement().getConceptID());
        builder.append(",DataRequirementInstance: " );
        builder.append(getDataRequirementInstance().getConceptID());
        builder.append("]");
        return builder.toString();
    }

    public Map<String, String> assignNewRecordIdentifiers(boolean includeDescendants)
    {
        Map<String, String> identifierMapping;
        String newRecordID;

        // Generate a new record ID.
        newRecordID = T8IdentifierUtilities.createNewGUID();

        // Replace all references in the parents of this record with the new ID.
        for (DataRecord parentRecord : getReferrerRecords())
        {
            parentRecord.replaceDocumentReference(id, newRecordID);
        }

        // Create an identifier mapping and assign the new ID to this record.
        identifierMapping = new HashMap<>();
        identifierMapping.put(id, newRecordID);
        setID(newRecordID);

        // Update the record ontology.
        if (ontology != null)
        {
            ontology.setID(newRecordID);
        }

        // Assign new identifiers to descendant records.
        if (includeDescendants)
        {
            for (DataRecord subRecord : subRecords)
            {
                identifierMapping.putAll(subRecord.assignNewRecordIdentifiers(includeDescendants));
            }
        }

        return identifierMapping;
    }

    public Map<String, String> assignNewAttachmentIdentifiers(boolean includeDescendantRecords)
    {
        Map<String, String> identifierMapping;
        Map<String, T8AttachmentDetails> attachmentDetailMap;

        attachmentDetailMap = getAttachmentDetails();

        clearAttachmentDetails();
        identifierMapping = new HashMap<>();
        for (String attachmentId : attachmentDetailMap.keySet())
        {
            T8AttachmentDetails attachmentDetails;
            String newAttachmentId;

            newAttachmentId = T8IdentifierUtilities.createNewGUID();
            attachmentDetails = attachmentDetailMap.get(attachmentId);
            attachmentDetails.setAttachmentId(newAttachmentId);
            replaceAttachmentDetails(attachmentId, newAttachmentId, attachmentDetails);
            identifierMapping.put(attachmentId, newAttachmentId);
        }

        // Assign new identifiers to descendant records.
        if (includeDescendantRecords)
        {
            for (DataRecord subRecord : subRecords)
            {
                identifierMapping.putAll(subRecord.assignNewAttachmentIdentifiers(includeDescendantRecords));
            }
        }

        return identifierMapping;
    }

    public Map<String, String> assignNewDataStringParticleSettingsIds(boolean includeDescendantRecords)
    {
        Map<String, String> idMapping;

        idMapping = new HashMap<>();
        for (T8DataStringParticleSettings settings: dataStringParticleSettings)
        {
            String oldId;
            String newId;

            oldId = settings.getId();
            newId = T8IdentifierUtilities.createNewGUID();
            settings.setId(newId);
            idMapping.put(oldId, newId);
        }

        // Assign new identifiers to descendant records.
        if (includeDescendantRecords)
        {
            for (DataRecord subRecord : subRecords)
            {
                idMapping.putAll(subRecord.assignNewDataStringParticleSettingsIds(includeDescendantRecords));
            }
        }

        return idMapping;
    }

    /**
     * For each of the data dependencies in the supplied list, this method sets
     * the value to the last concept in the supplied path.  For each of the
     * affected values set by this method, a recursive call is made to update
     * that value's dependency parents in turn using the parent path of the
     * concept that was set.
     * @param dataDependencies The dependencies to set.
     * @param path
     */
    public void setDependencyValues(List<DataDependency> dataDependencies, T8ConceptGraphPath path)
    {
        String conceptId;

        conceptId = path.getLastConceptId();
        for (DataDependency dataDependency : dataDependencies)
        {
            RecordValue affectedValue;

            affectedValue = setDependencyValue(dataDependency, conceptId);
            if (!path.isFirstConceptId(conceptId))
            {
                List<DataDependency> affectedDependencies;

                affectedDependencies = affectedValue.getValueRequirement().getDataDependencies();
                if (affectedDependencies.size() > 0)
                {
                    setDependencyValues(affectedDependencies, path.getParentPath(conceptId));
                }
            }
        }
    }

    /**
     * This method sets the value matching the specified data dependency.  It is
     * important to remember that a DataDependency specifies the parent on which
     * a dependency is based, so this method sets the value of the parent in
     * a dependency relationship.
     * @param dataDependency The dependency requirement for which to set the
     * dependency value on this record.
     * @param conceptId The value to set.
     * @return The record value to which this method applies its changes.
     */
    public RecordValue setDependencyValue(DataDependency dataDependency, String conceptId)
    {
        DataRecord requiredDocument;

        requiredDocument = null;
        if (dataDependency.isDrInstanceSpecified())
        {
            Set<DataRecord> requiredDocuments;

            // Find all documents matching the Data Requirement Instance ID specified by the dependency requirement.
            requiredDocuments = findDataRecordsByDrInstance(T8HierarchicalSetType.LINE, dataDependency.getDrInstanceId());
            if (requiredDocuments.size() > 1) throw new RuntimeException("DR Instance dependency refers to multiple ancestor records: " + dataDependency.getDrInstanceId());
            else if (requiredDocuments.size() > 0) requiredDocument = requiredDocuments.iterator().next();
        }
        else if (dataDependency.isDrSpecified())
        {
            Set<DataRecord> requiredDocuments;

            // Find all documents matching the Data Requirement ID specified by the dependency requirement.
            requiredDocuments = findDataRecordsByDr(T8HierarchicalSetType.LINE, dataDependency.getDrId());
            if (requiredDocuments.size() > 1) throw new RuntimeException("DR dependency refers to multiple ancestor records: " + dataDependency.getDrId());
            else if (requiredDocuments.size() > 0) requiredDocument = requiredDocuments.iterator().next();
        }
        else // Property dependency.
        {
            // The dependency is on this record.
            requiredDocument = this;
        }

        // Now use the required documents and find the required property values in each.
        if (requiredDocument != null)
        {
            return requiredDocument.setValue(dataDependency.getPropertyId(), dataDependency.getFieldId(), conceptId, false);
        }
        else throw new IllegalArgumentException("No record matching data dependency could be found: " + dataDependency);
    }

    public List<DependencyValue> getDependentValues(RecordValue parentValue, boolean includeDescendentRecords)
    {
        List<DependencyValue> dependentValues;

        dependentValues = new ArrayList<>();

        // Add all dependent values from this record.
        for (RecordValue value : getAllValues())
        {
            if (value instanceof DependencyValue)
            {
                DependencyValue dependentValue;

                dependentValue = (DependencyValue)value;
                if (dependentValue.isDependentOn(parentValue))
                {
                    dependentValues.add(dependentValue);
                }
            }
        }

        // Add all dependent values from the descendant records if required.
        if (includeDescendentRecords)
        {
            for (DataRecord subRecord : subRecords)
            {
                dependentValues.addAll(subRecord.getDependentValues(parentValue, includeDescendentRecords));
            }
        }

        // Return the complete list.
        return dependentValues;
    }

    /**
     * Generates the a hash code based on the content of this record i.e. the
     * values and FFT containing in the record.
     * @param includeDescendantRecords
     * @param includeFFT
     * @return
     */
    public int getContentHashCode(boolean includeDescendantRecords, boolean includeFFT)
    {
        int hash;

        hash = 5;
        hash = 53 * hash + java.util.Objects.hashCode(this.fft);
        for (RecordSection recordSection : recordSections)
        {
            hash = 53 * hash + recordSection.getContentHashCode(includeFFT);
        }

        if (includeDescendantRecords)
        {
            for (DataRecord subRecord : subRecords)
            {
                hash = 53 * hash + subRecord.getContentHashCode(includeDescendantRecords, includeFFT);
            }
        }

        return hash;
    }

    public DataRecord copy(boolean copyOntology, boolean copyTerminology, boolean copyAttributes, boolean copyDescriptions, boolean copyAttachmentDetails, boolean copyDescendants)
    {
        return this.copy(copyOntology, copyTerminology, copyAttributes, copyDescriptions, copyAttachmentDetails, copyDescendants, copyDescendants);
    }

    public DataRecord copy(boolean copyOntology, boolean copyTerminology, boolean copyAttributes, boolean copyDescriptions, boolean copyAttachmentDetails, boolean copyDescendants, boolean copyIndependentDescendants)
    {
        DataRequirementInstance newDrInstance;
        DataRequirement newDr;
        DataRecord copy;

        // Copy the Dr Instance on which the record is based.
        newDrInstance = drInstance.copy();
        newDr = newDrInstance.getDataRequirement();

        // Create a copy of this record.
        copy = new DataRecord(newDrInstance);
        copy.setID(id);
        copy.setFFT(fft);
        copy.setInsertedAt(insertedAt);
        copy.setInsertedByID(insertedById);
        copy.setInsertedByIid(insertedByIid);
        copy.setUpdatedAt(updatedAt);
        copy.setUpdatedByID(updatedById);
        copy.setUpdatedByIid(updatedByIid);

        // Set the access layer on the copy to the same settings as this instance.
        copy.setAccess(accessLayer, false);

        // Copy the ontology if required.
        if ((copyOntology) && (ontology != null)) copy.setOntology(ontology.copy(true, true, true, true, true));

        // Copy the terminology if required.
        if (copyTerminology)
        {
            copy.setCode(code);
            copy.setTerm(term);
            copy.setAbbreviation(abbreviation);
            copy.setDefinition(definition);
        }

        // Copy the tags if required.
        if (copyAttributes)
        {
            copy.setAttributes(attributes);
        }

        // Copy descriptions if required.
        if (copyDescriptions)
        {
            for (T8DataString description : dataStrings)
            {
                copy.addDataString(description.copy());
            }
        }

        // Copy attachment details if required.
        if (!copyAttachmentDetails)
        {
            copy.clearAttachmentDetails();
        }

        // Copy the record content.
        for (RecordSection section : recordSections)
        {
            SectionRequirement newSectionRequirement;

            newSectionRequirement = newDr.getSectionRequirement(section.getId());
            copy.setRecordSection(section.copy(newSectionRequirement));
        }

        // Copy data string particle settings.
        for (T8DataStringParticleSettings settings : dataStringParticleSettings)
        {
            copy.addDataStringParticleSettings(settings.copy());
        }

        // Copy descendants if required.
        if (copyDescendants)
        {
            for (DataRecord subRecord : subRecords)
            {
                boolean dependent;

                // Check for dependent reference to the sub-record.
                if (!subRecord.isIndependentRecord())
                {
                    dependent = false;
                    for (DocumentReferenceList reference : getReferences(subRecord.getID()))
                    {
                        if (!reference.isIndependent())
                        {
                            dependent = true;
                            break;
                        }
                    }
                }
                else dependent = false;

                // Only copy the descendant if it is not independent or if we are flagged to copy independents as well.
                if ((dependent) || (copyIndependentDescendants))
                {
                    copy.addSubRecord(subRecord.copy(copyOntology, copyTerminology, copyAttributes, copyDescriptions, copyAttachmentDetails, copyDescendants, copyIndependentDescendants));
                }
            }
        }

        // Return the copied record.
        return copy;
    }

    @Override
    public DataRecord copy()
    {
        return copy(true, true, true, true, true, true, true);
    }

    @Override
    public DataRecord saveState()
    {
        return stateStore.putState(null, copy());
    }

    @Override
    public DataRecord saveState(String stateId)
    {
        return stateStore.putState(stateId, copy());
    }

    @Override
    public DataRecord getSavedState()
    {
        return stateStore.getState(null);
    }

    @Override
    public DataRecord getSavedState(String stateId)
    {
        return stateStore.getState(stateId);
    }

    @Override
    public void clearSavedStates()
    {
        stateStore.clear();
    }
}
