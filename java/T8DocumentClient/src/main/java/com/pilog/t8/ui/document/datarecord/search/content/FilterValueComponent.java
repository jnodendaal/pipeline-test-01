package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface FilterValueComponent
{
    public void clearFilterValue();
    public void setFilterValue(Object value);
    public Object getFilterValue();
    public boolean isRangeApplicable();
    public List<DataFilterOperator> getApplicableOperators();
    public void setOperator(DataFilterOperator operator);
}
