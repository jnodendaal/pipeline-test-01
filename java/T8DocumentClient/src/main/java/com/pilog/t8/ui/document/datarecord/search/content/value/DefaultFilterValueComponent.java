package com.pilog.t8.ui.document.datarecord.search.content.value;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.ui.document.datarecord.search.content.FilterValueComponent;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import org.jdesktop.swingx.prompt.PromptSupport;

/**
 * @author Bouwer du Preez
 */
public class DefaultFilterValueComponent extends JTextArea implements FilterValueComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(DefaultFilterValueComponent.class);
    private final T8DataRecordSearch parentSearch;
    private final T8ComponentController controller;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8ConfigurationManager configurationManager;
    private DataFilterOperator operator;
    private boolean collectionOperator;

    public DefaultFilterValueComponent(T8DataRecordSearch parentSearch)
    {
        this.parentSearch = parentSearch;
        this.controller = parentSearch.getController();
        this.clientContext = controller.getClientContext();
        this.sessionContext = controller.getSessionContext();
        this.configurationManager = clientContext.getConfigurationManager();
        super.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(new Color(171, 173, 179), 1), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
        super.setFont(new Font("Tahoma", Font.PLAIN, 11));
        ((AbstractDocument)super.getDocument()).setDocumentFilter(new DocumentPasteFilter());
        setRows(0);
    }

    @Override
    public boolean isRangeApplicable()
    {
        return false;
    }

    @Override
    public List<T8DataFilterCriterion.DataFilterOperator> getApplicableOperators()
    {
        return Arrays.asList(T8DataFilterCriterion.DataFilterOperator.values());
    }

    @Override
    public void setOperator(DataFilterOperator inOperator)
    {
        this.operator = inOperator;
        this.collectionOperator = (operator == DataFilterOperator.IN || operator == DataFilterOperator.NOT_IN);
        if (collectionOperator)
        {
            PromptSupport.setPrompt("One value per line...", (JTextArea)this);
        }
        else
        {
            PromptSupport.setPrompt(null, (JTextArea)this);
        }
    }

    @Override
    public void clearFilterValue()
    {
        setText(null);
    }

    @Override
    public void setFilterValue(Object value)
    {
        if (collectionOperator)
        {
            if (value instanceof Collection)
            {
                Iterator listIterator;
                StringBuilder newText;
                int count;

                count = 0;
                newText = new StringBuilder();
                listIterator = ((Collection)value).iterator();
                while (listIterator.hasNext())
                {
                    Object nextValue;

                    nextValue = listIterator.next();
                    if (nextValue != null)
                    {
                        String nextStringValue;

                        nextStringValue = nextValue.toString().trim();
                        if (nextStringValue.length() > 0)
                        {
                            if (count > 0) newText.append("\n");
                            newText.append(nextStringValue);
                            count++;
                        }
                    }
                }

                setText(newText.toString());
            }
            else
            {
                setText(value != null ? value.toString() : null);
            }
        }
        else
        {
            if (value instanceof Collection)
            {
                Iterator listIterator;

                listIterator = ((Collection)value).iterator();
                if (listIterator.hasNext())
                {
                    Object nextValue;

                    nextValue = listIterator.next();
                    if (nextValue != null)
                    {
                        String nextStringValue;

                        nextStringValue = nextValue.toString().trim();
                        if (nextStringValue.length() > 0)
                        {
                            setText(nextStringValue);
                        }
                        else setText(null);
                    }
                    else setText(null);
                }
                else setText(null);
            }
            else
            {
                setText(value != null ? value.toString() : null);
            }
        }
    }

    @Override
    public Object getFilterValue()
    {
        String textAreaValue;

        textAreaValue = getText();
        if (collectionOperator)
        {
            if (!Strings.isNullOrEmpty(textAreaValue))
            {
                ArrayList<String> searchValues;

                searchValues = new ArrayList<>();
                for (String value : textAreaValue.split("\n"))
                {
                    String searchValue;

                    searchValue = value.trim();
                    if (searchValue.length() > 0)
                    {
                        searchValues.add(searchValue);
                    }
                }

                return searchValues;
            }
            else return new ArrayList<>();
        }
        else
        {
            return textAreaValue != null ? textAreaValue.trim() : null;
        }
    }

    private class DocumentPasteFilter extends DocumentFilter
    {
        @Override
        public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException
        {
            // Only if a value was pasted, otherwise add it as is
            if (text != null && text.length() > 1)
            {
                StringBuilder textBuilder;
                String[] values;

                try
                {
                    if (!getText().isEmpty())
                    {
                        // If there is text and it doesn't end on a newline, add one
                        if (getText().endsWith("\n"))
                        {
                            textBuilder = new StringBuilder();
                        } else textBuilder = new StringBuilder("\n");
                    } else textBuilder = new StringBuilder();

                    // Split the pasted value on commas and change it to each item on a newline
                    values = text.split(",", 0);
                    for (String value : values)
                    {
                        if (!value.trim().isEmpty()) textBuilder.append(value.trim()).append("\n");
                    }

                    // Now add the new string to the document
                    super.replace(fb, offset, length, textBuilder.toString(), attrs);
                }
                catch (Exception ex)
                {
                    LOGGER.log("Couldn't properly add the pasted value", ex);
                }
            } else super.replace(fb, offset, length, text, attrs);
        }
    }
}
