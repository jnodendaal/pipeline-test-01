package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.data.document.datarecord.filter.T8DrFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.document.datarecord.filter.T8DrInstanceFilterCriteria;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class SubCriteriaPanel extends JPanel implements FilterClauseComponent
{
    private final T8DataRecordSearch parentSearch;
    private final SubCriteriaPanel parentPanel;
    private final Map<FilterClauseComponent, ConjunctionPanel> criteriaComponents;
    private final boolean allowCriteriaAddition;
    private final boolean allowSubCriteriaAddition;

    public SubCriteriaPanel(T8DataRecordSearch parentSearch, SubCriteriaPanel parentPanel)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.criteriaComponents = new LinkedHashMap<>();
        this.allowCriteriaAddition = true;
        this.allowSubCriteriaAddition = true;
        initComponents();
        rebuildLayout();
    }

    public SubCriteriaPanel(T8DataRecordSearch parentSearch, SubCriteriaPanel parentPanel, T8RecordFilterCriteria filter)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.criteriaComponents = new LinkedHashMap<>();
        this.allowCriteriaAddition = filter.isAllowCriteriaAddition();
        this.allowSubCriteriaAddition = filter.isAllowSubCriteriaAddition();
        initComponents();
        setFilterClauses(filter.getFilterClauses());
    }

    private void setFilterClauses(LinkedHashMap<T8DataFilterClause, DataFilterConjunction> clauses)
    {
        for (T8DataFilterClause clause : clauses.keySet())
        {
            if (clause instanceof T8RecordFilterCriteria)
            {
                SubCriteriaPanel clausePanel;
                ConjunctionPanel conjunctionPanel;

                clausePanel = new SubCriteriaPanel(parentSearch, this, ((T8RecordFilterCriteria)clause));
                conjunctionPanel = new ConjunctionPanel(this, clauses.get(clause));
                criteriaComponents.put(clausePanel, conjunctionPanel);
            }
            else if (clause instanceof T8DrFilterCriteria)
            {
                DrCriteriaPanel clausePanel;
                ConjunctionPanel conjunctionPanel;

                clausePanel = new DrCriteriaPanel(parentSearch, this, ((T8DrFilterCriteria)clause));
                conjunctionPanel = new ConjunctionPanel(this, clauses.get(clause));
                criteriaComponents.put(clausePanel, conjunctionPanel);
            }
            else
            {
                DrInstanceCriteriaPanel clausePanel;
                ConjunctionPanel conjunctionPanel;

                clausePanel = new DrInstanceCriteriaPanel(parentSearch, this, ((T8DrInstanceFilterCriteria)clause));
                conjunctionPanel = new ConjunctionPanel(this, clauses.get(clause));
                criteriaComponents.put(clausePanel, conjunctionPanel);
            }
        }

        rebuildLayout();
    }

    public void setAdvancedOptionsVisible(boolean setVisible)
    {
        for (Component component : getComponents())
        {
            if(component instanceof ConjunctionPanel)
            {
                component.setVisible(setVisible);
            }
            else if(component instanceof SubCriteriaControlPanel)
            {
                component.setVisible(setVisible);
            }
            else if(component instanceof DrInstanceCriteriaPanel)
            {
                ((DrInstanceCriteriaPanel) component).setAdvancedOptionsVisible(setVisible);
            }
            else if(component instanceof DrCriteriaPanel)
            {
                ((DrCriteriaPanel) component).setAdvancedOptionsVisible(setVisible);
            }
        }
    }

    public SubCriteriaPanel getParentPanel()
    {
        return parentPanel;
    }

    public LayoutColumnWidths recalculateLayoutColumnWidths()
    {
        LayoutColumnWidths maxWidths;

        maxWidths = new LayoutColumnWidths();
        for (FilterClauseComponent clauseComponent : criteriaComponents.keySet())
        {
            if (clauseComponent instanceof DrInstanceCriteriaPanel)
            {
                maxWidths.updateToMax(((DrInstanceCriteriaPanel)clauseComponent).getLayoutColumnWidths());
            }
            else if (clauseComponent instanceof DrCriteriaPanel)
            {
                maxWidths.updateToMax(((DrCriteriaPanel)clauseComponent).getLayoutColumnWidths());
            }
        }

        return maxWidths;
    }

    @Override
    public void doLayout()
    {
        LayoutColumnWidths widths;

        widths = recalculateLayoutColumnWidths();
        for (FilterClauseComponent clauseComponent : criteriaComponents.keySet())
        {
            if (clauseComponent instanceof DrInstanceCriteriaPanel)
            {
                ((DrInstanceCriteriaPanel)clauseComponent).setLinkWidths(widths);
            }
            else if (clauseComponent instanceof DrCriteriaPanel)
            {
                ((DrCriteriaPanel)clauseComponent).setLinkWidths(widths);
            }
        }

        super.doLayout();
    }

    private void rebuildLayout()
    {
        ConjunctionPanel conjunctionPanel;
        SubCriteriaControlPanel optionPanel;
        GridBagConstraints constraints;
        int row;

        row = 0;
        removeAll();
        for (FilterClauseComponent clauseComponent : criteriaComponents.keySet())
        {
            // Add the conjunction panel.
            conjunctionPanel = criteriaComponents.get(clauseComponent);
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row;
            constraints.weightx = 0;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(conjunctionPanel, constraints);
            conjunctionPanel.setVisible(row > 0);

            if (!parentSearch.isAdvancedOptionsEnabled())
            {
                conjunctionPanel.setVisible(false);
            }

            // Add the clause component.
            constraints = new GridBagConstraints();
            constraints.gridx = 1;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.BOTH;
            constraints.anchor = GridBagConstraints.NORTHEAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add((Component)clauseComponent, constraints);
        }

        // Add one last conjunction panel (so the user can add a new clause).
        optionPanel = new SubCriteriaControlPanel(parentSearch, this, allowCriteriaAddition, allowSubCriteriaAddition);
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = row;
        constraints.gridwidth = 2;
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(5, 5, 5, 5);
        add(optionPanel, constraints);

        if (!parentSearch.isAdvancedOptionsEnabled())
        {
            optionPanel.setVisible(false);
        }

        revalidate();
        repaint();
    }

    public void removeClause(FilterClauseComponent clauseComponent)
    {
        criteriaComponents.remove(clauseComponent);
        rebuildLayout();
    }

    public void addFilter(String drInstanceID, String drInstanceTerm)
    {
        criteriaComponents.put(new DrInstanceCriteriaPanel(parentSearch, this, drInstanceID, drInstanceTerm), new ConjunctionPanel(this, DataFilterConjunction.AND));
        rebuildLayout();
    }

    public void removeSubCriteria()
    {
        if (parentPanel != null)
        {
            parentPanel.removeClause(this);
        }
    }

    public void addSubCriteria()
    {
        criteriaComponents.put(new SubCriteriaPanel(parentSearch, this), new ConjunctionPanel(this, DataFilterConjunction.AND));
        rebuildLayout();
    }

    @Override
    public void clearFilterValues()
    {
        for (FilterClauseComponent clauseComponent : criteriaComponents.keySet())
        {
            clauseComponent.clearFilterValues();
        }
    }

    @Override
    public T8RecordFilterCriteria getFilterClause()
    {
        T8RecordFilterCriteria filterCriteria;

        filterCriteria = new T8RecordFilterCriteria("test");
        for (FilterClauseComponent clauseComponent : criteriaComponents.keySet())
        {
            ConjunctionPanel conjunctionComponent;

            conjunctionComponent = criteriaComponents.get(clauseComponent);
            filterCriteria.addFilterClause(conjunctionComponent.getConjunction(), clauseComponent.getFilterClause());
        }

        return filterCriteria;
    }

    @Override
    public void removeHistory(HistoryFilterPanel historyFilterPanel)
    {
    }

    @Override
    public void removeCode(CodeCriterionPanel codeCriterionPanel)
    {
    }

    @Override
    public void removeProperty(PropertyCriterionPanel propertyFilterPanel)
    {
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
