package com.pilog.t8.data.org;

import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ClientTerminologyProvider implements TerminologyProvider
{
    private final T8TerminologyClientApi trmApi;
    private String defaultLanguageId;

    public T8ClientTerminologyProvider(T8TerminologyClientApi trmApi)
    {
        this.trmApi = trmApi;
    }

    @Override
    public void setLanguage(String languageID)
    {
        defaultLanguageId = languageID;
    }

    @Override
    public String getCode(String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(null, conceptID);
        return terminology != null ? terminology.getCode() : null;
    }

    @Override
    public String getTerm(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getTerm() : null;
    }

    @Override
    public String getAbbreviation(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getAbbreviation() : null;
    }

    @Override
    public String getDefinition(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getDefinition() : null;
    }

    @Override
    public T8ConceptTerminology getTerminology(String languageId, String conceptId)
    {
        try
        {
            List<T8ConceptTerminology> terminologyList;

            terminologyList = trmApi.retrieveTerminology(ArrayLists.typeSafeList(languageId), ArrayLists.typeSafeList(conceptId));
            return terminologyList.size() > 0 ? terminologyList.get(0) : null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving terminology for concept: " + conceptId, e);
        }
    }

    @Override
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIds, Collection<String> conceptIds)
    {
        try
        {
            return trmApi.retrieveTerminology(new ArrayList<String>(languageIds), new ArrayList<String>(conceptIds));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving terminology for concepts: " + conceptIds, e);
        }
    }
}

