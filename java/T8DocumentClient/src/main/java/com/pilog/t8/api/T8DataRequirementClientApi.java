package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import java.util.Collection;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRequirementApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_DATA_REQUIREMENT_CLIENT";

    public T8DataRequirementClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8DataRequirementClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    public DataRequirement retrieveDataRequirement(String drId, String languageId, boolean includeOntology, boolean includeTerminology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DR_ID, drId);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY, includeOntology);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        return (DataRequirement)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_RETRIEVE_DR, inputParameters).get(PARAMETER_DR);
    }

    public DataRequirementInstance retrieveDataRequirementInstance(String drInstanceId, String languageId, boolean includeOntology, boolean includeTerminology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DR_IID, drInstanceId);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY, includeOntology);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        return (DataRequirementInstance)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_RETRIEVE_DRI, inputParameters).get(PARAMETER_DRI);
    }

    public List<T8DataRequirementComment> retrieveDataRequirementComments(Collection<String> orgIds, Collection<String> languageIds, Collection<String> drIds, Collection<String> drInstanceIds, Collection<String> commentTypeIds) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ORG_IDS, orgIds);
        inputParameters.put(PARAMETER_LANGUAGE_IDS, languageIds);
        inputParameters.put(PARAMETER_DR_IDS, drIds);
        inputParameters.put(PARAMETER_DR_IIDS, drInstanceIds);
        inputParameters.put(PARAMETER_COMMENT_TYPE_IDS, commentTypeIds);
        return (List<T8DataRequirementComment>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_RETRIEVE_DR_COMMENTS, inputParameters).get(PARAMETER_DR_COMMENTS);
    }

    public void insertDataRequirementInstance(T8OntologyConcept concept, String drId, boolean independent) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT, concept);
        inputParameters.put(PARAMETER_DR_ID, drId);
        inputParameters.put(PARAMETER_INDEPENDENT, independent);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_INSERT_DRI, inputParameters);
    }

    public void saveDataRequirementInstance(T8OntologyConcept concept, String drId, boolean independent) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT, concept);
        inputParameters.put(PARAMETER_DR_ID, drId);
        inputParameters.put(PARAMETER_INDEPENDENT, independent);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_SAVE_DRI, inputParameters);
    }

    public void saveDataRequirement(DataRequirement dr, boolean saveOntology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DR, dr);
        inputParameters.put(PARAMETER_SAVE_ONTOLOGY, saveOntology);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_SAVE_DR, inputParameters);
    }

    public void deleteDataRequirementInstance(String drInstanceId, boolean deleteOntology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DR_IID, drInstanceId);
        inputParameters.put(PARAMETER_DELETE_ONTOLOGY, deleteOntology);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_DELETE_DRI, inputParameters);
    }

    public void deleteDataRequirement(String drId, boolean deleteOntology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DR_ID, drId);
        inputParameters.put(PARAMETER_DELETE_ONTOLOGY, deleteOntology);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DRQ_DELETE_DR, inputParameters);
    }
}
