package com.pilog.t8.ui.document.datarecord.search;

import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.filter.T8PropertyFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterCriteria;
import com.pilog.t8.data.document.datarecord.filter.T8ValueFilterCriterion;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.org.T8ClientTerminologyProvider;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.data.document.datarecord.filter.T8RecordFilterCriteriaDefinition;
import com.pilog.t8.definition.ui.document.datarecord.search.T8DataRecordSearchDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.ui.document.datarecord.search.content.SubCriteriaPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordSearch extends JXPanel implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordSearch.class);

    private final T8Context context;
    private final T8ComponentController controller;
    private final T8DataRecordSearchDefinition definition;
    private final T8DataRecordSearchOperationHandler operationHandler;
    private final T8TerminologyClientApi trmApi;
    private final T8ComponentController internalController;
    private final T8DefinitionManager definitionManager;;
    private CachedTerminologyProvider terminologyProvider;
    private SubCriteriaPanel criteriaPanel;

    public T8DataRecordSearch(T8DataRecordSearchDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.definitionManager = controller.getClientContext().getDefinitionManager();
        this.operationHandler = new T8DataRecordSearchOperationHandler(this);
        this.trmApi = (T8TerminologyClientApi)controller.getApi(T8TerminologyClientApi.API_IDENTIFIER);
        this.internalController = new T8DefaultComponentController(controller, new T8ProjectContext(context, definition.getRootProjectId()), definition.getIdentifier(), false);
        this.setOpaque(definition.isOpaque());
        initComponents();
        rebuildLayout();
    }

    private void rebuildLayout()
    {
        this.criteriaPanel = new SubCriteriaPanel(this, null);
        add(criteriaPanel, BorderLayout.CENTER);
        revalidate();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8DataRecordSearchDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        // Initialize the terminology provider.
        terminologyProvider = new T8LRUCachedTerminologyProvider(new T8ClientTerminologyProvider(trmApi), 200);
        terminologyProvider.setLanguage(context.getSessionContext().getContentLanguageIdentifier());
    }

    @Override
    public void startComponent()
    {
        T8RecordFilterCriteriaDefinition presetFilterDefinition;

        // Start the component controller.
        internalController.start();

        // Set the preset filter (if any).
        presetFilterDefinition = definition.getPresetFilterDefinition();
        if (presetFilterDefinition == null)
        {
            String presetFilterId;

            presetFilterId = definition.getPresetFilterId();

            if (presetFilterId != null)
            {
                try
                {
                    presetFilterDefinition = definitionManager.getRawDefinition(context, definition.getRootProjectId(), presetFilterId);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        }

        if (presetFilterDefinition != null)
        {
            T8RecordFilterCriteria filterCriteria;

            filterCriteria = presetFilterDefinition.getRecordFilterCriteriaInstance(context);
            if (filterCriteria != null)
            {
                setFilterCriteria(filterCriteria);
            }
        }
    }

    @Override
    public void stopComponent()
    {
        // Stop the component controller.
        internalController.stop();
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public List<Pair<String, String>> getSelectedDRInstance(String drId)
    {
        Map<String, Object> outputParameters;
        Map<String, Object> inputParameters;
        String dialogIdentifier;

        dialogIdentifier = definition.getDRInstanceLookupDialogIdentifier();
        inputParameters = new HashMap<>();
        inputParameters.put(dialogIdentifier + "$P_DR_ID", drId);

        outputParameters = controller.showDialog(dialogIdentifier, inputParameters, false);
        if (outputParameters != null)
        {
            List<Pair<String, String>> instancePairList;
            List<Map<String, Object>> returnMapList;

            instancePairList = new ArrayList<>();

            // Get the return map from the dialog.
            returnMapList = (List<Map<String, Object>>)outputParameters.get(dialogIdentifier + "$P_DR_INSTANCE_LIST");

            for (Map<String, Object> returnMap : returnMapList)
            {
                String drInstanceId;
                String drInstanceTerm;

                // Get the nessacary values from the map.
                drInstanceId = (String)returnMap.get("DR_INSTANCE_ID");
                drInstanceTerm = (String)returnMap.get("DR_INSTANCE_TERM");

                instancePairList.add(new Pair(drInstanceId, drInstanceTerm));
            }
            return instancePairList;
        }
        else return null;
    }

    public boolean isAdvancedOptionsEnabled()
    {
        boolean optionsEnabled;

        optionsEnabled = definition.isAdvancedOptionsEnabled();

        return optionsEnabled;
    }

    public List<Pair<String, String>> getSelectedCodeType(String drInstanceID, String drId, List<String> excludedCodeTypeIdList, boolean allowMultipleSelection)
    {
        String dialogId;

        dialogId = definition.getCodeTypeLookupDialogIdentifier();
        if (dialogId != null)
        {
            Map<String, Object> outputParameters;
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<String, Object>();
            inputParameters.put(dialogId + "$P_ALLOW_MULTIPLE_SELECTION", allowMultipleSelection);
            inputParameters.put(dialogId + "$P_FILTER_DR_INSTANCE_ID", drInstanceID);
            inputParameters.put(dialogId + "$P_FILTER_DR_ID", drId);
            inputParameters.put(dialogId + "$P_FILTER_EXCLUDED_CODE_TYPE_ID_LIST", excludedCodeTypeIdList);
            outputParameters = controller.showDialog(dialogId, inputParameters, false);
            if (outputParameters != null)
            {
                List<Map<String, String>> selectedCodeTypeList;

                selectedCodeTypeList = (List<Map<String, String>>)outputParameters.get(dialogId + "$P_SELECTED_CODE_TYPES");
                if (selectedCodeTypeList != null)
                {
                    List<Pair<String, String>> selectedPairs;

                    selectedPairs = new ArrayList<Pair<String, String>>();
                    for (Map<String, String> selectedCodeType : selectedCodeTypeList)
                    {
                        if (selectedCodeType.size() == 2)
                        {
                            selectedPairs.add(new Pair<String, String>(selectedCodeType.get("ID"), selectedCodeType.get("TERM")));
                        }
                        else throw new RuntimeException("Invalid element found in selected code type list at index " + selectedCodeTypeList.indexOf(selectedCodeType) + ": " + selectedCodeType);
                    }

                    return selectedPairs;
                }
                else return null;
            }
            else return null;
        }
        else
        {
            LOGGER.log("No dialog specified for code type selection.");
            return null;
        }
    }

    public List<T8PropertyFilterCriterion> getSelectedProperty(String drInstanceID, String drId, List<String> excludedPropertyIdList, boolean allowMultipleSelection)
    {
        String dialogId;

        dialogId = definition.getPropertyLookupDialogIdentifier();
        if (dialogId != null)
        {
            Map<String, Object> outputParameters;
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<String, Object>();
            inputParameters.put(dialogId + "$P_ALLOW_MULTIPLE_SELECTION", allowMultipleSelection);
            inputParameters.put(dialogId + "$P_FILTER_DR_INSTANCE_ID", drInstanceID);
            inputParameters.put(dialogId + "$P_FILTER_DR_ID", drId);
            inputParameters.put(dialogId + "$P_FILTER_EXCLUDED_PROPERTY_ID_LIST", excludedPropertyIdList);
            outputParameters = controller.showDialog(dialogId, inputParameters, false);
            if (outputParameters != null)
            {
                List<Map<String, String>> selectedPropertyList;

                selectedPropertyList = (List<Map<String, String>>)outputParameters.get(dialogId + "$P_SELECTED_PROPERTIES");
                if (selectedPropertyList != null)
                {
                    List<T8PropertyFilterCriterion> criterionList;

                    criterionList = new ArrayList<>();
                    for (Map<String, String> selectedProperty : selectedPropertyList)
                    {
                        T8PropertyFilterCriterion propertyCriterion;
                        String propertyId;
                        String propertyTerm;
                        String propertyDefinition;
                        String propertyDataType;

                        propertyId = selectedProperty.get("ID");
                        propertyTerm = selectedProperty.get("TERM");
                        propertyDefinition = selectedProperty.get("DEFINITION");
                        propertyDataType = selectedProperty.get("DATA_TYPE");

                        propertyCriterion = new T8PropertyFilterCriterion(propertyId, null);
                        propertyCriterion.addValueCriterion(T8ValueFilterCriterion.createCriterion(RequirementType.getRequirementType(propertyDataType), DataFilterOperator.EQUAL, null));
                        criterionList.add(propertyCriterion);
                    }

                    return criterionList;
                }
                else return null;
            }
            else return null;
        }
        else
        {
            LOGGER.log("No dialog specified for property selection.");
            return null;
        }
    }

    public Map<String, Object> getSelectedControlledConcept(String drInstanceId, String drId, String propertyId)
    {
        String dialogId;
        List<String> rootDrInstanceList;

        dialogId = definition.getConceptLookupDialogIdentifier();
        rootDrInstanceList = definition.getDataFileStructureIDList();

        if (dialogId != null)
        {
            Map<String, Object> outputParameters;
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<String, Object>();
            inputParameters.put(dialogId + "$P_PARENT_FILTER_CRITERIA", getFilterCriteria());
            inputParameters.put(dialogId + "$P_DR_INSTANCE_ID", drInstanceId);
            inputParameters.put(dialogId + "$P_DR_ID", drId);
            inputParameters.put(dialogId + "$P_PROPERTY_ID", propertyId);
            inputParameters.put(dialogId + "$P_ROOT_DR_INSTANCE_LIST", rootDrInstanceList);
            outputParameters = controller.showDialog(dialogId, inputParameters, false);
            if (outputParameters != null)
            {
                Map<String, Object> returnMap;

                // Get the return map from the dialog.
                returnMap = (Map<String, Object>)outputParameters.get(dialogId + "$P_SELECTED_CONCEPT");

                return returnMap;
            }
            else return null;
        }
        else
        {
            LOGGER.log("No dialog specified for concept selection.");
            return null;
        }
    }

    public List<Map<String, Object>> getSelectedOrganization(List<String> organizationTypeIdList)
    {
        String dialogId;

        dialogId = definition.getOrganizationLookupDialogIdentifier();

        if (dialogId != null)
        {
            Map<String, Object> outputParameters;
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<String, Object>();
            inputParameters.put(dialogId + "$P_ORG_TYPE_ID_LIST", organizationTypeIdList);

            outputParameters = controller.showDialog(dialogId, inputParameters, false);
            if (outputParameters != null)
            {
                List<Map<String, Object>> returnMapList;

                // Get the return map from the dialog.
                returnMapList = (List<Map<String, Object>>)outputParameters.get(dialogId + "$P_ORG_ID_LIST");

                return returnMapList;
            }
            else return null;
        }
        else
        {
            LOGGER.log("No dialog specified for organization selection.");
            return null;
        }
    }

    public Pair<String, String> getSelectedUser()
    {
        Map<String, Object> outputParameters;
        String dialogIdentifier;

        dialogIdentifier = definition.getUserLookupDialogIdentifier();
        outputParameters = controller.showDialog(dialogIdentifier, null, false);
        if (outputParameters != null)
        {
            String userID;
            String displayString;

            userID = (String)outputParameters.get(dialogIdentifier + "$P_USER_ID");
            displayString = (String)outputParameters.get(dialogIdentifier + "$P_USER_DISPLAY_STRING");
            return new Pair(userID, displayString);
        }
        else return null;
    }

    public CachedTerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    public String getAgentDisplayString(String agentId)
    {
        try
        {
            T8DefinitionMetaData metaData;

            metaData = definitionManager.getDefinitionMetaData(null, agentId);
            return metaData != null ? metaData.getDisplayName() : agentId;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while fetching meta data using Agent ID: " + agentId, e);
            return agentId;
        }
    }

    public void setFilterCriteria(T8RecordFilterCriteria filterCriteria)
    {
        remove(criteriaPanel);
        this.criteriaPanel = new SubCriteriaPanel(this, null, filterCriteria);
        add(criteriaPanel, BorderLayout.CENTER);
        revalidate();
    }

    public void setAdvancedOptionsVisible(boolean setVisible)
    {
        for (Component component : getComponents())
        {
            if(component instanceof SubCriteriaPanel)
            {
                ((SubCriteriaPanel) component).setAdvancedOptionsVisible(setVisible);
            }
        }
    }

    public T8RecordFilterCriteria getFilterCriteria()
    {
        T8RecordFilterCriteria filterCriteria;

        filterCriteria = criteriaPanel.getFilterClause();
        if (filterCriteria != null) filterCriteria.setRecordFilterFieldIdentifier(definition.getFilterFieldIdentifier());
        return filterCriteria;
    }

    public void clearFilterValues()
    {
        criteriaPanel.clearFilterValues();
    }

    public void fireFilterCriteriaUpdated()
    {
        // TODO:  Fire a component event to signa the change of filter criteria.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
