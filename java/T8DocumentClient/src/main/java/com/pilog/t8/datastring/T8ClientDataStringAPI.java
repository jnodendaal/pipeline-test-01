package com.pilog.t8.datastring;

import com.pilog.epic.annotation.EPICClass;
import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.document.datastring.T8DataStringInstance;
import com.pilog.t8.data.document.datastring.T8DataStringFormatLink;
import com.pilog.t8.data.document.datastring.T8DataStringType;
import com.pilog.t8.data.document.datastring.T8DataStringFormat;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import java.util.List;
import java.util.Map;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;

import static com.pilog.t8.definition.api.T8DataStringApiResource.*;

/**
 * @author Gavin Boshoff
 */
@EPICClass(PreferredVariableName = "dataStringAPI", Description = "Data String API", VariableCreationTemplate = "dataStringAPI", onlyShowDeclaredMethods = true)
public class T8ClientDataStringAPI implements T8Api
{
    public static final String API_IDENTIFIER = "@API_CLIENT_DATA_STRING";

    private final T8Context accessContext;

    public T8ClientDataStringAPI(T8ClientContext clientContext, T8Context accessContext)
    {
        this.accessContext = accessContext;
    }

    public T8ClientDataStringAPI(T8ComponentController controller, String projectId)
    {
        this(controller.getClientContext(), controller.getContext());
    }

    /**
     * This will render the description for a specific data record using the
     * specified language identifier. This operation will not persist the
     * rendering generated and as such is safe to use for testing renderings.
     *
     * @param languageID The {@code String} language ID to use for the rendering
     * @param dataRecordID The {@code String} data record ID for which the
     *      rendering should be performed
     *
     * @return The {@code String} rendered text
     *
     * @throws Exception If there is any error during the rendering for the
     *      specific language-record combination
     */
    public String renderDataString(String languageID, String dataRecordID) throws Exception
    {
        Map<String, Object> operationParameters;
        Map<String, Object> operationResult;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_LANGUAGE_ID, PARAMETER_DATA_RECORD_ID}, new Object[]{languageID, dataRecordID});
        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_RENDER, operationParameters);

        return (String)operationResult.get(PARAMETER_DATA_STRING);
    }

    /**
     * Generates all of the concept data required for the rendering type to be
     * available in the system. The rendering type is also added to the required
     * rendering tables and then becomes available for use to set up the actual
     * formatting and detail for the type.
     *
     * @param renderingType The {@code T8DataStringType} which contains all of
     *      the details for the new type
     *
     * @return The {@code String} rendering instance ID of the newly created
     *      rendering type
     *
     * @throws Exception If there is any error during the creation of the
     *      new rendering type
     */
    public String insertDataStringType(T8DataStringType renderingType) throws Exception
    {
        return (String)T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_INSERT_DATA_STRING_TYPE, HashMaps.createSingular(PARAMETER_DATA_STRING_TYPE, renderingType)).get(PARAMETER_DATA_STRING_INSTANCE_ID);
    }

    /**
     * Saves the details for a rendering instance. This method will handle both
     * creations and updates of rendering instance details. If the instance ID
     * is provided, the existing instance will be updated. If not, a new
     * instance will be created.
     *
     * @param dataStringInstance The {@code T8DataStringInstance} which contains
     *      the details of the instance to be created/modified
     * @param organizationID The organization ID to which the rendering instance
     *      should be linked
     * @param dataRequirementIID An optional {@code String} DR Instance ID. If
     *      specified, the data string instance is linked to it
     * @param instanceDisplayName The {@code String} display name for a new
     *      instance. If an existing instance is modified, the value is ignored
     *      and can be {@code null}
     *
     * @return The {@code String} rendering instance ID of the updated rendering
     *      instance, or the generated ID for the newly created rendering
     *      instance
     *
     * @throws Exception If there is any error during the rendering instance
     *      save process
     */
    public String saveDataStringInstance(T8DataStringInstance dataStringInstance, String organizationID, String dataRequirementIID, String instanceDisplayName) throws Exception
    {
        Map<String, Object> operationParameters;
        Map<String, Object> operationResult;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_DATA_STRING_INSTANCE, PARAMETER_ORGANIZATION_ID, PARAMETER_DR_IID, PARAMETER_DATA_STRING_INSTANCE_DISPLAY_NAME}, new Object[]{dataStringInstance, organizationID, dataRequirementIID, instanceDisplayName});
        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_SAVE_DATA_STRING_INSTANCE, operationParameters);

        return (String)operationResult.get(PARAMETER_DATA_STRING_INSTANCE_ID);
    }

    /**
     * Links a data string instance to a specific data requirement instance,
     * using the current session's root organization ID.
     *
     * @param dataStringInstanceID The {@code String} data string instance
     *      identifier to be linked
     * @param dataRequirementIID The {@code String} data requirement instance ID
     *      to be linked to
     *
     * @throws Exception If there is any error during the link creation between
     *      the instances
     */
    public void insertDataStringInstanceLink(String dataStringInstanceID, String dataRequirementIID) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_DATA_STRING_INSTANCE_ID, PARAMETER_DR_IID}, new Object[]{dataStringInstanceID, dataRequirementIID});
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_INSERT_DATA_STRING_INSTANCE_LINK, operationParameters);
    }

    /**
     * Removes the link between a data string instance and a specific data
     * requirement instance for the current session's root organization ID.
     *
     * @param dataStringInstanceID The {@code String} data string instance
     *      identifier from which the link should be broken
     * @param dataRequirementIID The {@code String} data requirement instance ID
     *      from which the link should be removed
     *
     * @throws Exception If there is any error during the removal of the link
     *      between the instances
     */
    public void deleteDataStringInstanceLink(String dataStringInstanceID, String dataRequirementIID) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_DATA_STRING_INSTANCE_ID, PARAMETER_DR_IID}, new Object[]{dataStringInstanceID, dataRequirementIID});
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_DELETE_DATA_STRING_INSTANCE_LINK, operationParameters);
    }

    /**
     * Deletes a data string instance, its associated links, and optionally, the
     * concept for the instance.
     *
     * @param dataStringInstanceID The {@code String} data string instance ID to
     *      be deleted
     * @param deleteConcept {@code true} to delete the associated concept.
     *      {@code false} otherwise
     *
     * @throws Exception If there is any error during the deletion of the
     *      specified data string instance and its associated data
     */
    public void deleteDataStringInstance(String dataStringInstanceID, boolean deleteConcept) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_DATA_STRING_INSTANCE_ID, PARAMETER_DELETE_CONCEPT}, new Object[]{dataStringInstanceID, deleteConcept});
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_DELETE_DATA_STRING_INSTANCE, operationParameters);
    }

    /**
     * Retrieves a {@code T8DataStringFormatLink} using the specified ID. The
     * associated format details are included, but the elements of the format
     * are not.
     *
     * @param linkID The {@code String} link ID for the format link to be
     *      retrieved
     *
     * @return The {@code T8DataStringFormatLink} for the specified ID.
     *      {@code null} if no such id exists
     *
     * @throws Exception If there is any error during the retrieval of the
     *      rendering format link
     */
    public T8DataStringFormatLink retrieveDataStringFormatLink(String linkID) throws Exception
    {
        Map<String, Object> operationResult;

        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT_LINK, HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT_LINK_ID, linkID));

        return (T8DataStringFormatLink)operationResult.get(PARAMETER_DATA_STRING_FORMAT_LINK);
    }

    /**
     * Saves the specified {@code T8DataStringFormatLink} to the data source. If
     * the format link contains a link ID, the existing link will be updated,
     * otherwise a new ID will be generated and a new link record created.
     *
     * @param formatLink The {@code T8DataStringFormatLink} to be
     *      persisted
     *
     * @return The {@code String} format link ID of the newly generated record,
     *      or the existing ID if it was updated
     *
     * @throws Exception If there is any error during the persistence of the
     *      format link
     */
    public String saveDataStringFormatLink(T8DataStringFormatLink formatLink) throws Exception
    {
        if (Strings.isEmpty(formatLink.getLinkId())) return T8ClientDataStringAPI.this.insertDataStringFormatLink(formatLink);
        else
        {
            updateDataStringFormatLink(formatLink);
            return formatLink.getLinkId();
        }
    }

    /**
     * Creates a new {@code T8DataStringFormatLink} record based on the supplied
     * details. This assumes that no link ID exists and will generate a new ID
     * for the record.
     *
     * @param formatLink The {@code T8DataStringFormatLink} to be created
     *
     * @return The {@code String} newly generated ID for the format link
     *
     * @throws Exception If there is any error during the generation and
     *      persistence of the data record
     * @throws IllegalArgumentException If the format link specified already
     *      contains an ID
     */
    public String insertDataStringFormatLink(T8DataStringFormatLink formatLink) throws Exception
    {
        Map<String, Object> operationResult;

        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_INSERT_DATA_STRING_FORMAT_LINK, HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT_LINK, formatLink));

        return (String)operationResult.get(PARAMETER_DATA_STRING_FORMAT_LINK_ID);
    }

    /**
     * Creates a new {@code T8DataStringFormatLink} and an associated
     * {@code T8DataStringFormat}. This is convenience method to avoid making
     * multiple server calls.
     *
     * @param formatLink The {@code T8DataStringFormatLink} to be created
     * @param format The {@code T8DataStringFormat} to be created
     *
     * @return The generated {@code String} rendering format link ID for the
     *      new record
     *
     * @throws Exception If there is any error during the persistence of the
     *      required data
     */
    public String insertDataStringFormatLink(T8DataStringFormatLink formatLink, T8DataStringFormat format) throws Exception
    {
        Map<String, Object> operationParameters;
        Map<String, Object> operationResult;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_DATA_STRING_FORMAT_LINK, PARAMETER_DATA_STRING_FORMAT}, new Object[]{formatLink, format});
        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_INSERT_DATA_STRING_FORMAT_LINK, operationParameters);

        return (String)operationResult.get(PARAMETER_DATA_STRING_FORMAT_LINK_ID);
    }

    /**
     * Updates the details of an existing {@code T8DataStringFormatLink} data
     * record if it exists.
     *
     * @param formatLink The {@code T8DataStringFormatLink} to be persisted
     *
     * @throws Exception If there is any error during the persistence of the
     *      rendering format link
     */
    public void updateDataStringFormatLink(T8DataStringFormatLink formatLink) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT_LINK, HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT_LINK, formatLink));
    }

    /**
     * Deletes a data string format link instance associated with the specified
     * data string format link ID.<br/>
     * <br/>
     * Optionally allows for the deletion of a data string format with the
     * format link ID as the entry point. In this instance, all elements
     * associated with the data string format will be deleted, including all
     * format links linked to the format, and finally, the format itself is
     * deleted.
     *
     * @param linkID The {@code String} ID of the data string format link to
     *      be deleted, or which is used to find the format to be deleted
     * @param deleteFormat {@code true} if the entire format and associated
     *      data should be deleted. {@code false} to delete the format link only
     *
     * @throws Exception If there is any error during the deletion of the format
     *      link and associated data, if any
     */
    public void deleteDataStringFormatLink(String linkID, boolean deleteFormat) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_DATA_STRING_FORMAT_LINK_ID, PARAMETER_DELETE_DATA_STRING_FORMAT}, new Object[]{linkID, deleteFormat});
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_DELETE_DATA_STRING_FORMAT_LINK, operationParameters);
    }

    /**
     * Retrieves a {@code T8DataStringFormat} using the specified ID. The
     * retrieval includes the associated rendering elements for the specific
     * format.
     *
     * @param renderingFormatID The {@code String} link ID for the format to be
     *      retrieved
     *
     * @return The {@code T8DataStringFormat} for the specified ID and its
     *      associated element data. {@code null} if no such id exists
     *
     * @throws Exception If there is any error during the retrieval of the
     *      rendering format
     */
    public T8DataStringFormat retrieveDataStringFormat(String renderingFormatID) throws Exception
    {
        Map<String, Object> operationResult;

        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_RETRIEVE_DATA_STRING_FORMAT, HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT_ID, renderingFormatID));

        return (T8DataStringFormat)operationResult.get(PARAMETER_DATA_STRING_FORMAT);
    }

    /**
     * Saves the specified {@code T8DataStringFormat} to the data source. If the
     * format contains a format ID, the existing format will be updated,
     * otherwise a new ID will be generated and a new format record created.
     *
     * @param format The {@code T8DataStringFormat} to be persisted
     *
     * @return The {@code String} format ID of the newly generated record, or
     *      the existing ID if it was updated
     *
     * @throws Exception If there is any error during the persistence of the
     *      rendering format
     */
    public String saveDataStringFormat(T8DataStringFormat format) throws Exception
    {
        if (Strings.isNullOrEmpty(format.getDataStringFormatID())) return insertDataStringFormat(format);
        else
        {
            updateDataStringFormat(format);
            return format.getDataStringFormatID();
        }
    }

    /**
     * Creates a new {@code T8DataStringFormat} record based on the supplied
     * details. This assumes that no format ID exists and will generate a new ID
     * for the record.
     *
     * @param format The {@code T8DataStringFormat} to be created
     *
     * @return The {@code String} newly generated ID for the format
     *
     * @throws Exception If there is any error during the generation and
     *      persistence of the data record
     * @throws IllegalArgumentException If the format specified already contains
     *      an ID
     */
    public String insertDataStringFormat(T8DataStringFormat format) throws Exception
    {
        Map<String, Object> operationResult;

        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_INSERT_DATA_STRING_FORMAT, HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT, format));

        return (String)operationResult.get(PARAMETER_DATA_STRING_FORMAT_ID);
    }

    /**
     * Updates the details of an existing {@code T8DataStringFormat} data record
     * if it exists.
     *
     * @param format The {@code T8DataStringFormat} to be persisted
     *
     * @throws Exception If there is any error during the persistence of the
     *      rendering format
     */
    public void updateDataStringFormat(T8DataStringFormat format) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_UPDATE_DATA_STRING_FORMAT, HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT, format));
    }

    /**
     * Deletes the data string format and all associated data string details for
     * the specified format ID. This will delete all format elements, the format
     * links, as well as the format itself.
     *
     * @param formatID The {@code String} ID of the data string format to be
     *      deleted
     *
     * @throws Exception If there is any error during the deletion of the data
     *      string format and its associated data
     */
    public void deleteDataStringFormat(String formatID) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_DELETE_DATA_STRING_FORMAT, HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT_ID, formatID));
    }

    /**
     * Deletes the data strings associated with the specified record ID list
     * and the data string instance ID list. Either the record ID list or the
     * data string instance ID list can be empty, but not both.<br/>
     * <br/>
     * The following scenarios are supported:<br/>
     * <ol>
     * <li>
     * If only record ID's are specified, all data strings associated with those
     * records will be deleted.</li>
     * <li>
     * If only data string instance ID's are specified, all the data string
     * associated with those data string instances will be deleted.<li/>
     * <li>
     * If both record ID's and instance ID's are specified, they are combined,
     * and only the records which have data strings for the specific data string
     * instances will be deleted.</li>
     * </ol>
     *
     * @param recordIDList The list of record ID's for which the data strings
     *      should be deleted
     * @param dataStringInstanceIDList The {@code List} of data string instance
     *      ID's for which the data strings should be deleted
     *
     * @return The total number of data strings which were deleted, using the
     *      filter values specified
     *
     * @throws IllegalArgumentException If both of the lists are {@code null} or
     *      empty
     * @throws Exception If there is any other error during the deletion of the
     *      required data strings
     */
    public int deleteDataStrings(List<String> recordIDList, List<String> dataStringInstanceIDList) throws Exception
    {
        Map<String, Object> operationParameters;
        Map<String, Object> operationResult;

        operationParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_RECORD_ID_LIST, PARAMETER_DATA_STRING_INSTANCE_ID_LIST}, new Object[]{recordIDList, dataStringInstanceIDList});
        operationResult = T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_DATA_STRING_DELETE_DATA_STRINGS, operationParameters);

        return (int)operationResult.getOrDefault(PARAMETER_DELETED_DATA_STRING_COUNT, 0);
    }
}