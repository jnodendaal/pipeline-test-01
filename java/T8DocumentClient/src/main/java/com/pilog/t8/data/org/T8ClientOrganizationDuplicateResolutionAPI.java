package com.pilog.t8.data.org;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.data.org.T8OrganizationDuplicateResolutionAPIResources.*;

/**
 * This class server as the API for all server-side code access to the duplicate
 * resolution process within the context of an organization.
 *
 * @author Bouwer du Preez
 */
public class T8ClientOrganizationDuplicateResolutionAPI implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context accessContext;

    public static final String API_IDENTIFIER = "@API_CLIENT_ORGANIZATION_DUPLICATE_RESOLUTION";

    public T8ClientOrganizationDuplicateResolutionAPI(T8ClientContext clientContext, T8Context accessContext)
    {
        this.clientContext = clientContext;
        this.accessContext = accessContext;
    }

    public T8ClientOrganizationDuplicateResolutionAPI(T8ComponentController controller)
    {
        this(controller.getClientContext(), controller.getContext());
    }

    public String submitResolution(String matchInstanceID, String familyID, String dataObjectIdentifier, String resolutionStateIdentifier) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_MATCH_INSTANCE_ID, matchInstanceID);
        inputParameters.put(PARAMETER_FAMILY_ID, familyID);
        inputParameters.put(PARAMETER_DATA_OBJECT_IDENTIFIER, dataObjectIdentifier);
        inputParameters.put(PARAMETER_RESOLUTION_STATE_IDENTIFIER, resolutionStateIdentifier);
        return (String)T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_ORG_DUP_RES_SUBMIT_RESOLUTION, inputParameters).get(PARAMETER_RESOLUTION_ID);
    }

    public void cancelResolution(String resolutionID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_RESOLUTION_ID, resolutionID);
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_ORG_DUP_RES_CANCEL_RESOLUTION, inputParameters);
    }

    public void finalizeResolution(String mergerIdentifier, String resolutionID, String dataObjectIdentifier, String masterStateIdentifier, String nonDuplicateStateIdentifier, String supersededStateIdentifier) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_DATA_RECORD_MERGER_IDENTIFIER, mergerIdentifier);
        inputParameters.put(PARAMETER_RESOLUTION_ID, resolutionID);
        inputParameters.put(PARAMETER_DATA_OBJECT_IDENTIFIER, dataObjectIdentifier);
        inputParameters.put(PARAMETER_MASTER_STATE_IDENTIFIER, masterStateIdentifier);
        inputParameters.put(PARAMETER_NON_DUPLICATE_STATE_IDENTIFIER, nonDuplicateStateIdentifier);
        inputParameters.put(PARAMETER_SUPERSEDED_STATE_IDENTIFIER, supersededStateIdentifier);
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_ORG_DUP_RES_FINALIZE_RESOLUTION, inputParameters);
    }

    public void setMaster(String familyID, String recordID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_FAMILY_ID, familyID);
        inputParameters.put(PARAMETER_RECORD_ID, recordID);
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_ORG_DUP_RES_SET_MASTER, inputParameters);
    }

    public void setExcluded(String familyID, String recordID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_FAMILY_ID, familyID);
        inputParameters.put(PARAMETER_RECORD_ID, recordID);
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_ORG_DUP_RES_SET_EXCLUDED, inputParameters);
    }

    public void setSuperseded(String familyID, String recordID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_FAMILY_ID, familyID);
        inputParameters.put(PARAMETER_RECORD_ID, recordID);
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_ORG_DUP_RES_SET_SUPERSEDED, inputParameters);
    }

    public void setPending(String familyID, String recordID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        inputParameters.put(PARAMETER_FAMILY_ID, familyID);
        inputParameters.put(PARAMETER_RECORD_ID, recordID);
        T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_ORG_DUP_RES_SET_PENDING, inputParameters);
    }
}
