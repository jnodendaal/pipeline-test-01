package com.pilog.t8.data.org.concept;

import com.pilog.epic.annotation.EPICClass;
import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.data.org.concept.T8ConceptMetaUsages;
import com.pilog.t8.definition.data.org.concept.T8ConceptUsages;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;

import static com.pilog.t8.definition.data.org.concept.T8ConceptDataAPIResources.*;

/**
 * @author Gavin Boshoff
 */
@EPICClass(PreferredVariableName = "cdAPI", Description = "Concept Data API", VariableCreationTemplate = "cdAPI", onlyShowDeclaredMethods = true)
public class T8ClientConceptDataAPI implements T8Api
{
    public static final String API_IDENTIFIER = "@API_CLIENT_CONCEPT_DATA";

    private final T8ClientContext clientContext;
    private final T8Context accessContext;

    public T8ClientConceptDataAPI(T8ClientContext clientContext, T8Context accessContext)
    {
        this.clientContext = clientContext;
        this.accessContext = accessContext;
    }

    public T8ClientConceptDataAPI(T8ComponentController controller, String projectId)
    {
        this(controller.getClientContext(), controller.getContext());
    }

    public T8ConceptUsages getConceptUsageCounts(String conceptID) throws Exception
    {
        return (T8ConceptUsages) T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_CD_GET_USAGE_COUNTS, HashMaps.createSingular(PARAMETER_CONCEPT_ID, conceptID)).get(PARAMETER_CONCEPT_USAGES);
    }

    /**
     * Retrieves the meta usage count and additional details for the specified
     * {@code String} concept ID. All definitions are searched for any
     * occurrence of the specified concept ID.
     *
     * @param conceptID The {@code String} concept ID for which the usages
     *      should be checked
     *
     * @return The {@code T8ConceptMetaUsages} for the specified concept ID
     *
     * @throws Exception If there is an error while executing the server call or
     *      an error occurs on the server side
     */
    public T8ConceptMetaUsages getConceptMetaUsageCounts(String conceptID) throws Exception
    {
        return (T8ConceptMetaUsages) T8MainServerClient.executeSynchronousOperation(accessContext, OPERATION_API_CD_GET_META_USAGE_COUNTS, HashMaps.createSingular(PARAMETER_CONCEPT_ID, conceptID)).get(PARAMETER_CONCEPT_META_USAGES);
    }
}