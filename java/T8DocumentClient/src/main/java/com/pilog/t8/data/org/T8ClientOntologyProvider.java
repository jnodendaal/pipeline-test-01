package com.pilog.t8.data.org;

import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ClientOntologyProvider implements OntologyProvider
{
    private final T8OntologyClientApi ontApi;

    public T8ClientOntologyProvider(T8OntologyClientApi ontApi)
    {
        this.ontApi = ontApi;
    }

    @Override
    public List<T8OntologyCode> getOntologyCodes(String conceptId)
    {
        try
        {
            return ontApi.retrieveConceptCodes(conceptId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving concept ontology codes: " + conceptId, e);
        }
    }

    @Override
    public List<T8OntologyLink> getOntologyLinks(String conceptId)
    {
        try
        {
            return ontApi.getOntologyLinks(conceptId);
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Exception while retrieving organization ontology links for concept: " + conceptId, ex);
        }
    }

    @Override
    public List<T8OntologyLink> getOntologyLinks(List<String> osIdList, String conceptId)
    {
        try
        {
            return ontApi.getOntologyLinks(osIdList,conceptId);
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Exception while retrieving organization ontology links for concept: " + conceptId, ex);
        }
    }

    @Override
    public T8OntologyConcept getOntologyConcept(String conceptId)
    {
        try
        {
            return ontApi.retrieveConcept(conceptId, true, true, true, true, true);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving concept ontology: " + conceptId, e);
        }
    }

    @Override
    public T8DataIterator<String> getConceptIDIterator(String ocId, boolean includeDescendantClasses)
    {
        throw new UnsupportedOperationException("Iterator not available on client side.");
    }
}
