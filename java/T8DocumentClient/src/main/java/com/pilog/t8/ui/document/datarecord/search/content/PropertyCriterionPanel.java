package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.document.datarecord.filter.T8PropertyFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8ValueFilterCriterion;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.ui.document.datarecord.search.content.value.BooleanFilterValueComponent;
import com.pilog.t8.ui.document.datarecord.search.content.value.ControlledConceptFilterValueComponent;
import com.pilog.t8.ui.document.datarecord.search.content.value.DateFilterValueComponent;
import com.pilog.t8.ui.document.datarecord.search.content.value.DefaultFilterValueComponent;
import com.pilog.t8.ui.document.datarecord.search.content.value.DocumentReferenceFilterValueComponent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXHyperlink;

/**
 * @author Bouwer du Preez
 */
public class PropertyCriterionPanel extends JPanel
{
    private final String resetOperatorId;
    private final T8DataRecordSearch parentSearch;
    private final FilterClauseComponent parentPanel;
    private FilterValueComponent lowerBoundValueComponent;
    private FilterValueComponent upperBoundValueComponent;
    private JXHyperlink propertyLookupLink;
    private String propertyId;
    private String fieldId;
    private String fieldTerm;
    private String propertyTerm;
    private RequirementType dataType;
    private final boolean allowRemoval;

    private static final String ANY_PROPERTY_TERM = "Any Property";
    private static final String RANGE_OPERATOR_ID = "RANGE";
    private static final String RANGE_OPERATOR_NAME = "Range";

    public PropertyCriterionPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, String propertyId, String propertyTerm, String fieldId, String fieldTerm, RequirementType dataType)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.propertyId = propertyId;
        this.propertyTerm = propertyTerm;
        this.fieldId = fieldId;
        this.fieldTerm = fieldTerm;
        this.dataType = dataType;
        this.resetOperatorId = DataFilterOperator.EQUAL.toString();
        this.allowRemoval = true;
        initComponents();
        constructPanel();
        constructValueFilterComponents(dataType);
    }

    public PropertyCriterionPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, T8PropertyFilterCriterion filter)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.propertyId = filter.getPropertyId();
        this.fieldId = filter.getFieldId();
        this.resetOperatorId = filter.getValueCriteria().size() > 0 ? filter.getValueCriteria().get(0).getOperator().toString() : DataFilterOperator.EQUAL.toString();
        this.allowRemoval = filter.isAllowRemoval();
        if (propertyId != null) this.propertyTerm = parentSearch.getTerminologyProvider().getTerm(null, this.propertyId);
        if (this.fieldId != null) this.fieldTerm = parentSearch.getTerminologyProvider().getTerm(null, this.fieldId);
        initComponents();
        constructPanel();
        constructValueFilterComponents(filter.getValueCriteria());
    }

    /**
     * Constructs and initializes this component.
     */
    private void constructPanel()
    {
        // Create the property lookup.
        propertyLookupLink = new JXHyperlink(new AbstractAction(constructDisplayString())
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
            }
        });
        jPanelPropertyLookup.add(propertyLookupLink, BorderLayout.CENTER);

        // If removal is disabled, hide the button.
        if (!allowRemoval) jButtonRemove.setVisible(false);

        // Set the operator renderer.
        this.jComboBoxOperator.setRenderer(new ComboCellRenderer());
    }

    private String constructDisplayString()
    {
        StringBuilder displayBuilder;

        displayBuilder = new StringBuilder();
        displayBuilder.append((this.propertyId != null ? this.propertyTerm : ANY_PROPERTY_TERM));
        if (this.propertyId != null && this.fieldId != null)
        {
            displayBuilder.append(":").append(this.fieldTerm);
        }

        return displayBuilder.toString();
    }

    private void constructValueFilterComponents(List<T8ValueFilterCriterion> valueCriteria)
    {
        if (valueCriteria.size() > 1)
        {
            T8ValueFilterCriterion lowerBoundCriterion;
            T8ValueFilterCriterion upperBoundCriterion;

            lowerBoundCriterion = valueCriteria.get(0);
            upperBoundCriterion = valueCriteria.get(1);
            constructValueFilterComponents(lowerBoundCriterion.getDataType());
            lowerBoundValueComponent.setFilterValue(lowerBoundCriterion.getFilterValue());
            upperBoundValueComponent.setFilterValue(upperBoundCriterion.getFilterValue());
            setFilterOperator(RANGE_OPERATOR_ID);
        }
        else if (valueCriteria.size() > 0)
        {
            T8ValueFilterCriterion valueCriterion;

            valueCriterion = valueCriteria.get(0);
            constructValueFilterComponents(valueCriterion.getDataType());
            lowerBoundValueComponent.setFilterValue(valueCriterion.getFilterValue());
            setFilterOperator(valueCriterion.getOperator().toString());
        }
        else
        {
            constructValueFilterComponents((RequirementType)null);
        }
    }

    /**
     * Sets the new filter data type on this component.
     * @param newDataType The new filter data type.
     */
    private void constructValueFilterComponents(RequirementType newDataType)
    {
        // Set the new data type.
        this.dataType = newDataType;

        // First we remove the previous child components from the panel.
        jPanelLowerBound.removeAll();
        lowerBoundValueComponent = null;
        upperBoundValueComponent = null;

        // Remove existing items.
        jComboBoxOperator.removeAllItems();

        // Construct value filter component depending on the type specified
        if (newDataType == null)
        {
            lowerBoundValueComponent = new DefaultFilterValueComponent(parentSearch);
            jPanelLowerBound.add((Component)lowerBoundValueComponent, BorderLayout.CENTER);
        }
        else
        {
            // Add the items available in the combobox.
            switch (newDataType)
            {
                case BOOLEAN_TYPE:
                    lowerBoundValueComponent = new BooleanFilterValueComponent(parentSearch);
                    jPanelLowerBound.add((Component)lowerBoundValueComponent, BorderLayout.CENTER);
                    break;
                case CONTROLLED_CONCEPT:
                    lowerBoundValueComponent = new ControlledConceptFilterValueComponent(parentSearch, parentPanel, propertyId);
                    jPanelLowerBound.add((Component)lowerBoundValueComponent, BorderLayout.CENTER);
                    break;
                case DATE_TIME_TYPE:
                    lowerBoundValueComponent = new DateFilterValueComponent(parentSearch, parentPanel, propertyId);
                    upperBoundValueComponent = new DateFilterValueComponent(parentSearch, parentPanel, propertyId);
                    jPanelLowerBound.add((Component)lowerBoundValueComponent, BorderLayout.CENTER);
                    jPanelUpperBound.add((Component)upperBoundValueComponent, BorderLayout.CENTER);
                    break;
                case DOCUMENT_REFERENCE:
                    lowerBoundValueComponent = new DocumentReferenceFilterValueComponent(parentSearch, parentPanel, propertyId);
                    jPanelLowerBound.add((Component)lowerBoundValueComponent, BorderLayout.CENTER);
                    break;
                default:
                    lowerBoundValueComponent = new DefaultFilterValueComponent(parentSearch);
                    jPanelLowerBound.add((Component)lowerBoundValueComponent, BorderLayout.CENTER);
                    break;
            }
        }

        // Add the applicable operators to the combo box.
        for (DataFilterOperator operator : lowerBoundValueComponent.getApplicableOperators())
        {
            jComboBoxOperator.addItem(new Pair<String, String>(operator.toString(), operator.getDisplayString()));
        }

        // Add the range operator if applicable.
        if (lowerBoundValueComponent.isRangeApplicable())
        {
            jComboBoxOperator.addItem(new Pair<String, String>(RANGE_OPERATOR_ID, RANGE_OPERATOR_NAME));
        }
    }

    /**
     * Sets the new filter operator to use on this component.
     * @param newOperator The new operator to set on this component.
     */
    private void setFilterOperator(String operatorId)
    {
        if (operatorId.equals(RANGE_OPERATOR_ID))
        {
            // Set the selected item.
            jComboBoxOperator.setSelectedItem(new Pair<String, String>(RANGE_OPERATOR_ID, RANGE_OPERATOR_NAME));

            // Set the operator on the filter components.
            if (lowerBoundValueComponent != null) lowerBoundValueComponent.setOperator(DataFilterOperator.GREATER_THAN_OR_EQUAL);
            if (upperBoundValueComponent != null) upperBoundValueComponent.setOperator(DataFilterOperator.LESS_THAN_OR_EQUAL);
            
            // Set the upper bound component visible.
            jLabelFrom.setVisible(true);
            jLabelTo.setVisible(true);
            jPanelUpperBound.setVisible(true);

            // Make sure the UI is validated after the changes we made.
            revalidate();
        }
        else
        {
            DataFilterOperator operator;

            // Set the selected item.
            operator = DataFilterOperator.valueOf(operatorId);
            jComboBoxOperator.setSelectedItem(new Pair<String, String>(operator.toString(), operator.getDisplayString()));

            // Set the operator on the filter components.
            if (lowerBoundValueComponent != null) lowerBoundValueComponent.setOperator(operator);
            if (upperBoundValueComponent != null) upperBoundValueComponent.setOperator(operator);
            
            // Set the upper bound component invisible.
            jLabelFrom.setVisible(false);
            jLabelTo.setVisible(false);
            jPanelUpperBound.setVisible(false);

            // Make sure the UI is validated after the changes we made.
            revalidate();
        }
    }

    /**
     * Removes this {@code PropertyCriterionPanel} from its parent, effectively discarding the component.
     */
    private void remove()
    {
        parentPanel.removeProperty(this);
    }

    /**
     * Returns the preferred layout widths used by this component.
     * @return The preferred layout widths used by this component.
     */
    public LayoutColumnWidths getLayoutColumnWidths()
    {
        LayoutColumnWidths widths;

        widths = new LayoutColumnWidths();
        widths.setWidth(0, jLabelProperty.getPreferredSize().width);
        widths.setWidth(1, propertyLookupLink.getPreferredSize().width);
        widths.setWidth(2, jComboBoxOperator.getPreferredSize().width);
        widths.setWidth(3, jPanelLowerBound.getPreferredSize().width);
        return widths;
    }

    /**
     * Sets the layout widths to be used by this component and its content.
     * @param widths The layout widths to set on this component.
     */
    public void setLayoutColumnWidths(LayoutColumnWidths widths)
    {
        Dimension size;

        size = jLabelProperty.getMinimumSize();
        size.width = widths.getWidth(0);
        jLabelProperty.setMinimumSize(size);
        jLabelProperty.setPreferredSize(size);

        size = jPanelPropertyLookup.getMinimumSize();
        size.width = widths.getWidth(1);
        jPanelPropertyLookup.setMinimumSize(size);
        jPanelPropertyLookup.setPreferredSize(size);

        size = jComboBoxOperator.getMinimumSize();
        size.width = widths.getWidth(2);
        jComboBoxOperator.setMinimumSize(size);
        jComboBoxOperator.setPreferredSize(size);

        revalidate();
    }

    /**
     * Creates a new {@code T8PropertyFilterCriterion} object from the state of this UI component.
     * @return The {@code T8PropertyFilterCriterion} object created from the state of this UI component.
     */
    public T8PropertyFilterCriterion getPropertyFilterCriterion()
    {
        T8PropertyFilterCriterion propertyCriterion;
        Pair<String, String> selectedOperator;

        propertyCriterion = new T8PropertyFilterCriterion(propertyId, fieldId);
        propertyCriterion.setAllowRemoval(allowRemoval);
        selectedOperator = (Pair<String, String>)jComboBoxOperator.getSelectedItem();

        if (selectedOperator.getValue1().equals(RANGE_OPERATOR_ID))
        {
            propertyCriterion.addValueCriterion(T8ValueFilterCriterion.createCriterion(dataType, DataFilterOperator.GREATER_THAN_OR_EQUAL, lowerBoundValueComponent.getFilterValue()));
            propertyCriterion.addValueCriterion(T8ValueFilterCriterion.createCriterion(dataType, DataFilterOperator.LESS_THAN_OR_EQUAL, upperBoundValueComponent.getFilterValue()));
        }
        else
        {
            DataFilterOperator operator;

            operator = DataFilterOperator.valueOf(selectedOperator.getValue1());
            propertyCriterion.addValueCriterion(T8ValueFilterCriterion.createCriterion(dataType, operator, lowerBoundValueComponent.getFilterValue()));
        }

        return propertyCriterion;
    }

    /**
     * Returns the id of the property set on this filter criterion. If no
     * property is set null is return, which indicates this filter criterion
     * applies to any property.
     *
     * @return The id of the property set on this filter criterion.
     */
    public String getPropertyId()
    {
        return propertyId;
    }

    /**
     * Returns the id of the field set on this filter criterion. If no field is
     * set, {@code null} is returned, which indicates that the filter criterion
     * applies to all fields for the specific property.
     *
     * @return The {@code String} field id, if set, {@code null} otherwise
     *
     * @see #getPropertyId()
     */
    public String getFieldId()
    {
        return this.fieldId;
    }

    /**
     * Translates the input string to the language of the current session.
     * @param inputString The input string to translate.
     * @return The translated version of the input string.
     */
    private String translate(String inputString)
    {
        return inputString;
    }

    /**
     * Clears the filter value currently set on this component.
     */
    public void clearFilterValue()
    {
        if (lowerBoundValueComponent != null) lowerBoundValueComponent.clearFilterValue();
        if (upperBoundValueComponent != null) upperBoundValueComponent.clearFilterValue();
    }

    /**
     * Resets the filter operator on this component to the default.
     */
    public void resetFilterOperator()
    {
        setFilterOperator(resetOperatorId);
    }

    private class ComboCellRenderer extends DefaultListCellRenderer
    {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            Pair<String, String> operator;

            operator = (Pair<String, String>)value;
            return super.getListCellRendererComponent(list, operator != null ? translate(operator.getValue2()) : null, index, isSelected, cellHasFocus);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelProperty = new javax.swing.JLabel();
        jPanelPropertyLookup = new javax.swing.JPanel();
        jComboBoxOperator = new javax.swing.JComboBox<>();
        jLabelFrom = new javax.swing.JLabel();
        jPanelLowerBound = new javax.swing.JPanel();
        jLabelTo = new javax.swing.JLabel();
        jPanelUpperBound = new javax.swing.JPanel();
        jToolBarControls = new javax.swing.JToolBar();
        jButtonRemove = new javax.swing.JButton();

        setBackground(new java.awt.Color(234, 234, 255));
        setLayout(new java.awt.GridBagLayout());

        jLabelProperty.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelProperty.setText("Property:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jLabelProperty, gridBagConstraints);

        jPanelPropertyLookup.setOpaque(false);
        jPanelPropertyLookup.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jPanelPropertyLookup, gridBagConstraints);

        jComboBoxOperator.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxOperatorItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jComboBoxOperator, gridBagConstraints);

        jLabelFrom.setText("From:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jLabelFrom, gridBagConstraints);

        jPanelLowerBound.setOpaque(false);
        jPanelLowerBound.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jPanelLowerBound, gridBagConstraints);

        jLabelTo.setText("To:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        add(jLabelTo, gridBagConstraints);

        jPanelUpperBound.setOpaque(false);
        jPanelUpperBound.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanelUpperBound.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jPanelUpperBound, gridBagConstraints);

        jToolBarControls.setFloatable(false);
        jToolBarControls.setRollover(true);
        jToolBarControls.setOpaque(false);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/cross-white.png"))); // NOI18N
        jButtonRemove.setFocusable(false);
        jButtonRemove.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRemove.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRemove.setOpaque(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarControls.add(jButtonRemove);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        add(jToolBarControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        remove();
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jComboBoxOperatorItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxOperatorItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxOperatorItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            setFilterOperator(((Pair<String, String>)evt.getItem()).getValue1());
        }
    }//GEN-LAST:event_jComboBoxOperatorItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JComboBox<Pair<String, String>> jComboBoxOperator;
    private javax.swing.JLabel jLabelFrom;
    private javax.swing.JLabel jLabelProperty;
    private javax.swing.JLabel jLabelTo;
    private javax.swing.JPanel jPanelLowerBound;
    private javax.swing.JPanel jPanelPropertyLookup;
    private javax.swing.JPanel jPanelUpperBound;
    private javax.swing.JToolBar jToolBarControls;
    // End of variables declaration//GEN-END:variables
}
