package com.pilog.t8.data.document.path;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.data.object.T8ClientDataObjectProvider;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ClientDocPathEvaluatorFactory implements DocPathEvaluatorFactory
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private OntologyProvider ontologyProvider;
    private TerminologyProvider terminologyProvider;
    private DataRecordProvider recordProvider;
    private T8DataObjectProvider dataObjectProvider;
    private String languageId;

    public T8ClientDocPathEvaluatorFactory(T8Context context)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
    }

    @Override
    public void setLanguage(String languageId)
    {
        this.languageId = languageId;
        if (terminologyProvider != null)
        {
            terminologyProvider.setLanguage(languageId);
        }
    }

    @Override
    public DocPathExpressionEvaluator getEvaluator()
    {
        T8ConfigurationManager configurationManager;
        DocPathExpressionEvaluator evaluator;

        configurationManager = clientContext.getConfigurationManager();

        if (ontologyProvider == null)
        {
            T8OntologyClientApi ontApi;

            ontApi =  configurationManager.getAPI(context, T8OntologyClientApi.API_IDENTIFIER);
            ontologyProvider = ontApi.getOntologyProvider();
        }

        if (terminologyProvider == null)
        {
            T8TerminologyClientApi trmApi;

            trmApi = configurationManager.getAPI(context, T8TerminologyClientApi.API_IDENTIFIER);
            terminologyProvider = trmApi.getTerminologyProvider();
            terminologyProvider.setLanguage(languageId);
        }

        if (recordProvider == null)
        {
            T8DataRecordClientApi recApi;

            recApi = configurationManager.getAPI(context, T8DataRecordClientApi.API_IDENTIFIER);
            recordProvider = recApi.getRecordProvider();
        }

        if (dataObjectProvider == null)
        {
            dataObjectProvider = new T8ClientDataObjectProvider(context);
        }

        evaluator = new DocPathExpressionEvaluator();
        evaluator.setOntologyProvider(ontologyProvider);
        evaluator.setTerminologyProvider(terminologyProvider);
        evaluator.setRecordProvider(recordProvider);
        evaluator.setDataObjectProvider(dataObjectProvider);
        return evaluator;
    }
}
