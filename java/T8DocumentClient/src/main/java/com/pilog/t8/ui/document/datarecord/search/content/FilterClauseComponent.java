package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterClause;

/**
 * @author Bouwer du Preez
 */
public interface FilterClauseComponent
{
    public void clearFilterValues();
    public T8RecordFilterClause getFilterClause();
    public void removeHistory(HistoryFilterPanel historyFilterPanel);
    public void removeCode(CodeCriterionPanel codeCriterionPanel);
    public void removeProperty(PropertyCriterionPanel propertyFilterPanel);
}
