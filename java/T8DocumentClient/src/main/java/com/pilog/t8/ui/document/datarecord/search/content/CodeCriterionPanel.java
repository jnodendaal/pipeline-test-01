package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarecord.filter.T8CodeFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8CodeFilterCriterion.CodeCriterionTarget;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.AbstractAction;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.prompt.PromptSupport;

/**
 * @author Bouwer du Preez
 */
public class CodeCriterionPanel extends JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(CodeCriterionPanel.class);

    private final DataFilterOperator resetOperator;
    private final T8DataRecordSearch parentSearch;
    private final FilterClauseComponent parentPanel;
    private JXHyperlink codeTypeLookup;
    private JScrollPane valueScrollPane;
    private JTextField valueTextField;
    private JTextArea valueTextArea;
    private CodeCriterionTarget target;
    private String codeTypeId;
    private String codeTypeTerm;
    private final boolean allowRemoval;

    private static final String ANY_CODE_TYPE_TERM = "Any Code Type";

    public CodeCriterionPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, CodeCriterionTarget target, String codeTypeId, String codeTypeTerm)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.codeTypeId = codeTypeId;
        this.codeTypeTerm = codeTypeTerm;
        this.allowRemoval = true;
        this.target = target;
        initComponents();
        constructPanel();
        createValueEditors(null);
        updateValueComponent(DataFilterOperator.EQUAL);
        for (Object item : DataFilterOperator.values()) jComboBoxOperator.addItem(item);
        jComboBoxOperator.setSelectedItem(DataFilterOperator.EQUAL);
        this.resetOperator = DataFilterOperator.EQUAL;
        jComboBoxOperator.setRenderer(new ComboCellRenderer());
    }

    public CodeCriterionPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, T8CodeFilterCriterion filter)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.codeTypeId = filter.getCodeTypeId();
        this.allowRemoval = filter.isAllowRemoval();
        this.target = filter.getTarget();
        if (codeTypeId != null) this.codeTypeTerm = parentSearch.getTerminologyProvider().getTerm(null, codeTypeId);
        else this.codeTypeTerm = null;
        initComponents();
        constructPanel();
        createValueEditors(filter.getFilterValue());
        updateValueComponent(filter.getOperator());
        for (Object item : DataFilterOperator.values()) jComboBoxOperator.addItem(item);
        jComboBoxOperator.setSelectedItem(filter.getOperator());
        this.resetOperator = filter.getOperator();
        jComboBoxOperator.setRenderer(new ComboCellRenderer());
    }

    private void updateValueComponent(DataFilterOperator operator)
    {
        // First we remove the previous child components from the panel.
        this.jPanelValue.removeAll();

        // Then we add the correct component, based on the operator.
        switch (operator)
        {
            case IN:
            case NOT_IN:
                this.jPanelValue.add(this.valueScrollPane, BorderLayout.CENTER);
                break;
            default:
                this.jPanelValue.add(this.valueTextField, BorderLayout.CENTER);
        }

        // Make sure the UI is validated after the changes we made.
        revalidate();
    }

    private void constructPanel()
    {
        // Create the code type lookup.
        codeTypeLookup = new JXHyperlink(new AbstractAction(codeTypeId != null ? codeTypeTerm : ANY_CODE_TYPE_TERM)
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                lookupCodeType();
            }
        });
        jPanelCodeTypeLookup.add(codeTypeLookup, BorderLayout.CENTER);

        // If removal is disabled, hide the button.
        if (!allowRemoval) jButtonRemove.setVisible(false);
    }

    private void createValueEditors(Object filterValue)
    {
        this.valueTextField = new JTextField();
        this.valueScrollPane = new JScrollPane();
        this.valueTextArea = new JTextArea();

        PromptSupport.setPrompt(translate("One value per line..."), this.valueTextArea);
        ((AbstractDocument)this.valueTextArea.getDocument()).setDocumentFilter(new DocumentPasteFilter());
        this.valueTextArea.setRows(5);
        this.valueScrollPane.setViewportView(this.valueTextArea);

        if (filterValue != null)
        {
            this.valueTextField.setText(filterValue.toString());
            this.valueTextArea.setText(filterValue.toString());
        }
    }

    private void lookupCodeType()
    {
        List<Pair<String, String>> selectedProperties;

        if (parentPanel instanceof DrCriteriaPanel)
        {
            DrCriteriaPanel drPanel;
            
            drPanel = (DrCriteriaPanel)parentPanel;
            selectedProperties = parentSearch.getSelectedCodeType(null, drPanel.getDrId(), null, false);
        }
        else
        {
            DrInstanceCriteriaPanel drInstancePanel;
            
            drInstancePanel = (DrInstanceCriteriaPanel)parentPanel;
            selectedProperties = parentSearch.getSelectedCodeType(drInstancePanel.getDrInstanceId(), null, null, false);
        }
        
        if ((selectedProperties != null) && (selectedProperties.size() > 0))
        {
            Pair<String, String> selectedProperty;

            selectedProperty = selectedProperties.get(0);
            codeTypeId = selectedProperty.getValue1();
            codeTypeTerm = selectedProperty.getValue2();
            codeTypeLookup.setText(codeTypeId != null ? codeTypeTerm : ANY_CODE_TYPE_TERM);
        }
    }

    private void remove()
    {
        parentPanel.removeCode(this);
    }

    public LayoutColumnWidths getLayoutColumnWidths()
    {
        LayoutColumnWidths widths;

        widths = new LayoutColumnWidths();
        widths.setWidth(0, jLabelCode.getPreferredSize().width);
        widths.setWidth(1, codeTypeLookup.getPreferredSize().width);
        widths.setWidth(2, jComboBoxOperator.getPreferredSize().width);
        widths.setWidth(3, jPanelValue.getPreferredSize().width);
        return widths;
    }

    public void setLayoutColumnWidths(LayoutColumnWidths widths)
    {
        Dimension size;

        size = jLabelCode.getMinimumSize();
        size.width = widths.getWidth(0);
        jLabelCode.setMinimumSize(size);
        jLabelCode.setPreferredSize(size);

        size = jPanelCodeTypeLookup.getMinimumSize();
        size.width = widths.getWidth(1);
        jPanelCodeTypeLookup.setMinimumSize(size);
        jPanelCodeTypeLookup.setPreferredSize(size);

        size = jComboBoxOperator.getMinimumSize();
        size.width = widths.getWidth(2);
        jComboBoxOperator.setMinimumSize(size);
        jComboBoxOperator.setPreferredSize(size);

        revalidate();
    }

    public T8CodeFilterCriterion getCodeFilterCriterion()
    {
        T8CodeFilterCriterion criterion;
        DataFilterOperator operator;

        criterion = new T8CodeFilterCriterion(codeTypeId);
        operator = (DataFilterOperator)jComboBoxOperator.getSelectedItem();
        criterion.setAllowRemoval(allowRemoval);
        criterion.setTarget(target);
        criterion.setFilterValue(getValue(operator));
        criterion.setOperator(operator);

        return criterion;
    }

    private Object getValue(DataFilterOperator operator)
    {
        if (operator == DataFilterOperator.IN || operator == DataFilterOperator.NOT_IN)
        {
            String textAreaValue;

            textAreaValue = this.valueTextArea.getText();
            if (!Strings.isNullOrEmpty(textAreaValue))
            {
                return Stream.of(this.valueTextArea.getText().split("\n")).map(String::trim).collect(Collectors.toCollection(ArrayList::new));
            }
            else return new ArrayList<>();
        }
        else return this.valueTextField.getText();
    }

    public String getCodeTypeId()
    {
        return codeTypeId;
    }

    private String translate(String inputString)
    {
        return inputString;
    }

    public void clearFilterValue()
    {
        this.valueTextField.setText(null);
        this.valueTextArea.setText(null);
    }

    public void resetFilterOperator()
    {
        jComboBoxOperator.setSelectedItem(this.resetOperator);
    }

    private class ComboCellRenderer extends DefaultListCellRenderer
    {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            DataFilterOperator operator;

            operator = (DataFilterOperator)value;
            return super.getListCellRendererComponent(list, translate(operator.getDisplayString()), index, isSelected, cellHasFocus);
        }
    }

    private class DocumentPasteFilter extends DocumentFilter
    {
        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException
        {
            // Only if a value was pasted, otherwise add it as is
            if (text != null && text.length() > 1)
            {
                StringBuilder textBuilder;
                String[] values;

                try
                {
                    if (!valueTextArea.getText().isEmpty())
                    {
                        // If there is text and it doesn't end on a newline, add one
                        if (valueTextArea.getText().endsWith("\n"))
                        {
                            textBuilder = new StringBuilder();
                        } else textBuilder = new StringBuilder("\n");
                    } else textBuilder = new StringBuilder();

                    // Split the pasted value on commas and change it to each item on a newline
                    values = text.split(",", 0);
                    for (String value : values)
                    {
                        if (!value.trim().isEmpty()) textBuilder.append(value.trim()).append("\n");
                    }

                    // Now add the new string to the document
                    super.replace(fb, offset, length, textBuilder.toString(), attrs);
                }
                catch (Exception ex)
                {
                    LOGGER.log("Couldn't properly add the pasted value", ex);
                }
            } else super.replace(fb, offset, length, text, attrs);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelCode = new javax.swing.JLabel();
        jPanelCodeTypeLookup = new javax.swing.JPanel();
        jComboBoxOperator = new javax.swing.JComboBox();
        jPanelValue = new javax.swing.JPanel();
        jToolBarControls = new javax.swing.JToolBar();
        jButtonRemove = new javax.swing.JButton();

        setBackground(new java.awt.Color(234, 234, 255));
        setLayout(new java.awt.GridBagLayout());

        jLabelCode.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelCode.setText("Code:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jLabelCode, gridBagConstraints);

        jPanelCodeTypeLookup.setOpaque(false);
        jPanelCodeTypeLookup.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jPanelCodeTypeLookup, gridBagConstraints);

        jComboBoxOperator.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxOperatorItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jComboBoxOperator, gridBagConstraints);

        jPanelValue.setOpaque(false);
        jPanelValue.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jPanelValue, gridBagConstraints);

        jToolBarControls.setFloatable(false);
        jToolBarControls.setRollover(true);
        jToolBarControls.setOpaque(false);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/cross-white.png"))); // NOI18N
        jButtonRemove.setFocusable(false);
        jButtonRemove.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRemove.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRemove.setOpaque(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarControls.add(jButtonRemove);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        add(jToolBarControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        remove();
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jComboBoxOperatorItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxOperatorItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxOperatorItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            DataFilterOperator operator;

            operator = (DataFilterOperator)evt.getItem();
            updateValueComponent(operator);
            if (operator == DataFilterOperator.IS_NULL)
            {
                valueTextField.setEditable(false);
            }
            else if (operator == DataFilterOperator.IS_NOT_NULL)
            {
                valueTextField.setEditable(false);
            }
            else valueTextField.setEditable(true);
        }
    }//GEN-LAST:event_jComboBoxOperatorItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JComboBox jComboBoxOperator;
    private javax.swing.JLabel jLabelCode;
    private javax.swing.JPanel jPanelCodeTypeLookup;
    private javax.swing.JPanel jPanelValue;
    private javax.swing.JToolBar jToolBarControls;
    // End of variables declaration//GEN-END:variables
}
