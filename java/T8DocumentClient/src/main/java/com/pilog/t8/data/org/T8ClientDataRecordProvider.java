package com.pilog.t8.data.org;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ClientDataRecordProvider implements DataRecordProvider
{
    private final T8DataRecordClientApi recApi;

    public T8ClientDataRecordProvider(T8DataRecordClientApi recApi)
    {
        this.recApi = recApi;
    }

    @Override
    public DataRecord getDataFile(String dataFileId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDrComments, boolean includeParticleSettings)
    {
        try
        {
            return recApi.retrieveFile(dataFileId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDrComments, includeParticleSettings);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving root data record: " + dataFileId, e);
        }
    }

    @Override
    public List<DataRecord> getDataFiles(List<String> dataFileIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        try
        {
            return recApi.retrieveFiles(dataFileIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data records: " + dataFileIdList, e);
        }
    }

    @Override
    public DataRecord getDataFileByContentRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        try
        {
            return recApi.retrieveFileByContentRecord(recordId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data Files by content record: " + recordId, e);
        }
    }

    @Override
    public List<DataRecord> getDataFilesByContentRecord(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        try
        {
            return recApi.retrieveFilesByContentRecord(recordIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data Files by content record: " + recordIdList, e);
        }
    }

    @Override
    public DataRecord getDataRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendantRecords)
    {
        try
        {
            return recApi.retrieveRecord(recordId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendantRecords);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data record: " + recordId, e);
        }
    }

    @Override
    public List<DataRecord> getDataRecords(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendantRecords)
    {
        try
        {
            return recApi.retrieveRecords(recordIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendantRecords);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data records: " + recordIdList, e);
        }
    }

    @Override
    public T8AttachmentDetails getAttachmentDetails(String attachmentId)
    {
        try
        {
            return recApi.retrieveAttachmentDetails(attachmentId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving details for attachment: " + attachmentId, e);
        }
    }

    @Override
    public Map<String, T8AttachmentDetails> getRecordAttachmentDetails(String recordId)
    {
        try
        {
            return recApi.retrieveRecordAttachmentDetails(recordId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving attachment details for data record: " + recordId, e);
        }
    }

    @Override
    public String getRecordDrInstanceId(String recordId)
    {
        try
        {
            return recApi.retrieveRecordDrInstanceId(recordId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data record DR Instance ID: " + recordId, e);
        }
    }
}
