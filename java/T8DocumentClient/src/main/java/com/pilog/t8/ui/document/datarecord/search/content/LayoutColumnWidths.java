package com.pilog.t8.ui.document.datarecord.search.content;

import java.util.Arrays;

/**
 * @author Bouwer du Preez
 */
public class LayoutColumnWidths
{
    private final int[] widths;
    
    public LayoutColumnWidths()
    {
        widths = new int[6];
    }
    
    public int getWidth(int columnIndex)
    {
        return widths[columnIndex];
    }
    
    public void setWidth(int columnIndex, int width)
    {
        widths[columnIndex] = width;
    }
    
    public void updateToMax(LayoutColumnWidths inputWidths)
    {
        for (int index = 0; index < widths.length; index++)
        {
            int inputWidth;
            
            inputWidth = inputWidths.getWidth(index);
            if (inputWidth > widths[index]) widths[index] = inputWidth;
        }
    }

    @Override
    public String toString()
    {
        return Arrays.toString(widths);
    }
}
