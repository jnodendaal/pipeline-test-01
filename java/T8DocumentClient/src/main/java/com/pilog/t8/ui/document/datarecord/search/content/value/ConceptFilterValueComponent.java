package com.pilog.t8.ui.document.datarecord.search.content.value;

import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.ui.document.datarecord.search.content.DrCriteriaPanel;
import com.pilog.t8.ui.document.datarecord.search.content.DrInstanceCriteriaPanel;
import com.pilog.t8.ui.document.datarecord.search.content.FilterClauseComponent;
import com.pilog.t8.ui.document.datarecord.search.content.FilterValueComponent;
import com.pilog.t8.utilities.strings.Strings;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Pieter Strydom
 */
public class ConceptFilterValueComponent extends JPanel implements FilterValueComponent
{
    private final T8DataRecordSearch parentSearch;
    private final T8ComponentController controller;
    private final FilterClauseComponent parentPanel;
    private String propertyId;
    private String conceptId;
    private String conceptTerm;

    public ConceptFilterValueComponent(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, String propertyId)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.propertyId = propertyId;
        this.controller = parentSearch.getController();
        initComponents();
    }

    @Override
    public void setOperator(DataFilterOperator operator)
    {
        if (operator != DataFilterOperator.EQUAL || operator != DataFilterOperator.NOT_EQUAL)
        {
            this.conceptId = null;
            this.conceptTerm = jFormattedTextFieldDisplay.getText();
        }
    }

    @Override
    public void clearFilterValue()
    {
        jFormattedTextFieldDisplay.setText(null);
        this.conceptId = null;
        this.conceptTerm = null;
    }

    @Override
    public void setFilterValue(Object filterValue)
    {
        String valueString;

        valueString = filterValue != null ? filterValue.toString() : null;
        if (Strings.trimToNull(valueString) == null)
        {
            jFormattedTextFieldDisplay.setText(null);
        }
        else
        {
            if ((filterValue instanceof String) && (T8IdentifierUtilities.isConceptID((String)filterValue)))
            {
                TerminologyProvider terminologyProvider;
                String valueTerm;

                terminologyProvider = parentSearch.getTerminologyProvider();
                this.conceptId = valueString;

                valueTerm = terminologyProvider.getTerm(null, valueString);

                jFormattedTextFieldDisplay.setText(valueTerm);
            }
            else
            {
                this.conceptTerm = valueString;
                jFormattedTextFieldDisplay.setText(valueString);
            }
        }
    }

    @Override
    public Object getFilterValue()
    {
        // If we have a concept id available return it, otherwise return the term.
        if (conceptId != null)
            return conceptId;
        else
            return conceptTerm;
    }

    @Override
    public boolean isRangeApplicable()
    {
        return false;
    }

    @Override
    public List<DataFilterOperator> getApplicableOperators()
    {
        return Arrays.asList(T8DataFilterCriterion.DataFilterOperator.values());
    }

    private String translate(String inputString)
    {
        return controller.translate(inputString);
    }

    private void lookup()
    {
        Map<String, Object> selectedValueMap;
        String conceptId;
        String conceptTerm;

        if (parentPanel instanceof DrCriteriaPanel)
        {
            DrCriteriaPanel drPanel;

            drPanel = (DrCriteriaPanel)parentPanel;
            selectedValueMap = parentSearch.getSelectedControlledConcept(null, drPanel.getDrId(), propertyId);
        }
        else
        {
            DrInstanceCriteriaPanel drInstancePanel;

            drInstancePanel = (DrInstanceCriteriaPanel)parentPanel;
            selectedValueMap = parentSearch.getSelectedControlledConcept(drInstancePanel.getDrInstanceId(), null, propertyId);
        }

        if (selectedValueMap != null)
        {
            // Get the nessacary values from the map.
            conceptId = (String)selectedValueMap.get("ID");
            conceptTerm = (String)selectedValueMap.get("TERM");

            this.conceptId = conceptId;
            this.conceptTerm = null;

            jFormattedTextFieldDisplay.setText(conceptTerm);
        }
    }

    /** This method will set the correct variables when text is typed into
     * the lookup and not selected.
     */
    private void setTextTyped()
    {
        this.conceptId = null;
        this.conceptTerm = jFormattedTextFieldDisplay.getText();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupValue = new javax.swing.ButtonGroup();
        jFormattedTextFieldDisplay = new javax.swing.JFormattedTextField();
        jButtonLookup = new javax.swing.JButton();
        jButtonClear = new javax.swing.JButton();

        setBackground(new java.awt.Color(234, 234, 255));
        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jFormattedTextFieldDisplay.setMaximumSize(null);
        jFormattedTextFieldDisplay.setMinimumSize(null);
        jFormattedTextFieldDisplay.setPreferredSize(null);
        jFormattedTextFieldDisplay.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jFormattedTextFieldDisplayKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(jFormattedTextFieldDisplay, gridBagConstraints);

        jButtonLookup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/searchIcon.png"))); // NOI18N
        jButtonLookup.setToolTipText("Search for Data Value");
        jButtonLookup.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonLookup.setMaximumSize(null);
        jButtonLookup.setMinimumSize(null);
        jButtonLookup.setPreferredSize(null);
        jButtonLookup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLookupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jButtonLookup, gridBagConstraints);

        jButtonClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"))); // NOI18N
        jButtonClear.setToolTipText("Clear Data Value");
        jButtonClear.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonClear.setMaximumSize(null);
        jButtonClear.setMinimumSize(null);
        jButtonClear.setPreferredSize(null);
        jButtonClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jButtonClear, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLookupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLookupActionPerformed
        lookup();
    }//GEN-LAST:event_jButtonLookupActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearActionPerformed
        clearFilterValue();
    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jFormattedTextFieldDisplayKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedTextFieldDisplayKeyReleased
        setTextTyped();
    }//GEN-LAST:event_jFormattedTextFieldDisplayKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupValue;
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonLookup;
    private javax.swing.JFormattedTextField jFormattedTextFieldDisplay;
    // End of variables declaration//GEN-END:variables

}
