package com.pilog.t8.ui.document.datarecord.search.content.value;

import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.ui.document.datarecord.search.content.FilterClauseComponent;

/**
 *
 * @author Pieter Strydom
 */
public class DocumentReferenceFilterValueComponent extends ConceptFilterValueComponent
{
    public DocumentReferenceFilterValueComponent(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, String propertyId)
    {
        super(parentSearch, parentPanel, propertyId);
    }
}
