package com.pilog.t8.ui.document.datarecord.search;

import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterCriteria;
import com.pilog.t8.definition.ui.document.datarecord.search.T8DataRecordSearchAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordSearchOperationHandler extends T8ComponentOperationHandler
{
    private final T8DataRecordSearch dataSearch;

    public T8DataRecordSearchOperationHandler(T8DataRecordSearch dataSearch)
    {
        super(dataSearch);
        this.dataSearch = dataSearch;
    }

    @Override
    public Map<String, Object> executeOperation(String operationId, Map<String, Object> operationParameters)
    {
        if (operationId.equals(T8DataRecordSearchAPIHandler.OPERATION_CLEAR_FILTER_VALUES))
        {
            dataSearch.clearFilterValues();
            return null;
        }
        else if (operationId.equals(T8DataRecordSearchAPIHandler.OPERATION_SET_SEARCH_FILTER_CRITERIA))
        {
            T8RecordFilterCriteria filterCriteria;

            filterCriteria = (T8RecordFilterCriteria)operationParameters.get(T8DataRecordSearchAPIHandler.PARAMETER_SEARCH_FILTER_CRITERIA);
            dataSearch.setFilterCriteria(filterCriteria);
            return null;
        }
        else if (operationId.equals(T8DataRecordSearchAPIHandler.OPERATION_GET_SEARCH_FILTER_CRITERIA))
        {
            HashMap<String, Object> output;

            output = new HashMap<>();
            output.put(T8DataRecordSearchAPIHandler.PARAMETER_SEARCH_FILTER_CRITERIA, dataSearch.getFilterCriteria());
            return output;
        }
        else if (operationId.equals(T8DataRecordSearchAPIHandler.OPERATION_SET_ADVANCED_OPTIONS_VISIBLE))
        {
            boolean setVisible;

            setVisible = (boolean)operationParameters.get(T8DataRecordSearchAPIHandler.PARAMETER_SET_ADVANCED_OPTIONS_VISIBLE);
            dataSearch.setAdvancedOptionsVisible(setVisible);
            return null;
        }
        else return super.executeOperation(operationId, operationParameters);
    }
}
