package com.pilog.t8.data.org;

import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.data.document.DataRequirementProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirement;

/**
 * @author Bouwer du Preez
 */
public class T8ClientOrganizationDataRequirementProvider implements DataRequirementProvider
{
    private final T8DataRequirementClientApi drqApi;

    public T8ClientOrganizationDataRequirementProvider(T8DataRequirementClientApi drqApi)
    {
        this.drqApi = drqApi;
    }

    @Override
    public DataRequirement getDataRequirement(String drId, String languageId, boolean includeOntology, boolean includeTerminology)
    {
        try
        {
            return drqApi.retrieveDataRequirement(drId, languageId, includeOntology, includeTerminology);
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Failed to retrieve the requested data requirement for Data Requirement ID : " + drId, ex);
        }
    }
}
