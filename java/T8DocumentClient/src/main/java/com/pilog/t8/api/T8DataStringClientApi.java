package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.data.document.datastring.T8DataStringFilter;
import com.pilog.t8.data.document.datastring.T8DataStringFormat;
import com.pilog.t8.data.document.datastring.T8DataStringFormatLink;
import com.pilog.t8.data.document.datastring.T8DataStringInstance;
import com.pilog.t8.data.document.datastring.T8DataStringInstanceLink;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettingsList;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.security.T8Context;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataStringApiResource.*;

/**
 *
 * @author Pieter Strydom
 */
public class T8DataStringClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_DATA_STRING_CLIENT";

    public T8DataStringClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8DataStringClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    public List<T8DataString> retrieveRenderings(List<T8DataStringFilter> renderingFilters) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FILTER_LIST, renderingFilters);
        return (List<T8DataString>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_RETRIEVE_RENDERINGS, inputParameters).get(PARAMETER_DATA_STRING_LIST);
    }

    public T8DataStringFormat retrieveDataStringFormat(String dsFormatId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT_ID, dsFormatId);
        return (T8DataStringFormat)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_RETRIEVE_DATA_STRING_FORMAT, inputParameters).get(PARAMETER_DATA_STRING_FORMAT);
    }

    /**
     * Retrieves the Data String Instance Links for the specified DR Instances.
     * This method returns a list of link objects but the {@code T8DataStringInstance}
     * object in each link object may be shared with others in the list if
     * more than one link points to the same instance.
     * @param drInstanceIds The DR Instances for which to retrieve links.  If null, all links to all DR Instances will be retrieved.
     * @return The list of retrieve Data String Instance Links.
     * @throws Exception
     */
    public List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> retrieveDataStringInstanceLinkPairs(Collection<String> drInstanceIds) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DR_INSTANCE_ID_LIST, drInstanceIds);
        return (List<Pair<T8DataStringInstanceLink, T8DataStringInstance>>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_RETRIEVE_DATA_STRING_INSTANCE_LINK_PAIRS, inputParameters).get(PARAMETER_DATA_STRING_INSTANCE_LINK_LIST);
    }

    public void insertDataStringInstanceLink(T8DataStringInstanceLink link) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE_LINK, link);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_INSERT_DATA_STRING_INSTANCE_LINK, inputParameters);
    }

    public void updateDataStringInstanceLink(T8DataStringInstanceLink link) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE_LINK, link);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_UPDATE_DATA_STRING_INSTANCE_LINK, inputParameters);
    }

    public void saveDataStringInstanceLink(T8DataStringInstanceLink link) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE_LINK, link);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_SAVE_DATA_STRING_INSTANCE_LINK, inputParameters);
    }

    public void insertDataString(String recordId, String dsInstanceId, String dataString) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_RECORD_ID, recordId);
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE_ID, dsInstanceId);
        inputParameters.put(PARAMETER_DATA_STRING, dataString);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_INSERT_DATA_STRING, inputParameters);
    }

    public void saveDataString(String recordId, String dsInstanceId, String dataString) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_RECORD_ID, recordId);
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE_ID, dsInstanceId);
        inputParameters.put(PARAMETER_DATA_STRING, dataString);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_SAVE_DATA_STRING, inputParameters);
    }

    public void insertDataStringFormat(T8DataStringFormat format) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT, format);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_INSERT_DATA_STRING_FORMAT, inputParameters);
    }

    public void updateDataStringFormat(T8DataStringFormat format) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT, format);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_UPDATE_DATA_STRING_FORMAT, inputParameters);
    }

    public void saveDataStringFormat(T8DataStringFormat format) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT, format);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_SAVE_DATA_STRING_FORMAT, inputParameters);
    }
    public void saveDataStringInstance(T8DataStringInstance dataStringInstance) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE, dataStringInstance);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_SAVE_DATA_STRING_INSTANCE, inputParameters);
    }

    public void saveDataStringFormatLink(T8DataStringFormatLink formatLink) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT_LINK, formatLink);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_SAVE_DATA_STRING_FORMAT_LINK, inputParameters);
    }

    public void insertDataStringFormatLink(T8DataStringFormatLink formatLink) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT_LINK, formatLink);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_INSERT_DATA_STRING_FORMAT_LINK, inputParameters);
    }

    public void updateDataStringFormatLink(T8DataStringFormatLink formatLink) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT_LINK, formatLink);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_UPDATE_DATA_STRING_FORMAT_LINK, inputParameters);
    }

    public T8DataStringFormatLink retrieveDataStringFormatLink(String linkId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT_LINK_ID, linkId);
        return (T8DataStringFormatLink)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_RETRIEVE_DATA_STRING_FORMAT_LINK, inputParameters).get(PARAMETER_DATA_STRING_FORMAT_LINK);
    }

    public void saveDataStringParticleSettings(List<T8DataStringParticleSettings> settingsList) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST, settingsList);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_SAVE_DATA_STRING_PARTICLE_SETTINGS, inputParameters);
    }

    public void insertDataStringParticleSettings(T8DataStringParticleSettings settings) throws Exception
    {
       Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_PARTICLE_SETTINGS, settings);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_INSERT_DATA_STRING_PARTICLE_SETTINGS, inputParameters);
    }

    public boolean updateDataStringParticleSettings(T8DataStringParticleSettings settings) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_PARTICLE_SETTINGS, settings);
        return (boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_UPDATE_DATA_STRING_PARTICLE_SETTINGS, inputParameters).get(PARAMETER_IS_UPDATED);
    }

    public boolean deleteDataStringParticleSettings(T8DataStringParticleSettings settings) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_PARTICLE_SETTINGS, settings);
        return (boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DELETE_DATA_STRING_PARTICLE_SETTINGS, inputParameters).get(PARAMETER_IS_DELETED);
    }

    public void deleteDataStringFormat(String dsFormatId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT_ID, dsFormatId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DELETE_DATA_STRING_FORMAT, inputParameters);
    }

    public void deleteDataStringFormatLink(String linkId, boolean deleteFormat) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_FORMAT_LINK_ID, linkId);
        inputParameters.put(PARAMETER_DELETE_FORMAT, deleteFormat);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DELETE_DATA_STRING_FORMAT_LINK, inputParameters);
    }

    public void deleteDataStringInstance(String dsInstanceId, boolean deleteConcept) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE_ID, dsInstanceId);
        inputParameters.put(PARAMETER_DELETE_CONCEPT, deleteConcept);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DELETE_DATA_STRING_INSTANCE, inputParameters);
    }

    public int deleteDataStrings(List<String> recordIds, List<String> dsIids) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_RECORD_ID_LIST, recordIds);
        inputParameters.put(PARAMETER_DATA_STRING_INSTANCE_ID_LIST, dsIids);
        return (int)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_DELETE_DATA_STRINGS, inputParameters).get(PARAMETER_DELETE_COUNT);
    }

    public List<T8DataStringParticleSettings> retrieveDataStringParticleSettings(String recordId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_RECORD_ID, recordId);
        return (List<T8DataStringParticleSettings>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_RETRIEVE_DATA_STRING_PARTICLE_SETTINGS, inputParameters).get(PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST);
    }

    public T8DataStringParticleSettingsList retrieveDataStringParticleSettings(Set<String> drIdSet, Set<String> drInstanceIdSet, Set<String> recordIdSet) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DR_ID_LIST, drIdSet);
        inputParameters.put(PARAMETER_DR_INSTANCE_ID_LIST, drIdSet);
        inputParameters.put(PARAMETER_RECORD_ID_LIST, drIdSet);
        return (T8DataStringParticleSettingsList)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_RETRIEVE_DATA_STRING_PARTICLE_SETTINGS, inputParameters).get(PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST);
    }
}
