package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.org.T8ClientTerminologyProvider;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8TerminologyApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8TerminologyClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_TERMINOLOGY_CLIENT";

    public T8TerminologyClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8TerminologyClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    public TerminologyProvider getTerminologyProvider()
    {
        return new T8LRUCachedTerminologyProvider(new T8ClientTerminologyProvider(this), 3000);
    }

    public T8ConceptTerminologyList retrieveTerminology(List<String> languageIDList, List<String> conceptIDList) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_LANGUAGE_ID_LIST, languageIDList);
        inputParameters.put(PARAMETER_CONCEPT_ID_LIST, conceptIDList);
        return (T8ConceptTerminologyList)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_TRM_RETRIEVE_TERMINOLOGY_LIST, inputParameters).get(PARAMETER_TERMINOLOGY_LIST);
    }

    public T8ConceptTerminology retrieveTerminology(String languageId, String conceptId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_CONCEPT_ID, conceptId);
        return (T8ConceptTerminology)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_TRM_RETRIEVE_TERMINOLOGY, inputParameters).get(PARAMETER_TERMINOLOGY);
    }

    public void saveTerminology(T8ConceptTerminology terminology, boolean deletionEnabled) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_TERMINOLOGY, terminology);
        inputParameters.put(PARAMETER_INCLUDE_DELETION, deletionEnabled);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_TRM_SAVE_TERMINOLOGY, inputParameters);
    }
}
