package com.pilog.t8.data.org;

import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ClientOrganizationDataRequirementInstanceProvider implements DataRequirementInstanceProvider
{
    private final T8DataRequirementClientApi drqApi;

    public T8ClientOrganizationDataRequirementInstanceProvider(T8Context context)
    {
        this.drqApi = context.getClientContext().getConfigurationManager().getAPI(context, T8DataRequirementClientApi.API_IDENTIFIER);
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance(String drInstanceId, String languageId, boolean includeOntology, boolean includeTerminology)
    {
        try
        {
            return drqApi.retrieveDataRequirementInstance(drInstanceId, languageId, includeOntology, includeTerminology);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(String.format("Failed to retrieve the data requirement instance using parameters: DR Instance ID [%s]; Language ID [%s]; Include Ontology [%b]; Include Terminology [%b]", drInstanceId, languageId, includeOntology, includeTerminology), ex);
        }
    }
}
