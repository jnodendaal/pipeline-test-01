package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datafile.T8DataFileSaveResult;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.data.org.T8ClientDataRecordProvider;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import static com.pilog.t8.definition.api.T8DataRecordApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_DATA_RECORD_CLIENT";

    public T8DataRecordClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8DataRecordClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    public DataRecordProvider getRecordProvider()
    {
        return new T8ClientDataRecordProvider(this);
    }

    public T8DataFileSaveResult saveFileStateChange(DataRecord dataFile) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_FILE, dataFile);
        return (T8DataFileSaveResult)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_SAVE_FILE_STATE_CHANGE, inputParameters).get(PARAMETER_SAVE_RESULT);
    }

    public T8DataFileValidationReport validateFileStateChange(DataRecord dataFile) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_FILE, dataFile);
        return (T8DataFileValidationReport)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_VALIDATE_FILE_STATE_CHANGE, inputParameters).get(PARAMETER_VALIDATION_REPORT);
    }

    public DataRecord retrieveFile(String fileId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        List<DataRecord> rootDataRecords;

        rootDataRecords = retrieveFiles(ArrayLists.typeSafeList(fileId), languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        return rootDataRecords.size() > 0 ? rootDataRecords.get(0) : null;
    }

    public List<DataRecord> retrieveFiles(List<String> fileIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_FILE_IDS, fileIdList);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY, includeOntology);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        inputParameters.put(PARAMETER_INCLUDE_DESCRIPTIONS, includeDescriptions);
        inputParameters.put(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, includeAttachmentDetails);
        inputParameters.put(PARAMETER_INCLUDE_DR_COMMENTS, includeDRComments);
        inputParameters.put(PARAMETER_INCLUDE_PARTICLE_SETTINGS, includeParticleSettings);
        return (List<DataRecord>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_FILES, inputParameters).get(PARAMETER_FILE_LIST);
    }

    public DataRecord retrieveFileByContentRecord(String fileId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        List<DataRecord> rootDataRecords;

        rootDataRecords = retrieveFilesByContentRecord(ArrayLists.typeSafeList(fileId), languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        return rootDataRecords.size() > 0 ? rootDataRecords.get(0) : null;
    }

    public List<DataRecord> retrieveFilesByContentRecord(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD_IDS, recordIdList);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY, includeOntology);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        inputParameters.put(PARAMETER_INCLUDE_DESCRIPTIONS, includeDescriptions);
        inputParameters.put(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, includeAttachmentDetails);
        inputParameters.put(PARAMETER_INCLUDE_DR_COMMENTS, includeDRComments);
        inputParameters.put(PARAMETER_INCLUDE_PARTICLE_SETTINGS, includeParticleSettings);
        return (List<DataRecord>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_FILES_BY_CONTENT_RECORD, inputParameters).get(PARAMETER_FILE_LIST);
    }

    public DataRecord retrieveRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendantRecords) throws Exception
    {
        List<DataRecord> records;

        records = retrieveRecords(ArrayLists.typeSafeList(recordId), languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendantRecords);
        return records.size() > 0 ? records.get(0) : null;
    }

    public List<DataRecord> retrieveRecords(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendantRecords) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD_IDS, recordIdList);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY, includeOntology);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        inputParameters.put(PARAMETER_INCLUDE_DESCRIPTIONS, includeDescriptions);
        inputParameters.put(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, includeAttachmentDetails);
        inputParameters.put(PARAMETER_INCLUDE_DR_COMMENTS, includeDRComments);
        inputParameters.put(PARAMETER_INCLUDE_PARTICLE_SETTINGS, includeParticleSettings);
        inputParameters.put(PARAMETER_INCLUDE_DESCENDANT_RECORDS, includeDescendantRecords);
        return (List<DataRecord>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_RECORDS, inputParameters).get(PARAMETER_RECORD_LIST);
    }

    public DataRecord createRecord(DataRecord dataFile, String recordId, String propertyId, String fieldId, String drInstanceId, String languageId, boolean includeTerminology, boolean includeDrComments) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_FILE, dataFile);
        inputParameters.put(PARAMETER_RECORD_ID, recordId);
        inputParameters.put(PARAMETER_PROPERTY_ID, propertyId);
        inputParameters.put(PARAMETER_FIELD_ID, fieldId);
        inputParameters.put(PARAMETER_DR_INSTANCE_ID, drInstanceId);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        inputParameters.put(PARAMETER_INCLUDE_DR_COMMENTS, includeDrComments);
        return (DataRecord)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_CREATE_RECORD, inputParameters).get(PARAMETER_RECORD);
    }

    public DataRecord changeRecordDrInstance(DataRecord dataFile, String recordId, String drInstanceId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDrComments, boolean includeParticleSettings) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_FILE, dataFile);
        inputParameters.put(PARAMETER_RECORD_ID, recordId);
        inputParameters.put(PARAMETER_DR_INSTANCE_ID, drInstanceId);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY, includeOntology);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        inputParameters.put(PARAMETER_INCLUDE_DESCRIPTIONS, includeDescriptions);
        inputParameters.put(PARAMETER_INCLUDE_ATTACHMENT_DETAILS, includeAttachmentDetails);
        inputParameters.put(PARAMETER_INCLUDE_DR_COMMENTS, includeDrComments);
        inputParameters.put(PARAMETER_INCLUDE_PARTICLE_SETTINGS, includeParticleSettings);
        return (DataRecord)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_CHANGE_RECORD_DR_INSTANCE, inputParameters).get(PARAMETER_FILE);
    }

    public String resolveConceptId(DataRecord contextRecord, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, String matchString, List<T8ConceptElementType> conceptElementTypes) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD, contextRecord);
        inputParameters.put(PARAMETER_TARGET_RECORD_ID, targetRecordId);
        inputParameters.put(PARAMETER_TARGET_PROPERTY_ID, targetPropertyId);
        inputParameters.put(PARAMETER_TARGET_FIELD_ID, targetFieldId);
        inputParameters.put(PARAMETER_TARGET_DATA_TYPE_ID, targetDataTypeId);
        inputParameters.put(PARAMETER_MATCH_STRING, matchString);
        inputParameters.put(PARAMETER_CONCEPT_ELEMENT_TYPES, conceptElementTypes);

        return (String)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RESOLVE_CONCEPT_ID, inputParameters).get(PARAMETER_CONCEPT_ID);
    }

    public List<RecordValue> retrieveValueSuggestions(DataRecord contextRecord, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, String searchPrefix, int pageOffset, int pageSize) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD, contextRecord);
        inputParameters.put(PARAMETER_TARGET_RECORD_ID, targetRecordId);
        inputParameters.put(PARAMETER_TARGET_PROPERTY_ID, targetPropertyId);
        inputParameters.put(PARAMETER_TARGET_FIELD_ID, targetFieldId);
        inputParameters.put(PARAMETER_TARGET_DATA_TYPE_ID, targetDataTypeId);
        inputParameters.put(PARAMETER_SEARCH_PREFIX, searchPrefix);
        inputParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        inputParameters.put(PARAMETER_PAGE_SIZE, pageSize);

        return (List<RecordValue>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_VALUE_SUGGESTIONS, inputParameters).get(PARAMETER_RECORD_VALUES);
    }

    public List<RecordValue> retrieveValueOptions(DataRecord contextRecord, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, int pageOffset, int pageSize) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD, contextRecord);
        inputParameters.put(PARAMETER_TARGET_RECORD_ID, targetRecordId);
        inputParameters.put(PARAMETER_TARGET_PROPERTY_ID, targetPropertyId);
        inputParameters.put(PARAMETER_TARGET_FIELD_ID, targetFieldId);
        inputParameters.put(PARAMETER_TARGET_DATA_TYPE_ID, targetDataTypeId);
        inputParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        inputParameters.put(PARAMETER_PAGE_SIZE, pageSize);
        return (List<RecordValue>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_VALUE_OPTIONS, inputParameters).get(PARAMETER_RECORD_VALUES);
    }

    public List<T8ConceptTerminology> retrieveDrInstanceOptions(DataRecord contextRecord, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, int pageOffset, int pageSize) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD, contextRecord);
        inputParameters.put(PARAMETER_TARGET_RECORD_ID, targetRecordId);
        inputParameters.put(PARAMETER_TARGET_PROPERTY_ID, targetPropertyId);
        inputParameters.put(PARAMETER_TARGET_FIELD_ID, targetFieldId);
        inputParameters.put(PARAMETER_TARGET_DATA_TYPE_ID, targetDataTypeId);
        inputParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        inputParameters.put(PARAMETER_PAGE_SIZE, pageSize);
        return (List<T8ConceptTerminology>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_DR_INSTANCE_OPTIONS, inputParameters).get(PARAMETER_CONCEPT_TERMINOLOGY_LIST);
    }

    public T8AttachmentDetails retrieveAttachmentDetails(String attachmentId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_ATTACHMENT_ID, attachmentId);
        return (T8AttachmentDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_ATTACHMENT_DETAILS, inputParameters).get(PARAMETER_ATTACHMENT_DETAILS);
    }

    public Map<String, T8AttachmentDetails> retrieveRecordAttachmentDetails(String recordId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD_ID, recordId);
        return (Map<String, T8AttachmentDetails>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_RECORD_ATTACHMENT_DETAILS, inputParameters).get(PARAMETER_ATTACHMENT_DETAILS);
    }

    public String retrieveRecordDrInstanceId(String recordId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ACCESS_CONTEXT, context);
        inputParameters.put(PARAMETER_RECORD_ID, recordId);
        return (String)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_RETRIEVE_RECORD_DR_INSTANCE_ID, inputParameters).get(PARAMETER_DR_INSTANCE_ID);
    }

    public T8FileDetails exportAttachment(String attachmentId, String fileContextIid, String fileContextId, String directoryPath, String fileName) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ATTACHMENT_ID, attachmentId);
        inputParameters.put(PARAMETER_FILE_CONTEXT_IID, fileContextIid);
        inputParameters.put(PARAMETER_FILE_CONTEXT_ID, fileContextId);
        inputParameters.put(PARAMETER_PATH, directoryPath);
        inputParameters.put(PARAMETER_FILENAME, fileName);
        return (T8FileDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_REC_EXPORT_ATTACHMENT, inputParameters).get(PARAMETER_ATTACHMENT_DETAILS);
    }
}
