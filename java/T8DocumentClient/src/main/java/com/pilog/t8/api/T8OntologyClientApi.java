package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.ontology.T8ConceptPair;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.T8LruCachedOntologyProvider;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.ontology.T8OntologyStructureProvider;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.org.T8ClientOntologyProvider;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8OntologyApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyClientApi implements T8Api, T8OntologyStructureProvider
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private T8ComponentController controller;

    public static final String API_IDENTIFIER = "@API_ONTOLOGY_CLIENT";

    public T8OntologyClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8OntologyClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    @Override
    public T8OntologyStructure getOntologyStructure()
    {
        try
        {
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<>();
            return (T8OntologyStructure)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_GET_ONTOLOGY_STRUCTURE, inputParameters).get(PARAMETER_ONTOLOGY_STRUCTURE);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching ontology structure.", e);
        }
    }

    public boolean deleteOntologyClass(String ocId, boolean deleteOntology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_OC_ID, ocId);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY, deleteOntology);
        return (boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_DELETE_ONTOLOGY_CLASS, inputParameters).get(PARAMETER_SUCCESS);
    }

    public void deleteDefinition(String definitionId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DEFINITION_ID, definitionId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_DELETE_DEFINITION, inputParameters);
    }

    public void deleteCode(String codeId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CODE_ID, codeId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_DELETE_CODE, inputParameters);
    }

    public void deleteConcept(String conceptId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_ID, conceptId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_DELETE_CONCEPT, inputParameters);
    }

    public void deleteTerm(String termId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_TERM_ID, termId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_DELETE_TERM, inputParameters);
    }

    public void deleteAbbreviation(String abbreviationId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ABBREVIATION_ID, abbreviationId);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_DELETE_ABBREVIATION, inputParameters);
    }

    public boolean moveOntologyClass(String newParentOcId, String ocId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_OC_ID, ocId);
        inputParameters.put(PARAMETER_PARENT_OC_ID, newParentOcId);
        return (boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_MOVE_ONTOLOGY_CLASS, inputParameters).get(PARAMETER_SUCCESS);
    }

    public T8OntologyStructure recacheOntologyStructure()
    {
        try
        {
            T8OntologyStructure dataStructure;

            dataStructure = (T8OntologyStructure)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RECACHE_ONTOLOGY_STRUCTURE, null).get(PARAMETER_ONTOLOGY_STRUCTURE);
            if (dataStructure == null) throw new Exception("Organization Data Structure not found.");
            else return dataStructure;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching Ontology Data Structure.", e);
        }
    }

    public T8OrganizationOntologyFactory getOntologyFactory()
    {
        T8OrganizationOntologyFactory factory;
        T8OrganizationStructure orgStructure;
        T8OrganizationClientApi orgApi;

        // Get the organization structure.
        orgApi = controller != null ? controller.getApi(T8OrganizationClientApi.API_IDENTIFIER) : clientContext.getConfigurationManager().getAPI(context, T8OrganizationClientApi.API_IDENTIFIER);
        orgStructure = orgApi.getOrganizationStructure();

        // Construct the factory and configure the default settings.
        factory = new T8OrganizationOntologyFactory(context);
        factory.setLanguageID(context.getLanguageId());
        factory.setDataStructureID(getOntologyStructure().getID());
        factory.setConceptORGID(orgStructure.getRootOrganizationID());
        factory.setTermORGID(orgStructure.getRootOrganizationID());
        factory.setAbbreviationORGID(orgStructure.getRootOrganizationID());
        factory.setDefinitionORGID(orgStructure.getRootOrganizationID());
        factory.setCodeORGID(orgStructure.getRootOrganizationID());
        factory.setTerminologyORGID(orgStructure.getRootOrganizationID());

        // Return the factory.
        return factory;
    }

    public OntologyProvider getOntologyProvider()
    {
        return new T8LruCachedOntologyProvider(new T8ClientOntologyProvider(this), 3000);
    }

    public List<T8OntologyCode> retrieveConceptCodes(String conceptId) throws Exception
    {
        return (List<T8OntologyCode>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_CONCEPT_CODES, HashMaps.createSingular(PARAMETER_CONCEPT_ID, conceptId)).get(PARAMETER_CONCEPT_CODES);
    }

    public List<T8OntologyAbbreviation> retrieveAbbreviations(List<String> abbreviationIdList) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ABBREVIATION_ID_LIST, abbreviationIdList);
        return (List<T8OntologyAbbreviation>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_ABBREVIATIONS, inputParameters).get(PARAMETER_ABBREVIATION_LIST);
    }

    public List<T8OntologyDefinition> retrieveDefinitions(List<String> definitionIdList) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DEFINITION_ID_LIST, definitionIdList);
        return (List<T8OntologyDefinition>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_DEFINITIONS, inputParameters).get(PARAMETER_DEFINITION_LIST);
    }

    public List<T8OntologyTerm> retrieveTerms(List<String> termIdList, boolean includeAbbreviations) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_TERM_ID_LIST, termIdList);
        inputParameters.put(PARAMETER_INCLUDE_ABBREVIATIONS, includeAbbreviations);
        return (List<T8OntologyTerm>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_TERMS, inputParameters).get(PARAMETER_TERM_LIST);
    }

    public List<T8OntologyCode> retrieveCodes(List<String> codeIdList) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CODE_ID_LIST, codeIdList);
        return (List<T8OntologyCode>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_CODES, inputParameters).get(PARAMETER_CODE_LIST);
    }

    public List<T8OntologyAbbreviation> retrieveTermAbbreviations(String termId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_TERM_ID, termId);
        return (List<T8OntologyAbbreviation>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_TERM_ABBREVIATIONS, inputParameters).get(PARAMETER_ABBREVIATION_LIST);
    }

    public List<T8OntologyDefinition> retrieveConceptDefinitions(String conceptId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_ID, conceptId);
        return (List<T8OntologyDefinition>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_CONCEPT_DEFINITIONS, inputParameters).get(PARAMETER_DEFINITION_LIST);
    }

    public List<T8OntologyTerm> retrieveConceptTerms(String conceptId, boolean includeAbbreviations) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_ID, conceptId);
        inputParameters.put(PARAMETER_INCLUDE_ABBREVIATIONS, includeAbbreviations);
        return (List<T8OntologyTerm>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_CONCEPT_TERMS, inputParameters).get(PARAMETER_TERM_LIST);
    }

    public void insertDefinition(T8OntologyDefinition definition) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DEFINITION, definition);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_INSERT_DEFINITION, inputParameters);
    }

    public void insertAbbreviation(T8OntologyAbbreviation abbreviation) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ABBREVIATION, abbreviation);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_INSERT_ABBREVIATION, inputParameters);
    }

    public void insertCode(T8OntologyCode code) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CODE, code);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_INSERT_CODE, inputParameters);
    }

    public void insertConcept(T8OntologyConcept concept, boolean insertTerms, boolean insertAbbreviations, boolean insertDefinitions, boolean insertCodes, boolean insertTerminology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT, concept);
        inputParameters.put(PARAMETER_INCLUDE_TERMS, insertTerms);
        inputParameters.put(PARAMETER_INCLUDE_ABBREVIATIONS, insertAbbreviations);
        inputParameters.put(PARAMETER_INCLUDE_DEFINITIONS, insertDefinitions);
        inputParameters.put(PARAMETER_INCLUDE_CODES, insertCodes);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, insertTerminology);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_INSERT_CONCEPT, inputParameters);
    }

    public void insertTerm(T8OntologyTerm term, boolean insertAbbreviations) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_TERM, term);
        inputParameters.put(PARAMETER_INCLUDE_ABBREVIATIONS, insertAbbreviations);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_INSERT_TERM, inputParameters);
    }

    public void saveDefinition(T8OntologyDefinition definition) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_DEFINITION, definition);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_SAVE_DEFINITION, inputParameters);
    }

    public void saveAbbreviation(T8OntologyAbbreviation abbreviation) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ABBREVIATION, abbreviation);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_SAVE_ABBREVIATION, inputParameters);
    }

    public void saveCode(T8OntologyCode code) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CODE, code);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_SAVE_CODE, inputParameters);
    }

    public void saveConcept(T8OntologyConcept concept, boolean deletionEnabled, boolean saveTerms, boolean saveAbbreviations, boolean saveDefinitions, boolean saveCodes, boolean saveTerminology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT, concept);
        inputParameters.put(PARAMETER_DELETION_ENABLED, deletionEnabled);
        inputParameters.put(PARAMETER_INCLUDE_TERMS, saveTerms);
        inputParameters.put(PARAMETER_INCLUDE_ABBREVIATIONS, saveAbbreviations);
        inputParameters.put(PARAMETER_INCLUDE_DEFINITIONS, saveDefinitions);
        inputParameters.put(PARAMETER_INCLUDE_CODES, saveCodes);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, saveTerminology);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_SAVE_CONCEPT, inputParameters);
    }

    public void saveTerm(T8OntologyTerm term, boolean deletionEnabled, boolean saveAbbreviations) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_TERM, term);
        inputParameters.put(PARAMETER_DELETION_ENABLED, deletionEnabled);
        inputParameters.put(PARAMETER_INCLUDE_ABBREVIATIONS, saveAbbreviations);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_SAVE_TERM, inputParameters);
    }

    public T8OntologyConcept retrieveConcept(String conceptId) throws Exception
    {
        return retrieveConcept(conceptId, true, true, true, true, true);
    }

    public T8OntologyConcept retrieveConcept(String conceptId, boolean includeOntologyLinks, boolean includeTerms, boolean includeAbbreviations, boolean includeDefinitions, boolean includeCodes) throws Exception
    {
        List<T8OntologyConcept> conceptList;

        conceptList = retrieveConcepts(ArrayLists.typeSafeList(conceptId), includeOntologyLinks, includeTerms, includeAbbreviations, includeDefinitions, includeCodes);
        return conceptList.size() > 0 ? conceptList.get(0) : null;
    }

    public List<T8OntologyConcept> retrieveConcepts(Collection<String> conceptIds, boolean includeOntologyLinks, boolean includeTerms, boolean includeAbbreviations, boolean includeDefinitions, boolean includeCodes) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_ID_LIST, conceptIds);
        inputParameters.put(PARAMETER_INCLUDE_ONTOLOGY_LINKS, includeOntologyLinks);
        inputParameters.put(PARAMETER_INCLUDE_TERMS, includeTerms);
        inputParameters.put(PARAMETER_INCLUDE_ABBREVIATIONS, includeAbbreviations);
        inputParameters.put(PARAMETER_INCLUDE_DEFINITIONS, includeDefinitions);
        inputParameters.put(PARAMETER_INCLUDE_CODES, includeCodes);
        return (List<T8OntologyConcept>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_CONCEPTS, inputParameters).get(PARAMETER_CONCEPT_LIST);
    }

    public List<T8OntologyLink> getOntologyLinks(String conceptId) throws Exception
    {
        return (List<T8OntologyLink>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_GET_ONTOLOGY_LINKS, HashMaps.createSingular(PARAMETER_CONCEPT_ID, conceptId)).get(PARAMETER_ONTOLOGY_LINKS);
    }
    public List<T8OntologyLink> getOntologyLinks(List<String> osIdList,String conceptId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_ID, conceptId);
        inputParameters.put(PARAMETER_OS_ID_LIST, osIdList);
        return (List<T8OntologyLink>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_GET_ONTOLOGY_LINKS, inputParameters).get(PARAMETER_ONTOLOGY_LINKS);
    }

    public void insertConceptRelation(String relationId, String tailConceptId, String headConceptId) throws Exception
    {
        List<T8ConceptPair> pairs;

        pairs = new ArrayList<>();
        pairs.add(new T8ConceptPair(relationId, tailConceptId, headConceptId));
        insertConceptRelations(pairs);
    }

    public void insertConceptRelations(List<T8ConceptPair> pairs) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_PAIRS, pairs);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_INSERT_CONCEPT_RELATIONS, inputParameters);
    }

    public void saveConceptRelation(String relationId, String tailConceptId, String headConceptId) throws Exception
    {
        List<T8ConceptPair> pairs;

        pairs = new ArrayList<>();
        pairs.add(new T8ConceptPair(relationId, tailConceptId, headConceptId));
        saveConceptRelations(pairs);
    }

    public void saveConceptRelations(List<T8ConceptPair> pairs) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_PAIRS, pairs);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_SAVE_CONCEPT_RELATIONS, inputParameters);
    }

    public void deleteConceptRelation(String relationId, String tailConceptId, String headConceptId) throws Exception
    {
        List<T8ConceptPair> pairs;

        pairs = new ArrayList<>();
        pairs.add(new T8ConceptPair(relationId, tailConceptId, headConceptId));
        deleteConceptRelations(pairs);
    }

    public void deleteConceptRelations(List<T8ConceptPair> pairs) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_PAIRS, pairs);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_DELETE_CONCEPT_RELATIONS, inputParameters);
    }

    public List<T8ConceptPair> validateConceptRelations(List<T8ConceptPair> pairs) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CONCEPT_PAIRS, pairs);
        return (List<T8ConceptPair>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_VALIDATE_CONCEPT_RELATIONS, inputParameters).get(PARAMETER_CONCEPT_PAIRS);
    }

    public List<T8ConceptPair> retrieveCrossConceptRelationPath(List<String> relationIds, String conceptId, String languageId, boolean headToTail, boolean includeOntology, boolean includeTerminology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_RELATION_IDS, relationIds);
        inputParameters.put(PARAMETER_CONCEPT_ID, conceptId);
        inputParameters.put(PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(PARAMETER_HEAD_TO_TAIL, headToTail);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, includeTerminology);
        return (List<T8ConceptPair>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ONT_RETRIEVE_CONCEPT_RELATION_PATH, inputParameters).get(PARAMETER_CONCEPT_PAIRS);
    }
}
