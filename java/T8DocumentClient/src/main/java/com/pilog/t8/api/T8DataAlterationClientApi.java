package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlterationImpact;
import com.pilog.t8.data.alteration.T8DataAlterationPackage;
import com.pilog.t8.data.alteration.T8DataAlterationPackageIndex;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataAlterationApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataAlterationClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_DATA_ALTERATION_CLIENT";

    public T8DataAlterationClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8DataAlterationClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    public T8DataAlterationImpact applyAlteration(T8DataAlteration alteration) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ALTERATION, alteration);
        return (T8DataAlterationImpact)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_APPLY_ALTERATION, inputParameters).get(PARAMETER_ALTERATION_IMPACT);
    }

    public T8DataAlterationPackageIndex insertAlteration(T8DataAlteration alteration) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ALTERATION, alteration);
        return (T8DataAlterationPackageIndex)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_INSERT_ALTERATION, inputParameters).get(PARAMETER_PACKAGE_INDEX);
    }

    public void insertAlterationPackage(T8DataAlterationPackage alterationPackage) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ALTERATION_PACKAGE, alterationPackage);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_INSERT_ALTERATION_PACKAGE, inputParameters);
    }

    public T8DataAlterationPackageIndex deleteAlteration(String packageIid, int step) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_PACKAGE_IID, packageIid);
        inputParameters.put(PARAMETER_STEP, step);
        return (T8DataAlterationPackageIndex)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_DELETE_ALTERATION, inputParameters).get(PARAMETER_PACKAGE_INDEX);
    }

    public void deleteAlterationPackage(String packageIid) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_PACKAGE_IID, packageIid);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_DELETE_ALTERATION_PACKAGE, inputParameters);
    }

    public T8DataAlterationPackageIndex getAlterationPackageIndex(String packageIid) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_PACKAGE_IID, packageIid);
        return (T8DataAlterationPackageIndex)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_GET_ALTERATION_PACKAGE_INDEX, inputParameters).get(PARAMETER_PACKAGE_INDEX);
    }

    public T8DataAlterationPackage retrieveAlterationPackage(String packageIid) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_PACKAGE_IID, packageIid);

       return (T8DataAlterationPackage) T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_RETRIEVE_ALTERATION_PACKAGE, inputParameters).get(PARAMETER_ALTERATION_PACKAGE);
    }

    public T8DataAlteration retrieveAlteration(String packageIid, int step) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_PACKAGE_IID, packageIid);
        inputParameters.put(PARAMETER_STEP, step);

       return (T8DataAlteration) T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_RETRIEVE_ALTERATION, inputParameters).get(PARAMETER_ALTERATION);
    }

    public void updateAlteration(T8DataAlteration alteration) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ALTERATION, alteration);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_UPDATE_ALTERATION, inputParameters);
    }

    public void updateAlterationPackage(T8DataAlterationPackage alterationPackage) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_ALTERATION_PACKAGE, alterationPackage);

        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ALT_UPDATE_ALTERATION_PACKAGE, inputParameters);
    }
}
