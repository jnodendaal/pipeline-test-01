package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datarecord.filter.T8HistoryFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8HistoryFilterCriterion.HistoryFilterOperator;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.utilities.components.datetimepicker.JXDateTimePicker;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.jdesktop.swingx.JXHyperlink;

/**
 * @author Bouwer du Preez
 */
public class HistoryFilterPanel extends JPanel
{
    private final HistoryFilterOperator resetOperator;
    private final T8DataRecordSearch parentSearch;
    private final FilterClauseComponent parentPanel;
    private JXDateTimePicker dateTimePicker;
    private JXHyperlink userLookupLink;
    private String agentID;
    private String agentDisplayString;

    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
    private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public HistoryFilterPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, String agentID, String agentDisplayString)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.agentID = agentID;
        this.agentDisplayString = agentDisplayString;
        initComponents();
        constructPanel(null);
        for (Object item : HistoryFilterOperator.values()) jComboBoxOperator.addItem(item);
        jComboBoxOperator.setSelectedItem(HistoryFilterOperator.UPDATED);
        this.resetOperator = HistoryFilterOperator.UPDATED;
        jComboBoxOperator.setRenderer(new ComboCellRenderer());
    }

    public HistoryFilterPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, T8HistoryFilterCriterion filter)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.agentID = filter.getAgentId();
        if (agentID != null) this.agentDisplayString = parentSearch.getAgentDisplayString(agentID);
        else this.agentDisplayString = null;
        initComponents();
        constructPanel(filter.getFilterTime());
        for (Object item : HistoryFilterOperator.values()) jComboBoxOperator.addItem(item);
        jComboBoxOperator.setSelectedItem(filter.getOperator());
        this.resetOperator = filter.getOperator();
        jComboBoxOperator.setRenderer(new ComboCellRenderer());
    }

    private void constructPanel(Long filterTime)
    {
        // Create the agent lookup.
        userLookupLink = new JXHyperlink(new AbstractAction(agentID != null ? agentDisplayString : "Any User")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                lookupUser();
            }
        });
        jPanelUserLookup.add(userLookupLink, BorderLayout.CENTER);

        // Create the date picker.
        dateTimePicker = new JXDateTimePicker();
        dateTimePicker.getEditor().setBorder(UIManager.getBorder("TextField.border"));
        dateTimePicker.setFormats(DEFAULT_DATE_TIME_FORMAT);
        dateTimePicker.setTimeFormat(new SimpleDateFormat(DEFAULT_TIME_FORMAT));
        if (filterTime != null) dateTimePicker.setDate(new Date(filterTime));
        jPanelValue.add(dateTimePicker, BorderLayout.CENTER);
    }

    private void lookupUser()
    {
        Pair<String, String> selectedUser;

        selectedUser = parentSearch.getSelectedUser();
        if (selectedUser != null)
        {
            agentID = selectedUser.getValue1();
            agentDisplayString = selectedUser.getValue2();
            userLookupLink.setText(agentID != null ? agentDisplayString : "Any User");
        }
    }

    private void close()
    {
        parentPanel.removeHistory(this);
    }

    public LayoutColumnWidths getLayoutColumnWidths()
    {
        LayoutColumnWidths widths;

        widths = new LayoutColumnWidths();
        widths.setWidth(0, jLabelUser.getPreferredSize().width);
        widths.setWidth(1, userLookupLink.getPreferredSize().width);
        widths.setWidth(2, jComboBoxOperator.getPreferredSize().width);
        return widths;
    }

    public void setLayoutColumnWidths(LayoutColumnWidths widths)
    {
        Dimension size;

        size = jLabelUser.getMinimumSize();
        size.width = widths.getWidth(0);
        jLabelUser.setMinimumSize(size);
        jLabelUser.setPreferredSize(size);

        size = jPanelUserLookup.getMinimumSize();
        size.width = widths.getWidth(1);
        jPanelUserLookup.setMinimumSize(size);
        jPanelUserLookup.setPreferredSize(size);

        revalidate();
    }

    public T8HistoryFilterCriterion getHistoryFilterCriterion()
    {
        T8HistoryFilterCriterion historyCriterion;
        Date date;

        try
        {
            dateTimePicker.commitEdit();
            date = dateTimePicker.getDate();
        }
        catch (ParseException e)
        {
            T8Log.log("Exception while parsing input timestamp.", e);
            date = null;
        }

        historyCriterion = new T8HistoryFilterCriterion(agentID);
        historyCriterion.setFilterTime(date != null ? date.getTime() : null);
        historyCriterion.setOperator((HistoryFilterOperator)jComboBoxOperator.getSelectedItem());
        return historyCriterion;
    }

    private String translate(String inputString)
    {
        return inputString;
    }

    public void clearFilterValue()
    {
        dateTimePicker.setDate(null);
    }

    public void resetFilterOperator()
    {
        jComboBoxOperator.setSelectedItem(this.resetOperator);
    }

    private class ComboCellRenderer extends DefaultListCellRenderer
    {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            HistoryFilterOperator operator;

            operator = (HistoryFilterOperator)value;
            return super.getListCellRendererComponent(list, translate(operator.getDisplayString()), index, isSelected, cellHasFocus);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelUser = new javax.swing.JLabel();
        jPanelUserLookup = new javax.swing.JPanel();
        jComboBoxOperator = new javax.swing.JComboBox();
        jPanelValue = new javax.swing.JPanel();
        jToolBarControls = new javax.swing.JToolBar();
        jButtonClose = new javax.swing.JButton();

        setBackground(new java.awt.Color(234, 234, 255));
        setLayout(new java.awt.GridBagLayout());

        jLabelUser.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelUser.setText("User:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jLabelUser, gridBagConstraints);

        jPanelUserLookup.setOpaque(false);
        jPanelUserLookup.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jPanelUserLookup, gridBagConstraints);

        jComboBoxOperator.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxOperatorItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jComboBoxOperator, gridBagConstraints);

        jPanelValue.setOpaque(false);
        jPanelValue.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jPanelValue, gridBagConstraints);

        jToolBarControls.setFloatable(false);
        jToolBarControls.setRollover(true);
        jToolBarControls.setOpaque(false);

        jButtonClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/cross-white.png"))); // NOI18N
        jButtonClose.setFocusable(false);
        jButtonClose.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonClose.setOpaque(false);
        jButtonClose.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseActionPerformed(evt);
            }
        });
        jToolBarControls.add(jButtonClose);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        add(jToolBarControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        close();
    }//GEN-LAST:event_jButtonCloseActionPerformed

    private void jComboBoxOperatorItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxOperatorItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxOperatorItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            HistoryFilterOperator operator;

            operator = (HistoryFilterOperator)evt.getItem();
            if (operator == HistoryFilterOperator.ADDED)
            {
                dateTimePicker.setEditable(false);
            }
            else if (operator == HistoryFilterOperator.UPDATED)
            {
                dateTimePicker.setEditable(false);
            }
            else
            {
                dateTimePicker.setEditable(true);
            }
        }
    }//GEN-LAST:event_jComboBoxOperatorItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JComboBox jComboBoxOperator;
    private javax.swing.JLabel jLabelUser;
    private javax.swing.JPanel jPanelUserLookup;
    private javax.swing.JPanel jPanelValue;
    private javax.swing.JToolBar jToolBarControls;
    // End of variables declaration//GEN-END:variables
}
