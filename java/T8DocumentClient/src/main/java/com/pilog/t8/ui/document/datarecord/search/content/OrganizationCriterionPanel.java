package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.filter.T8OrganizationFilterCriterion;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 * @author Pieter Strydom
 */
public class OrganizationCriterionPanel extends JPanel
{
    private final T8DataRecordSearch parentSearch;
    private final T8ComponentController controller;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8ConfigurationManager configurationManager;
    private final FilterClauseComponent parentPanel;
    private final List<String> organizationTypeIdList;
    private final DataFilterOperator resetOperator;
    private final String componentLabel;
    private List<String> orgIdList;
    private List<String> orgTermList;

    public OrganizationCriterionPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, List<String> organizationTypeIdList, String label)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.organizationTypeIdList = organizationTypeIdList;
        this.controller = parentSearch.getController();
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.configurationManager = clientContext.getConfigurationManager();
        this.componentLabel = label;
        initComponents();
        constructPanel();
        createValueEditors(null);
        for (Object item : DataFilterOperator.values()) jComboBoxOperator.addItem(item);
        jComboBoxOperator.setSelectedItem(DataFilterOperator.EQUAL);
        this.resetOperator = DataFilterOperator.EQUAL;
        jComboBoxOperator.setRenderer(new ComboCellRenderer());
    }

    public OrganizationCriterionPanel(T8DataRecordSearch parentSearch, FilterClauseComponent parentPanel, T8OrganizationFilterCriterion filter)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.organizationTypeIdList = filter.getOrganizationTypeIdList();
        this.controller = parentSearch.getController();
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.configurationManager = clientContext.getConfigurationManager();
        this.componentLabel = filter.getLabel();
        initComponents();
        constructPanel();
        createValueEditors(filter.getFilterValue());
        for (Object item : DataFilterOperator.values()) jComboBoxOperator.addItem(item);
        jComboBoxOperator.setSelectedItem(filter.getOperator());
        this.resetOperator = filter.getOperator();
        jComboBoxOperator.setRenderer(new ComboCellRenderer());
    }

    public void clearFilterValue()
    {
        jFormattedTextFieldDisplay.setText(null);
        this.orgIdList = null;
        this.orgTermList = null;
    }

    public void resetFilterOperator()
    {
        jComboBoxOperator.setSelectedItem(this.resetOperator);
    }

    private void constructPanel()
    {
        if (!Strings.isNullOrEmpty(componentLabel))
        {
            jLabelOrgType.setText(componentLabel);
        }
    }

    private void createValueEditors(Object filterValue)
    {
        if (filterValue instanceof List)
        {
            List<String> filterValues;
            StringBuilder displayString;

            filterValues = (List<String>)filterValue;
            displayString = new StringBuilder();

            for(int valueIndex = 0; valueIndex < filterValues.size(); valueIndex++)
            {
                String value;

                value = filterValues.get(valueIndex);

                if ((value instanceof String) && (T8IdentifierUtilities.isConceptID((String)value)))
                {
                    TerminologyProvider terminologyProvider;
                    String valueTerm;

                    if (orgIdList == null) this.orgIdList = new ArrayList<>();
                    terminologyProvider = parentSearch.getTerminologyProvider();
                    this.orgIdList.add(value);

                    valueTerm = terminologyProvider.getTerm(null, value);

                    if (valueIndex == 0) displayString.append(valueTerm);
                    else displayString.append(("," + valueTerm));
                }
                else
                {
                    if (orgTermList == null) this.orgTermList = new ArrayList<>();
                    this.orgTermList.add(value);
                    if (valueIndex == 0) displayString.append(value);
                    else displayString.append(("," + value));
                }
            }

            jFormattedTextFieldDisplay.setText(displayString.toString());
        }
        else
        {
            String valueString;

            valueString = filterValue != null ? filterValue.toString() : null;
            if (Strings.trimToNull(valueString) == null)
            {
                jFormattedTextFieldDisplay.setText(null);
            }
            else
            {
                jFormattedTextFieldDisplay.setText(valueString);
            }
        }
    }

    public T8OrganizationFilterCriterion getOrganizationFilterCriterion()
    {
        T8OrganizationFilterCriterion criterion;
        DataFilterOperator operator;

        criterion = new T8OrganizationFilterCriterion(organizationTypeIdList, componentLabel);
        operator = (DataFilterOperator)jComboBoxOperator.getSelectedItem();
        criterion.setFilterValue(getValue(operator));
        criterion.setOperator(operator);

        return criterion;
    }

    private Object getValue(DataFilterOperator operator)
    {
        if (operator == DataFilterOperator.IS_NULL || operator == DataFilterOperator.IS_NOT_NULL) return new ArrayList<>();
        else if (orgIdList != null && orgIdList.size() > 0) return orgIdList;
        else return orgTermList;
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public LayoutColumnWidths getLayoutColumnWidths()
    {
        LayoutColumnWidths widths;

        widths = new LayoutColumnWidths();
        widths.setWidth(0, jLabelOrganization.getPreferredSize().width);
        widths.setWidth(1, jLabelOrgType.getPreferredSize().width);
        widths.setWidth(2, jComboBoxOperator.getPreferredSize().width);
        widths.setWidth(3, jPanelValue.getPreferredSize().width);
        return widths;
    }

    public void setLayoutColumnWidths(LayoutColumnWidths widths)
    {
        Dimension size;

        size = jLabelOrganization.getMinimumSize();
        size.width = widths.getWidth(0);
        jLabelOrganization.setMinimumSize(size);
        jLabelOrganization.setPreferredSize(size);

        size = jLabelOrgType.getMinimumSize();
        size.width = widths.getWidth(1);
        jLabelOrgType.setMinimumSize(size);
        jLabelOrgType.setPreferredSize(size);

        size = jComboBoxOperator.getMinimumSize();
        size.width = widths.getWidth(2);
        jComboBoxOperator.setMinimumSize(size);
        jComboBoxOperator.setPreferredSize(size);

        size = jPanelValue.getMinimumSize();
        size.width = widths.getWidth(3);
        jPanelValue.setMinimumSize(size);
        jPanelValue.setPreferredSize(size);

        revalidate();
    }

    private void lookup()
    {
        List<Map<String, Object>> orgIdMapList;
        StringBuilder textString;
        List<String> orgIdList;

        textString = new StringBuilder();
        orgIdList = new ArrayList<>();
        orgIdMapList = parentSearch.getSelectedOrganization(organizationTypeIdList);

        if (orgIdMapList != null)
        {
            for (int counter = 0; counter < orgIdMapList.size(); counter++)
            {
                Map<String, Object> orgIdMap;
                String orgId;
                String orgTerm;

                orgIdMap = orgIdMapList.get(counter);
                orgId = (String)orgIdMap.get("ORG_ID");
                orgTerm = (String)orgIdMap.get("ORG_TERM");

                orgIdList.add(orgId);

                if (counter == (orgIdMapList.size()-1))
                {
                    textString.append(orgTerm);
                }
                else
                {
                    textString.append(orgTerm);
                    textString.append(",");
                }
            }

            this.orgIdList = orgIdList;
            this.orgTermList = null;

            jFormattedTextFieldDisplay.setText(textString.toString());
        }
    }

    /** This method will set the correct variables when text is typed into
     * the lookup and not selected.
     */
    private void setTextTyped()
    {
        List<String> orgTermList;
        List<String> orgTrimmedList;
        String typedText;

        orgTermList = new ArrayList<>();
        orgTrimmedList = new ArrayList<>();
        typedText = jFormattedTextFieldDisplay.getText();

        orgTermList = Arrays.asList(typedText.split(","));

        for (String orgTerm : orgTermList)
        {
            orgTrimmedList.add(orgTerm.trim());
        }

        this.orgTermList = orgTrimmedList;
        this.orgIdList = null;
    }

    private class ComboCellRenderer extends DefaultListCellRenderer
    {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            DataFilterOperator operator;

            operator = (DataFilterOperator)value;
            return super.getListCellRendererComponent(list, translate(operator.getDisplayString()), index, isSelected, cellHasFocus);
        }
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupValue = new javax.swing.ButtonGroup();
        jLabelOrganization = new javax.swing.JLabel();
        jLabelOrgType = new javax.swing.JLabel();
        jComboBoxOperator = new javax.swing.JComboBox();
        jPanelValue = new javax.swing.JPanel();
        jFormattedTextFieldDisplay = new javax.swing.JFormattedTextField();
        jButtonLookup = new javax.swing.JButton();
        jButtonClear = new javax.swing.JButton();

        setBackground(new java.awt.Color(234, 234, 255));
        setMaximumSize(null);
        setMinimumSize(null);
        setPreferredSize(null);
        setLayout(new java.awt.GridBagLayout());

        jLabelOrganization.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelOrganization.setText("Organization:");
        jLabelOrganization.setMaximumSize(null);
        jLabelOrganization.setMinimumSize(null);
        jLabelOrganization.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        add(jLabelOrganization, gridBagConstraints);

        jLabelOrgType.setAlignmentX(0.5F);
        jLabelOrgType.setDoubleBuffered(true);
        jLabelOrgType.setMaximumSize(null);
        jLabelOrgType.setMinimumSize(null);
        jLabelOrgType.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jLabelOrgType, gridBagConstraints);

        jComboBoxOperator.setMaximumSize(null);
        jComboBoxOperator.setMinimumSize(null);
        jComboBoxOperator.setPreferredSize(null);
        jComboBoxOperator.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxOperatorItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        add(jComboBoxOperator, gridBagConstraints);

        jPanelValue.setBackground(new java.awt.Color(234, 234, 255));
        jPanelValue.setLayout(new java.awt.GridBagLayout());

        jFormattedTextFieldDisplay.setMaximumSize(null);
        jFormattedTextFieldDisplay.setMinimumSize(null);
        jFormattedTextFieldDisplay.setPreferredSize(null);
        jFormattedTextFieldDisplay.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jFormattedTextFieldDisplayKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelValue.add(jFormattedTextFieldDisplay, gridBagConstraints);

        jButtonLookup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/searchIcon.png"))); // NOI18N
        jButtonLookup.setToolTipText("Search for Data Value");
        jButtonLookup.setFocusable(false);
        jButtonLookup.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonLookup.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonLookup.setMaximumSize(null);
        jButtonLookup.setMinimumSize(null);
        jButtonLookup.setPreferredSize(null);
        jButtonLookup.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonLookup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLookupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelValue.add(jButtonLookup, gridBagConstraints);

        jButtonClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"))); // NOI18N
        jButtonClear.setToolTipText("Clear Data Value");
        jButtonClear.setFocusable(false);
        jButtonClear.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonClear.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonClear.setMaximumSize(null);
        jButtonClear.setMinimumSize(null);
        jButtonClear.setPreferredSize(null);
        jButtonClear.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelValue.add(jButtonClear, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 10);
        add(jPanelValue, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLookupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLookupActionPerformed
        lookup();
    }//GEN-LAST:event_jButtonLookupActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearActionPerformed
        clearFilterValue();
    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jFormattedTextFieldDisplayKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedTextFieldDisplayKeyReleased
        setTextTyped();
    }//GEN-LAST:event_jFormattedTextFieldDisplayKeyReleased

    private void jComboBoxOperatorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxOperatorItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            DataFilterOperator operator;

            operator = (DataFilterOperator)evt.getItem();
            if (operator == DataFilterOperator.IS_NULL)
            {
                jFormattedTextFieldDisplay.setEditable(false);
                jButtonLookup.setEnabled(false);
                jButtonClear.setEnabled(false);
            }
            else if (operator == DataFilterOperator.IS_NOT_NULL)
            {
                jFormattedTextFieldDisplay.setEditable(false);
                jButtonLookup.setEnabled(false);
                jButtonClear.setEnabled(false);
            }
            else
            {
                jFormattedTextFieldDisplay.setEditable(true);
                jButtonLookup.setEnabled(true);
                jButtonClear.setEnabled(true);
            }
        }
    }//GEN-LAST:event_jComboBoxOperatorItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupValue;
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonLookup;
    private javax.swing.JComboBox jComboBoxOperator;
    private javax.swing.JFormattedTextField jFormattedTextFieldDisplay;
    private javax.swing.JLabel jLabelOrgType;
    private javax.swing.JLabel jLabelOrganization;
    private javax.swing.JPanel jPanelValue;
    // End of variables declaration//GEN-END:variables
}
