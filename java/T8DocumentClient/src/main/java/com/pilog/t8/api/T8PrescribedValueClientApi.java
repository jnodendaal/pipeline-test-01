package com.pilog.t8.api;

import com.pilog.epic.annotation.EPICClass;
import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.PrescribedValue;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8PrescribedValueApiResource.*;

/**
 * @author Andre Scheepers
 */
@EPICClass(PreferredVariableName = "pvAPI", Description = "Prescribed Value API", VariableCreationTemplate = "pvAPI", onlyShowDeclaredMethods = true)
public class T8PrescribedValueClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_PRESCRIBED_VALUE_CLIENT";

    public T8PrescribedValueClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8PrescribedValueClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    /**
     * Retrieves a set of suggested values based on the {@code ValueRequirement}
     * and search prefix string which is specified. The suggestions are returned
     * in a {@code RecordValue} format which enables using the actual object,
     * rather than only the {@code String} value.
     *
     * @param valueRequirement The {@code ValueRequirement} which defines the
     *      value type
     * @param searchPrefx The {@code String} which serves as a prefix for the
     *      suggested value search
     *
     * @return The set of {@code RecordValue} objects which define the set of
     *      suggested values
     *
     * @throws Exception If there is any error during the retrieval of the set
     *      of suggested values
     */
    public List<RecordValue> retrievePrescribedValueSuggestions(ValueRequirement valueRequirement, String searchPrefx) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_VALUE_REQUIREMENT, valueRequirement);
        inputParameters.put(PARAMETER_SEARCH_PREFIX, searchPrefx);
        return (List<RecordValue>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_RETRIEVE_PRESCRIBED_VALUE_SUGGESTIONS, inputParameters).get(PARAMETER_RECORD_VALUES);
    }

    /**
     * Replaces all references of standard values, as defined by the standard
     * values reference filter with the specified standard value identifier.
     * The filter essentially defines the where clause for the standard value
     * references to be updated with the new standard value ID.
     *
     * @param prescribedValueIDList The prescribed value ID list used to
     *      identify the prescribed values to be replaced.
     * @param replacementValueID The {@code String} prescribed value ID
     *      to be used as the replacement ID
     *
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified standard value ID
     *
     * @throws Exception If there is any error while updating the standard value
     *      references with the new value
     */
    public Integer replacePrescribedValueReferences(List<String> prescribedValueIDList, String replacementValueID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_VALUE_ID_LIST, prescribedValueIDList);
        inputParameters.put(PARAMETER_REPLACEMENT_VALUE_ID, replacementValueID);
        return (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_REPLACE_PRESCRIBED_VALUE_REFERENCES, inputParameters).getOrDefault(PARAMETER_REFERENCE_UPDATE_COUNT, 0);
    }

    /**
     * Replaces all references of standard values, as defined by the standard
     * values reference filter with the specified standard value identifier.
     * The filter essentially defines the where clause for the standard value
     * references to be updated with the new standard value ID.
     *
     * @param prescribedValueIDList The prescribed value ID list used to
     *      identify the prescribed values to be replaced.
     * @param replacementValueID The {@code String} prescribed value ID
     *      to be used as the replacement ID
     *
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified standard value ID
     *
     * @throws Exception If there is any error while updating the standard value
     *      references with the new value
     */
    public Integer replacePrescribedValues(List<String> prescribedValueIDList, String replacementValueID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_VALUE_ID_LIST, prescribedValueIDList);
        inputParameters.put(PARAMETER_REPLACEMENT_VALUE_ID, replacementValueID);
        return (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_REPLACE_PRESCRIBED_VALUES, inputParameters).getOrDefault(PARAMETER_REFERENCE_UPDATE_COUNT, 0);
    }

    public void savePrescribedValue(PrescribedValue prescribedValue) throws Exception
    {
        savePrescribedValues(ArrayLists.newArrayList(prescribedValue));
    }

    public void insertPrescribedValue(PrescribedValue prescribedValue) throws Exception
    {
        insertPrescribedValues(ArrayLists.newArrayList(prescribedValue));
    }

    public void updatePrescribedValue(PrescribedValue prescribedValue) throws Exception
    {
        updatePrescribedValues(ArrayLists.newArrayList(prescribedValue));
    }

    /**
     * Saves the specified data requirement value. If the value already exists,
     * it is simply updated. Otherwise a new entry is created.
     * @param prescribedValues The list of {@code PrescribedValue} objects to be saved.
     *
     * @throws Exception If there is an error during the update or insertion of
     *      the data requirement value
     */
    public void savePrescribedValues(List<PrescribedValue> prescribedValues) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_SAVE_PRESCRIBED_VALUES, HashMaps.createSingular(PARAMETER_PRESCRIBED_VALUE_LIST, prescribedValues));
    }

    public void insertPrescribedValues(List<PrescribedValue> prescribedValues) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_INSERT_PRESCRIBED_VALUES, HashMaps.createSingular(PARAMETER_PRESCRIBED_VALUE_LIST, prescribedValues));
    }

    public void updatePrescribedValues(List<PrescribedValue> prescribedValues) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_UPDATE_PRESCRIBED_VALUES, HashMaps.createSingular(PARAMETER_PRESCRIBED_VALUE_LIST, prescribedValues));
    }

    /**
     * Removes the specified prescribed values. If the value is in use, it is
     * not deleted.
     * The value ID of a existing prescribed value, that is not in use,
     * needs to be passed.
     *
     * @param svIdentifiersList A list of the Prescribed Value ID's that are to
     *      be deleted.
     * @throws Exception If there is an error during the deletion of the data
     *      requirement value
     */
    public void deletePrescribedValues(List svIdentifiersList) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_VALUE_ID_LIST, svIdentifiersList);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_DELETE_PRESCRIBED_VALUES, inputParameters);
    }

    /**
     * Sets the standardized flag for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.
     *
     * @param standardValueID The {@code String} ID of the standard value to be
     *      approved
     * @param standardIndicator {@code true} to flag as standardized. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the standardized flag
     *      for the specified value
     */
    public void setStandard(String standardValueID, boolean standardIndicator) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_VALUE_ID, standardValueID);
        inputParameters.put(PARAMETER_STANDARD_INDICATOR, standardIndicator);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_SET_STANDARD, inputParameters);
    }

    /**
     * Sets the approved flag for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.
     *
     * @param standardValueID The {@code String} ID of the standard value to be
     *      approved
     * @param approvedIndicator {@code true} to flag as standardized. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the standardized flag
     *      for the specified value
     */
    public void setApproved(String standardValueID, boolean approvedIndicator) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_VALUE_ID, standardValueID);
        inputParameters.put(PARAMETER_APPROVED_INDICATOR, approvedIndicator);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_PV_SET_APPROVED, inputParameters);
    }
}
