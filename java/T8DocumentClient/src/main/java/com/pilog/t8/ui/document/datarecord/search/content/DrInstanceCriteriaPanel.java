package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.data.document.datarecord.filter.T8CodeFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8CodeFilterCriterion.CodeCriterionTarget;
import com.pilog.t8.data.document.datarecord.filter.T8DrInstanceFilterCriteria;
import com.pilog.t8.data.document.datarecord.filter.T8HistoryFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8OrganizationFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8PropertyFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterClause;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * @author Bouwer du Preez
 */
public class DrInstanceCriteriaPanel extends JPanel implements FilterClauseComponent
{
    private final T8DataRecordSearch parentSearch;
    private final SubCriteriaPanel parentPanel;
    private final List<PropertyCriterionPanel> propertyComponents;
    private final List<HistoryFilterPanel> historyComponents;
    private final List<CodeCriterionPanel> codeComponents;
    private final List<OrganizationCriterionPanel> organizationComponents;
    private final String drInsanceId;
    private final String drInstanceTerm;
    private final String displayName;
    private final boolean allowPropertyAddition;
    private final boolean allowHistoryAddition;
    private final boolean allowCodeAddition;
    private final boolean allowRemoval;

    public DrInstanceCriteriaPanel(T8DataRecordSearch parentSearch, SubCriteriaPanel criteriaPanel, String drInstanceId, String drInstanceTerm)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = criteriaPanel;
        this.propertyComponents = new ArrayList<>();
        this.historyComponents = new ArrayList<>();
        this.codeComponents = new ArrayList<>();
        this.organizationComponents = new ArrayList<>();
        this.drInsanceId = drInstanceId;
        this.displayName = null;
        this.drInstanceTerm = drInstanceTerm;
        this.allowPropertyAddition = true;
        this.allowHistoryAddition = true;
        this.allowCodeAddition = true;
        this.allowRemoval = true;
        initComponents();
        setBorder();
        rebuildLayout();
    }

    public DrInstanceCriteriaPanel(T8DataRecordSearch parentSearch, SubCriteriaPanel parentPanel, T8DrInstanceFilterCriteria filterCriteria)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.propertyComponents = new ArrayList<>();
        this.historyComponents = new ArrayList<>();
        this.codeComponents = new ArrayList<>();
        this.organizationComponents = new ArrayList<>();
        this.drInsanceId = filterCriteria.getDrInstanceId();
        this.displayName = filterCriteria.getDisplayName();
        this.allowPropertyAddition = filterCriteria.isAllowPropertyAddition();
        this.allowHistoryAddition = filterCriteria.isAllowHistoryAddition();
        this.allowCodeAddition = filterCriteria.isAllowCodeAddition();
        this.allowRemoval = filterCriteria.isAllowRemoval();
        if (drInsanceId != null) this.drInstanceTerm = parentSearch.getTerminologyProvider().getTerm(null, drInsanceId);
        else this.drInstanceTerm = null;
        initComponents();
        setBorder();
        setFilterCriteria(filterCriteria);
    }

    private void setFilterCriteria(T8DrInstanceFilterCriteria filterCriteria)
    {
        for (T8PropertyFilterCriterion propertyFilter : filterCriteria.getPropertyCriteria())
        {
            propertyComponents.add(new PropertyCriterionPanel(parentSearch, this, propertyFilter));
        }

        for (T8HistoryFilterCriterion historyFilter : filterCriteria.getHistoryCriteria())
        {
            historyComponents.add(new HistoryFilterPanel(parentSearch, this, historyFilter));
        }

        for (T8CodeFilterCriterion codeFilter : filterCriteria.getCodeCriteria())
        {
            codeComponents.add(new CodeCriterionPanel(parentSearch, this, codeFilter));
        }

        for (T8OrganizationFilterCriterion organizationFilter : filterCriteria.getOrganizationCriteria())
        {
            organizationComponents.add(new OrganizationCriterionPanel(parentSearch, this, organizationFilter));
        }

        rebuildLayout();
    }

    public String getDrInstanceId()
    {
        return drInsanceId;
    }

    private void setBorder()
    {
        String title;

        title = (drInstanceTerm != null) ? drInstanceTerm : (displayName != null) ? displayName : "All Record Types";
        setBorder(BorderFactory.createTitledBorder(null, title, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", 1, 13), new Color(0, 0, 0)));
    }

    private void rebuildLayout()
    {
        DrInstanceControlPanel optionPanel;
        GridBagConstraints constraints;
        int row;

        // Clear the panel.
        row = 0;
        removeAll();

        // Add all organization filter panels.
        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            // Add the code panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(organizationComponent, constraints);
            organizationComponent.setVisible(row > 0);
        }

        // Add all code filter panels.
        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            // Add the code panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(codeComponent, constraints);
            codeComponent.setVisible(row > 0);
        }

        // Add all property filter panels.
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            // Add the property panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(propertyComponent, constraints);
            propertyComponent.setVisible(row > 0);
        }

        // Add all history filter panels.
        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            // Add the history panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(historyComponent, constraints);
            historyComponent.setVisible(row > 0);
        }

        // Add the control panel (so the user can add a new clause).
        if (requiresControlPanel())
        {
            optionPanel = new DrInstanceControlPanel(parentSearch, this, allowPropertyAddition, allowHistoryAddition, allowRemoval);
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row;
            constraints.gridwidth = 2;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.NONE;
            constraints.anchor = GridBagConstraints.WEST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(optionPanel, constraints);

            if (!parentSearch.isAdvancedOptionsEnabled())
            {
                optionPanel.setVisible(false);
            }
        }

        revalidate();
        repaint();
    }

    private boolean requiresControlPanel()
    {
        if (allowPropertyAddition) return true;
        else if (allowHistoryAddition) return true;
        else if (allowCodeAddition) return true;
        else if (allowRemoval) return true;
        else return false;
    }

    public LayoutColumnWidths getLayoutColumnWidths()
    {
        LayoutColumnWidths maxWidths;

        maxWidths = new LayoutColumnWidths();
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            maxWidths.updateToMax(propertyComponent.getLayoutColumnWidths());
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            maxWidths.updateToMax(historyComponent.getLayoutColumnWidths());
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            maxWidths.updateToMax(codeComponent.getLayoutColumnWidths());
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            maxWidths.updateToMax(organizationComponent.getLayoutColumnWidths());
        }

        return maxWidths;
    }

    public void setLinkWidths(LayoutColumnWidths widths)
    {
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            propertyComponent.setLayoutColumnWidths(widths);
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            historyComponent.setLayoutColumnWidths(widths);
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            codeComponent.setLayoutColumnWidths(widths);
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            organizationComponent.setLayoutColumnWidths(widths);
        }

        revalidate();
    }

    public void setAdvancedOptionsVisible(boolean setVisible)
    {
        for (Component component : getComponents())
        {
            if(component instanceof DrInstanceControlPanel)
            {
                component.setVisible(setVisible);
            }
        }
    }

    @Override
    public void clearFilterValues()
    {
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            propertyComponent.clearFilterValue();
            propertyComponent.resetFilterOperator();
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            historyComponent.clearFilterValue();
            historyComponent.resetFilterOperator();
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            codeComponent.clearFilterValue();
            codeComponent.resetFilterOperator();
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            organizationComponent.clearFilterValue();
            organizationComponent.resetFilterOperator();
        }
    }

    @Override
    public T8RecordFilterClause getFilterClause()
    {
        T8DrInstanceFilterCriteria filterClause;

        filterClause = new T8DrInstanceFilterCriteria(drInsanceId);
        filterClause.setAllowCodeAddition(allowCodeAddition);
        filterClause.setAllowHistoryAddition(allowHistoryAddition);
        filterClause.setAllowPropertyAddition(allowPropertyAddition);
        
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            filterClause.addPropertyCriterion(propertyComponent.getPropertyFilterCriterion());
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            filterClause.addHistoryCriterion(historyComponent.getHistoryFilterCriterion());
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            filterClause.addCodeCriterion(codeComponent.getCodeFilterCriterion());
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            filterClause.addOrganizationCriterion(organizationComponent.getOrganizationFilterCriterion());
        }

        return filterClause;
    }

    public List<String> getPropertyIdList()
    {
        List<String> idList;

        idList = new ArrayList<>();
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            String id;

            id = propertyComponent.getPropertyId();
            if (id != null) idList.add(id);
        }

        return idList;
    }

    public boolean containsProperty(String propertyId)
    {
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            String id;

            id = propertyComponent.getPropertyId();
            if (Objects.equals(id, propertyId)) return true;
        }

        return false;
    }

    public void addProperty(String propertyId, String propertyTerm, RequirementType dataType)
    {
        if (!containsProperty(propertyId))
        {
            propertyComponents.add(new PropertyCriterionPanel(parentSearch, this, propertyId, propertyTerm, null, null, dataType));
            rebuildLayout();
        }
    }

    public void addProperty(T8PropertyFilterCriterion criterion)
    {
        propertyComponents.add(new PropertyCriterionPanel(parentSearch, this, criterion));
        rebuildLayout();
    }

    @Override
    public void removeProperty(PropertyCriterionPanel propertyFilterPanel)
    {
        propertyComponents.remove(propertyFilterPanel);
        rebuildLayout();
    }

    public void addHistory(String userID, String userDisplayString)
    {
        historyComponents.add(new HistoryFilterPanel(parentSearch, this, userID, userDisplayString));
        rebuildLayout();
    }

    @Override
    public void removeHistory(HistoryFilterPanel historyFilterPanel)
    {
        historyComponents.remove(historyFilterPanel);
        rebuildLayout();
    }

    public void addCode(CodeCriterionTarget target, String codeTypeId, String codeTypeTerm)
    {
        codeComponents.add(new CodeCriterionPanel(parentSearch, this, target, codeTypeId, codeTypeTerm));
        rebuildLayout();
    }

    @Override
    public void removeCode(CodeCriterionPanel codeCriterionPanel)
    {
        codeComponents.remove(codeCriterionPanel);
        rebuildLayout();
    }

    public void addOrganization(List<String> organizationTypeIdList, String label)
    {
        organizationComponents.add(new OrganizationCriterionPanel(parentSearch, this, organizationTypeIdList, label));
        rebuildLayout();
    }

    public void removeOrganization(OrganizationCriterionPanel organizationCriterionPanel)
    {
        organizationComponents.remove(organizationCriterionPanel);
        rebuildLayout();
    }

    public void removeFilter()
    {
        parentPanel.removeClause(this);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setBackground(new java.awt.Color(234, 234, 255));
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
