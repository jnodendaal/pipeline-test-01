package com.pilog.t8.ui.document.datarequirement;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 * @author Bouwer du Preez
 */
public class ValueRequirementListCellRenderer extends DefaultListCellRenderer
{
    private static final T8Logger logger = T8Log.getLogger(ValueRequirementListCellRenderer.class);

    private final TerminologyProvider terminologyProvider;

    public ValueRequirementListCellRenderer(TerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        ValueRequirement valueRequirement;

        valueRequirement = (ValueRequirement)value;
        if (valueRequirement != null)
        {
            String conceptID;
            String term;
            String code;
            String displayString;

            // Get the concept ID to render.
            conceptID = valueRequirement.getValue();

            // Get the term and definition for the value concept and construct a display String.
            try
            {
                term = terminologyProvider.getTerm(null, conceptID);
                code = terminologyProvider.getCode(conceptID);
            }
            catch (Exception e)
            {
                logger.log("Exception while retrieving concept terminology: " + conceptID, e);
                term = null;
                code = null;
            }

            // iF we have a term, render it.
            if (term != null)
            {
                // If the code is available, append it to the term.
                if (!Strings.isNullOrEmpty(code))
                {
                    displayString = term + " (" + code + ")";

                    // Make sure the display value is not too long.  If it is, use the term only.
                    if (displayString.length() > 100) displayString = term;
                }
                else displayString = term;
            }
            else
            {
                displayString = "No Term Found";
            }

            // Return the renderer.
            return super.getListCellRendererComponent(list, displayString, index, isSelected, cellHasFocus);
        }
        else
        {
            return super.getListCellRendererComponent(list, " ", index, isSelected, cellHasFocus);
        }
    }
}
