package com.pilog.t8.ui.document.datarecord.search.content;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.filter.T8CodeFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8DrFilterCriteria;
import com.pilog.t8.data.document.datarecord.filter.T8HistoryFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8OrganizationFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8PropertyFilterCriterion;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterClause;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.document.datarecord.search.T8DataRecordSearch;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Pieter Strydom
 */
public class DrCriteriaPanel extends JPanel implements FilterClauseComponent
{
    private final T8DataRecordSearch parentSearch;
    private final SubCriteriaPanel parentPanel;
    private final List<PropertyCriterionPanel> propertyComponents;
    private final List<HistoryFilterPanel> historyComponents;
    private final List<CodeCriterionPanel> codeComponents;
    private final List<OrganizationCriterionPanel> organizationComponents;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final DataFilterOperator instanceOperator;
    private List<String> instanceIdList;
    private List<String> instanceTermList;
    private final String drId;
    private final String drTerm;
    private final String displayName;
    private final String instanceLabel;
    private final boolean allowPropertyAddition;
    private final boolean allowHistoryAddition;
    private final boolean allowCodeAddition;
    private final boolean allowInstanceSelection;
    private final boolean allowRemoval;

    public DrCriteriaPanel(T8DataRecordSearch parentSearch, SubCriteriaPanel criteriaPanel, String drId, String drTerm)
    {System.out.println("CONSTRUCTOR 1");
        this.parentSearch = parentSearch;
        this.parentPanel = criteriaPanel;
        this.controller = parentSearch.getController();
        this.clientContext = controller.getClientContext();
        this.configurationManager = clientContext.getConfigurationManager();
        this.context = controller.getContext();
        this.propertyComponents = new ArrayList<>();
        this.historyComponents = new ArrayList<>();
        this.codeComponents = new ArrayList<>();
        this.organizationComponents = new ArrayList<>();
        this.drId = drId;
        this.displayName = null;
        this.drTerm = drTerm;
        this.allowPropertyAddition = true;
        this.allowHistoryAddition = true;
        this.allowCodeAddition = true;
        this.allowRemoval = true;
        this.allowInstanceSelection = false;
        this.instanceLabel = null;
        this.instanceOperator = DataFilterOperator.EQUAL;
        initComponents();
        setBorder();
        rebuildLayout();
    }

    public DrCriteriaPanel(T8DataRecordSearch parentSearch, SubCriteriaPanel parentPanel, T8DrFilterCriteria filterCriteria)
    {
        this.parentSearch = parentSearch;
        this.parentPanel = parentPanel;
        this.controller = parentSearch.getController();
        this.clientContext = controller.getClientContext();
        this.configurationManager = clientContext.getConfigurationManager();
        this.context = controller.getContext();
        this.propertyComponents = new ArrayList<>();
        this.historyComponents = new ArrayList<>();
        this.codeComponents = new ArrayList<>();
        this.organizationComponents = new ArrayList<>();
        this.drId = filterCriteria.getDrId();
        this.displayName = filterCriteria.getDisplayName();
        this.allowPropertyAddition = filterCriteria.isAllowPropertyAddition();
        this.allowHistoryAddition = filterCriteria.isAllowHistoryAddition();
        this.allowCodeAddition = filterCriteria.isAllowCodeAddition();
        this.allowRemoval = filterCriteria.isAllowRemoval();
        this.allowInstanceSelection = filterCriteria.isAllowInstanceSelection();
        this.instanceLabel = filterCriteria.getInstanceLabel();
        this.instanceOperator = filterCriteria.getInstanceOperator();
        if (drId != null) this.drTerm = parentSearch.getTerminologyProvider().getTerm(null, drId);
        else this.drTerm = null;
        initComponents();
        setBorder();
        setFilterCriteria(filterCriteria);
    }

    private void setFilterCriteria(T8DrFilterCriteria filterCriteria)
    {
        for (T8PropertyFilterCriterion propertyFilter : filterCriteria.getPropertyCriteria())
        {
            propertyComponents.add(new PropertyCriterionPanel(parentSearch, this, propertyFilter));
        }

        for (T8HistoryFilterCriterion historyFilter : filterCriteria.getHistoryCriteria())
        {
            historyComponents.add(new HistoryFilterPanel(parentSearch, this, historyFilter));
        }

        for (T8CodeFilterCriterion codeFilter : filterCriteria.getCodeCriteria())
        {
            codeComponents.add(new CodeCriterionPanel(parentSearch, this, codeFilter));
        }

        for (T8OrganizationFilterCriterion organizationFilter : filterCriteria.getOrganizationCriteria())
        {
            organizationComponents.add(new OrganizationCriterionPanel(parentSearch, this, organizationFilter));
        }

        createInstanceValueEditors(filterCriteria.getInstanceFilterValue());
        rebuildLayout();
    }

    private void createInstanceValueEditors(Object filterValue)
    {
        if (filterValue instanceof List)
        {
            List<String> filterValues;
            StringBuilder displayString;

            filterValues = (List<String>)filterValue;
            displayString = new StringBuilder();

            for(int valueIndex = 0; valueIndex < filterValues.size(); valueIndex++)
            {
                String value;

                value = filterValues.get(valueIndex);

                if ((value instanceof String) && (T8IdentifierUtilities.isConceptId((String)value)))
                {
                    TerminologyProvider terminologyProvider;
                    String valueTerm;

                    if (instanceIdList == null) this.instanceIdList = new ArrayList<>();
                    terminologyProvider = parentSearch.getTerminologyProvider();
                    this.instanceIdList.add(value);

                    valueTerm = terminologyProvider.getTerm(null, value);

                    if (valueIndex == 0) displayString.append(valueTerm);
                    else displayString.append(("," + valueTerm));
                }
                else
                {
                    if (instanceTermList == null) this.instanceTermList = new ArrayList<>();
                    this.instanceTermList.add(value);
                    if (valueIndex == 0) displayString.append(value);
                    else displayString.append(("," + value));
                }
            }

            jFormattedTextFieldDisplay.setText(displayString.toString());
        }
        else
        {
            String valueString;

            valueString = filterValue != null ? filterValue.toString() : null;
            if (Strings.trimToNull(valueString) == null)
            {
                jFormattedTextFieldDisplay.setText(null);
            }
            else
            {
                jFormattedTextFieldDisplay.setText(valueString);
            }
        }
    }

    public String getDrId()
    {
        return drId;
    }

    private void setBorder()
    {
        String title;

        title = (drTerm != null) ? drTerm : (displayName != null) ? displayName : "All Record Types";
        setBorder(BorderFactory.createTitledBorder(null, title, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", 1, 13), new Color(0, 0, 0)));
    }

    private void rebuildLayout()
    {
        DrControlPanel optionPanel;
        GridBagConstraints constraints;
        int row;

        // Clear the panel.
        row = 0;
        removeAll();

        if (allowInstanceSelection)
        {
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            for (Object item : DataFilterOperator.values())
            {
                if(item != DataFilterOperator.IS_NULL && item != DataFilterOperator.IS_NOT_NULL)
                {
                    jComboBoxOperator.addItem(item);
                }
            }
            jComboBoxOperator.setSelectedItem(instanceOperator);
            jComboBoxOperator.setRenderer(new ComboCellRenderer());
            if (instanceLabel != null) jLabelInstanceType.setText(instanceLabel);
            add(jPanelInstanceSelection, constraints);
            jPanelInstanceSelection.setVisible(row > 0);
        }

        // Add all organization filter panels.
        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            // Add the code panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(organizationComponent, constraints);
            organizationComponent.setVisible(row > 0);
        }

        // Add all code filter panels.
        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            // Add the code panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(codeComponent, constraints);
            codeComponent.setVisible(row > 0);
        }

        // Add all property filter panels.
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            // Add the property panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(propertyComponent, constraints);
            propertyComponent.setVisible(row > 0);
        }

        // Add all history filter panels.
        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            // Add the history panel.
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row++;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(historyComponent, constraints);
            historyComponent.setVisible(row > 0);
        }

        // Add the control panel (so the user can add a new clause).
        if (requiresControlPanel())
        {
            optionPanel = new DrControlPanel(parentSearch, this, allowPropertyAddition, allowHistoryAddition, allowRemoval);
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = row;
            constraints.gridwidth = 2;
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.fill = GridBagConstraints.NONE;
            constraints.anchor = GridBagConstraints.WEST;
            constraints.insets = new Insets(5, 5, 5, 5);
            add(optionPanel, constraints);

            if (!parentSearch.isAdvancedOptionsEnabled())
            {
                optionPanel.setVisible(false);
            }
        }

        revalidate();
        repaint();
    }

    private boolean requiresControlPanel()
    {
        if (allowPropertyAddition) return true;
        else if (allowHistoryAddition) return true;
        else if (allowCodeAddition) return true;
        else if (allowRemoval) return true;
        else return false;
    }

    public LayoutColumnWidths getLayoutColumnWidths()
    {
        LayoutColumnWidths maxWidths;

        maxWidths = new LayoutColumnWidths();
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            maxWidths.updateToMax(propertyComponent.getLayoutColumnWidths());
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            maxWidths.updateToMax(historyComponent.getLayoutColumnWidths());
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            maxWidths.updateToMax(codeComponent.getLayoutColumnWidths());
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            maxWidths.updateToMax(organizationComponent.getLayoutColumnWidths());
        }

        // Get the widths for the instance selection panel.
        if (allowInstanceSelection) maxWidths.updateToMax(getInstanceLayoutColumnWidths());

        return maxWidths;
    }

    public void setLinkWidths(LayoutColumnWidths widths)
    {
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            propertyComponent.setLayoutColumnWidths(widths);
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            historyComponent.setLayoutColumnWidths(widths);
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            codeComponent.setLayoutColumnWidths(widths);
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            organizationComponent.setLayoutColumnWidths(widths);
        }

        // Set the widths for the instance selection panel.
        if (allowInstanceSelection) setInstanceLayoutColumnWidths(widths);

        revalidate();
    }

    public LayoutColumnWidths getInstanceLayoutColumnWidths()
    {
        LayoutColumnWidths widths;

        widths = new LayoutColumnWidths();
        widths.setWidth(0, jLabelInstance.getPreferredSize().width);
        widths.setWidth(1, jLabelInstanceType.getPreferredSize().width);
        widths.setWidth(2, jComboBoxOperator.getPreferredSize().width);
        widths.setWidth(3, jPanelValue.getPreferredSize().width);
        return widths;
    }

    public void setInstanceLayoutColumnWidths(LayoutColumnWidths widths)
    {
        Dimension size;

        size = jLabelInstance.getMinimumSize();
        size.width = widths.getWidth(0);
        jLabelInstance.setMinimumSize(size);
        jLabelInstance.setPreferredSize(size);

        size = jLabelInstanceType.getMinimumSize();
        size.width = widths.getWidth(1);
        jLabelInstanceType.setMinimumSize(size);
        jLabelInstanceType.setPreferredSize(size);

        size = jComboBoxOperator.getMinimumSize();
        size.width = widths.getWidth(2);
        jComboBoxOperator.setMinimumSize(size);
        jComboBoxOperator.setPreferredSize(size);

        size = jPanelValue.getMinimumSize();
        size.width = widths.getWidth(3);
        jPanelValue.setMinimumSize(size);
        jPanelValue.setPreferredSize(size);

        revalidate();
    }

    public void setAdvancedOptionsVisible(boolean setVisible)
    {
        for (Component component : getComponents())
        {
            if(component instanceof DrControlPanel)
            {
                component.setVisible(setVisible);
            }
        }
    }

    @Override
    public void clearFilterValues()
    {
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            propertyComponent.clearFilterValue();
            propertyComponent.resetFilterOperator();
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            historyComponent.clearFilterValue();
            historyComponent.resetFilterOperator();
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            codeComponent.clearFilterValue();
            codeComponent.resetFilterOperator();
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            organizationComponent.clearFilterValue();
            organizationComponent.resetFilterOperator();
        }

        // Clear the filter values for the instance selection component.
        clearInstanceSelectionValues();
        jComboBoxOperator.setSelectedItem(this.instanceOperator);
    }

    public void clearInstanceSelectionValues()
    {
        jFormattedTextFieldDisplay.setText(null);
        this.instanceTermList = null;
        this.instanceIdList = null;
    }

    @Override
    public T8RecordFilterClause getFilterClause()
    {
        T8DrFilterCriteria filterClause;

        filterClause = new T8DrFilterCriteria(drId);
        filterClause.setAllowHistoryAddition(allowHistoryAddition);
        filterClause.setAllowInstanceSelection(allowInstanceSelection);
        filterClause.setAllowCodeAddition(allowCodeAddition);
        filterClause.setAllowPropertyAddition(allowPropertyAddition);

        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            filterClause.addPropertyCriterion(propertyComponent.getPropertyFilterCriterion());
        }

        for (HistoryFilterPanel historyComponent : historyComponents)
        {
            filterClause.addHistoryCriterion(historyComponent.getHistoryFilterCriterion());
        }

        for (CodeCriterionPanel codeComponent : codeComponents)
        {
            filterClause.addCodeCriterion(codeComponent.getCodeFilterCriterion());
        }

        for (OrganizationCriterionPanel organizationComponent : organizationComponents)
        {
            filterClause.addOrganizationCriterion(organizationComponent.getOrganizationFilterCriterion());
        }

        // Set the Instance search data to the dr filter criteria.
        filterClause.setInstanceOperator((DataFilterOperator)jComboBoxOperator.getSelectedItem());
        filterClause.setInstanceLabel(instanceLabel);
        if (instanceIdList != null && instanceIdList.size() > 0)
        {
            filterClause.setInstanceFilterValue(instanceIdList);
        }
        else filterClause.setInstanceFilterValue(instanceTermList);

        return filterClause;
    }

    public List<String> getPropertyIdList()
    {
        List<String> idList;

        idList = new ArrayList<>();
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            String id;

            id = propertyComponent.getPropertyId();
            if (id != null) idList.add(id);
        }

        return idList;
    }

    public boolean containsProperty(String propertyId)
    {
        for (PropertyCriterionPanel propertyComponent : propertyComponents)
        {
            String id;

            id = propertyComponent.getPropertyId();
            if (Objects.equals(id, propertyId)) return true;
        }

        return false;
    }

    public void addProperty(String propertyId, String propertyTerm, RequirementType dataType)
    {
        if (!containsProperty(propertyId))
        {
            propertyComponents.add(new PropertyCriterionPanel(parentSearch, this, propertyId, propertyTerm, null, null, dataType));
            rebuildLayout();
        }
    }

    public void addProperty(T8PropertyFilterCriterion criterion)
    {
        propertyComponents.add(new PropertyCriterionPanel(parentSearch, this, criterion));
        rebuildLayout();
    }

    @Override
    public void removeProperty(PropertyCriterionPanel propertyFilterPanel)
    {
        propertyComponents.remove(propertyFilterPanel);
        rebuildLayout();
    }

    public void addHistory(String userID, String userDisplayString)
    {
        historyComponents.add(new HistoryFilterPanel(parentSearch, this, userID, userDisplayString));
        rebuildLayout();
    }

    @Override
    public void removeHistory(HistoryFilterPanel historyFilterPanel)
    {
        historyComponents.remove(historyFilterPanel);
        rebuildLayout();
    }

    public void addCode(T8CodeFilterCriterion.CodeCriterionTarget target, String codeTypeId, String codeTypeTerm)
    {
        codeComponents.add(new CodeCriterionPanel(parentSearch, this, target, codeTypeId, codeTypeTerm));
        rebuildLayout();
    }

    @Override
    public void removeCode(CodeCriterionPanel codeCriterionPanel)
    {
        codeComponents.remove(codeCriterionPanel);
        rebuildLayout();
    }

    public void addOrganization(List<String> organizationTypeIdList, String label)
    {
        organizationComponents.add(new OrganizationCriterionPanel(parentSearch, this, organizationTypeIdList, label));
        rebuildLayout();
    }

    public void removeOrganization(OrganizationCriterionPanel organizationCriterionPanel)
    {
        organizationComponents.remove(organizationCriterionPanel);
        rebuildLayout();
    }

    public void removeFilter()
    {
        parentPanel.removeClause(this);
    }

    /** This method will set the correct variables when text is typed into
     * the instance selection lookup and not selected.
     */
    private void setTextTyped()
    {
        List<String> instanceTermList;
        List<String> instanceTrimmedList;
        String typedText;

        instanceTermList = new ArrayList<>();
        instanceTrimmedList = new ArrayList<>();
        typedText = jFormattedTextFieldDisplay.getText();

        instanceTermList = Arrays.asList(typedText.split(","));

        for (String instanceTerm : instanceTermList)
        {
            instanceTrimmedList.add(instanceTerm.trim());
        }

        this.instanceTermList = instanceTrimmedList;
        this.instanceIdList = null;
    }

    private void instanceLookup()
    {
        List<Pair<String, String>> instancePairList;
        StringBuilder textString;
        List<String> instanceIdList;

        textString = new StringBuilder();
        instanceIdList = new ArrayList<>();
        instancePairList = parentSearch.getSelectedDRInstance(this.drId);

        if (instancePairList != null)
        {
            for (int counter = 0; counter < instancePairList.size(); counter++)
            {
                Pair<String, String> instancePair;
                String instanceId;
                String instanceTerm;

                instancePair = instancePairList.get(counter);
                instanceId = (String)instancePair.getValue1();
                instanceTerm = (String)instancePair.getValue2();

                instanceIdList.add(instanceId);

                if (counter == (instancePairList.size()-1))
                {
                    textString.append(instanceTerm);
                }
                else
                {
                    textString.append(instanceTerm);
                    textString.append(",");
                }
            }

            this.instanceIdList = instanceIdList;
            this.instanceTermList = null;

            jFormattedTextFieldDisplay.setText(textString.toString());
        }
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    private class ComboCellRenderer extends DefaultListCellRenderer
    {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            DataFilterOperator operator;

            operator = (DataFilterOperator)value;
            return super.getListCellRendererComponent(list, operator != null ? translate(operator.getDisplayString()) : null, index, isSelected, cellHasFocus);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the FormEditor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelInstanceSelection = new javax.swing.JPanel();
        jLabelInstance = new javax.swing.JLabel();
        jLabelInstanceType = new javax.swing.JLabel();
        jComboBoxOperator = new javax.swing.JComboBox();
        jPanelValue = new javax.swing.JPanel();
        jFormattedTextFieldDisplay = new javax.swing.JFormattedTextField();
        jButtonLookup = new javax.swing.JButton();
        jButtonClear = new javax.swing.JButton();

        setBackground(new java.awt.Color(234, 234, 255));
        setLayout(new java.awt.GridBagLayout());

        jPanelInstanceSelection.setBackground(new java.awt.Color(234, 234, 255));
        jPanelInstanceSelection.setLayout(new java.awt.GridBagLayout());

        jLabelInstance.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelInstance.setText("Instance:");
        jLabelInstance.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelInstanceSelection.add(jLabelInstance, gridBagConstraints);

        jLabelInstanceType.setAlignmentX(0.5F);
        jLabelInstanceType.setDoubleBuffered(true);
        jLabelInstanceType.setMaximumSize(null);
        jLabelInstanceType.setMinimumSize(null);
        jLabelInstanceType.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelInstanceSelection.add(jLabelInstanceType, gridBagConstraints);

        jComboBoxOperator.setMaximumSize(null);
        jComboBoxOperator.setMinimumSize(null);
        jComboBoxOperator.setPreferredSize(null);
        jComboBoxOperator.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxOperatorItemStateChanged(evt);
            }
        });
        jComboBoxOperator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxOperatorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelInstanceSelection.add(jComboBoxOperator, gridBagConstraints);

        jPanelValue.setBackground(new java.awt.Color(234, 234, 255));
        jPanelValue.setLayout(new java.awt.GridBagLayout());

        jFormattedTextFieldDisplay.setMaximumSize(null);
        jFormattedTextFieldDisplay.setMinimumSize(null);
        jFormattedTextFieldDisplay.setPreferredSize(null);
        jFormattedTextFieldDisplay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextFieldDisplayActionPerformed(evt);
            }
        });
        jFormattedTextFieldDisplay.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jFormattedTextFieldDisplayKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelValue.add(jFormattedTextFieldDisplay, gridBagConstraints);

        jButtonLookup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/searchIcon.png"))); // NOI18N
        jButtonLookup.setToolTipText("Search for Data Value");
        jButtonLookup.setFocusable(false);
        jButtonLookup.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonLookup.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonLookup.setMaximumSize(null);
        jButtonLookup.setMinimumSize(null);
        jButtonLookup.setPreferredSize(null);
        jButtonLookup.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonLookup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLookupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelValue.add(jButtonLookup, gridBagConstraints);

        jButtonClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"))); // NOI18N
        jButtonClear.setToolTipText("Clear Data Value");
        jButtonClear.setFocusable(false);
        jButtonClear.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonClear.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonClear.setMaximumSize(null);
        jButtonClear.setMinimumSize(null);
        jButtonClear.setPreferredSize(null);
        jButtonClear.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelValue.add(jButtonClear, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 10);
        jPanelInstanceSelection.add(jPanelValue, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jPanelInstanceSelection, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxOperatorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxOperatorItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            DataFilterOperator operator;

            operator = (DataFilterOperator)evt.getItem();
            if (operator == DataFilterOperator.IS_NULL || operator == DataFilterOperator.IS_NOT_NULL)
            {
                jFormattedTextFieldDisplay.setEditable(false);
                jButtonLookup.setEnabled(false);
                jButtonClear.setEnabled(false);
            }
            else
            {
                jFormattedTextFieldDisplay.setEditable(true);
                jButtonLookup.setEnabled(true);
                jButtonClear.setEnabled(true);
            }
        }
    }//GEN-LAST:event_jComboBoxOperatorItemStateChanged

    private void jFormattedTextFieldDisplayKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedTextFieldDisplayKeyReleased
        setTextTyped();
    }//GEN-LAST:event_jFormattedTextFieldDisplayKeyReleased

    private void jButtonLookupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLookupActionPerformed
        instanceLookup();
    }//GEN-LAST:event_jButtonLookupActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearActionPerformed
        clearInstanceSelectionValues();
    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jComboBoxOperatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxOperatorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxOperatorActionPerformed

    private void jFormattedTextFieldDisplayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedTextFieldDisplayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFormattedTextFieldDisplayActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonLookup;
    private javax.swing.JComboBox jComboBoxOperator;
    private javax.swing.JFormattedTextField jFormattedTextFieldDisplay;
    private javax.swing.JLabel jLabelInstance;
    private javax.swing.JLabel jLabelInstanceType;
    private javax.swing.JPanel jPanelInstanceSelection;
    private javax.swing.JPanel jPanelValue;
    // End of variables declaration//GEN-END:variables
}
