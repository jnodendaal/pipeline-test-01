package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCriteria;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordMatchApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordMatchClientApi implements T8Api
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_DATA_RECORD_MATCH_CLIENT";

    public T8DataRecordMatchClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8DataRecordMatchClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    public T8RecordMatchCriteria retrieveMatchCriteria(String criteriaID) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_CRITERIA_ID, criteriaID);
        return (T8RecordMatchCriteria)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_RECORD_MATCH_RETRIEVE_MATCH_CRITERIA, inputParameters).get(PARAMETER_MATCH_CRITERIA);
    }
}
