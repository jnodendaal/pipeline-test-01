package com.pilog.t8.api;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.org.T8OrganizationStructureProvider;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.api.T8OrganizationApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationClientApi implements T8Api, T8OrganizationStructureProvider
{
    private final T8ClientContext clientContext;
    private final T8Context context;

    public static final String API_IDENTIFIER = "@API_ORGANIZATION_CLIENT";

    public T8OrganizationClientApi(T8Context context)
    {
        this.context = context;
        this.clientContext = context.getClientContext();
    }

    public T8OrganizationClientApi(T8ComponentController controller)
    {
        this.context = controller.getContext();
        this.clientContext = context.getClientContext();
    }

    @Override
    public T8OrganizationStructure getOrganizationStructure()
    {
        try
        {
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<>();
            return (T8OrganizationStructure)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ORG_GET_ORGANIZATION_STRUCTURE, inputParameters).get(PARAMETER_ORGANIZATION_STRUCTURE);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching organization structure.", e);
        }
    }

    public void saveOrganization(String parentOrgId, String orgTypeId, T8OntologyConcept organizationConcept, boolean saveTerminology) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_PARENT_ORG_ID, parentOrgId);
        inputParameters.put(PARAMETER_ORG_TYPE_ID, orgTypeId);
        inputParameters.put(PARAMETER_CONCEPT, organizationConcept);
        inputParameters.put(PARAMETER_INCLUDE_TERMINOLOGY, saveTerminology);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ORG_SAVE_ORGANIZATION, inputParameters);
    }

    public boolean moveOrganization(String orgId, String newParentOrgId) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_PARENT_ORG_ID, newParentOrgId);
        inputParameters.put(PARAMETER_ORG_ID, orgId);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ORG_MOVE_ORGANIZATION, inputParameters).get(PARAMETER_SUCCESS);
    }

    public T8OrganizationStructure recacheOrganizationStructure()
    {
        try
        {
            T8OrganizationStructure organizationStructure;

            organizationStructure = (T8OrganizationStructure)T8MainServerClient.executeSynchronousOperation(context, OPERATION_API_ORG_RECACHE_ORGANIZATION_STRUCTURE, null).get(PARAMETER_ORGANIZATION_STRUCTURE);
            if (organizationStructure == null) throw new Exception("Organization Structure not found.");
            else return organizationStructure;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while recaching Organization Structure.", e);
        }
    }
}
