package com.pilog.t8.mainserver;

import java.io.Serializable;
import java.util.Map;

/**
 * This class defines the request for execution of a specific operation on the
 * MainServer.
 *
 * @author Bouwer du Preez
 */
public class T8OperationExecutionRequest implements Serializable
{
    private String operationId;
    private ExecutionType operationExecutionType;
    private Map<String, Object> operationParameters;

    public enum ExecutionType {SYNCHRONOUS, ASYNCHRONOUS};

    public T8OperationExecutionRequest(String operationId, ExecutionType operationExecutionType, Map<String, Object> operationParameters)
    {
        this.operationId = operationId;
        this.operationExecutionType = operationExecutionType;
        this.operationParameters = operationParameters;
    }

    public String getOperationId()
    {
        return operationId;
    }

    public void setOperationId(String operationId)
    {
        this.operationId = operationId;
    }

    public ExecutionType getOperationExecutionType()
    {
        return operationExecutionType;
    }

    public void setOperationExecutionType(ExecutionType operationExecutionType)
    {
        this.operationExecutionType = operationExecutionType;
    }

    public Map<String, Object> getOperationParameters()
    {
        return operationParameters;
    }

    public void setOperationParameters(Map<String, Object> operationParameters)
    {
        this.operationParameters = operationParameters;
    }

    @Override
    public String toString()
    {
        return "[Operation:" + operationId + " Type:" + operationExecutionType + " Parameters:" + operationParameters + "]";
    }
}
