package com.pilog.t8.mainserver;

import java.io.Serializable;

/**
 * This class is used as the response message sent from MainServer to
 * MainServerClient in response to a previous request.
 *
 * @author Bouwer du Preez
 */
public class T8ServerResponse implements Serializable
{
    private Object responseData;
    private ResponseType responseType;
    private long timeRequestReceived;
    private long timeSent;

    public enum ResponseType
    {
        SERVER_RUNTIME_DATA,
        OPERATION_RESULT,
        OPERATION_STATUS,
        LOGIN_RESPONSE,
        HEARTBEAT,
        PASSWORD_CHANGE_RESPONSE,
        THROWABLE,
        OPERATION_DEFINITIONS,
        SUCCESS,
        OPERATION_STATUSES,
        SESSION_CONTEXT,
        AUTHENTICATION_RESPONSE
    };

    public T8ServerResponse(ResponseType responseType, Object responseData)
    {
        this.responseType = responseType;
        this.responseData = responseData;
    }

    public Object getResponseData()
    {
        return responseData;
    }

    public void setResponseData(Object responseData)
    {
        this.responseData = responseData;
    }

    public ResponseType getResponseType()
    {
        return responseType;
    }

    public void setResponseType(ResponseType responseType)
    {
        this.responseType = responseType;
    }

    public long getTimeSent()
    {
        return timeSent;
    }

    public void setTimeSent(long timeSent)
    {
        this.timeSent = timeSent;
    }

    public long getTimeRequestReceived()
    {
        return timeRequestReceived;
    }

    public void setTimeRequestReceived(long timeRequestReceived)
    {
        this.timeRequestReceived = timeRequestReceived;
    }
}
