package com.pilog.t8.mainserver;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MainServerRuntimeData implements Serializable
{
    private long freeMemory;
    private long totalMemory;
    private long maximumMemory;
    private int availableProcessors;
    private String applicationVersion;
    private String buildTimestamp;
    private Map<String, String> systemProperties;
    private Map<String, String> serverCodeVersions;

    public T8MainServerRuntimeData()
    {
    }

    public long getFreeMemory()
    {
        return freeMemory;
    }

    public void setFreeMemory(long freeMemory)
    {
        this.freeMemory = freeMemory;
    }

    public long getTotalMemory()
    {
        return totalMemory;
    }

    public void setTotalMemory(long totalMemory)
    {
        this.totalMemory = totalMemory;
    }

    public long getMaximumMemory()
    {
        return maximumMemory;
    }

    public void setMaximumMemory(long maximumMemory)
    {
        this.maximumMemory = maximumMemory;
    }

    public long getAvailableProcessors()
    {
        return availableProcessors;
    }

    public void setAvailableProcessors(int availableProcessors)
    {
        this.availableProcessors = availableProcessors;
    }

    public Map<String, String> getSystemProperties()
    {
        return systemProperties;
    }

    public void setSystemProperties(Map<String, String> systemProperties)
    {
        this.systemProperties = systemProperties;
    }

    public String getApplicationVersion()
    {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion)
    {
        this.applicationVersion = applicationVersion;
    }

    public String getBuildTimestamp()
    {
        return buildTimestamp;
    }

    public void setBuildTimestamp(String buildTimestamp)
    {
        this.buildTimestamp = buildTimestamp;
    }

    public Map<String, String> getServerCodeVersions()
    {
        return serverCodeVersions;
    }

    public void setServerCodeVersions(Map<String, String> serverCodeVersions)
    {
        this.serverCodeVersions = serverCodeVersions;
    }
}
