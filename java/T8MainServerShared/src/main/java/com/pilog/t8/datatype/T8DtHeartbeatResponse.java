package com.pilog.t8.datatype;

import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8AbstractDataType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.security.T8HeartbeatResponse;

/**
 * @author Bouwer du Preez
 */
public class T8DtHeartbeatResponse extends T8AbstractDataType implements T8DataType
{
    public static final String IDENTIFIER = "@HEARTBEAT_RESPONSE";

    public T8DtHeartbeatResponse() {}

    public T8DtHeartbeatResponse(T8DefinitionManager context) {}

    public T8DtHeartbeatResponse(T8DefinitionManager context, String stringRepresentation)
    {
        if (!stringRepresentation.startsWith(IDENTIFIER)) throw new RuntimeException("Cannot parse " + IDENTIFIER + " data type from string representation: " + stringRepresentation);
    }

    @Override
    public String getDataTypeIdentifier()
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeStringRepresentation(boolean includeAttributes)
    {
        return IDENTIFIER;
    }

    @Override
    public String getDataTypeClassName()
    {
        return T8HeartbeatResponse.class.getCanonicalName();
    }

    @Override
    public JsonValue serialize(Object object)
    {
        if (object != null)
        {
            T8HeartbeatResponse hearbeatResponse;
            JsonObject jsonHeartbeatResponse;

            // Create the concept JSON object.
            hearbeatResponse = (T8HeartbeatResponse)object;
            jsonHeartbeatResponse = new JsonObject();
            jsonHeartbeatResponse.add("taskCount", hearbeatResponse.getTaskCount());
            jsonHeartbeatResponse.add("reportCount", hearbeatResponse.getReportCount());
            jsonHeartbeatResponse.add("processCount", hearbeatResponse.getProcessCount());

            // Return the final task object.
            return jsonHeartbeatResponse;
        }
        else return JsonValue.NULL;
    }

    @Override
    public T8HeartbeatResponse deserialize(JsonValue jsonValue)
    {
        if ((jsonValue != null) && (jsonValue != JsonValue.NULL))
        {
            T8HeartbeatResponse heartbeatResponse;
            JsonObject jsonHeartbeatResponse;

            // Create the task details object.
            jsonHeartbeatResponse = jsonValue.asObject();
            heartbeatResponse = new T8HeartbeatResponse();
            heartbeatResponse.setTaskCount(jsonHeartbeatResponse.getInt("taskCount", 0));
            heartbeatResponse.setReportCount(jsonHeartbeatResponse.getInt("reportCount", 0));

            // Return the completed object.
            return heartbeatResponse;
        }
        else return null;
    }
}