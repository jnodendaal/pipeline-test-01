package com.pilog.t8.mainserver;

import com.pilog.t8.security.T8Context;
import java.io.Serializable;

/**
 * This class is used as the request message sent from MainServerClient to
 * MainServer.
 *
 * @author Bouwer du Preez
 */
public class T8ServerRequest implements Serializable
{
    private T8Context accessContext;
    private Object requestData;
    private RequestType requestType;
    private long timeSent;

    public enum RequestType
    {
        GET_SERVER_RUNTIME_DATA,
        RESTART_SERVER,
        EXECUTE_OPERATION,
        STOP_OPERATION,
        CANCEL_OPERATION,
        PAUSE_OPERATION,
        RESUME_OPERATION,
        GET_OPERATION_STATUS,
        GET_OPERATION_STATUSES,
        GET_ALL_OPERATION_STATUSES,
        SET_CONTEXT_PATH,
        CREATE_NEW_SESSION,
        CHANGE_PASSWORD,
        REQUEST_PASSWORD_CHANGE,
        CHECK_PASSWORD_CHANGE_AVAILABILITY,
        CHECK_USERNAME_AVAILABILITY,
        CHECK_USER_REGISTERED,
        HEARTBEAT,
        LOGIN,
        LOGOUT,
        SWITCH_USER_PROFILE,
        AUTHENTICATE
    };

    public T8ServerRequest(T8Context accessContext, RequestType requestType, Object requestData)
    {
        this.accessContext = accessContext;
        this.requestType = requestType;
        this.requestData = requestData;
    }

    public Object getRequestData()
    {
        return requestData;
    }

    public void setRequestData(Object requestData)
    {
        this.requestData = requestData;
    }

    public RequestType getRequestType()
    {
        return requestType;
    }

    public void setRequestType(RequestType requestType)
    {
        this.requestType = requestType;
    }

    public T8Context getContext()
    {
        return accessContext;
    }

    public void setAccessContext(T8Context accessContext)
    {
        this.accessContext = accessContext;
    }

    public long getTimeSent()
    {
        return timeSent;
    }

    public void setTimeSent(long timeSent)
    {
        this.timeSent = timeSent;
    }

    @Override
    public String toString()
    {
        return "T8ServerRequest{requestType=" + requestType + ", requestData=" + requestData + ", accessContext=" + accessContext + "}";
    }
}
