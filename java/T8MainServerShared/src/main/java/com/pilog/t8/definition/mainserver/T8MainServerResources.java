package com.pilog.t8.definition.mainserver;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8MainServerResources implements T8DefinitionResource
{
    public static final String DE_STATE_OPERATION_QUEUE = "@E_STATE_OPERATION_QUEUE";
    public static final String DS_STATE_OPERATION_QUEUE = "@DS_STATE_OPERATION_QUEUE";

    public static final String F_OPERATION_IID = "$OPERATION_IID";
    public static final String F_OPERATION_ID = "$OPERATION_ID";
    public static final String F_TIME_QUEUED = "$TIME_QUEUED";
    public static final String F_TIME_PROCESSED = "$TIME_PROCESSED";
    public static final String F_PARAMETERS = "$PARAMETERS";
    public static final String F_PRIORITY = "$PRIORITY";
    public static final String F_ORG_ID = "$ORG_ID";
    public static final String F_ROOT_ORG_ID = "$ROOT_ORG_ID";
    public static final String F_AGENT_ID = "$AGENT_ID";
    public static final String F_AGENT_IID = "$AGENT_IID";
    public static final String F_STATUS = "$STATUS";
    public static final String F_INFORMATION = "$INFORMATION";

    // Table Names.
    public static final String TABLE_STATE_OPERATION_QUEUE = "OPR_QUE";

    @Override
    public List<T8Definition> getResourceDefinitions(T8ServerContext serverContext, Set<String> typeIdentifiers)
    {
        List<T8Definition> definitions;

        definitions = new ArrayList<>();

        if (typeIdentifiers.contains(T8TableDataSourceResourceDefinition.TYPE_IDENTIFIER))
        {
            T8TableDataSourceDefinition tableDataSourceDefinition;

            tableDataSourceDefinition = new T8TableDataSourceResourceDefinition(DS_STATE_OPERATION_QUEUE);
            tableDataSourceDefinition.setConnectionIdentifier(serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId());
            tableDataSourceDefinition.setTableName(TABLE_STATE_OPERATION_QUEUE);
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_OPERATION_IID, "OPERATION_IID", true, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_OPERATION_ID, "OPERATION_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_QUEUED, "TIME_QUEUED", false, true, T8DataType.DATE_TIME));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TIME_PROCESSED, "TIME_PROCESSED", false, true, T8DataType.DATE_TIME));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PARAMETERS, "PARAMETERS", false, true, T8DataType.LONG_STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PRIORITY, "PRIORITY", false, true, T8DataType.INTEGER));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ROOT_ORG_ID, "ROOT_ORG_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_AGENT_IID, "AGENT_IID", false, true, T8DataType.GUID));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_AGENT_ID, "AGENT_ID", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_STATUS, "STATUS", false, true, T8DataType.STRING));
            tableDataSourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_INFORMATION, "INFORMATION", false, true, T8DataType.STRING));

            definitions.add(tableDataSourceDefinition);
        }

        // Added entity definitions if needed.
        if (typeIdentifiers.contains(T8DataEntityResourceDefinition.TYPE_IDENTIFIER))
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = new T8DataEntityResourceDefinition(DE_STATE_OPERATION_QUEUE);
            entityDefinition.setDataSourceIdentifier(DS_STATE_OPERATION_QUEUE);
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_OPERATION_IID, DS_STATE_OPERATION_QUEUE + F_OPERATION_IID, true, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_OPERATION_ID, DS_STATE_OPERATION_QUEUE + F_OPERATION_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_QUEUED, DS_STATE_OPERATION_QUEUE + F_TIME_QUEUED, false, true, T8DataType.DATE_TIME));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TIME_PROCESSED, DS_STATE_OPERATION_QUEUE + F_TIME_PROCESSED, false, true, T8DataType.DATE_TIME));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PARAMETERS, DS_STATE_OPERATION_QUEUE + F_PARAMETERS, false, true, T8DataType.LONG_STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PRIORITY, DS_STATE_OPERATION_QUEUE + F_PRIORITY, false, true, T8DataType.INTEGER));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ROOT_ORG_ID, DS_STATE_OPERATION_QUEUE + F_ROOT_ORG_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_STATE_OPERATION_QUEUE + F_ORG_ID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_AGENT_IID, DS_STATE_OPERATION_QUEUE + F_AGENT_IID, false, true, T8DataType.GUID));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_AGENT_ID, DS_STATE_OPERATION_QUEUE + F_AGENT_ID, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_STATUS, DS_STATE_OPERATION_QUEUE + F_STATUS, false, true, T8DataType.STRING));
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_INFORMATION, DS_STATE_OPERATION_QUEUE + F_INFORMATION, false, true, T8DataType.LONG_STRING));
            definitions.add(entityDefinition);
        }

        return definitions;
    }
}
