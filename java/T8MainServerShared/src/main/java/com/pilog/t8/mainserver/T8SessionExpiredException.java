/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.mainserver;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8SessionExpiredException extends Exception
{
    private final boolean anonymousSession;

    public T8SessionExpiredException()
    {
        this(false);
    }

    public T8SessionExpiredException(boolean anonymousSession)
    {
        this("Unknown", anonymousSession);
    }

    public T8SessionExpiredException(String message, boolean anonymousSession)
    {
        super(message);
        this.anonymousSession = anonymousSession;
    }

    public T8SessionExpiredException(String message, Throwable cause, boolean anonymousSession)
    {
        super(message, cause);
        this.anonymousSession = anonymousSession;
    }

    public T8SessionExpiredException(Throwable cause, boolean anonymousSession)
    {
        this("Unknown", cause, anonymousSession);
    }

    public boolean isAnonymousSession()
    {
        return this.anonymousSession;
    }
}
