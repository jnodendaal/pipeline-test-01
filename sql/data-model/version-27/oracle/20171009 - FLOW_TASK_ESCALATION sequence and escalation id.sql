ALTER TABLE FLOW_TASK_ESCALATION ADD SEQUENCE NUMBER(3,0);
ALTER TABLE FLOW_TASK_ESCALATION ADD ESCALATION_ID VARCHAR2(255 CHAR);