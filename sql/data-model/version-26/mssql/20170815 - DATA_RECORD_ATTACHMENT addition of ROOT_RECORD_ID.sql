ALTER TABLE "DATA_RECORD_ATTACHMENT" ADD "ROOT_RECORD_ID" UNIQUEIDENTIFIER;
GO

UPDATE a
SET a.ROOT_RECORD_ID = b.ROOT_RECORD_ID
FROM DATA_RECORD_ATTACHMENT a
INNER JOIN DATA_RECORD_DOC b
ON a.RECORD_ID = b.RECORD_ID
GO

ALTER TABLE "DATA_RECORD_ATTACHMENT" ALTER COLUMN "ROOT_RECORD_ID" UNIQUEIDENTIFIER NOT NULL;
GO

CREATE NONCLUSTERED INDEX [IX_REC_ATT_RTT_REC_ID]
ON [dbo].[DATA_RECORD_ATTACHMENT] ([ROOT_RECORD_ID])
GO
