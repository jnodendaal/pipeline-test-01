var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

const ENV = process.env.NODE_ENV = process.env.ENV = 'development';

module.exports = webpackMerge(commonConfig, 
{
  devtool: 'cheap-module-eval-source-map',

  output: 
  {
    path: helpers.root('dist'),
    filename: '[name].js',
    chunkFilename: '[id].chunk.js'
  },

  plugins: 
  [
    new ExtractTextPlugin('[name].css'),
    new webpack.DefinePlugin
    ({
      'process.env': 
      {
        'ENV': JSON.stringify(ENV)
      }
    }),
    new HtmlWebpackPlugin
    ({
      template: 'src/index.ejs'
    })
  ],

  devServer: 
  {
    historyApiFallback: true,
    stats: 'minimal'
  }
});