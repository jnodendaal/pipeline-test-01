import { Component } from '@angular/core';
import { SecurityManagerService } from './core/service/security-manager.service';

@Component
({
  selector: 'app',
  templateUrl: 'app.component.html',  
  //styleUrls: [ '../assets/css/styles.css' ],
  providers: [ ]  
})
export class AppComponent  
{
  constructor()
  {
    console.log("AppComponent");
  }  
}
