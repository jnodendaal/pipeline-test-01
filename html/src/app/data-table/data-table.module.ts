import { NgModule }                             from '@angular/core';
import { CommonModule }                         from '@angular/common';
import { FormsModule }                          from '@angular/forms';
import { SharedModule }                         from '../core/shared.module';

import { DataTableComponent}                    from './component/data-table.component';
import { DataCardDeckComponent }                from './component/data-card-deck.component';
import { FilterBarComponent}                    from './component/filter-bar.component';
import { DataTableRow }                         from './component/row.component';
import { PixelConverterPipe }                   from './utility/pixel-converter.pipe';
import { HideDirective }                        from './utility/hide.directive';
import { MinPipe }                              from './utility/min';
import { HighlightTextDirective }               from '../core/utility/highlight-text.directive';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    SharedModule
  ],
  declarations: 
  [ 
    DataTableComponent,
    DataCardDeckComponent,
    FilterBarComponent,
    DataTableRow, 
    PixelConverterPipe, 
    HideDirective, 
    MinPipe,
  ],
  exports:      
  [ 
    DataTableComponent,
    DataCardDeckComponent,
    FilterBarComponent
  ],
  providers:    [ ]
})
export class DataTableModule 
{
}