import { Component, Input, Output, EventEmitter, ContentChildren, QueryList, TemplateRef, ContentChild, ViewChildren, OnInit } from '@angular/core';
import { DataTableRow, RowClickedEvent, RowCallback } from './row.component';
import { DataTableFilter } from '../model/data-table.model';

@Component
({
    selector: 'data-table',
    templateUrl: 'data-table.component.html',
    styleUrls: ['data-table.component.css']
    
})
export class DataTableComponent implements OnInit 
{
    // UI components.
    @ViewChildren(DataTableRow) rows: QueryList<DataTableRow>;
    @ContentChild('dataTableExpand') expandTemplate: TemplateRef<any>;

    // One-time optional bindings with default values.
    @Input() indexColumnVisible = false;
    @Input() indexColumnHeader = '';
    @Input() rowColors: RowCallback;
    @Input() rowTooltip: RowCallback;
    @Input() selectionEnabled = false;
    @Input() multiSelect = true;
    @Input() expandableRows = false;
    @Input() expandColumnVisible = false;
    @Input() rowClickEnabled = false;
    @Input() itemCount: number;
    @Input() setup: TableSetup;
    @Input() searchPattern: string;

    @Output() selectionChanged = new EventEmitter<SelectionChangedEvent>();
    @Output() rowClicked = new EventEmitter();
    @Output() rowDoubleClicked = new EventEmitter();
    @Output() headerClicked = new EventEmitter();

    private dataEntities: any[] = [];
    private selectedRows: DataTableRow[] = [];
    private selectAllCheckBox: boolean = false;

    constructor()
    {
    }

    ngOnInit() 
    {
        this.initDefaultClickEvents();
    }

    private initDefaultClickEvents() 
    {
        if (this.rowClickEnabled) 
        {
            this.rowClicked.subscribe((tableEvent: any) => tableEvent.row.selected = !tableEvent.row.selected);
        }
    }

    @Input() 
    get entities() 
    {
        return this.dataEntities;
    }

    set entities(entities: any[]) 
    {
        this.dataEntities = entities;
    }
    
    private onRowClicked(row: DataTableRow, event: any) 
    {
        this.rowClicked.emit({ row, event } as RowClickedEvent);
    }
    
    private onRowDoubleClicked(row: DataTableRow, event: any) 
    {
        this.rowDoubleClicked.emit({ row, event });
    }
    
    private onHeaderClicked(fieldId: string, event: MouseEvent) 
    {
    }

    private getRowColor(item: any, index: number, row: DataTableRow) 
    {
        if (this.rowColors !== undefined) 
        {
            return (<RowCallback>this.rowColors)(item, row, index);
        }
    }

    private toggleSelectAll() 
    {
        this.selectAllCheckBox = !this.selectAllCheckBox;
        this.onSelectAllChanged(this.selectAllCheckBox);
    }

    public getSelectedEntities(): any[]
    {
        let selectedEntities: any[];

        selectedEntities = new Array();
        for (let selectedRow of this.selectedRows)
        {
            selectedEntities.push(selectedRow.entity);
        }

        return selectedEntities;
    }

    private onSelectAllChanged(value: boolean) 
    {
        let entities: any[];

        entities = new Array();
        for (let row of this.rows.toArray())
        {
            row.setSelected(value);
            if (value) entities.push(row.entity);
        }

        // Emit the event.
        this.selectionChanged.emit({ selectedEntities: entities } as SelectionChangedEvent);
    }
    
    private onRowSelectionChanged(eventRow: DataTableRow) 
    {
        // Maintain the selectedRows collection.
        this.selectedRows.length = 0;
        if (this.multiSelect) 
        {
            for (let row of this.rows.toArray())
            {
                if (row.isSelected())
                {
                    this.selectedRows.push(row);
                }
            }
        } 
        else 
        {
            if (eventRow.isSelected()) 
            {
                this.selectedRows.push(eventRow);
                for (let row of this.rows.toArray())
                {
                    if (row != eventRow)
                    {
                        row.setSelected(false);
                    }
                }
            } 
        }

        // Emit the change event.
        this.selectionChanged.emit({ selectedEntities: this.getSelectedEntities() } as SelectionChangedEvent);
    }
}


export interface TableSetup
{
    fields: FieldSetup[];
}

export interface FieldSetup
{
    id: string;
    type: string;
    name: string;
    visible: boolean;
    description: string;
}

export interface SelectionChangedEvent
{
    selectedEntities: any[];
}