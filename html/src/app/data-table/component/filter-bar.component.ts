import { Component, ViewChild, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ApplicationSettings } from '../../app.config';
import { Router, ActivatedRoute } from '@angular/router';
import { Context } from '../../core/model/security.model';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { DataFilter, DataFilterCriteria, DataFilterCriterion, DataFilterFieldOrdering } from '../../core/model/data-filter.model';
import { RecordFilterCriteria } from '../../core/model/data-record-filter.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { ComponentState } from '../../core/model/component.model';
import { TableSetup, FieldSetup } from './data-table.component';
import { CardSetup } from './data-card-deck.component';
import * as DataFilterModel from '../../core/model/data-filter.model';

@Component
({
    selector: 'data-filter-bar',
    templateUrl: 'filter-bar.component.html',
    styleUrls: ['filter-bar.component.css'],
    providers: [ ]
})
export class FilterBarComponent
{
    private componentManager: ComponentManagerService;   
    private dataCount: number;

    @Input()
    state: FilterBarState;

    @Input()
    entityId: string;

    @Input()
    setup: FilterBarSetup;

    @Input()
    tableSetup: TableSetup;

    @Input()
    cardSetup: CardSetup;
    
    // All operators supported by the filter bar.
    private operators =
    [
        DataFilterModel.LIKE,
        DataFilterModel.NOT_LIKE,
        DataFilterModel.EQUAL,
        DataFilterModel.NOT_EQUAL,
        DataFilterModel.GREATER_THAN,
        DataFilterModel.GREATER_THAN_OR_EQUAL,
        DataFilterModel.LESS_THAN,
        DataFilterModel.LESS_THAN_OR_EQUAL,
        DataFilterModel.STARTS_WITH,
        DataFilterModel.ENDS_WITH,
        DataFilterModel.IS_NULL,
        DataFilterModel.IS_NOT_NULL
    ];

    constructor(componentManager: ComponentManagerService)
    {
        this.componentManager = componentManager;
        this.dataCount = 0;
    }

    @Output() filterChanged = new EventEmitter();
    private onFilterChanged() 
    {
        this.filterChanged.emit(this.getDataFilter());
    }

    @Output() downloadReport = new EventEmitter();
    private onDownloadReport() 
    {
        this.downloadReport.emit(this.getDataFilter());
    }

    private setFilterCriterionOperator(criterion: DataFilterCriterion, operator: string)
    {
        if (criterion.operator != operator)
        {
            criterion.operator = operator;
            this.onFilterChanged();
        }
    }

    private setFieldOrderAscending(ordering: DataFilterFieldOrdering)
    {
        if (ordering.method != "ASCENDING")
        {
            ordering.method ="ASCENDING";
            this.onFilterChanged();
        }
    }

    private setFieldOrderDescending(ordering: DataFilterFieldOrdering)
    {
        if (ordering.method != "DESCENDING")
        {
            ordering.method ="DESCENDING";
            this.onFilterChanged();
        }
    }

    public getDataFilter(): DataFilter
    {
        let newFilter;

        newFilter = new DataFilter("FILTER_BAR", this.entityId);
        if (this.state)
        {
            newFilter.addCriteria(this.state.filterCriteria);
            newFilter.addFieldOrderings(this.state.fieldOrdering);

            // Add any record filter that if available.
            if (this.state.recordFilterCriteria)
            {
                this.state.recordFilterCriteria.fieldId = this.entityId + "$ROOT_RECORD_ID";
                newFilter.addClause(this.state.recordFilterCriteria);
            }

            return newFilter;
        }
        else return newFilter;
    }

    public getFilterTerms(): string[]
    {
        let terms: string[];

        terms = new Array();
        for (let criterion of this.state.filterCriteria)
        {
            if (criterion.hasCriteria())
            {
                terms.push(criterion.value);
            }
        }

        return terms;
    }

    private clearSearchExpression()
    {
        this.state.searchExpression = undefined;
        this.onFilterChanged();
    }

    private clearRecordSearchCriteria()
    {
        this.state.recordFilterCriteria = undefined;
        this.onFilterChanged();
    }

    private addFilterCriterion(fieldId: string)
    {
        this.state.filterCriteria.push(new DataFilterCriterion(null, "AND", fieldId, "LIKE", null));
    }

    private removeFilterCriterion(criterion: DataFilterCriterion)
    {
        let index: number;
        
        index = this.state.filterCriteria.indexOf(criterion);
        if (index > -1)
        {
            this.state.filterCriteria.splice(index, 1);
            this.onFilterChanged();
        }
    }

    private addFieldOrdering(fieldId: string)
    {
        this.state.fieldOrdering.push(new DataFilterFieldOrdering(fieldId, "ASCENDING"));
        this.onFilterChanged();
    }

    private removeFieldOrdering(ordering: DataFilterFieldOrdering)
    {
        let index: number;
        
        index = this.state.fieldOrdering.indexOf(ordering);
        if (index > -1)
        {
            this.state.fieldOrdering.splice(index, 1);
            this.onFilterChanged();
        }
    }

    private nextPage()
    {
        this.state.pageOffset += this.state.pageSize;
        this.state.pageNumber = this.state.pageOffset / this.state.pageSize + 1;
        this.onFilterChanged();
    }

    private previousPage()
    {
        if (this.state.pageOffset > 0)
        {
            this.state.pageOffset -= this.state.pageSize;
            this.state.pageNumber = this.state.pageOffset / this.state.pageSize + 1;
            this.onFilterChanged();
        }
    }

    private setPageSize(size: number)
    {
        this.state.pageSize = size;
        this.onFilterChanged();
    }

    private getFieldSetup(fieldId: string): FieldSetup
    {
        for (let field of this.tableSetup.fields)
        {
            if (field.id == fieldId)
            {
                return field;
            }
        }

        return null;
    }

    private getFieldName(fieldId: string): string
    {
        for (let field of this.tableSetup.fields)
        {
            if (field.id == fieldId)
            {
                return field.name;
            }
        }

        return null;
    }

    private getOperatorName(operatorId: string)
    {
        for (let operator of this.operators)
        {
            if (operator.id == operatorId)
            {
                return operator.name;
            }
        }

        return operatorId;
    }

    private getRecordFilterDisplayString()
    {
        if (this.state.recordFilterCriteria)
        {
            return this.state.recordFilterCriteria.getDisplayString(this.componentManager.getTerminology());
        }

        else return undefined;
    }

    private hasCriteria(): boolean
    {
        return this.state.filterCriteria.length > 0 || this.state.fieldOrdering.length > 0 || this.state.searchExpression != null || this.state.recordFilterCriteria != null;
    }
}

export class FilterBarState
{
    filterCriteria: DataFilterCriterion[];
    fieldOrdering: DataFilterFieldOrdering[];
    recordFilterCriteria: RecordFilterCriteria;
    searchExpression: string;
    pageOffset: number;
    pageSize: number;
    pageNumber: number;
    viewType: string; // 'CARD', 'TABLE'

    constructor()
    {
        this.pageOffset = 0;
        this.pageSize = 8;
        this.viewType = "CARD";
        this.filterCriteria = new Array();
        this.fieldOrdering = new Array();
    }
}

export interface FilterBarSetup
{
    viewSwitchEnabled: boolean;
    downloadReportEnabled: boolean;
}
