import { Component, ViewChild, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ApplicationSettings } from '../../app.config';
import { Router, ActivatedRoute } from '@angular/router';
import { Context } from '../../core/model/security.model';
import { DataEntity } from '../../core/model/data-entity.model';
import { DataFilter, DataFilterCriteria, DataFilterCriterion } from '../../core/model/data-filter.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { ComponentState } from '../../core/model/component.model';

@Component
({
    selector: 'data-card-deck',
    templateUrl: 'data-card-deck.component.html',
    styleUrls: ['data-card-deck.component.css'],
    providers: [ ]
})
export class DataCardDeckComponent
{   
    private attachmentUrl: string;

    @Input()
    setup: CardSetup;

    @Input()
    entities: DataEntity[];

    @Input()
    searchPattern: string;

    constructor()
    {
        this.attachmentUrl = ApplicationSettings.API_ATTACHMENT_URL + "/";
    }

    private getImageUrl(entity: DataEntity)
    {
        let attachmentId: string;

        attachmentId = entity.getField(this.setup.imageField.id);
        if (attachmentId)
        {
            return this.attachmentUrl + attachmentId;
        }
        else
        {
            return ApplicationSettings.NO_IMAGE_URL;
        }
    }

    @Output() cardClick = new EventEmitter<CardEvent>();
    private onCardClick(entity: DataEntity)
    {
        this.cardClick.emit({entity: entity});
    }
}

export interface CardSetup
{
    badgeField: FieldSetup;
    imageField: FieldSetup;
    headerField: FieldSetup;
    bodyFields: FieldSetup[];
    footerField: FieldSetup;
}

export interface FieldSetup
{
    id: string;
    type: string;
    visible: boolean;
    name: string;
    description: string;
}

export interface CardEvent
{
    entity: DataEntity;
}