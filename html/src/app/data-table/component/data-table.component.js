"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var column_component_1 = require("./column.component");
var row_component_1 = require("./row.component");
var data_table_model_1 = require("./data-table.model");
var data_table_model_2 = require("./data-table.model");
var drag_1 = require("./drag");
var table_style_1 = require("./table.style");
var DataTableComponent = (function () {
    function DataTableComponent() {
        this._items = [];
        this.header = true;
        this.pagination = true;
        this.indexColumnVisible = true;
        this.indexColumnHeader = '';
        this.selectColumnVisible = false;
        this.multiSelect = true;
        this.expandableRows = false;
        this.expandColumnVisible = false;
        this.translations = data_table_model_2.defaultTranslations;
        this.rowClickEnabled = false;
        this.autoReload = true;
        this.showReloading = false;
        this._reloading = false;
        this.reload = new core_1.EventEmitter();
        this.rowClick = new core_1.EventEmitter();
        this.rowDoubleClick = new core_1.EventEmitter();
        this.headerClick = new core_1.EventEmitter();
        this.cellClick = new core_1.EventEmitter();
        this.selectedRows = [];
        this._selectAllCheckbox = false;
        // Column resizing.
        this._resizeInProgress = false;
        this.resizeLimit = 30;
        this.filter = new data_table_model_1.DataTableFilter();
    }
    DataTableComponent.prototype.ngOnInit = function () {
        this.initDefaultClickEvents();
        if (this.autoReload && this._scheduledReload == null) {
            this.refreshData();
        }
    };
    DataTableComponent.prototype.initDefaultClickEvents = function () {
        var _this = this;
        this.headerClick.subscribe(function (tableEvent) { return _this.sortColumn(tableEvent.column); });
        if (this.rowClickEnabled) {
            this.rowClick.subscribe(function (tableEvent) { return tableEvent.row.selected = !tableEvent.row.selected; });
        }
    };
    DataTableComponent.prototype.getFilter = function () {
        return this.filter;
    };
    Object.defineProperty(DataTableComponent.prototype, "items", {
        get: function () {
            return this._items;
        },
        set: function (items) {
            this._items = items;
            this._onReloadFinished();
        },
        enumerable: true,
        configurable: true
    });
    DataTableComponent.prototype.sort = function (orderByFieldId, orderAscending) {
        this.filter.orderByFieldId = orderByFieldId;
        this.filter.orderAscending = orderAscending;
    };
    Object.defineProperty(DataTableComponent.prototype, "reloading", {
        get: function () {
            return this._reloading;
        },
        enumerable: true,
        configurable: true
    });
    DataTableComponent.prototype.refreshData = function () {
        this._reloading = true;
        this.reload.emit(this.filter);
    };
    DataTableComponent.prototype._onReloadFinished = function () {
        this._selectAllCheckbox = false;
        this._reloading = false;
    };
    // for avoiding cascading reloads if multiple params are set at once:
    DataTableComponent.prototype._triggerReload = function () {
        var _this = this;
        if (this._scheduledReload) {
            clearTimeout(this._scheduledReload);
        }
        this._scheduledReload = setTimeout(function () {
            _this.refreshData();
        });
    };
    DataTableComponent.prototype.rowClicked = function (row, event) {
        this.rowClick.emit({ row: row, event: event });
    };
    DataTableComponent.prototype.rowDoubleClicked = function (row, event) {
        this.rowDoubleClick.emit({ row: row, event: event });
    };
    DataTableComponent.prototype.headerClicked = function (column, event) {
        if (!this._resizeInProgress) {
            this._triggerReload();
            this.headerClick.emit({ column: column, event: event });
        }
        else {
            this._resizeInProgress = false; // this is because I can't prevent click from mousup of the drag end
        }
    };
    DataTableComponent.prototype.cellClicked = function (column, row, event) {
        this.cellClick.emit({ row: row, column: column, event: event });
    };
    DataTableComponent.prototype.onFilterUpdated = function () {
        this._triggerReload();
    };
    DataTableComponent.prototype.sortColumn = function (column) {
        if (column.sortable) {
            var ascending = this.filter.orderByFieldId === column.fieldId ? !this.filter.orderAscending : true;
            this.sort(column.fieldId, ascending);
        }
    };
    Object.defineProperty(DataTableComponent.prototype, "columnCount", {
        get: function () {
            var count = 0;
            count += this.indexColumnVisible ? 1 : 0;
            count += this.selectColumnVisible ? 1 : 0;
            count += this.expandColumnVisible ? 1 : 0;
            this.columns.toArray().forEach(function (column) {
                count += column.visible ? 1 : 0;
            });
            return count;
        },
        enumerable: true,
        configurable: true
    });
    DataTableComponent.prototype.getRowColor = function (item, index, row) {
        if (this.rowColors !== undefined) {
            return this.rowColors(item, row, index);
        }
    };
    Object.defineProperty(DataTableComponent.prototype, "selectAllCheckbox", {
        get: function () {
            return this._selectAllCheckbox;
        },
        set: function (value) {
            this._selectAllCheckbox = value;
            this._onSelectAllChanged(value);
        },
        enumerable: true,
        configurable: true
    });
    DataTableComponent.prototype._onSelectAllChanged = function (value) {
        this.rows.toArray().forEach(function (row) { return row.selected = value; });
    };
    DataTableComponent.prototype.onRowSelectChanged = function (row) {
        // maintain the selectedRow(s) view
        if (this.multiSelect) {
            var index = this.selectedRows.indexOf(row);
            if (row.selected && index < 0) {
                this.selectedRows.push(row);
            }
            else if (!row.selected && index >= 0) {
                this.selectedRows.splice(index, 1);
            }
        }
        else {
            if (row.selected) {
                this.selectedRow = row;
            }
            else if (this.selectedRow === row) {
                this.selectedRow = undefined;
            }
        }
        // unselect all other rows:
        if (row.selected && !this.multiSelect) {
            this.rows.toArray().filter(function (row_) { return row_.selected; }).forEach(function (row_) {
                if (row_ !== row) {
                    row_.selected = false;
                }
            });
        }
    };
    DataTableComponent.prototype.resizeColumnStart = function (event, column, columnElement) {
        var _this = this;
        this._resizeInProgress = true;
        drag_1.drag(event, {
            move: function (moveEvent, dx) {
                if (_this._isResizeInLimit(columnElement, dx)) {
                    column.width = columnElement.offsetWidth + dx;
                }
            },
        });
    };
    DataTableComponent.prototype._isResizeInLimit = function (columnElement, dx) {
        /* This is needed because CSS min-width didn't work on table-layout: fixed.
         Without the limits, resizing can make the next column disappear completely,
         and even increase the table width. The current implementation suffers from the fact,
         that offsetWidth sometimes contains out-of-date values. */
        if ((dx < 0 && (columnElement.offsetWidth + dx) <= this.resizeLimit) ||
            (!columnElement.nextElementSibling) ||
            (dx >= 0 && (columnElement.nextElementSibling.offsetWidth + dx) <= this.resizeLimit)) {
            return false;
        }
        else
            return true;
    };
    return DataTableComponent;
}());
__decorate([
    core_1.ContentChildren(column_component_1.DataTableColumn),
    __metadata("design:type", core_1.QueryList)
], DataTableComponent.prototype, "columns", void 0);
__decorate([
    core_1.ViewChildren(row_component_1.DataTableRow),
    __metadata("design:type", core_1.QueryList)
], DataTableComponent.prototype, "rows", void 0);
__decorate([
    core_1.ContentChild('dataTableExpand'),
    __metadata("design:type", core_1.TemplateRef)
], DataTableComponent.prototype, "expandTemplate", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], DataTableComponent.prototype, "headerTitle", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "header", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "pagination", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "indexColumnVisible", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "indexColumnHeader", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Function)
], DataTableComponent.prototype, "rowColors", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Function)
], DataTableComponent.prototype, "rowTooltip", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "selectColumnVisible", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "multiSelect", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "expandableRows", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "expandColumnVisible", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "translations", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "rowClickEnabled", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "autoReload", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "showReloading", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Array])
], DataTableComponent.prototype, "items", null);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], DataTableComponent.prototype, "itemCount", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "reload", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "rowClick", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "rowDoubleClick", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "headerClick", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], DataTableComponent.prototype, "cellClick", void 0);
DataTableComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'data-table',
        templateUrl: 'data-table.component.html',
        styles: [table_style_1.TABLE_STYLE]
    }),
    __metadata("design:paramtypes", [])
], DataTableComponent);
exports.DataTableComponent = DataTableComponent;
//# sourceMappingURL=data-table.component.js.map