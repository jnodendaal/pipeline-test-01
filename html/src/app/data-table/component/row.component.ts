import { Component, Input, Inject, forwardRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ApplicationSettings } from '../../app.config';
import { DataTableComponent } from './data-table.component';

export type RowCallback = (item: any, row: DataTableRow, index: number) => string;

@Component
({
    selector: '[dataTableRow]',
    templateUrl: 'row.component.html',
    styleUrls: ['row.component.css']
})
export class DataTableRow implements OnDestroy 
{
    @Input() searchPattern: string;
    @Input() entity: any;
    @Input() index: number;

    @Output() selectionChanged = new EventEmitter();

    private expanded: boolean;
    private selected: boolean;
    private attachmentImageUrl: string;

    constructor(@Inject(forwardRef(() => DataTableComponent)) public dataTable: DataTableComponent) 
    {
        this.attachmentImageUrl = ApplicationSettings.API_ATTACHMENT_URL + "/";
    }

    ngOnDestroy() 
    {
        this.selected = false;
    }

    private getAttachmentUrl(attachmentId: string)
    {
        if (attachmentId)
        {
            return this.attachmentImageUrl + attachmentId;
        }
        else
        {
            return ApplicationSettings.NO_IMAGE_URL;
        } 
    }

    toggleSelection()
    {
        this.selected = !this.selected;
        this.selectionChanged.emit(this.selected);
    }

    public isSelected(): boolean
    {
        return this.selected;
    }

    public setSelected(selected: boolean)
    {
        this.selected = selected;
    }

    get displayIndex() 
    {
        return this.index + 1;
    }

    getTooltip() 
    {
        if (this.dataTable.rowTooltip) 
        {
            return this.dataTable.rowTooltip(this.entity, this, this.index);
        }
        else return '';
    }

    private _this = this; // FIXME is there no template keyword for this in angular 2?
}

export interface RowClickedEvent
{
    row: DataTableRow;
    event: any;
}