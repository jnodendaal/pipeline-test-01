export class DataTableFilter 
{
    pageOffset: number;
    pageSize: number;
    orderByFieldId: string;
    orderAscending: boolean;

    constructor()
    {
        this.orderByFieldId = null;
        this.orderAscending = true;
        this.pageOffset = 0;
        this.pageSize = 10;
    }

    public getPageIndex() 
    {
        return Math.floor(this.pageOffset / this.pageSize) + 1;
    }

    public setOffsetToPageIndex(value: number) 
    {
        this.pageOffset = (value - 1) * this.pageSize;
    }
}

export class DataTableSetup
{
    fields: DataTableFieldSetup[];

    constructor()
    {
        this.fields = new Array();
    }
}

export class DataTableFieldSetup
{
    id: string;
    type: string;
    visible: boolean;
    width: number;
    name: string;
    description: string;

    constructor()
    {
    }

    public fromJson(jsonField: any): DataTableFieldSetup
    {
        this.id = jsonField.id;
        this.type = jsonField.type;
        this.visible = jsonField.visible;
        this.width = jsonField.width;
        this.name = jsonField.name;
        this.description = jsonField.description;
        return this;
    }
}