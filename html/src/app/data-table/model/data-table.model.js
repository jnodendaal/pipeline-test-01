"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultTranslations = {
    indexColumn: 'index',
    selectColumn: 'select',
    expandColumn: 'expand',
    paginationLimit: 'Limit',
    paginationRange: 'Results'
};
var DataTableFilter = (function () {
    function DataTableFilter() {
        this.orderByFieldId = null;
        this.orderAscending = true;
        this.pageOffset = 0;
        this.pageSize = 10;
    }
    DataTableFilter.prototype.getPageIndex = function () {
        return Math.floor(this.pageOffset / this.pageSize) + 1;
    };
    DataTableFilter.prototype.setOffsetToPageIndex = function (value) {
        this.pageOffset = (value - 1) * this.pageSize;
    };
    return DataTableFilter;
}());
exports.DataTableFilter = DataTableFilter;
//# sourceMappingURL=data-table.model.js.map