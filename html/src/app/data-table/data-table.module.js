"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var data_table_component_1 = require("./data-table.component");
var data_table_column_selector_component_1 = require("./data-table-column-selector.component");
var column_component_1 = require("./column.component");
var row_component_1 = require("./row.component");
var pagination_component_1 = require("./pagination.component");
var pixel_converter_pipe_1 = require("./pixel-converter.pipe");
var hide_directive_1 = require("./hide.directive");
var min_1 = require("./min");
var demo_component_1 = require("./demo/demo.component");
var data_table_demo1_1 = require("./demo/demo1/data-table-demo1");
var data_table_demo2_1 = require("./demo/demo2/data-table-demo2");
var data_table_demo3_1 = require("./demo/demo3/data-table-demo3");
var DataTableModule = (function () {
    function DataTableModule() {
    }
    return DataTableModule;
}());
DataTableModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ngx_bootstrap_1.Ng2BootstrapModule
        ],
        declarations: [
            data_table_component_1.DataTableComponent,
            data_table_column_selector_component_1.DataTableColumnSelectorComponent,
            column_component_1.DataTableColumn,
            row_component_1.DataTableRow,
            pagination_component_1.DataTablePagination,
            pixel_converter_pipe_1.PixelConverterPipe,
            hide_directive_1.HideDirective,
            min_1.MinPipe,
            demo_component_1.DataTableDemoComponent,
            data_table_demo1_1.DataTableDemo1,
            data_table_demo2_1.DataTableDemo2,
            data_table_demo3_1.DataTableDemo3
        ],
        exports: [data_table_component_1.DataTableComponent, data_table_column_selector_component_1.DataTableColumnSelectorComponent, column_component_1.DataTableColumn, demo_component_1.DataTableDemoComponent, data_table_demo1_1.DataTableDemo1, data_table_demo2_1.DataTableDemo2, data_table_demo3_1.DataTableDemo3],
        providers: []
    })
], DataTableModule);
exports.DataTableModule = DataTableModule;
//# sourceMappingURL=data-table.module.js.map