import { NgModule }                               from '@angular/core';
import { Routes, RouterModule }                   from '@angular/router';
import { CommonModule }                           from '@angular/common';
import { FormsModule }                            from '@angular/forms';
import { SharedModule }                           from '../core/shared.module';
import { DataFileEditorModule }                   from '../data-file-editor/data-file-editor.module';
import { CreationTaskComponent }                  from './component/creation-task.component';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    SharedModule,
    DataFileEditorModule,
    RouterModule.forChild(
    [
        { path: '', component: CreationTaskComponent },
        { path: 'creation-task', component: CreationTaskComponent, data: { id: "CREATION_TASK" } }
    ])
  ],
  declarations: 
  [ 
    CreationTaskComponent
  ],
  exports:      [ CreationTaskComponent ],
  providers:    [ CreationTaskComponent ]
})
export class MaterialCreationWorkflowModule 
{
}