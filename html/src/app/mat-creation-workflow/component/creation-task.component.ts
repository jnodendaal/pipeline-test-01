import { Component, ViewChild, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { Observable, Subscription } from 'rxjs/Rx';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { WorkflowManagerService } from '../../core/service/workflow-manager.service';
import { FunctionalityAccessHandle } from '../../core/model/functionality.model';
import { DataFilter } from '../../core/model/data-filter.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FlowStatus, Task, TaskCompletionResultType } from '../../core/model/workflow.model';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { Context } from '../../core/model/security.model';
import { CoreComponent } from '../../core/component/core.component';
import { ComponentState } from '../../core/model/component.model';

@Component
({
    selector: 'creation-task',
    templateUrl: 'creation-task.component.html',
    styleUrls: ['creation-task.component.css'],
    providers: [ ComponentManagerService ]
})
export class CreationTaskComponent extends CoreComponent<CreationTaskState> implements OnInit, OnDestroy
{   
    private workflowManager: WorkflowManagerService;
    private actionConfirmationVisible: boolean;
    private selectedAction: TaskAction;
    private loading: boolean;
    private status: FlowStatus;
    private taskStatus: string;
    private unsavedChanges: boolean;

    @ViewChild('dataFileEditor')
    private dataFileEditor: DataFileEditorComponent;

    @ViewChild('actionConfirmationModal') 
    private actionConfirmationModel: ModalDirective;

    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, workflowManager: WorkflowManagerService, componentManager: ComponentManagerService)
    {
        super(router, route, functionalityManager, componentManager);
        this.loading = false;
        this.taskStatus = "NEW";
        this.unsavedChanges = false;
    }

    public ngOnInit()
    {
        // Load the request record.  This call must be done asynchronously, to allow UI display of detail view to take effect.
        setTimeout(() => this.dataFileEditor.openFile(this.parameters.requestRecordId));
    }

    public ngOnDestroy()
    {
        this.releaseFunctionality();
    }

    public saveTask()
    {
        this.unsavedChanges = false;
        this.dataFileEditor.saveFile()
        .subscribe
            (
                data => 
                {
                    if (data.isSuccess())
                    {
                        this.componentManager.toastSuccess("Success", "Task changes saved successfully");
                        this.taskStatus = "SAVED";
                    }
                    else if (data.isValidationFailure())
                    {
                        this.componentManager.toastError("Validation Failure", "The record contains invalid data the must be corrected before it can be saved.");
                        this.taskStatus = "VALIDATION_FAILURE";
                    }
                    else
                    {
                        this.componentManager.toastError("Technical Failure", "The record could not be saved due to a technical failure.  Please contact your administrator.");
                        this.taskStatus = "TECHNICAL_FAILURE";
                    }
                },
                error => 
                {
                    console.log("Error while saving task: " + error);
                    this.loading = false;
                }
            );
    }

    private onFileUpdate()
    {
        this.unsavedChanges = true;
        setTimeout(() =>
        {
            if (this.unsavedChanges) // We wait a little while before updating the status because we want to prevent a flicker between states while saving.
            {
                if (this.taskStatus != "VALIDATION_FAILURE")
                {
                    this.taskStatus = "UNSAVED";
                }
            }
        }, 500);
    }

    private showActionConfirmation(action: TaskAction)
    {
        this.selectedAction = action;
        this.actionConfirmationVisible = true;
    }

    private hideActionConfirmation()
    {
        this.actionConfirmationModel.hide();
    }

    private onActionConfirmationHidden()
    {
        this.selectedAction = null;
        this.actionConfirmationVisible = false;
    }

    private confirmationSelectedAction()
    {
        this.hideActionConfirmation();
        this.performTaskAction(this.selectedAction);
    }

    public performTaskAction(action: TaskAction)
    {
        let outputParameters: any;

        // Compile the task output parameters.
        outputParameters = 
        {
            [this.parameters.actionParameterId] : action.actionId
        }

        // Complete the task and process the output parameters.
        this.workflowManager.completeTask(this.ctx, this.parameters.taskIid, outputParameters)
        .subscribe
            (
                data => 
                {
                    this.loading = false;
                    console.log(data);
                    
                    // Navigate to the home page.
                    switch (data.resultType)
                    {
                        case TaskCompletionResultType.SUCCESS:
                            console.log("Task completed successfully.");
                            this.componentManager.toastSuccess("Success", "Task completed successfully");
                            setTimeout(() => this.router.navigate(["/main/home"]));
                            break;
                        case TaskCompletionResultType.FAILURE_ACCESS:
                            console.log("Task completion failure: task not accessible by current user.");
                            this.componentManager.toastError("Failure", "You do not have access to complete this task");
                            break;
                        case TaskCompletionResultType.FAILURE_CLAIMED:
                            console.log("Task completion failure: task already claimed by another user.");
                            this.componentManager.toastError("Failure", "This task has already been claimed by another user");
                            break;
                        case TaskCompletionResultType.FAILURE_COMPLETED:
                            console.log("Task completion failure: task already completed.");
                            this.componentManager.toastError("Failure", "This task has already been completed by another user");
                            break;                            
                        case TaskCompletionResultType.FAILURE_VALIDATION:
                            console.log("Task completion failure: task validation failed.");
                            this.componentManager.toastError("Failure", "Some of the data entered is invalid");
                            // TODO:  Handle task completion.
                            break;                                                        
                    }
                },
                error => 
                {
                    console.log("Error while completing task: " + error);
                    this.componentManager.toastError("A technical problem occurred - please try again", "Failure");
                    this.loading = false;
                }
            );
    }

    protected createState(): CreationTaskState
    {
        this.state = new CreationTaskState();
        return this.state;
    }

    protected getState(): CreationTaskState
    {
        return this.state;
    }

    protected setState(state: CreationTaskState)
    {
        this.state = state;
    }
}

export interface ComponentParameters
{
    taskId: string;
    taskIid: string;
    taskName: string;
    taskDescription: string;
    requestRecordId: string;
    actionParameterId: string;
    forwardActions: TaskAction[];
    backwardActions: TaskAction[];
}

export interface TaskAction
{
    actionId: string;
    text: string;
    tooltip: string;
    confirmationHeader: string;
    confirmationText: string;
}

export class CreationTaskState extends ComponentState
{
}