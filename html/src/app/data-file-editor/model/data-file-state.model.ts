import 
{ 
    DataRecord, 
    RecordSection, 
    RecordProperty, 
    RecordValue,
    StringValue,
    TextValue,
    Time,
    DateTime,
    DateValue,
    NumberValue,
    MeasuredNumber,
    MeasuredRange,
    ControlledConcept,
    UnitOfMeasure,
    QualifierOfMeasure,
    AttachmentList,
    DocumentReferenceList,
    Composite,
    Field,
    BooleanValue
} from '../../core/model/data-record.model';

import 
{ 
    DataRequirementInstance, 
    SectionRequirement, 
    PropertyRequirement, 
    ValueRequirement,
    StringRequirement,
    TextRequirement,
    TimeRequirement,
    DateRequirement,
    DateTimeRequirement,
    NumberRequirement,
    MeasuredNumberRequirement,
    MeasuredRangeRequirement,
    ControlledConceptRequirement,
    UnitOfMeasureRequirement,
    QualifierOfMeasureRequirement,
    AttachmentListRequirement,
    DocumentReferenceListRequirement, 
    CompositeRequirement,
    FieldRequirement,
    BooleanRequirement
} from '../../core/model/data-requirement.model';

import 
{ 
    DataFileValidationReport, 
    ValidationError, 
    RecordValueValidationError,
    DataFileUpdate 
} from './data-file-editor.model';

import { RecordAccessLayer, SectionAccessLayer, PropertyAccessLayer } from '../../core/model/data-record-access.model';

/*
    State Model
*/

// The StateController is not necessarily part of the state but provides functions that grant access to parts of the entire state model.
export interface StateController
{
    type: string;

    changesSaved(): void;
    processLocalUpdate(update: DataFileUpdate): void;
    processRemoteUpdate(update: DataFileUpdate): void;
    setValidationReport(report : DataFileValidationReport): void;
}

export abstract class FileState implements StateController
{
    file: DataRecord;
    type: string;

    constructor(type: string, file: DataRecord)
    {
        this.type = type;
        this.file = file;
    }

    public abstract changesSaved(): void;
    public abstract processLocalUpdate(update: DataFileUpdate): void;
    public abstract processRemoteUpdate(update: DataFileUpdate): void;
    public abstract setValidationReport(report : DataFileValidationReport): void;
}

export class RecordState
{
    controller: StateController;
    headerVisible: boolean;
    loading: boolean;
    temporaryId: string; // Used when a record has been temporarily created while the full creation is pending on the server.
    record: DataRecord;
    requirement: DataRequirementInstance;
    access: RecordAccessLayer;
    parentState: PageState;
    sectionStates: SectionState[];
    validationErrors: RecordValueValidationError[];
    updated: boolean;

    constructor(controller: StateController, record: DataRecord)
    {
        this.controller = controller;
        this.record = record;
        this.requirement = record != null ? record.drInstance : null;
        this.access = record != null ? record.access : null;
        this.headerVisible = true;
        this.sectionStates = new Array();
        this.validationErrors = new Array();
        this.loading = false;
        this.temporaryId = null;
    }

    public setParentState(state: PageState)
    {
        this.parentState = state;
    }

    public addSectionState(section: SectionState)
    {
        section.setParentState(this);
        this.sectionStates.push(section);
    }

    public getReferencePropertyState(): PropertyState
    {
        if (this.parentState)
        {
            let referencePageState: PageState;
            
            referencePageState = this.parentState.getParentPageState();
            if (referencePageState)
            {
                let referenceProperty: RecordProperty;
                let parentRecordState: RecordState;

                referenceProperty = this.parentState.referenceProperty;
                parentRecordState = referencePageState.getRecordState(referenceProperty.getRecord().recordId);
                return parentRecordState.getPropertyState(referenceProperty.propertyId);
            }
            else return null;
        }
        else return null;
    }

    public getPropertyState(propertyId: string): PropertyState
    {
        for (let sectionState of this.sectionStates)
        {
            let propertyState: PropertyState;

            propertyState = sectionState.getPropertyState(propertyId);
            if (propertyState) return propertyState;
        }

        return null;
    }

    public getPropertyStates(): PropertyState[]
    {
        let propertyStates: PropertyState[];

        propertyStates = new Array();
        for (let sectionState of this.sectionStates)
        {
            propertyStates.push(...sectionState.propertyStates);
        }

        return propertyStates;
    }

    public hasValidationErrors(): boolean
    {
        return this.validationErrors.length > 0;
    }

    public setValidationReport(report: DataFileValidationReport): void
    {
        this.validationErrors.length = 0;
        if (report) this.validationErrors.push(...report.getValidationErrors(this.record.recordId, null, null));
        for (let sectionState of this.sectionStates)
        {
            sectionState.setValidationReport(report);
        }
    }

    public setUpdated(updated: boolean)
    {
        this.updated = updated;
        if (updated) // If the update flag is true, propagate it to the parent states.
        {
            this.parentState.setUpdated(true);
        }
        else // If the updated flag is false, propagate it to the child states.
        {
            for (let sectionState of this.sectionStates)
            {
                sectionState.setUpdated(false);
            }
        }
    }    
}

export class SectionState
{
    id: string;
    record: DataRecord;
    section: RecordSection;
    requirement: SectionRequirement;
    access: SectionAccessLayer;
    parentState: RecordState;
    propertyStates: PropertyState[];
    headerVisible: boolean;
    updated: boolean;

    constructor(record: DataRecord, requirement: SectionRequirement, section: RecordSection)
    {
        this.id = requirement.sectionId;
        this.record = record;
        this.requirement = requirement;
        this.access = record.access.getSectionAccessLayer(this.id);
        this.section = section;
        this.headerVisible = true;
        this.propertyStates = new Array();
    }

    public setParentState(state: RecordState)
    {
        this.parentState = state;
    }

    public addPropertyState(property: PropertyState)
    {
        property.setParentState(this);
        this.propertyStates.push(property);
    }

    public getPropertyState(propertyId: string): PropertyState
    {
        for (let propertyState of this.propertyStates)
        {
            if (propertyState.property.propertyId == propertyId) return propertyState;
        }

        return null;
    }

    public setValidationReport(report: DataFileValidationReport): void
    {
        for (let propertyState of this.propertyStates)
        {
            propertyState.setValidationReport(report);
        }
    }

    public resetUpdatedFlag()
    {
        for (let propertyState of this.propertyStates)
        {
            propertyState.setUpdated(false);
        }
    }

    public setUpdated(updated: boolean)
    {
        this.updated = updated;
        if (updated)
        {
            this.parentState.setUpdated(true);
        }
        else
        {
            for (let propertyState of this.propertyStates)
            {
                propertyState.setUpdated(false);
            }
        }
    }
}

export class PropertyState
{
    id: string;
    visible: boolean;
    updated: boolean;
    record: DataRecord;
    property: RecordProperty;
    requirement: PropertyRequirement;
    access: PropertyAccessLayer;
    parentState: SectionState;
    valueState: ValueState;
    validationErrors: RecordValueValidationError[];

    constructor(record: DataRecord, requirement: PropertyRequirement, property: RecordProperty)
    {
        this.id = requirement.propertyId;
        this.visible = true;
        this.record = record;
        this.requirement = requirement;
        this.property = property;
        this.validationErrors = new Array();
        this.updated = false;
        this.access = property.parentRecord.access.getPropertyAccessLayer(this.id);
    }

    public setParentState(state: SectionState)
    {
        this.parentState = state;
    }

    public addValueState(state: ValueState)
    {
        this.valueState = state;
        this.valueState.setPropertyState(this);
    }

    public hasValidationErrors(): boolean
    {
        return this.validationErrors.length > 0;
    }

    public setValidationReport(report: DataFileValidationReport): void
    {
        this.validationErrors.length = 0;
        if (report) this.validationErrors.push(...report.getValidationErrors(this.record.recordId, this.id, null));
    }

    public setUpdated(updated: boolean)
    {
        this.updated = updated;
        if (updated)
        {
            this.parentState.setUpdated(true);
        }
    }
}

export class ValueState
{
    controller: StateController;
    type: string;
    record: DataRecord;
    requirement: ValueRequirement;
    value: RecordValue;
    propertyState: PropertyState;

    constructor(controller: StateController, record: DataRecord, requirement: ValueRequirement, value: RecordValue)
    {
        this.controller = controller;
        this.type = requirement.type;
        this.record = record;
        this.requirement = requirement;
        this.value = value;
    }

    public setPropertyState(propertyState: PropertyState)
    {
        this.propertyState = propertyState;
    }
}

export class StringState extends ValueState
{
    value: StringValue;
    requirement: StringRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: StringRequirement, value: StringValue)
    {
        super(controller, record, requirement, value);
    }
}

export class TextState extends ValueState
{
    value: TextValue;
    requirement: TextRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: TextRequirement, value: TextValue)
    {
        super(controller, record, requirement, value);
    }
}

export class ControlledConceptState extends ValueState
{
    value: ControlledConcept;
    requirement: ControlledConceptRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: ControlledConceptRequirement, value: ControlledConcept)
    {
        super(controller, record, requirement, value);
    }
}

export class UnitOfMeasureState extends ValueState
{
    value: UnitOfMeasure;
    requirement: UnitOfMeasureRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: UnitOfMeasureRequirement, value: UnitOfMeasure)
    {
        super(controller, record, requirement, value);
    }
}

export class QualifierOfMeasureState extends ValueState
{
    value: QualifierOfMeasure;
    requirement: QualifierOfMeasureRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: QualifierOfMeasureRequirement, value: QualifierOfMeasure)
    {
        super(controller, record, requirement, value);
    }
}

export class BooleanState extends ValueState
{
    value: BooleanValue;
    requirement: BooleanRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: BooleanRequirement, value: BooleanValue)
    {
        super(controller, record, requirement, value);
    }
}

export class TimeState extends ValueState
{
    value: Time;
    requirement: TimeRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: TimeRequirement, value: Time)
    {
        super(controller, record, requirement, value);
    }
}

export class DateTimeState extends ValueState
{
    value: DateTime;
    requirement: DateTimeRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: DateTimeRequirement, value: DateTime)
    {
        super(controller, record, requirement, value);
    }
}

export class DateState extends ValueState
{
    value: DateValue;
    requirement: DateRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: DateTimeRequirement, value: DateValue)
    {
        super(controller, record, requirement, value);
    }
}

export class CompositeState extends ValueState
{
    value: Composite;
    requirement: CompositeRequirement;
    fieldStates: FieldState[];

    constructor(controller: StateController, record: DataRecord, requirement: CompositeRequirement, value: Composite)
    {
        super(controller, record, requirement, value);
        this.fieldStates = new Array();
    }

    public addFieldState(fieldState: FieldState)
    {
        fieldState.setPropertyState(this.propertyState);
        this.fieldStates.push(fieldState);
    }

    public setPropertyState(propertyState: PropertyState)
    {
        this.propertyState = propertyState;
        for (let fieldState of this.fieldStates)
        {
            fieldState.setPropertyState(propertyState);
        }
    }
}

export class FieldState extends ValueState
{
    value: Field;
    requirement: FieldRequirement;
    valueState: ValueState;

    constructor(controller: StateController, record: DataRecord, requirement:FieldRequirement, value: Field)
    {
        super(controller, record, requirement, value);
    }

    public setPropertyState(propertyState: PropertyState)
    {
        this.propertyState = propertyState;
        if (this.valueState)
        {
            this.valueState.setPropertyState(propertyState);
        }
    }

    public setValueLayout(state: ValueState)
    {
        state.propertyState = this.propertyState;
        this.valueState = state;
    }
}

export class NumberState extends ValueState
{
    value: NumberValue;
    requirement: NumberRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: NumberRequirement, value: NumberValue)
    {
        super(controller, record, requirement, value);
    }
}

export class MeasureNumberState extends ValueState
{
    value: MeasuredNumber;
    requirement: MeasuredNumberRequirement;
    unitOfMeasureState: UnitOfMeasureState;

    constructor(controller: StateController, record: DataRecord, requirement: MeasuredNumberRequirement, value: MeasuredNumber)
    {
        super(controller, record, requirement, value);
    }

    public setPropertyState(propertyState: PropertyState)
    {
        this.propertyState = propertyState;
        if (this.unitOfMeasureState) this.unitOfMeasureState.setPropertyState(propertyState);
    }

    public setUnitOfMeasureLayout(state: UnitOfMeasureState)
    {
        state.setPropertyState(this.propertyState);
        this.unitOfMeasureState = state;
    }
}

export class MeasuredRangeState extends ValueState
{
    value: MeasuredRange;
    requirement: MeasuredRangeRequirement;
    unitOfMeasureState: UnitOfMeasureState;

    constructor(controller: StateController, record: DataRecord, requirement: MeasuredRangeRequirement, value: MeasuredRange)
    {
        super(controller, record, requirement, value);
    }

    public setPropertyState(propertyState: PropertyState)
    {
        this.propertyState = propertyState;
        if (this.unitOfMeasureState) this.unitOfMeasureState.setPropertyState(propertyState);
    }

    public setUnitOfMeasureLayout(state: UnitOfMeasureState)
    {
        state.setPropertyState(this.propertyState);
        this.unitOfMeasureState = state;
    }
}

export class AttachmentListState extends ValueState
{
    value: AttachmentList;
    requirement: AttachmentListRequirement;

    constructor(controller: StateController, record: DataRecord, requirement: AttachmentListRequirement, value: AttachmentList)
    {
        super(controller, record, requirement, value);
    }
}

export class DocumentReferenceListState extends ValueState
{
    pullUp: boolean;
    referenceRecordStates: RecordState[];
    temporaryRecordStateIds: string[];

    constructor(controller: StateController, record: DataRecord, requirement: DocumentReferenceListRequirement, value: DocumentReferenceList)
    {
        super(controller, record, requirement, value);
        this.referenceRecordStates = new Array();
        this.temporaryRecordStateIds = new Array();
    }

    public addReferenceRecordState(recordState: RecordState)
    {
        this.referenceRecordStates.push(recordState);
    }

    public addReferenceRecordStates(recordStates: RecordState[])
    {
        this.referenceRecordStates.push(...recordStates);
    }

    public getReferenceRecordState(recordId: string)
    {
        for (let recordState of this.referenceRecordStates)
        {
            if (recordState.record.recordId == recordId)
            {
                return recordState;
            }
        }
    }

    public addTemporaryRecordStateId(temporaryId: string)
    {
        this.temporaryRecordStateIds.push(temporaryId);
    }

    public removeTemporaryRecordStateId(temporaryId: string)
    {
        let index: number;

        index = this.temporaryRecordStateIds.indexOf(temporaryId);
        if (index > -1)
        {
            this.temporaryRecordStateIds.splice(index, 1);
        }
    }
}

export class PagedFileState extends FileState
{
    pageStates: PageState[];
    validationErrors: RecordValueValidationError[];
    updated: boolean;

    constructor(file: DataRecord)
    {
        super('PAGED', file);
        this.pageStates = new Array();
        this.validationErrors = new Array();
        this.updated = false;
    }

    public changesSaved()
    {
        this.setUpdated(false);
    }

    public processLocalUpdate(update: DataFileUpdate)
    {
        for (let recordCreation of update.recordCreations)
        {
            let referencingPropertyState: PropertyState;
            let documentReferenceState: DocumentReferenceListState;
            let parentRecordState: RecordState;
            let newRecordState: RecordState;
            let parentPageState: PageState;
            let pageState: PageState;
            let pageId: string;

            // Get the page layout to which we will add the new record layout.  If it does not exist, create it.
            pageId = recordCreation.recordId + ":" + recordCreation.propertyId;
            pageState = this.getPageState(pageId);
            if (pageState == null)
            {
                let referencingPropertyRequirement: PropertyRequirement;
                let referencingProperty: RecordProperty;
                let referencingRecord: DataRecord;

                // Get referencing information.
                parentPageState = this.getPageStateContainingRecord(recordCreation.recordId);
                parentRecordState = parentPageState.getRecordState(recordCreation.recordId);
                referencingPropertyState = parentRecordState.getPropertyState(recordCreation.propertyId);
                referencingRecord = parentRecordState.record;
                referencingProperty = referencingPropertyState.property;
                referencingPropertyRequirement = referencingRecord.drInstance.getProperty(recordCreation.propertyId);

                // Create the new page state.
                pageState = new PageState(this, pageId, referencingPropertyRequirement.term, referencingRecord.getFile(), parentPageState, referencingProperty);
                parentPageState.addChildPageState(pageState);
                this.addPageState(pageState);
            }
            else
            {
                // The target page state was found, so just get its parent.
                parentPageState = pageState.parentPageState;
                parentRecordState = parentPageState.getRecordState(recordCreation.recordId);
                referencingPropertyState = parentRecordState.getPropertyState(recordCreation.propertyId);
            }

            // Add a new temporary record state to the page, setting it to the 'loading' state.
            newRecordState = new RecordState(this, null);
            newRecordState.loading = true;
            newRecordState.temporaryId = recordCreation.temporaryId;
            newRecordState.access = new RecordAccessLayer();
            newRecordState.access.visible = true;
            pageState.addRecordState(newRecordState);

            // Add the temporary document references.
            documentReferenceState = referencingPropertyState.valueState as DocumentReferenceListState;
            documentReferenceState.addTemporaryRecordStateId(newRecordState.temporaryId);
        }
    }

    public processRemoteUpdate(update: DataFileUpdate)
    {
        // Add any remotely created new records to the local layout.
        for (let newRecordUpdate of update.newRecords)
        {
            // If a temporary id is set on the newRecordUpdate it indicates that the new record is a response to a creation previously initiated on the client.
            if (newRecordUpdate.temporaryId)
            {
                let recordState: RecordState;

                recordState = this.getRecordStateByTemporaryId(newRecordUpdate.temporaryId);
                if (recordState != null)
                {
                    let referencePropertyState: PropertyState;
                    let documentReferenceState: DocumentReferenceListState;
                    let stateConstructor: PageStateConstructor;
                    let newRecord: DataRecord;
                    let newState: RecordState;

                    // Create a new record state from the newly created record.
                    stateConstructor = new PageStateConstructor();
                    newRecord = newRecordUpdate.newRecord;
                    newState = stateConstructor.createRecordState(this, newRecord);

                    // Transfer all the new state content to this state.
                    recordState.record = newState.record;                    
                    recordState.requirement = newState.requirement;
                    recordState.headerVisible = newState.headerVisible;
                    for (let sectionState of newState.sectionStates)
                    {
                        recordState.addSectionState(sectionState);
                    }

                    // Remove the temporary id from the referencing property state.
                    referencePropertyState = recordState.getReferencePropertyState();
                    documentReferenceState = referencePropertyState.valueState as DocumentReferenceListState;
                    documentReferenceState.removeTemporaryRecordStateId(recordState.temporaryId);
                    documentReferenceState.addReferenceRecordState(recordState);

                    // Finally, unset the loading indicators so that this state can now be properly displayed.
                    recordState.temporaryId = null;
                    recordState.loading = false;
                }
            }
            else // A new creation that was not initiated on the client.
            {
                let newRecordState: RecordState;
                let constructor: PageStateConstructor;

                // Create the new record state.
                constructor = new PageStateConstructor();
                newRecordState = constructor.createRecordState(this, newRecordUpdate.newRecord);

                // Add the record state to this model.
                this.addRecordState(newRecordUpdate.recordId, newRecordUpdate.propertyId, newRecordState);
            }
        }

        // Remove any remotely deleted records from the local layout.
        for (let recordDeletion of update.recordDeletions)
        {
            let targetPageState: PageState;

            // Get the target record from which the record has been deleted.
            targetPageState = this.getPageStateContainingRecord(recordDeletion.subRecordId);
            if (targetPageState)
            {
                targetPageState.removeRecordState(recordDeletion.subRecordId);
            }
        }
    }

    public setValidationReport(report: DataFileValidationReport): void
    {
        this.validationErrors.length = 0;
        if (report) this.validationErrors.push(...report.getValidationErrors(null, null, null));
        for (let pageState of this.pageStates)
        {
            pageState.setValidationReport(report);
        }
    }

    public hasValidationErrors(): boolean
    {
        for (let pageState of this.pageStates)
        {
            if (pageState.hasValidationErrors()) return true;
        }

        return false;
    }

    public getPageState(pageId: string): PageState
    {
        for (let pageState of this.pageStates)
        {
            if (pageState.id == pageId)
            {
                return pageState;
            }
        }

        return null;
    }

    public addRecordState(parentRecordId: string, parentPropertyId: string, newState: RecordState): PageState
    {
        let pageState: PageState;
        let pageId: string;

        // Get the page layout to which we will add the new record layout.  If it does not exist, create it.
        pageId = parentRecordId + ":" + parentPropertyId;
        pageState = this.getPageState(pageId);
        if (pageState == null)
        {
            let referencingPropertyRequirement: PropertyRequirement;
            let referencingPropertyState: PropertyState;
            let referencingProperty: RecordProperty;
            let referencingRecord: DataRecord;
            let parentRecordState: RecordState;
            let parentPageState: PageState;

            // Get referencing information.
            parentPageState = this.getPageStateContainingRecord(parentRecordId);
            parentRecordState = parentPageState.getRecordState(parentRecordId);
            referencingPropertyState = parentRecordState.getPropertyState(parentPropertyId);
            referencingRecord = parentRecordState.record;
            referencingProperty = referencingPropertyState.property;
            referencingPropertyRequirement = referencingRecord.drInstance.getProperty(parentPropertyId);

            // Create the new page state.
            pageState = new PageState(this, pageId, referencingPropertyRequirement.term, referencingRecord.getFile(), parentPageState, referencingProperty);
            parentPageState.addChildPageState(pageState);
            this.addPageState(pageState);

            // Add the new record state to the new page.
            pageState.addRecordState(newState);
            return pageState;
        }
        else
        {
            // Add the new record state to the new page.
            pageState.addRecordState(newState);
            return pageState;
        }
    }

    public addPageState(page: PageState)
    {
        page.setParentState(this);
        this.pageStates.push(page);
    }

    public getRootPageState(): PageState
    {
        for (let pageState of this.pageStates)
        {
            if (pageState.getParentPageState() == null)
            {
                return pageState;
            }
        }

        return null;
    }

    public getFirstPageState(): PageState
    {
        return this.pageStates.length > 0 ? this.pageStates[0] : null;
    }

    public getPageStateContainingRecord(recordId: string): PageState
    {
        for (let pageState of this.pageStates)
        {
            if (pageState.containsRecordState(recordId))
            {
                return pageState;
            }
        }

        return null;
    }

    public getRecordState(recordId: string): RecordState
    {
        for (let pageState of this.pageStates)
        {
            let recordState: RecordState;

            recordState = pageState.getRecordState(recordId);
            if (recordState != null) return recordState;
        }

        return null;
    }

    public getRecordStateByTemporaryId(temporaryId: string): RecordState
    {
        for (let pageState of this.pageStates)
        {
            let recordState: RecordState;

            recordState = pageState.getRecordStateByTemporaryId(temporaryId);
            if (recordState != null) return recordState;
        }

        return null;
    }

    public getPropertyStates(): PropertyState[]
    {
        let propertyStates: PropertyState[];

        propertyStates = new Array();
        for (let pageState of this.pageStates)
        {
            propertyStates.push(...pageState.getPropertyStates());
        }

        return propertyStates;
    }

    public getPropertyState(recordId: string, propertyId: string): PropertyState
    {
        for (let pageState of this.pageStates)
        {
            let recordState: RecordState;

            recordState = pageState.getRecordState(recordId);
            if (recordState != null)
            {
                return recordState.getPropertyState(propertyId);
            }
        }

        return null;
    }

    public setUpdated(updated: boolean)
    {
        this.updated = updated;
        if (!updated) // If the updated flag is false, propagate it to the child states.
        {
            for (let pageState of this.pageStates)
            {
                pageState.setUpdated(false);
            }
        }
    }    
}

export class PageState
{
    controller: StateController;
    id: string;
    title: string;
    file: DataRecord;
    fileState: PagedFileState;
    referenceProperty: RecordProperty;
    recordStates: RecordState[];
    validationErrors: ValidationError[];
    childPageStates: PageState[];
    parentPageState: PageState;
    updated: boolean;

    constructor(controller: StateController, id: string, title: string, file: DataRecord, parentPageState: PageState, referenceProperty: RecordProperty)
    {
        this.controller = controller;
        this.title = title
        this.id = id;
        this.file = file;
        this.referenceProperty = referenceProperty;
        this.recordStates = new Array();
        this.validationErrors = new Array();
        this.childPageStates = new Array();
        this.parentPageState = parentPageState;
    }

    public getParentRecordId(): string
    {
        return this.referenceProperty != null ? this.referenceProperty.getRecord().recordId : null;
    }

    public setParentState(state: PagedFileState)
    {
        this.fileState = state;
    }

    public addRecordState(recordState: RecordState)
    {
        recordState.setParentState(this);
        this.recordStates.push(recordState);
    }

    public addRecordStates(states: RecordState[])
    {
        for (let recordState of states)
        {
            this.addRecordState(recordState);
        }
    }

    public removeRecordState(recordId: string) : RecordState
    {
        for (let index = 0; index < this.recordStates.length; index++)
        {
            let recordState: RecordState;

            recordState = this.recordStates[index];
            if (recordState.record.recordId == recordId)
            {
                this.recordStates.splice(index, 1);
                return recordState;
            }
        }

        return null;
    }

    public getRecordState(recordId: string): RecordState
    {
        for (let recordState of this.recordStates)
        {
            if (recordState.record.recordId == recordId) return recordState;
        }

        return null;
    }

    public getRecordStateByTemporaryId(temporaryId: string): RecordState
    {
        for (let recordState of this.recordStates)
        {
            if (recordState.temporaryId == temporaryId) return recordState;
        }

        return null;
    }

    public containsRecordState(recordId: string): boolean
    {
        return this.getRecordState(recordId) != null;
    }
    
    public getPropertyStates(): PropertyState[]
    {
        let propertyStates: PropertyState[];

        propertyStates = new Array();
        for (let recordLayout of this.recordStates)
        {
            propertyStates.push(...recordLayout.getPropertyStates());
        }

        return propertyStates;
    }

    public getPageStatePath(): PageState[]
    {
        let path: PageState[];
        let parent: PageState;

        path = new Array();
        parent = this.parentPageState;
        while (parent != null)
        {
            path.push(parent);
            parent = parent.parentPageState;
        }

        path = path.reverse();
        return path;
    }

    public getParentPageState(): PageState
    {
        return this.parentPageState;
    }

    public setParentPageState(parentState: PageState)
    {
        this.parentPageState = parentState;
    }

    public getChildPageStatesByParentRecordId(recordId: string): PageState[]
    {
        let pageStates;

        pageStates = new Array();
        for (let childPageState of this.childPageStates)
        {
            if (childPageState.getParentRecordId() == recordId)
            {
                pageStates.push(childPageState);
            }
        }

        return pageStates;
    }

    public addChildPageState(childPageState: PageState)
    {
        childPageState.setParentPageState(this);
        this.childPageStates.push(childPageState);
    }

    public removeChildPageState(childPageState: PageState)
    {
        let index: number;

        index = this.childPageStates.indexOf(childPageState);
        if (index > -1)
        {
            childPageState.setParentPageState(null);
            this.childPageStates.splice(index, 1);
        }
    }

    public hasValidationErrors(): boolean
    {
        return this.validationErrors.length > 0;
    }

    public setValidationReport(report: DataFileValidationReport): void
    {
        this.validationErrors.length = 0;
        
        for (let recordState of this.recordStates)
        {
            if (report) this.validationErrors.push(...report.getValidationErrors(recordState.record.recordId, null, null));
            recordState.setValidationReport(report);
        }

        for (let pageState of this.childPageStates)
        {
            pageState.setValidationReport(report);
        }
    }

    public setUpdated(updated: boolean)
    {
        this.updated = updated;
        if (updated) // If the update flag is true, propagate it to the parent states.
        {
            this.fileState.setUpdated(true);
        }
        else // If the updated flag is false, propagate it to the child states.
        {
            for (let recordState of this.recordStates)
            {
                recordState.setUpdated(false);
            }
        }
    }    
}

export class PageStateConstructor
{
    public createPagedFileState(file: DataRecord): PagedFileState
    {
        let state: PagedFileState;
        let rootPageLayout: PageState;

        // Add the root record on its own page.
        state = new PagedFileState(file);
        this.createPageState(state, file.recordId, file.drInstance.term, file, null, null, [file]);
        return state;
    }

    public createPageState(fileState: PagedFileState, id: string, title: string, file: DataRecord, parentPageLayout: PageState, referenceProperty: RecordProperty, records: DataRecord[]): PageState
    {
        let state: PageState;

        // Create the new page layout and add it to the file layout.
        state = new PageState(fileState, id, title, file, parentPageLayout, referenceProperty);
        fileState.addPageState(state);

        // Now add all the record layouts contained by the page (and then recursively create al descendant page layouts).
        for (let record of records)
        {
            let recordState: RecordState;

            recordState = this.createRecordState(fileState, record);
            state.addRecordState(recordState);

            for (let propertyRequirement of record.drInstance.getProperties())
            {
                if (propertyRequirement.value.type == "DOCUMENT_REFERENCE")
                {
                    let propertyState: PropertyState;
                    let recordProperty: RecordProperty;

                    recordProperty = record.getProperty(propertyRequirement.propertyId);
                    propertyState = recordState.getPropertyState(propertyRequirement.propertyId);
                    if (recordProperty != null)
                    {
                        let documentReferenceListState: DocumentReferenceListState;
                        let documentReference: DocumentReferenceList;

                        documentReference = recordProperty.value as DocumentReferenceList;
                        documentReferenceListState = propertyState.valueState as DocumentReferenceListState;
                        if ((documentReference != null) && (documentReference.references.length > 0))
                        {
                            let childPageState: PageState;
                            let pageTitle: string;
                            let pageId: string;

                            pageId = record.recordId + ":" + propertyRequirement.propertyId;
                            pageTitle = propertyRequirement.term;
                            childPageState = fileState.getPageState(pageId);
                            if (childPageState == null)
                            {
                                let pageRecords: DataRecord[];

                                pageRecords = new Array();
                                for (let reference of documentReference.references)
                                {
                                    let referencedRecord: DataRecord;

                                    referencedRecord = record.getRecord(reference.conceptId);
                                    if (referencedRecord)
                                    {
                                        pageRecords.push(referencedRecord);
                                    }
                                }

                                // Create the child page layout and add it to the file layout.
                                childPageState = this.createPageState(fileState, pageId, pageTitle, file, state, recordProperty, pageRecords);
                                state.addChildPageState(childPageState);

                                // Add all referenced record layouts to the document reference layout.
                                documentReferenceListState.addReferenceRecordStates(childPageState.recordStates);
                            }
                        }
                    }
                }
            }
        }

        // Return the final layout.
        return state;
    }

    public createRecordState(controller: StateController, record: DataRecord): RecordState
    {
        let state: RecordState;

        state = new RecordState(controller, record);
        for (let sectionRequirement of record.drInstance.sections)
        {
            state.addSectionState(this.createSectionState(controller, record, sectionRequirement));
        }

        return state;
    }

    public createSectionState(controller: StateController, record: DataRecord, sectionRequirement: SectionRequirement): SectionState
    {
        let sectionId: string;
        let section: RecordSection;
        let state: SectionState;

        sectionId = sectionRequirement.sectionId;
        section = record.getSection(sectionId);
        state = new SectionState(record, sectionRequirement, section);
        for (let propertyRequirement of sectionRequirement.properties)
        {
            state.addPropertyState(this.createPropertyState(controller, record, section, propertyRequirement));
        }

        return state;
    }

    public createPropertyState(controller: StateController, record: DataRecord, section: RecordSection, propertyRequirement: PropertyRequirement): PropertyState
    {
        let state: PropertyState;
        let propertyId: string;
        let property: RecordProperty;
        let propertyLayout: PropertyState;

        propertyId = propertyRequirement.propertyId;
        property = section.getProperty(propertyId);
        if (property == null)
        {
            property = new RecordProperty(propertyId); 
            section.addProperty(property);
        } 

        // Fill the property, so that all possible value are present, even if empty.
        property.fill(true);  
        
        // Create the new property layout.
        state = new PropertyState(record, propertyRequirement, property);
        state.addValueState(this.createValueState(controller, record, property, propertyRequirement.value, property.value))
        return state;
    }

    public createValueState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: ValueRequirement, value: RecordValue): ValueState
    {
        switch (valueRequirement.type)
        {
            case "STRING":
                return this.createStringState(controller, record, property, valueRequirement, value as StringValue);
            case "TEXT":
                return this.createTextState(controller, record, property, valueRequirement, value as TextValue);
            case "BOOLEAN":
                return this.createBooleanState(controller, record, property, valueRequirement, value as BooleanValue);
            case "TIME":
                return this.createTimeState(controller, record, property, valueRequirement as TimeRequirement, value as Time);
            case "DATE":
                return this.createDateState(controller, record, property, valueRequirement as DateRequirement, value as DateValue);
            case "DATE_TIME":
                return this.createDateTimeState(controller, record, property, valueRequirement as DateTimeRequirement, value as DateTime);
            case "NUMBER":
                return this.createNumberState(controller, record, property, valueRequirement, value as NumberValue);
            case "REAL":
                return this.createNumberState(controller, record, property, valueRequirement, value as NumberValue);
            case "INTEGER":
                return this.createNumberState(controller, record, property, valueRequirement, value as NumberValue);
            case "MEASURED_NUMBER":
                return this.createMeasuredNumberState(controller, record, property, valueRequirement as MeasuredNumberRequirement, value as MeasuredNumber);
            case "MEASURED_RANGE":
                return this.createMeasuredRangeState(controller, record, property, valueRequirement as MeasuredRangeRequirement, value as MeasuredRange);
            case "CONTROLLED_CONCEPT":
                return this.createControlledConceptState(controller, record, property, valueRequirement, value as ControlledConcept);
            case "ATTACHMENT":
                return this.createAttachmentListState(controller, record, property, valueRequirement, value as AttachmentList);
            case "DOCUMENT_REFERENCE":
                return this.createDocumentReferenceListState(controller, record, property, valueRequirement, value as DocumentReferenceList);
            case "COMPOSITE":
                return this.createCompositeState(controller, record, property, valueRequirement as CompositeRequirement, value as Composite);
            case "FIELD":
                return this.createFieldState(controller, record, property, valueRequirement as FieldRequirement, value as Field);
            default:
                console.log("Unsupported layout type: " + valueRequirement.type);
                return new ValueState(controller, record, valueRequirement, value);                
        }
    }

    public createStringState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: StringRequirement, value: StringValue)
    {
        let state: StringState;

        state = new StringState(controller, record, valueRequirement, value);
        return state;
    }

    public createTextState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: TextRequirement, value: TextValue)
    {
        let state: TextState;

        state = new TextState(controller, record, valueRequirement, value);
        return state;
    }

    public createTimeState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: TimeRequirement, value: Time)
    {
        let state: TimeState;

        state = new TimeState(controller, record, valueRequirement, value);
        return state;
    }

    public createDateState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: DateRequirement, value: DateValue)
    {
        let state: DateState;

        state = new DateState(controller, record, valueRequirement, value);
        return state;
    }

    public createDateTimeState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: DateTimeRequirement, value: DateTime)
    {
        let state: DateTimeState;

        state = new DateTimeState(controller, record, valueRequirement, value);
        return state;
    }

    public createNumberState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: NumberRequirement, value: NumberValue)
    {
        let state: NumberState;

        state = new NumberState(controller, record, valueRequirement, value);
        return state;
    }

    public createCompositeState(controller: StateController, record: DataRecord, property: RecordProperty, compositeRequirement: CompositeRequirement, composite: Composite): CompositeState
    {
        let state: CompositeState;

        state = new CompositeState(controller, record, compositeRequirement, composite);
        for (let fieldRequirement of compositeRequirement.fields)
        {
            let field: Field;

            field = composite.getField(fieldRequirement.fieldId);
            state.addFieldState(this.createFieldState(controller, record, property, fieldRequirement, field));
        }

        return state;
    }

    public createFieldState(controller: StateController, record: DataRecord, property: RecordProperty, fieldRequirement: FieldRequirement, field: Field): FieldState
    {
        let fieldLayout: FieldState;

        fieldLayout = new FieldState(controller, record, fieldRequirement, field);
        fieldLayout.setValueLayout(this.createValueState(controller, record, property, fieldRequirement.value, field.value));
        return fieldLayout;
    }

    public createMeasuredNumberState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: MeasuredNumberRequirement, value: MeasuredNumber)
    {
        let state: MeasureNumberState;

        state = new MeasureNumberState(controller, record, valueRequirement, value);
        state.setUnitOfMeasureLayout(new UnitOfMeasureState(controller, record, valueRequirement.unitOfMeasure, value.unitOfMeasure));
        return state;
    }

    public createMeasuredRangeState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: MeasuredRangeRequirement, value: MeasuredRange)
    {
        let state: MeasuredRangeState;

        state = new MeasuredRangeState(controller, record, valueRequirement, value);
        state.setUnitOfMeasureLayout(new UnitOfMeasureState(controller, record, valueRequirement.unitOfMeasure, value.unitOfMeasure));
        return state;
    }

    public createBooleanState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: BooleanRequirement, value: BooleanValue)
    {
        let state: BooleanState;

        state = new BooleanState(controller, record, valueRequirement, value);
        return state;
    }

    public createControlledConceptState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: ControlledConceptRequirement, value: ControlledConcept)
    {
        let state: ControlledConceptState;

        state = new ControlledConceptState(controller, record, valueRequirement, value);
        return state;
    }

    public createAttachmentListState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: AttachmentListRequirement, value: AttachmentList)
    {
        let state: AttachmentListState;

        state = new AttachmentListState(controller, record, valueRequirement, value);
        return state;
    }

    public createDocumentReferenceListState(controller: StateController, record: DataRecord, property: RecordProperty, valueRequirement: DocumentReferenceListRequirement, value: DocumentReferenceList)
    {
        let state: DocumentReferenceListState;

        state = new DocumentReferenceListState(controller, record, valueRequirement, value);
        return state;
    }
}

