import 
{ 
    DataRecord, 
    RecordSection, 
    RecordProperty,
    ValueContent
} from '../../core/model/data-record.model';

import 
{ 
    DataRequirementInstance,
    SectionRequirement, 
    PropertyRequirement 
} from '../../core/model/data-requirement.model';
import { PropertyAccessLayer } from '../../core/model/data-record-access.model';

export class DataFileSaveResult
{
    private success: boolean;
    private validationReport: DataFileValidationReport;

    constructor()
    {
    }

    public fromJson(jsonResult: any): DataFileSaveResult
    {
        this.success = jsonResult.success;
        if (jsonResult.validationReport)
        {
            this.validationReport = new DataFileValidationReport();
            this.validationReport.fromJson(jsonResult.validationReport);
        }

        return this;
    }

    public isSuccess(): boolean
    {
        return this.success;
    }

    public isValidationFailure(): boolean
    {
        return (this.validationReport != null);
    }

    public getValidationReport(): DataFileValidationReport
    {
        return this.validationReport;
    }
}


export class DataFileValidationReport
{
    private recordReports: DataRecordValidationReport[];

    constructor()
    {
        this.recordReports = new Array();
    }

    public fromJson(jsonReport: any): DataFileValidationReport
    {
        if (jsonReport.recordReports)
        {
            for (let jsonRecordReport of jsonReport.recordReports)
            {
                let recordReport: DataRecordValidationReport;

                recordReport = new DataRecordValidationReport();
                recordReport.fromJson(jsonRecordReport);
                this.recordReports.push(recordReport);
            }
        }

        return this;
    }

    public getRecordValidationReport(recordId: string): DataRecordValidationReport
    {
        for (let report of this.recordReports)
        {
            if (report.getRecordId() == recordId)
            {
                return report;
            }
        }

        return null;
    }

    public getValidationErrors(recordId: string, propertyId: string, fieldId: string): RecordValueValidationError[]
    {
        if (recordId)
        {
            let recordReport: DataRecordValidationReport;

            recordReport = this.getRecordValidationReport(recordId);
            if (recordReport)
            {
                return recordReport.getValidationErrors(propertyId, fieldId);
            }
            else return new Array();
        }
        else
        {
            let errors: RecordValueValidationError[];

            errors = new Array();
            for (let report of this.recordReports)
            {
                errors.push(...report.getValidationErrors(null, null));
            }

            return errors;
        }
    }
}

export class DataRecordValidationReport
{
    private recordId: string;
    private valueErrors: RecordValueValidationError[]; 

    constructor()
    {
        this.valueErrors = new Array();
    }

    public fromJson(jsonReport: any): DataRecordValidationReport
    {
        this.recordId = jsonReport.recordId;
        if (jsonReport.valueErrors)
        {
            for (let jsonValueError of jsonReport.valueErrors)
            {
                let valueError: RecordValueValidationError;

                valueError = new RecordValueValidationError();
                valueError.fromJson(jsonValueError);
                this.valueErrors.push(valueError);
            }
        }

        return this;
    }

    public getRecordId(): string
    {
        return this.recordId;
    }

    public getValidationErrors(propertyId: string, fieldId: string): RecordValueValidationError[]
    {
        let errors: RecordValueValidationError[];

        errors = new Array();
        for (let error of this.valueErrors)
        {
            if ((!propertyId) || (propertyId == error.getPropertyId()))
            {
                if ((!fieldId) || (fieldId == error.getFieldId()))
                {
                    errors.push(error);
                }
            }
        }

        return errors;
    }
}

export abstract class ValidationError
{
    type: string;

    constructor(type: string)
    {
        this.type = type;
    }
}

export class RecordValueValidationError extends ValidationError
{
    private recordId: string;
    private propertyId: string;
    private fieldId: string;
    private message: string;

    constructor()
    {
        super("VALUE");
    }

    public fromJson(jsonError: any): RecordValueValidationError
    {
        this.recordId = jsonError.recordId;
        this.propertyId = jsonError.propertyId;
        this.fieldId = jsonError.fieldId;
        this.message = jsonError.message;
        return this;
    }

    public getRecordId()
    {
        return this.recordId;
    }

    public getPropertyId()
    {
        return this.propertyId;
    }

    public getFieldId()
    {
        return this.fieldId;
    }

    public getMessage()
    {
        return this.message;
    }
}


/*
    Data Record Update Model
*/
export class DataFileUpdate
{
    fileId: string;
    valueUpdates: DataFileValueUpdate[];
    recordCreations: DataFileRecordCreation[]; // Holds creations initiated but not yet completed.
    newRecords: DataFileNewRecord[]; // Holds new records to be added to the model.
    recordDeletions: DataFileRecordDeletion[];
    newAttachments: DataFileNewAttachment[];
    propertyAccessUpdates: PropertyAccessUpdate[];

    constructor(fileId: string)
    {
        this.fileId = fileId;
        this.valueUpdates = new Array();
        this.recordCreations = new Array();
        this.newRecords = new Array();
        this.recordDeletions = new Array();
        this.newAttachments = new Array();
        this.propertyAccessUpdates = new Array();
    }

    fromJson(jsonValue: any)
    {
        this.fileId = jsonValue.fileId;
        for (let jsonValueUpdate of jsonValue.valueUpdates)
        {
            let valueUpdate: DataFileValueUpdate;

            valueUpdate = new DataFileValueUpdate();
            valueUpdate.fromJson(jsonValueUpdate);
            this.addValueUpdate(valueUpdate);
        }

        for (let jsonRecordCreation of jsonValue.recordCreations)
        {
            let recordCreation: DataFileRecordCreation;

            recordCreation = new DataFileRecordCreation();
            recordCreation.fromJson(jsonRecordCreation);
            this.addRecordCreation(recordCreation);
        }

        for (let jsonNewRecord of jsonValue.newRecords)
        {
            let newRecordUpdate: DataFileNewRecord;

            newRecordUpdate = new DataFileNewRecord();
            newRecordUpdate.fromJson(jsonNewRecord);
            this.addNewRecord(newRecordUpdate);
        }

        for (let jsonRecordDeletion of jsonValue.recordDeletions)
        {
            let recordDeletion: DataFileRecordDeletion;

            recordDeletion = new DataFileRecordDeletion();
            recordDeletion.fromJson(jsonRecordDeletion);
            this.addRecordDeletion(recordDeletion);
        }

        for (let jsonNewAttachment of jsonValue.newAttachments)
        {
            let newAttachment: DataFileNewAttachment;

            newAttachment = new DataFileNewAttachment();
            newAttachment.fromJson(jsonNewAttachment);
            this.addNewAttachment(newAttachment);
        }

        for (let jsonPropertyAccessUpdate of jsonValue.propertyAccessUpdates)
        {
            let newAccessUpdate: PropertyAccessUpdate;

            newAccessUpdate = new PropertyAccessUpdate();
            newAccessUpdate.fromJson(jsonPropertyAccessUpdate);
            this.addPropertyAccessUpdate(newAccessUpdate);
        }
    }

    addValueUpdate(update: DataFileValueUpdate)
    {
        this.valueUpdates.push(update);
    }

    addRecordCreation(creation: DataFileRecordCreation)
    {
        this.recordCreations.push(creation);
    }

    addNewRecord(newRecord: DataFileNewRecord)
    {
        this.newRecords.push(newRecord);
    }

    addRecordDeletion(deletion: DataFileRecordDeletion)
    {
        this.recordDeletions.push(deletion);
    }

    addNewAttachment(newAttachment: DataFileNewAttachment)
    {
        this.newAttachments.push(newAttachment);
    }

    addPropertyAccessUpdate(newUpdate: PropertyAccessUpdate)
    {
        this.propertyAccessUpdates.push(newUpdate);
    }

    public getNewRecordByTemporaryId(temporaryId: string): DataFileNewRecord
    {
        for (let newRecord of this.newRecords)
        {
            if (newRecord.temporaryId == temporaryId) return newRecord;
        }

        return null;
    }
}

export class DataFileValueUpdate
{
    recordId: string;
    propertyId: string;
    fieldId: string;
    newValue: ValueContent;

    constructor(recordId: string = null, propertyId: string = null, fieldId: string = null, newValue: ValueContent = null)
    {
        this.recordId = recordId;
        this.propertyId = propertyId;
        this.fieldId = fieldId;
        this.setNewValue(newValue);
    }

    fromJson(jsonValueUpdate: any)
    {
        this.fieldId = jsonValueUpdate.fieldId;
        this.recordId = jsonValueUpdate.recordId;
        this.propertyId = jsonValueUpdate.propertyId;
        this.setNewValue(ValueContent.createFromJson(jsonValueUpdate.newValue));
    }

    setNewValue(value : ValueContent)
    {
        this.newValue = value;
    }
}

export class DataFileRecordCreation
{
    recordId: string;
    propertyId: string;
    fieldId: string;
    drInstanceId: string;
    temporaryId: string;

    constructor(recordId: string = null, propertyId: string = null, fieldId: string = null, drInstanceId: string = null, temporaryId: string = null)
    {
        this.recordId = recordId;
        this.propertyId = propertyId;
        this.fieldId = fieldId;
        this.drInstanceId = drInstanceId;
        this.temporaryId = temporaryId;
    }

    fromJson(jsonRecordCreation: any)
    {
        this.recordId = jsonRecordCreation.recordId;
        this.propertyId = jsonRecordCreation.propertyId;
        this.fieldId = jsonRecordCreation.fieldId;
        this.drInstanceId = jsonRecordCreation.drInstanceId;
        this.temporaryId = jsonRecordCreation.temporaryId;
    }
}

export class DataFileNewRecord
{
    recordId: string;
    propertyId: string;
    fieldId: string;
    temporaryId: string;
    newRecord: DataRecord;

    constructor()
    {
    }

    fromJson(jsonNewRecord: any)
    {
        let newRecord: DataRecord;

        this.fieldId = jsonNewRecord.fieldId;
        this.recordId = jsonNewRecord.recordId;
        this.propertyId = jsonNewRecord.propertyId;
        this.temporaryId = jsonNewRecord.temporaryId;

        newRecord = new DataRecord();
        newRecord.fromJson(jsonNewRecord.newRecord);
        this.setNewRecord(newRecord);
    }

    setNewRecord(newRecord: DataRecord)
    {
        this.newRecord = newRecord;
    }
}

export class DataFileRecordDeletion
{
    recordId: string;
    propertyId: string;
    subRecordId: string;

    constructor()
    {
    }

    fromJson(jsonRecordDeletion: any)
    {
        this.recordId = jsonRecordDeletion.recordId;
        this.propertyId = jsonRecordDeletion.propertyId;
        this.subRecordId = jsonRecordDeletion.subRecordId;
    }
}

export class DataFileNewAttachment
{
    recordId: string;
    propertyId: string;
    fieldId: string;
    filename: string;
    fileData: string;

    constructor(recordId: string = null, propertyId: string = null, fieldId: string = null, filename: string = null, fileData: string = null)
    {
        this.recordId = recordId;
        this.propertyId = propertyId;
        this.fieldId = fieldId;
        this.filename = filename;
        this.fileData = fileData;
    }

    fromJson(jsonNewAttachment: any)
    {
        let newRecord: DataRecord;

        this.fieldId = jsonNewAttachment.fieldId;
        this.recordId = jsonNewAttachment.recordId;
        this.propertyId = jsonNewAttachment.propertyId;
        this.filename = jsonNewAttachment.filename;
        this.fileData = jsonNewAttachment.fileData;
    }
}

export class PropertyAccessUpdate
{
    recordId: string;
    propertyId: string;
    editable: boolean;
    visible: boolean;
    required: boolean;
    fftEditable : boolean;
    insertEnabled : boolean;
    updateEnabled : boolean;
    deleteEnabled : boolean;

    constructor()
    {
    }

    fromJson(jsonUpdate: any)
    {
        this.recordId = jsonUpdate.recordId;
        this.propertyId = jsonUpdate.propertyId;
        this.editable = jsonUpdate.editable;
        this.visible = jsonUpdate.visible;
        this.required = jsonUpdate.required;
        this.fftEditable = jsonUpdate.fftEditable;
        this.insertEnabled = jsonUpdate.insertEnabled;
        this.updateEnabled = jsonUpdate.updateEnabled;
        this.deleteEnabled = jsonUpdate.deleteEnabled;
    }

    public apply(propertyAccess: PropertyAccessLayer)
    {
        propertyAccess.visible = this.visible;
        propertyAccess.required = this.required;
        propertyAccess.editable = this.editable;
        propertyAccess.fftEditable = this.fftEditable;
        propertyAccess.insertEnabled = this.insertEnabled;
        propertyAccess.updateEnabled = this.updateEnabled;
        propertyAccess.deleteEnabled = this.deleteEnabled;
    }
}