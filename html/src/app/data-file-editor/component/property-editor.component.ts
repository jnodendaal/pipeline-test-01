import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DataRecord, RecordProperty } from '../../core/model/data-record.model';
import { PropertyRequirement } from '../../core/model/data-requirement.model';
import { PropertyState } from '../model/data-file-state.model';

@Component
({
    selector: 'property-editor',
    templateUrl: 'property-editor.component.html',
    styleUrls: ['property-editor.component.css']
})
export class PropertyEditorComponent  
{   
    @Input()
    state: PropertyState;
    
    constructor()
    {
    }

    @Output() recordNavigation: EventEmitter<string> = new EventEmitter<string>();
    navigateToRecord(recordId: string)
    {
        this.recordNavigation.emit(recordId);
    }
}
