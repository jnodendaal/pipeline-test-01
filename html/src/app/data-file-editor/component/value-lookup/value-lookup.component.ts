import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DataRecord, RecordValue } from '../../../core/model/data-record.model';
import { ValueState } from '../../model/data-file-state.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';

@Component
({
    selector: 'record-value-lookup',
    templateUrl: 'value-lookup.component.html',
    styleUrls: ['value-lookup.component.css']
})
export class ValueLookupComponent  
{   
    private editorService: DataFileEditorService;
    private state: ValueState;
    private targetLocation: ValueLocation;
    private searchString: string;
    private searchPattern: string;
    private loading: boolean;
    private values: RecordValue[];

    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
    }

    @Input()
    set valueState(valueState: ValueState)
    {
        console.log(valueState);
        this.state = valueState;
        this.targetLocation = new ValueLocation();
        this.targetLocation.fileId = this.state.record.getFileId();
        this.targetLocation.recordId = this.state.record.recordId;
        this.targetLocation.propertyId = this.state.propertyState.requirement.propertyId;
        this.targetLocation.fieldId = this.state.requirement.getFieldId();
        this.targetLocation.dataTypeId = this.state.requirement.type;
        this.reloadValues();
    }

    reloadValues()
    {
        // Create a seach pattern from the terms specified.
        if (this.searchString)
        {
            let pattern: string;

            // Split the search terms on whitespace and add each term to the filter.
            for (let searchTerm of this.searchString.split(" "))
            {
                pattern += "|";
                pattern += searchTerm;
            }

            // Set the search pattern used for text highlighting.
            this.searchPattern = pattern;
        }
        else
        {
            // Reset the search pattern.
            this.searchPattern = "";
        }

        // Load the values using the search terms.
        this.loading = true;
        this.editorService.retrieveValueOptions(this.targetLocation.fileId, this.targetLocation.recordId, this.targetLocation.propertyId, this.targetLocation.fieldId, this.targetLocation.dataTypeId, this.searchString, 0, 7)
            .subscribe
            (
                data => 
                {
                    this.loading = false;
                    console.log(data);
                    setTimeout(this.values = data);
                },
                error => 
                {
                    console.log("Error while retrieving value options: " + error);
                    this.loading = false;
                }
            );
    }

    @Output() valueSelected: EventEmitter<RecordValue> = new EventEmitter<RecordValue>();
    onValueSelection(selectedValue: RecordValue)
    {
        console.log(selectedValue);
        this.valueSelected.emit(selectedValue);
    }
}

export class ValueLocation
{
    fileId: string;
    recordId: string;
    propertyId: string;
    fieldId: string;
    dataTypeId: string;
}