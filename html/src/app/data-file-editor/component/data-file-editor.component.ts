import { Component, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { DataFileEditorService } from '../service/data-file-editor.service';
import { DocumentViewComponent } from './document-view.component';
import { NotificationViewComponent } from './notification-view.component';
import { RecordAccessLayer } from '../../core/model/data-record-access.model';
import { DataRecord } from '../../core/model/data-record.model';
import { DataFileUpdate } from '../model/data-file-editor.model';
import { StateController, FileState, PageStateConstructor } from '../model/data-file-state.model';
import { DataFileSaveResult } from '../model/data-file-editor.model';
import { Context } from '../../core/model/security.model';
import { Observable } from 'rxjs/Observable';
import { Subscription }   from 'rxjs/Subscription';

@Component
({
    selector: 'data-file-editor',
    templateUrl: 'data-file-editor.component.html',
    styleUrls: ['data-file-editor.component.css']
})
export class DataFileEditorComponent  
{   
    private editorService: DataFileEditorService;
    private remoteUpdateSubscription: Subscription;
    private localUpdateSubscription: Subscription;
    private updateCycleEndSubscription: Subscription;
    private state: StateController;
    private dataFile: DataRecord;
    private loadingText: string;
    private loading: boolean;
    private ctx: Context;

    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.dataFile = null;
        this.loading = false;
        this.localUpdateSubscription = this.editorService.localUpdateAnnounced$.subscribe(
            update => 
            {
                this.processLocalUpdate(update);
            });
        this.remoteUpdateSubscription = this.editorService.remoteUpdateAnnounced$.subscribe(
            update => 
            {
                this.processRemoteUpdate(update);
            });
        this.updateCycleEndSubscription = this.editorService.updateCycleEndAnnounced$.subscribe(
            update => 
            {
                this.processUpdateCycleEnd(update);
            });
    }

    ngOnDestroy() 
    {
        // Prevent observer memory leak when component is destroyed.
        this.remoteUpdateSubscription.unsubscribe();
        this.localUpdateSubscription.unsubscribe();
        this.updateCycleEndSubscription.unsubscribe();
    }

    @Input()
    set context(context: Context)
    {
        this.ctx = context;
        this.editorService.setContext(context);
    }

    openFile(fileId: string)
    {
        console.log("Loading File: " + fileId);
        this.loadingText = "Loading Record...";
        this.loading = true;
        this.editorService.openFile(fileId)
            .subscribe
            (
                data => 
                {
                    this.loading = false;
                    this.setFile(data);
                },
                error => 
                {
                    console.log("Error while opening data file: " + error);
                    this.loading = false;
                }
            );
    }

    saveFile(): Observable<DataFileSaveResult>
    {
        if (this.dataFile)
        {
            let resultObservable: Observable<DataFileSaveResult>;
            let fileId: string;

            fileId = this.dataFile.recordId;
            console.log("Saving File: " + fileId);
            this.loadingText = "Saving Record...";
            this.loading = true;
            return this.editorService.saveFile(fileId)
                .map((saveResult: DataFileSaveResult) => 
                {
                    // Process the save result and stop the loading indicator.
                    this.state.setValidationReport(saveResult.getValidationReport());
                    if (saveResult.isSuccess()) this.state.changesSaved();
                    this.loading = false;

                    // Return the save result.
                    return saveResult;
                });
        }
    }

    setFile(dataFile: DataRecord)
    {
        this.dataFile = dataFile;
        if (this.dataFile)
        {
            this.state = this.createState(dataFile);
        }
        else
        {
            this.state = null;
        }
    }

    closeFile(saveChanges: boolean)
    {
        if (this.dataFile)
        {
            let fileId: string;

            fileId = this.dataFile.recordId;
            console.log("Closing File: " + fileId);
            this.loadingText = "Closing Record...";
            this.loading = true;
            this.editorService.closeFile(fileId, saveChanges)
                .subscribe
                (
                    data => 
                    {
                        let saveResult: DataFileSaveResult;

                        // Get the save result and stop the loading indicator.
                        saveResult = data;
                        this.loading = false;
                    },
                    error => 
                    {
                        console.log("Error while saving data file: " + error);
                        this.loading = false;
                    }
                );
        }
    }

    private processLocalUpdate(update: DataFileUpdate)
    {
        this.state.processLocalUpdate(update);
    }

    
    private processRemoteUpdate(update: DataFileUpdate)
    {
        this.state.processRemoteUpdate(update);
    }
    
    @Output() fileUpdate: EventEmitter<void> = new EventEmitter<void>();
    private processUpdateCycleEnd(update: DataFileUpdate)
    {
        this.fileUpdate.emit();
    }

    private createState(dataFile: DataRecord): FileState
    {
        let stateConstructor: PageStateConstructor;
        let fileState: FileState;

        // Create a paged state.
        stateConstructor = new PageStateConstructor();
        fileState = stateConstructor.createPagedFileState(dataFile);
        return fileState;
    }
}

export interface DataFileViewComponent
{
}
