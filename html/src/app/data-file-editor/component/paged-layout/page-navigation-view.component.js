"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_editor_service_1 = require("../data-file-editor.service");
var data_file_layout_model_1 = require("../data-file-layout.model");
var PageNavigationViewComponent = (function () {
    function PageNavigationViewComponent(recordEditorService) {
        this.outputNavigation = new core_1.EventEmitter();
        this.editorService = recordEditorService;
        this.subscription = this.editorService.updateCycleEndAnnounced$.subscribe(function (update) {
            console.log("Update Cycle End observed by page navigation view");
        });
    }
    Object.defineProperty(PageNavigationViewComponent.prototype, "inputLayout", {
        set: function (layout) {
            this.layout = layout;
        },
        enumerable: true,
        configurable: true
    });
    PageNavigationViewComponent.prototype.navigateTo = function (pageId) {
        this.outputNavigation.emit(pageId);
    };
    PageNavigationViewComponent.prototype.ngOnDestroy = function () {
        // Prevent observer memory leak when component is destroyed.
        this.subscription.unsubscribe();
    };
    return PageNavigationViewComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", data_file_layout_model_1.PagedFileLayout),
    __metadata("design:paramtypes", [data_file_layout_model_1.PagedFileLayout])
], PageNavigationViewComponent.prototype, "inputLayout", null);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], PageNavigationViewComponent.prototype, "outputNavigation", void 0);
PageNavigationViewComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'page-navigation-view',
        templateUrl: 'page-navigation-view.component.html'
    }),
    __metadata("design:paramtypes", [data_file_editor_service_1.DataFileEditorService])
], PageNavigationViewComponent);
exports.PageNavigationViewComponent = PageNavigationViewComponent;
//# sourceMappingURL=page-navigation-view.component.js.map