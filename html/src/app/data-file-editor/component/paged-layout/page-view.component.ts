import { Component, Input, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { DataRecord, RecordProperty, DocumentReferenceList, DocumentReference } from '../../../core/model/data-record.model';
import { PagedFileState, PageState, RecordState, ValueState } from '../../model/data-file-state.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { RecordValueValidationError, DataFileUpdate, DataFileRecordCreation } from '../../model/data-file-editor.model';
import { PropertyRequirement, DataRequirementInstance } from '../../../core/model/data-requirement.model';

@Component
({
    selector: 'page-view',
    templateUrl: 'page-view.component.html'
})
export class PageViewComponent implements OnDestroy
{   
    private editorService: DataFileEditorService;
    private pageState: PageState;
    private referenceState: ValueState;
    private referencingRecord: DataRecord;
    private referencingProperty: RecordProperty;
    private valueLookupVisible: boolean;
    private navigationModalVisible: boolean;
    private validationErrorModalVisible: boolean;
    private recordStatePath: RecordState[];
    private valueErrors: ValueErrorMessage[];

    @ViewChild('currentNavitation') 
    private currentNavigation: any;

    @ViewChild('navigationModal') 
    private navigationModal: ModalDirective;

    @ViewChild('validationErrorModal') 
    private validationErrorModal: ModalDirective;

    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
        this.recordStatePath = new Array();
        this.valueErrors = new Array();
    }

    ngOnDestroy() 
    {
    }

    @Input()
    set state(state: PageState)
    {
        let fileState: PagedFileState;
        let referencingPropertyId;
        let referencingRecordId;
        let referencing

        this.pageState = state;
        this.referencingProperty = state.referenceProperty;
        if (this.referencingProperty)
        {
            let recordPath: DataRecord[];

            fileState = state.fileState;
            this.referencingRecord = this.referencingProperty.getRecord();
            recordPath = this.referencingRecord.getRecordPath();
            referencingRecordId = this.referencingRecord.recordId;
            referencingPropertyId = this.referencingProperty.propertyId;

            // Get the referencing property state.
            this.referenceState = fileState.getPropertyState(referencingRecordId, referencingPropertyId).valueState;
            
            // Get the states of each record in the path.
            this.recordStatePath.length = 0;
            for (let record of recordPath)
            {
                this.recordStatePath.push(fileState.getRecordState(record.recordId));
            }
        }
        else
        {
            this.referencingRecord = null;
            this.recordStatePath = new Array();
        }
    }

    @Output() pageNavigation: EventEmitter<string> = new EventEmitter<string>();
    navigateToPage(pageId: string)
    {
        this.pageNavigation.emit(pageId);
    }

    @Output() recordNavigation: EventEmitter<string> = new EventEmitter<string>();
    navigateToRecord(recordId: string)
    {
        this.recordNavigation.emit(recordId);
    }

    public scrollToRecord(recordId: string)
    {
        setTimeout(() => 
        {
            let recordElement;
            let topOffset;

            // Find the y-offset to which we need to scroll by subtracting the sticky header height from the y-offset of the target element.
            topOffset = document.getElementById('currentNavigation').offsetHeight * -1;
            recordElement = document.getElementById('R-' + recordId);
            topOffset += recordElement.getBoundingClientRect().top + window.pageYOffset;

            // We use window.scroll instead of element.scrollIntoView in order to account for the stucky navigation header.
            window.scroll({top: topOffset, left: 0, behavior:"instant"});
        });
    }

    public onLookupSelection(value: DocumentReferenceList)
    {
        this.hideValueLookup();
        this.addNewRecord(value.references[0]);
    }

    public onPageSelection(pageId: string)
    {
        this.hideNavigationModal();
        this.navigateToPage(pageId);
    }

    public onRecordSelection(recordId: string)
    {
        this.hideNavigationModal();
        this.navigateToRecord(recordId);
    }

    public showValueLookup():void 
    {
        this.valueLookupVisible = true;
    }

    public hideValueLookup():void 
    {
        this.valueLookupVisible = false;
    }

    public showNavigationModal()
    {
        this.navigationModalVisible = true;
    }

    public hideNavigationModal():void 
    {
        this.navigationModal.hide();
    }

    public onNavigationModalHidden():void 
    {
        this.navigationModalVisible = false;
    }

    public showValidationErrorModal()
    {
        // Construct the list of validation errors.
        this.valueErrors.length = 0;
        for (let error of this.pageState.fileState.validationErrors)
        {
            if (error instanceof RecordValueValidationError)
            {
                let validationError: RecordValueValidationError;
                let targetRecordState: RecordState;
                let targetDrInstance: DataRequirementInstance;
                let targetPropertyRequirement: PropertyRequirement;
                let targetProperty: RecordProperty;
                let targetRecord: DataRecord;

                validationError = error as RecordValueValidationError;
                targetRecordState = this.pageState.fileState.getRecordState(validationError.getRecordId());
                targetRecord = targetRecordState.record;
                targetDrInstance = targetRecord.drInstance;
                targetProperty = targetRecord.getProperty(error.getPropertyId());
                targetPropertyRequirement = targetRecord.drInstance.getProperty(error.getPropertyId());

                this.valueErrors.push(new ValueErrorMessage(targetRecord.recordId, targetDrInstance.term, targetPropertyRequirement.term, targetProperty.valueString, error.getMessage()));
            }
        }

        this.validationErrorModalVisible = true;
    }

    public hideValidationErrorModal():void 
    {
        this.validationErrorModal.hide();
    }

    public onValidationErrorModalHidden():void 
    {
        this.validationErrorModalVisible = false;
    }

    private onErrorSelection(error: ErrorMessage)
    {
        this.hideValidationErrorModal();
        this.navigateToRecord(error.recordId);
    }

    private addNewRecord(newReference: DocumentReference)
    {
        let update: DataFileUpdate;
        let parentRecord: DataRecord;
        let temporaryId: string;

        // Generate a temporary id for the new record.
        temporaryId = Date.now().toString();

        // Announce the local update.
        parentRecord = this.referencingProperty.getRecord();
        update = new DataFileUpdate(parentRecord.getFile().getFileId());
        update.addRecordCreation(new DataFileRecordCreation(parentRecord.recordId, this.referencingProperty.propertyId, null, newReference.conceptId, temporaryId));
        this.editorService.announceLocalUpdate(parentRecord, update);
    }
}

class ErrorMessage
{
    recordId: string;

    constructor(recordId: string)
    {
        this.recordId = recordId;
    }
}

class ValueErrorMessage extends ErrorMessage
{
    instanceTerm: string;
    propertyTerm: string;
    propertyValueString: string;
    message: string;

    constructor(recordId: string, instanceTerm: string, propertyTerm: string, propertyValueString: string, message: string)
    {
        super(recordId);
        this.instanceTerm = instanceTerm;
        this.propertyTerm = propertyTerm;
        this.propertyValueString = propertyValueString;
        this.message = message;
    }
}