import { Component, ViewChild, Input } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { DataFileViewComponent } from '../data-file-editor.component';
import { DocumentViewComponent } from '../document-view.component';
import { NotificationViewComponent } from '../notification-view.component';
import { PageViewComponent } from './page-view.component';
import { DataRecord } from '../../../core/model/data-record.model';
import { PagedFileState, PageState } from '../../model/data-file-state.model';

@Component
({
    selector: 'paged-view',
    templateUrl: 'paged-view.component.html',
    styleUrls: ['paged-view.component.css']
})
export class PagedViewComponent implements DataFileViewComponent
{   
    private editorService: DataFileEditorService;
    private currentPage: PageState;
    private loading: boolean;
    private fileState: PagedFileState;

    @ViewChild(PageViewComponent)
    private pageViewComponent: PageViewComponent;

    @ViewChild(NotificationViewComponent)
    private notificationViewComponent: NotificationViewComponent;

    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.loading = false;
    }

    @Input()
    set state(state: PagedFileState)
    {
        this.fileState = state;
        this.currentPage = this.fileState.getFirstPageState();
    }

    public navigateToPage(pageId: string)
    {
        this.currentPage = this.fileState.getPageState(pageId);
    }

    public navigateToRecord(recordId: string)
    {
        this.currentPage = this.fileState.getPageStateContainingRecord(recordId);
        this.pageViewComponent.scrollToRecord(recordId);
    }
}
