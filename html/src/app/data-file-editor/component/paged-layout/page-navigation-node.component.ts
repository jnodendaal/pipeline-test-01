import { Component, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { DataRecord } from '../../../core/model/data-record.model';
import { FileState, PageState } from '../../model/data-file-state.model';
import { Subscription }   from 'rxjs/Subscription';

@Component
({
    selector: 'page-navigation-node',
    templateUrl: 'page-navigation-node.component.html',
    styleUrls: ['page-navigation-node.component.css']
})
export class PageNavigationNodeComponent
{   
    private state: PageState;

    constructor()
    {
    }

    @Input()
    set pageState(state: PageState)
    {
        this.state = state;
    }

    @Output() pageSelected: EventEmitter<string> = new EventEmitter<string>();
    navigateToPage(pageId: string)
    {
        this.pageSelected.emit(pageId);
    }

    @Output() recordSelected: EventEmitter<string> = new EventEmitter<string>();
    navigateToRecord(recordId: string)
    {
        this.recordSelected.emit(recordId);
    }
}
