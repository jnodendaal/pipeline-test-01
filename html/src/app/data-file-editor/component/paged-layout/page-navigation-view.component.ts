import { Component, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { DataRecord } from '../../../core/model/data-record.model';
import { FileState, PagedFileState } from '../../model/data-file-state.model';
import { Subscription }   from 'rxjs/Subscription';

@Component
({
    selector: 'page-navigation-view',
    templateUrl: 'page-navigation-view.component.html'
})
export class PageNavigationViewComponent implements OnDestroy
{   
    private editorService: DataFileEditorService;
    private subscription: Subscription;
    private state: PagedFileState;

    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
    }

    @Input()
    set fileState(state: PagedFileState)
    {
        this.state = state;
    }

    @Output() pageSelected: EventEmitter<string> = new EventEmitter<string>();
    onPageSelected(pageId: string)
    {
        this.pageSelected.emit(pageId);
    }

    @Output() recordSelected: EventEmitter<string> = new EventEmitter<string>();
    onRecordSelected(recordId: string)
    {
        this.recordSelected.emit(recordId);
    }

    ngOnDestroy() 
    {
        // Prevent observer memory leak when component is destroyed.
        this.subscription.unsubscribe();
    }
}
