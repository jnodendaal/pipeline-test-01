"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_editor_service_1 = require("./data-file-editor.service");
var data_file_layout_model_1 = require("./data-file-layout.model");
// Temporary imports for testing purposes.
var data_file_layout_model_2 = require("./data-file-layout.model");
var DataFileEditorComponent = (function () {
    function DataFileEditorComponent(recordEditorService) {
        this.editorService = recordEditorService;
        this.dataFile = null;
        this.loading = false;
    }
    DataFileEditorComponent.prototype.openFile = function (fileId) {
        var _this = this;
        console.log("Loading File: " + fileId);
        this.loading = true;
        this.editorService.openFile(fileId)
            .subscribe(function (data) {
            _this.loading = false;
            _this.setFile(data);
        }, function (error) {
            console.log("Error while opening data file: " + error);
            _this.loading = false;
        });
    };
    DataFileEditorComponent.prototype.saveFile = function () {
        var _this = this;
        if (this.dataFile) {
            var fileId = void 0;
            fileId = this.dataFile.recordId;
            console.log("Saving File: " + fileId);
            this.loading = true;
            this.editorService.saveFile(fileId)
                .subscribe(function (data) {
                _this.loading = false;
            }, function (error) {
                console.log("Error while saving data file: " + error);
                _this.loading = false;
            });
        }
    };
    DataFileEditorComponent.prototype.setFile = function (dataFile) {
        this.dataFile = dataFile;
        this.layout = this.createLayout(dataFile);
    };
    DataFileEditorComponent.prototype.createLayout = function (dataFile) {
        var fileLayoutDefinition;
        var layoutConstructor;
        var fileLayout;
        //fileLayoutDefinition = this.createPresetLayoutDefinition(dataFile);
        //fileLayout = fileLayoutDefinition.createLayout(dataFile);
        //fileLayout = fileLayoutDefinition.createLayout(dataFile);
        layoutConstructor = new data_file_layout_model_1.PagedLayoutConstructor();
        fileLayout = layoutConstructor.createDefaultPagedLayout(dataFile);
        console.log(fileLayout);
        return fileLayout;
    };
    // private createDefaultLayout(dataFile: DataRecord): PagedFileLayout
    // {
    //     pagedLayout: PagedFileLayout;
    // }
    DataFileEditorComponent.prototype.createPresetLayoutDefinition = function (dataFile) {
        // Create a test layout definition for the new data file.
        var fileLayout;
        var pageLayout;
        var recordLayout;
        var sectionLayout;
        var subSectionLayout;
        var propertyLayout;
        var subPropertyLayout;
        var docReferenceLayout;
        fileLayout = new data_file_layout_model_2.PagedFileLayoutDefinition();
        pageLayout = new data_file_layout_model_2.PageLayoutDefinition("VENDOR_DETAILS", "Vendor Details");
        fileLayout.addPageLayoutDefinition(pageLayout);
        recordLayout = new data_file_layout_model_2.RecordLayoutDefinition();
        pageLayout.addRecordLayoutDefinition(recordLayout);
        recordLayout.drInstanceId = "9970A8062DD84B309CF69703635F147C";
        sectionLayout = new data_file_layout_model_2.SectionLayoutDefinition("4CBBE67226454DB5B8A592F512CCE720"); // Vendor Master Information
        sectionLayout.headerVisible = false;
        recordLayout.addSectionLayoutDefinition(sectionLayout);
        propertyLayout = this.createVendorTypeLayoutDefinition();
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        // propertyLayout = new PropertyLayoutDefinition("*"); // Wildcard
        // sectionLayout.addProperty(propertyLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("23D5EDAD13D04F098C5F3A50266745E3"); // Extension Details
        propertyLayout.visible = false;
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("B4A1C4FFE80843E08EDB7D36BFA961E1"); // Accreditation
        propertyLayout.visible = false;
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("DA3B6D2A75204CAEAD55074109A12B5B"); // Supply Details
        propertyLayout.visible = false;
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("8BDEA9A197E14CB89BDA36570F4EAAFA"); // Organization Root
        propertyLayout.visible = false;
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        pageLayout = new data_file_layout_model_2.PageLayoutDefinition("BANKING_DETAILS", "Banking Details");
        fileLayout.addPageLayoutDefinition(pageLayout);
        recordLayout = new data_file_layout_model_2.RecordLayoutDefinition();
        pageLayout.addRecordLayoutDefinition(recordLayout);
        recordLayout.drInstanceId = "32A38BB5045B4A9699D283128E2725B4";
        sectionLayout = new data_file_layout_model_2.SectionLayoutDefinition("*");
        recordLayout.addSectionLayoutDefinition(sectionLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("*");
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("9856678F2C48405A92F0D56EEAF8CB30"); // Proof of Banking Detail.
        propertyLayout.visible = false;
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("FFA5B18ECF37424AAB36076C4C8C68EC"); // Supplier information.
        propertyLayout.visible = false;
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        // pageLayout = new PageLayoutDefinition("EXTENSION_DETAILS", "Extension Details");
        // fileLayout.addPage(pageLayout);
        // recordLayout = new RecordLayoutDefinition();
        // pageLayout.addRecord(recordLayout);
        // recordLayout.drInstanceId = "3FDD61107C3F49F48D19B6197C8F9C54";
        // sectionLayout = new SectionLayoutDefinition("*");
        // recordLayout.addSection(sectionLayout);
        // propertyLayout = new PropertyLayoutDefinition("*");
        // sectionLayout.addProperty(propertyLayout);
        return fileLayout;
    };
    DataFileEditorComponent.prototype.createVendorTypeLayoutDefinition = function () {
        var recordLayout;
        var sectionLayout;
        var propertyLayout;
        var docReferenceLayout;
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("F5D772E076BD400BAF87B1F841E9A1AC"); // Vendor Type
        propertyLayout.visible = true;
        docReferenceLayout = new data_file_layout_model_2.DocumentReferenceLayoutDefinition();
        docReferenceLayout.pullUp = true;
        recordLayout = new data_file_layout_model_2.RecordLayoutDefinition();
        docReferenceLayout.addRecordLayoutDefinition(recordLayout);
        recordLayout.drInstanceId = "54FFDDDCDD0C476AA219C46E68AEE0A1"; // Business Trust Local
        sectionLayout = new data_file_layout_model_2.SectionLayoutDefinition("*");
        recordLayout.addSectionLayoutDefinition(sectionLayout);
        propertyLayout = new data_file_layout_model_2.PropertyLayoutDefinition("*");
        sectionLayout.addPropertyLayoutDefinition(propertyLayout);
        propertyLayout.valueLayoutDefinition = docReferenceLayout;
        return propertyLayout;
    };
    return DataFileEditorComponent;
}());
__decorate([
    core_1.ViewChild('viewComponent'),
    __metadata("design:type", Object)
], DataFileEditorComponent.prototype, "viewComponent", void 0);
DataFileEditorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'data-file-editor',
        templateUrl: 'data-file-editor.component.html',
        styleUrls: ['data-file-editor.component.css']
    }),
    __metadata("design:paramtypes", [data_file_editor_service_1.DataFileEditorService])
], DataFileEditorComponent);
exports.DataFileEditorComponent = DataFileEditorComponent;
//# sourceMappingURL=data-file-editor.component.js.map