"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_editor_service_1 = require("./data-file-editor.service");
var data_record_model_1 = require("../../models/data-record.model");
var data_file_layout_model_1 = require("./data-file-layout.model");
var NotificationViewComponent = (function () {
    function NotificationViewComponent(recordEditorService) {
        var _this = this;
        this.editorService = recordEditorService;
        this.completeness = 0;
        this.dataQuality = new data_record_model_1.RecordDataQuality(null);
        this.subscription = this.editorService.updateCycleEndAnnounced$.subscribe(function (update) {
            _this.refreshDataQuality();
        });
    }
    NotificationViewComponent.prototype.ngOnDestroy = function () {
        // Prevent observer memory leak when component is destroyed.
        this.subscription.unsubscribe();
    };
    Object.defineProperty(NotificationViewComponent.prototype, "inputLayout", {
        set: function (layout) {
            this.layout = layout;
            this.refreshDataQuality();
        },
        enumerable: true,
        configurable: true
    });
    NotificationViewComponent.prototype.refreshDataQuality = function () {
        if (this.layout.file) {
            this.dataQuality = this.layout.file.getDataQuality(true);
            this.completeness = Math.round(this.dataQuality.propertyDataCount / this.dataQuality.propertyCount * 100);
        }
    };
    return NotificationViewComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", data_file_layout_model_1.FileLayout),
    __metadata("design:paramtypes", [data_file_layout_model_1.FileLayout])
], NotificationViewComponent.prototype, "inputLayout", null);
NotificationViewComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'notification-view',
        templateUrl: 'notification-view.component.html',
        styleUrls: ['notification-view.component.css']
    }),
    __metadata("design:paramtypes", [data_file_editor_service_1.DataFileEditorService])
], NotificationViewComponent);
exports.NotificationViewComponent = NotificationViewComponent;
//# sourceMappingURL=notification-view.component.js.map