"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_layout_model_1 = require("./data-file-layout.model");
var SectionEditorComponent = (function () {
    function SectionEditorComponent() {
    }
    Object.defineProperty(SectionEditorComponent.prototype, "inputLayout", {
        set: function (layout) {
            this.layout = layout;
        },
        enumerable: true,
        configurable: true
    });
    return SectionEditorComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", data_file_layout_model_1.SectionLayout),
    __metadata("design:paramtypes", [data_file_layout_model_1.SectionLayout])
], SectionEditorComponent.prototype, "inputLayout", null);
SectionEditorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'section-editor',
        templateUrl: 'section-editor.component.html',
        styleUrls: ['section-editor.component.css']
    }),
    __metadata("design:paramtypes", [])
], SectionEditorComponent);
exports.SectionEditorComponent = SectionEditorComponent;
//# sourceMappingURL=section-editor.component.js.map