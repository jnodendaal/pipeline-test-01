import { Component, ViewChild, Input } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { DataFileViewComponent } from '../data-file-editor.component';
import { DocumentViewComponent } from '../document-view.component';
import { NotificationViewComponent } from '../notification-view.component';
import { DataRecord } from '../../../core/model/data-record.model';
import { FileState } from '../../model/data-file-state.model';

@Component
({
    selector: 'default-layout-view',
    templateUrl: 'default-layout-view.component.html',
    styleUrls: ['default-layout-view.component.css']
})
export class DefaultLayoutViewComponent implements DataFileViewComponent
{   
    private editorService: DataFileEditorService;
    private loading: boolean;
    
    @Input()
    private state: FileState;

    @ViewChild(DocumentViewComponent)
    private documentViewComponent: DocumentViewComponent;

    @ViewChild(NotificationViewComponent)
    private notificationViewComponent: NotificationViewComponent;

    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.loading = false;
    }
}
