"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_editor_service_1 = require("../data-file-editor.service");
var document_view_component_1 = require("../document-view.component");
var notification_view_component_1 = require("../notification-view.component");
var data_file_layout_model_1 = require("../data-file-layout.model");
var DefaultLayoutViewComponent = (function () {
    function DefaultLayoutViewComponent(recordEditorService) {
        this.editorService = recordEditorService;
        this.loading = false;
    }
    Object.defineProperty(DefaultLayoutViewComponent.prototype, "inputLayout", {
        set: function (layout) {
            console.log(layout);
            this.layout = layout;
        },
        enumerable: true,
        configurable: true
    });
    return DefaultLayoutViewComponent;
}());
__decorate([
    core_1.ViewChild(document_view_component_1.DocumentViewComponent),
    __metadata("design:type", document_view_component_1.DocumentViewComponent)
], DefaultLayoutViewComponent.prototype, "documentViewComponent", void 0);
__decorate([
    core_1.ViewChild(notification_view_component_1.NotificationViewComponent),
    __metadata("design:type", notification_view_component_1.NotificationViewComponent)
], DefaultLayoutViewComponent.prototype, "notificationViewComponent", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", data_file_layout_model_1.FileLayout),
    __metadata("design:paramtypes", [data_file_layout_model_1.FileLayout])
], DefaultLayoutViewComponent.prototype, "inputLayout", null);
DefaultLayoutViewComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'default-layout-view',
        templateUrl: 'default-layout-view.component.html',
        styleUrls: ['default-layout-view.component.css']
    }),
    __metadata("design:paramtypes", [data_file_editor_service_1.DataFileEditorService])
], DefaultLayoutViewComponent);
exports.DefaultLayoutViewComponent = DefaultLayoutViewComponent;
//# sourceMappingURL=default-layout-view.component.js.map