import { Component, OnDestroy, Input } from '@angular/core';
import { DataFileEditorService } from '../service/data-file-editor.service';
import { DataRecord } from '../../core/model/data-record.model';
import { FileDataQualityReport, DataQualityMetrics, DataQualityMetricsSummary } from '../../core/model/data-record-quality.model';
import { FileState } from '../model/data-file-state.model';
import { Subscription }   from 'rxjs/Subscription';

@Component
({
    selector: 'notification-view',
    templateUrl: 'notification-view.component.html',
    styleUrls: ['notification-view.component.css']
})
export class NotificationViewComponent implements OnDestroy
{
    private editorService: DataFileEditorService;
    private subscription: Subscription;
    private state: FileState;
    private dqMetrics: DataQualityMetricsSummary;
    public completeness: number;
    public validity: number;

    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.completeness = 0;
        this.validity = 0;
        this.dqMetrics = new DataQualityMetricsSummary();
        this.subscription = this.editorService.updateCycleEndAnnounced$.subscribe(
            update => 
            {
                this.refreshDataQuality();
            });
    }

    ngOnDestroy() 
    {
        // Prevent observer memory leak when component is destroyed.
        this.subscription.unsubscribe();
    }

    @Input()
    set fileState(state: FileState)
    {
        this.state = state;
        this.refreshDataQuality();
    }

    public refreshDataQuality()
    {
        if (this.state.file)
        {
            this.dqMetrics = this.state.file.getFileDataQualityReport().metrics.getSummary();
            this.completeness = this.dqMetrics.characteristicCompleteness;
            this.validity = this.dqMetrics.characteristicValidity;
        }
    }
}
