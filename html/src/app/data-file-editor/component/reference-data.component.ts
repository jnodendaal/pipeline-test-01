import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DataRecord, RecordSection } from '../../core/model/data-record.model';
import { SectionRequirement } from '../../core/model/data-requirement.model';
import { SectionState } from '../model/data-file-state.model';

@Component
({
    selector: 'ontology-editor',
    templateUrl: 'code-editor.component.html',
    styleUrls: ['code-editor.component.css']
})
export class CodeEditorComponent  
{   
    @Input()
    state: SectionState;

    constructor()
    {
    }

    @Output() recordNavigation: EventEmitter<string> = new EventEmitter<string>();
    navigateToRecord(recordId: string)
    {
        this.recordNavigation.emit(recordId);
    }
}
