import { Component, Input } from '@angular/core';
import { DataRecord } from '../../core/model/data-record.model';
import { FileState } from '../model/data-file-state.model';

@Component
({
    selector: 'document-view',
    templateUrl: 'document-view.component.html',
    styleUrls: ['document-view.component.css']
})
export class DocumentViewComponent  
{   
    private layout: FileState;
    private title: string;

    @Input()
    state: FileState;

    constructor()
    {
        this.title = "Document View";
    }
}
