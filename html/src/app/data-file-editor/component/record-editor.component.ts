import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { DataFileEditorService } from '../service/data-file-editor.service';
import { DataRecord, RecordSection, RecordProperty, DocumentReferenceList, DocumentReferenceListContent, DocumentReference } from '../../core/model/data-record.model';
import { DataFileUpdate, DataFileValueUpdate } from '../model/data-file-editor.model';
import { RecordState, SectionState, PageStateConstructor } from '../model/data-file-state.model';

@Component
({
    selector: 'record-editor',
    templateUrl: 'record-editor.component.html',
    styleUrls: ['record-editor.component.css']
})
export class RecordEditorComponent implements OnDestroy
{   
    private editorService: DataFileEditorService;
    private deletionConfirmationVisible: boolean;

    @Input()
    state: RecordState;
    
    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.deletionConfirmationVisible = false;
    }

    ngOnDestroy() 
    {
    }


    @Output() recordNavigation: EventEmitter<string> = new EventEmitter<string>();
    navigateToRecord(recordId: string)
    {
        this.recordNavigation.emit(recordId);
    }

    private showDeletionConfirmation()
    {
        this.deletionConfirmationVisible = true;
    }

    private hideDeletionConfirmation()
    {
        this.deletionConfirmationVisible = false;
    }

    private deleteRecord()
    {
        let referrerProperty: RecordProperty;
        let newRecordLayout: RecordState;
        let documentReferenceList: DocumentReferenceList;
        let documentReferenceListContent: DocumentReferenceListContent;
        let update: DataFileUpdate;
        let referrerRecord: DataRecord;

        // Remove the reference to this record from the referencing property.
        console.log("Deleting record: " + this.state.record.recordId);
        referrerProperty = this.state.parentState.referenceProperty;
        referrerRecord = referrerProperty.getRecord();
        documentReferenceList = referrerProperty.value as DocumentReferenceList;
        documentReferenceListContent = documentReferenceList.getContent();
        documentReferenceListContent.removeReference(this.state.record.recordId);

        // Announce the local update.
        update = new DataFileUpdate(referrerRecord.getFile().getFileId());
        update.addValueUpdate(new DataFileValueUpdate(referrerRecord.recordId, referrerProperty.propertyId, null, documentReferenceListContent));
        this.editorService.announceLocalUpdate(referrerRecord, update);
    }    
}
