import { Component, Input } from '@angular/core';
import { DataRecord, Composite } from '../../../core/model/data-record.model';
import { PropertyRequirement, CompositeRequirement } from '../../../core/model/data-requirement.model';
import { CompositeState } from '../../model/data-file-state.model';

@Component
({
    selector: 'composite-editor',
    templateUrl: 'composite-editor.component.html',
    styleUrls: ['composite-editor.component.css']
})
export class CompositeEditorComponent  
{   
    private propertyRequirement: PropertyRequirement;

    @Input()
    private state: CompositeState;

    constructor()
    {
    }
}
