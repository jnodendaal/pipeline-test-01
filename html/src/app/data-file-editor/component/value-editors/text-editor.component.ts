import { Component, Input, OnDestroy } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { TextState } from '../../model/data-file-state.model';
import { PropertyRequirement, TextRequirement } from '../../../core/model/data-requirement.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import 
{ 
    DataRecord, 
    RecordProperty, 
    TextValue
} from '../../../core/model/data-record.model';

@Component
({
    selector: 'text-editor',
    templateUrl: 'text-editor.component.html',
    styleUrls: ['text-editor.component.css']
})
export class TextEditorComponent 
{   
    private editorService: DataFileEditorService;
    private oldValue: string; // Stores the old string value to compare to when changes occur.
    private recordProperty: RecordProperty;

    @Input()
    private state: TextState;

    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
    }
    
    public announceValueUpdate()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.state.value.text)
        {
            let fileUpdate: DataFileUpdate;
            let valueUpdate: DataFileValueUpdate;

            // Create the new value update.
            valueUpdate = new DataFileValueUpdate(this.state.record.recordId, this.state.requirement.getPropertyId(), this.state.requirement.getFieldId(), this.state.value.getContent());

            // Announce the local file update.
            fileUpdate = new DataFileUpdate(this.state.record.getFileId());
            fileUpdate.addValueUpdate(valueUpdate);
            this.editorService.announceLocalUpdate(this.state.record, fileUpdate);

            // Set the old value to the current value.
            this.oldValue = this.state.value.text;

            // Mark the property as updated.
            this.state.propertyState.setUpdated(true);
        }
    }
}
