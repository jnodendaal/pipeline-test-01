import { Component, Input } from '@angular/core';
import { AttachmentListState } from '../../model/data-file-state.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { DataFileUpdate, DataFileValueUpdate, DataFileNewAttachment } from '../../model/data-file-editor.model';
import { PropertyRequirement, AttachmentListRequirement } from '../../../core/model/data-requirement.model';
import { DataRecord, RecordProperty, AttachmentList } from '../../../core/model/data-record.model';
import { ApplicationSettings } from '../../../app.config';

@Component
({
    selector: 'attachment-list-editor',
    templateUrl: 'attachment-list-editor.component.html',
    styleUrls: ['attachment-list-editor.component.css']
})
export class AttachmentListEditorComponent  
{   
    private editorService: DataFileEditorService;
    private attachmentUrl: string;

    @Input()
    private state: AttachmentListState;
    
    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
        this.attachmentUrl = ApplicationSettings.API_ATTACHMENT_URL + '/';
    }

    removeAttachment(attachmentId: string)
    {
        let record: DataRecord;
        let update: DataFileUpdate;

        // Remove the attachment from the local list.
        this.state.value.removeAttachment(attachmentId);

        // Announce the local update.
        record = this.state.value.getParentRecord();
        update = new DataFileUpdate(record.getFile().getFileId());
        update.addValueUpdate(new DataFileValueUpdate(record.recordId, this.state.requirement.getPropertyId(), this.state.requirement.getFieldId(), this.state.value.getContent()));
        this.editorService.announceLocalUpdate(record, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }

    onFileSelectionChange(event: any) 
    {
        var reader = new FileReader();
        let fileList: FileList;
        let files: File[];
        let self: any;

        // Store a reference to this object to use in callback.
        self = this;

        // Create a new file reader to be used for conversion of binary file data to base64.
        reader = new FileReader();
        
        // Create an array of files to be uploaded.
        files = new Array();
        fileList = event.target.files;
        for (let index = 0; index < fileList.length; index++)
        {
            let reader: FileReader;
            let file: File;

            // Get the file to read, create a reader for it and set the completion handler.
            file = fileList.item(index);
            reader = new FileReader();
            reader.onload = function(readerEvt: any) 
            {
                var binaryString: string;
                var base64String: string;

                binaryString = readerEvt.target.result;
                base64String = btoa(binaryString);
                self.onFileRead(file, base64String);
            };

            // Read the file.
            reader.readAsBinaryString(file);
        }
    }

    private onFileRead(file: File, fileData: string)
    {
        let record: DataRecord;
        let update: DataFileUpdate;

        // Announce the local update.
        record = this.state.value.getParentRecord();
        update = new DataFileUpdate(record.getFile().getFileId());
        update.addNewAttachment(new DataFileNewAttachment(record.recordId, this.state.requirement.getPropertyId(), this.state.requirement.getFieldId(), file.name, fileData));
        this.editorService.announceLocalUpdate(record, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }
}
