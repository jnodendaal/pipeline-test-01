"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_editor_service_1 = require("../data-file-editor.service");
var data_file_layout_model_1 = require("../data-file-layout.model");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/of");
var ControlledConceptEditorComponent = (function () {
    function ControlledConceptEditorComponent(editorService) {
        var _this = this;
        this.editorService = editorService;
        this.dataSource = Observable_1.Observable.create(function (observer) {
            // Runs on every search.
            observer.next(_this.layout.value.term);
        }).mergeMap(function (token) { return _this.getStatesAsObservable(token); });
    }
    Object.defineProperty(ControlledConceptEditorComponent.prototype, "inputLayout", {
        set: function (layout) {
            this.layout = layout;
        },
        enumerable: true,
        configurable: true
    });
    ControlledConceptEditorComponent.prototype.getStatesAsObservable = function (token) {
        return this.editorService.retrieveValueSuggestions(this.layout.record.getFileId(), this.layout.record.recordId, this.layout.value.parentProperty.propertyId, null, this.layout.value.type, token, 0, 7);
        // let query = new RegExp(token, 'ig');
        // return Observable.of(this.statesComplex.filter((state: any) => 
        // {
        //     return query.test(state.name);
        // }));
    };
    ControlledConceptEditorComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
    };
    ControlledConceptEditorComponent.prototype.changeTypeaheadNoResults = function (e) {
        this.typeaheadNoResults = e;
    };
    ControlledConceptEditorComponent.prototype.typeaheadOnSelect = function (e) {
        console.log('Selected value: ', e.value);
        console.log(e.item);
    };
    ControlledConceptEditorComponent.prototype.announceValueUpdate = function () {
    };
    return ControlledConceptEditorComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", data_file_layout_model_1.ControlledConceptLayout),
    __metadata("design:paramtypes", [data_file_layout_model_1.ControlledConceptLayout])
], ControlledConceptEditorComponent.prototype, "inputLayout", null);
ControlledConceptEditorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'controlled-concept-editor',
        templateUrl: 'controlled-concept-editor.component.html',
        styleUrls: ['controlled-concept-editor.component.css']
    }),
    __metadata("design:paramtypes", [data_file_editor_service_1.DataFileEditorService])
], ControlledConceptEditorComponent);
exports.ControlledConceptEditorComponent = ControlledConceptEditorComponent;
//# sourceMappingURL=controlled-concept-editor.component.js.map