"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_editor_service_1 = require("../data-file-editor.service");
var data_record_model_1 = require("../../../models/data-record.model");
var StringEditorComponent = (function () {
    function StringEditorComponent(recordEditorService) {
        this.editorService = recordEditorService;
        this.value = new data_record_model_1.StringValue();
    }
    Object.defineProperty(StringEditorComponent.prototype, "valueRequirement", {
        set: function (stringRequirement) {
            this.propertyRequirement = stringRequirement.parentProperty;
            this.stringRequirement = stringRequirement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StringEditorComponent.prototype, "targetRecord", {
        set: function (record) {
            this.record = record;
            this.recordProperty = this.record.getProperty(this.propertyRequirement.propertyId);
            if (this.recordProperty.value) {
                // If the input record contains the value target of this editor, 
                // then set the local value to that of the record.
                this.value = this.recordProperty.value;
                this.oldValue = this.value.string;
            }
            else {
                // If the input record does not contain the value target of this editor,
                // set the record to contain the value created by this editor.
                this.recordProperty.setValue(this.value);
                this.oldValue = null;
            }
        },
        enumerable: true,
        configurable: true
    });
    StringEditorComponent.prototype.announceValueUpdate = function () {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.value.string) {
            var fileUpdate = void 0;
            var valueUpdate = void 0;
            // Create the new value update.
            valueUpdate = new data_record_model_1.DataFileValueUpdate(this.record.recordId, this.propertyRequirement.propertyId, null);
            valueUpdate.setNewValue(this.value.getContent());
            // Announce the local file update.
            fileUpdate = new data_record_model_1.DataFileUpdate(this.record.getFileId());
            fileUpdate.addValueUpdate(valueUpdate);
            this.editorService.announceLocalUpdate(this.record, fileUpdate);
            // Set the old value to the current value.
            this.oldValue = this.value.string;
        }
    };
    return StringEditorComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", data_record_model_1.StringRequirement),
    __metadata("design:paramtypes", [data_record_model_1.StringRequirement])
], StringEditorComponent.prototype, "valueRequirement", null);
__decorate([
    core_1.Input(),
    __metadata("design:type", data_record_model_1.DataRecord),
    __metadata("design:paramtypes", [data_record_model_1.DataRecord])
], StringEditorComponent.prototype, "targetRecord", null);
StringEditorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'string-editor',
        templateUrl: 'string-editor.component.html',
        styleUrls: ['string-editor.component.css']
    }),
    __metadata("design:paramtypes", [data_file_editor_service_1.DataFileEditorService])
], StringEditorComponent);
exports.StringEditorComponent = StringEditorComponent;
//# sourceMappingURL=string-editor.component.js.map