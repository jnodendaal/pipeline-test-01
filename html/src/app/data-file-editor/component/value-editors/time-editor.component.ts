import { Component, Input, OnDestroy } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { PropertyRequirement, TimeRequirement } from '../../../core/model/data-requirement.model';
import { DataFileUpdate, DataFileValueUpdate, DataFileNewAttachment } from '../../model/data-file-editor.model';
import { TimeState } from '../../model/data-file-state.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import { SimpleDateFormat } from '../../../core/utility/date.utility';
import 
{ 
    DataRecord, 
    RecordProperty, 
    Time
} from '../../../core/model/data-record.model';
import * as moment from 'moment';
import { Moment } from 'moment';

@Component
({
    selector: 'time-editor',
    templateUrl: 'time-editor.component.html',
    styleUrls: ['time-editor.component.css']
})
export class TimeEditorComponent 
{   
    private editorService: DataFileEditorService;
    private oldValue: string; // Stores the old millisecond value to compare to when changes occur.
    private recordProperty: RecordProperty;
    private timeRequirement: TimeRequirement;
    private propertyRequirement: PropertyRequirement;
    private showDatePicker: boolean;
    private timeString: string;
    private formatPattern: string;
    private timeState: TimeState;
    
    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.showDatePicker = false;
        this.timeString = null;
    }

    @Input()
    set state(layout: TimeState) 
    {
        this.timeState = layout;
        this.propertyRequirement = layout.requirement.parentProperty;
        this.timeRequirement = layout.requirement;
        this.oldValue = layout.value.time;
        this.formatPattern = this.timeRequirement.dateTimeFormatPattern != null ? SimpleDateFormat.toMomentFormat(this.timeRequirement.dateTimeFormatPattern) : "hh:mm:ss";
        this.refreshTimeString();
    }

    public onBlur(): void
    {
        let enteredTime: Moment;

        enteredTime = moment(this.timeString, this.formatPattern);
        if (enteredTime.isValid())
        {
            this.timeState.value.time = enteredTime.toDate().getTime().toString();  
            this.announceValueUpdate();
        }
        else
        {
            this.timeState.value.time = this.timeString;    
            this.announceValueUpdate();
        }
    }

    public announceValueUpdate()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.timeState.value.time)
        {
            let fileUpdate: DataFileUpdate;
            let valueUpdate: DataFileValueUpdate;

            // Create the new value update.
            valueUpdate = new DataFileValueUpdate(this.timeState.record.recordId, this.timeRequirement.getPropertyId(), this.timeRequirement.getFieldId(), this.timeState.value.getContent());

            // Announce the local file update.
            fileUpdate = new DataFileUpdate(this.timeState.record.getFileId());
            fileUpdate.addValueUpdate(valueUpdate);
            this.editorService.announceLocalUpdate(this.timeState.record, fileUpdate);

            // Set the old value to the current value.
            this.oldValue = this.timeState.value.time;

            // Mark the property as updated.
            this.timeState.propertyState.setUpdated(true);
        }
    }

    private refreshTimeString()
    {
        let milliseconds: number;

        milliseconds = parseInt(this.timeState.value.time);
        if (isNaN(milliseconds))
        {
            this.timeString = this.timeState.value.time;
        }
        else
        {
            this.timeString = moment(milliseconds).format(this.formatPattern);
        }
    }
}