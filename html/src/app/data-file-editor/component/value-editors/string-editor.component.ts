import { Component, Input, OnDestroy } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { PropertyRequirement, StringRequirement } from '../../../core/model/data-requirement.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { StringState } from '../../model/data-file-state.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import 
{ 
    DataRecord, 
    RecordProperty, 
    StringValue
} from '../../../core/model/data-record.model';

@Component
({
    selector: 'string-editor',
    templateUrl: 'string-editor.component.html',
    styleUrls: ['string-editor.component.css']
})
export class StringEditorComponent 
{   
    private editorService: DataFileEditorService;
    private stringState: StringState;
    private oldValue: string; // Stores the old string value to compare to when changes occur.
    private recordProperty: RecordProperty;
    private stringRequirement: StringRequirement;
    private propertyRequirement: PropertyRequirement;
    
    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
    }

    @Input()
    set state(state: StringState) 
    {
        this.stringState = state;
        this.propertyRequirement = state.requirement.parentProperty;
        this.stringRequirement = state.requirement;
        this.oldValue = state.value.string;
    }

    public announceValueUpdate()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.stringState.value.string)
        {
            let fileUpdate: DataFileUpdate;
            let valueUpdate: DataFileValueUpdate;

            // Create the new value update.
            valueUpdate = new DataFileValueUpdate(this.stringState.record.recordId, this.stringRequirement.getPropertyId(), this.stringRequirement.getFieldId(), this.stringState.value.getContent());

            // Announce the local file update.
            fileUpdate = new DataFileUpdate(this.stringState.record.getFileId());
            fileUpdate.addValueUpdate(valueUpdate);
            this.editorService.announceLocalUpdate(this.stringState.record, fileUpdate);

            // Set the old value to the current value.
            this.oldValue = this.stringState.value.string;

            // Mark the property as updated.
            this.stringState.propertyState.setUpdated(true);
        }
    }
}
