import { Component, Input } from '@angular/core';
import { DataRecord, RecordProperty } from '../../../core/model/data-record.model';
import { FieldRequirement } from '../../../core/model/data-requirement.model';
import { FieldState } from '../../model/data-file-state.model';

@Component
({
    selector: 'field-editor',
    templateUrl: 'field-editor.component.html',
    styleUrls: ['field-editor.component.css']
})
export class FieldEditorComponent  
{   
    @Input()
    private state: FieldState;
    
    constructor()
    {
    }
}
