"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_file_layout_model_1 = require("../data-file-layout.model");
var DocumentReferenceEditorComponent = (function () {
    function DocumentReferenceEditorComponent() {
        this.referencedRecords = new Array();
        this.deletionEnabled = true;
        this.activeLayout = null;
    }
    Object.defineProperty(DocumentReferenceEditorComponent.prototype, "inputLayout", {
        set: function (layout) {
            this.layout = layout;
            if (this.layout.value) {
                var value = void 0;
                // If the input record contains the value target of this editor, 
                // then set the local value to that of the record.
                value = this.layout.value;
                for (var _i = 0, _a = value.references; _i < _a.length; _i++) {
                    var reference = _a[_i];
                    var referencedRecord = void 0;
                    referencedRecord = this.layout.record.getRecord(reference);
                    if (referencedRecord) {
                        this.referencedRecords.push(referencedRecord);
                    }
                    else {
                        console.log("Referenced record not found: " + reference);
                    }
                }
                // If the layout defines a pull-up, set the first active record as the referenced record.
                if (this.layout.pullUp) {
                    if (this.layout.recordLayouts.length > 0) {
                        this.activeLayout = this.layout.recordLayouts[0];
                    }
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    DocumentReferenceEditorComponent.prototype.removeReference = function (recordId) {
        console.log("Reference removed: " + recordId);
    };
    DocumentReferenceEditorComponent.prototype.referenceSelected = function (recordId) {
    };
    DocumentReferenceEditorComponent.prototype.referenceDeselected = function (recordId) {
    };
    return DocumentReferenceEditorComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", data_file_layout_model_1.DocumentReferenceLayout),
    __metadata("design:paramtypes", [data_file_layout_model_1.DocumentReferenceLayout])
], DocumentReferenceEditorComponent.prototype, "inputLayout", null);
DocumentReferenceEditorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'document-reference-editor',
        templateUrl: 'document-reference-editor.component.html',
        styleUrls: ['document-reference-editor.component.css']
    }),
    __metadata("design:paramtypes", [])
], DocumentReferenceEditorComponent);
exports.DocumentReferenceEditorComponent = DocumentReferenceEditorComponent;
//# sourceMappingURL=document-reference-editor.component.js.map