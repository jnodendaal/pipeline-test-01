import { Component, Input } from '@angular/core';
import { BooleanState } from '../../model/data-file-state.model';
import { PropertyRequirement, BooleanRequirement } from '../../../core/model/data-requirement.model';
import { DataRecord, RecordProperty, BooleanValue } from '../../../core/model/data-record.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';

@Component
({
    selector: 'boolean-editor',
    templateUrl: 'boolean-editor.component.html',
    styleUrls: ['boolean-editor.component.css']
})
export class BooleanEditorComponent  
{   
    private editorService: DataFileEditorService;

    @Input()
    private state: BooleanState;
    
    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
    }

    onValueChange(event: any)
    {
        let update: DataFileUpdate;
        let record: DataRecord;
        let selecedValue: boolean;

        // Determine the value that has been selected.
        switch (event.target.id)
        {
            case "radioTrue":
                selecedValue = true;
                break;
            case "radioFalse":
                selecedValue = false;
                break;
            case "radioUnspecified":
                selecedValue = null;
                break;
        }

        // Set the local value to the one selected.
        this.state.value.value = selecedValue;

        // Announce the local update.
        record = this.state.value.getParentRecord();
        update = new DataFileUpdate(record.getFile().getFileId());
        update.addValueUpdate(new DataFileValueUpdate(record.recordId, this.state.requirement.getPropertyId(), this.state.requirement.getFieldId(), this.state.value.getContent()));
        this.editorService.announceLocalUpdate(record, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }
}
