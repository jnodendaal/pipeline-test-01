import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataRecord, RecordProperty, ControlledConcept } from '../../../core/model/data-record.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { PropertyRequirement, ControlledConceptRequirement } from '../../../core/model/data-requirement.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { ControlledConceptState } from '../../model/data-file-state.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { TypeaheadDirective } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component
({
    selector: 'controlled-concept-editor',
    templateUrl: 'controlled-concept-editor.component.html',
    styleUrls: ['controlled-concept-editor.component.css']
})
export class ControlledConceptEditorComponent  
{   
    private editorService: DataFileEditorService;
    public typeaheadLoading: boolean;
    public typeaheadNoResults: boolean;
    public dataSource: Observable<any>;
    private isValueLookupShown: boolean;
    private error: boolean;

    @Input()
    private state: ControlledConceptState;
    
    @ViewChild(TypeaheadDirective)
    private typeahead: TypeaheadDirective;

    @ViewChild('autoShownModal') 
    private autoShownModal: ModalDirective;

    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
        this.isValueLookupShown = false;
        this.error = true;
        this.dataSource = Observable.create((observer: any) => 
        {
            // Runs on every search.
            observer.next(this.state.value.term);
        }).mergeMap((token: string) => this.getStatesAsObservable(token));
    }

    public getStatesAsObservable(token: string): Observable<any> 
    {
        return this.editorService.retrieveValueSuggestions(this.state.record.getFileId(), this.state.record.recordId, this.state.requirement.getPropertyId(), this.state.requirement.getFieldId(), this.state.value.type, token, 0, 7);
    }

    public changeTypeaheadLoading(e: boolean): void 
    {
        this.typeaheadLoading = e;
    }

    public changeTypeaheadNoResults(e: boolean): void 
    {
        this.typeaheadNoResults = e;
    }

    public onTypeaheadSelection(e: TypeaheadMatch): void 
    {
        this.setSelectedValue(e.item as ControlledConcept);
    }

    public onLookupSelection(value: ControlledConcept)
    {
        this.hideValueLookup();
        this.setSelectedValue(value);
    }

    public setSelectedValue(value: ControlledConcept)
    {
        let update: DataFileUpdate;
        let record: DataRecord;

        // Set all local values to those of the selected item.
        if (value != null)
        {
            this.state.value.conceptId = value.conceptId;
            this.state.value.code = value.code;
            this.state.value.term = value.term;
            this.state.value.definition = value.definition;
            this.state.value.abbreviation = value.abbreviation;
        }
        else
        {
            this.state.value.conceptId = null;
            this.state.value.code = null;
            this.state.value.term = null;
            this.state.value.definition = null;
            this.state.value.abbreviation = null;
        }

        // Announce the local update.
        record = this.state.value.getParentRecord();
        update = new DataFileUpdate(record.getFile().getFileId());
        update.addValueUpdate(new DataFileValueUpdate(record.recordId, this.state.requirement.getPropertyId(), this.state.requirement.getFieldId(), this.state.value.getContent()));
        this.editorService.announceLocalUpdate(record, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }

    public announceValueUpdate()
    {
    }

    public showOptions(e: any)
    {
        // When this method is called, the minimum length of the string required for typeahead option retrieval is set to 0,
        // allowing the options dropdown to be displayed immediately.  This is a hack to work around the absense of a proper
        // show(stringToMatch: string) method in the TypeaheadDirective.
        this.typeahead.typeaheadMinLength = 0;
    }

    public hideOptions(e: any)
    {
        // When this method is called, the minimum length of the string required for typeahead option retrieval is set to 1,
        // preventing the options dropdown to be displayed immediately.  This is a hack to work around the absense of a proper
        // show(stringToMatch: string) method in the TypeaheadDirective.
        this.typeahead.typeaheadMinLength = 1;
    }

    public showValueLookup():void 
    {
        this.isValueLookupShown = true;
    }

    public hideValueLookup():void 
    {
        this.autoShownModal.hide();
    }

    public onValueLookupHidden():void 
    {
        this.isValueLookupShown = false;
    }
}
