import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataRecord, RecordProperty, MeasuredRange, UnitOfMeasure } from '../../../core/model/data-record.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { PropertyRequirement, MeasuredRangeRequirement } from '../../../core/model/data-requirement.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { MeasuredRangeState } from '../../model/data-file-state.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { TypeaheadDirective } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component
({
    selector: 'measured-range-editor',
    templateUrl: 'measured-range-editor.component.html',
    styleUrls: ['measured-range-editor.component.css']
})
export class MeasuredRangeEditorComponent  
{   
    private editorService: DataFileEditorService;
    private rangeState: MeasuredRangeState;
    public typeaheadLoading: boolean;
    public typeaheadNoResults: boolean;
    public dataSource: Observable<any>;
    public isValueLookupShown: boolean;
    private oldLowerBoundValue: string; // Stores the old lower bound number to compare to when changes occur.
    private oldUpperBoundValue: string; // Stores the old upper bound number to compare to when changes occur.
    private error: boolean;

    @ViewChild(TypeaheadDirective)
    private typeahead: TypeaheadDirective;

    @ViewChild('autoShownModal') 
    private autoShownModal: ModalDirective;

    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
        this.isValueLookupShown = false;
        this.error = true;
        this.dataSource = Observable.create((observer: any) => 
        {
            // Runs on every search.
            observer.next(this.rangeState.value.unitOfMeasure.term);
        }).mergeMap((token: string) => this.getStatesAsObservable(token));
    }

    @Input()
    set state(state: MeasuredRangeState) 
    {
        this.rangeState = state;
        console.log(state.value);
        this.oldLowerBoundValue = state.value.lowerBoundNumber.value;
        this.oldUpperBoundValue = state.value.upperBoundNumber.value;
    }

    public getStatesAsObservable(token: string): Observable<any> 
    {
        return this.editorService.retrieveValueSuggestions(this.rangeState.record.getFileId(), this.rangeState.record.recordId, this.rangeState.value.parentProperty.propertyId, null, this.rangeState.value.unitOfMeasure.type, token, 0, 7);
    }

    public changeTypeaheadLoading(e: boolean): void 
    {
        this.typeaheadLoading = e;
    }

    public changeTypeaheadNoResults(e: boolean): void 
    {
        this.typeaheadNoResults = e;
    }

    public onTypeaheadSelection(e: TypeaheadMatch): void 
    {
        this.setSelectedValue(e.item as UnitOfMeasure);
    }

    public onLookupSelection(value: UnitOfMeasure)
    {
        this.hideValueLookup();
        this.setSelectedValue(value);
    }

    public setSelectedValue(uom: UnitOfMeasure): void 
    {
        // Set all local values to those of the selected item.
        if (uom != null)
        {
            this.rangeState.value.unitOfMeasure.conceptId = uom.conceptId;
            this.rangeState.value.unitOfMeasure.code = uom.code;
            this.rangeState.value.unitOfMeasure.term = uom.term;
            this.rangeState.value.unitOfMeasure.definition = uom.definition;
            this.rangeState.value.unitOfMeasure.abbreviation = uom.abbreviation;
        }
        else
        {
            this.rangeState.value.unitOfMeasure.conceptId = null;
            this.rangeState.value.unitOfMeasure.code = null;
            this.rangeState.value.unitOfMeasure.term = null;
            this.rangeState.value.unitOfMeasure.definition = null;
            this.rangeState.value.unitOfMeasure.abbreviation = null;
        }

        // Announce the local update.
        this.announceValueUpdate();
    }

    public onLowerBoundBlur()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldLowerBoundValue != this.rangeState.value.lowerBoundNumber.value)
        {
            // Announce the local value update.
            this.announceValueUpdate();
            
            // Set the old value to the current value.
            this.oldLowerBoundValue = this.rangeState.value.lowerBoundNumber.value;
        }
    }

    public onUpperBoundBlur()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldUpperBoundValue != this.rangeState.value.upperBoundNumber.value)
        {
            // Announce the local value update.
            this.announceValueUpdate();
            
            // Set the old value to the current value.
            this.oldUpperBoundValue = this.rangeState.value.upperBoundNumber.value;
        }
    }

    public announceValueUpdate()
    {
        let update: DataFileUpdate;
        let record: DataRecord;

        record = this.rangeState.value.getParentRecord();
        update = new DataFileUpdate(record.getFile().getFileId());
        update.addValueUpdate(new DataFileValueUpdate(record.recordId, this.rangeState.value.parentProperty.propertyId, null, this.rangeState.value.getContent()));
        this.editorService.announceLocalUpdate(record, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }

    public showOptions(e: any)
    {
        // When this method is called, the minimum length of the string required for typeahead option retrieval is set to 0,
        // allowing the options dropdown to be displayed immediately.  This is a hack to work around the absense of a proper
        // show(stringToMatch: string) method in the TypeaheadDirective.
        this.typeahead.typeaheadMinLength = 0;
    }

    public hideOptions(e: any)
    {
        // When this method is called, the minimum length of the string required for typeahead option retrieval is set to 1,
        // preventing the options dropdown to be displayed immediately.  This is a hack to work around the absense of a proper
        // show(stringToMatch: string) method in the TypeaheadDirective.
        this.typeahead.typeaheadMinLength = 1;
    }

    public showValueLookup():void 
    {
        this.isValueLookupShown = true;
    }

    public hideValueLookup():void 
    {
        this.autoShownModal.hide();
    }

    public onValueLookupHidden():void 
    {
        this.isValueLookupShown = false;
    }
}
