import { Component, Input, OnDestroy } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { PropertyRequirement, DateRequirement } from '../../../core/model/data-requirement.model';
import { DateState } from '../../model/data-file-state.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { SimpleDateFormat } from '../../../core/utility/date.utility';
import 
{ 
    DataRecord, 
    RecordProperty, 
    DateTime
} from '../../../core/model/data-record.model';
import * as moment from 'moment';
import { Moment } from 'moment';

@Component
({
    selector: 'date-editor',
    templateUrl: 'date-editor.component.html',
    styleUrls: ['date-editor.component.css']
})
export class DateEditorComponent 
{   
    private editorService: DataFileEditorService;
    private dateState: DateState;
    private oldValue: string; // Stores the old millisecond value to compare to when changes occur.
    private recordProperty: RecordProperty;
    private dateRequirement: DateRequirement;
    private propertyRequirement: PropertyRequirement;
    private showDatePicker: boolean;
    private date: Date;
    private dateString: string;
    private formatPattern: string;
    
    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.showDatePicker = false;
        this.date = null;
        this.dateString = null;
    }

    @Input()
    set state(state: DateState) 
    {
        this.dateState = state;
        this.propertyRequirement = state.requirement.parentProperty;
        this.dateRequirement = state.requirement;
        this.date = state.value.date != null ? new Date(parseInt(state.value.date)) : null;
        this.oldValue = state.value.date;
        this.formatPattern = this.dateRequirement.dateTimeFormatPattern != null ? SimpleDateFormat.toMomentFormat(this.dateRequirement.dateTimeFormatPattern) : "YYYY-MM-DD";
        this.updateDateString();
    }

    public announceValueUpdate()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.dateState.value.date)
        {
            let fileUpdate: DataFileUpdate;
            let valueUpdate: DataFileValueUpdate;

            // Create the new value update.
            valueUpdate = new DataFileValueUpdate(this.dateState.record.recordId, this.dateRequirement.getPropertyId(), this.dateRequirement.getFieldId(), this.dateState.value.getContent());

            // Announce the local file update.
            fileUpdate = new DataFileUpdate(this.dateState.record.getFileId());
            fileUpdate.addValueUpdate(valueUpdate);
            this.editorService.announceLocalUpdate(this.dateState.record, fileUpdate);

            // Set the old value to the current value.
            this.oldValue = this.dateState.value.date;

            // Mark the property as updated.
            this.dateState.propertyState.setUpdated(true);
        }
    }

    public onBlur(): void
    {
        let enteredDate: Moment;

        enteredDate = moment(this.dateString, this.formatPattern);
        if (enteredDate.isValid())
        {
            this.date = enteredDate.toDate();
            this.dateState.value.date = this.date.getTime().toString();    
            this.updateDateString();
            this.announceValueUpdate();
        }
        else
        {
            this.dateState.value.date = this.dateString;    
            this.updateDateString();
            this.announceValueUpdate();
        }
    }

    public onDateSelected(): void
    {
        this.toggleDatePicker();
    }

    public onPickerModelChange(event: any)
    {
        if (this.date)
        {
            console.log(this.date);
            this.dateState.value.date = this.date.getTime().toString();    
            this.updateDateString();
            this.announceValueUpdate();
        }
        else
        {
            this.dateState.value.date = null;
            this.updateDateString();
            this.announceValueUpdate();
        }
    }

    public toggleDatePicker(): void
    {
        this.showDatePicker = !this.showDatePicker;
    }

    private updateDateString()
    {
        this.dateString = this.date != null ? moment(this.date).format(this.formatPattern) : null;
    }
}
