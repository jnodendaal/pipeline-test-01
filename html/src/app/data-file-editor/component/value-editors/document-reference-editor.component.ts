import { Component, Input, Output, ViewChild, EventEmitter, OnDestroy } from '@angular/core';
import { PropertyRequirement, DocumentReferenceListRequirement } from '../../../core/model/data-requirement.model';
import { DataRecord, RecordProperty, DocumentReferenceList, DocumentReference, DocumentReferenceListContent } from '../../../core/model/data-record.model';
import { DataFileUpdate, DataFileNewRecord, DataFileRecordCreation, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { RecordState, DocumentReferenceListState } from '../../model/data-file-state.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs/Subscription';

@Component
({
    selector: 'document-reference-editor',
    templateUrl: 'document-reference-editor.component.html',
    styleUrls: ['document-reference-editor.component.css']
})
export class DocumentReferenceEditorComponent implements OnDestroy  
{   
    private editorService: DataFileEditorService;
    private deletionEnabled: boolean;
    private valueLookupVisible: boolean;
    private deletionConfirmationVisible: boolean;
    private referenceIdToBeDeleted: string;
    private updateSubscription: Subscription;
    private pendingNavigationRecordId: string;
    private loading: boolean;

    @Input()
    private state: DocumentReferenceListState;
    
    @ViewChild('deletionConfirmationModal') 
    private deletionConfirmationModal: ModalDirective;

    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
        this.deletionEnabled = true;
        this.valueLookupVisible = false;
        this.deletionConfirmationVisible = false;
        this.referenceIdToBeDeleted = null;
        this.loading = false;
        this.pendingNavigationRecordId = null;
        this.updateSubscription = this.editorService.updateCycleEndAnnounced$
            .subscribe
            (
                data => 
                {
                    // If the record to which our panding navigation refers has been created, navigate to it now.
                    if (this.pendingNavigationRecordId)
                    {
                        let newRecord: DataFileNewRecord;

                        // See if the remote update contained the new record for which we are waiting.
                        newRecord =  data.getNewRecordByTemporaryId(this.pendingNavigationRecordId);
                        if (newRecord)
                        {
                            // Make sure to reset the pending navigation id before navigation.
                            this.pendingNavigationRecordId = null;
                            this.navigateToRecord(newRecord.newRecord.recordId);
                        }
                    }
                }
            );
    }

    ngOnDestroy() 
    {
        this.updateSubscription.unsubscribe();
    }

    @Output() recordNavigation: EventEmitter<string> = new EventEmitter<string>();
    navigateToRecord(recordId: string)
    {
        this.recordNavigation.emit(recordId);
    }

    private showDeletionConfirmation(referenceId: string)
    {
        this.referenceIdToBeDeleted = referenceId;
        this.deletionConfirmationVisible = true;
    }

    private hideDeletionConfirmation()
    {
        this.referenceIdToBeDeleted = null;
        this.deletionConfirmationModal.hide();
    }

    private onDeletionConfirmationHidden()
    {
        this.deletionConfirmationVisible = false;
    }

    private confirmDeletion()
    {
        this.removeReference(this.referenceIdToBeDeleted);
        this.referenceIdToBeDeleted = null;
        this.hideDeletionConfirmation();
    }

    private removeReference(recordId: string)
    {
        let referrerProperty: RecordProperty;
        let newRecordLayout: RecordState;
        let documentReferenceList: DocumentReferenceList;
        let documentReferenceListContent: DocumentReferenceListContent;
        let update: DataFileUpdate;
        let referrerRecord: DataRecord;

        // Remove the reference to this record from the referencing property.
        // Removal of the actual sub-record will be handled on the server and subsequently on the client once a remote update is received.
        console.log("Deleting reference: " + recordId);
        referrerProperty = this.state.value.parentProperty;
        referrerRecord = referrerProperty.getRecord();
        documentReferenceList = this.state.value as DocumentReferenceList;
        documentReferenceListContent = documentReferenceList.getContent();
        documentReferenceListContent.removeReference(recordId);

        // Announce the local update.
        update = new DataFileUpdate(referrerRecord.getFile().getFileId());
        update.addValueUpdate(new DataFileValueUpdate(referrerRecord.recordId, referrerProperty.propertyId, null, documentReferenceListContent));
        this.editorService.announceLocalUpdate(referrerRecord, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }

    public onLookupSelection(value: DocumentReferenceList)
    {
        this.hideValueLookup();
        this.addNewRecord(value.references[0]);
    }

    public showValueLookup(): void 
    {
        this.valueLookupVisible = true;
    }

    public hideValueLookup(): void 
    {
        this.valueLookupVisible = false;
    }

    private addNewRecord(newReference: DocumentReference)
    {
        let update: DataFileUpdate;
        let record: DataRecord;
        let temporaryId: string;

        // Generate a temporary id for the new record.
        temporaryId = Date.now().toString();
        this.pendingNavigationRecordId = temporaryId; // The record will be created asynchronously on the server. After creation, we want to navigate to it.

        // Announce the local update.
        record = this.state.value.getParentRecord();
        update = new DataFileUpdate(record.getFile().getFileId());
        update.addRecordCreation(new DataFileRecordCreation(record.recordId, this.state.value.parentProperty.propertyId, null, newReference.conceptId, temporaryId));
        this.editorService.announceLocalUpdate(record, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }
}
