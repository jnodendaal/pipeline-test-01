import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataRecord, RecordProperty, MeasuredNumber, UnitOfMeasure } from '../../../core/model/data-record.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { PropertyRequirement, MeasuredNumberRequirement } from '../../../core/model/data-requirement.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { MeasureNumberState } from '../../model/data-file-state.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { TypeaheadDirective } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component
({
    selector: 'measured-number-editor',
    templateUrl: 'measured-number-editor.component.html',
    styleUrls: ['measured-number-editor.component.css']
})
export class MeasuredNumberEditorComponent  
{   
    private editorService: DataFileEditorService;
    public typeaheadLoading: boolean;
    public typeaheadNoResults: boolean;
    public dataSource: Observable<any>;
    public isValueLookupShown: boolean;
    private oldValue: string; // Stores the old number to compare to when changes occur.
    private error: boolean;

    @Input()
    private state: MeasureNumberState;

    @ViewChild(TypeaheadDirective)
    private typeahead: TypeaheadDirective;

    @ViewChild('autoShownModal') 
    private autoShownModal: ModalDirective;

    constructor(editorService: DataFileEditorService)
    {
        this.editorService = editorService;
        this.isValueLookupShown = false;
        this.error = true;
        this.dataSource = Observable.create((observer: any) => 
        {
            // Runs on every search.
            observer.next(this.state.value.unitOfMeasure.term);
        }).mergeMap((token: string) => this.getStatesAsObservable(token));
    }

    public getStatesAsObservable(token: string): Observable<any> 
    {
        return this.editorService.retrieveValueSuggestions(this.state.record.getFileId(), this.state.record.recordId, this.state.value.parentProperty.propertyId, null, this.state.value.unitOfMeasure.type, token, 0, 7);
    }

    public changeTypeaheadLoading(e: boolean): void 
    {
        this.typeaheadLoading = e;
    }

    public changeTypeaheadNoResults(e: boolean): void 
    {
        this.typeaheadNoResults = e;
    }

    public onTypeaheadSelection(e: TypeaheadMatch): void 
    {
        this.setSelectedValue(e.item as UnitOfMeasure);
    }

    public onLookupSelection(value: UnitOfMeasure)
    {
        this.hideValueLookup();
        this.setSelectedValue(value);
    }

    public setSelectedValue(uom: UnitOfMeasure): void 
    {
        // Set all local values to those of the selected item.
        if (uom != null)
        {
            this.state.value.unitOfMeasure.conceptId = uom.conceptId;
            this.state.value.unitOfMeasure.code = uom.code;
            this.state.value.unitOfMeasure.term = uom.term;
            this.state.value.unitOfMeasure.definition = uom.definition;
            this.state.value.unitOfMeasure.abbreviation = uom.abbreviation;
        }
        else
        {
            this.state.value.unitOfMeasure.conceptId = null;
            this.state.value.unitOfMeasure.code = null;
            this.state.value.unitOfMeasure.term = null;
            this.state.value.unitOfMeasure.definition = null;
            this.state.value.unitOfMeasure.abbreviation = null;
        }

        // Announce the local update.
        this.announceValueUpdate();
    }

    public onValueBlur()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.state.value.number.number)
        {
            // Announce the local value update.
            this.announceValueUpdate();
            
            // Set the old value to the current value.
            this.oldValue = this.state.value.number.number;
        }
    }

    public announceValueUpdate()
    {
        let update: DataFileUpdate;
        let record: DataRecord;

        record = this.state.value.getParentRecord();
        update = new DataFileUpdate(record.getFile().getFileId());
        update.addValueUpdate(new DataFileValueUpdate(record.recordId, this.state.value.parentProperty.propertyId, null, this.state.value.getContent()));
        this.editorService.announceLocalUpdate(record, update);

        // Mark the property as updated.
        this.state.propertyState.setUpdated(true);
    }

    public showOptions(e: any)
    {
        // When this method is called, the minimum length of the string required for typeahead option retrieval is set to 0,
        // allowing the options dropdown to be displayed immediately.  This is a hack to work around the absense of a proper
        // show(stringToMatch: string) method in the TypeaheadDirective.
        this.typeahead.typeaheadMinLength = 0;
    }

    public hideOptions(e: any)
    {
        // When this method is called, the minimum length of the string required for typeahead option retrieval is set to 1,
        // preventing the options dropdown to be displayed immediately.  This is a hack to work around the absense of a proper
        // show(stringToMatch: string) method in the TypeaheadDirective.
        this.typeahead.typeaheadMinLength = 1;
    }

    public showValueLookup():void 
    {
        this.isValueLookupShown = true;
    }

    public hideValueLookup():void 
    {
        this.autoShownModal.hide();
    }

    public onValueLookupHidden():void 
    {
        this.isValueLookupShown = false;
    }
}
