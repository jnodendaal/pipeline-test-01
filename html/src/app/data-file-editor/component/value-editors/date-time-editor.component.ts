import { Component, Input, OnDestroy } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { PropertyRequirement, DateTimeRequirement } from '../../../core/model/data-requirement.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { DateTimeState } from '../../model/data-file-state.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import { SimpleDateFormat } from '../../../core/utility/date.utility';
import 
{ 
    DataRecord, 
    RecordProperty, 
    DateTime
} from '../../../core/model/data-record.model';
import * as moment from 'moment';
import { Moment } from 'moment';

@Component
({
    selector: 'date-time-editor',
    templateUrl: 'date-time-editor.component.html',
    styleUrls: ['date-time-editor.component.css']
})
export class DateTimeEditorComponent 
{   
    private editorService: DataFileEditorService;
    private dateTimeState: DateTimeState;
    private oldValue: string; // Stores the old millisecond value to compare to when changes occur.
    private recordProperty: RecordProperty;
    private dateTimeRequirement: DateTimeRequirement;
    private propertyRequirement: PropertyRequirement;
    private showDatePicker: boolean;
    private dateTime: Date;
    private dateTimeString: string;
    private formatPattern: string;
    
    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
        this.showDatePicker = false;
        this.dateTime = null;
        this.dateTimeString = null;
    }

    @Input()
    set state(state: DateTimeState) 
    {
        this.dateTimeState = state;
        this.propertyRequirement = state.requirement.parentProperty;
        this.dateTimeRequirement = state.requirement;
        this.dateTime = state.value.dateTime != null ? new Date(parseInt(state.value.dateTime)) : null;
        this.oldValue = state.value.dateTime;
        this.formatPattern = this.dateTimeRequirement.dateTimeFormatPattern != null ? SimpleDateFormat.toMomentFormat(this.dateTimeRequirement.dateTimeFormatPattern) : "YYYY-MM-DD hh:mm:ss";
        this.updateDateTimeString();
    }

    public announceValueUpdate()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.dateTimeState.value.dateTime)
        {
            let fileUpdate: DataFileUpdate;
            let valueUpdate: DataFileValueUpdate;

            // Create the new value update.
            valueUpdate = new DataFileValueUpdate(this.dateTimeState.record.recordId, this.dateTimeRequirement.getPropertyId(), this.dateTimeRequirement.getFieldId(), this.dateTimeState.value.getContent());

            // Announce the local file update.
            fileUpdate = new DataFileUpdate(this.dateTimeState.record.getFileId());
            fileUpdate.addValueUpdate(valueUpdate);
            this.editorService.announceLocalUpdate(this.dateTimeState.record, fileUpdate);

            // Set the old value to the current value.
            this.oldValue = this.dateTimeState.value.dateTime;

            // Mark the property as updated.
            this.dateTimeState.propertyState.setUpdated(true);
        }
    }

    public onBlur(): void
    {
        let enteredDateTime: Moment;

        enteredDateTime = moment(this.dateTimeString, this.formatPattern);
        if (enteredDateTime.isValid())
        {
            this.dateTime = enteredDateTime.toDate();
            this.dateTimeState.value.dateTime = this.dateTime.getTime().toString();    
            this.updateDateTimeString();
            this.announceValueUpdate();
        }
        else
        {
            this.dateTimeState.value.dateTime = this.dateTimeString;    
            this.updateDateTimeString();
            this.announceValueUpdate();
        }
    }

    public onDateTimeSelected(): void
    {
        this.toggleDatePicker();
    }

    public onPickerModelChange(event: any)
    {
        if (this.dateTime)
        {
            this.dateTimeState.value.dateTime = this.dateTime.getTime().toString();    
            this.updateDateTimeString();
            this.announceValueUpdate();
        }
        else
        {
            this.dateTimeState.value.dateTime = null;
            this.updateDateTimeString();
            this.announceValueUpdate();
        }
    }

    public toggleDatePicker(): void
    {
        this.showDatePicker = !this.showDatePicker;
    }

    private updateDateTimeString()
    {
        this.dateTimeString = this.dateTime != null ? moment(this.dateTime).format(this.formatPattern) : null;
    }
}
