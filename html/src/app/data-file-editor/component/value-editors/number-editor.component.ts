import { Component, Input, OnDestroy } from '@angular/core';
import { DataFileEditorService } from '../../service/data-file-editor.service';
import { PropertyRequirement, NumberRequirement } from '../../../core/model/data-requirement.model';
import { DataFileUpdate, DataFileValueUpdate } from '../../model/data-file-editor.model';
import { NumberState } from '../../model/data-file-state.model';
import { PropertyValidationWarning } from '../../../core/model/data-record-validation.model';
import 
{ 
    DataRecord, 
    RecordProperty, 
    StringValue
} from '../../../core/model/data-record.model';

@Component
({
    selector: 'number-editor',
    templateUrl: 'number-editor.component.html',
    styleUrls: ['number-editor.component.css']
})
export class NumberEditorComponent 
{   
    private editorService: DataFileEditorService;
    private oldValue: string; // Stores the old string value to compare to when changes occur.
    private recordProperty: RecordProperty;

    @Input()
    private state: NumberState;
    
    constructor(recordEditorService: DataFileEditorService)
    {
        this.editorService = recordEditorService;
    }

    public announceValueUpdate()
    {
        // Make sure never to report a local change if the old and new values are the same.
        if (this.oldValue != this.state.value.number)
        {
            let fileUpdate: DataFileUpdate;
            let valueUpdate: DataFileValueUpdate;

            // Create the new value update.
            valueUpdate = new DataFileValueUpdate(this.state.record.recordId, this.state.requirement.getPropertyId(), this.state.requirement.getFieldId(), this.state.value.getContent());

            // Announce the local file update.
            fileUpdate = new DataFileUpdate(this.state.record.getFileId());
            fileUpdate.addValueUpdate(valueUpdate);
            this.editorService.announceLocalUpdate(this.state.record, fileUpdate);

            // Set the old value to the current value.
            this.oldValue = this.state.value.number;

            // Mark the property as updated.
            this.state.propertyState.setUpdated(true);
        }
    }
}
