"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var shared_module_1 = require("../../shared.module");
var data_file_editor_component_1 = require("./data-file-editor.component");
var default_layout_view_component_1 = require("./default-layout/default-layout-view.component");
var paged_layout_view_component_1 = require("./paged-layout/paged-layout-view.component");
var navigation_view_component_1 = require("./navigation-view.component");
var notification_view_component_1 = require("./notification-view.component");
var page_navigation_view_component_1 = require("./paged-layout/page-navigation-view.component");
var page_view_component_1 = require("./paged-layout/page-view.component");
var document_view_component_1 = require("./document-view.component");
var record_editor_component_1 = require("./record-editor.component");
var section_editor_component_1 = require("./section-editor.component");
var property_editor_component_1 = require("./property-editor.component");
var boolean_editor_component_1 = require("./value-editors/boolean-editor.component");
var string_editor_component_1 = require("./value-editors/string-editor.component");
var text_editor_component_1 = require("./value-editors/text-editor.component");
var controlled_concept_editor_component_1 = require("./value-editors/controlled-concept-editor.component");
var document_reference_editor_component_1 = require("./value-editors/document-reference-editor.component");
var data_file_editor_service_1 = require("./data-file-editor.service");
var DataFileEditorModule = (function () {
    function DataFileEditorModule() {
    }
    return DataFileEditorModule;
}());
DataFileEditorModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ngx_bootstrap_1.Ng2BootstrapModule,
            shared_module_1.SharedModule
        ],
        declarations: [
            data_file_editor_component_1.DataFileEditorComponent,
            default_layout_view_component_1.DefaultLayoutViewComponent,
            paged_layout_view_component_1.PagedLayoutViewComponent,
            record_editor_component_1.RecordEditorComponent,
            navigation_view_component_1.NavigationViewComponent,
            page_navigation_view_component_1.PageNavigationViewComponent,
            page_view_component_1.PageViewComponent,
            notification_view_component_1.NotificationViewComponent,
            document_view_component_1.DocumentViewComponent,
            section_editor_component_1.SectionEditorComponent,
            property_editor_component_1.PropertyEditorComponent,
            string_editor_component_1.StringEditorComponent,
            text_editor_component_1.TextEditorComponent,
            boolean_editor_component_1.BooleanEditorComponent,
            controlled_concept_editor_component_1.ControlledConceptEditorComponent,
            document_reference_editor_component_1.DocumentReferenceEditorComponent
        ],
        exports: [data_file_editor_component_1.DataFileEditorComponent],
        providers: [data_file_editor_service_1.DataFileEditorService]
    })
], DataFileEditorModule);
exports.DataFileEditorModule = DataFileEditorModule;
//# sourceMappingURL=data-file-editor.module.js.map