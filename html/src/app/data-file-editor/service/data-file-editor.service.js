"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Subject_1 = require("rxjs/Subject");
var data_record_model_1 = require("../../models/data-record.model");
var data_file_editor_model_1 = require("./data-file-editor.model");
require("rxjs/add/operator/map");
var DataFileEditorService = (function () {
    function DataFileEditorService(http) {
        this.http = http;
        // Observable file update sources.
        this.localUpdateAnnouncedSource = new Subject_1.Subject();
        this.remoteUpdateAnnouncedSource = new Subject_1.Subject();
        this.updateCycleEndAnnouncedSource = new Subject_1.Subject();
        // Observable file update streams.
        this.localUpdateAnnounced$ = this.localUpdateAnnouncedSource.asObservable();
        this.remoteUpdateAnnounced$ = this.remoteUpdateAnnouncedSource.asObservable();
        this.updateCycleEndAnnounced$ = this.updateCycleEndAnnouncedSource.asObservable();
        console.log("Data File Editor Service Constructor executing...");
        this.editorSessionId = "TEST_DF_EDITOR_SESSION_ID";
        this.contextUrl = "http://bouwer:8080/TECH8/api/operation/";
    }
    /**
     * Update cycle for all records that form part of the data file editor:
     * 1. Local record is edited by user.
     * 2. Local record announces local update to service using {announceLocalUpdate}.
     * 3. Service observers are notified of local update via {localUpdateAnnounced$} observable.
     * 4. Service transmits local update with remote server.
     * 5. Remote server reponds with remote update to local service.
     * 6. Local service announces remote update using {announceRemoteUpdate}.
     * 7. Service observers are notified of remote update via {remoteUpdateAnnounced$} observable.
     */
    DataFileEditorService.prototype.announceLocalUpdate = function (sourceRecord, update) {
        var _this = this;
        // Announce the local update to all observers.
        console.log("Local update announced from source record: " + sourceRecord.recordId);
        console.log(update);
        this.localUpdateAnnouncedSource.next(update);
        // Send the local update to the remote server.
        this.updateFile(update)
            .subscribe(function (data) {
            // Process the remote update.
            _this.processRemoteUpdate(sourceRecord, data);
            // Announce the remote update (if any) to local observers.
            _this.announceRemoteUpdate(data);
        }, function (error) {
            console.log("Error while transmitting local update to server: " + error);
        });
    };
    DataFileEditorService.prototype.processRemoteUpdate = function (sourceRecord, update) {
        console.log(update);
    };
    DataFileEditorService.prototype.announceRemoteUpdate = function (update) {
        console.log("Remote update announced.");
        this.remoteUpdateAnnouncedSource.next(update);
        console.log("Update cycle end announced.");
        this.updateCycleEndAnnouncedSource.next(update);
    };
    DataFileEditorService.prototype.updateFile = function (update) {
        var input = { '@OP_API_RED_UPDATE_FILE$P_EDITOR_SESSION_ID': this.editorSessionId, '@OP_API_RED_UPDATE_FILE$P_FILE_UPDATE': update };
        return this.http.post(this.contextUrl + '@op_api_red_update_file', JSON.stringify(input), { withCredentials: true })
            .map(function (response) {
            var output = response.json();
            return output['@OP_API_RED_UPDATE_FILE$P_FILE_UPDATE'];
        });
    };
    DataFileEditorService.prototype.openFile = function (fileId) {
        var input = { '@OP_API_RED_OPEN_FILE$P_EDITOR_SESSION_ID': this.editorSessionId, '@OP_API_RED_OPEN_FILE$P_FILE_ID': fileId };
        return this.http.post(this.contextUrl + '@op_api_red_open_file', JSON.stringify(input), { withCredentials: true })
            .map(function (response) {
            var output = response.json();
            var record = new data_record_model_1.DataRecord();
            record.fromJson(output['@OP_API_RED_OPEN_FILE$P_FILE']);
            return record;
        });
    };
    DataFileEditorService.prototype.saveFile = function (fileId) {
        var input = { '@OP_API_RED_SAVE_FILE$P_EDITOR_SESSION_ID': this.editorSessionId, '@OP_API_RED_SAVE_FILE$P_FILE_ID': fileId };
        return this.http.post(this.contextUrl + '@op_api_red_save_file', JSON.stringify(input), { withCredentials: true })
            .map(function (response) {
            var output = response.json();
            var saveResult = new data_file_editor_model_1.FileSaveResult();
            console.log(output);
            return saveResult;
        });
    };
    DataFileEditorService.prototype.retrieveValueSuggestions = function (fileId, recordId, propertyId, fieldId, dataTypeId, searchPrefix, pageOffset, pageSize) {
        var input = {
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_EDITOR_SESSION_ID': this.editorSessionId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_FILE_ID': fileId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_RECORD_ID': recordId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_PROPERTY_ID': propertyId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_FIELD_ID': fieldId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_DATA_TYPE_ID': dataTypeId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_SEARCH_PREFIX': searchPrefix,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_PAGE_OFFSET': pageOffset,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_PAGE_SIZE': pageSize
        };
        return this.http.post(this.contextUrl + '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS', JSON.stringify(input), { withCredentials: true })
            .map(function (response) {
            var output = response.json();
            var valueList = new Array();
            for (var _i = 0, _a = output['@OP_API_RED_OPEN_FILE$P_FILE']; _i < _a.length; _i++) {
                var jsonValue = _a[_i];
                var recordValue = void 0;
                recordValue = data_record_model_1.RecordValue.createValueFromJson(jsonValue);
                valueList.push(recordValue);
            }
            console.log(output);
            return valueList;
        });
    };
    return DataFileEditorService;
}());
DataFileEditorService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], DataFileEditorService);
exports.DataFileEditorService = DataFileEditorService;
//# sourceMappingURL=data-file-editor.service.js.map