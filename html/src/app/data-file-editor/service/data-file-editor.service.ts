import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import 
{ 
    DataRecord, 
    RecordProperty, 
    RecordValue, 
    Field, 
    ValueStringSettings,
    BooleanValue 
} from '../../core/model/data-record.model';
import 
{ 
    DataFileSaveResult,DataFileUpdate, 
    DataFileValueUpdate
} from '../model/data-file-editor.model';
import 'rxjs/add/operator/map'
import { Context } from '../../core/model/security.model';
 
@Injectable()
export class DataFileEditorService 
{
    private editorSessionId: string; // Stores the id of the unique editor session.
    private valueStringSettings: ValueStringSettings;
    private ctx: Context;

    // Observable file update sources.
    private localUpdateAnnouncedSource = new Subject<DataFileUpdate>();
    private remoteUpdateAnnouncedSource = new Subject<DataFileUpdate>();
    private updateCycleEndAnnouncedSource = new Subject<DataFileUpdate>();
    
    // Observable file update streams.
    localUpdateAnnounced$ = this.localUpdateAnnouncedSource.asObservable();
    remoteUpdateAnnounced$ = this.remoteUpdateAnnouncedSource.asObservable();
    updateCycleEndAnnounced$ = this.updateCycleEndAnnouncedSource.asObservable();

    constructor(private http: HttpClient) 
    {
        console.log("Data File Editor Service Constructor executing...");
        this.editorSessionId = "TEST_DF_EDITOR_SESSION_ID";
        this.valueStringSettings = new ValueStringSettings();
        this.valueStringSettings.addExcludedType(BooleanValue.TYPE_ID);
    }

    public setContext(context: Context)
    {
        this.ctx = context;
    }

    private getContextParameters()
    {
        return new HttpParams().set('functionality_iid', this.ctx.functionalityIid);
    }

    /**
     * Update cycle for all records that form part of the data file editor:
     * 1. Local record is edited by user on editor component.
     * 2. Local editor component announces local update to service using {announceLocalUpdate}.
     * 3. Local Service observers are notified of local update via {localUpdateAnnounced$} observable.
     * 4. Local Service transmits local update to remote server.
     * 5. Remote server reponds with remote update to local service.
     * 6. Local service updates record model with updates from remote server.
     * 7. Local service announces remote update using {announceRemoteUpdate}.
     * 8. Service observers are notified of remote update via {remoteUpdateAnnounced$} observable.
     */
    announceLocalUpdate(sourceRecord: DataRecord, update: DataFileUpdate) 
    {
        // Announce the local update to all observers.
        console.log("Local update announced from source record: " + sourceRecord.recordId);
        console.log(update);
        this.localUpdateAnnouncedSource.next(update);

        // Send the local update to the remote server.
        this.updateFile(update)
            .subscribe
            (
                data => 
                {
                    // Process the remote update.
                    this.processRemoteUpdate(sourceRecord, data);

                    // Announce the remote update (if any) to local observers.
                    this.announceRemoteUpdate(data);
                },
                error => 
                {
                    console.log("Error while transmitting local update to server: " + error);
                }
            );
    }

    processRemoteUpdate(sourceRecord: DataRecord, update: DataFileUpdate)
    {
        console.log("Processing remote update...");
        console.log(update);
        if (update)
        {
            let file: DataRecord;

            // Get the currently open file.
            file = sourceRecord.getFile();

            // Distribute remote value updates to corresponding local variables.
            for (let valueUpdate of update.valueUpdates)
            {
                let targetRecord: DataRecord;

                targetRecord = file.getRecord(valueUpdate.recordId);
                if (targetRecord)
                {
                    let targetProperty: RecordProperty;

                    // Set the new value on the record.
                    targetProperty = targetRecord.getProperty(valueUpdate.propertyId);
                    targetProperty.value.setContent(valueUpdate.newValue);

                    // Refresh the record's value string.
                    targetRecord.refreshValueString(this.valueStringSettings);
                }
            }

            // Add any remotely created new records to the local model.
            for (let newRecordUpdate of update.newRecords)
            {
                let targetRecord: DataRecord;

                // Get the target record to which the new record has been added.
                targetRecord = file.getRecord(newRecordUpdate.recordId);
                if (targetRecord)
                {
                    // Add the new sub-record to its parent.
                    targetRecord.addSubRecord(newRecordUpdate.newRecord);

                    // Refresh the target record's value string.
                    targetRecord.refreshValueString(this.valueStringSettings);
                }
            }

            // Remove any remotely deleted records from the local model.
            for (let recordDeletion of update.recordDeletions)
            {
                let targetRecord: DataRecord;

                // Get the target record from which the record has been deleted.
                targetRecord = file.getRecord(recordDeletion.recordId);
                if (targetRecord)
                {
                    // Add the new sub-record to its parent.
                    targetRecord.removeSubRecord(recordDeletion.subRecordId);

                    // Refresh the target record's value string.
                    targetRecord.refreshValueString(this.valueStringSettings);
                }
            }

            // Process remote property access updates.
            for (let propertyUpdate of update.propertyAccessUpdates)
            {
                let targetRecord: DataRecord;

                // Get the target record to which the update is applicable.
                targetRecord = file.getRecord(propertyUpdate.recordId);
                if (targetRecord)
                {
                    // Apply the update to the target property access layer.
                    propertyUpdate.apply(targetRecord.access.getPropertyAccessLayer(propertyUpdate.propertyId));
                }
                else console.log("WARNING: Target record for remote property access update not found: " + propertyUpdate.required);
            }
        }
    }

    announceRemoteUpdate(update: DataFileUpdate) 
    {
        console.log("Remote update announced.");
        this.remoteUpdateAnnouncedSource.next(update);
        console.log("Update cycle end announced.");
        this.updateCycleEndAnnouncedSource.next(update);
    }

    updateFile(update: DataFileUpdate): Observable<DataFileUpdate>
    {
        const input = { '@OP_API_RED_UPDATE_FILE$P_EDITOR_SESSION_ID': this.editorSessionId, '@OP_API_RED_UPDATE_FILE$P_FILE_UPDATE': update };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@op_api_red_update_file', JSON.stringify(input), { withCredentials: true, params: this.getContextParameters() })
            .map((response: Response) => 
            {
                let outputUpdate: DataFileUpdate;

                outputUpdate = new DataFileUpdate(update.fileId);
                outputUpdate.fromJson(response['@OP_API_RED_UPDATE_FILE$P_FILE_UPDATE']);
                return outputUpdate;
            });
    }

    openFile(fileId: string): Observable<DataRecord>
    {
        const input = { '@OP_API_RED_OPEN_FILE$P_EDITOR_SESSION_ID': this.editorSessionId, '@OP_API_RED_OPEN_FILE$P_FILE_ID': fileId };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@op_api_red_open_file', JSON.stringify(input), { withCredentials: true, params: this.getContextParameters() })
            .map((response: Response) => 
            {
                let record = new DataRecord();
                
                record.fromJson(response['@OP_API_RED_OPEN_FILE$P_FILE']);
                return record;
            });
    }

    saveFile(fileId: string): Observable<DataFileSaveResult>
    {
        const input = { '@OP_API_RED_SAVE_FILE$P_EDITOR_SESSION_ID': this.editorSessionId, '@OP_API_RED_SAVE_FILE$P_FILE_ID': fileId };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@op_api_red_save_file', JSON.stringify(input), { withCredentials: true, params: this.getContextParameters() })
            .map((response: Response) => 
            {
                let saveResult = new DataFileSaveResult();
                
                console.log(response);
                saveResult.fromJson(response['@OP_API_RED_SAVE_FILE$P_SAVE_RESULT']);
                return saveResult;
            });
    }

    closeFile(fileId: string, saveChanges: boolean): Observable<DataFileSaveResult>
    {
        const input = { '@OP_API_RED_CLOSE_FILE$P_EDITOR_SESSION_ID': this.editorSessionId, '@OP_API_RED_CLOSE_FILE$P_FILE_ID': fileId, '@OP_API_RED_CLOSE_FILE$P_SAVE_CHANGES': saveChanges };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@op_api_red_close_file', JSON.stringify(input), { withCredentials: true, params: this.getContextParameters() })
            .map((response: Response) => 
            {
                let saveResult = new DataFileSaveResult();
                
                console.log(response);
                saveResult.fromJson(response['@OP_API_RED_CLOSE_FILE$P_SAVE_RESULT']);
                return saveResult;
            });
    }

    retrieveValueSuggestions(fileId: string, recordId: string, propertyId:string, fieldId:string, dataTypeId: string, searchPrefix: string, pageOffset: number, pageSize: number): Observable<Array<RecordValue>>
    {
        const input = 
        { 
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_EDITOR_SESSION_ID': this.editorSessionId, 
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_FILE_ID': fileId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_RECORD_ID': recordId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_PROPERTY_ID': propertyId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_FIELD_ID': fieldId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_DATA_TYPE_ID': dataTypeId,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_SEARCH_PREFIX': searchPrefix,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_PAGE_OFFSET': pageOffset,
            '@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_PAGE_SIZE': pageSize
        };

        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS', JSON.stringify(input), { withCredentials: true, params: this.getContextParameters() })
            .map((response: Response) => 
            {
                let valueList = new Array();
                
                for (let jsonValue of response['@OP_API_RED_RETRIEVE_VALUE_SUGGESTIONS$P_RECORD_VALUES'])
                {
                    let recordValue: RecordValue;

                    recordValue = RecordValue.createFromJson(jsonValue);
                    valueList.push(recordValue);
                }

                return valueList;
            });
    }

    retrieveValueOptions(fileId: string, recordId: string, propertyId:string, fieldId:string, dataTypeId: string, searchString: string, pageOffset: number, pageSize: number): Observable<Array<RecordValue>>
    {
        const input = 
        { 
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_EDITOR_SESSION_ID': this.editorSessionId, 
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_FILE_ID': fileId,
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_RECORD_ID': recordId,
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_PROPERTY_ID': propertyId,
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_FIELD_ID': fieldId,
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_DATA_TYPE_ID': dataTypeId,
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_SEARCH_STRING': searchString,
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_PAGE_OFFSET': pageOffset,
            '@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_PAGE_SIZE': pageSize
        };

        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_API_RED_RETRIEVE_VALUE_OPTIONS', JSON.stringify(input), { withCredentials: true, params: this.getContextParameters() })
            .map((response: Response) => 
            {
                let valueList = new Array();
                
                for (let jsonValue of response['@OP_API_RED_RETRIEVE_VALUE_OPTIONS$P_RECORD_VALUES'])
                {
                    let recordValue: RecordValue;

                    recordValue = RecordValue.createFromJson(jsonValue);
                    valueList.push(recordValue);
                }

                return valueList;
            });
    }
}