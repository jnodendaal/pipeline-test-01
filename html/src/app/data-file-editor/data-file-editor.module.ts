import { NgModule }                             from '@angular/core';
import { CommonModule }                         from '@angular/common';
import { FormsModule }                          from '@angular/forms';
import { DatepickerModule  }                    from 'ngx-bootstrap';
import { SharedModule }                         from '../core/shared.module';

import { DataFileEditorComponent }              from './component/data-file-editor.component';
import { DefaultLayoutViewComponent }           from './component/default-layout/default-layout-view.component';
import { PagedViewComponent }                   from './component/paged-layout/paged-view.component';
import { NotificationViewComponent }            from './component/notification-view.component';
import { PageNavigationViewComponent }          from './component/paged-layout/page-navigation-view.component';
import { PageNavigationNodeComponent }          from './component/paged-layout/page-navigation-node.component';
import { PageViewComponent }                    from './component/paged-layout/page-view.component';
import { DocumentViewComponent }                from './component/document-view.component';
import { RecordEditorComponent }                from './component/record-editor.component';
import { SectionEditorComponent }               from './component/section-editor.component';
import { PropertyEditorComponent }              from './component/property-editor.component';
import { BooleanEditorComponent }               from './component/value-editors/boolean-editor.component';
import { StringEditorComponent }                from './component/value-editors/string-editor.component';
import { NumberEditorComponent }                from './component/value-editors/number-editor.component';
import { TextEditorComponent }                  from './component/value-editors/text-editor.component';
import { TimeEditorComponent }                  from './component/value-editors/time-editor.component';
import { DateEditorComponent }                  from './component/value-editors/date-editor.component';
import { DateTimeEditorComponent }              from './component/value-editors/date-time-editor.component';
import { AttachmentListEditorComponent }        from './component/value-editors/attachment-list-editor.component';
import { ControlledConceptEditorComponent }     from './component/value-editors/controlled-concept-editor.component';
import { DocumentReferenceEditorComponent }     from './component/value-editors/document-reference-editor.component';
import { MeasuredNumberEditorComponent }        from './component/value-editors/measured-number-editor.component';
import { MeasuredRangeEditorComponent }         from './component/value-editors/measured-range-editor.component';
import { CompositeEditorComponent }             from './component/value-editors/composite-editor.component';
import { FieldEditorComponent }                 from './component/value-editors/field-editor.component';
import { ValueLookupComponent }                 from './component/value-lookup/value-lookup.component';
import { DataFileEditorService }                from './service/data-file-editor.service';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    DatepickerModule,
    SharedModule
  ],
  declarations: 
  [ 
    DataFileEditorComponent,
    DefaultLayoutViewComponent,
    PagedViewComponent,
    RecordEditorComponent, 
    PageNavigationViewComponent, 
    PageNavigationNodeComponent, 
    PageViewComponent,
    NotificationViewComponent, 
    DocumentViewComponent, 
    SectionEditorComponent, 
    PropertyEditorComponent, 
    StringEditorComponent, 
    NumberEditorComponent,
    TextEditorComponent, 
    TimeEditorComponent,
    DateEditorComponent,
    DateTimeEditorComponent,
    BooleanEditorComponent,
    AttachmentListEditorComponent,
    ControlledConceptEditorComponent,
    DocumentReferenceEditorComponent,
    MeasuredNumberEditorComponent,
    MeasuredRangeEditorComponent,
    ValueLookupComponent,
    FieldEditorComponent,
    CompositeEditorComponent
  ],
  exports:      [ DataFileEditorComponent ],
  providers:    [ DataFileEditorService ]
})
export class DataFileEditorModule 
{
}