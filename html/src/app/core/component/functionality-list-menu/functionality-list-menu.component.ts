import { Component, ViewChild, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SecurityManagerService } from '../../service/security-manager.service';
import { Context } from '../../model/security.model';
import { FunctionalityHandle, FunctionalityGroupHandle, FunctionalityAccessHandle } from '../../model/functionality.model';
import { FunctionalityManagerService } from '../../service/functionality-manager.service';
import { Subscription } from 'rxjs/Subscription';

@Component
({
    selector: 'functionality-list-menu',
    templateUrl: 'functionality-list-menu.component.html',
    styleUrls: [ 'functionality-list-menu.component.css']
})
export class FunctionalityListMenuComponent implements OnDestroy
{   
    private router: Router;
    private ctx: Context;
    private objectId: string;
    private objectIid: string;
    private functionalityManager: FunctionalityManagerService;
    private sessionSubscription: Subscription;
    private groupId: string;
    private groupHandle: FunctionalityGroupHandle;
    private loading: boolean;

    constructor(router: Router, securityManager: SecurityManagerService, functionalityManagerService: FunctionalityManagerService)
    {
        this.router = router;
        this.functionalityManager = functionalityManagerService;
        this.loading = false;
        this.sessionSubscription = securityManager.sessionUpdated$.subscribe
        (
            session => 
            {
                console.log("Session update observed from functionality list menu: " + this.groupId);
                this.refresh();
            }
        );            
    }

    ngOnDestroy()
    {
        this.sessionSubscription.unsubscribe();
    }
    
    @Input()
    set context(context: Context)
    {
        this.ctx = context;
    }

    @Input()
    set targetObjectId(objectId: string)
    {
        this.objectId = objectId;
    }

    @Input()
    set targetObjectIid(objectIid: string)
    {
        this.objectIid = objectIid;
    }

    @Input()
    set functionalityGroupId(functionalityGroupId: string)
    {
        console.log("List menu functionality group: " + functionalityGroupId);
        this.groupId = functionalityGroupId;
        this.refresh();
    }

    public refresh()
    {
        console.log("Refreshing group: " + this.groupId + ", objectId: " + this.objectId + ", objectIid: " + this.objectIid);
        this.loading = true;
        this.functionalityManager.getFunctionalityGroupHandle(this.ctx, this.groupId, this.objectId, this.objectIid)
            .subscribe
            (
                data => 
                {
                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    this.loading = false;
                    setTimeout(() => this.setFunctionalityGroupHandle(data)); 
                },
                error => 
                {
                    console.log("Error while retrieving functionality group handle " + this.groupId + ": " + error);
                    this.loading = false;
                }
            );
    }

    public setFunctionalityGroupHandle(functionalityGroupHandle: FunctionalityGroupHandle)
    {
        this.groupHandle = functionalityGroupHandle;
    }

    private accessFunctionality(functionalityHandle: FunctionalityHandle)
    {
        this.loading = true;
        this.functionalityManager.accessFunctionality(functionalityHandle.id, functionalityHandle.getPublicParameters())
            .subscribe
            (
                data => 
                {
                    let accessHandle: FunctionalityAccessHandle;

                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    accessHandle = data as FunctionalityAccessHandle;
                    if (accessHandle.accessType == "ACCESS")
                    {
                        console.log("Navigating to url: " + accessHandle.componentUri);
                        setTimeout(() => this.router.navigate([accessHandle.componentUri, { 'functionality_iid':accessHandle.iid }]));
                    }
                    else
                    {
                        console.log("Functionality Access denied: " + accessHandle.accessType + " - " + accessHandle.accessMessage);    
                        this.loading = false;
                    }
                },
                error => 
                {
                    console.log("Error while accessing functionality " + functionalityHandle.id + ": " + error);
                    this.loading = false;
                }
            );
    }

    onFunctionalitySelected(functionalityHandle: FunctionalityHandle)
    {
        console.log("Functionality Selected: " + functionalityHandle.id);
        this.accessFunctionality(functionalityHandle);
    }
}
