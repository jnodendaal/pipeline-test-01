import { Component, ViewChild, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { FunctionalityManagerService } from '../../service/functionality-manager.service';
import { FunctionalityAccessHandle } from '../../model/functionality.model';
import { ComponentManagerService } from '../../../core/service/component-manager.service';
import { Task, TaskSearchResults } from '../../model/workflow.model';
import { WorkflowManagerService } from '../../service/workflow-manager.service';
import { UserManagerService } from '../../service/user-manager.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Context } from '../../../core/model/security.model';
import { CoreComponent } from '../../../core/component/core.component';
import { ComponentState } from '../../../core/model/component.model';
import { ApplicationSettings } from '../../../app.config';

@Component
({
    selector: 'task-list',
    templateUrl: 'task-list.component.html',
    styleUrls: ['task-list.component.css'],
    providers: [ ComponentManagerService ]
})
export class TaskListComponent extends CoreComponent<TaskListState> implements OnInit, OnDestroy
{   
    private userManager: UserManagerService;
    private flowManager: WorkflowManagerService;
    private dataCount: number;
    private loading: boolean;
    private searchPattern: string;
    private searchExpression: string;
    private searchResults: TaskSearchResults;
    private pageSize: number;
    private pageOffset: number; 
    private pageNumber: number;
    private itemMenuVisible: boolean;
    private selectedTask: Task;
    
    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, flowManager: WorkflowManagerService, userManager: UserManagerService)
    {
        super(router, route, functionalityManager, componentManager);
        this.loading = false;
        this.flowManager = flowManager;
        this.userManager = userManager;
        this.dataCount = 0;
        this.pageSize = 10;
        this.pageOffset = 0;
        this.pageNumber = 1;
        this.loading = false;
        this.selectedTask = null;
        this.searchResults = new TaskSearchResults();
    }

    public ngOnInit()
    {
        this.newSearch();
    }

    public ngOnDestroy()
    {
        this.releaseFunctionality();
    }

    private getThumbnailUrl(task: Task)
    {
        if (task.userThumbnailUrl)
        {
            return task.userThumbnailUrl
        }
        else return ApplicationSettings.NO_IMAGE_URL;
    }

    private onTaskSelected(task: Task)
    {
        console.log("Task selected:" + task.taskIid);
        this.selectedTask = task;
        this.loading = true;

        // If the task has not been claimed, do it now.
        if (!task.claimed)
        {
            this.flowManager.claimTask(this.ctx, task.taskIid)
            .subscribe
            (
                data =>
                {
                    // Start the task functionality.
                    this.accessTaskFunctionality(task.functionalityId, task.taskIid);
                },
                error =>
                {
                    this.loading = false;
                    console.log("Error while caliming task: " + task.taskIid);
                }
            )
        }

        // Start the task functionality.
        this.accessTaskFunctionality(task.functionalityId, task.taskIid);
    }

    private newSearch()
    {
        this.pageOffset = 0;
        this.pageNumber = 1;
        this.search();
    }

    private search()
    {
        this.loading = true;
        this.flowManager.searchTasks(this.ctx, this.searchExpression, this.pageOffset, this.pageSize)
        .subscribe
            (
                data => 
                {
                    this.loading = false;
                    this.searchResults = data;
                    this.searchPattern = "";

                    for (let task of this.searchResults.tasks)
                    {
                        if (task.fromUserId)
                        {
                            task.userThumbnailUrl = this.userManager.getUserProfileThumbnailUrl(task.fromUserId);
                        }
                        else
                        {
                            task.userThumbnailUrl = this.userManager.getUserProfileThumbnailUrl(task.initiatorId);
                        }
                    }
                    
                    for (let searchTerm of data.searchTerms)
                    {
                        if (this.searchPattern.length > 0) this.searchPattern += "|";
                        this.searchPattern += searchTerm;
                    }
                },
                error => 
                {
                    console.log("Error while searching material catalogue: " + error);
                    this.loading = false;
                }
            );
    }

    private nextPage()
    {
        this.pageOffset += this.pageSize;
        this.pageNumber = this.pageOffset / this.pageSize + 1;
        this.search();
    }

    private previousPage()
    {
        if (this.pageOffset > 0)
        {
            this.pageOffset -= this.pageSize;
            this.pageNumber = this.pageOffset / this.pageSize + 1;
            this.search();
        }
    }

    private accessTaskFunctionality(functionalityId: string, taskIid: string)
    {
        this.loading = true;
        this.functionalityManager.accessFunctionality(functionalityId, {[functionalityId + "$P_TASK_IID"]: taskIid})
            .subscribe
            (
                data => 
                {
                    let accessHandle: FunctionalityAccessHandle;

                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    accessHandle = data as FunctionalityAccessHandle;
                    if (accessHandle.accessType == "ACCESS")
                    {
                        let navigationExtras: NavigationExtras;
                        
                        // Set the options for the navigation (include functionality_iid as query parameter).
                        navigationExtras = 
                        {
                            queryParams: 
                            {
                                'functionality_iid':accessHandle.iid
                            },
                            queryParamsHandling:'merge'
                        }
                        
                        console.log("Navigating to url: " + accessHandle.componentUri);
                        setTimeout(() => this.router.navigate([accessHandle.componentUri], navigationExtras));
                    }
                    else
                    {
                        console.log("Functionality Access denied: " + accessHandle.accessType + " - " + accessHandle.accessMessage);    
                        this.loading = false;
                    }
                },
                error => 
                {
                    console.log("Error while accessing task functionality " + functionalityId + ": " + error);
                    this.loading = false;
                }
            );
    }

    protected createState(): TaskListState
    {
        this.state = new TaskListState();
        return this.state;
    }

    protected getState(): TaskListState
    {
        return this.state;
    }

    protected setState(state: TaskListState)
    {
        this.state = state;
    }
}

export class TaskListState extends ComponentState
{
}