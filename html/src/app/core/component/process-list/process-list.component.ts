import { Component, ViewChild, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FunctionalityManagerService } from '../../service/functionality-manager.service';
import { FunctionalityAccessHandle } from '../../model/functionality.model';
import { ComponentManagerService } from '../../../core/service/component-manager.service';
import { Process, ProcessSearchResults } from '../../model/process.model';
import { ProcessManagerService } from '../../service/process-manager.service';
import { UserManagerService } from '../../service/user-manager.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { saveAs } from "file-saver";
import { Context } from '../../../core/model/security.model';
import { CoreComponent } from '../../../core/component/core.component';
import { ComponentState } from '../../../core/model/component.model';
import { ApplicationSettings } from '../../../app.config';

@Component
({
    selector: 'process-list',
    templateUrl: 'process-list.component.html',
    styleUrls: ['process-list.component.css'],
    providers: [ ComponentManagerService ]
})
export class ProcessListComponent extends CoreComponent<ProcessListState> implements OnInit, OnDestroy
{   
    private processManager: ProcessManagerService;
    private userManager: UserManagerService;
    private dataCount: number;
    private loading: boolean;
    private searchPattern: string;
    private searchExpression: string;
    private searchResults: ProcessSearchResults;
    private pageSize: number;
    private pageOffset: number;
    private pageNumber: number;
    private selectedProcess: Process;
    private deletionConfirmationVisible: boolean;
    private processToBeDeleted: Process;
    private imageUrl: string;

    @ViewChild('deletionConfirmationModal') 
    private deletionConfirmationModal: ModalDirective;
    
    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, processManager: ProcessManagerService, userManager: UserManagerService)
    {
        super(router, route, functionalityManager, componentManager);
        this.processManager = processManager;
        this.userManager = userManager;
        this.dataCount = 0;
        this.pageSize = 10;
        this.pageOffset = 0;
        this.pageNumber = 1;
        this.loading = false;
        this.selectedProcess = null;
        this.searchResults = new ProcessSearchResults();
        this.deletionConfirmationVisible = false;
        this.imageUrl = ApplicationSettings.API_IMAGE_URL + "/";
    }

    public ngOnInit()
    {
        this.newSearch();
    }

    public ngOnDestroy()
    {
        this.releaseFunctionality();
    }

    private getThumbnailUrl(process: Process)
    {
        if (process.thumbnailImageId)
        {
            return this.imageUrl + encodeURIComponent(process.thumbnailImageId);
        }
        else return ApplicationSettings.NO_IMAGE_URL;
    }

    private onProcessSelected(process: Process)
    {
        console.log("Process selected:" + process.iid);
        this.selectedProcess = process;
    }

    private deleteProcess(process: Process)
    {
        console.log("Process to be deleted:" + process.iid);
        this.processManager.finalizeProcess(this.ctx, process.iid)
            .subscribe
            (
                data=>
                {
                    this.search();
                }
            )
    }

    private showDeletionConfirmation(process: Process)
    {
        this.processToBeDeleted = process;
        this.deletionConfirmationVisible = true;
    }

    private hideDeletionConfirmation()
    {
        this.processToBeDeleted = null;
        this.deletionConfirmationModal.hide();
    }

    private onDeletionConfirmationHidden()
    {
        this.deletionConfirmationVisible = false;
    }

    private confirmDeletion()
    {
        this.deleteProcess(this.processToBeDeleted);
        this.processToBeDeleted = null;
        this.hideDeletionConfirmation();
    }

    private newSearch()
    {
        this.pageOffset = 0;
        this.pageNumber = 1;
        this.search();
    }

    private search()
    {
        this.loading = true;
        this.processManager.searchProcesses(this.ctx, this.searchExpression, this.pageOffset, this.pageSize)
        .subscribe
            (
                data => 
                {
                    this.loading = false;
                    this.searchResults = data;
                    this.searchPattern = "";
                    
                    for (let searchTerm of data.searchTerms)
                    {
                        if (this.searchPattern.length > 0) this.searchPattern += "|";
                        this.searchPattern += searchTerm;
                    }
                },
                error => 
                {
                    console.log("Error while searching processes: " + error);
                    this.loading = false;
                }
            );
    }

    private nextPage()
    {
        this.pageOffset += this.pageSize;
        this.pageNumber = this.pageOffset / this.pageSize + 1;
        this.search();
    }

    private previousPage()
    {
        if (this.pageOffset > 0)
        {
            this.pageOffset -= this.pageSize;
            this.pageNumber = this.pageOffset / this.pageSize + 1;
            this.search();
        }
    }

    protected createState(): ProcessListState
    {
        this.state = new ProcessListState();
        return this.state;
    }

    protected getState(): ProcessListState
    {
        return this.state;
    }

    protected setState(state: ProcessListState)
    {
        this.state = state;
    }
}

export class ProcessListState extends ComponentState
{
}