import { Component, ViewChild, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { SecurityManagerService } from '../../service/security-manager.service';
import { Context } from '../../model/security.model';
import { FunctionalityHandle, FunctionalityGroupHandle, FunctionalityAccessHandle } from '../../model/functionality.model';
import { FunctionalityManagerService } from '../../service/functionality-manager.service';
import { Subscription } from 'rxjs/Subscription';

@Component
({
    selector: 'functionality-card-menu',
    templateUrl: 'functionality-card-menu.component.html',
    styleUrls: [ 'functionality-card-menu.component.css']
})
export class FunctionalityCardMenuComponent implements OnDestroy
{   
    private router: Router;
    private accessHandle: FunctionalityAccessHandle;
    private parameters: any;
    private activatedRoute: ActivatedRoute;
    private functionalityManager: FunctionalityManagerService;
    private sessionSubscription: Subscription;
    private groupHandle: FunctionalityGroupHandle;
    private functionalityId: string;
    private loading: boolean;

    constructor(router: Router, activatedRoute:ActivatedRoute, securityManager: SecurityManagerService, functionalityManagerService: FunctionalityManagerService)
    {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.functionalityManager = functionalityManagerService;
        this.loading = false;
        this.parameters = new Object(); // Initialize the object to prevent undefined references.
        this.activatedRoute.params
            .map(params => params['functionality_id'])
            .subscribe((functionalityId) => 
            {
                this.functionalityId = functionalityId;
                this.initializeComponent();
            })
        this.sessionSubscription = securityManager.sessionUpdated$.subscribe
        (
            session => 
            {
                console.log("Session update observed from functionality card menu: " + this.functionalityId);
                this.refresh();
            }
        );            
    }

    ngOnDestroy()
    {
        this.functionalityManager.releaseFunctionality(this.accessHandle.iid).subscribe();
        this.sessionSubscription.unsubscribe();
    }

    public initializeComponent()
    {
        console.log("Accessing functionality: " + this.functionalityId);
        this.loading = true;
        this.functionalityManager.accessFunctionality(this.functionalityId, null)
            .subscribe
            (
                data => 
                {
                    this.accessHandle = data;
                    this.parameters = data.componentParameters;
                    console.log(this.parameters);
                    this.refresh();
                },
                error => 
                {
                    console.log("Error while accessing functionality " + this.functionalityId + ": " + error);
                    this.loading = false;
                }
            );
    }

    public refresh()
    {
        let groupId: any;

        groupId = this.parameters.groupId;
        console.log("Refreshing group: " + groupId);

        this.loading = true;
        this.functionalityManager.getFunctionalityGroupHandle(this.accessHandle.getContext(), groupId, null, null)
            .subscribe
            (
                data => 
                {
                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    this.loading = false;
                    setTimeout(() => this.setFunctionalityGroupHandle(data)); 
                },
                error => 
                {
                    console.log("Error while retrieving functionality group handle " + groupId + ": " + error);
                    this.loading = false;
                }
            );
    }

    public setFunctionalityGroupHandle(functionalityGroupHandle: FunctionalityGroupHandle)
    {
        this.groupHandle = functionalityGroupHandle;
    }

    private accessFunctionality(functionalityId: string)
    {
        this.loading = true;
        this.functionalityManager.accessFunctionality(functionalityId, null)
            .subscribe
            (
                data => 
                {
                    let accessHandle: FunctionalityAccessHandle;

                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    accessHandle = data as FunctionalityAccessHandle;
                    if (accessHandle.accessType == "ACCESS")
                    {
                        let navigationExtras: NavigationExtras;
                        
                        // Set the options for the navigation (include functionality_iid as query parameter).
                        navigationExtras = 
                        {
                            queryParams: 
                            {
                                'functionality_iid':accessHandle.iid
                            },
                            queryParamsHandling:'merge'
                        }
                        
                        console.log("Navigating to url: " + accessHandle.componentUri);
                        setTimeout(() => this.router.navigate([accessHandle.componentUri], navigationExtras));
                    }
                    else
                    {
                        console.log("Functionality Access denied: " + accessHandle.accessType + " - " + accessHandle.accessMessage);    
                        this.loading = false;
                    }
                },
                error => 
                {
                    console.log("Error while accessing functionality " + functionalityId + ": " + error);
                    this.loading = false;
                }
            );
    }

    onFunctionalitySelected(id: string)
    {
        console.log("Functionality Selected: " + id);
        this.accessFunctionality(id);
    }
}
