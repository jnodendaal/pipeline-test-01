"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var functionality_model_1 = require("../../models/functionality.model");
var FunctionalityCardMenuComponent = (function () {
    function FunctionalityCardMenuComponent(router, activatedRoute) {
        var functionality;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.functionalities = new Array();
        this.loading = false;
        functionality = new functionality_model_1.FunctionalityHandle();
        functionality.id = "@FUNC_MMM_DATA_QUALITY_REPORT";
        functionality.name = "Material Data Quality Report";
        functionality.url = "/main/report/material_data_quality_report";
        functionality.iconClass = "glyphicon glyphicon-cog";
        this.functionalities.push(functionality);
        functionality = new functionality_model_1.FunctionalityHandle();
        functionality.id = "@FUNC_SMM_DATA_QUALITY_REPORT";
        functionality.name = "Service Data Quality Report";
        functionality.url = "/main/report/service_data_quality_report";
        functionality.iconClass = "glyphicon glyphicon-wrench";
        this.functionalities.push(functionality);
        functionality = new functionality_model_1.FunctionalityHandle();
        functionality.id = "@FUNC_VMM_DATA_QUALITY_REPORT";
        functionality.name = "Vendor Data Quality Report";
        functionality.url = "/main/report/vendor_data_quality_report";
        functionality.iconClass = "glyphicon glyphicon-shopping-cart";
        this.functionalities.push(functionality);
    }
    FunctionalityCardMenuComponent.prototype.onFunctionalitySelected = function (id) {
        var _this = this;
        console.log("Functionality Selected: " + id);
        var _loop_1 = function (functionality) {
            if (functionality.id == id) {
                //this.router.navigate(['../report/', taskIid], { relativeTo: this.activatedRoute });
                this_1.loading = true;
                setTimeout(function () { return _this.router.navigateByUrl(functionality.url); });
                return { value: void 0 };
            }
        };
        var this_1 = this;
        for (var _i = 0, _a = this.functionalities; _i < _a.length; _i++) {
            var functionality = _a[_i];
            var state_1 = _loop_1(functionality);
            if (typeof state_1 === "object")
                return state_1.value;
        }
    };
    return FunctionalityCardMenuComponent;
}());
FunctionalityCardMenuComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'functionality-card-menu',
        templateUrl: 'functionality-card-menu.component.html',
        styleUrls: ['functionality-card-menu.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router, router_1.ActivatedRoute])
], FunctionalityCardMenuComponent);
exports.FunctionalityCardMenuComponent = FunctionalityCardMenuComponent;
//# sourceMappingURL=functionality-card-menu.component.js.map