import { Component, ViewChild, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { SecurityManagerService } from '../../service/security-manager.service';
import { Context } from '../../model/security.model';
import { FunctionalityHandle, FunctionalityGroupHandle, FunctionalityAccessHandle } from '../../model/functionality.model';
import { FunctionalityManagerService } from '../../service/functionality-manager.service';
import { Subscription } from 'rxjs/Subscription';

@Component
({
    selector: 'functionality-access',
    templateUrl: 'functionality-access.component.html',
    styleUrls: [ 'functionality-access.component.css']
})
export class FunctionalityAccessComponent
{   
    private router: Router;
    private accessHandle: FunctionalityAccessHandle;
    private parameters: any;
    private activatedRoute: ActivatedRoute;
    private functionalityManager: FunctionalityManagerService;
    private groupHandle: FunctionalityGroupHandle;
    private functionalityId: string;
    private loading: boolean;

    constructor(router: Router, activatedRoute:ActivatedRoute, securityManager: SecurityManagerService, functionalityManagerService: FunctionalityManagerService)
    {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.functionalityManager = functionalityManagerService;
        this.loading = false;
        this.parameters = new Object(); // Initialize the object to prevent undefined references.
        this.activatedRoute.params
            .map(params => params['functionality_id'])
            .subscribe((functionalityId) => 
            {
                this.functionalityId = functionalityId;
                this.accessFunctionality(functionalityId);
            })
    }

    private accessFunctionality(functionalityId: string)
    {
        this.loading = true;
        this.functionalityManager.accessFunctionality(functionalityId, null)
            .subscribe
            (
                data => 
                {
                    let accessHandle: FunctionalityAccessHandle;

                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    accessHandle = data as FunctionalityAccessHandle;
                    if (accessHandle.accessType == "ACCESS")
                    {
                        let navigationExtras: NavigationExtras;
                        
                        // Set the options for the navigation (include functionality_iid as query parameter).
                        navigationExtras = 
                        {
                            queryParams: 
                            {
                                'functionality_iid':accessHandle.iid
                            },
                            queryParamsHandling:'merge'
                        }
                        
                        console.log("Navigating to url: " + accessHandle.componentUri);
                        setTimeout(() => this.router.navigate([accessHandle.componentUri], navigationExtras));
                    }
                    else
                    {
                        console.log("Functionality Access denied: " + accessHandle.accessType + " - " + accessHandle.accessMessage);    
                        this.loading = false;
                    }
                },
                error => 
                {
                    console.log("Error while accessing functionality " + functionalityId + ": " + error);
                    this.loading = false;
                }
            );
    }
}
