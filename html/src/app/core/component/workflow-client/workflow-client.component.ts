import { Component, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';
import { FunctionalityManagerService } from '../../service/functionality-manager.service';
import { WorkflowManagerService } from '../../service/workflow-manager.service';
import { FunctionalityAccessHandle } from '../../model/functionality.model';
import { DataFilter } from '../../model/data-filter.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FlowStatus, Task } from '../../model/workflow.model';

@Component
({
    selector: 'workflow-client',
    templateUrl: 'workflow-client.component.html',
    styleUrls: ['workflow-client.component.css'],
    providers: [ ]
})
export class WorkflowClientComponent implements OnDestroy
{   
    private functionalityManager: FunctionalityManagerService;
    private accessHandle: FunctionalityAccessHandle;
    private refreshSubscription: Subscription;
    private router: Router;
    private parameters: any;
    private workflowManager: WorkflowManagerService;
    private loading: boolean;
    private status: FlowStatus;
    private flowIid: string;

    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, workflowManager: WorkflowManagerService)
    {
        console.log("WorkflowClientComponent");
        this.router = router;
        this.functionalityManager = functionalityManager;
        this.workflowManager = workflowManager;
        this.parameters = {};
        this.loading = false;

        // Process the activated route to retrieve the functionality iid of this component..
        route.params.subscribe(params => 
        {
            this.initializeComponent(params['functionality_iid']);
        });

        // Create the periodic refresh observable.
        this.refreshSubscription = Observable
            .interval(500)
            .subscribe(() => 
            {
                this.refreshStatus();
            });
    }

    private initializeComponent(functionalityIid: string)
    {
        // Get the access handle from the functionality manager using the iid passed to this component via the activated route.
        this.accessHandle = this.functionalityManager.getFunctionalityAccessHandle(functionalityIid);
        if (this.accessHandle)
        {
            // Get the component parameters from the access handle.
            this.parameters = this.accessHandle.componentParameters;
            this.flowIid = this.parameters.flowIid;
        }
        else throw ("Functionality access handle not found: " + functionalityIid);
    }

    public ngOnDestroy()
    {
        // No need to explicitly release this functionality, as the workflow will only be released after completion.
        // Make sure to unsibscribe so that the refresh will no longer occur.
        this.refreshSubscription.unsubscribe();
    }

    /**
     * Every time this method is executed it will fetch the latest status of the current flow from the server and update the ui.
     * If any tasks have become available for the current user, the first of the available tasks will immediately be accessed.
     */
    private refreshStatus()
    {
        console.log("Fetching flow status: " + this.flowIid);
        this.loading = true;
        this.workflowManager.getFlowStatus(this.flowIid)
        .subscribe
            (
                data => 
                {
                    this.loading = false;
                    this.status = data;
                    if (this.status.hasAvailableTasks())
                    {
                        // Access the first available task on the flow.
                        this.accessTask(this.status.getFirstAvailableTask());
                    }
                },
                error => 
                {
                    console.log("Error while fetching status for flow " + this.flowIid + ": " + error);
                    this.loading = false;
                }
            );
    }

    private accessTask(task: Task)
    {
        this.loading = true;
        this.functionalityManager.accessFunctionality(task.functionalityId, {[task.functionalityId + "$P_TASK_IID"]:task.taskIid})
            .subscribe
            (
                data => 
                {
                    let accessHandle: FunctionalityAccessHandle;

                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    accessHandle = data as FunctionalityAccessHandle;
                    if (accessHandle.accessType == "ACCESS")
                    {
                        console.log("Navigating to url: " + accessHandle.componentUri);
                        //setTimeout(() => this.router.navigate([accessHandle.componentUri], { queryParams: { functionality_iid: accessHandle.iid }, queryParamsHandling: "merge" }));
                        setTimeout(() => this.router.navigate([accessHandle.componentUri, { 'functionality_iid':accessHandle.iid }]));
                    }
                    else
                    {
                        console.log("Functionality Access denied: " + accessHandle.accessType + " - " + accessHandle.accessMessage);    
                        this.loading = false;
                    }
                },
                error => 
                {
                    console.log("Error while accessing functionality " + task.functionalityId + ": " + error);
                    this.loading = false;
                }
            );
    }
}
