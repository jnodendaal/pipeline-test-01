import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, RoutesRecognized, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { CoreComponent } from '../core.component';
import { ComponentManagerService } from '../../service/component-manager.service';

@Component
({
    selector: 'app-breadcrumb',
    templateUrl: 'breadcrumb.component.html',
    styleUrls: ['breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit, OnDestroy
{
    private componentManager: ComponentManagerService;
    private routeSubscription : Subscription;
    private breadcrumbs: Array<Breadcrumb>;
    private label: string;
    private queryParams: {[key: string]: any} = {};

    // Build your breadcrumb starting with the root route of your current activated route
    constructor(private router: Router, private activatedRoute: ActivatedRoute, componentManager: ComponentManagerService) 
    {
        this.componentManager = componentManager;
        this.label = "Navigation";
        // Subscribe to router events.
        this.routeSubscription = this.router.events
            .filter(event => event instanceof NavigationEnd)
            .distinctUntilChanged()
            .map(event => this.createNewBreadcrumbs(this.activatedRoute.snapshot)).subscribe
            (
                data => 
                {
                    // Udpate the local breadcrumbs collection.
                    this.setBreadcrumbs(data);
                }
            );
    }

    @Input() 
    set navigationLabel(label: string) 
    {
        this.label = label;
    }

    ngOnInit()
    {
        // Build the initial breadcrumbs for the start route.
        this.breadcrumbs = this.createNewBreadcrumbs(this.activatedRoute.snapshot);
    }

    ngOnDestroy()
    {
        this.routeSubscription.unsubscribe();
    }

    private setBreadcrumbs(newCrumbs: Array<Breadcrumb>)
    {
        this.breadcrumbs = newCrumbs;
    }

    private createNewBreadcrumbs(route: ActivatedRouteSnapshot, breadcrumbs: Array<Breadcrumb> = []): Array<Breadcrumb> 
    {
        let newBreadcrumbs: Array<Breadcrumb>;

        if ((route.routeConfig.data) && (route.routeConfig.data.breadcrumb))
        {
            let breadcrumb: Breadcrumb;
            let label: string;
            
            // Get the label for the breadcrumb.
            label = route.routeConfig.data['breadcrumb'];

            // Construct the new breadcrumb object.
            breadcrumb = 
            {
                label: label,
                path: route.routeConfig.path,
                url: route.url.join(''),
                params: route.params,
                queryParams: route.queryParams
            };

            // If this is the last route segement in the url, save the query params as they are now.  If not, get params as they have previously been saved.
            if (route.firstChild)
            {
                breadcrumb.queryParams = this.queryParams[breadcrumb.url];
            }
            else
            {
                this.queryParams[breadcrumb.url] = breadcrumb.queryParams;
            }

            // Add the new breadcrumb to the last index in the breadcrumbs list.
            newBreadcrumbs = [...breadcrumbs, breadcrumb];
        }
        else newBreadcrumbs = breadcrumbs;

        // If more sub-routes exist, create breadcrumbs for them.
        if (route.firstChild) 
        {
            // If we are not on our current path yet,
            // there will be more children to look after, to build our breadcumb
            return this.createNewBreadcrumbs(route.firstChild, newBreadcrumbs);
        }
        else
        {
            // No more sub-routes to evaluate, so return the final list of breadcrumbs.
            return newBreadcrumbs;
        }
    }
}

export interface Breadcrumb 
{
    label: string;
    path: string;
    url: string;
    params: any;
    queryParams: any;
};