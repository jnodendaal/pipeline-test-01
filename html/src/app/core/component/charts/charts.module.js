"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var ng2_charts_1 = require("ng2-charts");
var angular2_contextmenu_1 = require("angular2-contextmenu");
var shared_module_1 = require("../../shared.module");
var doughnut_chart_component_1 = require("./doughnut-chart.component");
var polar_area_chart_component_1 = require("./polar-area-chart.component");
var bar_chart_component_1 = require("./bar-chart.component");
var context_menu_example_1 = require("./context-menu-example");
var T8ChartsModule = (function () {
    function T8ChartsModule() {
    }
    return T8ChartsModule;
}());
T8ChartsModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            router_1.RouterModule,
            ngx_bootstrap_1.Ng2BootstrapModule,
            shared_module_1.SharedModule,
            ng2_charts_1.ChartsModule,
            angular2_contextmenu_1.ContextMenuModule
        ],
        declarations: [
            doughnut_chart_component_1.DoughnutChartComponent,
            polar_area_chart_component_1.PolarAreaChartComponent,
            bar_chart_component_1.BarChartComponent,
            context_menu_example_1.ContextMenuExampleComponent
        ],
        exports: [doughnut_chart_component_1.DoughnutChartComponent, polar_area_chart_component_1.PolarAreaChartComponent, bar_chart_component_1.BarChartComponent],
        providers: []
    })
], T8ChartsModule);
exports.T8ChartsModule = T8ChartsModule;
//# sourceMappingURL=charts.module.js.map