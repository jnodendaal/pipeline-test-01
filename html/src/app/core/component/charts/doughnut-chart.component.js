"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DoughnutChartComponent = (function () {
    function DoughnutChartComponent() {
        this.doughnutChartType = 'doughnut';
        this.showLegend = true;
    }
    DoughnutChartComponent.prototype.setDefinition = function (definition) {
        this.definition = definition;
    };
    // Events.
    DoughnutChartComponent.prototype.chartClicked = function (e) {
    };
    DoughnutChartComponent.prototype.chartHovered = function (e) {
    };
    return DoughnutChartComponent;
}());
DoughnutChartComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'doughnut-chart',
        templateUrl: './doughnut-chart.component.html'
    }),
    __metadata("design:paramtypes", [])
], DoughnutChartComponent);
exports.DoughnutChartComponent = DoughnutChartComponent;
var DoughnutChartDefinition = (function () {
    function DoughnutChartDefinition() {
        this.labels = new Array();
        this.values = new Array();
        this.legendEnabled = true;
        this.contextMenuOptions = new Array();
    }
    DoughnutChartDefinition.prototype.addValue = function (label, value) {
        this.labels.push(label);
        this.values.push(value);
    };
    DoughnutChartDefinition.prototype.addContextMenuOption = function (menuOption) {
        this.contextMenuOptions.push(menuOption);
    };
    return DoughnutChartDefinition;
}());
exports.DoughnutChartDefinition = DoughnutChartDefinition;
var ContextMenuOption = (function () {
    function ContextMenuOption(id, text) {
        this.id = id;
        this.text = text;
    }
    return ContextMenuOption;
}());
exports.ContextMenuOption = ContextMenuOption;
//# sourceMappingURL=doughnut-chart.component.js.map