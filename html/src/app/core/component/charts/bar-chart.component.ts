import { Component, ViewChild, Output, EventEmitter } from '@angular/core';

@Component
({
    selector: 'bar-chart',
    templateUrl: './bar-chart.component.html'
})
export class BarChartComponent
{
    private model: BarChartModel;
    private chartHeight: string;
    private categoryHeight: number; // The height that will be multiplied by the number of categories to determine the chart height.
    private barChartType:string = 'horizontalBar';
    private barChartLegend:boolean = true;
    private labels: any;
    private barChartOptions:any = 
    {
        scaleShowVerticalLines: false,
        maintainAspectRatio: false,
        scales: 
        {
            xAxes: 
            [{
                ticks: 
                {
                    beginAtZero: true,
                    min: 10
                }
            }],
            yAxes: 
            [{
                ticks: 
                {
                    beginAtZero: true,
                    min: 10,
                    stepSize: 10
                }
            }]
        }
    };

    constructor()
    {
        this.model = null;
        this.categoryHeight = 50;
    }

    public setModel(model: BarChartModel)
    {
        this.chartHeight = model.getCategoryCount() * this.categoryHeight + "px"; 
        this.model = model;
        
        // The current version of ng2-charts has a bug the prevents a simulaneous change to the chart labels and data from being picked up.
        // This delayed setting of the new mode labels, is a temporary workaround for this issue.
        setTimeout(() => 
        {
            this.labels = model.categoryLabels;
        }, 50);
    }

    @Output() chartClicked: EventEmitter<BarChartClickedEvent> = new EventEmitter();
    onChartClicked(chartEvent:any)
    {
        if (chartEvent.active[0])
        {
            let mouseEvent = chartEvent.event;
            let dataIndex = chartEvent.active[0]._index;

            this.chartClicked.emit(new BarChartClickedEvent(mouseEvent, this.model.getCategoryAtIndex(dataIndex)));
        }
    }

    private chartHovered(e:any):void 
    {
    }

    private chartContextMenuItemSelected(menuItem: any)
    {
        console.log("Menu Item Selected: " + menuItem);
    }
}

export class BarChartClickedEvent
{
    mouseEvent: MouseEvent;
    category: BarChartCategoryModel;

    constructor(mouseEvent: MouseEvent, category: BarChartCategoryModel)
    {
        this.mouseEvent = mouseEvent;
        this.category = category;
    }
}

export class BarChartModel
{
    categoryIds: string[];
    categoryLabels: string[];
    series: BarChartSeriesModel[];
    contextMenuOptions: ContextMenuOption[];

    constructor()
    {
        this.categoryIds = new Array();
        this.categoryLabels = new Array();
        this.series = new Array();
        this.contextMenuOptions = new Array();
    }

    public getCategoryAtIndex(index: number): BarChartCategoryModel
    {
        return new BarChartCategoryModel(this.categoryIds[index], this.categoryLabels[index]);
    }

    public getCategoryCount()
    {
        return this.categoryIds.length;
    }

    public getSeries(id: string)
    {
        for (let series of this.series)
        {
            if (series.id == id)
            {
                return series;
            }
        }

        return null;
    }

    public addSeries(id: string, label: string): BarChartSeriesModel
    {
        let seriesDefinition = new BarChartSeriesModel(id, label);
        this.series.push(seriesDefinition);
        return seriesDefinition;
    }

    public addCategory(id:string, label: string)
    {
        this.categoryIds.push(id);
        this.categoryLabels.push(label);
    }

    public addData(seriesId: string, data: any)
    {
        let series: BarChartSeriesModel;
        
        series = this.getSeries(seriesId);
        if (series != null) 
        {
            series.addData(data);
        }
        else throw "Series not found: " + seriesId;
    }

    public addContextMenuOption(menuOption: ContextMenuOption)
    {
        this.contextMenuOptions.push(menuOption);
    }
}

export class BarChartSeriesModel
{
    id: string;
    label: string;
    data: any[];

    constructor(id: string, label: string)
    {
        this.id = id;
        this.label = label;
        this.data = new Array();
    }

    public addData(data: any)
    {
        this.data.push(data);
    }
}

export class BarChartCategoryModel
{
    id: string;
    label: string;

    constructor(id: string, label: string)
    {
        this.id = id;
        this.label = label;
    }
}

export class ContextMenuOption
{
    id: string;
    text: string;

    constructor(id: string, text: string)
    {
        this.id = id;
        this.text = text;
    }
}