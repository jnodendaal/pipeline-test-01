"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular2_contextmenu_1 = require("angular2-contextmenu");
var BarChartComponent = (function () {
    function BarChartComponent(contextMenuService) {
        this.barChartType = 'horizontalBar';
        this.barChartLegend = true;
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            min: 10
                        }
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            min: 10,
                            stepSize: 10
                        }
                    }]
            }
        };
        this.chartClicked = new core_1.EventEmitter();
        this.model = null;
        this.categoryHeight = 50;
        this.contextMenuService = contextMenuService;
    }
    BarChartComponent.prototype.setModel = function (model) {
        this.chartHeight = model.getCategoryCount() * this.categoryHeight + "px";
        this.model = model;
    };
    BarChartComponent.prototype.onChartClicked = function (chartEvent) {
        if (chartEvent.active[0]) {
            var mouseEvent = chartEvent.event;
            var dataIndex = chartEvent.active[0]._index;
            this.chartClicked.emit(new BarChartClickedEvent(mouseEvent, this.model.getCategoryAtIndex(dataIndex)));
        }
    };
    BarChartComponent.prototype.chartHovered = function (e) {
    };
    BarChartComponent.prototype.chartContextMenuItemSelected = function (menuItem) {
        console.log("Menu Item Selected: " + menuItem);
    };
    return BarChartComponent;
}());
__decorate([
    core_1.ViewChild('chartContextMenu'),
    __metadata("design:type", angular2_contextmenu_1.ContextMenuComponent)
], BarChartComponent.prototype, "contextMenu", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], BarChartComponent.prototype, "chartClicked", void 0);
BarChartComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'bar-chart',
        templateUrl: './bar-chart.component.html',
        providers: [angular2_contextmenu_1.ContextMenuService]
    }),
    __metadata("design:paramtypes", [angular2_contextmenu_1.ContextMenuService])
], BarChartComponent);
exports.BarChartComponent = BarChartComponent;
var BarChartClickedEvent = (function () {
    function BarChartClickedEvent(mouseEvent, category) {
        this.mouseEvent = mouseEvent;
        this.category = category;
    }
    return BarChartClickedEvent;
}());
exports.BarChartClickedEvent = BarChartClickedEvent;
var BarChartModel = (function () {
    function BarChartModel() {
        this.categoryIds = new Array();
        this.categoryLabels = new Array();
        this.series = new Array();
        this.contextMenuOptions = new Array();
    }
    BarChartModel.prototype.getCategoryAtIndex = function (index) {
        return new BarChartCategoryModel(this.categoryIds[index], this.categoryLabels[index]);
    };
    BarChartModel.prototype.getCategoryCount = function () {
        return this.categoryIds.length;
    };
    BarChartModel.prototype.getSeries = function (id) {
        for (var _i = 0, _a = this.series; _i < _a.length; _i++) {
            var series = _a[_i];
            if (series.id == id) {
                return series;
            }
        }
        return null;
    };
    BarChartModel.prototype.addSeries = function (id, label) {
        var seriesDefinition = new BarChartSeriesModel(id, label);
        this.series.push(seriesDefinition);
        return seriesDefinition;
    };
    BarChartModel.prototype.addCategory = function (id, label) {
        this.categoryIds.push(id);
        this.categoryLabels.push(label);
    };
    BarChartModel.prototype.addData = function (seriesId, data) {
        var series;
        series = this.getSeries(seriesId);
        if (series != null) {
            series.addData(data);
        }
        else
            throw "Series not found: " + seriesId;
    };
    BarChartModel.prototype.addContextMenuOption = function (menuOption) {
        this.contextMenuOptions.push(menuOption);
    };
    return BarChartModel;
}());
exports.BarChartModel = BarChartModel;
var BarChartSeriesModel = (function () {
    function BarChartSeriesModel(id, label) {
        this.id = id;
        this.label = label;
        this.data = new Array();
    }
    BarChartSeriesModel.prototype.addData = function (data) {
        this.data.push(data);
    };
    return BarChartSeriesModel;
}());
exports.BarChartSeriesModel = BarChartSeriesModel;
var BarChartCategoryModel = (function () {
    function BarChartCategoryModel(id, label) {
        this.id = id;
        this.label = label;
    }
    return BarChartCategoryModel;
}());
exports.BarChartCategoryModel = BarChartCategoryModel;
var ContextMenuOption = (function () {
    function ContextMenuOption(id, text) {
        this.id = id;
        this.text = text;
    }
    return ContextMenuOption;
}());
exports.ContextMenuOption = ContextMenuOption;
//# sourceMappingURL=bar-chart.component.js.map