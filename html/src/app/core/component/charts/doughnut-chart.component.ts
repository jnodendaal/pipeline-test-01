import { Component } from '@angular/core';
 
@Component
({
  selector: 'doughnut-chart',
  templateUrl: './doughnut-chart.component.html'
})
export class DoughnutChartComponent 
{
  private definition: DoughnutChartDefinition;
  private doughnutChartType:string = 'doughnut';
  private showLegend: boolean;

  constructor()
  {
    this.showLegend = true;
  }

  public setDefinition(definition: DoughnutChartDefinition)
  {
      this.definition = definition;
  } 

  // Events.
  public chartClicked(e:any):void 
  {
  }

  public chartHovered(e:any):void 
  {
  }
}


export class DoughnutChartDefinition
{
    labels: string[];
    values: any[];
    legendEnabled: boolean;
    contextMenuOptions: ContextMenuOption[];

    constructor()
    {
        this.labels = new Array();
        this.values = new Array();
        this.legendEnabled = true;
        this.contextMenuOptions = new Array();
    }

    public addValue(label: string, value: any)
    {
        this.labels.push(label);
        this.values.push(value);
    }

    public addContextMenuOption(menuOption: ContextMenuOption)
    {
        this.contextMenuOptions.push(menuOption);
    }
}

export class ContextMenuOption
{
    id: string;
    text: string;

    constructor(id: string, text: string)
    {
        this.id = id;
        this.text = text;
    }
}