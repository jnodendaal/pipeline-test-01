import { Component } from '@angular/core';
import 'chartjs';
 
@Component
({
  selector: 'polar-area-chart',
  templateUrl: './polar-area-chart.component.html'
})
export class PolarAreaChartComponent 
{
  private definition: PolarAreaChartDefinition;
  private polarAreaLegend: boolean = true;
  private polarAreaChartType:string = 'polarArea';
  private polarAreaChartOptions:any = 
  {
    responsive: true,
    legend: 
    {
      display: true,
      fullWidth: true,
      position: 'bottom'
    }
  };

  constructor()
  {
    this.polarAreaLegend = true;
  }

  public setDefinition(definition: PolarAreaChartDefinition)
  {
      this.definition = definition;
  } 

  // Events.
  public chartClicked(e:any):void 
  {
  }

  public chartHovered(e:any):void 
  {
  }
}

export class PolarAreaChartDefinition
{
    labels: string[];
    values: any[];
    legendEnabled: boolean;
    contextMenuOptions: ContextMenuOption[];

    constructor()
    {
        this.labels = new Array();
        this.values = new Array();
        this.legendEnabled = true;
        this.contextMenuOptions = new Array();
    }

    public addValue(label: string, value: any)
    {
        this.labels.push(label);
        this.values.push(value);
    }

    public addContextMenuOption(menuOption: ContextMenuOption)
    {
        this.contextMenuOptions.push(menuOption);
    }
}

export class ContextMenuOption
{
    id: string;
    text: string;

    constructor(id: string, text: string)
    {
        this.id = id;
        this.text = text;
    }
}