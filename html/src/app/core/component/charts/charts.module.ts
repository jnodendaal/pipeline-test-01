import { NgModule }                             from '@angular/core';
import { CommonModule }                         from '@angular/common';
import { FormsModule }                          from '@angular/forms';
import { RouterModule }                         from '@angular/router';
import { ChartsModule }                         from 'ng2-charts';
import { SharedModule }                         from '../../shared.module';

import { DoughnutChartComponent }               from './doughnut-chart.component';
import { PolarAreaChartComponent }              from './polar-area-chart.component';
import { BarChartComponent }                    from './bar-chart.component';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    RouterModule,
    SharedModule,
    ChartsModule
  ],
  declarations: 
  [ 
    DoughnutChartComponent,
    PolarAreaChartComponent,
    BarChartComponent
  ],
  exports:      [ DoughnutChartComponent, PolarAreaChartComponent, BarChartComponent ],
  providers:    [ ]
})
export class T8ChartsModule 
{
}