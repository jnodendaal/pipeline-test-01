import { Component, ViewChild, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FunctionalityManagerService } from '../../service/functionality-manager.service';
import { FunctionalityAccessHandle } from '../../model/functionality.model';
import { ComponentManagerService } from '../../../core/service/component-manager.service';
import { Report, ReportSearchResults } from '../../model/report.model';
import { ReportManagerService } from '../../service/report-manager.service';
import { UserManagerService } from '../../service/user-manager.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { saveAs } from "file-saver";
import { Context } from '../../../core/model/security.model';
import { CoreComponent } from '../../../core/component/core.component';
import { ComponentState } from '../../../core/model/component.model';
import { ApplicationSettings } from '../../../app.config';

@Component
({
    selector: 'report-list',
    templateUrl: 'report-list.component.html',
    styleUrls: ['report-list.component.css'],
    providers: [ ComponentManagerService ]
})
export class ReportListComponent extends CoreComponent<ReportListState> implements OnInit, OnDestroy
{   
    private reportManager: ReportManagerService;
    private userManager: UserManagerService;
    private dataCount: number;
    private loading: boolean;
    private searchPattern: string;
    private searchExpression: string;
    private searchResults: ReportSearchResults;
    private pageSize: number;
    private pageOffset: number;
    private pageNumber: number;
    private selectedReport: Report;
    private reportUrl: string;
    private imageUrl: string;
    private deletionConfirmationVisible: boolean;
    private reportToBeDeleted: Report;

    @ViewChild('deletionConfirmationModal') 
    private deletionConfirmationModal: ModalDirective;
    
    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, reportManager: ReportManagerService, userManager: UserManagerService)
    {
        super(router, route, functionalityManager, componentManager);
        this.reportManager = reportManager;
        this.userManager = userManager;
        this.dataCount = 0;
        this.pageSize = 10;
        this.pageOffset = 0;
        this.pageNumber = 1;
        this.loading = false;
        this.selectedReport = null;
        this.searchResults = new ReportSearchResults();
        this.reportUrl = ApplicationSettings.API_REPORT_URL + '/';
        this.imageUrl = ApplicationSettings.API_IMAGE_URL + "/";
        this.deletionConfirmationVisible = false;
    }

    public ngOnInit()
    {
        this.newSearch();
    }

    public ngOnDestroy()
    {
        this.releaseFunctionality();
    }

    private getThumbnailUrl(report: Report)
    {
        if (report.thumbnailImageId)
        {
            return this.imageUrl + encodeURIComponent(report.thumbnailImageId);
        }
        else return ApplicationSettings.NO_IMAGE_URL;
    }

    private onReportSelected(report: Report)
    {
        console.log("Report selected:" + report.iid);
        this.selectedReport = report;

        // Download the report.
        this.reportManager.downloadReport(report.iid)
            .subscribe
            (
                blob => 
                {
                    saveAs(blob, report.fileName);
                }
            );
    }

    private deleteReport(report: Report)
    {
        console.log("Report deletion:" + report.iid);
        this.reportManager.deleteReport(this.ctx, report.iid)
            .subscribe
            (
                data=>
                {
                    this.search();
                }
            )
    }

    private showDeletionConfirmation(report: Report)
    {
        this.reportToBeDeleted = report;
        this.deletionConfirmationVisible = true;
    }

    private hideDeletionConfirmation()
    {
        this.reportToBeDeleted = null;
        this.deletionConfirmationModal.hide();
    }

    private onDeletionConfirmationHidden()
    {
        this.deletionConfirmationVisible = false;
    }

    private confirmDeletion()
    {
        this.deleteReport(this.reportToBeDeleted);
        this.reportToBeDeleted = null;
        this.hideDeletionConfirmation();
    }

    private newSearch()
    {
        this.pageOffset = 0;
        this.pageNumber = 1;
        this.search();
    }

    private search()
    {
        this.loading = true;
        this.reportManager.searchReports(this.ctx, this.searchExpression, this.pageOffset, this.pageSize)
        .subscribe
            (
                data => 
                {
                    this.loading = false;
                    this.searchResults = data;
                    this.searchPattern = "";
                    
                    for (let searchTerm of data.searchTerms)
                    {
                        if (this.searchPattern.length > 0) this.searchPattern += "|";
                        this.searchPattern += searchTerm;
                    }
                },
                error => 
                {
                    console.log("Error while searching reports: " + error);
                    this.loading = false;
                }
            );
    }

    private nextPage()
    {
        this.pageOffset += this.pageSize;
        this.pageNumber = this.pageOffset / this.pageSize + 1;
        this.search();
    }

    private previousPage()
    {
        if (this.pageOffset > 0)
        {
            this.pageOffset -= this.pageSize;
            this.pageNumber = this.pageOffset / this.pageSize + 1;
            this.search();
        }
    }

    protected createState(): ReportListState
    {
        this.state = new ReportListState();
        return this.state;
    }

    protected getState(): ReportListState
    {
        return this.state;
    }

    protected setState(state: ReportListState)
    {
        this.state = state;
    }
}

export class ReportListState extends ComponentState
{
}