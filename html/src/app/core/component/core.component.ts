import { Component, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FunctionalityManagerService } from '../service/functionality-manager.service';
import { ComponentManagerService } from '../service/component-manager.service';
import { FunctionalityAccessHandle } from '../model/functionality.model';
import { ComponentState } from '../model/component.model';
import { RouteUtility } from '../utility/angular.utility';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Context } from '../model/security.model';
import { IdUtility } from '../utility/id.utility';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

export abstract class CoreComponent<S extends ComponentState>
{
    protected functionalityManager: FunctionalityManagerService;
    protected accessHandle: FunctionalityAccessHandle;
    protected componentManager: ComponentManagerService;
    protected routeUtility: RouteUtility;
    protected parameters: any;
    protected ctx: Context;
    protected router: Router;
    protected route: ActivatedRoute;
    protected routeSnapshot: ActivatedRouteSnapshot;
    protected state: S; 
    protected newState: boolean;
    protected routeComponent: boolean;
    protected initialized: boolean;

    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService)
    {
        console.log("Constructing component: " + this.constructor.name);
        this.router = router;
        this.route = route;
        this.routeSnapshot = route.snapshot;
        this.functionalityManager = functionalityManager;
        this.componentManager = componentManager;
        this.routeUtility = new RouteUtility(router, route);
        this.parameters = {};
        this.createState();
        this.newState = true;
        this.initialized = false;
        this.routeComponent = (this.routeSnapshot.routeConfig.component.name == this.constructor.name);

        // Process the query parameters of the activated route.
        route.queryParams.distinctUntilChanged().subscribe(params => 
        {
            // Make sure to check the initialized flag to ensure that we do not get cyclic executions.
            if (!this.initialized)
            {
                this.routeSnapshot = this.route.snapshot;
                this.initCoreComponent(params);
            }
        });
    }

    private initCoreComponent(queryParams: any)
    {
        let stateId: string;

        // If this component is the routeComponent get its id from the route.
        if (this.routeComponent)
        {
            this.state.setId(this.routeSnapshot.data ? this.routeSnapshot.data.id : null);
            this.state.setInputParameters(this.componentManager.getComponentInputParameters(this.state.getId()));
        }

        // If a state for this component is specified, load it.
        stateId = this.state.getId();
        if (stateId)
        {
            let functionalityIid: string;
            let statesParameter: string;

            // Log this event.
            console.log("Initializing component: " + this.constructor.name + ":" + stateId);

            // Set the initialized flag to that this method does not run again.
            this.initialized = true;
            
            // Get the access handle from the functionality manager using the iid passed to this component via the activated route.
            functionalityIid = queryParams['functionality_iid'];
            this.accessHandle = this.functionalityManager.getFunctionalityAccessHandle(functionalityIid);
            if (this.accessHandle)
            {
                // Get the component context.
                this.ctx = this.accessHandle.getContext();
                
                // Get the component parameters from the access handle.
                this.parameters = this.accessHandle.componentParameters[stateId];
            }
            else throw ("Functionality access handle not found: " + functionalityIid);
            
            // Load the state of this component.
            statesParameter = queryParams['states'];
            if (statesParameter)
            {
                let states: any;
                let state: S;
                
                states = JSON.parse(statesParameter);
                state = this.componentManager.loadState(states[stateId]) as S;
                if (state)
                {
                    this.newState = false;
                    this.setState(state);
                    if (state) console.log("Loaded state from " + this.constructor.name + ": " + stateId + ":" + state.getIid());
                }
                else // The set of saved states does not contain this state, so add it now by saving the state.
                {
                    // Save the state.
                    this.saveState();
                }
            }
            else // No states have been saved yet, so save the state of this component now.
            {
                // Save the state.
                this.saveState();
            }
        }
    }

    public releaseFunctionality()
    {
        this.functionalityManager.releaseFunctionality(this.accessHandle.iid).subscribe();
    }

    @Input()
    set id(componentId: string)
    {
        this.state.id = componentId;
        this.state.setId(componentId);
        this.state.setInputParameters(this.componentManager.getComponentInputParameters(componentId));
        this.initCoreComponent(this.routeSnapshot.queryParams);
    }
    
    public getComponentQueryParameters(): any
    {
        let params =
        {
            "functionality_iid": this.accessHandle.iid,
            "states": this.route.queryParams["states"]
        };

        return params;
    }

    public getSubComponentQueryParameters(): any
    {
        let params =
        {
            "functionality_iid": this.accessHandle.iid,
            "states": this.routeSnapshot.queryParams["states"]
        };

        return params;
    }

    protected saveState()
    {
        let state: S;

        state = this.getState();
        if (state != null)
        {
            let statesParameter: string;
            let states: any;
            let stateId;

            stateId = state.getId();
            console.log("Saving component state: " + this.constructor.name + ":" + stateId);
            statesParameter = this.routeSnapshot.queryParams['states'];
            if (statesParameter)
            {
                states = JSON.parse(statesParameter);
            }
            else
            {
                states = {};
            }

            states[stateId] = state.getIid();
            this.componentManager.saveState(state);
            this.routeUtility.changeQueryParameter("states", JSON.stringify(states), true);
        }
    }

    protected abstract getState(): S;
    protected abstract createState(): S;
    protected abstract setState(state: S): void;
}