import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) 
{
  if (parentModule) 
  {
    throw new Error(`${moduleName} has already been loaded and is intended as a single import only.`);
  }
}

export class RouteUtility
{
  private router: Router;
  private activatedRoute: ActivatedRoute;

  constructor(router: Router, activatedRoute: ActivatedRoute)
  {
    this.router = router;
    this.activatedRoute = activatedRoute;
  }

  public changeQueryParameter(parameterId: string, parameterValue: any, replaceUrl: boolean)
  {
    this.router.navigate([], 
    {
      relativeTo: this.activatedRoute,
      queryParams: 
      {
        ...this.activatedRoute.snapshot.queryParams,
        [parameterId]: parameterValue
      },
      replaceUrl: replaceUrl
    });
  }

  public changeQueryParameters(params: any, replaceUrl: boolean)
  {
    this.router.navigate([], 
    {
      relativeTo: this.activatedRoute,
      queryParams: 
      {
        ...this.activatedRoute.snapshot.queryParams,
        ...params
      },
      replaceUrl: replaceUrl
    });
  }
}