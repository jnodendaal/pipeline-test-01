var uuid = require('uuid');
var validate = require('uuid-validate');

export class IdUtility
{
    public static newId(): string
    {
        let id: string;

        id = uuid.v4();
        id = id.replace(/[-]/g, ""); // Remove all dashes.
        return id.toUpperCase();
    }

    public static isId(id: string): boolean
    {
        let uuid: string;

        uuid = id.substr(0,8);
        uuid = uuid + "-" + id.substr(8,4);
        uuid = uuid + "-" + id.substr(12,4);
        uuid = uuid + "-" + id.substr(16,4);
        uuid = uuid + "-" + id.substr(20);
        return validate(uuid, 4);
    }
}