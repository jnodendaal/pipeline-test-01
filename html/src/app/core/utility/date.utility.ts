export class SimpleDateFormat
{
    /**
     * This method converts a date time format string as specified in the Java SimpleDateFormat documentation
     * and converts it to the equivalent Moment.js format.
     * SimpleDateFormat: https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
     * Moment: https://momentjs.com/docs/
     * @param simpleDateFormat The date time format string in Java SimpleDateFormat notation.
     */
    public static toMomentFormat(simpleDateFormat: string): string
    {
        let format = simpleDateFormat;

        format = format.replace(/Y/g, "G"); // Week year.
        format = format.replace(/y/g, "Y"); // Year.
        format = format.replace(/d/g, "D"); // Day of month.
        return format;
    }
}