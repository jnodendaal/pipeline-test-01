"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HighlightTextDirective = (function () {
    function HighlightTextDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.matchOperatorsRe = '/[|\\{}()[\]^$+*?.]/g';
    }
    HighlightTextDirective.prototype.ngOnInit = function () {
        if (typeof this.classToApply === 'undefined') {
            this.classToApply = '';
        }
        if (typeof this.pattern === 'undefined') {
            this.renderer.setElementProperty(this.el.nativeElement, 'innerHTML', this.text);
            return;
        }
        else {
            var search = this.escapeStringRegexp(this.pattern.toString());
            this.renderer.setElementProperty(this.el.nativeElement, 'innerHTML', this.replace(this.text, search));
        }
    };
    HighlightTextDirective.prototype.replace = function (txt, search) {
        var searchRgx = new RegExp('(' + search + ')', 'gi');
        return txt.replace(searchRgx, "<span class=\"" + this.classToApply + "\">$1</span>");
    };
    HighlightTextDirective.prototype.escapeStringRegexp = function (str) {
        if (typeof str !== 'string') {
            throw new TypeError('Expected a string');
        }
        return str.replace(this.matchOperatorsRe, '\\$&');
    };
    ;
    return HighlightTextDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], HighlightTextDirective.prototype, "pattern", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], HighlightTextDirective.prototype, "text", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], HighlightTextDirective.prototype, "classToApply", void 0);
HighlightTextDirective = __decorate([
    core_1.Directive({
        selector: '[highlightText]'
    }),
    __metadata("design:paramtypes", [core_1.ElementRef, core_1.Renderer])
], HighlightTextDirective);
exports.HighlightTextDirective = HighlightTextDirective;
//# sourceMappingURL=highlight-text.directive.js.map