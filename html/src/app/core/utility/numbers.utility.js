"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Numbers = (function () {
    function Numbers() {
    }
    /**
     * Decimal adjustment of a number.
     * Source from Mozilla MDN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
     *
     * @param {String}  type  The type of adjustment.
     * @param {Number}  value The number.
     * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
     * @returns {Number} The adjusted value.
     */
    Numbers.decimalAdjust = function (type, value, exp) {
        // If the exp is undefined or zero use the normal Math.round() function.
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math.round(value);
        }
        else {
            value = +value;
            exp = +exp;
            // If the value is not a number or the exp is not an integer return NaN.
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            else {
                // If the value is negative...
                if (value < 0) {
                    return -Numbers.decimalAdjust(type, -value, exp);
                }
                else {
                    var valueString = void 0;
                    // Shift
                    valueString = value.toString().split('e');
                    value = Math[type](+(valueString[0] + 'e' + (valueString[1] ? (+valueString[1] - exp) : -exp)));
                    // Shift back
                    valueString = value.toString().split('e');
                    return +(valueString[0] + 'e' + (valueString[1] ? (+valueString[1] + exp) : exp));
                }
            }
        }
    };
    Numbers.round = function (value, exp) {
        return this.decimalAdjust('round', value, exp);
    };
    Numbers.floor = function (value, exp) {
        return this.decimalAdjust('floor', value, exp);
    };
    Numbers.ceil = function (value, exp) {
        return this.decimalAdjust('ceil', value, exp);
    };
    return Numbers;
}());
exports.Numbers = Numbers;
//# sourceMappingURL=numbers.utility.js.map