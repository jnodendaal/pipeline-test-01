export class NumberUtility
{
  /**
   * Decimal adjustment of a number.
   * Source from Mozilla MDN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
   *
   * @param {String}  type  The type of adjustment.
   * @param {Number}  value The number.
   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
   * @returns {Number} The adjusted value.
   */
    private static decimalAdjust(type: string, value: number, exp: number): number 
    {
        // If the exp is undefined or zero use the normal Math.round() function.
        if (typeof exp === 'undefined' || +exp === 0) 
        {
            return Math.round(value);
        }
        else
        {
            value = +value;
            exp = +exp;

            // If the value is not a number or the exp is not an integer return NaN.
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) 
            {
                return NaN;
            }
            else
            {
                // If the value is negative...
                if (value < 0) 
                {
                    return -NumberUtility.decimalAdjust(type, -value, exp);
                }
                else
                {
                    let valueString: string[];

                    // Shift
                    valueString = value.toString().split('e');
                    value = Math[type](+(valueString[0] + 'e' + (valueString[1] ? (+valueString[1] - exp) : -exp)));
                    // Shift back
                    valueString = value.toString().split('e');
                    return +(valueString[0] + 'e' + (valueString[1] ? (+valueString[1] + exp) : exp));
                }
            }
        }
    }

    public static round(value: number, exp: number) 
    {
        return this.decimalAdjust('round', value, exp);
    }

    public static floor(value: number, exp: number) 
    {
        return this.decimalAdjust('floor', value, exp);
    }

    public static ceil(value: number, exp: number) 
    {
        return this.decimalAdjust('ceil', value, exp);
    }
}