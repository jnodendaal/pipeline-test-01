import { Component, Directive, TemplateRef, ViewContainerRef, Input, ComponentFactory, ComponentFactoryResolver, OnInit } from '@angular/core';
import { EmbeddedViewRef } from '@angular/core/src/linker/view_ref';
import { ComponentManagerService } from '../service/component-manager.service';

@Component
({
    template: `<router-outlet (activate)='onActivate($event)' (deactivate)='onDeactivate($event)'></router-outlet>`
})
export class RouteViewComponent 
{
    private componentManager: ComponentManagerService;

    constructor(componentManager: ComponentManagerService)
    {
        this.componentManager = componentManager;
    }

    public onActivate(event: any)
    {
    }

    public onDeactivate(event: any)
    {
    }
}

/**
 * This purpose of this directive is to automatically add a <router-outlet> to the target component.
 * Where a route is displayed in the outlet, the content of the component is then hidden so that it
 * appears as if the new route has completely replaced the existing component.  This allows nested
 * routes to be displayed in the same view with only the lowest level route being active in the
 * DOM.
 * 
 * The switch handled by this directive is between the 'normalView' (the target templateRef) and the
 * 'routeView' which is the <router-outlet> component that will display a sub-route component and replace
 * the current 'normalView' component.
 */
@Directive({selector: '[routeView]'})
export class RouteViewDirective implements OnInit
{
    private templateRef: TemplateRef<void>;
    private vcr: ViewContainerRef;
    private cfr: ComponentFactoryResolver;
    private embeddedViewRef: EmbeddedViewRef<void>;
    private viewVisible:boolean;

    // This input is the initial and overriding enable switch of the directive.  
    // If this value is false, the directive will not add a <router-outlet> to the target container and the routeView cannot be made visible..
    @Input('routeView') enabled: boolean;

    // This input switches between the routeView (true) and the normal view (false).
    // The routeView is a <router-outlet> in which a route component is displayed i.e. when this setting is false, the <router-outlet> is removed
    // and the regular input templateRef is displayed.
    @Input() 
    set routeViewVisible(value: boolean) 
    {
        if (this.enabled)
        {
            this.viewVisible = value;
            this.updateView();
        }
    }

    constructor(templateRef: TemplateRef<void>, vcr: ViewContainerRef, cfr: ComponentFactoryResolver)    
    { 
        this.templateRef = templateRef;
        this.vcr = vcr;
        this.cfr = cfr;
    }

    ngOnInit()
    {
        if (this.enabled)
        {
            let cmpFactory = this.cfr.resolveComponentFactory(RouteViewComponent)
            this.vcr.createComponent(cmpFactory);
        }
        else
        {
            // The route view is not visible, so embed the templateRef of the non-route view component.
            this.embeddedViewRef = this.vcr.createEmbeddedView(this.templateRef)
        }
    }

    private updateView()
    {
        // If the route view is visible, remove the templateRef that has been embedded (the non-route view).
        if (this.viewVisible) 
        {
            // Make sure we have embedded a view previously before attempting to remove it.
            if (this.embeddedViewRef) 
            {
                this.vcr.remove(this.vcr.indexOf(this.embeddedViewRef));
                this.embeddedViewRef = null;
            }
        }
        else
        {
            // The route view is not visible, so embed the templateRef of the non-route view component.
            this.embeddedViewRef = this.vcr.createEmbeddedView(this.templateRef)
        }
    }
}
