import { Directive, ElementRef, Renderer, Input, OnInit } from '@angular/core';

@Directive
({
  selector: '[highlightText]'
})
export class HighlightTextDirective implements OnInit 
{
    private matchOperatorsRe: string;
    @Input() pattern: string;
    @Input() text: string;
    @Input() classToApply: string;

    constructor(private el: ElementRef, private renderer: Renderer)
    {
        this.matchOperatorsRe = '/[|\\{}()[\]^$+*?.]/g';
    }

    ngOnInit() 
    {
        if (typeof this.classToApply === 'undefined') 
        {
            this.classToApply = '';
        }

        if (this.text)
        {
            if (typeof this.pattern === 'undefined') 
            {
                    this.renderer.setElementProperty(this.el.nativeElement, 'innerHTML', this.text);
                    return;
            }
            else
            {
                let search = this.escapeStringRegexp(this.pattern.toString());
                this.renderer.setElementProperty(this.el.nativeElement, 'innerHTML', this.replace(this.text, search));
            }
        }
    }

    replace(txt: string, search: string) 
    {
        if (txt)
        {
            let matchRegex: RegExp;

            matchRegex = new RegExp('(' + search + ')', 'gi');
            return txt.replace(matchRegex, `<span class="${this.classToApply}">$1</span>`);
        }
    }

    escapeStringRegexp (str: string) 
    {
        if (typeof str !== 'string') 
        {
            throw new TypeError('Expected a string');
        }

        return str.replace(this.matchOperatorsRe, '\\$&');
    };
}