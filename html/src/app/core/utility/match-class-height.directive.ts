import 
{
    Directive, ElementRef, AfterViewInit, AfterViewChecked, 
    Input, HostListener
} from '@angular/core';

@Directive
({
    selector: '[matchClassHeight]'
})
export class MatchClassHeightDirective implements AfterViewInit, AfterViewChecked 
{
    // Class name to match height.
    @Input()
    matchClassHeight: any;

    constructor(private el: ElementRef)
    {
    }

    /**
     * We have to add listeners to all images because they may affect the layout of the target elements once loaded.
     */
    ngAfterViewInit()
    {
        let images = this.el.nativeElement.querySelectorAll('img');
        if (images)
        {
            for (let img of images)
            {
                img.addEventListener('load', this.imageLoaded, false);
            }
        }
    }

    imageLoaded()
    {
        if (this.el)
        {
            this.matchHeight(this.el.nativeElement, this.matchClassHeight)
        }
    }

    ngAfterViewChecked()
    {
        this.matchHeight(this.el.nativeElement, this.matchClassHeight);
    }

    @HostListener('window:resize') 
    onResize()
    {
        this.matchHeight(this.el.nativeElement, this.matchClassHeight);
    }

    matchHeight(parent: HTMLElement, className: string) 
    {
        if (!parent) return;
        else
        {
            const targetElements = parent.getElementsByClassName(className);
            if (!targetElements) return;
            else
            {
                // Reset height of all target elements.
                Array.from(targetElements).forEach((x: HTMLElement) => 
                {
                    x.style.height = 'initial';
                });

                // Gather the heights of all target elements.
                const itemHeights = Array.from(targetElements)
                    .map(x => x.getBoundingClientRect().height);

                // Find max height.
                const maxHeight = itemHeights.reduce((prev, curr) => 
                {
                    return curr > prev ? curr : prev;
                }, 0);

                // Apply max height to all target elements.
                Array.from(targetElements).forEach((x: HTMLElement) => x.style.height = `${maxHeight}px`);
            }
        }
    }
}