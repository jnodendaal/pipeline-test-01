import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RequestOptions, ResponseContentType } from '@angular/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { ReportSearchResults } from '../model/report.model';
import { Context } from '../../core/model/security.model';
import 'rxjs/add/operator/map'
 
@Injectable()
export class ReportManagerService 
{
    constructor(private http: HttpClient) 
    {
        console.log("ReportManagerService");
    }

    private getContextParameters(context: Context)
    {
        return new HttpParams().set('functionality_iid', context.functionalityIid);
    }

    public searchReports(context: Context, searchExpression: string, pageOffset: number, pageSize: number): Observable<ReportSearchResults>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '#OP_SEARCH_REPORTS$P_SEARCH_EXPRESSION': searchExpression, 
            '#OP_SEARCH_REPORTS$P_PAGE_OFFSET': pageOffset,
            '#OP_SEARCH_REPORTS$P_PAGE_SIZE': pageSize
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('#OP_SEARCH_REPORTS'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                let searchResults = new ReportSearchResults();

                console.log(response);
                searchResults.pageOffset = pageOffset;
                searchResults.searchExpression = searchExpression;
                searchResults.searchTerms = response['#OP_SEARCH_REPORTS$P_SEARCH_TERMS'];
                searchResults.addReportsFromJson(response['#OP_SEARCH_REPORTS$P_REPORTS']);
                console.log(searchResults);
                return searchResults;
            });
    }

    public generateReport(context: Context, reportId:string, parameters: any): Observable<string>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '@OP_RPT_GENERATE_REPORT$P_REPORT_ID': reportId,
            '@OP_RPT_GENERATE_REPORT$P_REPORT_PARAMETERS': parameters,
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('@OP_RPT_GENERATE_REPORT'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                return response['@OP_RPT_GENERATE_REPORT$P_PROCESS_IID'];
            });
    }

    public deleteReport(context: Context, reportIid:string): Observable<boolean>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '@OP_RPT_DELETE_REPORT$P_REPORT_IID': reportIid
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('@OP_RPT_DELETE_REPORT'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                return response['@OP_RPT_DELETE_REPORT$P_SUCCESS'];
            });
    }

    public downloadReport(reportId: string): Observable<Blob> 
    {
        return this.http.post(ApplicationSettings.API_REPORT_URL + '/' + reportId, null, { responseType: 'blob', withCredentials: true })
            .map((response) => 
            {
                return response;
            });
    }
}
