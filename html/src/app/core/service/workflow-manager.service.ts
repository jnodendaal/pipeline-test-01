import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { FlowStatus, TaskListSummary, TaskSearchResults, TaskCompletionResult } from '../model/workflow.model';
import { Context } from '../../core/model/security.model';
import 'rxjs/add/operator/map'
 
@Injectable()
export class WorkflowManagerService 
{
    constructor(private http: HttpClient) 
    {
        console.log("Workflow Service Constructor executing...");
    }

    private getContextParameters(context: Context)
    {
        return new HttpParams().set('functionality_iid', context.functionalityIid);
    }

    public getFlowStatus(flowIid: string): Observable<FlowStatus>
    {
        const input = { '@OP_FLW_GET_FLOW_STATUS$P_FLOW_IID': flowIid };
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('@OP_FLW_GET_FLOW_STATUS'), JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                return FlowStatus.fromJson(response['@OP_FLW_GET_FLOW_STATUS$P_FLOW_STATUS']);
            });
    }

    public getTaskListSummary(): Observable<TaskListSummary>
    {
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('@OP_FLW_GET_TASK_LIST_SUMMARY'), null, { withCredentials: true })
            .map((response: Response) => 
            {
                return response['@OP_FLW_GET_TASK_LIST_SUMMARY$P_TASK_LIST_SUMMARY'];
            });
    }

    public claimTask(context: Context, taskIid: string): Observable<boolean>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '@OP_FLW_CLAIM_TASK$P_TASK_IID': taskIid
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('@OP_FLW_CLAIM_TASK'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                if (response) return true;
                else return false;
            });
    }

    public completeTask(context: Context, taskIid: string, outputParameters: any): Observable<TaskCompletionResult>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '@OP_FLW_COMPLETE_TASK$P_TASK_IID': taskIid,
            '@OP_FLW_COMPLETE_TASK$P_OUTPUT_PARAMETERS': outputParameters
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('@OP_FLW_COMPLETE_TASK'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                return TaskCompletionResult.fromJson(response['@OP_FLW_COMPLETE_TASK$P_TASK_COMPLETION_RESULT']);
            });
    }

    public searchTasks(context: Context, searchExpression: string, pageOffset: number, pageSize: number): Observable<TaskSearchResults>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '#OP_SEARCH_TASKS$P_SEARCH_EXPRESSION': searchExpression, 
            '#OP_SEARCH_TASKS$P_PAGE_OFFSET': pageOffset,
            '#OP_SEARCH_TASKS$P_PAGE_SIZE': pageSize
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('#OP_SEARCH_TASKS'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                let searchResults = new TaskSearchResults();

                console.log(response);
                searchResults.pageOffset = pageOffset;
                searchResults.searchExpression = searchExpression;
                searchResults.searchTerms = response['#OP_SEARCH_TASKS$P_SEARCH_TERMS'];
                searchResults.addTasksFromJson(response['#OP_SEARCH_TASKS$P_TASKS']);
                console.log(searchResults);
                return searchResults;
            });
    }
}
