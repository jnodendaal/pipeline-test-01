import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { SessionDetails, UserDetails, UserProfileDetails, WorkflowProfileDetails } from '../model/user.model';
import { HeartbeatResponse } from '../model/security.model';
import { Observable, Subscription } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map'
 
@Injectable()
export class SecurityManagerService 
{
    private session: Session;
    private heartbeatResponse: HeartbeatResponse;
    private heartbeatSubscription: Subscription;
    private sessionUpdatedSource = new Subject<Session>(); // Observable Session source.
    private heartbeatSource = new Subject<HeartbeatResponse>(); // Observable HeartbeatResponse source.

    // Observable Session stream.
    public sessionUpdated$ = this.sessionUpdatedSource.asObservable();

    // Observable HeartbeatResponse stream.
    public heartbeat$ = this.heartbeatSource.asObservable();

    constructor(private http: HttpClient) 
    {
        this.session = new Session();
        this.session.loggedIn = false;
        console.log("SecurityManagerService");
    }

    public startHeartbeat()
    {
        // Run the first iteration immediately because subsequent iterations will only occur after first interval.
        this.heartbeat().subscribe();

        // Create the periodic refresh observable.
        this.heartbeatSubscription = Observable
            .interval(10000)
            .subscribe(() => 
            {
                this.heartbeat().subscribe();
            });
    }

    public stopHeartbeat()
    {
        // Make sure to unsibscribe so that the refresh will no longer occur.
        this.heartbeatSubscription.unsubscribe();
    }

    getSession()
    {
        return this.session;
    }

    isLoggedIn()
    {
        return this.session.loggedIn;
    }
 
    login(username: string, password: string): Observable<LoginResponse>
    {
        const requestParameters = { '@OP_SEC_LOGIN$P_USERNAME': username, '@OP_SEC_LOGIN$P_PASSWORD': password != null ? password.split('') : null, '@OP_SEC_LOGIN$P_END_EXISTING_SESSION': true }; // Java server requires passwords as char arrays for security.
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_SEC_LOGIN', JSON.stringify(requestParameters), { withCredentials: true })
            .map((response: Response) => 
            {
                let responseCode: string;
                let message: string;

                console.log(response);
                responseCode = response['@OP_SEC_LOGIN$P_RESPONSE_CODE'];
                message = response['@OP_SEC_LOGIN$P_MESSAGE'];
                switch (responseCode)
                {
                    case 'SUCCESS':
                    case 'SESSION_ALREADY_LOGGED_IN':
                    case 'SUCCESS_NEW_PASSWORD':
                    case 'PASSWORD_EXPIRED':
                    case 'PASSWORD_EXPIRY_WARNING':
                        let jsonSessionDetails: any;
                    
                        // Update the local session object.
                        this.session.loggedIn = true;
                        jsonSessionDetails = response['@OP_SEC_LOGIN$P_SESSION_DETAILS'];
                        if (jsonSessionDetails) 
                        {
                            this.updateSession(new SessionDetails().fromJson(jsonSessionDetails));
                        }
                        else this.session.sessionDetails = null;

                        // Start the session heartbeat.
                        this.startHeartbeat();
    
                        // Return the login response.
                        console.log("Login Succeeded: " + responseCode + " " + message);
                        return new LoginResponse(responseCode, message);
                    default:
                        // Update the local session object.
                        this.session.loggedIn = false;
                        this.session.sessionDetails = null;

                        // Stop the session heartbeat.
                        this.stopHeartbeat();

                        // Create and return the login response.
                        console.log("Login Failed: " + responseCode + " " + message);
                        return new LoginResponse(responseCode, message);
                }
            });
    }
 
    logout()
    {
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_SEC_LOGOUT', null, { withCredentials: true })
            .map((response: Response) => 
            {
                // Stop the session heartbeat.
                this.stopHeartbeat();

                // Update the session.
                this.session.loggedIn = false;
                this.sessionUpdatedSource.next(this.session); // Emit change via observable to all subscribers.
            });
    }

    changePassword(password: string): Observable<PasswordChangeResponse>
    {
        const requestParameters = { '@OP_SEC_CHANGE_USER_PASSWORD$P_PASSWORD': password != null ? password.split('') : null }; // Java server requires passwords as char arrays for security.
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_SEC_CHANGE_USER_PASSWORD', JSON.stringify(requestParameters), { withCredentials: true })
            .map((response: Response) => 
            {
                let success: boolean;
                let message: string;

                console.log(response);
                success = response['@OP_SEC_CHANGE_USER_PASSWORD$P_SUCCESS'];
                message = response['@OP_SEC_CHANGE_USER_PASSWORD$P_MESSAGE'];
                return new PasswordChangeResponse(success, message);
            });
    }

    getAccessibleUserProfileDetails(): Observable<UserProfileDetails[]>
    {
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_SEC_GET_ACCESSIBLE_USER_PROFILE_DETAILS', null, { withCredentials: true })
            .map((response: Response) => 
            {
                let jsonArray = response['@OP_SEC_GET_ACCESSIBLE_USER_PROFILE_DETAILS$P_USER_PROFILE_DETAILS'];
                let detailsList: UserProfileDetails[];

                // Pack the JSON data into objects.
                detailsList = new Array();
                if (jsonArray)
                {
                    for (let jsonProfileDetails of jsonArray)
                    {
                        let profileDetails: UserProfileDetails;

                        profileDetails = new UserProfileDetails();
                        profileDetails.fromJson(jsonProfileDetails);
                        detailsList.push(profileDetails);
                    }
                }

                return detailsList;
            });
    }

    switchUserProfile(userProfileId: string): Observable<boolean>
    {
        const inputParameters = { '@OP_SEC_SWITCH_USER_PROFILE$P_USER_PROFILE_ID': userProfileId };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_SEC_SWITCH_USER_PROFILE', JSON.stringify(inputParameters), { withCredentials: true })
            .map((response: Response) => 
            {
                if (response['@OP_SEC_SWITCH_USER_PROFILE$P_SUCCESS'])
                {
                    let jsonSessionDetails: any;

                    // Update the local session object.
                    jsonSessionDetails = response['@OP_SEC_SWITCH_USER_PROFILE$P_SESSION_DETAILS'];
                    if (jsonSessionDetails) 
                    {
                        this.updateSession(new SessionDetails().fromJson(jsonSessionDetails));
                    }
                    else this.session.sessionDetails = null;

                    return true;
                }
                else return false;
            });
    }

    getUserDetails(): Observable<UserDetails>
    {
        const inputParameters = { '@OP_SEC_GET_USER_DETAILS$P_USER_ID': this.session.sessionDetails.userId };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_SEC_GET_USER_DETAILS', null, { withCredentials: true })
            .map((response: Response) => 
            {
                let jsonUserDetails = response['@OP_SEC_GET_USER_DETAILS$P_USER_DETAILS'];
                return new UserDetails().fromJson(jsonUserDetails);
            });
    }

    updateUserDetails(userDetails: UserDetails): Observable<SessionDetails>
    {
        const inputParameters = { '@OP_SEC_UPDATE_USER_DETAILS$P_USER_DETAILS': userDetails };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_SEC_UPDATE_USER_DETAILS', inputParameters, { withCredentials: true })
            .map((response: Response) => 
            {
                if (response['@OP_SEC_UPDATE_USER_DETAILS$P_SUCCESS'])
                {
                    let jsonSessionDetails = response['@OP_SEC_UPDATE_USER_DETAILS$P_SESSION_DETAILS'];
                    let sessionDetails: SessionDetails;
                    
                    sessionDetails = new SessionDetails().fromJson(jsonSessionDetails);
                    this.updateSession(sessionDetails);
                    return sessionDetails;
                }
            });
    }

    private heartbeat(): Observable<HeartbeatResponse>
    {
        return this.http.post(ApplicationSettings.API_URL + '/api/heartbeat', null, { withCredentials: true })
            .map((response: Response) => 
            {
                this.heartbeatResponse = HeartbeatResponse.fromJson(response);
                this.heartbeatSource.next(this.heartbeatResponse); // Emit change via observable to all subscribers.
                return this.heartbeatResponse;
            });
    }

    private updateSession(sessionDetails: SessionDetails)
    {
        this.session.sessionDetails = sessionDetails;
        this.sessionUpdatedSource.next(this.session); // Emit change via observable to all subscribers.
    }
}

export class Session
{
    loggedIn: boolean;
    sessionDetails: SessionDetails;
    heartbeat: HeartbeatResponse;
}

export class LoginResponse
{
    responseCode: string;
    message: string;

    constructor(code: string, message: string)
    {
        this.responseCode = code;
        this.message = message;
    }
}

export class PasswordChangeResponse
{
    success: boolean;
    message: string;

    constructor(success: boolean, message: string)
    {
        this.success = success;
        this.message = message;
    }
}