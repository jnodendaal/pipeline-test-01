import { Injectable } from '@angular/core';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { ComponentState } from '../../core/model/component.model';
import { TerminologySet } from '../../core/model/terminology.model';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/operator/map'
 
@Injectable()
export class ComponentManagerService 
{
    private toastr: ToastrService;
    private states: {[key: string]: ComponentState} = {};
    private inputParameters: {[key: string]: any} = {};
    private terminologySet: TerminologySet;

    constructor(toastrService: ToastrService) 
    {
        console.log("ComponentManagerService");
        this.toastr = toastrService;
        this.states = {};
        this.terminologySet = new TerminologySet(null);
    }

    public clearStates()
    {
        this.states = {};
    }

    public saveState(state: ComponentState)
    {
        this.states[state.getIid()] = state;
    }

    public loadState(iid: string): ComponentState
    {
        return this.states[iid];
    }

    public getComponentInputParameters(id: string)
    {
        return this.inputParameters[id];
    }

    public setComponentInputParameters(id: string, parameters: any)
    {
        this.inputParameters[id] = parameters;
    }

    public toastSuccess(title: string, message: string)
    {
        this.toastr.success(message, title);
    }

    public toastInfo(title: string, message: string)
    {
        this.toastr.info(message, title);
    }

    public toastError(title: string, message: string)
    {
        this.toastr.error(message, title);
    }

    public getTerminology(): TerminologySet
    {
        return this.terminologySet;
    }

    public addTerminology(terminology: TerminologySet)
    {
        this.terminologySet.addTerminologySet(terminology);
    }
}