import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { Context } from '../../core/model/security.model';
import { FunctionalityGroupHandle, FunctionalityAccessHandle } from '../../core/model/functionality.model';
import 'rxjs/add/operator/map'
 
@Injectable()
export class FunctionalityManagerService 
{
    private accessHandles: Map<string, FunctionalityAccessHandle>;

    constructor(private http: HttpClient) 
    {
        console.log("FunctionalityManagerService");
        this.accessHandles = new Map();
    }

    private getContextParameters(context: Context)
    {
        return new HttpParams().set('functionality_iid', context.functionalityIid);
    }

    public getFunctionalityAccessHandle(functionalityIid: string): FunctionalityAccessHandle
    {
        return this.accessHandles[functionalityIid];
    }

    public getFunctionalityGroupHandle(context: Context, functionalityGroupId: string, dataObjectId: string, dataObjectIid: string): Observable<FunctionalityGroupHandle>
    {
        const input = 
        { 
            '@OS_GET_FUNCTIONALITY_GROUP_HANDLE$P_FUNCTIONALITY_GROUP_ID': functionalityGroupId ? functionalityGroupId.toUpperCase() : null, 
            '@OS_GET_FUNCTIONALITY_GROUP_HANDLE$P_DATA_OBJECT_ID': dataObjectId,
            '@OS_GET_FUNCTIONALITY_GROUP_HANDLE$P_DATA_OBJECT_IID': dataObjectIid
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OS_GET_FUNCTIONALITY_GROUP_HANDLE', JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context)})
            .map((response: Response) => 
            {
                let groupHandle = new FunctionalityGroupHandle();

                console.log(response);
                groupHandle.fromJson(response['@OS_GET_FUNCTIONALITY_GROUP_HANDLE$P_FUNCTIONALITY_GROUP_HANDLE']);
                console.log(groupHandle);
                return groupHandle;
            });
    }

    public accessFunctionality(functionalityId: string, parameters: any): Observable<FunctionalityAccessHandle>
    {
        const input = 
        { 
            '@OS_ACCESS_FUNCTIONALITY$P_FUNCTIONALITY_ID': functionalityId ? functionalityId.toUpperCase() : null, 
            '@OS_ACCESS_FUNCTIONALITY$P_FUNCTIONALITY_PARAMETERS': parameters
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OS_ACCESS_FUNCTIONALITY', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                let accessHandle = new FunctionalityAccessHandle();

                // Get the access handle from the operation output.
                accessHandle.fromJson(response['@OS_ACCESS_FUNCTIONALITY$P_FUNCTIONALITY_EXECUTION_HANDLE']);

                // Cache the access handle locally, so that all component can share it.
                if (accessHandle != null) this.accessHandles[accessHandle.iid] = accessHandle;
                return accessHandle;
            });
    }

    public releaseFunctionality(functionalityIid: string): Observable<void>
    {
        // Remove the locally cached handle identifier.
        delete this.accessHandles[functionalityIid];

        // Create the input for the server operation.
        const input = 
        { 
            '@OS_RELEASE_FUNCTIONALITY$P_FUNCTIONALITY_IID': functionalityIid
        };
        
        // Post the server request to release the functionality and subscribe immediately
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OS_RELEASE_FUNCTIONALITY', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
                {
                    console.log("Access Handles Cached: " + this.accessHandles.size);
                }
            );
    }
}
