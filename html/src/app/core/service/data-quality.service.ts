import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { GroupedDataQualityReport } from '../../core/model/data-record-quality.model';
import 'rxjs/add/operator/map'
 
@Injectable()
export class DataQualityService 
{
    constructor(private http: HttpClient) 
    {
        console.log("Data Quality Service Constructor executing...");
    }

    public retrieveRootDataQualityReport(rootDrInstanceIdList: Array<String>): Observable<GroupedDataQualityReport>
    {
        const input = { '@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT$P_ROOT_DR_INSTANCE_ID_LIST': rootDrInstanceIdList };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                let dqReport = new GroupedDataQualityReport();

                dqReport.fromJson(response['@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT$P_DATA_QUALITY_REPORT']);
                return dqReport;
            });
    }

    public retrieveDataQualityReportGroupedByDrInstance(rootDrInstanceId: string): Observable<GroupedDataQualityReport>
    {
        const input = 
        { 
            '@OP_API_DQ_RETRIEVE_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE$P_ROOT_DR_INSTANCE_ID': rootDrInstanceId, 
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_API_DQ_RETRIEVE_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                let dqReport = new GroupedDataQualityReport();

                dqReport.fromJson(response['@OP_API_DQ_RETRIEVE_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE$P_DATA_QUALITY_REPORT']);
                return dqReport;
            });
    }

    public retrieveRootDataQualityReportGroupedByDrInstance(rootDrInstanceId: string, groupByTargetDrInstanceId: string, groupByTargetPropertyId: string): Observable<GroupedDataQualityReport>
    {
        const input = 
        { 
            '@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE$P_ROOT_DR_INSTANCE_ID': rootDrInstanceId, 
            '@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE$P_GROUP_BY_TARGET_DR_INSTANCE_ID': groupByTargetDrInstanceId, 
            '@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE$P_GROUP_BY_TARGET_PROPERTY_ID': groupByTargetPropertyId, 
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                let dqReport = new GroupedDataQualityReport();

                dqReport.fromJson(response['@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_DR_INSTANCE$P_DATA_QUALITY_REPORT']);
                return dqReport;
            });
    }

    public retrieveRootDataQualityReportGroupedByValueConcept(rootDrInstanceId: string, groupByTargetDrInstanceId: string, groupByTargetPropertyId: string): Observable<GroupedDataQualityReport>
    {
        const input = 
        { 
            '@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT$P_ROOT_DR_INSTANCE_ID': rootDrInstanceId, 
            '@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT$P_GROUP_BY_TARGET_DR_INSTANCE_ID': groupByTargetDrInstanceId, 
            '@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT$P_GROUP_BY_TARGET_PROPERTY_ID': groupByTargetPropertyId, 
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                let dqReport = new GroupedDataQualityReport();

                dqReport.fromJson(response['@OP_API_DQ_RETRIEVE_ROOT_DATA_QUALITY_REPORT_GROUPED_BY_VALUE_CONCEPT$P_DATA_QUALITY_REPORT']);
                return dqReport;
            });
    }
}
