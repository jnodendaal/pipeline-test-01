import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { TimeUnit } from '../../core/model/time.model';
import { FileDetails } from '../../core/model/file.model';
import 'rxjs/add/operator/map'
 
@Injectable()
export class FileManagerService 
{
    constructor(private http: HttpClient) 
    {
        console.log("FileManagerService");
    }

    public openFileContext(contextIid: string, contextId: string, expirationTime: TimeUnit): Observable<boolean>
    {
        const input = 
        { 
            '@OP_OPEN_FILE_CONTEXT$P_CONTEXT_IID': contextIid, 
            '@OP_OPEN_FILE_CONTEXT$P_CONTEXT_ID': contextId,
            '@OP_OPEN_FILE_CONTEXT$P_EXPIRATION_TIME': expirationTime
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_OPEN_FILE_CONTEXT', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                console.log(response);
                return response['@OP_OPEN_FILE_CONTEXT$P_SUCCESS'];
            });
    }

    public closeFileContext(contextIid: string): Observable<boolean>
    {
        const input = 
        { 
            '@OP_CLOSE_FILE_CONTEXT$P_CONTEXT_IID': contextIid
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_CLOSE_FILE_CONTEXT', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                console.log(response);
                return response['@OP_CLOSE_FILE_CONTEXT$P_SUCCESS'];
            });
    }

    public createDirectory(contextIid: string, filePath: string): Observable<boolean>
    {
        const input = 
        { 
            '@OP_CREATE_DIRECTORY$P_CONTEXT_IID': contextIid,
            '@OP_CREATE_DIRECTORY$P_FILE_PATH': filePath
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_CREATE_DIRECTORY', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                console.log(response);
                return response['@OP_CREATE_DIRECTORY$P_SUCCESS'];
            });
    }

    public deleteFile(contextIid: string, filePath: string): Observable<boolean>
    {
        const input = 
        { 
            '@OP_DELETE_FILE$P_CONTEXT_IID': contextIid,
            '@OP_DELETE_FILE$P_FILE_PATH': filePath
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_DELETE_FILE', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                console.log(response);
                return response['@OP_DELETE_FILE$P_SUCCESS'];
            });
    }

    public renameFile(contextIid: string, filePath: string, newFilename: string): Observable<boolean>
    {
        const input = 
        { 
            '@OP_RENAME_FILE$P_CONTEXT_IID': contextIid,
            '@OP_RENAME_FILE$P_FILE_PATH': filePath,
            '@OP_RENAME_FILE$P_FILE_NAME': newFilename
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_RENAME_FILE', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                console.log(response);
                return response['@OP_RENAME_FILE$P_SUCCESS'];
            });
    }

    public uploadFileData(contextIid: string, filePath: string, binaryData: string): Observable<number>
    {
        const input = 
        { 
            '@OP_UPLOAD_FILE_DATA$P_CONTEXT_IID': contextIid, 
            '@OP_UPLOAD_FILE_DATA$P_FILE_PATH': filePath,
            '@OP_UPLOAD_FILE_DATA$P_BINARY_DATA': binaryData
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_UPLOAD_FILE_DATA', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                console.log(response);
                return response['@OP_UPLOAD_FILE_DATA$P_BYTE_SIZE'];
            });
    }
}
