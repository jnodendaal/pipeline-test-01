import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { TimeUnit } from '../../core/model/time.model';
import { UserDetails } from '../../core/model/user.model';
import { IdUtility } from '../../core/utility/id.utility';
import 'rxjs/add/operator/map'
 
@Injectable()
export class UserManagerService 
{
    private userDataFileContextId: string;
    
    // Because of the fact that browsers cache images, this id is used to make image urls unique when the underlying data on the server as been updated.
    // This forces the browser to recache the image, thus using the latest version even though the actual url has not changed.
    private cacheBreaker: string;

    constructor(private http: HttpClient) 
    {
        console.log("UserManagerService");
        this.userDataFileContextId = "@FILE_CONTEXT_USER_DATA"; // TODO: Move to proper config settings.
        this.cacheBreaker = IdUtility.newId();
    }

    public getUserDataUrl(userId: string): string
    {
        return ApplicationSettings.API_URL + '/api/file/' + this.userDataFileContextId + '/' + userId + '/profile_picture.jpg?' + this.cacheBreaker;
    }

    public getUserProfilePictureUrl(userId: string): string
    {
        return ApplicationSettings.API_URL + '/api/file/' + this.userDataFileContextId + '/' + userId + '/profile_picture.jpg?' + this.cacheBreaker;
    }

    public getUserProfileThumbnailUrl(userId: string): string
    {
        return ApplicationSettings.API_URL + '/api/file/' + this.userDataFileContextId + '/' + userId + '/profile_thumbnail.jpg?' + this.cacheBreaker;
    }

    public changeProfilePicture(fileContextIid: string, filePath: string): Observable<boolean>
    {
        const input = 
        { 
            '@OP_USR_CHANGE_PROFILE_PICTURE$P_FILE_CONTEXT_IID': fileContextIid, 
            '@OP_USR_CHANGE_PROFILE_PICTURE$P_FILE_PATH': filePath
        };
        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_USR_CHANGE_PROFILE_PICTURE', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                console.log(response);
                
                // Reset the cache breaker to ensure that changes to server-side pictures are reflected.
                this.cacheBreaker = IdUtility.newId();
                return true;
            });
    }
}
