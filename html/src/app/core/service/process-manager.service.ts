import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RequestOptions, ResponseContentType } from '@angular/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { ProcessSearchResults } from '../model/process.model';
import { Context } from '../../core/model/security.model';
import 'rxjs/add/operator/map'
 
@Injectable()
export class ProcessManagerService 
{
    constructor(private http: HttpClient) 
    {
        console.log("ProcessManagerService");
    }

    private getContextParameters(context: Context)
    {
        return new HttpParams().set('functionality_iid', context.functionalityIid);
    }

    public searchProcesses(context: Context, searchExpression: string, pageOffset: number, pageSize: number): Observable<ProcessSearchResults>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '#OP_SEARCH_PROCESSES$P_SEARCH_EXPRESSION': searchExpression, 
            '#OP_SEARCH_PROCESSES$P_PAGE_OFFSET': pageOffset,
            '#OP_SEARCH_PROCESSES$P_PAGE_SIZE': pageSize
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('#OP_SEARCH_PROCESSES'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                let searchResults = new ProcessSearchResults();

                console.log(response);
                searchResults.pageOffset = pageOffset;
                searchResults.searchExpression = searchExpression;
                searchResults.searchTerms = response['#OP_SEARCH_PROCESSES$P_SEARCH_TERMS'];
                searchResults.addProcessesFromJson(response['#OP_SEARCH_PROCESSES$P_PROCESSES']);
                console.log(searchResults);
                return searchResults;
            });
    }

    public finalizeProcess(context: Context, processIid:string): Observable<boolean>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '@OP_PRC_FINALIZE_PROCESS$P_PROCESS_IID': processIid
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('@OP_PRC_FINALIZE_PROCESS'), JSON.stringify(input), { withCredentials: true, params: this.getContextParameters(context) })
            .map((response: Response) => 
            {
                return response['@OP_PRC_FINALIZE_PROCESS$P_SUCCESS'];
            });
    }
}
