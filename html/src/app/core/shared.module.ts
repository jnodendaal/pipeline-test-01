import { NgModule }                             from '@angular/core';
import { CommonModule }                         from '@angular/common';
import { FormsModule }                          from '@angular/forms';
import { RouterModule }                         from '@angular/router';
import { TypeaheadModule }                      from 'ngx-bootstrap/typeahead';
import { ModalModule }                          from 'ngx-bootstrap/modal';
import { TabsModule }                           from 'ngx-bootstrap/tabs';
import { BsDropdownModule }                     from 'ngx-bootstrap/dropdown';
import { ProgressbarModule }                    from 'ngx-bootstrap/progressbar';
import { LoadingIndicatorComponent }            from './component/loading-indicator/loading-indicator.component';
import { FunctionalityAccessComponent }         from './component/functionality-access/functionality-access.component';
import { FunctionalityCardMenuComponent }       from './component/functionality-card-menu/functionality-card-menu.component';
import { FunctionalityListMenuComponent }       from './component/functionality-list-menu/functionality-list-menu.component';
import { RouteViewComponent }                   from './utility/route-view.directive';
import { TaskListComponent }                    from './component/task-list/task-list.component';
import { ReportListComponent }                  from './component/report-list/report-list.component';
import { ProcessListComponent }                 from './component/process-list/process-list.component';
import { WorkflowClientComponent }              from './component/workflow-client/workflow-client.component';
import { BreadcrumbComponent }                  from './component/breadcrumb/breadcrumb.component';
import { throwIfAlreadyLoaded }                 from './utility/angular.utility';
import { HighlightTextDirective }               from './utility/highlight-text.directive';
import { MatchClassHeightDirective }            from './utility/match-class-height.directive';
import { RouteViewDirective }                   from './utility/route-view.directive';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    ModalModule,
    RouterModule,
    TypeaheadModule,
    TabsModule,
    BsDropdownModule,
    ProgressbarModule
  ],
  declarations: 
  [ 
    LoadingIndicatorComponent,
    FunctionalityAccessComponent,
    FunctionalityCardMenuComponent,
    FunctionalityListMenuComponent,
    BreadcrumbComponent,
    TaskListComponent,
    ReportListComponent,
    ProcessListComponent,
    WorkflowClientComponent,
    RouteViewComponent,
    HighlightTextDirective,
    MatchClassHeightDirective,
    RouteViewDirective
  ],
  entryComponents: 
  [
    RouteViewComponent
  ],
  exports:      
  [ 
    ModalModule,
    TypeaheadModule,
    TabsModule,
    BsDropdownModule,
    ProgressbarModule,
    LoadingIndicatorComponent, 
    FunctionalityAccessComponent,
    FunctionalityCardMenuComponent,
    FunctionalityListMenuComponent,
    BreadcrumbComponent,
    TaskListComponent,
    ReportListComponent,
    ProcessListComponent,
    WorkflowClientComponent,
    RouteViewComponent,
    HighlightTextDirective,
    MatchClassHeightDirective,
    RouteViewDirective
  ],
  // If any providers are listed, make sure that they are not Singleton, 
  // as these providers will be created each time this module is imported in a lazy-loaded feature module.
  providers:    [ ] 
})
export class SharedModule 
{
    /**
     * This module contains all shared components to be used by other modules in the system.
     * No singletons should be included in this module as their proper place is in the CoreModule.
     * An instance of this module (with its own DI sub-tree) is created whenever imported by a lazy-loaded feature module.
     */
  constructor() 
  {
  }
}