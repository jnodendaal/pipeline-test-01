"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var data_table_module_1 = require("./components/data-table/data-table.module");
var loading_indicator_component_1 = require("./components/loading-indicator/loading-indicator.component");
var functionality_card_menu_component_1 = require("./components/functionality-card-menu/functionality-card-menu.component");
var highlight_text_directive_1 = require("./utilities/highlight-text.directive");
var SharedModule = (function () {
    /**
     * This module contains all shared components to be used by other modules in the system.
     * No singletons should be included in this module as their proper place is in the CoreModule.
     * An instance of this module (with its own DI sub-tree) is created whenever imported by a lazy-loaded feature module.
     */
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ngx_bootstrap_1.Ng2BootstrapModule,
            data_table_module_1.DataTableModule
        ],
        declarations: [
            loading_indicator_component_1.LoadingIndicatorComponent,
            functionality_card_menu_component_1.FunctionalityCardMenuComponent,
            highlight_text_directive_1.HighlightTextDirective
        ],
        exports: [
            data_table_module_1.DataTableModule,
            loading_indicator_component_1.LoadingIndicatorComponent,
            functionality_card_menu_component_1.FunctionalityCardMenuComponent,
            highlight_text_directive_1.HighlightTextDirective
        ],
        // If any providers are listed, make sure that they are not Singleton, 
        // as these providers will be created each time this module is imported in a lazy-loaded feature module.
        providers: []
    }),
    __metadata("design:paramtypes", [])
], SharedModule);
exports.SharedModule = SharedModule;
//# sourceMappingURL=shared.module.js.map