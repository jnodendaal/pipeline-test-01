export class DataEntity
{
    id: string;

    public fromJson(jsonEntity: any): DataEntity
    {
        for (let key in jsonEntity)
        {
            this[key] = jsonEntity[key];
        }

        return this;
    }

    public getField(fieldId: string): any
    {
        // If an id with namespace is required, remove the namespace as fields are stored without one.
        if (!fieldId.startsWith("$"))
        {
            fieldId = fieldId.substring(this.id.length);
        }

        return this[fieldId];
    }
}