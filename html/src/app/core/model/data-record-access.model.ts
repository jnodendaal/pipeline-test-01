/*
    Data Record Access Layer Model
*/
export class RecordAccessLayer
{
    editable: boolean;
    visible: boolean;
    fftEditable: boolean;
    parentAccessLayer: RecordAccessLayer;
    sectionAccessLayers: SectionAccessLayer[];
    classificationAccessLayers: ClassificationAccessLayer[];
    particleSettingsAccessLayers: ParticleSettingsAccessLayer[];

    constructor()
    {
        this.sectionAccessLayers = new Array();
        this.classificationAccessLayers = new Array();
        this.particleSettingsAccessLayers = new Array();
    }

    public fromJson(jsonRecord: any): RecordAccessLayer
    {
        this.editable = jsonRecord.editable;
        this.visible = jsonRecord.visible;
        this.fftEditable = jsonRecord.fftEditable;
        
        if (jsonRecord.sections)
        {
            for (let jsonSection of jsonRecord.sections)
            {
                this.addSectionAccessLayer(new SectionAccessLayer().fromJson(jsonSection));
            }
        }

        return this;
    }

    public setParent(access: RecordAccessLayer)
    {
        this.parentAccessLayer = access;
    }

    public addSectionAccessLayer(access: SectionAccessLayer)
    {
        access.setParent(this);
        this.sectionAccessLayers.push(access);
    }

    public getSectionAccessLayer(sectionId: string): SectionAccessLayer
    {
        for (let sectionAccess of this.sectionAccessLayers)
        {
            if (sectionAccess.sectionId == sectionId)
            {
                return sectionAccess;
            }
        }

        return null;
    }

    public getPropertyAccessLayer(propertyId: string): PropertyAccessLayer
    {
        for (let sectionAccess of this.sectionAccessLayers)
        {
            let propertyAccess: PropertyAccessLayer;

            propertyAccess = sectionAccess.getPropertyAccessLayer(propertyId);
            if (propertyAccess) return propertyAccess;
        }

        return null;
    }
}

export class SectionAccessLayer
{
    sectionId: string;
    editable: boolean;
    visible: boolean;
    required: boolean;
    parentAccessLayer: RecordAccessLayer;
    propertyAccessLayers: PropertyAccessLayer[];

    constructor()
    {
        this.propertyAccessLayers = new Array();
    }

    public fromJson(jsonSection: any): SectionAccessLayer
    {
        this.sectionId = jsonSection.sectionId;
        this.editable = jsonSection.editable;
        this.visible = jsonSection.visible;
        this.required = jsonSection.required;

        if (jsonSection.properties)
        {
            for (let jsonProperty of jsonSection.properties)
            {
                this.addPropertyAccessLayer(new PropertyAccessLayer().fromJson(jsonProperty));
            }
        }

        return this;
    }

    public setParent(parent: RecordAccessLayer)
    {
        this.parentAccessLayer = parent;
    }

    public addPropertyAccessLayer(access: PropertyAccessLayer)
    {
        access.setParent(this);
        this.propertyAccessLayers.push(access);
    }

    public getPropertyAccessLayer(propertyId: string): PropertyAccessLayer
    {
        for (let propertyAccessLayer of this.propertyAccessLayers)
        {
            if (propertyAccessLayer.propertyId == propertyId)
            {
                return propertyAccessLayer;
            }
        }

        return null;
    }
}

export class PropertyAccessLayer
{
    propertyId: string;
    editable: boolean;
    visible: boolean;
    required: boolean;
    insertEnabled: boolean;
    updateEnabled: boolean;
    deleteEnabled: boolean;
    fftEditable: boolean;
    parentAccessLayer: SectionAccessLayer;

    constructor()
    {
    }

    public fromJson(jsonAccess: any): PropertyAccessLayer
    {
        this.propertyId = jsonAccess.propertyId;
        this.editable = jsonAccess.editable;
        this.visible = jsonAccess.visible;
        this.required = jsonAccess.required;
        this.insertEnabled = jsonAccess.insertEnabled;
        this.updateEnabled = jsonAccess.updateEnabled;
        this.deleteEnabled = jsonAccess.deleteEnabled;
        this.fftEditable = jsonAccess.fftEditable;
        return this;
    }

    public setParent(access: SectionAccessLayer)
    {
        this.parentAccessLayer = access;
    }
}

export class FieldAccessLayer
{
    fieldId: string;
    editable: boolean;
    visible: boolean;
    required: boolean;
    insertEnabled: boolean;
    updateEnabled: boolean;
    deleteEnabled: boolean;
    filterConceptIdSet: string[];
    filterConceptGroupIDSet: string[];
    parentAccessLayer: SectionAccessLayer;

    constructor()
    {
    }

    public fromJson(jsonAccess: any): FieldAccessLayer
    {
        this.fieldId = jsonAccess.fieldId;
        this.editable = jsonAccess.editable;
        this.visible = jsonAccess.visible;
        this.required = jsonAccess.required;
        this.insertEnabled = jsonAccess.insertEnabled;
        this.updateEnabled = jsonAccess.updateEnabled;
        this.deleteEnabled = jsonAccess.deleteEnabled;
        return this;
    }
}

export abstract class ValueAccessLayer
{
}

export class ClassificationAccessLayer
{
}

export class ParticleSettingsAccessLayer
{
}