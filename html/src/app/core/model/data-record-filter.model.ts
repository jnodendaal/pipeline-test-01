import { DataFilterCriteria, DataFilterClause, DataFilterOperator } from "./data-filter.model";
import { TerminologySet } from './terminology.model';

export class RecordFilterCriteria extends DataFilterCriteria implements DataFilterClause
{
    isAllowCriteriaAddition: boolean;
    isAllowSubCriteriaAddition: boolean;
    fieldId: string;

    constructor(id: string, conjunction: string)
    {
        super(id, conjunction);
        this.type = "@RECORD_FILTER_CRITERIA";
    }

    public copy(): RecordFilterCriteria
    {
        let copy: RecordFilterCriteria;

        copy = new RecordFilterCriteria(this.id, this.conjunction);
        copy.id = this.id;
        copy.fieldId = this.fieldId;
        copy.conjunction = this.conjunction;
        copy.isAllowCriteriaAddition = this.isAllowCriteriaAddition;
        copy.isAllowSubCriteriaAddition = this.isAllowSubCriteriaAddition;
        for (let clause of this.criteria)
        {
            this.addClause(clause.copy());            
        }

        return copy;
    }

    public static fromJson(jsonCriteria: any): RecordFilterCriteria
    {
        let item: RecordFilterCriteria;
        let clauses: DataFilterClause[];
       
        item = new RecordFilterCriteria(jsonCriteria.id, jsonCriteria.conjunction);
        item.fieldId = jsonCriteria.fieldId;
        item.isAllowCriteriaAddition = jsonCriteria.isAllowCriteriaAddition;
        item.isAllowSubCriteriaAddition = jsonCriteria.isAllowSubCriteriaAddition;
        
        clauses = jsonCriteria.criteria;
        for (let key = 0; key < clauses.length; key++)
        {
            let criteria: any;
            let type: String;
            
            criteria = clauses[key];
            type = criteria.type;
            
            if ('@DR_FILTER_CRITERIA' == type)
            {
                item.criteria.push(DrFilterCriteria.fromJson(criteria));
            }
            else if ('@DRI_FILTER_CRITERIA' == type)
            {
                item.criteria.push(DrInstanceFilterCriteria.fromJson(criteria));
            }
        }
        return item;
    }

    public getDisplayString(terminology: TerminologySet): string
    {
        let displayString: string;

        displayString = "";
        for (let clause of this.criteria)
        {
            if (clause.hasCriteria())
            {
                if (clause instanceof DrInstanceFilterCriteria)
                {
                    let stringValue: string;

                    stringValue = clause.getDisplayString(terminology);
                    if (stringValue)
                    {
                        if (displayString) displayString += ", ";
                        displayString += stringValue;
                    } 
                }
                else if (clause instanceof DrFilterCriteria)
                {
                    let stringValue: string;

                    stringValue = clause.getDisplayString(terminology);
                    if (stringValue)
                    {
                        if (displayString) displayString += ", ";
                        displayString += stringValue;
                    } 
                }
            }
        }

        return displayString;
    }
}

export class DrInstanceFilterCriteria implements DataFilterClause
{
    id: string;
    conjunction: string;
    type: string;
    drInstanceId: string;
    displayName: string;
    isAllowCodeAddition: Boolean;
    isAllowPropertyAddition: Boolean;
    isAllowHistoryAddition: Boolean;
    isAllowRemoval: Boolean;
    codeCriteria: CodeFilterCriterion[];
    propertyCriteria: PropertyFilterCriterion[];
    organizationCriteria: OrganizationFilterCriterion[];

    constructor()
    {
        this.codeCriteria = new Array();
        this.propertyCriteria = new Array();
        this.organizationCriteria = new Array();
    }

    public copy(): DrInstanceFilterCriteria
    {
        let copy: DrInstanceFilterCriteria;

        copy = new DrInstanceFilterCriteria();
        copy.id = this.id;
        copy.conjunction = this.conjunction;
        copy.type = this.type;
        copy.drInstanceId = this.drInstanceId;
        copy.displayName = this.displayName;
        copy.isAllowCodeAddition = this.isAllowCodeAddition;
        copy.isAllowHistoryAddition = this.isAllowHistoryAddition;
        copy.isAllowPropertyAddition = this.isAllowPropertyAddition;
        copy.isAllowRemoval = this.isAllowRemoval;

        for (let code of this.codeCriteria)
        {
            this.codeCriteria.push(code.copy());
        }

        for (let property of this.propertyCriteria)
        {
            copy.propertyCriteria.push(property.copy());
        }

        for (let organization of this.organizationCriteria)
        {
            copy.organizationCriteria.push(organization.copy());
        }

        return copy;
    }

    public static fromJson(jsonItem: any): DrInstanceFilterCriteria
    {
        let item: DrInstanceFilterCriteria;
        let codeCriteriaList: any[];
        let propertyCriteriaList: any[];
        let organizationCriteriaList: any[];

        item = new DrInstanceFilterCriteria();
        item.conjunction = jsonItem.conjunction;
        item.type = jsonItem.type;
        item.drInstanceId = jsonItem.drInstanceId;
        item.displayName = jsonItem.displayName;
        item.isAllowCodeAddition = jsonItem.isAllowCodeAddition;
        item.isAllowPropertyAddition = jsonItem.isAllowPropertyAddition;
        item.isAllowHistoryAddition = jsonItem.isAllowHistoryAddition;
        item.isAllowRemoval = jsonItem.isAllowRemoval;

        codeCriteriaList = jsonItem.codeCriteria;
        for (let key = 0; key < codeCriteriaList.length; key++)
        {
            item.codeCriteria.push(CodeFilterCriterion.fromJson(codeCriteriaList[key]));
        }

        propertyCriteriaList = jsonItem.propertyCriteria;
        for (let key = 0; key < propertyCriteriaList.length; key++)
        {
            item.propertyCriteria.push(PropertyFilterCriterion.fromJson(propertyCriteriaList[key]));
        }

        organizationCriteriaList = jsonItem.organizationCriteria;
        for (let key = 0; key < organizationCriteriaList.length; key++)
        {
            item.organizationCriteria.push(OrganizationFilterCriterion.fromJson(organizationCriteriaList[key]));
        }
        return item;
    }

    public hasCriteria(): boolean
    {
        for (let propertyCriterion of this.propertyCriteria)
        {
            if (propertyCriterion.hasCriteria()) return true;
        }

        // for (let historyCriterion of this.historyCriteria)
        // {
        //     if (historyCriterion.hasCriterion()) return true;
        // }

        for (let codeCriterion of this.codeCriteria)
        {
            if (codeCriterion.hasCriteria()) return true;
        }

        for (let organizationCriterion of this.organizationCriteria)
        {
            if (organizationCriterion.hasCriteria()) return true;
        }

        return false;        
    }

    public getDisplayString(terminology: TerminologySet): string
    {
        let displayString: string;

        displayString = "";
        for (let propertyCriterion of this.propertyCriteria)
        {
            if (propertyCriterion.hasCriteria())
            {
                let stringValue: string;

                stringValue = propertyCriterion.getDisplayString(terminology);
                if (stringValue)
                {
                    if (displayString) displayString += ", ";
                    displayString += stringValue;
                }  
            }
        }

        for (let codeCriterion of this.codeCriteria)
        {
            if (codeCriterion.hasCriteria())
            {
                let stringValue: string;

                stringValue = codeCriterion.getDisplayString(terminology);
                if (stringValue)
                {
                    if (displayString) displayString += ", ";
                    displayString += stringValue;
                }  
            }
        }

        for (let organizationCriterion of this.organizationCriteria)
        {
            if (organizationCriterion.hasCriteria())
            {
                let stringValue: string;

                stringValue = organizationCriterion.getDisplayString(terminology);
                if (stringValue)
                {
                    if (displayString) displayString += ", ";
                    displayString += stringValue;
                }   
            } 
        }

        return displayString;
    }
}

export class DrFilterCriteria implements DataFilterClause
{
    id: string;
    conjunction: string;
    type: string;
    drId: string;
    displayName: string;
    isAllowCodeAddition: Boolean;
    isAllowPropertyAddition: Boolean;
    isAllowHistoryAddition: Boolean;
    isAllowRemoval: Boolean;
    isAllowInstanceSelection: Boolean;
    instanceLabel: string;
    instanceOperator: string;
    instanceValue: any;
    codeCriteria: CodeFilterCriterion[];
    propertyCriteria: PropertyFilterCriterion[];
    organizationCriteria: OrganizationFilterCriterion[];

    constructor()
    {
        this.codeCriteria = new Array();
        this.propertyCriteria = new Array();
        this.organizationCriteria = new Array();
    }

    public copy(): DrFilterCriteria
    {
        let copy: DrFilterCriteria;

        copy = new DrFilterCriteria();
        copy.conjunction = this.conjunction;
        copy.type = this.type;
        copy.drId = this.drId;
        copy.displayName = this.displayName;
        copy.isAllowCodeAddition = this.isAllowCodeAddition;
        copy.isAllowPropertyAddition = this.isAllowPropertyAddition;
        copy.isAllowHistoryAddition = this.isAllowHistoryAddition;
        copy.isAllowRemoval = this.isAllowRemoval;
        copy.instanceLabel = this.instanceLabel;
        copy.instanceOperator = this.instanceOperator;
        copy.instanceValue  = this.instanceValue;
        
        for (let code of this.codeCriteria)
        {
            copy.codeCriteria.push(code.copy());
        }

        for (let property of this.propertyCriteria)
        {
            copy.propertyCriteria.push(property.copy());
        }

        for (let organization of this.organizationCriteria)
        {
            copy.organizationCriteria.push(organization.copy());
        }

        return copy;
    }

    public static fromJson(jsonItem: any): DrFilterCriteria
    {
        let item: DrFilterCriteria;
        let codeCriteriaList: any[];
        let propertyCriteriaList: any[];
        let organizationCriteriaList: any[];
        
        item = new DrFilterCriteria();
        item.conjunction = jsonItem.conjunction;
        item.type = jsonItem.type;
        item.drId = jsonItem.drId;
        item.displayName = jsonItem.displayName;
        item.isAllowCodeAddition = jsonItem.isAllowCodeAddition;
        item.isAllowPropertyAddition = jsonItem.isAllowPropertyAddition;
        item.isAllowHistoryAddition = jsonItem.isAllowHistoryAddition;
        item.isAllowRemoval = jsonItem.isAllowRemoval;
        item.isAllowInstanceSelection = jsonItem.isAllowInstanceSelection;
        item.instanceLabel = jsonItem.instanceLabel;
        item.instanceOperator = jsonItem.instanceOperator;
        item.instanceValue = jsonItem.instanceValue;

        codeCriteriaList = jsonItem.codeCriteria;
        for (let key = 0; key < codeCriteriaList.length; key++)
        {
            item.codeCriteria.push(CodeFilterCriterion.fromJson(codeCriteriaList[key]));
        }

        propertyCriteriaList = jsonItem.propertyCriteria;
        for (let key = 0; key < propertyCriteriaList.length; key++)
        {
            item.propertyCriteria.push(PropertyFilterCriterion.fromJson(propertyCriteriaList[key]));
        }

        organizationCriteriaList = jsonItem.organizationCriteria;
        for (let key = 0; key < organizationCriteriaList.length; key++)
        {
            item.organizationCriteria.push(OrganizationFilterCriterion.fromJson(organizationCriteriaList[key]));
        }
        return item;
    }

    public hasCriteria(): boolean
    {
        for (let propertyCriterion of this.propertyCriteria)
        {
            if (propertyCriterion.hasCriteria()) return true;
        }

        // for (let historyCriterion of this.historyCriteria)
        // {
        //     if (historyCriterion.hasCriterion()) return true;
        // }

        for (let codeCriterion of this.codeCriteria)
        {
            if (codeCriterion.hasCriteria()) return true;
        }

        for (let organizationCriterion of this.organizationCriteria)
        {
            if (organizationCriterion.hasCriteria()) return true;
        }

        return false;        
    }

    public getDisplayString(terminology: TerminologySet): string
    {
        let displayString: string;

        displayString = "";
        for (let propertyCriterion of this.propertyCriteria)
        {
            if (propertyCriterion.hasCriteria())
            {
                let stringValue: string;

                stringValue = propertyCriterion.getDisplayString(terminology);
                if (stringValue)
                {
                    if (displayString) displayString += ", ";
                    displayString += stringValue;
                }  
            }
        }

        for (let codeCriterion of this.codeCriteria)
        {
            if (codeCriterion.hasCriteria())
            {
                let stringValue: string;

                stringValue = codeCriterion.getDisplayString(terminology);
                if (stringValue)
                {
                    if (displayString) displayString += ", ";
                    displayString += stringValue;
                }  
            }
        }

        for (let organizationCriterion of this.organizationCriteria)
        {
            if (organizationCriterion.hasCriteria())
            {
                let stringValue: string;

                stringValue = organizationCriterion.getDisplayString(terminology);
                if (stringValue)
                {
                    if (displayString) displayString += ", ";
                    displayString += stringValue;
                }   
            } 
        }

        return displayString;
    }
}

export class OrganizationFilterCriterion
{
    label: string;
    orgTypeIds: string[];
    valueCriteria: ValueFilterCriterion;

    constructor()
    {
    }

    public copy(): OrganizationFilterCriterion
    {
        let copy: OrganizationFilterCriterion;

        copy = new OrganizationFilterCriterion();
        copy.label = this.label;
        copy.orgTypeIds.push(...this.orgTypeIds);
        copy.valueCriteria = this.valueCriteria.copy();
        return copy;
    }

    public static fromJson(jsonItem: any): OrganizationFilterCriterion
    {
        let item: OrganizationFilterCriterion;
        let valueCriteria: any;

        item = new OrganizationFilterCriterion();
        item.label = jsonItem.label;
        item.orgTypeIds = jsonItem.orgTypeIds

        valueCriteria = jsonItem.valueCriteria;
        item.valueCriteria = ValueFilterCriterion.fromJson(valueCriteria);
        return item;
    }

    public hasCriteria(): boolean
    {
        if (this.valueCriteria)
        {
            return this.valueCriteria.hasCriteria();
        }
        else return false;
    }

    public getDisplayString(terminology: TerminologySet): string
    {
        return this.valueCriteria.getDisplayString(terminology);
    }
}

export class PropertyFilterCriterion
{
    propertyId: string;
    fieldId: string;
    isAllowRemoval: Boolean;
    valueCriteria: ValueFilterCriterion[];

    constructor()
    {
        this.valueCriteria = new Array();
    }

    public copy(): PropertyFilterCriterion
    {
        let copy: PropertyFilterCriterion;

        copy = new PropertyFilterCriterion();
        copy.propertyId = this.propertyId;
        copy.fieldId = this.fieldId;
        copy.isAllowRemoval = this.isAllowRemoval;
        
        for (let value of this.valueCriteria)
        {
            copy.valueCriteria.push(value.copy());
        }

        return copy;
    }

    public static fromJson(jsonItem: any): PropertyFilterCriterion
    {
        let item: PropertyFilterCriterion;
        let valueCriteria: any;

        item = new PropertyFilterCriterion();
        item.propertyId = jsonItem.propertyId;
        item.fieldId = jsonItem.fieldId;
        item.isAllowRemoval = jsonItem.isAllowRemoval;

        valueCriteria = jsonItem.valueCriteria;
        for (let key = 0; key < valueCriteria.length; key++)
        {
            item.valueCriteria.push(ValueFilterCriterion.fromJson(valueCriteria[key]));
        }
        return item;
    }

    public hasCriteria(): boolean
    {
        for (let valueCriterion of this.valueCriteria)
        {
            if (valueCriterion.hasCriteria()) return true;
        }
        
        return false;
    }

    public getDisplayString(terminology: TerminologySet): string
    {
        let displayString: string;

        displayString = "";
        for (let valueCriterion of this.valueCriteria)
        {
            if (valueCriterion.hasCriteria())
            {
                displayString += terminology.getTerm(this.propertyId) + " " + valueCriterion.getDisplayString(terminology);
            } 
        }

        return displayString;
    }
}

export class CodeFilterCriterion
{
    codeTypeId: string;
    target: string;
    isAllowRemoval: Boolean;
    valueCriteria: ValueFilterCriterion;

    constructor()
    {
    }

    public copy()
    {
        let copy: CodeFilterCriterion;

        copy = new CodeFilterCriterion();
        copy.codeTypeId = this.codeTypeId;
        copy.target = this.target;
        copy.isAllowRemoval = this.isAllowRemoval;
        copy.valueCriteria = this.valueCriteria.copy();
        return copy;
    }

    public static fromJson(jsonItem: any): CodeFilterCriterion
    {
        let item: CodeFilterCriterion;
        let valueCriteria: any;
        
        item = new CodeFilterCriterion();
        item.codeTypeId = jsonItem.codeTypeId;
        item.target = jsonItem.target;
        item.isAllowRemoval = jsonItem.isAllowRemoval;

        valueCriteria = jsonItem.valueCriteria;
        item.valueCriteria = ValueFilterCriterion.fromJson(valueCriteria);
        return item;
    }

    public hasCriteria(): boolean
    {
        if (this.valueCriteria)
        {
            return this.valueCriteria.hasCriteria();
        }
        else return false;
    }

    public getDisplayString(terminology: TerminologySet): string
    {
        let displayString: string;

        displayString = "";
        if (this.valueCriteria.hasCriteria())
        {
            displayString += terminology.getTerm(this.codeTypeId) + " " + this.valueCriteria.getDisplayString(terminology);
        } 

        return displayString;
    }
}

export class ValueFilterCriterion
{
    type: string;
    operator: string;
    value: any;

    constructor()
    {
    }

    public copy(): ValueFilterCriterion
    {
        let copy: ValueFilterCriterion;

        copy = new ValueFilterCriterion();
        copy.type = this.type;
        copy.operator = this.operator;
        copy.value = this.value;
        return copy;
    }

    public static fromJson(jsonItem: any): ValueFilterCriterion
    {
        let item: ValueFilterCriterion;
        
        item = new ValueFilterCriterion();
        item.type = jsonItem.type;
        item.operator = jsonItem.operator;
        item.value = jsonItem.value;
        return item;
    }

    public hasCriteria(): boolean
    {
        if (this.operator == "IS_NULL") return true; // This operator does not require a filter value.
        else if (this.operator == "IS_NOT_NULL") return true; // This operator does not require a filter value.
        else if (this.value == null) return false; // All other operators require a filter value.
        else if (this.value instanceof String) // If the filter value is a String type, make sure it is not empty.
        {
            return this.value.trim().length > 0;
        }
        else return true;
    }

    public getDisplayString(terminology: TerminologySet): string
    {
        return DataFilterOperator.getOperatorName(this.operator) + " \"" + this.value + "\"";
    }
}

export interface ValueType 
{
    clearSelectedValue() : void;
    getValueCriterion(): ValueFilterCriterion;
    addTerminology(terminology : TerminologySet): void;
}