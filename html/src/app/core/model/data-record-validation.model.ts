
/*
    Data Record Validation Model
*/
export class PropertyValidationError
{
    message: string;

    constructor(message: string)
    {
        this.message = message;
    }
}

export class PropertyValidationWarning
{
    message: string;

    constructor(message: string)
    {
        this.message = message;
    }
}
