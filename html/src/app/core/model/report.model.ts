export class Report
{
    iid: string;
    id: string;
    name: string;
    description: string;
    thumbnailImageId: string;
    generationStartTime: number;
    generationEndTime: number;
    expirationTime: number;
    fileName: string;
    fileContextId: string;

    constructor()
    {
    }

    public static fromJson(jsonReport: any): Report
    {
        let newReport: Report;

        newReport = new Report();
        newReport.iid = jsonReport.iid;
        newReport.name = jsonReport.name;
        newReport.description = jsonReport.description;
        newReport.thumbnailImageId = jsonReport.thumbnailImageId;
        newReport.generationStartTime = jsonReport.generationStartTime;
        newReport.generationEndTime = jsonReport.generationEndTime;
        newReport.expirationTime = jsonReport.expirationTime;
        newReport.fileName = jsonReport.fileName;
        newReport.fileContextId = jsonReport.fileContextId;
        return newReport;
    }
}

export class ReportSearchResults
{
    searchExpression: string;
    searchTerms: string;
    pageOffset: number;
    reports: Report[];

    constructor()
    {
        this.reports = new Array();
    }

    public addReportsFromJson(jsonReports: any)
    {
        if (jsonReports)
        {
            for (let jsonReport of jsonReports)
            {
                this.reports.push(Report.fromJson(jsonReport));
            }
        }
    }
}