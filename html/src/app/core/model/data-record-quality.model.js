"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GroupedDataQualityReport = (function () {
    function GroupedDataQualityReport() {
    }
    GroupedDataQualityReport.prototype.fromJson = function (jsonReport) {
        this.groups = new Array();
        for (var _i = 0, _a = jsonReport.groups; _i < _a.length; _i++) {
            var jsonGroup = _a[_i];
            var group = new DataQualityMetricsGroup();
            group.fromJson(jsonGroup);
            this.groups.push(group);
        }
    };
    GroupedDataQualityReport.prototype.getSummary = function () {
        var summary;
        summary = new DataQualityMetricsSummary();
        summary.recordCount = this.getRecordCount();
        summary.averagePropertyCount = this.getAveragePropertyCount();
        summary.averagePropertyDataCount = this.getAveragePropertyDataCount();
        summary.averageCharacteristicCount = this.getAverageCharacteristicCount();
        summary.averageCharacteristicDataCount = this.getAverageCharacteristicDataCount();
        summary.characteristicCompleteness = this.getCharacteristicCompleteness();
        summary.characteristicValidity = this.getCharacteristicValidity();
        summary.nonCharacteristicCompleteness = this.getNonCharacteristicCompleteness();
        summary.nonCharacteristicValidity = this.getNonCharacteristicValidity();
        return summary;
    };
    GroupedDataQualityReport.prototype.getAveragePropertyCount = function () {
        return this.getPropertyCount() / this.getRecordCount();
    };
    GroupedDataQualityReport.prototype.getAveragePropertyDataCount = function () {
        return this.getPropertyDataCount() / this.getRecordCount();
    };
    GroupedDataQualityReport.prototype.getAverageCharacteristicCount = function () {
        return this.getCharacteristicCount() / this.getRecordCount();
    };
    GroupedDataQualityReport.prototype.getAverageCharacteristicDataCount = function () {
        return this.getCharacteristicDataCount() / this.getRecordCount();
    };
    GroupedDataQualityReport.prototype.getCharacteristicCompleteness = function () {
        return this.getCharacteristicDataCount() / this.getCharacteristicCount() * 100;
    };
    GroupedDataQualityReport.prototype.getCharacteristicValidity = function () {
        return this.getCharacteristicValidCount() / this.getCharacteristicDataCount() * 100;
    };
    GroupedDataQualityReport.prototype.getNonCharacteristicCompleteness = function () {
        return this.getNonCharacteristicDataCount() / this.getNonCharacteristicCount() * 100;
    };
    GroupedDataQualityReport.prototype.getNonCharacteristicValidity = function () {
        return this.getNonCharacteristicValidCount() / this.getNonCharacteristicDataCount() * 100;
    };
    GroupedDataQualityReport.prototype.getRecordCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getRecordCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getPropertyCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getPropertyCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getPropertyDataCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getPropertyDataCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getCharacteristicCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getCharacteristicCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getCharacteristicDataCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getCharacteristicDataCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getCharacteristicValidCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getCharacteristicValidCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getNonCharacteristicCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getNonCharacteristicCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getNonCharacteristicDataCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getNonCharacteristicDataCount();
        }
        return count;
    };
    GroupedDataQualityReport.prototype.getNonCharacteristicValidCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.groups; _i < _a.length; _i++) {
            var group = _a[_i];
            count += group.metrics.getNonCharacteristicValidCount();
        }
        return count;
    };
    return GroupedDataQualityReport;
}());
exports.GroupedDataQualityReport = GroupedDataQualityReport;
var DataQualityMetricsGroup = (function () {
    function DataQualityMetricsGroup() {
    }
    DataQualityMetricsGroup.prototype.fromJson = function (jsonGroup) {
        this.id = jsonGroup.id;
        this.code = jsonGroup.code;
        this.term = jsonGroup.term;
        this.definition = jsonGroup.definition;
        this.ocId = jsonGroup.ocId;
        this.ocTerm = jsonGroup.ocTerm;
        this.metrics = new DataQualityMetrics();
        this.metrics.fromJson(jsonGroup.metrics);
    };
    return DataQualityMetricsGroup;
}());
exports.DataQualityMetricsGroup = DataQualityMetricsGroup;
var DataQualityMetrics = (function () {
    function DataQualityMetrics() {
    }
    DataQualityMetrics.prototype.fromJson = function (jsonMetrics) {
        this.recordCount = jsonMetrics.recordCount;
        this.propertyCount = jsonMetrics.propertyCount;
        this.propertyDataCount = jsonMetrics.propertyDataCount;
        this.propertyValidCount = jsonMetrics.propertyValidCount;
        this.propertyVerificationCount = jsonMetrics.propertyVerificationCount;
        this.propertyAccurateCount = jsonMetrics.propertyAccurateCount;
        this.characteristicCount = jsonMetrics.characteristicCount;
        this.characteristicDataCount = jsonMetrics.characteristicDataCount;
        this.characteristicValidCount = jsonMetrics.characteristicValidCount;
        this.characteristicVerificationCount = jsonMetrics.characteristicVerificationCount;
        this.characteristicAccurateCount = jsonMetrics.characteristicAccurateCount;
    };
    DataQualityMetrics.prototype.getSummary = function () {
        var summary;
        summary = new DataQualityMetricsSummary();
        summary.recordCount = this.getRecordCount();
        summary.averageCharacteristicCount = this.getAverageCharacteristicCount();
        summary.averageCharacteristicDataCount = this.getAverageCharacteristicDataCount();
        summary.characteristicCompleteness = this.getCharacteristicCompleteness();
        summary.characteristicValidity = this.getCharacteristicValidity();
        summary.nonCharacteristicCompleteness = this.getNonCharacteristicCompleteness();
        summary.nonCharacteristicValidity = this.getNonCharacteristicValidity();
        return summary;
    };
    DataQualityMetrics.prototype.getAverageCharacteristicCount = function () {
        return this.characteristicCount / this.recordCount;
    };
    DataQualityMetrics.prototype.getAverageCharacteristicDataCount = function () {
        return this.characteristicDataCount / this.recordCount;
    };
    DataQualityMetrics.prototype.getPropertyCompleteness = function () {
        return this.propertyDataCount / this.propertyCount * 100;
    };
    DataQualityMetrics.prototype.getCharacteristicCompleteness = function () {
        return this.characteristicDataCount / this.characteristicCount * 100;
    };
    DataQualityMetrics.prototype.getCharacteristicValidity = function () {
        return this.characteristicValidCount / this.characteristicDataCount * 100;
    };
    DataQualityMetrics.prototype.getNonCharacteristicCompleteness = function () {
        return this.getNonCharacteristicDataCount() / this.getNonCharacteristicCount() * 100;
    };
    DataQualityMetrics.prototype.getNonCharacteristicValidity = function () {
        return this.getNonCharacteristicValidCount() / this.getNonCharacteristicDataCount() * 100;
    };
    DataQualityMetrics.prototype.getRecordCount = function () {
        return this.recordCount;
    };
    DataQualityMetrics.prototype.setRecordCount = function (recordCount) {
        this.recordCount = recordCount;
    };
    DataQualityMetrics.prototype.getPropertyCount = function () {
        return this.propertyCount;
    };
    DataQualityMetrics.prototype.setPropertyCount = function (propertyCount) {
        this.propertyCount = propertyCount;
    };
    DataQualityMetrics.prototype.getPropertyDataCount = function () {
        return this.propertyDataCount;
    };
    DataQualityMetrics.prototype.setPropertyDataCount = function (propertyDataCount) {
        this.propertyDataCount = propertyDataCount;
    };
    DataQualityMetrics.prototype.getPropertyVerificationCount = function () {
        return this.propertyVerificationCount;
    };
    DataQualityMetrics.prototype.setPropertyVerificationCount = function (propertyVerificationCount) {
        this.propertyVerificationCount = propertyVerificationCount;
    };
    DataQualityMetrics.prototype.getPropertyAccurateCount = function () {
        return this.propertyAccurateCount;
    };
    DataQualityMetrics.prototype.setPropertyAccurateCount = function (propertyAccurateCount) {
        this.propertyAccurateCount = propertyAccurateCount;
    };
    DataQualityMetrics.prototype.getPropertyIncorrectCount = function () {
        return this.propertyDataCount - this.propertyAccurateCount;
    };
    DataQualityMetrics.prototype.getPropertyValidCount = function () {
        return this.propertyValidCount;
    };
    DataQualityMetrics.prototype.setPropertyValidCount = function (propertyValidCount) {
        this.propertyValidCount = propertyValidCount;
    };
    DataQualityMetrics.prototype.getPropertyInvalidCount = function () {
        return this.propertyDataCount - this.propertyValidCount;
    };
    DataQualityMetrics.prototype.getCharacteristicCount = function () {
        return this.characteristicCount;
    };
    DataQualityMetrics.prototype.setCharacteristicCount = function (characteristicCount) {
        this.characteristicCount = characteristicCount;
    };
    DataQualityMetrics.prototype.getCharacteristicDataCount = function () {
        return this.characteristicDataCount;
    };
    DataQualityMetrics.prototype.setCharacteristicDataCount = function (characteristicDataCount) {
        this.characteristicDataCount = characteristicDataCount;
    };
    DataQualityMetrics.prototype.getCharacteristicVerificationCount = function () {
        return this.characteristicVerificationCount;
    };
    DataQualityMetrics.prototype.setCharacteristicVerificationCount = function (characteristicVerificationCount) {
        this.characteristicVerificationCount = characteristicVerificationCount;
    };
    DataQualityMetrics.prototype.getCharacteristicAccurateCount = function () {
        return this.characteristicAccurateCount;
    };
    DataQualityMetrics.prototype.setCharacteristicAccurateCount = function (characteristicAccurateCount) {
        this.characteristicAccurateCount = characteristicAccurateCount;
    };
    DataQualityMetrics.prototype.getCharacteristicIncorrectCount = function () {
        return this.characteristicDataCount - this.characteristicAccurateCount;
    };
    DataQualityMetrics.prototype.getCharacteristicValidCount = function () {
        return this.characteristicValidCount;
    };
    DataQualityMetrics.prototype.setCharacteristicValidCount = function (characteristicValidCount) {
        this.characteristicValidCount = characteristicValidCount;
    };
    DataQualityMetrics.prototype.getCharacteristicInvalidCount = function () {
        return this.characteristicDataCount - this.characteristicValidCount;
    };
    DataQualityMetrics.prototype.getNonCharacteristicCount = function () {
        return this.propertyCount - this.characteristicCount;
    };
    DataQualityMetrics.prototype.getNonCharacteristicDataCount = function () {
        return this.propertyDataCount - this.characteristicDataCount;
    };
    DataQualityMetrics.prototype.getNonCharacteristicAccurateCount = function () {
        return this.propertyAccurateCount - this.characteristicAccurateCount;
    };
    DataQualityMetrics.prototype.getNonCharacteristicIncorrectCount = function () {
        return this.getPropertyIncorrectCount() - this.getCharacteristicIncorrectCount();
    };
    DataQualityMetrics.prototype.getNonCharacteristicValidCount = function () {
        return this.propertyValidCount - this.characteristicValidCount;
    };
    DataQualityMetrics.prototype.getNonCharacteristicInvalidCount = function () {
        return this.getPropertyInvalidCount() - this.getCharacteristicInvalidCount();
    };
    return DataQualityMetrics;
}());
exports.DataQualityMetrics = DataQualityMetrics;
var DataQualityMetricsSummary = (function () {
    function DataQualityMetricsSummary() {
    }
    return DataQualityMetricsSummary;
}());
exports.DataQualityMetricsSummary = DataQualityMetricsSummary;
//# sourceMappingURL=data-record-quality.model.js.map