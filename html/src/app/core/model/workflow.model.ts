import { DataObject } from './data-object.model';

export class FlowStatus
{
    flowIid: string;
    flowId: string;
    initiatorUserName: string;
    availableTasks: Task[];

    constructor()
    {
        this.availableTasks = new Array();
    }

    public static fromJson(jsonStatus: any): FlowStatus
    {
        let status: FlowStatus;

        status = new FlowStatus();
        status.flowIid = jsonStatus.flowIid;
        status.flowId = jsonStatus.flowId;
        status.initiatorUserName = jsonStatus.initiatorUserName;

        if (jsonStatus.availableTasks)
        {
            for (let jsonTask of jsonStatus.availableTasks)
            {
                status.availableTasks.push(Task.fromJson(jsonTask));
            }
        }

        return status;
    }

    public hasAvailableTasks(): boolean
    {
        return this.availableTasks.length > 0;
    }

    public getFirstAvailableTask(): Task
    {
        return this.availableTasks.length > 0 ? this.availableTasks[0] : null;
    }
}

export class TaskListSummary
{
    totalTaskCount: number;
    claimedTaskCount: number;
    taskTypes: TaskTypeSummary[];
}

export class TaskTypeSummary
{
    taskId: string;
    name: string;
    description: string;
    totalCount: number;
    claimedCount: number;
}

export class TaskList
{
    tasks: Task[];
}

export class Task
{
    flowId: string;
    flowIid: string;
    taskId: string;
    taskIid: string;
    taskName: string;
    taskDescription: string;
    functionalityId: string;
    restrictionUserId: string;
    restrictionProfileId: string;
    initiatorId: string;
    claimed: boolean;
    claimedByUserId: string;
    timeIssued: number;
    timeClaimed: number;
    priority: number;
    fromUserId: string;
    fromUserDisplayName: string;
    userThumbnailUrl: string;
    fields: TaskField[];

    constructor()
    {
        this.fields = new Array();
    }

    public static fromJson(jsonTask: any): Task
    {
        let newTask: Task;

        newTask = new Task();
        newTask.flowIid = jsonTask.flowIid;
        newTask.flowId = jsonTask.flowId;
        newTask.taskId = jsonTask.taskId;
        newTask.taskIid = jsonTask.taskIid;
        newTask.taskName = jsonTask.taskName;
        newTask.taskDescription = jsonTask.taskDescription;
        newTask.functionalityId = jsonTask.functionalityId;
        newTask.restrictionUserId = jsonTask.restrictionUserId;
        newTask.restrictionProfileId = jsonTask.restrictionProfileId;
        newTask.initiatorId = jsonTask.initiatorId;
        newTask.claimed = jsonTask.claimed;
        newTask.claimedByUserId = jsonTask.claimedByUserId;
        newTask.timeIssued = jsonTask.timeIssued;
        newTask.timeClaimed = jsonTask.timeClaimed;
        newTask.priority = jsonTask.priority;
        newTask.fromUserId = jsonTask.fromUserId;
        newTask.fromUserDisplayName = jsonTask.fromUserDisplayName;

        if (jsonTask.fields)
        {
            for (let jsonField of jsonTask.fields)
            {
                newTask.fields.push(TaskField.fromJson(jsonField));
            }
        }

        return newTask;
    }
}

export class TaskField
{
    id: string;
    name: string;
    description: string;
    value: any;

    public static fromJson(jsonField: any): TaskField
    {
        let field: TaskField;

        field = new TaskField;
        field.id = jsonField.id;
        field.name = jsonField.name;
        field.description = jsonField.description;
        field.value = jsonField.value;
        return field;
    }
}

export class TaskSearchResults
{
    searchExpression: string;
    searchTerms: string;
    pageOffset: number;
    tasks: Task[];

    constructor()
    {
        this.tasks = new Array();
    }

    public addTasksFromJson(jsonTasks: any)
    {
        if (jsonTasks)
        {
            for (let jsonTask of jsonTasks)
            {
                this.tasks.push(Task.fromJson(jsonTask));
            }
        }
    }
}

export class TaskCompletionResult
{
    taskIid: string;
    resultType: TaskCompletionResultType;
    resultParameters: any;

    constructor()
    {
    }

    public static fromJson(jsonResult: any): TaskCompletionResult
    {
        let result: TaskCompletionResult;

        result = new TaskCompletionResult();
        result.taskIid = jsonResult.taskIid;
        result.resultType = TaskCompletionResultType[jsonResult.resultType as keyof TaskCompletionResultType];
        result.resultParameters = jsonResult.resultParameters;
        return result;
    }

    public isSuccess(): boolean
    {
        return this.resultType == "SUCCESS";
    }
}

export enum TaskCompletionResultType 
{
    SUCCESS = "SUCCESS", // Task completion failure due access violation: user restricted from task.
    FAILURE_ACCESS = "FAILURE_ACCESS", // Task completion failure due access violation: user restricted from task.
    FAILURE_CLAIMED = "FAILURE_CLAIMED", // Task completion failure due to current task state: claimed by another user.
    FAILURE_COMPLETED = "FAILURE_COMPLETED", // Task completion failure due to current task state:  already completed.
    FAILURE_VALIDATION = "FAILURE_VALIDATION", // Task completion failure due to end condition validation.
}