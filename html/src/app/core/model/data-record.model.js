"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/*
    Data Record Model
*/
var DataRecord = (function () {
    function DataRecord() {
        this.sections = new Array();
        this.subRecords = new Array();
    }
    DataRecord.prototype.fromJson = function (jsonRecord) {
        this.recordId = jsonRecord.recordId;
        this.drInstance = new DataRequirementInstance();
        this.drInstance.fromJson(jsonRecord.drInstance);
        // Parse all sections contained by this record.
        for (var _i = 0, _a = jsonRecord.sections; _i < _a.length; _i++) {
            var jsonSection = _a[_i];
            var section = new RecordSection();
            section.parentRecord = this;
            section.fromJson(jsonSection);
            this.sections.push(section);
        }
        // Parse all sub-records contained by this record.
        if (jsonRecord.subRecords) {
            for (var _b = 0, _c = jsonRecord.subRecords; _b < _c.length; _b++) {
                var jsonSubRecord = _c[_b];
                var subRecord = new DataRecord();
                subRecord.parentRecord = this;
                subRecord.fromJson(jsonSubRecord);
                this.subRecords.push(subRecord);
            }
        }
        // Refresh this record's value string.
        this.refreshValueString();
    };
    DataRecord.prototype.refreshValueString = function () {
        this.valueString = this.drInstance.term + ":" + this.recordId;
    };
    DataRecord.prototype.getFileId = function () {
        var rootRecord;
        // This must be fixed to properly check that the root record is a DataFile.
        rootRecord = this.getRootRecord();
        return rootRecord != null ? rootRecord.recordId : null;
    };
    DataRecord.prototype.getFile = function () {
        var nextRecord;
        nextRecord = this;
        while (nextRecord.parentRecord) {
            nextRecord = nextRecord.parentRecord;
        }
        return nextRecord;
    };
    DataRecord.prototype.getRootRecord = function () {
        var nextRecord;
        nextRecord = this;
        while (nextRecord.parentRecord) {
            nextRecord = nextRecord.parentRecord;
        }
        return nextRecord;
    };
    DataRecord.prototype.getSubRecord = function (recordId) {
        for (var _i = 0, _a = this.subRecords; _i < _a.length; _i++) {
            var subRecord = _a[_i];
            if (subRecord.recordId = recordId)
                return subRecord;
        }
        return null;
    };
    DataRecord.prototype.getRecord = function (recordId) {
        var recordQueue;
        recordQueue = new Array();
        recordQueue.push(this.getRootRecord());
        while (recordQueue.length > 0) {
            var nextRecord = void 0;
            nextRecord = recordQueue.pop();
            if (nextRecord.recordId == recordId) {
                return nextRecord;
            }
            else {
                recordQueue.push.apply(recordQueue, nextRecord.subRecords);
            }
        }
        return null;
    };
    DataRecord.prototype.getRecords = function () {
        var recordQueue;
        var records;
        records = new Array();
        recordQueue = new Array();
        recordQueue.push(this.getRootRecord());
        while (recordQueue.length > 0) {
            var nextRecord = void 0;
            nextRecord = recordQueue.pop();
            records.push(nextRecord);
            recordQueue.push.apply(recordQueue, nextRecord.subRecords);
        }
        return records;
    };
    DataRecord.prototype.getSection = function (sectionId) {
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            if (section.sectionId == sectionId) {
                return section;
            }
        }
        return null;
    };
    DataRecord.prototype.getProperty = function (propertyId) {
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            for (var _b = 0, _c = section.properties; _b < _c.length; _b++) {
                var property = _c[_b];
                if (property.propertyId == propertyId) {
                    return property;
                }
            }
        }
        return null;
    };
    DataRecord.prototype.getPropertyDataCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            count += section.getPropertyDataCount();
        }
        return count;
    };
    DataRecord.prototype.getProperties = function () {
        var properties;
        properties = new Array();
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            properties.push.apply(properties, section.properties);
        }
        return properties;
    };
    DataRecord.prototype.getDataQuality = function (includeDescendants) {
        var quality;
        // Add the data quality metrics of this record.
        quality = new RecordDataQuality(this.recordId);
        quality.propertyCount = this.drInstance.getPropertyCount();
        quality.propertyDataCount = this.getPropertyDataCount();
        // Add the data quality metrics of this record's descendants if required.
        if (includeDescendants) {
            for (var _i = 0, _a = this.subRecords; _i < _a.length; _i++) {
                var subRecord = _a[_i];
                quality.addDataQuality(subRecord.getDataQuality(includeDescendants));
            }
        }
        return quality;
    };
    return DataRecord;
}());
exports.DataRecord = DataRecord;
var RecordSection = (function () {
    function RecordSection() {
        this.properties = new Array();
    }
    RecordSection.prototype.fromJson = function (jsonSection) {
        this.sectionId = jsonSection.sectionId;
        for (var _i = 0, _a = jsonSection.properties; _i < _a.length; _i++) {
            var jsonProperty = _a[_i];
            var property = new RecordProperty(null);
            property.parentRecord = this.parentRecord;
            property.fromJson(jsonProperty);
            this.properties.push(property);
        }
    };
    RecordSection.prototype.getProperty = function (propertyId) {
        for (var _i = 0, _a = this.properties; _i < _a.length; _i++) {
            var property = _a[_i];
            if (property.propertyId == propertyId) {
                return property;
            }
        }
        return null;
    };
    RecordSection.prototype.addProperty = function (property) {
        this.properties.push(property);
    };
    RecordSection.prototype.getPropertyDataCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.properties; _i < _a.length; _i++) {
            var property = _a[_i];
            if (property.hasContent()) {
                count++;
            }
        }
        return count;
    };
    return RecordSection;
}());
exports.RecordSection = RecordSection;
var RecordProperty = (function () {
    function RecordProperty(propertyId) {
        this.propertyId = propertyId;
    }
    RecordProperty.prototype.fromJson = function (jsonProperty) {
        this.propertyId = jsonProperty.propertyId;
        this.value = RecordValue.createValueFromJson(jsonProperty.value);
        if (this.value) {
            this.value.parentProperty = this;
            this.value.parentRecord = this.parentRecord;
        }
    };
    RecordProperty.prototype.setValue = function (value) {
        this.value = value;
        if (value != null) {
            this.value.parentRecord = this.parentRecord;
            this.value.parentProperty = this;
        }
    };
    RecordProperty.prototype.hasContent = function () {
        return this.value != null && this.value.hasContent();
    };
    return RecordProperty;
}());
exports.RecordProperty = RecordProperty;
var RecordValue = (function () {
    function RecordValue(type) {
        this.type = type;
    }
    RecordValue.prototype.fromJson = function (jsonValue) {
    };
    RecordValue.createValueFromJson = function (jsonValue) {
        if (jsonValue) {
            switch (jsonValue.type) {
                case "STRING":
                    {
                        var value = new StringValue();
                        value.fromJson(jsonValue);
                        return value;
                    }
                case "TEXT":
                    {
                        var value = new TextValue();
                        value.fromJson(jsonValue);
                        return value;
                    }
                case "CONTROLLED_CONCEPT":
                    {
                        var value = new ControlledConcept();
                        value.fromJson(jsonValue);
                        return value;
                    }
                case "DOCUMENT_REFERENCE":
                    {
                        var value = new DocumentReference();
                        value.fromJson(jsonValue);
                        return value;
                    }
                default:
                    {
                        var value = new StringValue();
                        value.fromJson(jsonValue);
                        return value;
                    }
            }
        }
        else {
            return null;
        }
    };
    return RecordValue;
}());
exports.RecordValue = RecordValue;
var StringValue = (function (_super) {
    __extends(StringValue, _super);
    function StringValue() {
        return _super.call(this, "STRING") || this;
    }
    StringValue.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        this.string = jsonValue.string;
    };
    StringValue.prototype.getContent = function () {
        var content;
        content = new StringContent();
        content.string = this.string;
        return content;
    };
    StringValue.prototype.hasContent = function () {
        return this.string != null && this.string.length > 0;
    };
    return StringValue;
}(RecordValue));
exports.StringValue = StringValue;
var TextValue = (function (_super) {
    __extends(TextValue, _super);
    function TextValue() {
        return _super.call(this, "TEXT") || this;
    }
    TextValue.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        this.text = jsonValue.text;
    };
    TextValue.prototype.getContent = function () {
        var content;
        content = new TextContent();
        content.text = this.text;
        return content;
    };
    TextValue.prototype.hasContent = function () {
        return this.text != null && this.text.length > 0;
    };
    return TextValue;
}(RecordValue));
exports.TextValue = TextValue;
var ControlledConcept = (function (_super) {
    __extends(ControlledConcept, _super);
    function ControlledConcept() {
        return _super.call(this, "CONTROLLED_CONCEPT") || this;
    }
    ControlledConcept.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        this.conceptId = jsonValue.conceptId;
        this.term = jsonValue.term;
        this.definition = jsonValue.definition;
        this.code = jsonValue.code;
    };
    ControlledConcept.prototype.hasContent = function () {
        return this.conceptId != null;
    };
    return ControlledConcept;
}(RecordValue));
exports.ControlledConcept = ControlledConcept;
var DocumentReference = (function (_super) {
    __extends(DocumentReference, _super);
    function DocumentReference() {
        var _this = _super.call(this, "DOCUMENT_REFERENCE") || this;
        _this.references = new Array();
        return _this;
    }
    DocumentReference.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        for (var _i = 0, _a = jsonValue.references; _i < _a.length; _i++) {
            var reference = _a[_i];
            this.references.push(reference.recordId);
        }
    };
    DocumentReference.prototype.hasContent = function () {
        return this.references.length > 0;
    };
    return DocumentReference;
}(RecordValue));
exports.DocumentReference = DocumentReference;
/*
    Record Content Model
*/
var ValueContent = (function () {
    function ValueContent(type) {
        this.type = type;
    }
    ValueContent.prototype.fromJson = function (jsonValue) {
    };
    return ValueContent;
}());
exports.ValueContent = ValueContent;
var StringContent = (function (_super) {
    __extends(StringContent, _super);
    function StringContent() {
        return _super.call(this, "STRING") || this;
    }
    StringContent.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        this.string = jsonValue.string;
    };
    return StringContent;
}(ValueContent));
exports.StringContent = StringContent;
var TextContent = (function (_super) {
    __extends(TextContent, _super);
    function TextContent() {
        return _super.call(this, "TEXT") || this;
    }
    TextContent.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        this.text = jsonValue.text;
    };
    return TextContent;
}(ValueContent));
exports.TextContent = TextContent;
var ControlledConceptContent = (function (_super) {
    __extends(ControlledConceptContent, _super);
    function ControlledConceptContent() {
        return _super.call(this, "CONTROLLED_CONCEPT") || this;
    }
    ControlledConceptContent.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        this.conceptId = jsonValue.conceptId;
        this.term = jsonValue.term;
        this.definition = jsonValue.definition;
        this.code = jsonValue.code;
    };
    return ControlledConceptContent;
}(ValueContent));
exports.ControlledConceptContent = ControlledConceptContent;
var DocumentReferenceContent = (function (_super) {
    __extends(DocumentReferenceContent, _super);
    function DocumentReferenceContent() {
        var _this = _super.call(this, "DOCUMENT_REFERENCE") || this;
        _this.references = new Array();
        return _this;
    }
    DocumentReferenceContent.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
        for (var _i = 0, _a = jsonValue.references; _i < _a.length; _i++) {
            var reference = _a[_i];
            this.references.push(reference.recordId);
        }
    };
    return DocumentReferenceContent;
}(ValueContent));
exports.DocumentReferenceContent = DocumentReferenceContent;
/*
    Data Requirement Model
*/
var DataRequirementInstance = (function () {
    function DataRequirementInstance() {
        this.sections = new Array();
    }
    DataRequirementInstance.prototype.fromJson = function (jsonDrInstance) {
        this.drInstanceId = jsonDrInstance.drInstanceId;
        this.term = jsonDrInstance.term;
        this.definition = jsonDrInstance.definition;
        for (var _i = 0, _a = jsonDrInstance.sections; _i < _a.length; _i++) {
            var jsonSection = _a[_i];
            var section = new SectionRequirement();
            section.fromJson(jsonSection);
            this.sections.push(section);
        }
    };
    DataRequirementInstance.prototype.getSection = function (sectionId) {
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            if (section.sectionId == sectionId) {
                return section;
            }
        }
        return null;
    };
    DataRequirementInstance.prototype.getProperty = function (propertyId) {
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            for (var _b = 0, _c = section.properties; _b < _c.length; _b++) {
                var property = _c[_b];
                if (property.propertyId == propertyId) {
                    return property;
                }
            }
        }
        return null;
    };
    DataRequirementInstance.prototype.getProperties = function () {
        var properties;
        properties = new Array();
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            properties.push.apply(properties, section.properties);
        }
        return properties;
    };
    DataRequirementInstance.prototype.getPropertyCount = function () {
        var count;
        count = 0;
        for (var _i = 0, _a = this.sections; _i < _a.length; _i++) {
            var section = _a[_i];
            count += section.getPropertyCount();
        }
        return count;
    };
    return DataRequirementInstance;
}());
exports.DataRequirementInstance = DataRequirementInstance;
var SectionRequirement = (function () {
    function SectionRequirement() {
        this.properties = new Array();
    }
    SectionRequirement.prototype.fromJson = function (jsonSection) {
        this.sectionId = jsonSection.sectionId;
        this.term = jsonSection.term;
        this.definition = jsonSection.definition;
        for (var _i = 0, _a = jsonSection.properties; _i < _a.length; _i++) {
            var jsonProperty = _a[_i];
            var property = new PropertyRequirement();
            property.fromJson(jsonProperty);
            this.properties.push(property);
        }
    };
    SectionRequirement.prototype.getProperty = function (propertyId) {
        for (var _i = 0, _a = this.properties; _i < _a.length; _i++) {
            var property = _a[_i];
            if (property.propertyId == propertyId) {
                return property;
            }
        }
        return null;
    };
    SectionRequirement.prototype.getPropertyCount = function () {
        return this.properties.length;
    };
    return SectionRequirement;
}());
exports.SectionRequirement = SectionRequirement;
var PropertyRequirement = (function () {
    function PropertyRequirement() {
    }
    PropertyRequirement.prototype.fromJson = function (jsonProperty) {
        this.propertyId = jsonProperty.propertyId;
        this.term = jsonProperty.term;
        this.definition = jsonProperty.definition;
        this.value = this.createValue(jsonProperty.value);
        this.value.parentProperty = this;
    };
    PropertyRequirement.prototype.createValue = function (jsonValue) {
        if (jsonValue) {
            switch (jsonValue.type) {
                case "STRING":
                    {
                        var value = new StringRequirement();
                        value.fromJson(jsonValue);
                        return value;
                    }
                case "TEXT":
                    {
                        var value = new TextRequirement();
                        value.fromJson(jsonValue);
                        return value;
                    }
                case "CONTROLLED_CONCEPT":
                    {
                        var value = new ControlledConceptRequirement();
                        value.fromJson(jsonValue);
                        return value;
                    }
                case "DOCUMENT_REFERENCE":
                    {
                        var value = new DocumentReferenceRequirement();
                        value.fromJson(jsonValue);
                        return value;
                    }
                default:
                    {
                        var value = new StringRequirement();
                        value.fromJson(jsonValue);
                        return value;
                    }
            }
        }
        else {
            console.log("Property Requirement without any Value Requirement encountered: " + this.propertyId);
            return null;
        }
    };
    return PropertyRequirement;
}());
exports.PropertyRequirement = PropertyRequirement;
var ValueRequirement = (function () {
    function ValueRequirement() {
    }
    ValueRequirement.prototype.fromJson = function (jsonValue) {
        this.type = jsonValue.type;
    };
    return ValueRequirement;
}());
exports.ValueRequirement = ValueRequirement;
var StringRequirement = (function (_super) {
    __extends(StringRequirement, _super);
    function StringRequirement() {
        return _super.call(this) || this;
    }
    StringRequirement.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
    };
    return StringRequirement;
}(ValueRequirement));
exports.StringRequirement = StringRequirement;
var TextRequirement = (function (_super) {
    __extends(TextRequirement, _super);
    function TextRequirement() {
        return _super.call(this) || this;
    }
    TextRequirement.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
    };
    return TextRequirement;
}(ValueRequirement));
exports.TextRequirement = TextRequirement;
var ControlledConceptRequirement = (function (_super) {
    __extends(ControlledConceptRequirement, _super);
    function ControlledConceptRequirement() {
        return _super.call(this) || this;
    }
    ControlledConceptRequirement.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
    };
    return ControlledConceptRequirement;
}(ValueRequirement));
exports.ControlledConceptRequirement = ControlledConceptRequirement;
var DocumentReferenceRequirement = (function (_super) {
    __extends(DocumentReferenceRequirement, _super);
    function DocumentReferenceRequirement() {
        return _super.call(this) || this;
    }
    DocumentReferenceRequirement.prototype.fromJson = function (jsonValue) {
        _super.prototype.fromJson.call(this, jsonValue);
    };
    return DocumentReferenceRequirement;
}(ValueRequirement));
exports.DocumentReferenceRequirement = DocumentReferenceRequirement;
/*
    Data Record Update Model
*/
var DataFileUpdate = (function () {
    function DataFileUpdate(fileId) {
        this.fileId = fileId;
        this.valueUpdates = new Array();
    }
    DataFileUpdate.prototype.addValueUpdate = function (update) {
        this.valueUpdates.push(update);
    };
    return DataFileUpdate;
}());
exports.DataFileUpdate = DataFileUpdate;
var DataFileValueUpdate = (function () {
    function DataFileValueUpdate(recordId, propertyId, fieldId) {
        this.recordId = recordId;
        this.propertyId = propertyId;
        this.fieldId = fieldId;
    }
    DataFileValueUpdate.prototype.setNewValue = function (value) {
        this.newValue = value;
    };
    return DataFileValueUpdate;
}());
exports.DataFileValueUpdate = DataFileValueUpdate;
/*
    Record Data Quality Model
*/
var RecordDataQuality = (function () {
    function RecordDataQuality(recordId) {
        this.recordId = recordId;
        this.propertyCount = 0;
        this.propertyDataCount = 0;
        this.characteristicCount = 0;
        this.characteristicDataCount = 0;
    }
    RecordDataQuality.prototype.addDataQuality = function (recordQuality) {
        this.propertyCount += recordQuality.propertyCount;
        this.propertyDataCount += recordQuality.propertyDataCount;
        this.characteristicCount += recordQuality.characteristicCount;
        this.characteristicDataCount += recordQuality.characteristicDataCount;
    };
    return RecordDataQuality;
}());
exports.RecordDataQuality = RecordDataQuality;
/*
    Data Record Access Layer Model
*/
var RecordAccessLayer = (function () {
    function RecordAccessLayer() {
    }
    return RecordAccessLayer;
}());
exports.RecordAccessLayer = RecordAccessLayer;
var SectionAccessLayer = (function () {
    function SectionAccessLayer() {
    }
    return SectionAccessLayer;
}());
exports.SectionAccessLayer = SectionAccessLayer;
var PropertyAccessLayer = (function () {
    function PropertyAccessLayer() {
    }
    return PropertyAccessLayer;
}());
exports.PropertyAccessLayer = PropertyAccessLayer;
//# sourceMappingURL=data-record.model.js.map