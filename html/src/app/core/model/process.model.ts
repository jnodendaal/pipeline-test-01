export class Process
{
    iid: string;
    id: string;
    name: string;
    description: string;
    thumbnailImageId: string;
    initiatorName: string;
    status: string;
    startTime: number;
    endTime: number;
    progress: number;

    constructor()
    {
        this.progress = 0.0;
    }

    public static fromJson(jsonProcess: any): Process
    {
        let newProcess: Process;

        newProcess = new Process();
        newProcess.iid = jsonProcess.iid;
        newProcess.name = jsonProcess.name;
        newProcess.description = jsonProcess.description;
        newProcess.thumbnailImageId = jsonProcess.thumbnailImageId;
        newProcess.initiatorName = jsonProcess.initiatorName;
        newProcess.status = jsonProcess.status;
        newProcess.startTime = jsonProcess.startTime;
        newProcess.endTime = jsonProcess.endTime;
        newProcess.progress = jsonProcess.progress;
        return newProcess;
    }
}

export class ProcessSearchResults
{
    searchExpression: string;
    searchTerms: string;
    pageOffset: number;
    processes: Process[];

    constructor()
    {
        this.processes = new Array();
    }

    public addProcessesFromJson(jsonProcesses: any)
    {
        if (jsonProcesses)
        {
            for (let jsonProcess of jsonProcesses)
            {
                this.processes.push(Process.fromJson(jsonProcess));
            }
        }
    }
}