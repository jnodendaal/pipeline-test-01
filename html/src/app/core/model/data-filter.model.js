"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var T8DataFilter = (function () {
    function T8DataFilter(entityId) {
        this.entityId = entityId;
        this.fields = new Array();
        this.order = new Array();
        this.criteria = new Array();
    }
    T8DataFilter.prototype.addFieldOrdering = function (fieldId, method) {
        this.order.push(new T8DataFilterFieldOrdering(fieldId, method));
    };
    T8DataFilter.prototype.addCriterion = function (conjunction, fieldId, operator, value) {
        this.criteria.push(new T8DataFilterCriterion(conjunction, fieldId, operator, value));
    };
    return T8DataFilter;
}());
exports.T8DataFilter = T8DataFilter;
var T8DataFilterCriteria = (function () {
    function T8DataFilterCriteria(conjunction) {
        this.conjunction = conjunction;
        this.criteria = new Array();
    }
    return T8DataFilterCriteria;
}());
exports.T8DataFilterCriteria = T8DataFilterCriteria;
var T8DataFilterCriterion = (function () {
    function T8DataFilterCriterion(conjunction, fieldId, operator, value) {
        this.conjunction = conjunction;
        this.fieldId = fieldId;
        this.operator = operator;
        this.value = value;
    }
    return T8DataFilterCriterion;
}());
exports.T8DataFilterCriterion = T8DataFilterCriterion;
var T8DataFilterFieldOrdering = (function () {
    function T8DataFilterFieldOrdering(fieldId, method) {
        this.fieldId = fieldId;
        this.method = method;
    }
    return T8DataFilterFieldOrdering;
}());
exports.T8DataFilterFieldOrdering = T8DataFilterFieldOrdering;
//# sourceMappingURL=data-filter.model.js.map