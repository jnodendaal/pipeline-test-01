export class DataFilter
{
    id: string;
    entityId: string;
    fields: string[];
    order: DataFilterFieldOrdering[];
    criteria: DataFilterCriteria;
    pageOffset: number;
    pageSize: number;

    constructor(id: string, entityId: string)
    {
        this.entityId = entityId;
        this.fields = new Array();
        this.order = new Array();
    }

    public copy(): DataFilter
    {
        let copy: DataFilter;

        copy = new DataFilter(this.id, this.entityId);
        copy.pageSize = this.pageOffset;
        copy.pageSize = this.pageSize;

        for (let ordering of this.order)
        {
            copy.addFieldOrdering(ordering.fieldId, ordering.method);
        }

        if (this.criteria)
        {
            copy.setCriteria(this.criteria.copy());
        }

        return copy;
    }

    public clearFieldOrdering()
    {
        this.order.length = 0;
    }

    public addFieldOrdering(fieldId: string, method: string)
    {
        this.order.push(new DataFilterFieldOrdering(fieldId, method));
    }

    public addFieldOrderings(fieldOrderings: DataFilterFieldOrdering[])
    {
        this.order.push(...fieldOrderings);
    }

    public removeFieldOrdering(fieldId: string): DataFilterFieldOrdering
    {
        for (let index = 0; index < this.order.length; index++)
        {
            let ordering: DataFilterFieldOrdering;

            ordering = this.order[index];
            if (ordering.fieldId == fieldId)
            {
                this.order.splice(index, 1);
                return ordering;
            }
        }

        return null;
    }

    public addCriterion(conjunction: string, fieldId: string, operator: string, value: any): DataFilterCriterion
    {
        if (!this.criteria)
        {
            this.criteria = new DataFilterCriteria(null, "AND");
        }

        return this.criteria.addCriterion(conjunction, fieldId, operator, value);
    }

    public addCriteria(criteria: DataFilterCriterion[])
    {
        if (!this.criteria)
        {
            this.criteria = new DataFilterCriteria(null, "AND");
        }

        this.criteria.addCriteria(criteria);
    }

    public setCriteria(criteria: DataFilterCriteria)
    {
        this.criteria = criteria;
    }

    public addClause(clause: DataFilterClause)
    {
        if (!this.criteria)
        {
            if (clause instanceof DataFilterCriteria)
            {
                this.setCriteria(clause as DataFilterCriteria);
                return;
            }
            else
            {
                this.setCriteria(new DataFilterCriteria(null, "AND"));
            }
        }

        this.criteria.addClause(clause);
    }

    public removeClause(id: string): DataFilterClause
    {
        return this.criteria ? this.criteria.removeClause(id) : null;
    }
}

export interface DataFilterClause
{
    id: string;
    type: string;

    copy(): DataFilterClause;
    hasCriteria(): boolean;
}

export class DataFilterCriteria implements DataFilterClause
{
    id: string;
    type: string;
    conjunction: string;
    criteria: DataFilterClause[];

    constructor(id: string, conjunction: string)
    {
        this.id = id;
        this.type = "@FILTER_CRITERIA";
        this.conjunction = conjunction;
        this.criteria = new Array();
    }

    public copy(): DataFilterCriteria
    {
        let copy: DataFilterCriteria;

        copy = new DataFilterCriteria(this.id, this.conjunction);
        for (let clause of this.criteria)
        {
            copy.addClause(clause.copy());
        }

        return copy;
    }

    public addCriteria(criteria: DataFilterCriterion[])
    {
        this.criteria.push(...criteria);  
    }

    public addCriterion(conjunction: string, fieldId: string, operator: string, value: any): DataFilterCriterion
    {
        let newCriterion: DataFilterCriterion;

        newCriterion = new DataFilterCriterion(null, conjunction, fieldId, operator, value);
        this.criteria.push(newCriterion);                
        return newCriterion;
    }

    public addClause(clause: DataFilterClause)
    {
        this.criteria.push(clause);
    }

    public removeClause(id: string): DataFilterClause
    {
        for (let index = 0; index < this.criteria.length; index++)
        {
            let clause: DataFilterClause;

            clause = this.criteria[index];
            if (clause.id == id)
            {
                this.criteria.splice(index, 1);
                return clause;
            }
            else if (clause instanceof DataFilterCriteria)
            {
                let removedClause: DataFilterClause;
                let criteria: DataFilterCriteria;

                criteria = clause as DataFilterCriteria;
                removedClause = criteria.removeClause(id);
                if (removedClause != null)
                {
                    return removedClause;
                }
            }
        }

        return null;
    }

    public hasCriteria(): boolean
    {
        for (let clause of this.criteria)
        {
            if (clause.hasCriteria()) return true;
        }

        return false;
    }
}


export class DataFilterCriterion implements DataFilterClause
{
    id: string;
    type: string;
    conjunction: string;
    fieldId: string;
    operator: string;
    value: any;

    constructor(id: string, conjunction: string, fieldId: string, operator: string, value: any)
    {
        this.id = id;
        this.type = "@FILTER_CRITERION";
        this.conjunction = conjunction;
        this.fieldId = fieldId;
        this.operator = operator;
        this.value = value;
    }

    public copy(): DataFilterCriterion
    {
        let copy: DataFilterCriterion;

        copy = new DataFilterCriterion(this.id, this.conjunction, this.fieldId, this.operator, this.value);
        return copy;
    }

    public hasCriteria(): boolean
    {
        if (this.operator == "IS_NULL") return true; // This operator does not require a filter value.
        else if (this.operator == "IS_NOT_NULL") return true; // This operator does not require a filter value.
        else if (this.value == null) return false; // All other operators require a filter value.
        else if (this.value instanceof String) // If the filter value is a String type, make sure it is not empty.
        {
            return this.value.trim().length > 0;
        }
        else return true;
    }
}

export class DataFilterFieldOrdering
{
    fieldId: string;
    method: string;

    constructor(fieldId: string, method: string)
    {
        this.fieldId = fieldId;
        this.method = method;
    }
}

export class DataFilterOperator
{
    public id: string;
    public name: string;

    constructor(id: string, name: string)
    {
        this.id = id;
        this.name = name;
    }

    public static getOperatorName(id: string): string
    {
        switch (id)
        {
            case EQUAL.id: return EQUAL.name;
            case NOT_EQUAL.id: return NOT_EQUAL.name;
            case GREATER_THAN.id: return GREATER_THAN.name;
            case GREATER_THAN_OR_EQUAL.id: return GREATER_THAN_OR_EQUAL.name;
            case LESS_THAN.id: return LESS_THAN.name;
            case LESS_THAN_OR_EQUAL.id: return LESS_THAN_OR_EQUAL.name;
            case LIKE.id: return LIKE.name;
            case STARTS_WITH.id: return STARTS_WITH.name;
            case ENDS_WITH.id: return ENDS_WITH.name;
            case MATCHES.id: return MATCHES.name;
            case NOT_LIKE.id: return NOT_LIKE.name;
            case IN.id: return IN.name;
            case NOT_IN.id: return NOT_IN.name;
            case IS_NULL.id: return IS_NULL.name;
            case IS_NOT_NULL.id: return IS_NOT_NULL.name;
            case THIS_DAY.id: return THIS_DAY.name;
            case CONTAINS.id: return CONTAINS.name;
            case EQUAL_TO_FIELD.id: return EQUAL_TO_FIELD.name;
            default: return id;
        }
    }
}

export const EQUAL = new DataFilterOperator('EQUAL','Equal To');
export const NOT_EQUAL = new DataFilterOperator('NOT_EQUAL','Not Equal To');
export const GREATER_THAN = new DataFilterOperator('GREATER_THAN','Greater Than');
export const GREATER_THAN_OR_EQUAL = new DataFilterOperator('GREATER_THAN_OR_EQUAL','Greater Than or Equal To');
export const LESS_THAN = new DataFilterOperator('LESS_THAN','Less than');
export const LESS_THAN_OR_EQUAL = new DataFilterOperator('LESS_THAN_OR_EQUAL','Less Than or Equal To');
export const LIKE = new DataFilterOperator('LIKE','Containing');
export const STARTS_WITH = new DataFilterOperator('STARTS_WITH','Beginning With');
export const ENDS_WITH = new DataFilterOperator('ENDS_WITH','Ending With');
export const MATCHES = new DataFilterOperator('MATCHES','Matching');
export const NOT_LIKE = new DataFilterOperator('NOT_LIKE','Not Containing');
export const IN = new DataFilterOperator('IN','In');
export const NOT_IN = new DataFilterOperator('NOT_IN','Not In');
export const IS_NULL = new DataFilterOperator('IS_NULL','Is Empty');
export const IS_NOT_NULL = new DataFilterOperator('IS_NOT_NULL','Is Not Empty');
export const THIS_DAY = new DataFilterOperator('THIS_DAY','This Day');
export const CONTAINS = new DataFilterOperator('CONTAINS','Full-Text');
export const EQUAL_TO_FIELD = new DataFilterOperator('EQUAL_TO_FIELD','Equal To Field');