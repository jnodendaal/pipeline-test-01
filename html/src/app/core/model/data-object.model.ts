export class DataObject
{
    id: string;
    fields: DataObjectField[];

    constructor()
    {
        this.fields = new Array();
    }

    public fromJson(jsonObject: any): DataObject
    {
        this.id = jsonObject.id;
        if (jsonObject.fields)
        {
            for (let jsonField of jsonObject.fields)
            {
                this.addField(new DataObjectField().fromJson(jsonField));
            }
        }

        return this;
    }

    public getDisplayFields(): DataObjectField[]
    {
        let displayFields: DataObjectField[];

        displayFields = new Array();
        for (let field of this.fields)
        {
            if (field.name) // Currently the check is on name but in future a boolean flag may be used to mark display fields.
            {
                displayFields.push(field);
            }
        }

        return displayFields;
    }

    public addField(field: DataObjectField)
    {
        this.fields.push(field);
    }

    public getField(fieldId: string): DataObjectField
    {
        // If an id with namespace is required, remove the namespace as fields are stored without one.
        if (fieldId.startsWith("@"))
        {
            fieldId = fieldId.substring(this.id.length);
        }

        // Search for the field.
        for (let field of this.fields)
        {
            if (field.id == fieldId)
            {
                return field;
            }
        }

        return null;
    }

    public getFieldValue(fieldId: string): any
    {
        let field: DataObjectField;

        field = this.getField(fieldId);
        return field != null ? field.value : null;
    }
}

export class DataObjectField
{
    id: string;
    type: string;
    name: string;
    description: string;
    sequence: number;
    priority: number;
    value: any;

    constructor()
    {
    }

    public fromJson(jsonField: any): DataObjectField
    {
        this.id = jsonField.id;
        this.name = jsonField.name;
        this.type = "STRING";
        this.description = jsonField.description;
        this.sequence = jsonField.sequence;
        this.priority = jsonField.priority;
        this.value = jsonField.value;
        return this;
    }
}