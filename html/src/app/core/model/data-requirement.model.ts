
/*
    Data Requirement Model
*/

export class DataRequirementInstance
{
    drInstanceId: string;
    term: string;
    definition: string;
    sections: SectionRequirement[];

    constructor()
    {
        this.sections = new Array();
    }

    public fromJson(jsonDrInstance: any)
    {
        this.drInstanceId = jsonDrInstance.drInstanceId;
        this.term = jsonDrInstance.term;
        this.definition = jsonDrInstance.definition;
        for (let jsonSection of jsonDrInstance.sections)
        {
            let section = new SectionRequirement();
            section.fromJson(jsonSection);
            this.sections.push(section);
        }
    }

    public getSection(sectionId: string): SectionRequirement
    {
        for (let section of this.sections)
        {
            if (section.sectionId == sectionId)    
            {
                return section;
            }
        }

        return null;
    }

    public getProperty(propertyId: string): PropertyRequirement
    {
        for (let section of this.sections)
        {
            for (let property of section.properties)
            {
                if (property.propertyId == propertyId)    
                {
                    return property;
                }
            }
        }

        return null;
    }

    public getProperties(): PropertyRequirement[]
    {
        let properties: PropertyRequirement[];

        properties = new Array();
        for (let section of this.sections)
        {
            properties.push(...section.properties);
        }

        return properties;
    }

    public getPropertyCount(): number
    {
        let count: number;

        count = 0;
        for (let section of this.sections)
        {
            count += section.getPropertyCount();
        }

        return count;
    }

    public getCharacteristicCount(): number
    {
        let count: number;

        count = 0;
        for (let section of this.sections)
        {
            count += section.getCharacteristicCount();
        }

        return count;
    }

    public getCharacteristicIds(): Array<string>
    {
        let ids: Array<string>;

        ids = new Array();
        for (let section of this.sections)
        {
            ids.push(...section.getCharacteristicIds());
        }

        return ids;
    }
}

export class SectionRequirement
{
    sectionId: string;
    term: string;
    definition: string;
    properties: PropertyRequirement[];

    constructor()
    {
        this.properties = new Array();
    }

    fromJson(jsonSection: any)
    {
        this.sectionId = jsonSection.sectionId;
        this.term = jsonSection.term;
        this.definition = jsonSection.definition;
        for (let jsonProperty of jsonSection.properties)
        {
            let property = new PropertyRequirement();
            property.fromJson(jsonProperty);
            this.properties.push(property);
        }
    }

    public getProperty(propertyId: string): PropertyRequirement
    {
        for (let property of this.properties)
        {
            if (property.propertyId == propertyId)    
            {
                return property;
            }
        }

        return null;
    }

    getPropertyCount(): number
    {
        return this.properties.length;
    }

    getCharacteristicCount(): number
    {
        let count: number;

        count = 0;
        for (let property of this.properties)
        {
            if (property.characteristic)    
            {
                count++;
            }
        }

        return count;
    }

    getCharacteristicIds(): Array<string>
    {
        let ids: Array<string>;

        ids = Array();
        for (let property of this.properties)
        {
            if (property.characteristic)    
            {
                ids.push(property.propertyId);
            }
        }

        return ids;
    }
}

export class PropertyRequirement
{
    propertyId: string;
    term: string;
    definition: string;
    characteristic: boolean;
    value: ValueRequirement;

    constructor()
    {
    }

    fromJson(jsonProperty: any)
    {
        this.propertyId = jsonProperty.propertyId;
        this.term = jsonProperty.term;
        this.definition = jsonProperty.definition;
        this.characteristic = jsonProperty.characteristic;
        this.setValueRequirement(ValueRequirement.createFromJson(jsonProperty.value));
    }

    public setValueRequirement(valueRequirement: ValueRequirement)
    {
        valueRequirement.setParentProperty(this);
        this.value = valueRequirement;
    }
}

export class ValueRequirement
{
    parentProperty: PropertyRequirement;
    parentField: FieldRequirement;
    type: string;

    public static VALUE_REQUIREMENT_STANDARD_NAMES = 
    [
        "type",
        "propertyId",
        "propertyIrdi",
        "fieldId",
        "fieldIrdi",
        "fields",
        "ontologyClassId",
        "term",
        "abbreviation",
        "definition",
        "code",
        "value",
        "number",
        "dependencies",
        "unitOfMeasure",
        "qualifiedOfMeasure",
        "lowerBoundNumber",
        "upperBoundNumber"
    ];

    constructor(type: string)
    {
        this.type = type;
    }

    fromJson(jsonValue: any)
    {
        // Set the type for this value requirement.
        this.type = jsonValue.type;

        // Set all attributes of this requirement.
        for (var name in jsonValue)
        {
            if (ValueRequirement.VALUE_REQUIREMENT_STANDARD_NAMES.indexOf(name) == -1)
            {
                this[name] = jsonValue[name];
            }
        }
    }

    public setParentProperty(property: PropertyRequirement)
    {
        this.parentProperty = property;
    }

    public setParentField(field: FieldRequirement)
    {
        this.parentField = field;
        this.parentProperty = field != null ? field.parentProperty : null;
    }

    public static createFromJson(jsonValue: any): ValueRequirement
    {
        if (jsonValue)
        {
            switch (jsonValue.type)
            {
                case "STRING":
                {
                    let value = new StringRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "TEXT":
                {
                    let value = new TextRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "BOOLEAN":
                {
                    let value = new BooleanRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "COMPOSITE":
                {
                    let value = new CompositeRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "FIELD":
                {
                    let value = new FieldRequirement(null);
                    value.fromJson(jsonValue);
                    return value;
                }
                case "CONTROLLED_CONCEPT":
                {
                    let value = new ControlledConceptRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "UNIT_OF_MEASURE":
                {
                    let value = new UnitOfMeasureRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "QUALIFIER_OF_MEASURE":
                {
                    let value = new QualifierOfMeasureRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DOCUMENT_REFERENCE":
                {
                    let value = new DocumentReferenceListRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "ATTACHMENT":
                {
                    let value = new AttachmentListRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "MEASURED_NUMBER":
                {
                    let value = new MeasuredNumberRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "MEASURED_RANGE":
                {
                    let value = new MeasuredRangeRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "NUMBER":
                {
                    let value = new NumberRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "LOWER_BOUND_NUMBER":
                {
                    let value = new LowerBoundNumberRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "UPPER_BOUND_NUMBER":
                {
                    let value = new UpperBoundNumberRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "INTEGER":
                {
                    let value = new IntegerRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "REAL":
                {
                    let value = new RealRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DATE":
                {
                    let value = new DateRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DATE_TIME":
                {
                    let value = new DateTimeRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "TIME":
                {
                    let value = new TimeRequirement();
                    value.fromJson(jsonValue);
                    return value;
                }
                default:
                {
                    throw new TypeError("Unsupported data type: " + jsonValue.type);
                }
            }
        }
        else
        {
            return null;
        } 
    }

    public getPropertyId(): string
    {
        return this.parentProperty != null ? this.parentProperty.propertyId : null;
    }

    public getFieldId(): string
    {
        return this.parentField != null ? this.parentField.fieldId : null;
    }
}

export class StringRequirement extends ValueRequirement
{
    constructor()
    {
        super("STRING");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class TextRequirement extends ValueRequirement
{
    constructor()
    {
        super("TEXT");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class BooleanRequirement extends ValueRequirement
{
    constructor()
    {
        super("BOOLEAN");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class ControlledConceptRequirement extends ValueRequirement
{
    constructor()
    {
        super("CONTROLLED_CONCEPT");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class DocumentReferenceListRequirement extends ValueRequirement
{
    constructor()
    {
        super("DOCUMENT_REFERENCE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class AttachmentListRequirement extends ValueRequirement
{
    constructor()
    {
        super("ATTACHMENT");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class CompositeRequirement extends ValueRequirement
{
    fields: FieldRequirement[];

    constructor()
    {
        super("COMPOSITE");
        this.fields = new Array();
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        for (let jsonField of jsonValue.fields)
        {
            let field: FieldRequirement;

            field = new FieldRequirement(jsonField.fieldId);
            field.fromJson(jsonField);
            this.addField(field);
        }
    }

    public addField(field: FieldRequirement)
    {
        field.setParentProperty(this.parentProperty);
        this.fields.push(field);
    }

    public getField(fieldId: string): FieldRequirement
    {
        for (let field of this.fields)
        {
            if (field.fieldId == fieldId)
            {
                return field;
            }
        }

        return null;
    }

    public setParentProperty(property: PropertyRequirement)
    {
        this.parentProperty = property;
        for (let field of this.fields)
        {
            field.setParentProperty(property);
        }
    }
}

export class FieldRequirement extends ValueRequirement
{
    fieldId: string;
    term: string;
    definition: string;
    characteristic: boolean;
    value: ValueRequirement;

    constructor(fieldId: string)
    {
        super("FIELD");
        this.fieldId = fieldId;
    }

    fromJson(jsonField: any)
    {
        super.fromJson(jsonField);
        this.fieldId = jsonField.fieldId;
        this.term = jsonField.term;
        this.definition = jsonField.definition;
        this.setValueRequirement(ValueRequirement.createFromJson(jsonField.value));
    }

    public setValueRequirement(valueRequirement: ValueRequirement)
    {
        valueRequirement.setParentField(this);
        this.value = valueRequirement;
    }

    public setParentProperty(property: PropertyRequirement)
    {
        this.parentProperty = property;
        if (this.value) this.value.setParentProperty(property);
    }
}

export class TimeRequirement extends ValueRequirement
{
    dateTimeFormatPattern: string;
    
    constructor()
    {
        super("TIME");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class DateTimeRequirement extends ValueRequirement
{
    dateTimeFormatPattern: string;
    
    constructor()
    {
        super("DATE_TIME");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class DateRequirement extends ValueRequirement
{
    dateTimeFormatPattern: string;
    
    constructor()
    {
        super("DATE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class NumberRequirement extends ValueRequirement
{
    constructor()
    {
        super("NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class LowerBoundNumberRequirement extends ValueRequirement
{
    constructor()
    {
        super("LOWER_BOUND_NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class UpperBoundNumberRequirement extends ValueRequirement
{
    constructor()
    {
        super("UPPER_BOUND_NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class IntegerRequirement extends ValueRequirement
{
    constructor()
    {
        super("INTEGER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class RealRequirement extends ValueRequirement
{
    constructor()
    {
        super("REAL");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
    }
}

export class UnitOfMeasureRequirement extends ValueRequirement
{
    ontologyClassId: string;

    constructor()
    {
        super("UNIT_OF_MEASRE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.ontologyClassId = jsonValue.ontologyClassId;
    }
}

export class QualifierOfMeasureRequirement extends ValueRequirement
{
    ontologyClassId: string;

    constructor()
    {
        super("QUALIFIER_OF_MEASURE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.ontologyClassId = jsonValue.ontologyClassId;
    }
}

export class MeasuredNumberRequirement extends ValueRequirement
{
    number: NumberRequirement;
    unitOfMeasure: UnitOfMeasureRequirement;
    qualifierOfMeasure: QualifierOfMeasureRequirement;

    constructor()
    {
        super("MEASURED_NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.number = ValueRequirement.createFromJson(jsonValue.number);
        this.unitOfMeasure = ValueRequirement.createFromJson(jsonValue.unitOfMeasure) as UnitOfMeasureRequirement;
        this.qualifierOfMeasure = ValueRequirement.createFromJson(jsonValue.qualifierOfMeasure) as QualifierOfMeasureRequirement;
    }
}

export class MeasuredRangeRequirement extends ValueRequirement
{
    lowerBoundNumber: NumberRequirement;
    upperBoundNumber: NumberRequirement;
    unitOfMeasure: UnitOfMeasureRequirement;
    qualifierOfMeasure: QualifierOfMeasureRequirement;

    constructor()
    {
        super("MEASURED_RANGE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.lowerBoundNumber = ValueRequirement.createFromJson(jsonValue.lowerBoundNumber);
        this.upperBoundNumber = ValueRequirement.createFromJson(jsonValue.upperBoundNumber);
        this.unitOfMeasure = ValueRequirement.createFromJson(jsonValue.unitOfMeasure) as UnitOfMeasureRequirement;
        this.qualifierOfMeasure = ValueRequirement.createFromJson(jsonValue.qualifierOfMeasure) as QualifierOfMeasureRequirement;
    }
}