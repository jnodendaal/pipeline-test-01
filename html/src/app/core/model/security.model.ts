export class Context
{
    functionalityIid: string;

    constructor()
    {
    }

    public static functionalityContext(functionalityIid: string): Context
    {
        let context: Context;

        context = new Context();
        return context.setFunctionalityIid(functionalityIid);
    }

    public setFunctionalityIid(functionalityIid: string): Context
    {
        this.functionalityIid = functionalityIid;
        return this;
    }
}

export class HeartbeatResponse
{
    taskCount: number;
    reportCount: number;
    notificationCount: number;
    processCount: number;

    constructor()
    {
    }

    public static fromJson(jsonValue: any)
    {
        let response: HeartbeatResponse;

        response = new HeartbeatResponse();
        response.taskCount = jsonValue.taskCount;
        response.reportCount = jsonValue.reportCount;
        response.notificationCount = jsonValue.notificationCount;
        response.processCount = jsonValue.processCount;
        return response;
    }
}