export abstract class TimeUnit
{
    private type:string;

    constructor(type: string)
    {
        this.type = type;
    }

    public getType(): string
    {
        return this.type;
    }

    abstract getMilliseconds(): number;
}

export class Millisecond extends TimeUnit
{
    private amount: number

    constructor(amount: number)
    {
        super("@MILLISECOND");
        this.amount = amount;    
    }

    public getMilliseconds(): number
    {
        return this.amount;
    }

    public getAmount(): number
    {
        return this.amount;
    }
}

export class Second extends TimeUnit
{
    private amount: number

    constructor(amount: number)
    {
        super("@SECOND");
        this.amount = amount;    
    }

    public getMilliseconds(): number
    {
        return this.amount * 1000;
    }

    public getAmount(): number
    {
        return this.amount;
    }
}

export class Minute extends TimeUnit
{
    private amount: number

    constructor(amount: number)
    {
        super("@MINUTE");
        this.amount = amount;    
    }

    public getMilliseconds(): number
    {
        return this.amount * 60000;
    }

    public getAmount(): number
    {
        return this.amount;
    }
}

export class Hour extends TimeUnit
{
    private amount: number

    constructor(minutes: number)
    {
        super("@HOUR");
        this.amount = minutes;    
    }

    public getMilliseconds(): number
    {
        return this.amount * 3600000;
    }

    public getAmount(): number
    {
        return this.amount;
    }
}