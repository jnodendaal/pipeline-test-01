import { IdUtility } from '../utility/id.utility';

export class ComponentState
{
    inputParameters: any;
    parentIid: string;
    iid: string;
    id: string;
    
    constructor()
    {
        this.iid = IdUtility.newId();
    }

    public getId(): string
    {
        return this.id;
    }

    public setId(id: string)
    {
        this.id = id;
    }

    public getIid(): string
    {
        return this.iid;
    }

    public setIid(iid: string)
    {
        this.iid = iid;
    }

    public getParentIid(): string
    {
        return this.parentIid;
    }

    public setParentIid(parentIid: string)
    {
        this.parentIid = parentIid;
    }

    public getInputParameters(): any
    {
        return this.inputParameters;
    }

    public setInputParameters(inputParameters: any)
    {
        this.inputParameters = inputParameters;
    }
}