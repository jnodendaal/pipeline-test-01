import 
{ 
    DataRequirementInstance, 
    SectionRequirement, 
    PropertyRequirement, 
    ValueRequirement, 
    MeasuredNumberRequirement, 
    MeasuredRangeRequirement,
    FieldRequirement, 
    CompositeRequirement,
    DateRequirement,
    DateTimeRequirement
} from './data-requirement.model';
import { RecordAccessLayer } from './data-record-access.model';
import { FileDataQualityReport, DataQualityMetrics } from './data-record-quality.model';
import * as moment from 'moment';
import { Moment } from 'moment';
import { SimpleDateFormat } from '../utility/date.utility';

export const STANDARD_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
export const STANDARD_DATE_FORMAT = "yyyy-MM-dd";

/*
    Data Record Model
*/
export class DataRecord
{
    parentRecord: DataRecord;
    recordId: string;
    valueString: string;
    sections: RecordSection[];
    subRecords: DataRecord[];
    drInstance: DataRequirementInstance;
    access: RecordAccessLayer;

    constructor()
    {
        this.sections = new Array();
        this.subRecords = new Array();
    }

    fromJson(jsonRecord: any)
    {
        this.recordId = jsonRecord.recordId;
        this.drInstance = new DataRequirementInstance();
        this.drInstance.fromJson(jsonRecord.drInstance);

        // Parse all sections contained by this record.
        for (let jsonSection of jsonRecord.sections)
        {
            let section = new RecordSection();
            section.parentRecord = this;
            section.fromJson(jsonSection);
            this.addSection(section);
        }

        // Parse all sub-records contained by this record.
        if (jsonRecord.subRecords)
        {
            for (let jsonSubRecord of jsonRecord.subRecords)
            {
                let subRecord = new DataRecord();
                subRecord.fromJson(jsonSubRecord);
                this.addSubRecord(subRecord);
            }
        }

        // Parse access layers.
        if (jsonRecord.access)
        {
            this.access = new RecordAccessLayer().fromJson(jsonRecord.access);
        }

        // Refresh this record's value string.
        this.refreshValueString(new ValueStringSettings());
    }

    public refreshValueString(settings: ValueStringSettings): string
    {
        let propertyAdded: boolean;

        propertyAdded = false;
        this.valueString = this.drInstance.term + ": ";
        for (let property of this.getProperties())
        {
            let propertyValueString: string;

            propertyValueString = property.refreshValueString(settings);
            if (propertyValueString)
            {
                if (propertyAdded) this.valueString += ", ";
                this.valueString += property.refreshValueString(settings);
                propertyAdded = true;
            }
        }

        return this.valueString;
    }

    public getFileId(): string
    {
        let rootRecord: DataRecord;

        // This must be fixed to properly check that the root record is a DataFile.
        rootRecord = this.getRootRecord();
        return rootRecord != null ? rootRecord.recordId : null;
    }

    public getFile(): DataRecord
    {
        let nextRecord: DataRecord;

        nextRecord = this;
        while (nextRecord.parentRecord)
        {
            nextRecord = nextRecord.parentRecord;
        }

        return nextRecord;
    }

    public getRootRecord(): DataRecord
    {
        let nextRecord: DataRecord;

        nextRecord = this;
        while (nextRecord.parentRecord)
        {
            nextRecord = nextRecord.parentRecord;
        }

        return nextRecord;
    }

    /**
     * Returns all records in the path that must be traversed from the root record to this current record.
     * The first record in the returned list will be the root record and the last will be this record.
     * If this record is the root, then only this record will be in the returned list.
     */
    public getRecordPath(): DataRecord[]
    {
        let recordPath: DataRecord[];
        let parent: DataRecord;

        recordPath = new Array();
        parent = this.parentRecord;
        while (parent != null)
        {
            recordPath.push(parent);
            parent = parent.parentRecord;
        }

        recordPath = recordPath.reverse();
        recordPath.push(this);
        return recordPath;
    }

    public addSubRecord(record: DataRecord)
    {
        record.parentRecord = this;
        this.subRecords.push(record);
    }

    public removeSubRecord(recordId: string): DataRecord
    {
        for (let index = 0; index < this.subRecords.length; index++)
        {
            let subRecord: DataRecord;

            subRecord = this.subRecords[index];
            if (subRecord.recordId == recordId)
            {
                this.subRecords.splice(index, 1);
                return subRecord;
            }
        }

        return null;
    }

    public getSubRecord(recordId: string): DataRecord
    {
        for (let subRecord of this.subRecords)
        {
            if (subRecord.recordId = recordId) return subRecord;
        }

        return null;
    }

    public getRecord(recordId: string): DataRecord
    {
        let recordQueue: DataRecord[];
        
        recordQueue = new Array();
        recordQueue.push(this.getRootRecord());
        while (recordQueue.length > 0)
        {
            let nextRecord: DataRecord;

            nextRecord = recordQueue.pop();
            if (nextRecord.recordId == recordId)
            {
                return nextRecord;
            }
            else
            {
                recordQueue.push(...nextRecord.subRecords);
            }
        }
        
        return null;
    }

    public getRecords(): DataRecord[]
    {
        let recordQueue: DataRecord[];
        let records: DataRecord[];
        
        records = new Array();
        recordQueue = new Array();
        recordQueue.push(this.getRootRecord());
        while (recordQueue.length > 0)
        {
            let nextRecord: DataRecord;

            nextRecord = recordQueue.pop();
            records.push(nextRecord);
            recordQueue.push(...nextRecord.subRecords);
        }
        
        return records;
    }

    public getSection(sectionId: string): RecordSection
    {
        for (let section of this.sections)
        {
            if (section.sectionId == sectionId)    
            {
                return section;
            }
        }

        return null;
    }

    public addSection(section: RecordSection)
    {
        section.setRecord(this);
        this.sections.push(section);
    }

    public getProperty(propertyId: string): RecordProperty
    {
        for (let section of this.sections)
        {
            for (let property of section.properties)
            {
                if (property.propertyId == propertyId)    
                {
                    return property;
                }
            }
        }

        return null;
    }

    public getPropertyDataCount(): number
    {
        let count: number;

        count = 0;
        for (let section of this.sections)
        {
            count += section.getPropertyDataCount();
        }

        return count;
    }

    public getCharacteristicDataCount(): number
    {
        let count: number;

        count = 0;
        for (let characteristic of this.getCharacteristics())
        {
            if (characteristic.hasContent(true)) count++;
        }

        return count;
    }

    public getCharacteristicValidCount(): number
    {
        let count: number;

        count = 0;
        for (let characteristic of this.getCharacteristics())
        {
            if (characteristic.hasContent(false)) count++;
        }

        return count;
    }

    public getProperties(): RecordProperty[]
    {
        let properties: RecordProperty[];

        properties = new Array();
        for (let section of this.sections)
        {
            properties.push(...section.properties);
        }

        return properties;
    }

    public getCharacteristics(): RecordProperty[]
    {
        let characteristics: RecordProperty[];

        characteristics = new Array();
        for (let characteristicId of this.drInstance.getCharacteristicIds())
        {
            let characteristic;

            characteristic = this.getProperty(characteristicId);
            if (characteristic) characteristics.push(characteristic);
        }

        return characteristics;
    }

    public getDataQualityMetrics(includeDescendants: boolean): DataQualityMetrics
    {
        let metrics: DataQualityMetrics;

        // Add the data quality metrics of this record.
        metrics = new DataQualityMetrics();
        metrics.recordCount = 1;
        metrics.propertyCount = this.drInstance.getPropertyCount();
        metrics.propertyDataCount = this.getPropertyDataCount();
        metrics.characteristicCount = this.drInstance.getCharacteristicCount();
        metrics.characteristicDataCount = this.getCharacteristicDataCount();
        metrics.characteristicValidCount = this.getCharacteristicValidCount();

        // Add the data quality metrics of this record's descendants if required.
        if (includeDescendants)
        {
            for (let subRecord of this.subRecords)
            {
                metrics.add(subRecord.getDataQualityMetrics(includeDescendants));
            }
        }

        return metrics;
    }

    public getFileDataQualityReport(): FileDataQualityReport
    {
        let dqReport: FileDataQualityReport;

        // Add the data quality metrics of this record.
        dqReport = new FileDataQualityReport(this.recordId);
        dqReport.metrics = this.getDataQualityMetrics(true);
        return dqReport;
    }
}

export class RecordSection
{
    parentRecord: DataRecord;
    sectionId: string;
    properties: RecordProperty[];

    constructor()
    {
        this.properties = new Array();
    }

    fromJson(jsonSection: any)
    {
        this.sectionId = jsonSection.sectionId;
        for (let jsonProperty of jsonSection.properties)
        {
            let property = new RecordProperty(null);
            property.parentRecord = this.parentRecord;
            property.fromJson(jsonProperty);
            this.addProperty(property);
        }
    }

    public getRecord(): DataRecord
    {
        return this.parentRecord;
    }

    public setRecord(record: DataRecord)
    {
        this.parentRecord = record;
        for (let property of this.properties)
        {
            property.setRecord(record);
        }
    }

    public getProperty(propertyId: string): RecordProperty
    {
        for (let property of this.properties)
        {
            if (property.propertyId == propertyId)    
            {
                return property;
            }
        }

        return null;
    }

    public addProperty(property: RecordProperty)
    {
        property.setRecord(this.parentRecord);
        this.properties.push(property);
    }

    public getPropertyDataCount(): number
    {
        let count: number;

        count = 0;
        for (let property of this.properties)
        {
            if (property.hasContent(true))    
            {
                count++;
            }
        }

        return count;
    }
}

export class RecordProperty
{
    parentRecord: DataRecord;
    propertyId: string;
    value: RecordValue;
    fft: string;
    valueString: string;

    constructor(propertyId: string)
    {
        this.propertyId = propertyId;
    }

    fromJson(jsonProperty: any)
    {
        this.propertyId = jsonProperty.propertyId;
        this.fft = jsonProperty.fft;
        this.value = RecordValue.createFromJson(jsonProperty.value);
        if (this.value) 
        {
            this.value.setParentProperty(this);
        }
    }

    public setValue(value: RecordValue)
    {
        this.value = value;
        if (value != null)
        {
            this.value.setParentProperty(this);
        }
    }

    public hasContent(includeFft: boolean): boolean
    {
        if ((this.fft) && (includeFft)) return true; // If fft should be included and we do have a non-empty fft value, return true.
        else return (this.value != null && this.value.hasContent()); // Check for value content.
    }

    public createNewValue(): RecordValue
    {
        this.value = RecordValue.createValue(this.getValueRequirement());
        if (this.value) 
        {
            this.value.setParentProperty(this);
            return this.value;
        }
        else return null;
    }

    public getValueRequirement(): ValueRequirement
    {
        let propertyRequirement;
        
        propertyRequirement = this.parentRecord.drInstance.getProperty(this.propertyId);
        return propertyRequirement.value;
    }

    public getRecord(): DataRecord
    {
        return this.parentRecord;
    }

    public setRecord(record: DataRecord)
    {
        this.parentRecord = record;
    }

    public fill(includeValues: boolean)
    {
        let valueRequirement: ValueRequirement;

        valueRequirement = this.parentRecord.drInstance.getProperty(this.propertyId).value;
        if ((includeValues) && (this.value == null))
        {
            this.setValue(RecordValue.createValue(valueRequirement));                
        }

        if (this.value instanceof Composite)
        {
            let compositeRequirement: CompositeRequirement;
            let composite: Composite;

            composite = this.value as Composite;
            compositeRequirement = valueRequirement as CompositeRequirement;
            for (let fieldRequirement of compositeRequirement.fields)
            {
                let field: Field;

                field = composite.getField(fieldRequirement.fieldId);
                if (field == null)
                {
                    field = RecordValue.createValue(fieldRequirement) as Field;
                    composite.addField(field);
                }

                if ((field.value == null) && (includeValues))
                {
                    field.setValue(RecordValue.createValue(fieldRequirement.value));
                }
            }
        }
    }

    public refreshValueString(settings: ValueStringSettings): string
    {
        if (this.value != null)
        {
            this.valueString = this.value.getValueString(settings);
            return this.valueString;
        }
        else
        {
            this.valueString = null;
            return this.valueString;
        }
    }
}

export abstract class RecordValue
{
    type: string;
    parentProperty: RecordProperty;
    parentRecord: DataRecord;
    parentField: Field;

    constructor(type: string)
    {
        this.type = type;
    }

    abstract hasContent(): boolean;
    abstract setContent(valueContent: ValueContent): void;
    abstract getValueString(settings: ValueStringSettings): string;

    public setParentProperty(property: RecordProperty)
    {
        this.parentProperty = property;
        this.parentRecord = property != null ? property.getRecord() : null;
    }

    public setParentField(field: Field)
    {
        this.parentField = field;
        this.parentProperty = field != null ? field.parentProperty : null;
    }

    public getParentRecord(): DataRecord
    {
        return this.parentProperty != null ? this.parentProperty.getRecord() : null;
    }

    public getParentProperty(): RecordProperty
    {
        return this.parentProperty;
    }

    public getParentField(): Field
    {
        return this.parentField;
    }

    public getPropertyId(): string
    {
        return this.parentProperty != null ? this.parentProperty.propertyId : null;
    }

    public getFieldId(): string
    {
        return this.parentField != null ? this.parentField.fieldId : null;
    }

    /**
     * Returns the ValueRequirement for this RecordValue if it is available in the
     * current object model.  Returns null if the applicable requirement is not available.
     */
    public getRequirement(): ValueRequirement
    {
        if (this.parentProperty)
        {
            let valueRequirement: ValueRequirement;
            
            valueRequirement = this.parentProperty.getValueRequirement();
            if (valueRequirement instanceof CompositeRequirement)
            {
                let compositeRequirement: CompositeRequirement;

                compositeRequirement = valueRequirement as CompositeRequirement;
                if (this instanceof Composite)
                {
                    return compositeRequirement;
                }
                else if (this instanceof Field)
                {
                    return compositeRequirement.getField(this.getFieldId());
                }
                else
                {
                    let fieldRequirement: FieldRequirement;

                    fieldRequirement = compositeRequirement.getField(this.getFieldId());
                    if (fieldRequirement)
                    {
                        return fieldRequirement.value;
                    }
                    else return null;
                }
            }
            else return valueRequirement;
        }
    }

    fromJson(jsonValue: any)
    {
    }

    public static createFromJson(jsonValue: any): RecordValue
    {
        if (jsonValue)
        {
            switch (jsonValue.type)
            {
                case "STRING":
                {
                    let value = new StringValue();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "TEXT":
                {
                    let value = new TextValue();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "BOOLEAN":
                {
                    let value = new BooleanValue();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "FIELD":
                {
                    let value = new Field(null);
                    value.fromJson(jsonValue);
                    return value;
                }
                case "COMPOSITE":
                {
                    let value = new Composite();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "CONTROLLED_CONCEPT":
                {
                    let value = new ControlledConcept();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "ATTACHMENT":
                {
                    let value = new AttachmentList();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DOCUMENT_REFERENCE":
                {
                    let value = new DocumentReferenceList();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "MEASURED_NUMBER":
                {
                    let value = new MeasuredNumber();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "MEASURED_RANGE":
                {
                    let value = new MeasuredRange();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "UNIT_OF_MEASURE":
                {
                    let value = new UnitOfMeasure();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "INTEGER":
                {
                    let value = new IntegerValue();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "REAL":
                {
                    let value = new RealValue();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "NUMBER":
                {
                    let value = new NumberValue();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DATE_TIME":
                {
                    let value = new DateTime();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DATE":
                {
                    let value = new DateValue();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "TIME":
                {
                    let value = new Time();
                    value.fromJson(jsonValue);
                    return value;
                }
                default:
                {
                    throw new TypeError("Unsupported data type '" + jsonValue.type + "' in JSON object: " + JSON.stringify(jsonValue));
                }
            }
        }
        else
        {
            return null;
        } 
    }

    public static createValue(valueRequirement: ValueRequirement): RecordValue
    {
        switch (valueRequirement.type)
        {
            case "STRING":
            {
                let value = new StringValue();
                return value;
            }
            case "TEXT":
            {
                let value = new TextValue();
                return value;
            }
            case "BOOLEAN":
            {
                let value = new BooleanValue();
                return value;
            }
            case "FIELD":
            {
                let fieldRequirement: FieldRequirement;
                let field: Field;

                fieldRequirement = valueRequirement as FieldRequirement;
                field = new Field(fieldRequirement.fieldId);
                field.setValue(RecordValue.createValue(fieldRequirement.value));
                return field;
            }
            case "COMPOSITE":
            {
                let compositeRequirement: CompositeRequirement;
                let composite: Composite;

                compositeRequirement = valueRequirement as CompositeRequirement;
                composite = new Composite();
                for (let fieldRequirement of compositeRequirement.fields)
                {
                    composite.addField(RecordValue.createValue(fieldRequirement) as Field);
                }

                return composite;
            }
            case "CONTROLLED_CONCEPT":
            {
                let value = new ControlledConcept();
                return value;
            }
            case "ATTACHMENT":
            {
                let value = new AttachmentList();
                return value;
            }
            case "DOCUMENT_REFERENCE":
            {
                let value = new DocumentReferenceList();
                return value;
            }
            case "UNIT_OF_MEASURE":
            {
                let value = new UnitOfMeasure();
                return value;
            }
            case "MEASURED_NUMBER":
            {
                let measuredNumberRequirement: MeasuredNumberRequirement;
                let value: MeasuredNumber;

                measuredNumberRequirement = valueRequirement as MeasuredNumberRequirement;

                value = new MeasuredNumber();
                value.number = RecordValue.createValue(measuredNumberRequirement.number) as NumberValue;
                value.unitOfMeasure = RecordValue.createValue(measuredNumberRequirement.unitOfMeasure) as UnitOfMeasure;
                return value;
            }
            case "MEASURED_RANGE":
            {
                let measuredRangeRequirement: MeasuredRangeRequirement;
                let value = new MeasuredRange();

                measuredRangeRequirement = valueRequirement as MeasuredRangeRequirement;

                value = new MeasuredRange();
                value.lowerBoundNumber = RecordValue.createValue(measuredRangeRequirement.lowerBoundNumber) as LowerBoundNumber;
                value.upperBoundNumber = RecordValue.createValue(measuredRangeRequirement.upperBoundNumber) as UpperBoundNumber;
                value.unitOfMeasure = RecordValue.createValue(measuredRangeRequirement.unitOfMeasure) as UnitOfMeasure;
                return value;
            }
            case "INTEGER":
            {
                let value = new IntegerValue();
                return value;
            }
            case "REAL":
            {
                let value = new RealValue();
                return value;
            }
            case "NUMBER":
            {
                let value = new NumberValue();
                return value;
            }
            case "LOWER_BOUND_NUMBER":
            {
                let value = new LowerBoundNumber();
                return value;
            }
            case "UPPER_BOUND_NUMBER":
            {
                let value = new UpperBoundNumber();
                return value;
            }
            case "DATE_TIME":
            {
                let value = new DateTime();
                return value;
            }
            case "DATE":
            {
                let value = new DateValue();
                return value;
            }
            case "TIME":
            {
                let value = new Time();
                return value;
            }
            default:
            {
                console.log(valueRequirement);
                throw new TypeError("Unsupported data type '" + valueRequirement.type + "' in ValueRequirement");
            }
        }
    }
}

export class StringValue extends RecordValue
{
    string: string;

    constructor()
    {
        super("STRING");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.string = jsonValue.string;
    }

    getContent(): StringContent
    {
        let content: StringContent;

        content = new StringContent();
        content.string = this.string;
        return content;
    }

    setContent(content: StringContent)
    {
        this.string = content != null ? content.string : null;
    }

    public hasContent(): boolean
    {
        return (this.string != null) && (this.string.trim().length > 0);
    }

    public getValueString(): string
    {
        return this.string;
    }
}

export class TextValue extends RecordValue
{
    text: string;

    constructor()
    {
        super("TEXT");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.text = jsonValue.text;
    }

    getContent(): TextContent
    {
        let content: TextContent;

        content = new TextContent();
        content.text = this.text;
        return content;
    }
    
    setContent(content: TextContent)
    {
        this.text = content != null ? content.text : null;
    }

    public hasContent(): boolean
    {
        return (this.text != null) && (this.text.trim().length > 0);
    }

    public getValueString(): string
    {
        return this.text;
    }
}

export class BooleanValue extends RecordValue
{
    public static TYPE_ID: string = "BOOLEAN";
    value: boolean;

    constructor()
    {
        super(BooleanValue.TYPE_ID);
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.value = jsonValue.value;
    }

    getContent(): BooleanContent
    {
        let content: BooleanContent;

        content = new BooleanContent();
        content.value = this.value != null ? this.value.toString() : null;
        return content;
    }

    setContent(content: BooleanContent)
    {
        this.value = content != null ? (content.value != null ? ("true" == content.value) : null) : null;
    }

    public hasContent(): boolean
    {
        return this.value != null;
    }

    public getValueString(settings: ValueStringSettings): string
    {
        if (settings.isTypeIncluded(this.type))
        {
            if (this.value == true)
            {
                return "true";
            }
            else if (this.value == false)
            {
                return "false";
            }
            else if (!this.value)
            {
                return "undefined"
            }
        }
        else return null;
    }
}

export class Time extends RecordValue
{
    time: string;

    constructor()
    {
        super("TIME");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.time = jsonValue.time;
    }

    getContent(): TimeContent
    {
        let content: TimeContent;

        content = new TimeContent();
        content.time = this.time;
        return content;
    }

    setContent(content: TimeContent)
    {
        this.time = content != null ? content.time : null;
    }

    public hasContent(): boolean
    {
        return (this.time != null);
    }

    public getValueString(): string
    {
        return this.time;
    }
}

export class DateTime extends RecordValue
{
    dateTime: string;

    constructor()
    {
        super("DATE_TIME");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.dateTime = jsonValue.dateTime;
    }

    getContent(): DateTimeContent
    {
        let content: DateTimeContent;

        content = new DateTimeContent();
        content.dateTime = this.dateTime;
        return content;
    }

    setContent(content: DateTimeContent)
    {
        this.dateTime = content != null ? content.dateTime : null;
    }

    public hasContent(): boolean
    {
        return (this.dateTime != null);
    }

    public getValueString(): string
    {
        let requirement: DateTimeRequirement;

        requirement = this.getRequirement() as DateTimeRequirement;
        if (requirement)
        {
            // If the requirement is available, use the specified format.
            if (requirement.dateTimeFormatPattern)
            {
                return moment(new Date(parseInt(this.dateTime))).format(SimpleDateFormat.toMomentFormat(requirement.dateTimeFormatPattern))
            }
            else
            {
                return moment(new Date(parseInt(this.dateTime))).format(SimpleDateFormat.toMomentFormat(STANDARD_DATE_TIME_FORMAT));
            }
        }
        else
        {
            // Requirement not available so use default formatting.
            return moment(new Date(parseInt(this.dateTime))).format(SimpleDateFormat.toMomentFormat(STANDARD_DATE_TIME_FORMAT));
        }
    }
}

export class DateValue extends RecordValue
{
    date: string;

    constructor()
    {
        super("DATE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.date = jsonValue.date;
    }

    getContent(): DateContent
    {
        let content: DateContent;

        content = new DateContent();
        content.date = this.date;
        return content;
    }

    setContent(content: DateContent)
    {
        this.date = content != null ? content.date : null;
    }

    public hasContent(): boolean
    {
        return (this.date != null);
    }

    public getValueString(): string
    {
        let requirement: DateRequirement;

        requirement = this.getRequirement() as DateRequirement;
        if (requirement)
        {
            // If the requirement is available, use the specified format.
            if (requirement.dateTimeFormatPattern)
            {
                return moment(new Date(parseInt(this.date))).format(SimpleDateFormat.toMomentFormat(requirement.dateTimeFormatPattern))
            }
            else
            {
                return moment(new Date(parseInt(this.date))).format(SimpleDateFormat.toMomentFormat(STANDARD_DATE_FORMAT));
            }
        }
        else
        {
            // Requirement not available so use default formatting.
            return moment(new Date(parseInt(this.date))).format(SimpleDateFormat.toMomentFormat(STANDARD_DATE_FORMAT));
        }
    }
}

export class ControlledConcept extends RecordValue
{
    conceptId: string;
    term: string;
    definition: string;
    code: string;
    abbreviation: string;

    constructor()
    {
        super("CONTROLLED_CONCEPT");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.conceptId = jsonValue.conceptId;
        this.term = jsonValue.term;
        this.definition = jsonValue.definition;
        this.code = jsonValue.code;
    }

    public hasContent(): boolean
    {
        return this.conceptId != null;
    }

    public getContent(): ControlledConceptContent
    {
        let content: ControlledConceptContent;

        content = new ControlledConceptContent();
        content.conceptId = this.conceptId;
        content.code = this.code;
        content.term = this.term;
        content.definition = this.definition;
        return content;
    }

    public setContent(content: ControlledConceptContent)
    {
        if (content)
        {
            this.conceptId = content.conceptId;
            this.code = content.code;
            this.term = content.term;
            this.definition = content.definition;
        }
        else
        {
            this.conceptId = null;
            this.code = null;
            this.term = null;
            this.definition = null;
        }
    }

    public getValueString(): string
    {
        return this.term;
    }
}

export class Composite extends RecordValue
{
    fields: Field[];

    constructor()
    {
        super("COMPOSITE");
        this.fields = new Array();
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        for (let field of jsonValue.fields)
        {
            let newField: Field;

            newField = new Field(field.fieldId);
            newField.fromJson(field);
            this.addField(newField);
        }
    }

    public getField(fieldId: string): Field
    {
        for (let field of this.fields)
        {
            if (field.fieldId == fieldId) return field;
        }

        return null;
    }

    public addField(field: Field)
    {
        this.fields.push(field);
        field.setParentProperty(this.parentProperty);
    }

    public hasContent(): boolean
    {
        for (let field of this.fields)
        {
            if (field.hasContent()) return true;
        }

        return false;
    }

    public setParentProperty(property: RecordProperty)
    {
        this.parentProperty = property;
        this.parentRecord = property != null ? property.getRecord() : null;
        for (let field of this.fields)
        {
            field.setParentProperty(property);
        }
    }

    public setContent(content: CompositeContent)
    {
    }

    public getValueString(settings: ValueStringSettings): string
    {
        let valueString: string;

        valueString = "";
        for (let field of this.fields)
        {
            let fieldValueString;

            fieldValueString = field.getValueString(settings);
            if (fieldValueString != null)
            {
                if (valueString.length > 0) valueString += ",";
                valueString += fieldValueString;
            }
        }

        return valueString.length > 0 ? valueString : null;
    }
}

export class Field extends RecordValue
{
    fieldId: string;
    value: RecordValue;

    constructor(fieldId: string)
    {
        super("FIELD");
        this.fieldId = fieldId;
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.fieldId = jsonValue.fieldId;
        this.setValue(RecordValue.createFromJson(jsonValue.value));
    }

    public setValue(value: RecordValue)
    {
        this.value = value;
        if (this.value != null)
        {
            this.value.setParentField(this);
        }
    }

    public setContent(content: FieldContent)
    {
        if (this.value)
        {
            this.value.setContent(content != null ? content.value : null);
        }
    }

    public hasContent(): boolean
    {
        return ((this.value) && (this.value.hasContent()));
    }

    public setParentProperty(property: RecordProperty)
    {
        this.parentProperty = property;
        this.parentRecord = property != null ? property.getRecord() : null;
        if (this.value != null) this.value.setParentProperty(property);
    }

    public getValueString(settings: ValueStringSettings): string
    {
        return this.value != null ? this.value.getValueString(settings) : null;
    }
}

export class DocumentReferenceList extends RecordValue
{
    references: DocumentReference[];

    constructor()
    {
        super("DOCUMENT_REFERENCE");
        this.references = new Array();
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        for (let reference of jsonValue.references)
        {
            let newReference: DocumentReference;

            newReference = new DocumentReference(reference.recordId);
            newReference.fromJson(reference);
            this.addReference(newReference);
        }
    }

    public hasContent(): boolean
    {
        return this.references.length > 0;
    }

    public getContent(): DocumentReferenceListContent
    {
        let content: DocumentReferenceListContent;

        content = new DocumentReferenceListContent();
        for (let reference of this.references)
        {
            content.addReference(reference.getContent());
        }

        return content;
    }

    public setContent(content: DocumentReferenceListContent)
    {
        this.references = new Array();
        if (content)
        {
            for (let referenceContent of content.references)
            {
                let newReference: DocumentReference;

                newReference = new DocumentReference(referenceContent.conceptId);
                newReference.setContent(referenceContent);
                this.addReference(newReference);
            }
        }
    }

    public addReference(reference: DocumentReference)
    {
        this.references.push(reference);
    }

    public removeReference(conceptId: string): DocumentReference
    {
        for (let index = 0; index < this.references.length; index++)
        {
            let reference: DocumentReference;

            reference = this.references[index];
            if (reference.conceptId == conceptId)
            {
                this.references.splice(index, 1);
                return reference;
            }
        }

        return null;
    }

    public getValueString(): string
    {
        return null;
    }
}

export class DocumentReference
{
    referenceType: string;
    conceptId: string;
    code: string;
    term: string;
    definition: string;
    abbreviation: string;
    
    constructor(recordId: string)
    {
        this.conceptId = recordId;
    }

    fromJson(jsonValue: any)
    {
        this.referenceType = jsonValue.referenceType;
        this.conceptId = jsonValue.conceptId;
        this.term = jsonValue.term;
        this.code = jsonValue.code;
        this.definition = jsonValue.definition;
        this.abbreviation = jsonValue.abbreviation;
    }

    public getContent(): DocumentReferenceContent
    {
        let content: DocumentReferenceContent;

        content = new DocumentReferenceContent();
        content.referenceType = this.referenceType;
        content.conceptId = this.conceptId;
        content.code = this.code;
        content.term = this.term;
        content.definition = this.definition;
        return content;
    }

    public setContent(content: DocumentReferenceContent)
    {
        if (content)
        {
            this.referenceType = content.referenceType;
            this.conceptId = content.conceptId;
            this.code = content.code;
            this.term = content.term;
            this.definition = content.definition;
        }
        else
        {
            this.referenceType = null;
            this.conceptId = null;
            this.code = null;
            this.term = null;
            this.definition = null;
        }
    }
}

export class AttachmentList extends RecordValue
{
    attachments: Attachment[];

    constructor()
    {
        super("ATTACHMENT");
        this.attachments = new Array();
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        for (let attachment of jsonValue.attachments)
        {
            let newAttachment: Attachment;

            newAttachment = new Attachment(attachment.attachmentId);
            newAttachment.fromJson(attachment);
            this.addAttachment(newAttachment);
        }
    }

    public setContent(content: AttachmentListContent)
    {
        this.attachments = new Array();
        if (content)
        {
            for (let attachmentContent of content.attachments)
            {
                let newAttachment: Attachment;

                newAttachment = new Attachment(attachmentContent.attachmentId);
                newAttachment.setContent(attachmentContent);
                this.addAttachment(newAttachment);
            }
        }
    }

    public getContent(): AttachmentListContent
    {
        let content: AttachmentListContent;

        content = new AttachmentListContent();
        for (let attachment of this.attachments)
        {
            content.addAttachment(attachment.getContent());
        }

        return content;
    }

    public hasContent(): boolean
    {
        return this.attachments.length > 0;
    }

    public addAttachment(attachment: Attachment)
    {
        this.attachments.push(attachment);
    }

    public removeAttachment(attachmentId: string): Attachment
    {
        for (let index = 0; index < this.attachments.length; index++)
        {
            let attachment: Attachment;

            attachment = this.attachments[index];
            if (attachment.attachmentId == attachmentId)
            {
                this.attachments.splice(index, 1);
                return attachment;
            }
        }

        return null;
    }

    public getValueString(): string
    {
        return null;
    }
}

export class Attachment
{
    attachmentId: string;
    fileContextId: string;
    fileContextInstanceId: string;
    filePath: string;
    fileSize: number;
    md5: string;
    mediaType: string;

    constructor(attachmentId: string)
    {
        this.attachmentId = attachmentId;
    }

    fromJson(jsonValue: any)
    {
        this.attachmentId = jsonValue.attachmentId;
        this.fileContextId = jsonValue.fileContextId;
        this.fileContextInstanceId = jsonValue.fileContextInstanceId;
        this.filePath = jsonValue.filePath;
        this.fileSize = jsonValue.fileSize;
        this.md5 = jsonValue.md5;
        this.mediaType = jsonValue.mediaType;
    }

    getContent(): AttachmentContent
    {
        let content: AttachmentContent;

        content = new AttachmentContent();
        content.attachmentId = this.attachmentId;
        content.fileContextId = this.fileContextId;
        content.fileContextIid = this.fileContextInstanceId;
        content.filePath = this.filePath;
        content.fileSize = this.fileSize;
        content.md5 = this.md5;
        content.mediaType = this.mediaType;
        return content;
    }

    setContent(content: AttachmentContent)
    {
        if (content)
        {
            this.attachmentId = content.attachmentId;
            this.fileContextId = content.fileContextId;
            this.fileContextInstanceId = content.fileContextIid;
            this.filePath = content.filePath;
            this.fileSize = content.fileSize;
            this.md5 = content.md5;
            this.mediaType = content.mediaType;
        }
        else
        {
            this.attachmentId = null;
            this.fileContextId = null;
            this.fileContextInstanceId = null;
            this.filePath = null;
            this.fileSize = null;
            this.md5 = null;
            this.mediaType = null;
        }
    }   
    
    public getFilename(): string
    {
        if (this.filePath)
        {
            let lastSlashIndex: number;
            let lastBackslashIndex: number;

            lastSlashIndex = this.filePath.lastIndexOf("/");
            lastBackslashIndex = this.filePath.lastIndexOf("\\");
            
            if (lastSlashIndex > lastBackslashIndex)
            {
                return this.filePath.slice(lastSlashIndex+1, this.filePath.length);
            }
            else if (lastBackslashIndex > lastSlashIndex)
            {
                return this.filePath.slice(lastBackslashIndex+1, this.filePath.length);
            }
            else
            {
                // The only way that the indices can be equal is if they are equal to -1 (no slashes found).
                return this.filePath;            
            }
        }
        else return null;
    }
}

export class MeasuredNumber extends RecordValue
{
    number: NumberValue;
    unitOfMeasure: UnitOfMeasure;
    qualifierOfMeasure: QualifierOfMeasure;

    constructor()
    {
        super("MEASURED_NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.number = new NumberValue();
        this.number.number = jsonValue.number;
        this.unitOfMeasure = new UnitOfMeasure();
        this.unitOfMeasure.conceptId = jsonValue.unitOfMeasureId;
        this.unitOfMeasure.term = jsonValue.unitOfMeasureTerm;
        this.unitOfMeasure.abbreviation = jsonValue.unitOfMeasureAbbreviation;
        this.unitOfMeasure.definition = jsonValue.unitOfMeasureDefinition;
        this.qualifierOfMeasure = new QualifierOfMeasure();
        this.qualifierOfMeasure.conceptId = jsonValue.qualifierOfMeasureId;
        this.qualifierOfMeasure.term = jsonValue.qualifierOfMeasureTerm;
        this.qualifierOfMeasure.abbreviation = jsonValue.qualifierOfMeasureAbbreviation;
        this.qualifierOfMeasure.definition = jsonValue.qualifierOfMeasureDefinition;
    }

    public setParentProperty(property: RecordProperty)
    {
        this.parentProperty = property;
        this.parentRecord = property != null ? property.getRecord() : null;
        if (this.unitOfMeasure) this.unitOfMeasure.setParentProperty(property);
    }

    public setUnitOfMeasure(uom: UnitOfMeasure)
    {
        uom.parentProperty = this.parentProperty;
        uom.parentRecord = this.parentRecord;
        this.unitOfMeasure = uom;
    }

    public hasContent(): boolean
    {
        if ((this.number) && (this.number.hasContent())) return true
        else if ((this.unitOfMeasure) && (this.unitOfMeasure.hasContent())) return true
        else if ((this.qualifierOfMeasure) && (this.qualifierOfMeasure.hasContent())) return true
        else return false;
    }

    public getContent(): MeasuredNumberContent
    {
        let content: MeasuredNumberContent;

        content = new MeasuredNumberContent();
        content.number = this.number.getContent();
        content.unitOfMeasure = this.unitOfMeasure.getContent();
        return content;
    }

    public setContent(content: MeasuredNumberContent)
    {
        this.number.setContent(content != null ? content.number : null);
        this.unitOfMeasure.setContent(content != null ? content.unitOfMeasure : null);
        this.qualifierOfMeasure.setContent(content != null ? content.qualifierOfMeasure : null);
    }

    public getValueString(): string
    {
        let valueString: string;

        valueString = "";
        if (this.number)
        {
            let numberValueString;

            numberValueString = this.number.getValueString();
            if (numberValueString) valueString += numberValueString;
        } 

        if (this.unitOfMeasure)
        {
            let uomValueString;

            uomValueString = this.unitOfMeasure.getValueString();
            if (uomValueString) valueString += uomValueString;
        } 

        return valueString.length > 0 ? valueString : null;
    }
}

export class MeasuredRange extends RecordValue
{
    lowerBoundNumber: LowerBoundNumber;
    upperBoundNumber: UpperBoundNumber;
    unitOfMeasure: UnitOfMeasure;
    qualifierOfMeasure: QualifierOfMeasure;

    constructor()
    {
        super("MEASURED_RANGE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.lowerBoundNumber = new LowerBoundNumber();
        this.lowerBoundNumber.value = jsonValue.lowerBoundNumber;
        this.upperBoundNumber = new UpperBoundNumber();
        this.upperBoundNumber.value = jsonValue.upperBoundNumber;
        this.unitOfMeasure = new UnitOfMeasure();
        this.unitOfMeasure.conceptId = jsonValue.unitOfMeasureId;
        this.unitOfMeasure.term = jsonValue.unitOfMeasureTerm;
        this.unitOfMeasure.abbreviation = jsonValue.unitOfMeasureAbbreviation;
        this.unitOfMeasure.definition = jsonValue.unitOfMeasureDefinition;
        this.qualifierOfMeasure = new QualifierOfMeasure();
        this.qualifierOfMeasure.conceptId = jsonValue.qualifierOfMeasureId;
        this.qualifierOfMeasure.term = jsonValue.qualifierOfMeasureTerm;
        this.qualifierOfMeasure.abbreviation = jsonValue.qualifierOfMeasureAbbreviation;
        this.qualifierOfMeasure.definition = jsonValue.qualifierOfMeasureDefinition;
    }

    public setParentProperty(property: RecordProperty)
    {
        this.parentProperty = property;
        this.parentRecord = property != null ? property.getRecord() : null;
        if (this.unitOfMeasure) this.unitOfMeasure.setParentProperty(property);
    }

    public setUnitOfMeasure(uom: UnitOfMeasure)
    {
        uom.parentProperty = this.parentProperty;
        uom.parentRecord = this.parentRecord;
        this.unitOfMeasure = uom;
    }

    public hasContent(): boolean
    {
        if ((this.upperBoundNumber) && (this.upperBoundNumber.hasContent())) return true
        else if ((this.unitOfMeasure) && (this.unitOfMeasure.hasContent())) return true
        else if ((this.qualifierOfMeasure) && (this.qualifierOfMeasure.hasContent())) return true
        else return false;
    }

    public getContent(): MeasuredRangeContent
    {
        let content: MeasuredRangeContent;

        content = new MeasuredRangeContent();
        if (this.lowerBoundNumber) content.lowerBoundNumber = this.lowerBoundNumber.getContent();
        if (this.upperBoundNumber) content.upperBoundNumber = this.upperBoundNumber.getContent();
        if (this.unitOfMeasure) content.unitOfMeasure = this.unitOfMeasure.getContent();
        if (this.qualifierOfMeasure) content.qualifierOfMeasure = this.qualifierOfMeasure.getContent();
        return content;
    }

    public setContent(content: MeasuredRangeContent)
    {
        this.lowerBoundNumber.setContent(content != null ? content.lowerBoundNumber : null);
        this.upperBoundNumber.setContent(content != null ? content.upperBoundNumber : null);
        this.unitOfMeasure.setContent(content != null ? content.unitOfMeasure : null);
        this.qualifierOfMeasure.setContent(content != null ? content.qualifierOfMeasure : null);
    }

    public getValueString(): string
    {
        let valueString: string;

        valueString = "";
        if (this.lowerBoundNumber)
        {
            let lowerBoundValueString;

            lowerBoundValueString = this.lowerBoundNumber.getValueString();
            if (lowerBoundValueString) valueString += lowerBoundValueString;
        } 

        if (this.upperBoundNumber)
        {
            let upperBoundValueString;

            upperBoundValueString = this.upperBoundNumber.getValueString();
            if (upperBoundValueString)
            {
                if (valueString.length > 0) valueString += "-";
                valueString += upperBoundValueString;
            }
        } 

        if (this.unitOfMeasure)
        {
            let uomValueString;

            uomValueString = this.unitOfMeasure.getValueString();
            if (uomValueString) valueString += uomValueString;
        } 
        return valueString.length > 0 ? valueString : null;
    }
}

export class UnitOfMeasure extends RecordValue
{
    conceptId: string;
    codeId: string;
    code: string;
    termId: string;
    term: string;
    abbreviationId: string;
    abbreviation: string;
    definitionId: string;
    definition: string;

    constructor()
    {
        super("UNIT_OF_MEASURE");
    }

    fromJson(jsonValue: any)
    {
        this.conceptId = jsonValue.conceptId;
        this.code = jsonValue.code;
        this.term = jsonValue.term;
        this.abbreviation = jsonValue.abbreviation;
        this.definition = jsonValue.definition;
    }

    public hasContent(): boolean
    {
        return this.conceptId != null;
    }

    public getContent(): UnitOfMeasureContent
    {
        let content: UnitOfMeasureContent;

        content = new UnitOfMeasureContent();
        content.conceptId = this.conceptId;
        content.code = this.code;
        content.term = this.term;
        content.definition = this.definition;
        return content;
    }

    public setContent(content: UnitOfMeasureContent)
    {
        if (content != null)
        {
            this.conceptId = content.conceptId;
            this.code = content.code;
            this.term = content.term;
            this.definition = content.definition;
        }
        else
        {
            this.clear();
        }
    }

    public clear()
    {
        this.conceptId = null;
        this.codeId = null;
        this.code = null;
        this.termId = null;
        this.term = null;
        this.abbreviationId = null;
        this.abbreviation = null;
        this.definitionId = null;
        this.definition = null;
    }

    public getValueString(): string
    {
        return this.term;
    }
}

export class QualifierOfMeasure extends RecordValue
{
    conceptId: string;
    codeId: string;
    code: string;
    termId: string;
    term: string;
    abbreviationId: string;
    abbreviation: string;
    definitionId: string;
    definition: string;

    constructor()
    {
        super("QUALIFIER_OF_MEASURE");
    }

    fromJson(jsonValue: any)
    {
        this.conceptId = jsonValue.conceptId;
        this.code = jsonValue.code;
        this.term = jsonValue.term;
        this.abbreviation = jsonValue.abbreviation;
        this.definition = jsonValue.definition;
    }

    public hasContent(): boolean
    {
        return this.conceptId != null;
    }

    public getContent(): QualifierOfMeasureContent
    {
        let content: QualifierOfMeasureContent;

        content = new QualifierOfMeasureContent();
        content.conceptId = this.conceptId;
        content.code = this.code;
        content.term = this.term;
        content.definition = this.definition;
        return content;
    }

    public setContent(content: QualifierOfMeasureContent)
    {
        if (content != null)
        {
            this.conceptId = content.conceptId;
            this.code = content.code;
            this.term = content.term;
            this.definition = content.definition;
        }
        else
        {
            this.clear();
        }
    }

    public clear()
    {
        this.conceptId = null;
        this.codeId = null;
        this.code = null;
        this.termId = null;
        this.term = null;
        this.abbreviationId = null;
        this.abbreviation = null;
        this.definitionId = null;
        this.definition = null;
    }

    public getValueString(): string
    {
        return this.term;
    }
}

export class NumberValue extends RecordValue
{
    number: string;

    constructor()
    {
        super("NUMBER");
    }

    public hasContent(): boolean
    {
        return (this.number != null);
    }

    public getContent(): NumberContent
    {
        let content: NumberContent;

        content = new NumberContent();
        content.value = this.number;
        return content;
    }

    public setContent(content: NumberContent)
    {
        if (content != null)
        {
            this.number = content.value;
        }
        else
        {
            this.clear();
        }
    }

    public clear()
    {
        this.number = null;
    }

    public getValueString(): string
    {
        return this.number;
    }
}

export class LowerBoundNumber extends RecordValue
{
    value: string;

    constructor()
    {
        super("LOWER_BOUND_NUMBER");
    }

    public hasContent(): boolean
    {
        return (this.value != null);
    }

    public getContent(): LowerBoundNumberContent
    {
        let content: LowerBoundNumberContent;

        content = new LowerBoundNumberContent();
        content.value = this.value;
        return content;
    }

    public setContent(content: LowerBoundNumberContent)
    {
        if (content != null)
        {
            this.value = content.value;
        }
        else 
        {
            this.clear();
        }
    }

    public clear()
    {
        this.value = null;
    }

    public getValueString(): string
    {
        return this.value;
    }
}

export class UpperBoundNumber extends RecordValue
{
    value: string;

    constructor()
    {
        super("UPPER_BOUND_NUMBER");
    }

    public hasContent(): boolean
    {
        return (this.value != null);
    }

    public getContent(): UpperBoundNumberContent
    {
        let content: UpperBoundNumberContent;

        content = new UpperBoundNumberContent();
        content.value = this.value;
        return content;
    }

    public setContent(content: UpperBoundNumberContent)
    {
        if (content != null)
        {
            this.value = content.value;
        }
        else 
        {
            this.clear();
        }
    }

    public clear()
    {
        this.value = null;
    }

    public getValueString(): string
    {
        return this.value;
    }
}

export class IntegerValue extends NumberValue
{
    number: string;

    constructor()
    {
        super();
    }

    public hasContent(): boolean
    {
        return (this.number != null);
    }
}

export class RealValue extends NumberValue
{
    number: string;

    constructor()
    {
        super();
    }

    public hasContent(): boolean
    {
        return (this.number != null);
    }
}

/*
    Record Content Model
*/

export class ValueContent
{
    type: string;

    constructor(type: string)
    {
        this.type = type;
    }

    fromJson(jsonValue: any)
    {
    }

    public static createFromJson(jsonValue: any): ValueContent
    {
        if (jsonValue)
        {
            switch (jsonValue.type)
            {
                case "STRING":
                {
                    let value = new StringContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "TEXT":
                {
                    let value = new TextContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "BOOLEAN":
                {
                    let value = new BooleanContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "FIELD":
                {
                    let value = new FieldContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "COMPOSITE":
                {
                    let value = new CompositeContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "CONTROLLED_CONCEPT":
                {
                    let value = new ControlledConceptContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "ATTACHMENT":
                {
                    let value = new AttachmentListContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DOCUMENT_REFERENCE":
                {
                    let value = new DocumentReferenceListContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "MEASURED_NUMBER":
                {
                    let value = new MeasuredNumberContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "MEASURED_RANGE":
                {
                    let value = new MeasuredRangeContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "UNIT_OF_MEASURE":
                {
                    let value = new UnitOfMeasureContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "INTEGER":
                {
                    let value = new IntegerContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "REAL":
                {
                    let value = new RealContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "NUMBER":
                {
                    let value = new NumberContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DATE_TIME":
                {
                    let value = new DateTimeContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "DATE":
                {
                    let value = new DateContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                case "TIME":
                {
                    let value = new TimeContent();
                    value.fromJson(jsonValue);
                    return value;
                }
                default:
                {
                    throw new TypeError("Unsupported data type '" + jsonValue.type + "' in JSON object: " + JSON.stringify(jsonValue));
                }
            }
        }
        else
        {
            return null;
        } 
    }
}

export class StringContent extends ValueContent
{
    string: string;

    constructor()
    {
        super("STRING");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.string = jsonValue.string;
    }
}

export class TextContent extends ValueContent
{
    text: string;

    constructor()
    {
        super("TEXT");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.text = jsonValue.text;
    }
}

export class CompositeContent extends ValueContent
{
    fields: FieldContent[];

    constructor()
    {
        super("COMPOSITE");
        this.fields = new Array();
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        for (let jsonFieldContent of jsonValue.fields)
        {
            let fieldContent: FieldContent;

            fieldContent = new FieldContent();
            fieldContent.fromJson(jsonFieldContent);
            this.fields.push(fieldContent);
        }
    }
}

export class FieldContent extends ValueContent
{
    fieldId: string;
    term: string;
    definition: string;
    code: string;
    value: ValueContent;

    constructor()
    {
        super("FIELD");
    }

    fromJson(jsonField: any)
    {
        super.fromJson(jsonField);
        this.fieldId = jsonField.fieldId;
        this.term = jsonField.term;
        this.definition = jsonField.definition;
        this.code = jsonField.code;
        this.value = ValueContent.createFromJson(jsonField.value);
    }
}

export class DateContent extends ValueContent
{
    date: string;

    constructor()
    {
        super("DATE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.date = jsonValue.date;
    }
}

export class DateTimeContent extends ValueContent
{
    dateTime: string;

    constructor()
    {
        super("DATE_TIME");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.dateTime = jsonValue.dateTime;
    }
}

export class TimeContent extends ValueContent
{
    time: string;

    constructor()
    {
        super("TIME");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.time = jsonValue.time;
    }
}

export class BooleanContent extends ValueContent
{
    value: string;

    constructor()
    {
        super("BOOLEAN");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.value = jsonValue.value;
    }
}

export class NumberContent extends ValueContent
{
    value: string;

    constructor()
    {
        super("NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.value = jsonValue.value;
    }
}

export class LowerBoundNumberContent extends ValueContent
{
    value: string;

    constructor()
    {
        super("LOWER_BOUND_NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.value = jsonValue.value;
    }
}

export class UpperBoundNumberContent extends ValueContent
{
    value: string;

    constructor()
    {
        super("UPPER_BOUND_NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.value = jsonValue.value;
    }
}

export class RealContent extends ValueContent
{
    value: string;

    constructor()
    {
        super("REAL");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.value = jsonValue.value;
    }
}

export class IntegerContent extends ValueContent
{
    value: string;

    constructor()
    {
        super("INTEGER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.value = jsonValue.value;
    }
}

export class MeasuredNumberContent extends ValueContent
{
    number: NumberContent;
    unitOfMeasure: UnitOfMeasureContent;
    qualifierOfMeasure: QualifierOfMeasureContent;

    constructor()
    {
        super("MEASURED_NUMBER");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);

        if (jsonValue.number)
        {
            this.number = new NumberContent();
            this.number.fromJson(jsonValue.number);
        }

        if (jsonValue.unitOfMeasure)
        {
            this.unitOfMeasure = new UnitOfMeasureContent();
            this.unitOfMeasure.fromJson(jsonValue.unitOfMeasure);
        }
    }
}

export class MeasuredRangeContent extends ValueContent
{
    lowerBoundNumber: LowerBoundNumberContent;
    upperBoundNumber: UpperBoundNumberContent;
    unitOfMeasure: UnitOfMeasureContent;
    qualifierOfMeasure: QualifierOfMeasureContent;

    constructor()
    {
        super("MEASURED_RANGE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);

        if (jsonValue.lowerBoundNumber)
        {
            this.lowerBoundNumber = new LowerBoundNumberContent();
            this.lowerBoundNumber.fromJson(jsonValue.lowerBoundNumber);
        }

        if (jsonValue.upperBoundNumber)
        {
            this.upperBoundNumber = new UpperBoundNumberContent();
            this.upperBoundNumber.fromJson(jsonValue.upperBoundNumber);
        }

        if (jsonValue.unitOfMeasure)
        {
            this.unitOfMeasure = new UnitOfMeasureContent();
            this.unitOfMeasure.fromJson(jsonValue.unitOfMeasure);
        }
    }
}

export class ControlledConceptContent extends ValueContent
{
    conceptId: string;
    term: string;
    definition: string;
    code: string;

    constructor()
    {
        super("CONTROLLED_CONCEPT");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.conceptId = jsonValue.conceptId;
        this.term = jsonValue.term;
        this.definition = jsonValue.definition;
        this.code = jsonValue.code;
    }
}

export class QualifierOfMeasureContent extends ValueContent
{
    conceptId: string;
    termId: string;
    term: string;
    definitionId: string;
    definition: string;
    codeId: string;
    code: string;
    abbreviationId: string;
    abbreviation: string;
    languageId: string;

    constructor()
    {
        super("QUALIFIER_OF_MEASURE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.conceptId = jsonValue.conceptId;
        this.termId = jsonValue.termId;
        this.term = jsonValue.term;
        this.definitionId = jsonValue.definitionId;
        this.definition = jsonValue.definition;
        this.codeId = jsonValue.codeId;
        this.code = jsonValue.code;
        this.abbreviationId = jsonValue.abbreviationId;
        this.abbreviation = jsonValue.abbreviation;
    }
}

export class UnitOfMeasureContent extends ValueContent
{
    conceptId: string;
    termId: string;
    term: string;
    definitionId: string;
    definition: string;
    codeId: string;
    code: string;
    abbreviationId: string;
    abbreviation: string;
    languageId: string;

    constructor()
    {
        super("UNIT_OF_MEASURE");
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        this.conceptId = jsonValue.conceptId;
        this.termId = jsonValue.termId;
        this.term = jsonValue.term;
        this.definitionId = jsonValue.definitionId;
        this.definition = jsonValue.definition;
        this.codeId = jsonValue.codeId;
        this.code = jsonValue.code;
        this.abbreviationId = jsonValue.abbreviationId;
        this.abbreviation = jsonValue.abbreviation;
    }
}

export class DocumentReferenceListContent extends ValueContent
{
    references: DocumentReferenceContent[];

    constructor()
    {
        super("DOCUMENT_REFERENCE");
        this.references = new Array();
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        for (let reference of jsonValue.references)
        {
            let referenceContent: DocumentReferenceContent;

            referenceContent = new DocumentReferenceContent();
            referenceContent.fromJson(reference);
            this.references.push(referenceContent);
        }
    }

    public addReference(reference: DocumentReferenceContent)
    {
        this.references.push(reference);
    }

    public removeReference(referenceId: string): DocumentReferenceContent
    {
        for (let index = 0; index < this.references.length; index++)
        {
            let reference: DocumentReferenceContent;

            reference = this.references[index];
            if (reference.conceptId == referenceId)
            {
                this.references.splice(index, 1);
                return reference;
            }
        }

        return null;
    }
}

export class DocumentReferenceContent
{
    referenceType: string;
    conceptId: string;
    term: string;
    definition: string;
    code: string;

    constructor()
    {
    }

    fromJson(jsonValue: any)
    {
        this.referenceType = jsonValue.referenceType;
        this.conceptId = jsonValue.conceptId;
        this.term = jsonValue.term;
        this.definition = jsonValue.definition;
        this.code = jsonValue.code;
    }
}

export class AttachmentListContent extends ValueContent
{
    attachments: AttachmentContent[];

    constructor()
    {
        super("ATTACHMENT");
        this.attachments = new Array();
    }

    fromJson(jsonValue: any)
    {
        super.fromJson(jsonValue);
        for (let attachment of jsonValue.attachments)
        {
            let attachmentContent: AttachmentContent;

            attachmentContent = new AttachmentContent();
            attachmentContent.fromJson(attachment);
            this.attachments.push(attachmentContent);
        }
    }

    public addAttachment(attachment: AttachmentContent)
    {
        this.attachments.push(attachment);
    }
}

export class AttachmentContent
{
    attachmentId: string;
    fileContextId: string;
    fileContextIid: string;
    filePath: string;
    fileSize: number;
    md5: string;
    mediaType: string;

    constructor()
    {
    }

    fromJson(jsonValue: any)
    {
        this.attachmentId = jsonValue.attachmentId;
        this.fileContextId = jsonValue.fileContextId;
        this.fileContextIid = jsonValue.fileContextInstanceId;
        this.filePath = jsonValue.filePath;
        this.fileSize = jsonValue.fileSize;
        this.md5 = jsonValue.md5;
        this.mediaType = jsonValue.mediaType;
    }
}

export class ValueStringSettings
{
    excludedTypeIds: string[];

    constructor()
    {
        this.excludedTypeIds = new Array();
    }

    public addExcludedType(typeId: string)
    {
        this.excludedTypeIds.push(typeId);
    }

    public isTypeIncluded(typeId: string)
    {
        return this.excludedTypeIds.indexOf(typeId) < 0;
    }
}