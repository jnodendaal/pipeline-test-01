export class FileDetails
{

}

export interface FileUploadProgress
{
    totalFiles: number;
    uploadedFiles: number;
    currentFilename: string;
    currentFileTotalBytes: number;
    currentFileUploadedBytes: number;
}