import { NumberUtility } from '../utility/number.utility';

export class GroupedDataQualityReport
{
    groups: DataQualityMetricsGroup[];

    public fromJson(jsonReport: any)
    {
        this.groups = new Array();
        for (let jsonGroup of jsonReport.groups)
        {
            let group = new DataQualityMetricsGroup();
            group.fromJson(jsonGroup);
            this.groups.push(group);
        }
    }

    public getSummary(): DataQualityMetricsSummary
    {
        let summary: DataQualityMetricsSummary;
        
        summary = new DataQualityMetricsSummary();
        summary.recordCount = this.getRecordCount();
        summary.averagePropertyCount = this.getAveragePropertyCount();
        summary.averagePropertyDataCount = this.getAveragePropertyDataCount();
        summary.averageCharacteristicCount = this.getAverageCharacteristicCount();
        summary.averageCharacteristicDataCount = this.getAverageCharacteristicDataCount();
        summary.characteristicCompleteness = this.getCharacteristicCompleteness();
        summary.characteristicValidity = this.getCharacteristicValidity();
        summary.nonCharacteristicCompleteness = this.getNonCharacteristicCompleteness();
        summary.nonCharacteristicValidity = this.getNonCharacteristicValidity();
        return summary;
    }

    public getAveragePropertyCount()
    {
        return this.getPropertyCount() / this.getRecordCount();
    }

    public getAveragePropertyDataCount()
    {
        return this.getPropertyDataCount() / this.getRecordCount();
    }

    public getAverageCharacteristicCount()
    {
        return this.getCharacteristicCount() / this.getRecordCount();
    }

    public getAverageCharacteristicDataCount()
    {
        return this.getCharacteristicDataCount() / this.getRecordCount();
    }

    public getCharacteristicCompleteness()
    {
        return this.getCharacteristicDataCount() / this.getCharacteristicCount() * 100;
    }

    public getCharacteristicValidity()
    {
        return this.getCharacteristicValidCount() / this.getCharacteristicDataCount() * 100;
    }

    public getNonCharacteristicCompleteness()
    {
        return this.getNonCharacteristicDataCount() / this.getNonCharacteristicCount() * 100;
    }

    public getNonCharacteristicValidity()
    {
        return this.getNonCharacteristicValidCount() / this.getNonCharacteristicDataCount() * 100;
    }

    public getRecordCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getRecordCount();
        }

        return count;
    }

    public getPropertyCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getPropertyCount();
        }

        return count;
    }

    public getPropertyDataCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getPropertyDataCount();
        }

        return count;
    }

    public getCharacteristicCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getCharacteristicCount();
        }

        return count;
    }

    public getCharacteristicDataCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getCharacteristicDataCount();
        }

        return count;
    }

    public getCharacteristicValidCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getCharacteristicValidCount();
        }

        return count;
    }

    public getNonCharacteristicCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getNonCharacteristicCount();
        }

        return count;
    }

    public getNonCharacteristicDataCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getNonCharacteristicDataCount();
        }

        return count;
    }

    public getNonCharacteristicValidCount(): number
    {
        let count: number;

        count = 0;
        for (let group of this.groups)
        {
            count += group.metrics.getNonCharacteristicValidCount();
        }

        return count;
    }
}

export class DataQualityMetricsGroup
{
    id: string;
    code: string;
    term: string;
    definition: string;
    ocId: string;
    ocTerm: string;
    metrics: DataQualityMetrics;

    public fromJson(jsonGroup: any)
    {
        this.id = jsonGroup.id;
        this.code = jsonGroup.code;
        this.term = jsonGroup.term;
        this.definition = jsonGroup.definition;
        this.ocId = jsonGroup.ocId;
        this.ocTerm = jsonGroup.ocTerm;
        this.metrics = new DataQualityMetrics();
        this.metrics.fromJson(jsonGroup.metrics);
    }
}

export class FileDataQualityReport
{
    fileId: string;
    metrics: DataQualityMetrics;

    constructor(fileId: string)
    {
        this.fileId = fileId;
    }

    public fromJson(jsonGroup: any)
    {
        this.fileId = jsonGroup.id;
        this.metrics = new DataQualityMetrics();
        this.metrics.fromJson(jsonGroup.metrics);
    }
}

export class DataQualityMetrics
{
    recordCount: number; // Total number of records used in the aggregation.
    propertyCount: number; // Total number of properties in the Data Requirements used.
    propertyDataCount: number; // Number of properties that have data (FFT or VALUE).
    propertyValidCount: number; // Number of properties that have valid data (VALUE).
    propertyVerificationCount: number; // Number of properties verified against an external source.
    propertyAccurateCount: number; // Number of properties verified against an external source and found to be accurate.
    characteristicCount: number; // Number of characteristic properties in the Data Requirement.
    characteristicDataCount: number; // Number of characteristic properties that have data (FFT or VALUE).
    characteristicValidCount: number; // Number of characteristic properties that have valid data (VALUE).
    characteristicVerificationCount: number; // Number of characteristic properties verified against an external source.
    characteristicAccurateCount: number; // Number of characteristic properties verified against an external source and found to be accurate.

    public fromJson(jsonMetrics: any)
    {
        this.recordCount = jsonMetrics.recordCount;
        this.propertyCount = jsonMetrics.propertyCount;
        this.propertyDataCount = jsonMetrics.propertyDataCount;
        this.propertyValidCount = jsonMetrics.propertyValidCount;
        this.propertyVerificationCount = jsonMetrics.propertyVerificationCount;
        this.propertyAccurateCount = jsonMetrics.propertyAccurateCount;
        this.characteristicCount = jsonMetrics.characteristicCount;
        this.characteristicDataCount = jsonMetrics.characteristicDataCount;
        this.characteristicValidCount = jsonMetrics.characteristicValidCount;
        this.characteristicVerificationCount = jsonMetrics.characteristicVerificationCount;
        this.characteristicAccurateCount = jsonMetrics.characteristicAccurateCount;
    }

    public getSummary(): DataQualityMetricsSummary
    {
        let summary: DataQualityMetricsSummary;
        
        summary = new DataQualityMetricsSummary();
        summary.recordCount = this.getRecordCount();
        summary.averageCharacteristicCount = this.getAverageCharacteristicCount();
        summary.averageCharacteristicDataCount = this.getAverageCharacteristicDataCount();
        summary.characteristicCompleteness = this.getCharacteristicCompleteness();
        summary.characteristicValidity = this.getCharacteristicValidity();
        summary.nonCharacteristicCompleteness = this.getNonCharacteristicCompleteness();
        summary.nonCharacteristicValidity = this.getNonCharacteristicValidity();
        return summary;
    }

    public add(metrics: DataQualityMetrics)
    {
        this.recordCount += metrics.getRecordCount();
        this.propertyCount += metrics.getPropertyCount();
        this.propertyDataCount += metrics.getPropertyDataCount();
        this.propertyValidCount += metrics.getPropertyValidCount();
        this.propertyVerificationCount += metrics.getPropertyVerificationCount();
        this.propertyAccurateCount += metrics.getPropertyAccurateCount();
        this.characteristicCount += metrics.getCharacteristicCount();
        this.characteristicDataCount += metrics.getCharacteristicDataCount();
        this.characteristicValidCount += metrics.getCharacteristicValidCount();
        this.characteristicVerificationCount += metrics.getCharacteristicVerificationCount();
        this.characteristicAccurateCount += metrics.getCharacteristicAccurateCount();
    }

    public getAverageCharacteristicCount()
    {
        return this.characteristicCount / this.recordCount;
    }

    public getAverageCharacteristicDataCount()
    {
        return this.characteristicDataCount / this.recordCount;
    }

    public getPropertyCompleteness()
    {
        return this.propertyDataCount / this.propertyCount * 100;
    }

    public getCharacteristicCompleteness()
    {
        return this.characteristicDataCount / this.characteristicCount * 100;
    }

    public getCharacteristicValidity()
    {
        return this.characteristicValidCount / this.characteristicDataCount * 100;
    }

    public getNonCharacteristicCompleteness()
    {
        return this.getNonCharacteristicDataCount() / this.getNonCharacteristicCount() * 100;
    }

    public getNonCharacteristicValidity()
    {
        return this.getNonCharacteristicValidCount() / this.getNonCharacteristicDataCount() * 100;
    }

    public getRecordCount(): number
    {
        return this.recordCount;
    }

    public setRecordCount(recordCount: number)
    {
        this.recordCount = recordCount;
    }

    public getPropertyCount()
    {
        return this.propertyCount;
    }

    public setPropertyCount(propertyCount: number)
    {
        this.propertyCount = propertyCount;
    }

    public getPropertyDataCount(): number
    {
        return this.propertyDataCount;
    }

    public setPropertyDataCount(propertyDataCount: number)
    {
        this.propertyDataCount = propertyDataCount;
    }

    public getPropertyVerificationCount(): number
    {
        return this.propertyVerificationCount;
    }

    public setPropertyVerificationCount(propertyVerificationCount: number)
    {
        this.propertyVerificationCount = propertyVerificationCount;
    }

    public getPropertyAccurateCount(): number
    {
        return this.propertyAccurateCount;
    }

    public setPropertyAccurateCount(propertyAccurateCount: number)
    {
        this.propertyAccurateCount = propertyAccurateCount;
    }

    public getPropertyIncorrectCount(): number
    {
        return this.propertyDataCount - this.propertyAccurateCount;
    }

    public getPropertyValidCount(): number
    {
        return this.propertyValidCount;
    }

    public setPropertyValidCount(propertyValidCount: number)
    {
        this.propertyValidCount = propertyValidCount;
    }

    public getPropertyInvalidCount(): number
    {
        return this.propertyDataCount - this.propertyValidCount;
    }

    public getCharacteristicCount(): number
    {
        return this.characteristicCount;
    }

    public setCharacteristicCount(characteristicCount: number)
    {
        this.characteristicCount = characteristicCount;
    }

    public getCharacteristicDataCount(): number
    {
        return this.characteristicDataCount;
    }

    public setCharacteristicDataCount(characteristicDataCount: number)
    {
        this.characteristicDataCount = characteristicDataCount;
    }

    public getCharacteristicVerificationCount(): number
    {
        return this.characteristicVerificationCount;
    }

    public setCharacteristicVerificationCount(characteristicVerificationCount: number)
    {
        this.characteristicVerificationCount = characteristicVerificationCount;
    }

    public getCharacteristicAccurateCount(): number
    {
        return this.characteristicAccurateCount;
    }

    public setCharacteristicAccurateCount(characteristicAccurateCount: number)
    {
        this.characteristicAccurateCount = characteristicAccurateCount;
    }

    public getCharacteristicIncorrectCount(): number
    {
        return this.characteristicDataCount - this.characteristicAccurateCount;
    }

    public getCharacteristicValidCount(): number
    {
        return this.characteristicValidCount;
    }

    public setCharacteristicValidCount(characteristicValidCount: number)
    {
        this.characteristicValidCount = characteristicValidCount;
    }

    public getCharacteristicInvalidCount(): number
    {
        return this.characteristicDataCount - this.characteristicValidCount;
    }

    public getNonCharacteristicCount(): number
    {
        return this.propertyCount - this.characteristicCount;
    }

    public getNonCharacteristicDataCount(): number
    {
        return this.propertyDataCount - this.characteristicDataCount;
    }

    public getNonCharacteristicAccurateCount(): number
    {
        return this.propertyAccurateCount - this.characteristicAccurateCount;
    }

    public getNonCharacteristicIncorrectCount(): number
    {
        return this.getPropertyIncorrectCount() - this.getCharacteristicIncorrectCount();
    }

    public getNonCharacteristicValidCount(): number
    {
        return this.propertyValidCount - this.characteristicValidCount;
    }

    public getNonCharacteristicInvalidCount(): number
    {
        return this.getPropertyInvalidCount() - this.getCharacteristicInvalidCount();
    }
}

export class DataQualityMetricsSummary
{
    recordCount: number;
    characteristicCompleteness: number;
    characteristicValidity: number;
    nonCharacteristicCompleteness: number;
    nonCharacteristicValidity: number;
    averagePropertyCount: number;
    averagePropertyDataCount: number;
    averageCharacteristicCount: number;
    averageCharacteristicDataCount: number;

    constructor()
    {
    }
}