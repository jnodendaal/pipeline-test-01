export class UserDetails 
{
    id: string;
    username: string;
    name: string;
    surname: string;
    emailAddress: string;
    mobileNumber: string;
    workTelephoneNumber: string;
    languageId: string;
    orgId: string;
    active: boolean;
    expiryDate: number; // Milliseconds since Unix epoch.
    userProfileDetails: UserProfileDetails[];
    workflowProfileDetails: WorkflowProfileDetails[];

    constructor()
    {
        this.userProfileDetails = new Array();
        this.workflowProfileDetails = new Array();
    }

    fromJson(jsonValue: any): UserDetails
    {
        this.id = jsonValue.id;
        this.username = jsonValue.username;
        this.name = jsonValue.name;
        this.surname = jsonValue.surname;
        this.emailAddress = jsonValue.emailAddress;
        this.mobileNumber = jsonValue.mobileNumber;
        this.workTelephoneNumber = jsonValue.workTelephoneNumber;
        this.languageId = jsonValue.languageId;
        this.orgId = jsonValue.orgId;
        this.active = jsonValue.active;
        this.expiryDate = jsonValue.expiryDate;

        for (let jsonProfileDetails of jsonValue.userProfileDetails)
        {
            let profileDetails: UserProfileDetails;

            profileDetails = new UserProfileDetails();
            profileDetails.fromJson(jsonProfileDetails);
            this.addUserProfileDetails(profileDetails);
        }

        for (let jsonProfileDetails of jsonValue.workflowProfileDetails)
        {
            let profileDetails: WorkflowProfileDetails;

            profileDetails = new WorkflowProfileDetails();
            profileDetails.fromJson(jsonProfileDetails);
            this.addWorkflowProfileDetails(profileDetails);
        }

        return this;
    }

    public addUserProfileDetails(details: UserProfileDetails)
    {
        this.userProfileDetails.push(details);
    }

    public addWorkflowProfileDetails(details: WorkflowProfileDetails)
    {
        this.workflowProfileDetails.push(details);
    }
}

export class UserProfileDetails
{
    id: string;
    name: string;
    description: string;

    fromJson(jsonValue: any)
    {
        this.id = jsonValue.id;
        this.name = jsonValue.name;
        this.description = jsonValue.description;
    }
}

export class WorkflowProfileDetails
{
    id: string;
    name: string;
    description: string;

    fromJson(jsonValue: any)
    {
        this.id = jsonValue.id;
        this.name = jsonValue.name;
        this.description = jsonValue.description;
    }
}

export class SessionDetails 
{
    sessionId: string;
    userId: string;
    userName: string;
    userSurname: string;
    userProfileDetails: UserProfileDetails;
    workflowProfileDetails: WorkflowProfileDetails[];

    constructor()
    {
        this.workflowProfileDetails = new Array();
    }

    fromJson(jsonValue: any): SessionDetails
    {
        this.sessionId = jsonValue.sessionId;
        this.userId = jsonValue.userId;
        this.userName = jsonValue.userName;
        this.userSurname = jsonValue.userSurname;

        if (jsonValue.userProfileDetails)
        {
            this.userProfileDetails = new UserProfileDetails();
            this.userProfileDetails.fromJson(jsonValue.userProfileDetails);
        }

        for (let jsonProfileDetails of jsonValue.workflowProfileDetails)
        {
            let profileDetails: WorkflowProfileDetails;

            profileDetails = new WorkflowProfileDetails();
            profileDetails.fromJson(jsonProfileDetails);
            this.addWorkflowProfileDetails(profileDetails);
        }

        return this;
    }

    public addWorkflowProfileDetails(details: WorkflowProfileDetails)
    {
        this.workflowProfileDetails.push(details);
    }
}