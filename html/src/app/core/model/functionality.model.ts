import { Context } from "./security.model";

export class FunctionalityHandle
{
    id: string;
    name: string;
    description: string;
    iconUri: string;
    enabled: boolean;
    confirmationMessage: string;
    disabledMessage: string;
    parameters: any;

    constructor()
    {
    }

    public fromJson(jsonFunctionalityHandle: any)
    {
        this.id = jsonFunctionalityHandle.id;
        this.name = jsonFunctionalityHandle.name;
        this.description = jsonFunctionalityHandle.description;
        this.iconUri = jsonFunctionalityHandle.iconUri;
        this.enabled = jsonFunctionalityHandle.enabled;
        this.confirmationMessage = jsonFunctionalityHandle.confirmationMessage;
        this.disabledMessage = jsonFunctionalityHandle.disabledMessage;
        this.parameters = jsonFunctionalityHandle.parameters;
    }

    public static createFromJson(jsonHandle: any): FunctionalityHandle
    {
        let handle: FunctionalityHandle;

        handle = new FunctionalityHandle();
        handle.fromJson(jsonHandle);
        return handle;
    }

    public getPublicParameters(): any
    {
        let publicParameters: any;

        publicParameters = {};
        if (this.parameters)
        {
            for (let key of Object.getOwnPropertyNames(this.parameters))
            {
                publicParameters[this.id + key] = this.parameters[key];
            }
        }

        return publicParameters;
    }
}

export class FunctionalityGroupHandle
{
    id: string;
    name: string;
    description: string;
    iconUri: string;
    functionalities: FunctionalityHandle[];
    subGroups: FunctionalityGroupHandle[];

    constructor()
    {
        this.functionalities = new Array();
        this.subGroups = new Array();
    }

    public fromJson(jsonGroupHandle: any)
    {
        this.id = jsonGroupHandle.id;
        this.name = jsonGroupHandle.name;
        this.description = jsonGroupHandle.description;
        this.iconUri = jsonGroupHandle.iconUrl;
        
        // Add all functionalities.
        if (jsonGroupHandle.functionalities)
        {
            for (let jsonFunctionalityHandle of jsonGroupHandle.functionalities)
            {
                this.addFunctionality(FunctionalityHandle.createFromJson(jsonFunctionalityHandle));
            }
        }

        // Add all sub-groups.
        if (jsonGroupHandle.subGroups)
        {
            for (let jsonSubGroupHandle of jsonGroupHandle.subGroups)
            {
                this.addSubGroup(FunctionalityGroupHandle.createFromJson(jsonSubGroupHandle));
            }
        }
    }

    public static createFromJson(jsonGroupHandle: any)
    {
        let groupHandle: FunctionalityGroupHandle;

        groupHandle = new FunctionalityGroupHandle();
        groupHandle.fromJson(jsonGroupHandle);
        return groupHandle;
    }

    public addFunctionality(functionality: FunctionalityHandle)
    {
        this.functionalities.push(functionality);
    }

    public addSubGroup(functionalityGroup: FunctionalityGroupHandle)
    {
        this.subGroups.push(functionalityGroup);
    }
}

export class FunctionalityAccessHandle
{
    /*  
        accessType enumeration is defined for the following string literals:
        ACCESS:  Access granted.
        DENIED_BLOCKED:  Access denied because the functionality requested is blocked by another functionality.
        DENIED_VALIDATION:  Access denied because the validation based on the target object failed.
        DENIED_PERMISSION:  Access denied because the user does not have the required permission. 
    */
    accessType: string;
    accessMessage: string; // Any message related to the above mentioned accessType.
    id: string;
    iid: string;
    name: string;
    description: string;
    iconUri: string;
    componentUri: string;
    componentParameters: any;

    constructor()
    {
    }

    public fromJson(jsonAccessHandle: any)
    {
        this.accessType = jsonAccessHandle.accessType;
        this.accessMessage = jsonAccessHandle.accessMessage;
        this.id = jsonAccessHandle.id;
        this.iid = jsonAccessHandle.iid;
        this.name = jsonAccessHandle.name;
        this.description = jsonAccessHandle.description;
        this.iconUri = jsonAccessHandle.iconUri;
        this.componentUri = jsonAccessHandle.componentUri;
        this.componentParameters = jsonAccessHandle.componentParameters;
    }

    public static createFromJson(jsonAccessHandle: any): FunctionalityAccessHandle
    {
        let handle: FunctionalityAccessHandle;

        handle = new FunctionalityAccessHandle();
        handle.fromJson(jsonAccessHandle);
        return handle;
    }

    public getContext(): Context
    {
        return Context.functionalityContext(this.iid);
    }
}