export class TerminologySet
{
    private concepts: {[key: string]: Terminology} = {};
    private languageId: string;

    constructor(languageId: string)
    {
        this.concepts = {};
        this.languageId = languageId;
    }

    public getTerm(conceptId: string): string
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        return terminology ? terminology.term : undefined;
    }

    public setTerm(conceptId: string, term: string)
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        if (!terminology)
        {
            terminology = new Terminology();
            this.concepts[conceptId] = terminology;
        }

        terminology.term = term;
    }

    public getDefinition(conceptId: string): string
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        return terminology ? terminology.definition : undefined;
    }

    public setDefinition(conceptId: string, definition: string)
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        if (!terminology)
        {
            terminology = new Terminology();
            this.concepts[conceptId] = terminology;
        }

        terminology.definition = definition;
    }

    public getCode(conceptId: string): string
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        return terminology ? terminology.code : undefined;
    }

    public setCode(conceptId: string, code: string)
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        if (!terminology)
        {
            terminology = new Terminology();
            this.concepts[conceptId] = terminology;
        }

        terminology.code = code;
    }

    public getAbbreviation(conceptId: string): string
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        return terminology ? terminology.abbreviation : undefined;
    }

    public setAbbreviation(conceptId: string, abbreviation: string)
    {
        let terminology: Terminology;

        terminology = this.concepts[conceptId];
        if (!terminology)
        {
            terminology = new Terminology();
            this.concepts[conceptId] = terminology;
        }

        terminology.abbreviation = abbreviation;
    }

    public addTerminologySet(terminology: TerminologySet)
    {
        for (let conceptId in terminology.concepts)
        {
            this.concepts[conceptId] = terminology.concepts[conceptId];
        }
    }

    public setTerms(terms: {[key: string]: string})
    {
        for (let conceptId in terms)
        {
            this.setTerm(conceptId, terms[conceptId]);
        }
    }
}

export class Terminology
{
    term: string;
    code: string;
    definition: string;
    abbreviation: string;
}