import { NgModule, Optional, SkipSelf }             from '@angular/core';
import { CommonModule }                             from '@angular/common';
import { throwIfAlreadyLoaded }                     from './utility/angular.utility';
import { SecurityManagerService }                   from './service/security-manager.service';
import { FunctionalityManagerService }              from './service/functionality-manager.service';
import { WorkflowManagerService }                   from './service/workflow-manager.service';
import { FileManagerService }                       from './service/file-manager.service';
import { UserManagerService }                       from './service/user-manager.service';
import { ReportManagerService }                     from './service/report-manager.service';
import { ProcessManagerService }                    from './service/process-manager.service';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
  ],
  declarations: 
  [ 
  ],
  exports:      
  [ 
  ],
  providers:
  [
    SecurityManagerService,
    FunctionalityManagerService,
    WorkflowManagerService,
    FileManagerService,
    UserManagerService,
    ReportManagerService,
    ProcessManagerService
  ]
})
export class CoreModule 
{
  /**
   * This module must contain only those Singleton services and components that are used once system-wide.
   * Only AppModule should import this module.
   * @param parentModule Uses @SkipSelf to find an instance of CoreModule in the parent DI tree.
   */
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) 
  {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}