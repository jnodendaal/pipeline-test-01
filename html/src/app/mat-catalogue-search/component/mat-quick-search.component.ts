import { Component, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { Context } from '../../core/model/security.model';
import { MaterialCatalogueSearchService } from '../service/mat-catalogue-search.service';
import { MaterialSearchCriteria } from '../model/mat-catalogue-search.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MaterialSearchResultsComponent } from '../component/mat-search-results.component';
import { ComponentState } from '../../core/model/component.model';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { CoreComponent } from '../../core/component/core.component';

@Component
({
    selector: 'mat-quick-search',
    templateUrl: 'mat-quick-search.component.html',
    styleUrls: ['mat-quick-search.component.css'],
    providers: []
})
export class MaterialQuickSearchComponent extends CoreComponent<QuickSearchState>
{   
    private searchService: MaterialCatalogueSearchService;
    private dataCount: number;
    private loading: boolean;
    private itemMenuVisible: boolean;
    private creationEnabled: boolean;

    public static COMPONENT_ID: string = "QUICK_SEARCH"

    @ViewChild('quickSearchResults')
    private quickSearchResults: MaterialSearchResultsComponent;
    
    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, searchService: MaterialCatalogueSearchService)
    {
        super(router, route, functionalityManager, componentManager);
        this.searchService = searchService;
        this.dataCount = 0;
        this.loading = false;
        this.creationEnabled = false;
    }

    private newSearch()
    {
        this.saveState();
        this.quickSearchResults.newSearch(new MaterialSearchCriteria(this.state.searchExpression, null));
    }

    protected createState(): QuickSearchState
    {
        this.state = new QuickSearchState();
        return this.state;
    }

    protected getState(): QuickSearchState
    {
        return this.state;
    }

    protected setState(state: QuickSearchState)
    {
        this.state = state;
    }
}

class QuickSearchState extends ComponentState
{
    searchPattern: string;
    searchExpression: string;

    constructor()
    {
        super();
    }
}