import { Component, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { FunctionalityAccessHandle } from '../../core/model/functionality.model';
import { Context } from '../../core/model/security.model';
import { DataFilter } from '../../core/model/data-filter.model';
import { TemplateEntity } from '../model/mat-catalogue-search.model';
import { MaterialCatalogueSearchService } from '../service/mat-catalogue-search.service';
import { TemplateSearchResults } from '../model/mat-catalogue-search.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { CoreComponent } from '../../core/component/core.component';
import { ComponentState } from '../../core/model/component.model';
import { FilterBarComponent, FilterBarState } from '../../data-table/component/filter-bar.component';

@Component
({
    selector: 'mat-template-search',
    templateUrl: 'mat-template-search.component.html',
    styleUrls: ['mat-template-search.component.css'],
    providers: [ ]
})
export class MaterialTemplateSearchComponent extends CoreComponent<TemplateSearchState>
{   
    private searchService: MaterialCatalogueSearchService;
    private templateMenuVisible: boolean;
    private selectedTemplate: TemplateEntity;
    private loading: boolean;

    @ViewChild('filterBar') 
    private filterBar: FilterBarComponent;

    @ViewChild('templateModal') 
    private templateMenuModal: ModalDirective;
    
    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, searchService: MaterialCatalogueSearchService)
    {
        super(router, route, functionalityManager, componentManager);
        this.searchService = searchService;
        this.loading = false;
        this.selectedTemplate = null;
    }

    ngOnInit()
    {
        // If this is a new state, process component input parameters.
        if (this.newState)
        {
            // Get the table setup from this component's input parameters.
            this.state.tableSetup = this.parameters.tableSetup;

            // Process the input parameters of the state.
            if (this.state.inputParameters)
            {
                // Async in order to allow state to be set on the filter-bar before we actually query it for a filter to use in the search.
                //setTimeout(() => this.newSearch(this.state.inputParameters[MaterialSearchResultsComponent.INPUT_PARAMETER_SEARCH_CRITERIA]));
            }
        }
    }

    private onTemplateSelected(template: TemplateEntity)
    {
        this.selectedTemplate = template;
        this.showTemplateMenu();
    }

    private onDescriptorDetailSelected(template: TemplateEntity)
    {
        this.selectedTemplate = template;
    }

    private newSearch()
    {
        this.state.filterBarState.pageOffset = 0;
        this.state.filterBarState.pageNumber = 1;
        this.search();
    }

    private search()
    {
        this.loading = true;
        this.searchService.searchDescriptors(this.state.filterBarState.searchExpression, this.getSearchFilter(), this.state.filterBarState.pageOffset, this.state.filterBarState.pageSize)
        .subscribe
            (
                data => 
                {
                    this.loading = false;
                    this.state.searchResults = data;
                    this.state.searchPattern = "";
                    
                    // Add the search terms returned from the server to the local search pattern.
                    for (let searchTerm of data.searchTerms)
                    {
                        if (this.state.searchPattern.length > 0) this.state.searchPattern += "|";
                        this.state.searchPattern += searchTerm;
                    }

                    // Add the filter bar filter terms to the search pattern.
                    for (let searchTerm of this.filterBar.getFilterTerms())
                    {
                        if (this.state.searchPattern.length > 0) this.state.searchPattern += "|";
                        this.state.searchPattern += searchTerm;
                    }

                    // Save the current state of this component.
                    this.saveState();
                },
                error => 
                {
                    console.log("Error while searching material templates: " + error);
                    this.loading = false;
                }
            );
    }

    private getSearchFilter(): DataFilter
    {
        let filter: DataFilter;

        // Get the filter from the filter bar component.
        filter = this.filterBar.getDataFilter();
        filter.entityId = this.parameters.entityId;
        return filter;
    }

    public showTemplateMenu():void 
    {
        this.templateMenuVisible = true;
    }

    public hideTemplateMenu():void 
    {
        this.templateMenuModal.hide();
    }

    public onTemplateMenuHidden():void 
    {
        this.templateMenuVisible = false;
    }

    public onTemplateFunctionalitySelection(functionality: any)
    {
        this.hideTemplateMenu();
    }

    protected createState(): TemplateSearchState
    {
        this.state = new TemplateSearchState();
        return this.state;
    }

    protected getState(): TemplateSearchState
    {
        return this.state;
    }

    protected setState(state: TemplateSearchState)
    {
        this.state = state;
    }
}

class TemplateSearchState extends ComponentState
{
    filterBarState: FilterBarState;
    searchResults: TemplateSearchResults;
    searchPattern: string;
    tableSetup: any;

    constructor()
    {
        super();
        this.filterBarState = new FilterBarState();
        this.filterBarState.viewType = "TABLE";
        this.searchResults = new TemplateSearchResults();
    }
}