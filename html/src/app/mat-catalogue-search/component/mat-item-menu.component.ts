import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { MaterialEntity } from '../model/mat-catalogue-search.model';
import { FunctionalityHandle } from '../../core/model/functionality.model';

@Component
({
    selector: 'mat-item-menu',
    templateUrl: 'mat-item-menu.component.html',
    styleUrls: ['mat-item-menu.component.css']
})
export class MaterialItemMenuComponent implements OnInit
{   
    private functionalityManager: FunctionalityManagerService;
    private loading: boolean;
    private functionalities: any[];
    private item: MaterialEntity;

    constructor(functionalityManager: FunctionalityManagerService)
    {
        this.functionalityManager = functionalityManager;
    }

    ngOnInit()
    {
        this.reloadFunctionalities();
    }

    reloadFunctionalities()
    {
        // // Load the details of profiles accessible to the current session user.
        // this.loading = true;
        // this.functionalityManager.getAccessibleUserProfileDetails()
        //     .subscribe
        //     (
        //         data => 
        //         {
        //             this.loading = false;
        //             console.log(data);
        //             setTimeout(this.profiles = data);
        //         },
        //         error => 
        //         {
        //             console.log("Error while retrieving accessible user profile details: " + error);
        //             this.loading = false;
        //         }
        //     );
    }

    @Input()
    set catalogueItem(item: MaterialEntity)
    {
        console.log(item);
        this.item = item;
        this.reloadFunctionalities();
    }

    @Output() functionalitySelected: EventEmitter<FunctionalityHandle> = new EventEmitter<FunctionalityHandle>();
    onFunctionalitySelection(selectedFunctionality: FunctionalityHandle)
    {
        console.log(selectedFunctionality);
        this.functionalitySelected.emit(selectedFunctionality);
    }
}