import { Component, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { FunctionalityAccessHandle } from '../../core/model/functionality.model';
import { Context } from '../../core/model/security.model';
import { DataFilter } from '../../core/model/data-filter.model';
import { TerminologySet } from '../../core/model/terminology.model';
import { MaterialEntity } from '../model/mat-catalogue-search.model';
import { MaterialCatalogueSearchService } from '../service/mat-catalogue-search.service';
import { MaterialSearchResults } from '../model/mat-catalogue-search.model';
import { MaterialSearchCriteria } from '../model/mat-catalogue-search.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { MaterialItemDetailComponent } from '../component/mat-item-detail.component';
import { MaterialSearchResultsComponent } from '../component/mat-search-results.component';
import { CoreComponent } from '../../core/component/core.component';
import { ComponentState } from '../../core/model/component.model';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { RecordFilterCriteria } from '../../core/model/data-record-filter.model';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component
({
    selector: 'mat-advanced-search',
    templateUrl: 'mat-advanced-search.component.html',
    styleUrls: ['mat-advanced-search.component.css'],
    providers: [ ]
})
export class MaterialAdvancedSearchComponent extends CoreComponent<AdvancedSearchState> implements OnInit
{   
    private searchService: MaterialCatalogueSearchService;
    private loading: boolean;
    
    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, searchService: MaterialCatalogueSearchService)
    {
        super(router, route, functionalityManager, componentManager);
        this.loading = false;
    }

    ngOnInit()
    {
        // Set the nessacary paramaters.
        if (this.newState)
        {
            this.state.recordFilterCriteria = RecordFilterCriteria.fromJson(this.parameters.advancedSearchFilter);
            this.state.terminology.setTerms(this.parameters.advancedSearchTerminology); // TODO: Full terminology objects must be sent from server.
            this.saveState();
        }
    }

    protected createState(): AdvancedSearchState
    {
        this.state = new AdvancedSearchState();
        return this.state;
    }

    protected getState(): AdvancedSearchState
    {
        return this.state;
    }

    protected setState(state: AdvancedSearchState)
    {
        this.state = state;
    }

    private searchRecordCriteria(recordFilterCriteria: RecordFilterCriteria)
    {
        let materialSearchCriteria: MaterialSearchCriteria;

        // Set the record filter criteria to the state and save the state.
        this.state.recordFilterCriteria = recordFilterCriteria;
        this.saveState();

        // Create the search criteria to send as input to results page.
        materialSearchCriteria = new MaterialSearchCriteria(null, recordFilterCriteria);
       
        // Route to the search and results page.
        this.componentManager.setComponentInputParameters("SEARCH_RESULTS", { [MaterialSearchResultsComponent.INPUT_PARAMETER_SEARCH_CRITERIA]: materialSearchCriteria });
        this.router.navigate(['./search-results'], { relativeTo: this.route, queryParams: this.getSubComponentQueryParameters() });
    }

    private searchTerminology(terminology: any)
    {
        this.state.terminology = terminology;
    }
}

class AdvancedSearchState extends ComponentState
{
    recordFilterCriteria: any;
    terminology: TerminologySet;

    constructor()
    {
        super();
        this.terminology = new TerminologySet(null);
    }
}
