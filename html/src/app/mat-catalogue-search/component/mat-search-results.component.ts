import { Component, ViewChild, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ApplicationSettings } from '../../app.config';
import { Router, ActivatedRoute } from '@angular/router';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { CoreComponent } from '../../core/component/core.component';
import { FunctionalityAccessHandle } from '../../core/model/functionality.model';
import { ReportManagerService } from '../../core/service/report-manager.service';
import { Context } from '../../core/model/security.model';
import { DataFilter } from '../../core/model/data-filter.model';
import { MaterialItemDetailComponent } from './mat-item-detail.component';
import { MaterialSearchCriteria, MaterialEntity } from '../model/mat-catalogue-search.model';
import { MaterialCatalogueSearchService } from '../service/mat-catalogue-search.service';
import { MaterialSearchResults } from '../model/mat-catalogue-search.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { ComponentState } from '../../core/model/component.model';
import { DataTableComponent } from '../../data-table/component/data-table.component';
import { FilterBarComponent, FilterBarState } from '../../data-table/component/filter-bar.component';
import { RowClickedEvent } from '../../data-table/component/row.component';
import { RecordFilterCriteria } from '../../core/model/data-record-filter.model';

@Component
({
    selector: 'mat-search-results',
    templateUrl: 'mat-search-results.component.html',
    styleUrls: ['mat-search-results.component.css'],
    providers: [ ]
})
export class MaterialSearchResultsComponent extends CoreComponent<SearchResultsState> implements OnInit
{   
    private searchService: MaterialCatalogueSearchService;
    private reportManager: ReportManagerService;
    private selectedObject: MaterialEntity;
    private itemMenuVisible: boolean;
    private loading: boolean;
    private tableSetup: any;
    private cardSetup: any;

    @ViewChild('filterBar') 
    private filterBar: FilterBarComponent;

    @ViewChild('itemMenuModal') 
    private itemMenuModal: ModalDirective;

    @ViewChild('resultTable')
    dataTable: DataTableComponent;

    public static INPUT_PARAMETER_SEARCH_CRITERIA: string = "searchCriteria";

    constructor(router: Router, activatedRoute: ActivatedRoute, componentManager: ComponentManagerService, functionalityManager: FunctionalityManagerService, reportManagerService: ReportManagerService, searchService: MaterialCatalogueSearchService)
    {
        super(router, activatedRoute, functionalityManager, componentManager);
        this.reportManager = reportManagerService;
        this.searchService = searchService;
        this.loading = false;
        this.selectedObject = null;
    }

    ngOnInit()
    {
        // Get the table and card setup from this component's input parameters.
        this.tableSetup = this.parameters.tableSetup;
        this.cardSetup = this.parameters.cardSetup;

        // If this is a new state, process component input parameters.
        if (this.newState)
        {
            // Process the input parameters of the state.
            if (this.state.inputParameters)
            {
                // Async in order to allow state to be set on the filter-bar before we actually query it for a filter to use in the search.
                setTimeout(() => this.newSearch(this.state.inputParameters[MaterialSearchResultsComponent.INPUT_PARAMETER_SEARCH_CRITERIA]));
            }
        }
    }

    private onItemSelected(object: MaterialEntity)
    {
        // Set the input parameter for the item detail page and then navigate to it.
        this.selectedObject = object;
        this.componentManager.setComponentInputParameters("ITEM_DETAIL", { [MaterialItemDetailComponent.INPUT_PARAMETER_FILE_ID]: object.recordId });
        this.router.navigate(['./item-detail/' + object.recordId], { relativeTo: this.route, queryParams: this.getSubComponentQueryParameters() });
    }

    public newSearch(searchCriteria: MaterialSearchCriteria)
    {
        if (searchCriteria)
        {
            this.state.filterBarState.searchExpression = searchCriteria.searchExpression;
            this.state.filterBarState.recordFilterCriteria = searchCriteria.recordFilterCriteria;
        }
        else
        {
            this.state.filterBarState.searchExpression = undefined;
            this.state.filterBarState.recordFilterCriteria = undefined;
        }

        this.state.filterBarState.pageOffset = 0;
        this.state.filterBarState.pageNumber = 1;
        this.search();
    }

    private search()
    {
        this.loading = true;
        this.searchService.searchItems(this.state.filterBarState.searchExpression, this.getSearchFilter(), this.state.filterBarState.pageOffset, this.state.filterBarState.pageSize)
        .subscribe
            (
                data => 
                {
                    this.loading = false;
                    this.state.searchResults = data;
                    this.state.searchPattern = "";
                    
                    // Add the search terms returned from the server to the local search pattern.
                    for (let searchTerm of data.searchTerms)
                    {
                        if (this.state.searchPattern.length > 0) this.state.searchPattern += "|";
                        this.state.searchPattern += searchTerm;
                    }

                    // Add the filter bar filter terms to the search pattern.
                    for (let searchTerm of this.filterBar.getFilterTerms())
                    {
                        if (this.state.searchPattern.length > 0) this.state.searchPattern += "|";
                        this.state.searchPattern += searchTerm;
                    }

                    // Save the current state of this component.
                    this.saveState();
                },
                error => 
                {
                    console.log("Error while searching material catalogue: " + error);
                    this.loading = false;
                }
            );
    }

    private getSearchFilter(): DataFilter
    {
        let filter: DataFilter;

        // Get the filter from the filter bar component.
        filter = this.filterBar.getDataFilter();
        filter.entityId = this.parameters.entityId;
        return filter;
    }

    private generateReport()
    {
        let reportId: string;
        let reportColumnMapping;

        // Create the column mapping to be used for report generation.
        reportColumnMapping = {};
        for (let field of this.tableSetup.fields)
        {
            if (field.visible)
            {
                reportColumnMapping[field.name] = field.id;
            }
        }

        // Send the request for the report to be generated.
        reportId = this.parameters.reportId;
        this.reportManager.generateReport(this.ctx, reportId, {[reportId + "$P_COLUMN_MAPPING"]:reportColumnMapping})
        .subscribe
            (
                data =>
                {
                    this.componentManager.toastInfo("Search Results Report", "Your report is being generated and should be available shortly");
                },
                error =>
                {
                    console.log("Error while generating search resultsl report: " + error);
                }
            );
    }

    rowDoubleClick(rowEvent: any) 
    {
        alert('Double clicked: ' + JSON.stringify(rowEvent.row.entity));
    }

    rowTooltip(item: any) 
    { 
        return item.corporateNumber; 
    }

    onSelectionChanged(event: any)
    {
        console.log(event);
    }

    public showItemMenu():void 
    {
        this.itemMenuVisible = true;
    }

    public hideItemMenu():void 
    {
        this.itemMenuModal.hide();
    }

    public onItemMenuHidden():void 
    {
        this.itemMenuVisible = false;
    }

    public onItemFunctionalitySelection(functionality: any)
    {
        this.hideItemMenu();
    }

    protected createState(): SearchResultsState
    {
        this.state = new SearchResultsState();
        return this.state;
    }

    protected getState(): SearchResultsState
    {
        return this.state;
    }

    protected setState(state: SearchResultsState)
    {
        this.state = state;
    }
}

class SearchResultsState extends ComponentState
{
    filterBarState: FilterBarState;
    searchResults: MaterialSearchResults;
    searchPattern: string;

    constructor()
    {
        super();
        this.filterBarState = new FilterBarState();
        this.searchResults = new MaterialSearchResults();
    }
}
