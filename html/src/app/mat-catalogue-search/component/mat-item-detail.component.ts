import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReportManagerService } from '../../core/service/report-manager.service';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { MaterialCatalogueSearchService } from '../service/mat-catalogue-search.service';
import { FunctionalityHandle } from '../../core/model/functionality.model';
import { Context } from '../../core/model/security.model';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CoreComponent } from '../../core/component/core.component';
import { ComponentState } from '../../core/model/component.model';
import { ValueStringSettings, BooleanValue } from '../../core/model/data-record.model';
import 'rxjs/add/operator/map';
import { generate } from 'rxjs/observable/generate';
import { ApplicationSettings } from '../../app.config';
import { ModalDirective } from 'ngx-bootstrap';

@Component
({
    selector: 'mat-item-detail',
    templateUrl: 'mat-item-detail.component.html',
    styleUrls: ['mat-item-detail.component.css']
})
export class MaterialItemDetailComponent extends CoreComponent<MaterialItemDetailState> 
{
    private reportManager: ReportManagerService;
    private toastr: ToastrService;
    private valueStringSettings: ValueStringSettings;
    private searchService: MaterialCatalogueSearchService;
    private loading: boolean;
    private isImageLookupShown: boolean;
    private preferredImageUrl: any;
    private secondaryImageUrls: any[];

    public static INPUT_PARAMETER_FILE_ID: string = "fileId"; 

    constructor(router: Router, activatedRoute: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, searchService: MaterialCatalogueSearchService, reportManagerService: ReportManagerService, toastrService: ToastrService)
    {
        super(router, activatedRoute, functionalityManager, componentManager);
        this.reportManager = reportManagerService;
        this.toastr = toastrService;
        this.valueStringSettings = new ValueStringSettings();
        this.valueStringSettings.addExcludedType(BooleanValue.TYPE_ID);
        this.searchService = searchService;
        this.loading = false;
        this.isImageLookupShown = false;
        this.initComponent();
    }

    @ViewChild('autoShownModal') 
    private autoShownModal: ModalDirective;

    protected initComponent()
    {
        // If this is a new state, process component input parameters.
        if (this.newState)
        {
            if (this.state.inputParameters)
            {
                let fileId: string;

                fileId = this.state.inputParameters[MaterialItemDetailComponent.INPUT_PARAMETER_FILE_ID];
                if (fileId)
                {
                    this.loadItemDetail(fileId);
                }
            }
        }
    }

    public generateReport()
    {
        let reportId: string;

        reportId = "#REPORT_JASPER_MAT_ITEM_DETAIL"; // This id must be passed to the component from the server-side initialization script and not hardcoded.
        this.reportManager.generateReport(this.ctx, reportId, {[reportId + "$P_ROOT_RECORD_ID"]:this.state.fileId})
        .subscribe
            (
                data =>
                {
                    this.toastr.info("Your report is being generated and should be available shortly", "Material Detail Report");
                },
                error =>
                {
                    console.log("Error while generating mat item detail report: " + error);
                }
            );
    }

    loadItemDetail(fileId: string)
    {
        this.state.fileId = fileId;
        this.loading = true;
        this.searchService.getItemDetail(fileId)
        .subscribe
            (
                data =>
                {
                    this.loading = false;
                    this.setItemDetail(data);
                },
                error =>
                {
                    console.log("Error while loading item detail: " + error);
                }
            );
    }

    private setItemDetail(dataMap: any)
    {
        this.state.dataMap = dataMap;
        this.saveState();

        this.preferredImageUrl = this.getPreferredImageUrl();
        this.secondaryImageUrls = this.getImageUrls();
    }

    private getPreferredImageUrl(): any
    {
        let preferredImageUrl: any;
        let preferredImageId: any;

        preferredImageId = this.state.dataMap.imageIds[0];

        if(preferredImageId != null)
        {
            preferredImageUrl = this.getImageUrl(preferredImageId);
        }
        else
        {
            preferredImageUrl = ApplicationSettings.NO_IMAGE_URL;
        }

        return preferredImageUrl;
    }

    private getImageUrls(): any[]
    {
        let imageUrls: any[];
        let imageIds: any[];
        
        imageIds = this.state.dataMap.imageIds;
        imageIds.splice(0,1);
       
        imageUrls = new Array();
        for(let imageId of imageIds)
        {
            imageUrls.push(this.getImageUrl(imageId));
        }

        return imageUrls;
    }

    private getImageUrl(imageId: any): any
    {
        let imageUrl: any;

        imageUrl = ApplicationSettings.API_ATTACHMENT_URL + "/" + imageId;
        return imageUrl;
    }

    public onImageLookupHidden():void 
    {
        this.isImageLookupShown = false;
    }

    public hideImageLookup():void 
    {
        this.autoShownModal.hide();
    }

    public showImageLookup():void 
    {
        this.isImageLookupShown = true;
    }

    protected createState(): MaterialItemDetailState
    {
        this.state = new MaterialItemDetailState();
        return this.state;
    }

    protected getState(): MaterialItemDetailState
    {
        return this.state;
    }

    protected setState(state: MaterialItemDetailState)
    {
        this.state = state;
    }
}

class MaterialItemDetailState extends ComponentState
{
    fileId: string;
    dataMap: any;

    constructor()
    {
        super();
        this.dataMap = new Map<String, String>();
    }
}
