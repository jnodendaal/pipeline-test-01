import { Component, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { FunctionalityAccessHandle } from '../../core/model/functionality.model';
import { Context } from '../../core/model/security.model';
import { MaterialCatalogueSearchService } from '../service/mat-catalogue-search.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ComponentManagerService } from '../../core/service/component-manager.service';
import { CoreComponent } from '../../core/component/core.component';
import { ComponentState } from '../../core/model/component.model';

@Component
({
    selector: 'mat-catalogue-search',
    templateUrl: 'mat-catalogue-search.component.html',
    styleUrls: ['mat-catalogue-search.component.css'],
    providers: [ ComponentManagerService ]
})
export class MaterialCatalogueSearchComponent extends CoreComponent<MaterialCatalogueSearchState> implements OnDestroy
{   
    private searchService: MaterialCatalogueSearchService;
    private loading: boolean;
    private navbarCollapsed: boolean;
    
    constructor(router: Router, route: ActivatedRoute, functionalityManager: FunctionalityManagerService, componentManager: ComponentManagerService, searchService: MaterialCatalogueSearchService)
    {
        super(router, route, functionalityManager, componentManager);
        this.searchService = searchService;
        this.loading = false;
        this.navbarCollapsed = true;
        this.searchService.setFunctionalityIid(this.accessHandle.iid);
    }

    public ngOnDestroy()
    {
        this.functionalityManager.releaseFunctionality(this.accessHandle.iid).subscribe();
    }

    private toggleNavbarCollapse()
    {
        this.navbarCollapsed = !this.navbarCollapsed;
    }

    protected createState(): MaterialCatalogueSearchState
    {
        this.state = new MaterialCatalogueSearchState();
        return this.state;
    }

    protected getState(): MaterialCatalogueSearchState
    {
        return this.state;
    }

    protected setState(state: MaterialCatalogueSearchState)
    {
        this.state = state;
    }
}

class MaterialCatalogueSearchState extends ComponentState
{
}
