import { DataFilter } from '../../core/model/data-filter.model';
import { DataEntity } from '../../core/model/data-entity.model';
import { RecordFilterCriteria } from '../../core/model/data-record-filter.model';

export class MaterialEntity extends DataEntity
{
    recordId:string;
    stateTerm: string;
    corporateNumber: string;
    erpNumber: string;
    partNumber: string;
    longDescription: string;
    shortDescription: string;
    lastActivityAt: number;
    imageAttachmentId: string;

    constructor()
    {
        super();
    }

    public fromJson(jsonObject: any): MaterialEntity
    {
        super.fromJson(jsonObject);
        this.recordId = this["$ROOT_RECORD_ID"];
        this.stateTerm = this["$CORPORATE_STATE_TERM"];
        this.corporateNumber = this["$CORPORATE_NUMBER"];
        this.erpNumber = this["$ERP_NUMBER"];
        this.longDescription = this["$PURCHASE_ORDER_DESCRIPTION"];
        this.shortDescription = this["$SHORT_FORMAT_DESCRIPTION"];
        this.partNumber = this["$PART_NUMBER"];
        this.lastActivityAt = this["$LAST_ACTIVITY_AT"];
        this.imageAttachmentId = this["$IMAGE_ATTACHMENT_ID"];
        return this;
    }
}

export class MaterialSearchCriteria
{
    searchExpression: string;
    recordFilterCriteria: RecordFilterCriteria;

    constructor(searchExpression: string, recordFilterCriteria: RecordFilterCriteria)
    {
        this.searchExpression = searchExpression;
        this.recordFilterCriteria = recordFilterCriteria;
    }
}

export class MaterialSearchResults
{
    searchExpression: string;
    searchTerms: string;
    pageOffset: number;
    entities: MaterialEntity[];

    constructor()
    {
        this.entities = new Array();
    }

    public addObjectsFromJson(jsonObjects: any)
    {
        if (jsonObjects)
        {
            for (let jsonObject of jsonObjects)
            {
                this.entities.push(new MaterialEntity().fromJson(jsonObject) as MaterialEntity);
            }
        }
    }
}

export class TemplateEntity extends DataEntity
{
    drIid: string;
    term: string;

    constructor()
    {
        super();
    }

    public fromJson(jsonObject: any): TemplateEntity
    {
        super.fromJson(jsonObject);
        this.drIid = this["$CONCEPT_ID"];
        this.term = this["$TERM"];
        return this;
    }
}

export class TemplateSearchResults
{
    searchExpression: string;
    searchTerms: string;
    pageOffset: number;
    entities: TemplateEntity[];

    constructor()
    {
        this.entities = new Array();
    }

    public addDescriptorsFromJson(jsonDescriptors: any)
    {
        if (jsonDescriptors)
        {
            for (let jsonDescriptor of jsonDescriptors)
            {
                this.entities.push(new TemplateEntity().fromJson(jsonDescriptor));
            }
        }
    }
}