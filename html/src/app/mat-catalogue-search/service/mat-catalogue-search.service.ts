import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { MaterialSearchResults, TemplateSearchResults } from '../model/mat-catalogue-search.model';
import 'rxjs/add/operator/map'
import { MaterialItemDetailComponent } from '../component/mat-item-detail.component';
import { DataFilter } from './../../core/model/data-filter.model';
 
@Injectable()
export class MaterialCatalogueSearchService 
{
    private accessParameters: HttpParams;

    constructor(private http: HttpClient) 
    {
        console.log("MaterialCatalogueSearchService");
        this.accessParameters = new HttpParams();
    }

    public setFunctionalityIid(functionalityIid: string)
    {
        if (functionalityIid != null)
        {
            this.accessParameters = this.accessParameters.set('functionality_iid', functionalityIid);
        }
        else this.accessParameters.delete('functionality_iid');
    }

    public searchItems(searchExpression: string, filter: DataFilter, pageOffset: number, pageSize: number): Observable<MaterialSearchResults>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '#OP_SEARCH_ITEMS$P_SEARCH_EXPRESSION': searchExpression, 
            '#OP_SEARCH_ITEMS$P_FILTER': filter,
            '#OP_SEARCH_ITEMS$P_PAGE_OFFSET': pageOffset,
            '#OP_SEARCH_ITEMS$P_PAGE_SIZE': pageSize
        };

        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('#OP_SEARCH_ITEMS'), JSON.stringify(input), { withCredentials: true, params: this.accessParameters })
            .map((response: Response) => 
            {
                let searchResults = new MaterialSearchResults();

                console.log(response);
                searchResults.pageOffset = pageOffset;
                searchResults.searchExpression = searchExpression;
                searchResults.searchTerms = response['#OP_SEARCH_ITEMS$P_SEARCH_TERMS'];
                searchResults.addObjectsFromJson(response['#OP_SEARCH_ITEMS$P_CATALOGUE_ITEMS']);
                console.log(searchResults);
                return searchResults;
            });
    }

    public searchDescriptors(searchExpression: string, filter: DataFilter, pageOffset: number, pageSize: number): Observable<TemplateSearchResults>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '#OP_SEARCH_TEMPLATES$P_SEARCH_EXPRESSION': searchExpression, 
            '#OP_SEARCH_TEMPLATES$P_FILTER': filter,
            '#OP_SEARCH_TEMPLATES$P_PAGE_OFFSET': pageOffset,
            '#OP_SEARCH_TEMPLATES$P_PAGE_SIZE': pageSize
        };

        console.log(this.accessParameters);
        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('#OP_SEARCH_TEMPLATES'), JSON.stringify(input), { withCredentials: true, params: this.accessParameters })
            .map((response: Response) => 
            {
                let searchResults = new TemplateSearchResults();

                console.log(response);
                searchResults.pageOffset = pageOffset;
                searchResults.searchExpression = searchExpression;
                searchResults.searchTerms = response['#OP_SEARCH_TEMPLATES$P_SEARCH_TERMS'];
                searchResults.addDescriptorsFromJson(response['#OP_SEARCH_TEMPLATES$P_TEMPLATES']);
                console.log(searchResults);
                return searchResults;
            });
    }

    public getItemDetail(rootRecordId: string): Observable<any>
    {
        // Create the operation input parameters.
        const input = 
        { 
            '#OP_RETRIEVE_DATA_RECORD_DETAIL$P_RECORD_ID': rootRecordId, 
        };
        
        console.log(this.accessParameters);
        // Post a request to the server-side operation and return the observable result.
        return this.http.post(ApplicationSettings.API_OPERATION_URL + '/' + encodeURIComponent('#OP_RETRIEVE_DATA_RECORD_DETAIL'), JSON.stringify(input), { withCredentials: true, params: this.accessParameters })
            .map((response: Response) => 
            {
                console.log(response);
                return response['#OP_RETRIEVE_DATA_RECORD_DETAIL$P_DETAIL_MAP'];
            });
    }
}
