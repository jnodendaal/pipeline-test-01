import { NgModule }                               from '@angular/core';
import { Routes, RouterModule }                   from '@angular/router';
import { CommonModule }                           from '@angular/common';
import { FormsModule }                            from '@angular/forms';
import { SharedModule }                           from '../core/shared.module';
import { DataFileEditorModule }                   from '../data-file-editor/data-file-editor.module';
import { DataTableModule }                        from '../data-table/data-table.module';
import { MaterialCatalogueSearchComponent }       from './component/mat-catalogue-search.component';
import { MaterialCatalogueSearchService }         from './service/mat-catalogue-search.service';
import { MaterialSearchResultsComponent }         from './component/mat-search-results.component';
import { MaterialQuickSearchComponent }           from './component/mat-quick-search.component';
import { MaterialAdvancedSearchComponent }        from './component/mat-advanced-search.component';
import { MaterialTemplateSearchComponent }      from './component/mat-template-search.component';
import { MaterialItemMenuComponent }              from './component/mat-item-menu.component';
import { MaterialItemDetailComponent }            from './component/mat-item-detail.component';
import { DataFileSearchModule }                   from '../data-file-search/data-file-search.module';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    SharedModule,
    DataFileEditorModule,
    DataFileSearchModule,
    DataTableModule,
    RouterModule.forChild(
    [
      { 
        path: '', 
        component: MaterialCatalogueSearchComponent, data: { id: "CATALOGUE_SEARCH" },
        children: 
        [
          { path: '', redirectTo: 'quick', pathMatch: 'full' },
          { 
            path: 'quick', component: MaterialQuickSearchComponent, data: { id: "QUICK_SEARCH", breadcrumb: 'Quick Search' },
            children: 
            [
              { path: 'item-detail/:record_id', component: MaterialItemDetailComponent, data: { id: "ITEM_DETAIL", breadcrumb: 'Item Detail' } }
            ]
          },
          { 
            path: 'advanced', component: MaterialAdvancedSearchComponent, data: { id: "ADVANCED_SEARCH", breadcrumb: 'Advanced Search' },
            children: 
            [
              { 
                path: 'search-results', component: MaterialSearchResultsComponent, data: { id: "SEARCH_RESULTS", breadcrumb: 'Search Results' } ,
                children: 
                [
                  { path: 'item-detail/:record_id', component: MaterialItemDetailComponent, data: { id: "ITEM_DETAIL", breadcrumb: 'Item Detail' } }
                ]
              }
            ] 
          },
          { path: 'template', component: MaterialTemplateSearchComponent, data: { id: "TEMPLATE_SEARCH", breadcrumb: 'Template Search' } }
        ]
      }
    ])
  ],
  declarations: 
  [ 
    MaterialCatalogueSearchComponent,
    MaterialSearchResultsComponent,
    MaterialItemMenuComponent,
    MaterialQuickSearchComponent,
    MaterialAdvancedSearchComponent,
    MaterialTemplateSearchComponent,
    MaterialItemDetailComponent
  ],
  exports:      [ MaterialCatalogueSearchComponent ],
  providers:    [ MaterialCatalogueSearchService ]
})
export class MaterialCatalogueSearchModule 
{
}