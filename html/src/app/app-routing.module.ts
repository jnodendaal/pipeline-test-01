import { NgModule }                           from '@angular/core';
import { Routes, RouterModule }               from '@angular/router';
import { FrameComponent }                     from './frame/component/frame.component';
import { LoginComponent }                     from './frame/component/login.component';
import { FunctionalityAccessComponent }       from './core/component/functionality-access/functionality-access.component';
import { FunctionalityCardMenuComponent }     from './core/component/functionality-card-menu/functionality-card-menu.component';
import { WorkflowClientComponent }            from './core/component/workflow-client/workflow-client.component';
import { TaskListComponent }                  from './core/component/task-list/task-list.component';
import { ReportListComponent }                from './core/component/report-list/report-list.component';
import { ProcessListComponent }               from './core/component/process-list/process-list.component';

export const routes: Routes = 
[
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'main', component: FrameComponent,
    children: 
    [
      // Core routes.
      { path: '', redirectTo: 'functionality-menu/@fnc_ng_home', pathMatch: 'full' }, 
      { path: 'home', redirectTo: 'functionality-menu/@fnc_ng_home' },
      { path: 'tasks', redirectTo: 'functionality/@fnc_ng_tasks' },
      { path: 'task-list', component: TaskListComponent, data: { id: "TASK_LIST", breadcrumb: 'Task List' } },
      { path: 'reports', redirectTo: 'functionality/@fnc_ng_reports' },
      { path: 'report-list', component: ReportListComponent, data: { id: "REPORT_LIST", breadcrumb: 'Report List' } },
      { path: 'processes', redirectTo: 'functionality/@fnc_ng_processes' },
      { path: 'process-list', component: ProcessListComponent, data: { id: "PROCESS_LIST", breadcrumb: 'Process List' } },
      { path: 'functionality/:functionality_id', component: FunctionalityAccessComponent },
      { path: 'functionality-menu/:functionality_id', component: FunctionalityCardMenuComponent },
      { path: 'workflow-client', component: WorkflowClientComponent, data: { id: "WORKFLOW_CLIENT", breadcrumb: 'Workflow' } },
      
      // Project specific routes.
      { path: 'mat-data-quality-report', loadChildren: './mat-data-quality-report/mat-data-quality-report.module#MaterialDataQualityReportModule'},
      { path: 'mat-creation-workflow', loadChildren: './mat-creation-workflow/mat-creation-workflow.module#MaterialCreationWorkflowModule'},
      { path: 'mat-catalogue-search', loadChildren: './mat-catalogue-search/mat-catalogue-search.module#MaterialCatalogueSearchModule', data: { breadcrumb: 'Material Search' }},
      { path: 'srv-data-quality-report', loadChildren: './srv-data-quality-report/srv-data-quality-report.module#ServiceDataQualityReportModule'},
      { path: 'ven-data-quality-report', loadChildren: './ven-data-quality-report/ven-data-quality-report.module#VendorDataQualityReportModule'},
      { path: 'admin/record-editor', loadChildren: './admin/admin.module#AdminModule'}
    ]
  }
];

@NgModule
({
  imports: 
  [
    RouterModule.forRoot
    (
      routes,
      { enableTracing: false } // Enabled for debugging purposes only.
    )
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}