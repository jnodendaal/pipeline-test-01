export class ApplicationSettings 
{
  public static API_URL: string;
  public static API_OPERATION_URL: string;
  public static API_ATTACHMENT_URL: string;
  public static API_REPORT_URL: string;
  public static API_IMAGE_URL: string;

  public static NO_IMAGE_URL: string;
  public static NO_PROFILE_THUMBNAIL_URL: string;
};

