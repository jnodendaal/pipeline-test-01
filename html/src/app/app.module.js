"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var core_module_1 = require("./core/core.module");
var shared_module_1 = require("./core/shared.module");
var data_file_editor_module_1 = require("./core/components/data-file-editor/data-file-editor.module");
var app_component_1 = require("./app.component");
var app_frame_component_1 = require("./core/components/frame/app-frame.component");
var main_navbar_component_1 = require("./core/components/frame/main-navbar.component");
var main_header_component_1 = require("./core/components/frame/main-header.component");
var main_breadcrumb_component_1 = require("./core/components/frame/main-breadcrumb.component");
var user_component_1 = require("./core/components/frame/user.component");
var home_component_1 = require("./core/components/frame/home.component");
var login_component_1 = require("./core/components/login/login.component");
var alert_component_1 = require("./core/components/login/alert.component");
var alert_service_1 = require("./core/components/login/alert.service");
var tasks_component_1 = require("./core/tasks/tasks.component");
var task_menu_component_1 = require("./core/tasks/task-menu.component");
var task_list_component_1 = require("./core/tasks/task-list.component");
var task_card_component_1 = require("./core/tasks/task-card.component");
var generic_task_component_1 = require("./core/tasks/generic-task.component");
var app_routing_module_1 = require("./app-routing.module");
/*
  Included Modules:
  - npm install chart.js --save
  - npm install ng2-charts --save
  - npm install ngx-bootstrap --save
  - npm install angular2-contextmenu
*/
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            app_routing_module_1.AppRoutingModule,
            ngx_bootstrap_1.Ng2BootstrapModule.forRoot(),
            core_module_1.CoreModule,
            shared_module_1.SharedModule,
            data_file_editor_module_1.DataFileEditorModule
        ],
        providers: [
            alert_service_1.AlertService
        ],
        declarations: [
            app_component_1.AppComponent,
            app_frame_component_1.AppFrameComponent,
            login_component_1.LoginComponent,
            alert_component_1.LoginAlertComponent,
            main_navbar_component_1.MainNavbarComponent,
            main_header_component_1.MainHeaderComponent,
            main_breadcrumb_component_1.MainBreadcrumbComponent,
            user_component_1.UserComponent,
            home_component_1.HomeComponent,
            tasks_component_1.TasksComponent,
            task_menu_component_1.TaskMenuComponent,
            task_list_component_1.TaskListComponent,
            task_card_component_1.TaskCardComponent,
            generic_task_component_1.GenericTaskComponent
        ],
        bootstrap: [
            app_component_1.AppComponent
        ]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map