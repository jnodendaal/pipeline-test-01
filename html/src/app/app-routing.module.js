"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var app_frame_component_1 = require("./core/components/frame/app-frame.component");
var home_component_1 = require("./core/components/frame/home.component");
var login_component_1 = require("./core/components/login/login.component");
var demo_component_1 = require("./core/components/data-table/demo/demo.component");
var tasks_component_1 = require("./core/tasks/tasks.component");
var generic_task_component_1 = require("./core/tasks/generic-task.component");
var functionality_card_menu_component_1 = require("./core/components/functionality-card-menu/functionality-card-menu.component");
exports.routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'main', component: app_frame_component_1.AppFrameComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: home_component_1.HomeComponent },
            { path: 'tasks', component: tasks_component_1.TasksComponent },
            { path: 'task/:iid', component: generic_task_component_1.GenericTaskComponent },
            { path: 'reports', component: functionality_card_menu_component_1.FunctionalityCardMenuComponent },
            { path: 'report/material_data_quality_report', loadChildren: 'app/mmm/reports/data-quality/material-data-quality-report.module#MaterialDataQualityReportModule' },
            { path: 'report/service_data_quality_report', loadChildren: 'app/smm/reports/data-quality/service-data-quality-report.module#ServiceDataQualityReportModule' },
            { path: 'report/vendor_data_quality_report', loadChildren: 'app/vmm/reports/data-quality/vendor-data-quality-report.module#VendorDataQualityReportModule' },
            { path: 'processes', component: demo_component_1.DataTableDemoComponent }
        ]
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(exports.routes)],
        exports: [router_1.RouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map