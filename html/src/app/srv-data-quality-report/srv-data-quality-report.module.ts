import { NgModule }                               from '@angular/core';
import { Routes, RouterModule }                   from '@angular/router';
import { CommonModule }                           from '@angular/common';
import { FormsModule }                            from '@angular/forms';
import { ChartsModule }                           from 'ng2-charts';
import { SharedModule }                           from '../core/shared.module';
import { T8ChartsModule }                         from '../core/component/charts/charts.module';
import { DataFileEditorModule }                   from '../data-file-editor/data-file-editor.module';
import { ServiceDataQualityReportComponent }      from './component/srv-data-quality-report.component';
import { DataQualityTableComponent }              from './component/srv-data-quality-table.component';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    SharedModule,
    DataFileEditorModule,
    ChartsModule,
    T8ChartsModule,
    RouterModule.forChild(
    [
        { path: '', component: ServiceDataQualityReportComponent }
    ])
  ],
  declarations: 
  [ 
    ServiceDataQualityReportComponent,
    DataQualityTableComponent
  ],
  exports:      [ ServiceDataQualityReportComponent ],
  providers:    [ ]
})
export class ServiceDataQualityReportModule 
{
  constructor()
  {
  }
}