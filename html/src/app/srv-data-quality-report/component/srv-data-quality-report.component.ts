import { Component, ViewChild, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { DataQualityService } from '../../core/service/data-quality.service';
import { ReportDefinition } from './srv-data-quality-report.component';
import { GroupedDataQualityReport, DataQualityMetricsGroup, DataQualityMetricsSummary } from '../../core/model/data-record-quality.model';
import { BarChartComponent, BarChartModel, BarChartCategoryModel } from '../../core/component/charts/bar-chart.component';
import { DoughnutChartComponent, DoughnutChartDefinition } from '../../core/component/charts/doughnut-chart.component';
import { DataQualityTableComponent, RecordSelectedEvent } from './srv-data-quality-table.component';
import { NumberUtility } from '../../core/utility/number.utility';

@Component
({
    selector: 'srv-data-quality-report',
    templateUrl: 'srv-data-quality-report.component.html',
    styleUrls: [ 'srv-data-quality-report.component.css' ],
    providers: [ DataQualityService ]
})
export class ServiceDataQualityReportComponent  
{   
    private definition: DataQualityAspectReportDefinition;
    private dqService: DataQualityService;
    private dqReport: GroupedDataQualityReport;
    private dqSummary: DataQualityMetricsSummary;
    private selectedBreakdownCategory: BarChartCategoryModel;
    private selectedRecord: RecordDetails;
    private breakdownChartPageSize: number;
    private breakdownChartPage: number;
    private loading: boolean;

    @ViewChild('completenessChart')
    private completenessChart: DoughnutChartComponent;

    @ViewChild('validityChart')
    private validityChart: DoughnutChartComponent;

    @ViewChild('breakdownChart')
    private breakdownChart: BarChartComponent;

    @ViewChild('recordTable')
    private recordTable: DataQualityTableComponent;

    @ViewChild('dataFileEditor')
    private datFileEditor: DataFileEditorComponent;

    @ViewChild('autoShownModal') public autoShownModal:ModalDirective;
    public isModalShown:boolean = false;
    
    constructor(dqService: DataQualityService)
    {
        this.dqService = dqService;
        this.dqReport = null;
        this.loading = false;
        this.breakdownChartPage = 0;
        this.breakdownChartPageSize = 15;
    }

    ngOnInit() 
    {
        let definition: DataQualityAspectReportDefinition;
        
        definition = new DataQualityAspectReportDefinition("2", "Service Category Breakdown");
        definition.rootDrInstanceId = "6D6879E08B1E4D47B0609608D257C96F";
        definition.groupByTargetDrInstanceId = "79146BCF36214690AB0D46AE9C4FC6CD";
        definition.groupByTargetPropertyId = "4AA9C6BD50014BA0A9EA9281DFC770CF";
        this.setDefinition(definition);
        this.refresh();
    }

    public setDefinition(definition: DataQualityAspectReportDefinition)
    {
        this.definition = definition;
    }

    public setDataQualityReport(dqReport: GroupedDataQualityReport)
    {
        let breakdownChartDefinition: BarChartModel;
        let completenessChartDefinition: DoughnutChartDefinition;
        let validityChartDefinition: DoughnutChartDefinition;

        // Update the local reference to the new report. 
        this.dqReport = dqReport;
        this.dqSummary = dqReport.getSummary();

        // Create the completeness chart definition.
        completenessChartDefinition = new DoughnutChartDefinition();
        completenessChartDefinition.addValue("Complete Properties", NumberUtility.round(this.dqSummary.characteristicCompleteness, -2));
        completenessChartDefinition.addValue("Incomplete Properties", NumberUtility.round(100 - this.dqSummary.characteristicCompleteness, -2));
        completenessChartDefinition.legendEnabled = false;
        this.completenessChart.setDefinition(completenessChartDefinition);

        // Create the validity chart definition.
        validityChartDefinition = new DoughnutChartDefinition();
        validityChartDefinition.addValue("Valid Properties", NumberUtility.round(this.dqSummary.characteristicValidity, -2));
        validityChartDefinition.addValue("Invalid Properties", NumberUtility.round(100 - this.dqSummary.characteristicValidity, -2));
        validityChartDefinition.legendEnabled = false;
        this.validityChart.setDefinition(validityChartDefinition);

        // Update the breakdown chart definition.
        this.refreshChartModel();
    }

    private refreshChartModel()
    {
        let breakdownChartDefinition: BarChartModel;
        let pageStartIndex;
        let pageEndIndex;

        // Update the breakdown chart definition.
        breakdownChartDefinition = new BarChartModel();
        breakdownChartDefinition.addSeries("COMPLETENESS", "Characteristic Completeness");
        breakdownChartDefinition.addSeries("VALIDITY", "Characteristic Validity");

        // Add all the data points on the specified page to the chart definition.
        pageStartIndex = this.breakdownChartPage * this.breakdownChartPageSize;
        pageEndIndex = pageStartIndex + this.breakdownChartPageSize;
        for (let groupIndex = pageStartIndex; groupIndex < this.dqReport.groups.length && groupIndex < pageEndIndex; groupIndex++)
        {
            let group: DataQualityMetricsGroup;

            group = this.dqReport.groups[groupIndex];
            breakdownChartDefinition.addCategory(group.id, group.term + " (" + group.metrics.recordCount + ")");
            breakdownChartDefinition.addData("COMPLETENESS", Math.round(group.metrics.getCharacteristicCompleteness()));
            breakdownChartDefinition.addData("VALIDITY", Math.round(group.metrics.getCharacteristicValidity()));
        }
        this.breakdownChart.setModel(breakdownChartDefinition);
    }

    gotoBreakdownPage(page: number)
    {
        this.breakdownChartPage = page;
        this.refreshChartModel();
    }

    gotoBreakdownFirstPage()
    {
        this.breakdownChartPage = 0;
        this.refreshChartModel();
    }

    gotoBreakdownLastPage()
    {
        this.breakdownChartPage = this.dqReport.groups.length - this.breakdownChartPageSize + 1;
        this.refreshChartModel();
    }

    gotoBreakdownNextPage()
    {
        if ((this.breakdownChartPage + 1) * this.breakdownChartPageSize < this.dqReport.groups.length) this.breakdownChartPage++;
        this.refreshChartModel();
    }

    gotoBreakdownPreviousPage()
    {
        if (this.breakdownChartPage > 0) this.breakdownChartPage--;
        this.refreshChartModel();
    }

    public refresh()
    {
        this.loading = true;
        this.dqService.retrieveRootDataQualityReportGroupedByValueConcept(this.definition.rootDrInstanceId, this.definition.groupByTargetDrInstanceId, this.definition.groupByTargetPropertyId)
            .subscribe
            (
                data => 
                {
                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    this.loading = false;
                    setTimeout(() => this.setDataQualityReport(data)); 
                },
                error => 
                {
                    console.log("Error while retrieving data quality report: " + error);
                    this.loading = false;
                }
            );
    }

    onBreakdownChartClicked(event: any)
    {
        console.log(event);
        this.selectedBreakdownCategory = event.category;
        this.recordTable.setFilterConceptId(this.selectedBreakdownCategory.id);
        this.recordTable.reloadData();
    }

    onRecordSelected(event: RecordSelectedEvent)
    {
        console.log("Record Selected: " + event.recordId);
        this.selectedRecord = new RecordDetails(event.recordId, event.corporateNumber, event.descriptor);
        this.showModal();
        setTimeout(() => this.datFileEditor.openFile(event.recordId));
    }
 
    public showModal():void 
    {
        this.isModalShown = true;
    }

    public hideModal():void 
    {
        this.autoShownModal.hide();
    }

    public onHidden():void 
    {
        this.isModalShown = false;
    }    
}

export interface ReportDefinition
{
    id: string;
    type: string;
    title: string;
}

export class DataQualityAspectReportDefinition implements ReportDefinition
{
    id: string;
    type: string;
    title: string;
    rootDrInstanceId: string;
    groupByTargetDrInstanceId: string;
    groupByTargetPropertyId: string;

    constructor(id: string, title: string)
    {
        this.id = id;
        this.title = title;
        this.type = 'ASPECT';
    }
}

class RecordDetails
{
    recordId: string;
    corporateNumber: string;
    descriptor: string;

    constructor(recordId: string, corporateNumber: string, descriptor: string)
    {
        this.recordId = recordId;
        this.corporateNumber = corporateNumber;
        this.descriptor = descriptor;
    }
}