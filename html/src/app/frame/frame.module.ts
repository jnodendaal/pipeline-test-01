import { NgModule }                             from '@angular/core';
import { CommonModule }                         from '@angular/common';
import { FormsModule }                          from '@angular/forms';
import { RouterModule }                         from '@angular/router';
import { SharedModule }                         from '../core/shared.module';

import { FrameComponent }                       from './component/frame.component';
import { UserComponent }                        from './component/user.component';
import { LoginComponent }                       from './component/login.component';
import { UserEditorComponent }                  from './component/user-editor.component';
import { UserProfileSwitchLookupComponent }     from './component/user-profile-switch-lookup.component';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    RouterModule,
    SharedModule
  ],
  declarations: 
  [ 
    FrameComponent,
    UserComponent,
    LoginComponent,
    UserEditorComponent,
    UserProfileSwitchLookupComponent
  ],
  exports:      [ LoginComponent, FrameComponent ],
  providers:    [ ]
})
export class FrameModule 
{
}