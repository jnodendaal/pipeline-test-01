import { Component, OnDestroy } from '@angular/core';
import { SecurityManagerService } from '../../core/service/security-manager.service';
import { FunctionalityHandle, FunctionalityGroupHandle, FunctionalityAccessHandle } from '../../core/model/functionality.model';
import { HeartbeatResponse } from '../../core/model/security.model';
import { FunctionalityManagerService } from '../../core/service/functionality-manager.service';
import { Subscription } from 'rxjs/Subscription';
import { ToastrService } from 'ngx-toastr';
import 
{
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';

@Component
({
  selector: 'app-frame',
  templateUrl: 'frame.component.html',
  animations: 
  [
    trigger('badgeState', 
    [
      state('default', style(
      {
        transform: 'scale(1)'
      })),
      state('alert',   style(
      {
        transform: 'scale(1)'
      })),
      transition('default => alert', [
        animate(300, keyframes([
          style({opacity: 0, transform: 'scale(1)', offset: 0}),
          style({opacity: 1, transform: 'scale(2)', offset: 0.3}),
          style({opacity: 1, transform: 'scale(1)', offset: 1.0})
        ]))
      ])
    ])
  ]
})
export class FrameComponent implements OnDestroy
{
    private loading: boolean;
    private parameters: any;
    private functionalityId: string;
    private accessHandle: FunctionalityAccessHandle;
    private securityManager: SecurityManagerService;
    private functionalityManager: FunctionalityManagerService;
    private toastr: ToastrService;
    private heartbeatSubscription: Subscription;
    private navbarCollapsed: boolean;
    private heartbeat: HeartbeatResponse;
    private reportState: string;
    private taskState: string;
    private processState: string;
    private notificationState: string;
    private savedTaskCount: number; // Used to detect changes to task list based on updated heartbeat counts.
    private savedReportCount: number; // Used to detect changes to report list based on updated heartbeat counts.
    private savedProcessCount: number; // Used to detect changes to process list based on updated heartbeat counts.
    private savedNotificationCount: number; // Used to detect changes to notification list based on updated heartbeat counts.
  
  constructor(securityManager: SecurityManagerService, functionalityManager: FunctionalityManagerService, toastrService: ToastrService)
  {
    console.log("AppFrameComponent Constructor");
    this.securityManager = securityManager;
    this.functionalityManager = functionalityManager;
    this.functionalityId = "@FNC_NG_MAIN";
    this.toastr = toastrService;
    this.parameters = {};
    this.navbarCollapsed = true;
    this.taskState = "default";
    this.reportState = "default";
    this.processState = "default";
    this.notificationState = "default";
    this.savedTaskCount = 0;
    this.savedReportCount = 0;
    this.savedProcessCount = 0;
    this.savedNotificationCount = 0;
    this.initializeComponent();
    this.heartbeatSubscription = securityManager.heartbeat$.subscribe
        (
            heartbeat => 
            {
                this.updateHeartbeat(heartbeat);
            }
        );
  }  

  public initializeComponent()
  {
    this.loading = true;
    this.functionalityManager.accessFunctionality(this.functionalityId, null)
      .subscribe
      (
        data => 
        {
          this.accessHandle = data;
          this.parameters = data.componentParameters;
          console.log(this.parameters);
        },
        error => 
        {
          console.log("Error while accessing functionality " + this.functionalityId + ": " + error);
          this.loading = false;
        }
      );
  }

  ngOnDestroy()
  {
    // Make sure to prevent memory leak by unsubscribing.
    this.heartbeatSubscription.unsubscribe();
  }

  private toggleNavbarCollapse()
  {
    this.navbarCollapsed = !this.navbarCollapsed;
  }

  private navLinkSelected(linkType: string)
  {
    this.navbarCollapsed = true;
    switch (linkType)
    {
      case "tasks":
        this.savedTaskCount = this.heartbeat.taskCount;
        this.taskState = "default";
        break;
      case "reports":
        this.savedReportCount = this.heartbeat.reportCount;
        this.reportState = "default";
        break;
      case "processes":
        this.savedProcessCount = this.heartbeat.processCount;
        this.processState = "default";
        break;
      case "notifications":
        this.savedNotificationCount = this.heartbeat.notificationCount;
        this.notificationState = "default";
        break;
    }
  }

  private updateHeartbeat(beat: HeartbeatResponse)
  {
    // Update the local heartbeat variable.
    this.heartbeat = beat;
    
    // Check task state.
    if (this.heartbeat.taskCount > this.savedTaskCount)
    {
      this.taskState = "alert";
      this.toastr.success("You have new tasks available", "Task Update");
      this.savedTaskCount = this.heartbeat.taskCount;
    }
    else 
    {
      this.savedTaskCount = this.heartbeat.taskCount;
    }

    // Check report state.
    if (this.heartbeat.reportCount > this.savedReportCount)
    {
      this.reportState = "alert";
      this.toastr.success("You have new reports available", "Report Update");
      this.savedReportCount = this.heartbeat.reportCount;
    }
    else 
    {
      this.savedReportCount = this.heartbeat.reportCount;
    }

    // Check process state.
    if (this.heartbeat.processCount > this.savedProcessCount)
    {
      this.processState = "alert";
      this.savedProcessCount = this.heartbeat.processCount;
    }
    else 
    {
      this.savedProcessCount = this.heartbeat.processCount;
    }

    // Check notification state.
    if (this.heartbeat.notificationCount > this.savedNotificationCount)
    {
      this.notificationState = "alert";
      this.savedNotificationCount = this.heartbeat.notificationCount;
    }
    else 
    {
      this.savedNotificationCount = this.heartbeat.notificationCount;
    }
  }
}
