import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SecurityManagerService } from '../../core/service/security-manager.service';
import { UserManagerService } from '../../core/service/user-manager.service';
import { ApplicationSettings } from '../../app.config';
import { Subscription } from 'rxjs/Subscription';
import { SessionDetails, UserProfileDetails } from '../../core/model/user.model';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component
({
    selector: 'user',
    templateUrl: 'user.component.html',
    styleUrls: ['user.component.css']
})
export class UserComponent implements OnDestroy 
{   
    private router: Router;
    private loggedIn: boolean;
    private userThumbnailUrl: string;
    private sessionDetails: SessionDetails;
    private securityManager: SecurityManagerService;
    private userManager: UserManagerService;
    private sessionSubscription: Subscription;
    private isProfileSwitchLookupShown: boolean;
    private isUserEditorShown: boolean;
    private noThumbnail:any; 

    @ViewChild('profileSwitchModal') 
    private profileSwitchModal: ModalDirective;

    @ViewChild('userEditorModal') 
    private userEditorModal: ModalDirective;

    constructor(router: Router, securityManager: SecurityManagerService, userManager: UserManagerService)
    {
        this.router = router;
        this.securityManager = securityManager;
        this.userManager = userManager;
        this.sessionDetails = securityManager.getSession().sessionDetails;
        this.loggedIn = securityManager.isLoggedIn();
        this.userThumbnailUrl = userManager.getUserProfileThumbnailUrl(this.sessionDetails.userId);
        this.noThumbnail = require("../../../assets/images/no-profile-thumbnail.jpg");
        this.sessionSubscription = securityManager.sessionUpdated$.subscribe
        (
            session => 
            {
                console.log("Session update observed from user component.");
                this.loggedIn = session.loggedIn;
                this.sessionDetails = session.sessionDetails;
                console.log(session.sessionDetails);
            }
        );
    }

    ngOnDestroy()
    {
        this.sessionSubscription.unsubscribe();
    }

    setNoThumbnail()
    {
        this.userThumbnailUrl = this.noThumbnail;
    }

    public showUserEditor():void 
    {
        this.isUserEditorShown = true;
    }

    public hideUserEditor():void 
    {
        this.userEditorModal.hide();
    }

    public onUserEditorHidden():void 
    {
        this.isUserEditorShown = false;
    }

    onUserProfileClick(event: any)
    {
        console.log("Profile clicked!!");
    }

    public showProfileSwitchLookup():void 
    {
        this.isProfileSwitchLookupShown = true;
    }

    public hideProfileSwitchLookup():void 
    {
        this.profileSwitchModal.hide();
    }

    public onProfileSwitchLookupHidden():void 
    {
        this.isProfileSwitchLookupShown = false;
    }

    public onProfileSwitchSelection(profile: UserProfileDetails)
    {
        this.hideProfileSwitchLookup();
        this.switchUserProfile(profile.id);
    }

    public onProfilePictureUpdated()
    {
        this.userThumbnailUrl = this.userManager.getUserProfileThumbnailUrl(this.sessionDetails.userId);
    }

    private switchUserProfile(profileId: string)
    {
        // No need to display a loading indicator, as this will be done by the main app frame.
        console.log("Switching to profile: " + profileId);
        this.securityManager.switchUserProfile(profileId)
            .subscribe
            (
                data => 
                {
                    if (<boolean>data)
                    {
                        this.router.navigate(['/main']);
                    }
                    else
                    {
                        console.log("Attempt to switch to user profile denied: " + profileId);
                    }
                },
                error => 
                {
                    console.log("Error while swithcing to user profile: " + profileId +" : " + error);
                }
            );
    }
}
