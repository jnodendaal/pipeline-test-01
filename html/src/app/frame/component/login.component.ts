import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SecurityManagerService } from '../../core/service/security-manager.service';
import { LoginResponse } from '../../core/service/security-manager.service';
import { AppConfigService } from '../../app-config.service';

@Component
({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit 
{
    private securityManager: SecurityManagerService;
    private loginHeader: string;
    private loading = false;
    private returnUrl: string;
    private message: LoginMessage;
    private username: string;
    private password: string;
    private newPassword: string;
    private newPasswordConfirmation: string;
    private newPasswordEntry: boolean;
    private links:any;

    constructor(private route: ActivatedRoute, private router: Router, securityManager: SecurityManagerService, private configService: AppConfigService) 
    { 
        this.securityManager = securityManager;
        this.newPasswordEntry = false;
        this.loginHeader = configService.get("loginHeader");
    }

    ngOnInit() 
    {
        // Set the login links from system configuration.
        this.links = this.configService.get("loginLinks");

        // Reset login status.
        if (this.securityManager.isLoggedIn())
        {
            this.loading = true;
            this.securityManager.logout()
                .subscribe
                (
                    data => 
                    {
                        console.log("Logout Succeeded.");
                        this.loading = false;
                    },
                    error => 
                    {
                        this.message = new LoginMessage("error", error.message);
                        this.loading = false;
                    }
                );
        }

        // Get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() 
    {
        this.loading = true;
        this.securityManager.login(this.username, this.password)
            .subscribe
            (
                data => 
                {
                    
                    console.log("Return URL: " + this.returnUrl);
                    switch(data.responseCode)
                    {
                        case 'SUCCESS':
                        case 'SESSION_ALREADY_LOGGED_IN':
                            this.message = new LoginMessage("success", "Successful Login");
                            this.navigateToMain();
                            break;
                        case 'SUCCESS_NEW_PASSWORD':
                            this.message = new LoginMessage("info", "Your password has been reset.  Please enter a new one.");
                            this.loading = false;
                            this.newPasswordEntry = true;
                            break;
                        case 'PASSWORD_EXPIRED':
                            this.message = new LoginMessage("info", "Your password has expired.  Please enter a new one.");
                            this.loading = false;
                            this.newPasswordEntry = true;
                            break;
                        case 'PASSWORD_EXPIRY_WARNING':
                            this.message = new LoginMessage("info", "Your password will expire soon.  Please enter a new one.");
                            this.loading = false;
                            this.newPasswordEntry = true;
                            break;
                        default:
                            this.message = new LoginMessage("error", "Login Unsuccessful.  Please verify your username and password.");
                            this.loading = false;
                            break;
                    }
                },
                error => 
                {
                    this.message = new LoginMessage("error", error.message);
                    this.loading = false;
                }
            );
    }

    changePassword()
    {
        if ((this.newPassword == null) || (this.newPassword.trim().length == 0))
        {
            console.log("1");
            this.message = new LoginMessage("error", "Please enter a new password.");
        }
        else if ((this.newPasswordConfirmation == null) || (this.newPasswordConfirmation.trim().length == 0))
        {
            console.log("2");
            this.message = new LoginMessage("error", "Please enter your new password in the confirmation box.");
        }
        else if (this.newPassword != this.newPasswordConfirmation)
        {
            console.log("3");
            this.message = new LoginMessage("error", "The new password does not match the confirmation.  Please re-enter your new password.");
        }
        else
        {
            this.loading = true;
            this.securityManager.changePassword(this.newPassword)
                .subscribe
                (
                    data => 
                    {
                        if (data.success)
                        {
                            this.message = new LoginMessage("success", "Password Updated Successfully");
                            this.navigateToMain();
                        }
                        else
                        {
                            this.message = new LoginMessage("error", data.message);
                            this.loading = false;
                        }
                    },
                    error => 
                    {
                        this.message = new LoginMessage("error", error.message);
                        this.loading = false;
                    }
                );
        }
    }

    navigateToMain()
    {
        if (this.returnUrl = '/')
        {
            // If we do not have a valid return url, just do the default navigation to the main path.
            this.router.navigate(['main/home']);
        }
        else
        {
            // We do have a valid return url so navigate to it.
            this.router.navigate([this.returnUrl]);
        }
    }
}

export class LoginMessage
{
    type: string;
    text: string;

    constructor(type: string, text: string)
    {
        this.type = type;
        this.text = text;
    }
}
