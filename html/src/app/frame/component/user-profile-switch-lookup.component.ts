import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SecurityManagerService } from '../../core/service/security-manager.service';
import { SessionDetails, UserProfileDetails } from '../../core/model/user.model';

@Component
({
    selector: 'user-profile-switch-lookup',
    templateUrl: 'user-profile-switch-lookup.component.html',
    styleUrls: ['user-profile-switch-lookup.component.css']
})
export class UserProfileSwitchLookupComponent implements OnInit
{   
    private securityManager: SecurityManagerService;
    private loading: boolean;
    private profiles: UserProfileDetails[];

    constructor(securityManager: SecurityManagerService)
    {
        this.securityManager = securityManager;
    }

    ngOnInit()
    {
        this.reloadProfiles();
    }

    reloadProfiles()
    {
        // Load the details of profiles accessible to the current session user.
        this.loading = true;
        this.securityManager.getAccessibleUserProfileDetails()
            .subscribe
            (
                data => 
                {
                    this.loading = false;
                    console.log(data);
                    setTimeout(this.profiles = data);
                },
                error => 
                {
                    console.log("Error while retrieving accessible user profile details: " + error);
                    this.loading = false;
                }
            );
    }

    @Output() profileSelected: EventEmitter<UserProfileDetails> = new EventEmitter<UserProfileDetails>();
    onProfileSelection(selectedProfile: UserProfileDetails)
    {
        console.log(selectedProfile);
        this.profileSelected.emit(selectedProfile);
    }
}