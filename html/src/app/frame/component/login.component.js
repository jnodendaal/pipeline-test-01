"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var session_service_1 = require("../../services/session.service");
var alert_service_1 = require("./alert.service");
var LoginComponent = (function () {
    function LoginComponent(route, router, sessionService, alertService) {
        this.route = route;
        this.router = router;
        this.alertService = alertService;
        this.model = {};
        this.loading = false;
        this.sessionService = sessionService;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Reset login status.
        if (this.sessionService.isLoggedIn()) {
            this.loading = true;
            this.sessionService.logout()
                .subscribe(function (data) {
                console.log("Logout Succeeded.");
                _this.loading = false;
            }, function (error) {
                _this.alertService.error(error);
                _this.loading = false;
            });
        }
        // Get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.sessionService.login(this.model.username, this.model.password)
            .subscribe(function (data) {
            console.log("Return URL: " + _this.returnUrl);
            if (_this.returnUrl = '/') {
                // If we do not have a valid return url, just do the default navigation to the main path.
                _this.router.navigate(['main']);
            }
            else {
                // We do have a valid return url so navigate to it.
                _this.router.navigate([_this.returnUrl]);
            }
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'login',
        templateUrl: 'login.component.html',
        styleUrls: ['login.component.css']
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute, router_1.Router, session_service_1.SessionService, alert_service_1.AlertService])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map