import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SecurityManagerService } from '../../core/service/security-manager.service';
import { FileManagerService } from '../../core/service/file-manager.service';
import { UserManagerService } from '../../core/service/user-manager.service';
import { SessionDetails, UserDetails } from '../../core/model/user.model';
import { ApplicationSettings } from '../../app.config';
import { Minute } from '../../core/model/time.model';
import { IdUtility } from '../../core/utility/id.utility';

@Component
({
    selector: 'user-editor',
    templateUrl: 'user-editor.component.html',
    styleUrls: ['user-editor.component.css']
})
export class UserEditorComponent  implements OnInit
{   
    private securityManager: SecurityManagerService;
    private fileManager: FileManagerService;
    private userManager: UserManagerService;
    private loading: boolean;
    private saving: boolean;
    private userDetails: UserDetails;
    private userPictureUrl: string
    private userDataFileContextId: string;
    private userProfilePictureFilename: string;
    private noProfilePicture: any; 

    constructor(securityManager: SecurityManagerService, fileManager: FileManagerService, userManager: UserManagerService)
    {
        this.securityManager = securityManager;
        this.fileManager = fileManager;
        this.userManager = userManager;
        this.saving = false;
        this.noProfilePicture = require("../../../assets/images/no-profile-picture.jpg");
    }

    ngOnInit()
    {
        this.refresh();
    }

    refresh()
    {
        // Load the details of the current session user.
        this.loading = true;
        this.securityManager.getUserDetails()
            .subscribe
            (
                data => 
                {
                    this.loading = false;
                    console.log(data);
                    setTimeout(this.setUserDetails(data));
                },
                error => 
                {
                    console.log("Error while retrieving user details: " + error);
                    this.loading = false;
                }
            );
    }

    setNoProfilePicture()
    {
        this.userPictureUrl = this.noProfilePicture;
    }

    setUserDetails(userDetails: UserDetails)
    {
        this.userDetails = userDetails;
        this.userPictureUrl = this.userManager.getUserProfilePictureUrl(this.userDetails.id);
    }

    public onFileSelectionChange(event: any)
    {
        var reader = new FileReader();
        let fileList: FileList;
        let files: File[];
        let self: any;

        // Start the saving indicator.
        this.saving = true;

        // Store a reference to this object to use in callback.
        self = this;

        // Create a new file reader to be used for conversion of binary file data to base64.
        reader = new FileReader();
        
        // Create an array of files to be uploaded.
        files = new Array();
        fileList = event.target.files;
        if (fileList.length > 0)
        {
            let reader: FileReader;
            let file: File;

            // Get the file to read, create a reader for it and set the completion handler.
            file = fileList.item(0);
            reader = new FileReader();
            reader.onload = function(readerEvt: any) 
            {
                var binaryString: string;
                var base64String: string;

                binaryString = readerEvt.target.result;
                base64String = btoa(binaryString);
                self.uploadFile(file, base64String);
            };

            // Set the error handler.
            reader.onerror = function(readerEvt: any)
            {
                self.saving = false;
            }

            // Set the abortion handler.
            reader.onabort = function(readerEvt: any)
            {
                self.saving = false;
            }

            // Read the file.
            reader.readAsBinaryString(file);
        }
    }

    private uploadFile(file: File, fileData: string)
    {
        let contextIid: string;

        // Generate a unique context iid for the image upload.
        contextIid = IdUtility.newId();

        // Open the file context.
        this.fileManager.openFileContext(contextIid, null, new Minute(1))
        .subscribe
        (
            data => 
            {
                // Upload the new picture.
                this.fileManager.uploadFileData(contextIid, file.name, fileData)
                .subscribe
                (
                    data => 
                    {
                        // Change the profile picture to the one uploaded.
                        this.userManager.changeProfilePicture(contextIid, file.name)
                        .subscribe
                        (
                            data =>
                            {
                                // Close the file context.
                                this.fileManager.closeFileContext(contextIid)
                                .subscribe
                                (
                                    data => 
                                    {
                                        // Image successfully uploaded.
                                        this.onUserProfileUpdated();
                                        this.saving = false;
                                    },
                                    error => 
                                    {
                                        console.log("Exception while closingfile context: " + contextIid);
                                        console.log(error);
                                        this.saving = false;
                                    }
                                );
                            },
                            error =>
                            {
                                console.log("Exception while changing profile picture to: " + file.name);
                                console.log(error);
                                this.saving = false;
                                this.fileManager.closeFileContext(contextIid).subscribe();
                            }
                        );
                    },
                    error => 
                    {
                        console.log("Exception while uploading profile picture.");
                        console.log(error);
                        this.saving = false;
                        this.fileManager.closeFileContext(contextIid).subscribe();
                    }
                );
            },
            error => 
            {
                console.log("Exception while opening file context during user profile picture upload.");
                console.log(error);
                this.saving = false;
                this.fileManager.closeFileContext(contextIid).subscribe();
            }
        );
    }

    @Output() profilePictureUpdated: EventEmitter<void> = new EventEmitter();
    onUserProfileUpdated()
    {
        this.userPictureUrl = this.userManager.getUserProfilePictureUrl(this.userDetails.id);
        this.profilePictureUpdated.emit();
    }

    @Output() userUpdated: EventEmitter<UserDetails> = new EventEmitter<UserDetails>();
    updateUserDetails()
    {
        // Load the details of the current session user.
        this.saving = true;
        this.securityManager.updateUserDetails(this.userDetails)
            .subscribe
            (
                data => 
                {
                    this.saving = false;
                    console.log(data);
                    //setTimeout(this.setUserDetails(data));
                    this.userUpdated.emit(this.userDetails);
                },
                error => 
                {
                    console.log("Error while updating user details: " + error);
                    this.saving = false;
                }
            );
    }
}
