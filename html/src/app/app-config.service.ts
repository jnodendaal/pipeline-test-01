import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationSettings } from './app.config';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Declare the type of the webpack public path variable to avoid TS compilation error.
declare var  __webpack_public_path__:string; 

@Injectable()
export class AppConfigService 
{
  private config: any;

  constructor(private http: HttpClient) 
  {
    console.log('Constructor:AppConfigService')
  }

  public get(key: string): any
  {
    let value = this.config[key];
    return value;
  }

  load() 
  {
    let configFilename: string;

    configFilename = (process.env.ENV === 'production') ? 'config.prod.json' : 'config.dev.json';
    return new Promise((resolve) => 
    {
      console.log("Reading configuration from: " + configFilename);
      this.http.get(configFilename)
        .subscribe(config => 
        {
          // Set the local config to the values read from the file.
          this.config = config;
          console.log(this.config);

          // If a context url is specified, set the global webpack public path to it (to enable proper chunk loading).
          if (this.config.contextUrl)
          {
            console.log("Setting contextUrl: " + this.config.contextUrl);
            __webpack_public_path__ = this.config.contextUrl;
          }
          else
          {
            console.log("Using default contextUrl: ./");
          }

          // Set the api url.
          if (this.config.apiUrl)
          {
            console.log("Setting apiUrl: " + this.config.apiUrl);
            ApplicationSettings.API_URL = this.config.apiUrl;
            ApplicationSettings.API_OPERATION_URL = this.config.apiUrl + '/api/operation';
            ApplicationSettings.API_ATTACHMENT_URL = this.config.apiUrl + '/api/attachment';
            ApplicationSettings.API_REPORT_URL = this.config.apiUrl + '/api/report';
            ApplicationSettings.API_IMAGE_URL = this.config.apiUrl + '/api/image';
          }
          else
          {
            let defaultApiUrl;

            defaultApiUrl = this.getDefaultApiUrl();
            console.log("Using default apiUrl: " + defaultApiUrl);
            ApplicationSettings.API_URL = defaultApiUrl;
            ApplicationSettings.API_OPERATION_URL = defaultApiUrl + '/api/operation';
            ApplicationSettings.API_ATTACHMENT_URL = defaultApiUrl + '/api/attachment';
            ApplicationSettings.API_REPORT_URL = defaultApiUrl + '/api/report';
            ApplicationSettings.API_IMAGE_URL = defaultApiUrl + '/api/image';
          }

          // Set the urls of required resources.
          ApplicationSettings.NO_IMAGE_URL = require("../assets/images/no-image.jpg");
          ApplicationSettings.NO_PROFILE_THUMBNAIL_URL = require("../assets/images/no-profile-thumbnail.jpg");

          // Resolve the promise.
          resolve();
        });
    });
  }

  public getDefaultApiUrl(): string
  {
    let protocol: string;
    let host: string;
    let path: string;

    protocol = location.protocol;
    host = location.host;
    path = location.pathname;
    return protocol + "//" + host + "/" + path.split('/')[1];
  }

  public getDefaultContextUrl(): string
  {
    let path: string;

    path = location.pathname;
    return "/" + path.split('/')[1];
  }
}