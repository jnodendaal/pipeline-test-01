import { NgModule }                               from '@angular/core';
import { CommonModule }                           from '@angular/common';
import { FormsModule }                            from '@angular/forms';
import { SharedModule }                           from '../core/shared.module';
import { DatepickerModule  }                      from 'ngx-bootstrap';

import { DataFileSearchComponent }                from './component/data-file-search.component';
import { DataFileSearchService }                  from './service/data-file-search.service';
import { FilterCriteriaComponent }                from './component/filter-criteria.component';
import { CollapseModule }                         from 'ngx-bootstrap';
import { PropertyCriterionComponent }             from './component/property-criterion.component';
import { CodeCriterionComponent }                 from './component/code-criterion.component';
import { OrganizationCriterionComponent }         from './component/organization-criterion.component';
import { DefaultTypeComponent }                   from './component/value-types/default-type.component';
import { ConceptTypeComponent }                   from './component/value-types/concept-type.component';
import { BooleanTypeComponent }                   from './component/value-types/boolean-type.component';
import { InstanceCriterionComponent }             from './component/instance-criterion.component';
import { DocumentReferenceTypeComponent }         from './component/value-types/document-reference-type.component';
import { DateTimeTypeComponent }                  from './component/value-types/date-time-type.component';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    SharedModule,
    CollapseModule.forRoot(),
    DatepickerModule
  ],
  declarations: 
  [ 
    DataFileSearchComponent,
    FilterCriteriaComponent,
    PropertyCriterionComponent,
    DefaultTypeComponent,
    CodeCriterionComponent,
    OrganizationCriterionComponent,
    ConceptTypeComponent,
    BooleanTypeComponent,
    InstanceCriterionComponent,
    DocumentReferenceTypeComponent,
    DateTimeTypeComponent
  ],

  exports:      [ DataFileSearchComponent ],
  providers:    [ DataFileSearchService ]
})
export class DataFileSearchModule 
{
}