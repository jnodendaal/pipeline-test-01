import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ValueFilterCriterion, ValueType } from '../../../core/model/data-record-filter.model';
import * as moment from 'moment';
import { Moment } from 'moment';
import { SimpleDateFormat } from '../../../core/utility/date.utility';

@Component
({
    selector: 'date-time-type',
    templateUrl: 'date-time-type.component.html',
    styleUrls: ['date-time-type.component.css']
})
export class DateTimeTypeComponent implements ValueType
{   
    private valueCriterion: ValueFilterCriterion;
    private propertyId: string;
    private showDatePicker: boolean;
    private dateTime: Date;
    private dateTimeString: string;
    private formatPattern: string;
    
    constructor()
    { 
        this.showDatePicker = false;
        this.formatPattern = "YYYY-MM-DD hh:mm:ss";
    }

    @Input()
    set inputValueCriterion(valueCriterion: ValueFilterCriterion) 
    {
        this.valueCriterion = valueCriterion;
        if(this.valueCriterion.value != null)
        {
            this.dateTime = new Date(parseInt(this.valueCriterion.value));
            this.updateDateTimeString();
        } 
    }
    @Input()
    set inputPropertyId(propertyId: string) 
    {
        this.propertyId = propertyId;
    }

    public toggleDatePicker(): void
    {
        this.showDatePicker = !this.showDatePicker;
    }

    public onPickerModelChange(event: any)
    {
        if (this.dateTime)
        {
            this.valueCriterion.value = this.dateTime.getTime().toString();    
            this.updateDateTimeString();
        }
        else
        {
            this.valueCriterion.value = null;
            this.updateDateTimeString();
        }
    }

    public onBlur(): void
    {
        if(this.dateTimeString)
        {
            let enteredDateTime: Moment;

            enteredDateTime = moment(this.dateTimeString, this.formatPattern);
            if (enteredDateTime.isValid())
            {
                this.dateTime = enteredDateTime.toDate();
                this.valueCriterion.value = this.dateTime.getTime().toString();    
                this.updateDateTimeString();
            }
            else
            {
                this.valueCriterion.value = this.dateTimeString;    
                this.updateDateTimeString();
            }
        }
        else
        {
            this.clearSelectedValue();
        }
    }

    public onDateTimeSelected(): void
    {
        this.toggleDatePicker();
    }

    private updateDateTimeString()
    {
        this.dateTimeString = this.dateTime != null ? moment(this.dateTime).format(this.formatPattern) : null;
    }

    public clearSelectedValue()
    {
        this.valueCriterion.value = null;
        this.dateTimeString = null;
        this.dateTime = null;
    } 

    public getValueCriterion(): ValueFilterCriterion
    {
        return this.valueCriterion;
    }

    public addTerminology(terminology : any): any
    { 
        return terminology;
    }
}
