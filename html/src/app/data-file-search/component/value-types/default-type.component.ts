import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ValueFilterCriterion, ValueType } from '../../../core/model/data-record-filter.model';

@Component
({
    selector: 'default-type',
    templateUrl: 'default-type.component.html',
    styleUrls: ['default-type.component.css']
})
export class DefaultTypeComponent implements ValueType  
{   
    private valueCriterion: ValueFilterCriterion;
    
    constructor()
    { 
    }

    @Input()
    set inputValueCriterion(valueCriterion: ValueFilterCriterion) 
    {
        this.valueCriterion = valueCriterion;
    }

    public clearSelectedValue()
    {
        this.valueCriterion.value = null;
    } 

    public getValueCriterion(): ValueFilterCriterion
    {
        return this.valueCriterion;
    }

    public addTerminology(terminology : any): any
    { 
        return terminology;
    }
}
