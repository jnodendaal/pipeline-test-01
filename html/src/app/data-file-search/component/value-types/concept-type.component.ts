import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DrFilterCriteria, DrInstanceFilterCriteria, ValueFilterCriterion, ValueType } from '../../../core/model/data-record-filter.model';
import { DataFileSearchService } from '../../service/data-file-search.service';
import { ModalDirective } from 'ngx-bootstrap';
import { IdUtility } from '../../../core/utility/id.utility';

@Component
({
    selector: 'concept-type',
    templateUrl: 'concept-type.component.html',
    styleUrls: ['concept-type.component.css']
})
export class ConceptTypeComponent implements ValueType
{   
    private valueCriterion: ValueFilterCriterion;
    private isValueLookupShown: boolean;
    private searchString: string;
    private searchPattern: string;
    private loading: boolean;
    private dataFileSearchService: DataFileSearchService;
    private valueLookupOptions: Array<Map<string,string>>;
    private displayValue: string;
    private propertyId: string;
    private filterCriteria: any;
    private terminology: any;
    
    constructor(dataFileSearchService: DataFileSearchService)
    { 
        this.dataFileSearchService = dataFileSearchService;
        this.isValueLookupShown = false;
    }

    @ViewChild('autoShownModal') 
    private autoShownModal: ModalDirective;

    @Input()
    set inputValueCriterion(valueCriterion: ValueFilterCriterion) 
    {   
        this.valueCriterion = valueCriterion;
    }
    @Input()
    set inputPropertyId(propertyId: string) 
    {
        this.propertyId = propertyId;
    }
    @Input()
    set inputFilterCriteria(filterCriteria: any) 
    {
        this.filterCriteria = filterCriteria;
    }
    @Input()
    set inputTerminology(terminology: any) 
    {
        this.terminology = terminology;

        let value = this.valueCriterion.value;
        if(value != null && IdUtility.isId(value))
        {
            this.displayValue = this.terminology[value];
        }
        else
        {
            this.displayValue = value;
        }
    }

    public onValueLookupHidden():void 
    {
        this.isValueLookupShown = false;
    }

    public hideValueLookup():void 
    {
        this.autoShownModal.hide();
    }

    public showValueLookup():void 
    {
        this.isValueLookupShown = true;
        this.reloadValues();
    }

    public clearSelectedValue()
    {
        this.valueCriterion.value = null;
        this.displayValue = null;
    }

    reloadValues()
    {
        let drId: string;
        let drInstanceId: string;

        // Create a seach pattern from the terms specified.
        if (this.searchString)
        {
            let pattern: string;

            // Split the search terms on whitespace and add each term to the filter.
            for (let searchTerm of this.searchString.split(" "))
            {
                pattern += "|";
                pattern += searchTerm;
            }

            // Set the search pattern used for text highlighting.
            this.searchPattern = pattern;
        }
        else
        {
            // Reset the search pattern.
            this.searchPattern = "";
        }

        // Get the correct id depending on the type of filter criteria.
        if (this.filterCriteria instanceof DrFilterCriteria)
        {
            let drCriteria: DrFilterCriteria;

            drCriteria = this.filterCriteria as DrFilterCriteria;
            drId = drCriteria.drId;
        }
        else if (this.filterCriteria instanceof DrInstanceFilterCriteria)
        {
            let drInstanceCriteria: DrInstanceFilterCriteria;

            drInstanceCriteria = this.filterCriteria as DrInstanceFilterCriteria;
            drInstanceId = drInstanceCriteria.drInstanceId;
        }

        // Load the values using the search terms.
        this.loading = true;
        this.dataFileSearchService.retrieveControlledConceptLookupOptions(drInstanceId, drId, this.propertyId, this.searchString)
            .subscribe
            (
                data => 
                {
                    this.loading = false;
                    console.log(data);
                    setTimeout(this.valueLookupOptions = data);
                },
                error => 
                {
                    console.log("Error while retrieving value lookup options: " + error);
                    this.loading = false;
                }
            );
    }

    private onValueSelection(selectedValue: Map<string,string>)
    {
        this.hideValueLookup();
        this.valueCriterion.value = selectedValue['conceptId'];
        this.displayValue = selectedValue['term'];
        this.searchString = null;
    }

    private onUserInput(input: any)
    {
        this.valueCriterion.value = input.target.value;
    }

    public getValueCriterion(): ValueFilterCriterion
    {
        return this.valueCriterion;
    }

    public addTerminology(terminology : any): any
    { 
        let value = this.valueCriterion.value;

        if(value != null && IdUtility.isId(value))
        {
            terminology[value] = this.displayValue;
        }
        return terminology;
    }
}
