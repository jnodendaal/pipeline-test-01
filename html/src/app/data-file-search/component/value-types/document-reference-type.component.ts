import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DataFileSearchService } from '../../service/data-file-search.service';
import { ConceptTypeComponent } from './concept-type.component';

@Component
({
    selector: 'document-reference-type',
    templateUrl: 'concept-type.component.html',
    styleUrls: ['document-reference-type.component.css']
})
export class DocumentReferenceTypeComponent  extends ConceptTypeComponent
{   
    constructor(dataFileSearchService: DataFileSearchService) 
    {
        super(dataFileSearchService);
    }
}
