import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DrFilterCriteria, DrInstanceFilterCriteria, ValueFilterCriterion, ValueType } from '../../../core/model/data-record-filter.model';

@Component
({
    selector: 'boolean-type',
    templateUrl: 'boolean-type.component.html',
    styleUrls: ['boolean-type.component.css']
})
export class BooleanTypeComponent implements ValueType
{   
    private valueCriterion: ValueFilterCriterion;
    private propertyId: string;
    private filterCriteria: any;
    private filterCriteriaId: string;
    
    constructor()
    { 
    }

    @Input()
    set inputValueCriterion(valueCriterion: ValueFilterCriterion) 
    {
        this.valueCriterion = valueCriterion;
    }
    @Input()
    set inputPropertyId(propertyId: string) 
    {
        this.propertyId = propertyId;
    }
    @Input()
    set inputFilterCriteria(filterCriteria: any) 
    {
        this.filterCriteria = filterCriteria;
        this.setFilterCriteriaId();
    }

    onValueChange(event: any)
    {
        let selecedValue: string;

        // Determine the value that has been selected.
        switch (event.target.id)
        {
            case "radioTrue":
                selecedValue = "true";
                break;
            case "radioFalse":
                selecedValue = "false";
                break;
            case "radioUnspecified":
                selecedValue = null;
                break;
        }

        this.valueCriterion.value = selecedValue;
    }

    private setFilterCriteriaId()
    {
        // Get the correct id depending on the type of filter criteria.
        if (this.filterCriteria instanceof DrFilterCriteria)
        {
            let drCriteria: DrFilterCriteria;

            drCriteria = this.filterCriteria as DrFilterCriteria;
            this.filterCriteriaId = drCriteria.drId;
        }
        else if (this.filterCriteria instanceof DrInstanceFilterCriteria)
        {
            let drInstanceCriteria: DrInstanceFilterCriteria;

            drInstanceCriteria = this.filterCriteria as DrInstanceFilterCriteria;
            this.filterCriteriaId = drInstanceCriteria.drInstanceId;
        }
    }

    public clearSelectedValue()
    {
        this.valueCriterion.value = null;
    }

    public getValueCriterion(): ValueFilterCriterion
    {
        return this.valueCriterion;
    }

    public addTerminology(terminology : any)
    { 
    }
}
