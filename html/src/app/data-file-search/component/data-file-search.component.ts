import { Component, Input, Output, EventEmitter, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { DataFileSearchService } from '../service/data-file-search.service';
import { RecordFilterCriteria } from '../../core/model/data-record-filter.model';
import { FilterCriteriaComponent } from './filter-criteria.component';
import { TerminologySet } from '../../core/model/terminology.model';
import { ComponentManagerService } from '../../core/service/component-manager.service';

@Component
({
    selector: 'data-file-search',
    templateUrl: 'data-file-search.component.html',
    styleUrls: ['data-file-search.component.css']
})

export class DataFileSearchComponent
{   
    private componentManager: ComponentManagerService;
    private searchService: DataFileSearchService;
    private recordFilterCriteria: RecordFilterCriteria;
    private terminology: TerminologySet;
    
    constructor(searchService: DataFileSearchService, componentManager: ComponentManagerService)
    {
        console.log("DataFileSearchComponent");
        this.searchService = searchService;
        this.componentManager = componentManager;
    }

    @ViewChildren(FilterCriteriaComponent) 
    private filterCriteriaComponents: QueryList<FilterCriteriaComponent>;

    @Input()
    set inputRecordFilterCriteria(recordFilterCriteria: RecordFilterCriteria) 
    {
        this.recordFilterCriteria = recordFilterCriteria;
    }
    
    @Input()
    set inputTerminology(terminology: TerminologySet) 
    {
        this.terminology = terminology;
    }

    @Output() search: EventEmitter<RecordFilterCriteria> = new EventEmitter();
    onSearch()
    {
        this.componentManager.addTerminology(this.getTerminology());
        this.search.emit(this.getFilterCriteria());
    }

    private onClearValues()
    {
       for(let filterCriteriaComponent of this.filterCriteriaComponents.toArray())
       {
            filterCriteriaComponent.clearValues();
       }
    }
    
    private getFilterCriteria(): RecordFilterCriteria
    {
        let recordCriteria: RecordFilterCriteria;

        recordCriteria = this.recordFilterCriteria;

        recordCriteria.criteria = new Array();
        for(let filterCriteriaComponent of this.filterCriteriaComponents.toArray())
        {
            recordCriteria.criteria.push(filterCriteriaComponent.getFilterClause());
        }

        return recordCriteria;
    }

    public getTerminology(): TerminologySet
    {
        let terminology = this.terminology;
        
        for(let filterCriteriaComponent of this.filterCriteriaComponents.toArray())
        {
            filterCriteriaComponent.addTerminology(terminology);
        }

        return terminology;
    }
}
