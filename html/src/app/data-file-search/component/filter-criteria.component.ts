import { Component, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { DrFilterCriteria, DrInstanceFilterCriteria } from '../../core/model/data-record-filter.model';
import { InstanceCriterionComponent } from './instance-criterion.component';
import { CodeCriterionComponent } from './code-criterion.component';
import { OrganizationCriterionComponent } from './organization-criterion.component';
import { PropertyCriterionComponent } from './property-criterion.component';
import { DataFilterClause } from '../../core/model/data-filter.model';
import { TerminologySet } from '../../core/model/terminology.model';

@Component
({
    selector: 'filter-criteria',
    templateUrl: 'filter-criteria.component.html',
    styleUrls: ['filter-criteria.component.css']
})
export class FilterCriteriaComponent  
{   
    private filterCriteria: DataFilterClause;
    private terminology: TerminologySet;
    private isCollapsed: boolean;
    private isAllowInstanceSelection: boolean;
    
    constructor()
    {
        this.isCollapsed = false;
        this.isAllowInstanceSelection = false;
    }

    @Input()
    set inputFilterCriteria(filterCriteria: DataFilterClause) 
    {
        this.filterCriteria = filterCriteria;
        this.setIsAllowInstanceSelection();
    }

    @Input()
    set inputTerminology(terminology: TerminologySet) 
    {
        this.terminology = terminology;
    }

    @ViewChildren(InstanceCriterionComponent) 
    private instanceCriterionComponents: QueryList<InstanceCriterionComponent>;

    @ViewChildren(CodeCriterionComponent) 
    private codeCriterionComponents: QueryList<CodeCriterionComponent>;

    @ViewChildren(OrganizationCriterionComponent) 
    private organizationCriterionComponents: QueryList<OrganizationCriterionComponent>;

    @ViewChildren(PropertyCriterionComponent) 
    private propertyCriterionComponents: QueryList<PropertyCriterionComponent>;

    collapsed(event: any): void 
    {
    }
     
    expanded(event: any): void 
    {
    }

    private getTerm(conceptId: string): string
    {
        return this.terminology.getTerm(conceptId);
    }

    private getFilterCriteriaTerm(): string
    {
        if (this.filterCriteria instanceof DrFilterCriteria)
        {
            let drCriteria: DrFilterCriteria;

            drCriteria = this.filterCriteria as DrFilterCriteria;
            return this.getTerm(drCriteria.drId);
        }
        else if (this.filterCriteria instanceof DrInstanceFilterCriteria)
        {
            let drInstanceCriteria: DrInstanceFilterCriteria;

            drInstanceCriteria = this.filterCriteria as DrInstanceFilterCriteria;
            return this.getTerm(drInstanceCriteria.drInstanceId);
        }
    }

    private setIsAllowInstanceSelection()
    {
        if (this.filterCriteria instanceof DrFilterCriteria)
        {
            let drCriteria: DrFilterCriteria;

            drCriteria = this.filterCriteria as DrFilterCriteria;

            if(drCriteria.isAllowInstanceSelection)
            {
                this.isAllowInstanceSelection = true;
            }
        }
    }

    public clearValues()
    {
        for(let instanceCriterionComponent of this.instanceCriterionComponents.toArray())
        {
            instanceCriterionComponent.clearSelectedValue();
        }

        for(let codeCriterionComponent of this.codeCriterionComponents.toArray())
        {
            codeCriterionComponent.clearSelectedValue();
        }

        for(let organizationCriterionComponent of this.organizationCriterionComponents.toArray())
        {
            organizationCriterionComponent.clearSelectedValue();
        }

        for(let propertyCriterionComponent of this.propertyCriterionComponents.toArray())
        {
            propertyCriterionComponent.clearValues();
        }
    }

    public getFilterClause(): DataFilterClause
    {
        let filterClause: any;

        filterClause = this.filterCriteria;

        for(let instanceCriterionComponent of this.instanceCriterionComponents.toArray())
        {
            let drFilterCriteria: DrFilterCriteria;

            drFilterCriteria = instanceCriterionComponent.getDrFilterCriteria()
            filterClause.instanceOperator = drFilterCriteria.instanceOperator;
            filterClause.instanceValue = drFilterCriteria.instanceValue;
        }

        filterClause.codeCriteria = new Array();
        for(let codeCriterionComponent of this.codeCriterionComponents.toArray())
        {
            filterClause.codeCriteria.push(codeCriterionComponent.getCodeCriterion());
        }

        filterClause.organizationCriteria = new Array();
        for(let organizationCriterionComponent of this.organizationCriterionComponents.toArray())
        {
            filterClause.organizationCriteria.push(organizationCriterionComponent.getOrganizationCriterion());
        }

        filterClause.propertyCriteria = new Array();
        for(let propertyCriterionComponent of this.propertyCriterionComponents.toArray())
        {
            filterClause.propertyCriteria.push(propertyCriterionComponent.getPropertyCriterion());
        }
        
        return filterClause;
    }

    public addTerminology(terminology : TerminologySet)
    {
        for(let instanceCriterionComponent of this.instanceCriterionComponents.toArray())
        {
            instanceCriterionComponent.addTerminology(terminology);
        }

        for(let organizationCriterionComponent of this.organizationCriterionComponents.toArray())
        {
            organizationCriterionComponent.addTerminology(terminology);
        }

        for(let propertyCriterionComponent of this.propertyCriterionComponents.toArray())
        {
            propertyCriterionComponent.addTerminology(terminology);
        }
    }
}
