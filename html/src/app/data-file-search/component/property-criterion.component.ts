import { Component, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { PropertyFilterCriterion, ValueFilterCriterion, ValueType } from '../../core/model/data-record-filter.model';
import { TerminologySet } from '../../core/model/terminology.model';
import * as DataFilterModel from '../../core/model/data-filter.model';

@Component
({
    selector: 'property-criterion',
    templateUrl: 'property-criterion.component.html',
    styleUrls: ['property-criterion.component.css']
})
export class PropertyCriterionComponent  
{   
    private propertyCriterion: PropertyFilterCriterion;
    private valueCriteria: ValueFilterCriterion[];
    private terminology: TerminologySet;
    private filterCriteria: any;
    private selectedOperator: string;
    private upperBoundValueCriterion: ValueFilterCriterion;

    // All operators supported by this component.
    private operators =
    [
        DataFilterModel.LIKE,
        DataFilterModel.NOT_LIKE,
        DataFilterModel.EQUAL,
        DataFilterModel.NOT_EQUAL,
        DataFilterModel.GREATER_THAN,
        DataFilterModel.GREATER_THAN_OR_EQUAL,
        DataFilterModel.LESS_THAN,
        DataFilterModel.LESS_THAN_OR_EQUAL,
        DataFilterModel.STARTS_WITH,
        DataFilterModel.ENDS_WITH,
        DataFilterModel.IS_NULL,
        DataFilterModel.IS_NOT_NULL
    ];
    
    constructor()
    {
    }

    @Input()
    set inputPropertyCriterion(propertyCriterion: PropertyFilterCriterion) 
    {
        this.propertyCriterion = propertyCriterion;
        this.valueCriteria = this.propertyCriterion.valueCriteria;
        this.setSelectedOperator();
        this.setOperators();
    }
    @Input()
    set inputTerminology(terminology: TerminologySet) 
    {
        this.terminology = terminology;
    }
    @Input()
    set inputFilterCriteria(filterCriteria: any) 
    {
        this.filterCriteria = filterCriteria;
    }
    
    @ViewChildren('valueComponent') 
    private valueCriterionComponents: QueryList<ValueType>;

    private setOperators()
    {
        if (this.valueCriteria != null && this.valueCriteria[0].type == 'DATE_TIME_TYPE')
        {
            this.operators.push(new DataFilterModel.DataFilterOperator('RANGE', 'Range'));
        }
    }

    private setSelectedOperator()
    {
        if(this.valueCriteria != null && this.valueCriteria.length > 1)
        {
            this.selectedOperator = "RANGE";
        }
        else
        {
            this.selectedOperator = this.valueCriteria[0].operator;
        }
    }

    private getPropertyTerm(): string
    {
        return this.terminology.getTerm(this.propertyCriterion.propertyId);
    }

    public clearValues()
    {
        for(let valueCriterionComponent of this.valueCriterionComponents.toArray())
        {
            valueCriterionComponent.clearSelectedValue();
        }
    } 

    public getPropertyCriterion(): PropertyFilterCriterion
    {
        let propertyCriterion: PropertyFilterCriterion;

        propertyCriterion = this.propertyCriterion;

        propertyCriterion.valueCriteria = new Array();
        for(let valueCriterionComponent of this.valueCriterionComponents.toArray())
        {
            let valueCriterion: ValueFilterCriterion;

            valueCriterion = valueCriterionComponent.getValueCriterion();
            valueCriterion.operator = this.selectedOperator;
            propertyCriterion.valueCriteria.push(valueCriterion);
        }

        // If there are more than 1 value criterions, linked on the property then a range operator was selected,
        // and the correct operator needs to be set.
        if (this.valueCriterionComponents.length > 1)
        {
            propertyCriterion.valueCriteria[0].operator = 'GREATER_THAN_OR_EQUAL';
            propertyCriterion.valueCriteria[1].operator = 'LESS_THAN_OR_EQUAL';
        }
        return propertyCriterion;
    }

    public addTerminology(terminology: TerminologySet)
    {
        for (let valueCriterionComponent of this.valueCriterionComponents.toArray())
        {
            valueCriterionComponent.addTerminology(terminology);
        }
    }

    private operatorChanged()
    {
        if(this.selectedOperator == 'RANGE')
        {
            if(this.upperBoundValueCriterion == null)
            {
                this.upperBoundValueCriterion = new ValueFilterCriterion();
                this.upperBoundValueCriterion.type = this.valueCriteria[0].type;
                this.upperBoundValueCriterion.operator = 'LESS_THAN_OR_EQUAL';
                this.valueCriteria.push(this.upperBoundValueCriterion);
            }
            else
            {
                this.valueCriteria.push(this.upperBoundValueCriterion);
            }
        }
        else if (this.valueCriteria != null && this.valueCriteria.length > 1)
        {
            this.valueCriteria.pop();
        }
    }
}
