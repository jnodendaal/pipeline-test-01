import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ValueFilterCriterion, OrganizationFilterCriterion } from '../../core/model/data-record-filter.model';
import { ModalDirective } from 'ngx-bootstrap';
import { DataFileSearchService } from '../service/data-file-search.service';
import { IdUtility } from '../../core/utility/id.utility';
import { TerminologySet } from '../../core/model/terminology.model';
import * as DataFilterModel from '../../core/model/data-filter.model';

@Component
({
    selector: 'organization-criterion',
    templateUrl: 'organization-criterion.component.html',
    styleUrls: ['organization-criterion.component.css']
})
export class OrganizationCriterionComponent  
{   
    private organizationCriterion: OrganizationFilterCriterion;
    private valueCriteria: ValueFilterCriterion;
    private isValueLookupShown: boolean;
    private searchString: string;
    private searchPattern: string;
    private loading: boolean;
    private dataFileSearchService: DataFileSearchService;
    private orgLookupOptions: Array<Map<string,string>>;
    private orgDisplayValue: string;
    private terminology: TerminologySet;

    // All operators supported by this component.
    private operators =
    [
        DataFilterModel.LIKE,
        DataFilterModel.NOT_LIKE,
        DataFilterModel.EQUAL,
        DataFilterModel.NOT_EQUAL,
        DataFilterModel.GREATER_THAN,
        DataFilterModel.GREATER_THAN_OR_EQUAL,
        DataFilterModel.LESS_THAN,
        DataFilterModel.LESS_THAN_OR_EQUAL,
        DataFilterModel.STARTS_WITH,
        DataFilterModel.ENDS_WITH,
        DataFilterModel.IS_NULL,
        DataFilterModel.IS_NOT_NULL
    ];
    
    constructor(dataFileSearchService: DataFileSearchService)
    {
        this.dataFileSearchService = dataFileSearchService;
        this.isValueLookupShown = false;
    }

    @ViewChild('autoShownModal') 
    private autoShownModal: ModalDirective;

    @Input()
    set inputOrganizationCriterion(organizationCriterion: OrganizationFilterCriterion) 
    {
        this.organizationCriterion = organizationCriterion;
        this.valueCriteria = this.organizationCriterion.valueCriteria;
    }

    @Input()
    set inputTerminology(terminology: TerminologySet) 
    {
        this.terminology = terminology;

        let value = this.valueCriteria.value;
        if(value != null && value instanceof Array)
        {
            value = value[0];
            if(IdUtility.isId(value))
            {
                this.orgDisplayValue = this.terminology.getTerm(value);
            }
            else
            {
                this.orgDisplayValue = value;
            }
        }
    }

    private getOperators(): DataFilterModel.DataFilterOperator[]
    {
        return this.operators;
    }

    public onValueLookupHidden():void 
    {
        this.isValueLookupShown = false;
    }

    public hideValueLookup():void 
    {
        this.autoShownModal.hide();
    }

    public showValueLookup():void 
    {
        this.isValueLookupShown = true;
        this.reloadValues();
    }

    public clearSelectedValue()
    {
        this.valueCriteria.value = null;
        this.orgDisplayValue = null;
    }

    reloadValues()
    {
        // Create a seach pattern from the terms specified.
        if (this.searchString)
        {
            let pattern: string;

            // Split the search terms on whitespace and add each term to the filter.
            for (let searchTerm of this.searchString.split(" "))
            {
                pattern += "|";
                pattern += searchTerm;
            }

            // Set the search pattern used for text highlighting.
            this.searchPattern = pattern;
        }
        else
        {
            // Reset the search pattern.
            this.searchPattern = "";
        }

        // Load the values using the search terms.
        this.loading = true;
        this.dataFileSearchService.retrieveOrgLookupOptions(this.organizationCriterion.orgTypeIds, this.searchString)
            .subscribe
            (
                data => 
                {
                    this.loading = false;
                    console.log(data);
                    setTimeout(this.orgLookupOptions = data);
                },
                error => 
                {
                    console.log("Error while retrieving organization lookup options: " + error);
                    this.loading = false;
                }
            );
    }

    private onOrgSelection(selectedOrg: Map<string,string>)
    {
        this.hideValueLookup();
        this.valueCriteria.value = new Array(selectedOrg['orgId']);
        this.orgDisplayValue = selectedOrg['term'];
        this.searchString = null;
    }

    private onUserInput(input: any)
    {
        this.valueCriteria.value = new Array(input.target.value);
    }

    public getOrganizationCriterion(): OrganizationFilterCriterion
    {
        let organizationCriterion: OrganizationFilterCriterion;

        organizationCriterion = this.organizationCriterion;
        organizationCriterion.valueCriteria = this.valueCriteria;
        return organizationCriterion;
    }

    public addTerminology(terminology: TerminologySet)
    {  
        let value = this.valueCriteria.value;

        if(value != null && value instanceof Array)
        {
            value = value[0];
            if(IdUtility.isId(value))
            {
                terminology.setTerm(value, this.orgDisplayValue);
            }
        }
        return terminology;
    }
}
