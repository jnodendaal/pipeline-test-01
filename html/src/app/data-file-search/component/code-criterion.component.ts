import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ValueFilterCriterion, CodeFilterCriterion } from '../../core/model/data-record-filter.model';
import { TerminologySet } from '../../core/model/terminology.model';
import * as DataFilterModel from '../../core/model/data-filter.model';

@Component
({
    selector: 'code-criterion',
    templateUrl: 'code-criterion.component.html',
    styleUrls: ['code-criterion.component.css']
})
export class CodeCriterionComponent  
{   
    private codeCriterion: CodeFilterCriterion;
    private valueCriteria: ValueFilterCriterion;
    private terminology: TerminologySet;

    // All operators supported by this component.
    private operators =
    [
        DataFilterModel.LIKE,
        DataFilterModel.NOT_LIKE,
        DataFilterModel.EQUAL,
        DataFilterModel.NOT_EQUAL,
        DataFilterModel.GREATER_THAN,
        DataFilterModel.GREATER_THAN_OR_EQUAL,
        DataFilterModel.LESS_THAN,
        DataFilterModel.LESS_THAN_OR_EQUAL,
        DataFilterModel.STARTS_WITH,
        DataFilterModel.ENDS_WITH,
        DataFilterModel.IS_NULL,
        DataFilterModel.IS_NOT_NULL
    ];
    
    constructor()
    {
    }

    @Input()
    set inputCodeCriterion(codeCriterion: CodeFilterCriterion) 
    {
        this.codeCriterion = codeCriterion;
        this.valueCriteria = this.codeCriterion.valueCriteria;
    }

    @Input()
    set inputTerminology(terminology: TerminologySet) 
    {
        this.terminology = terminology;
    }

    private getCodeTypeTerm(): string
    {
        return this.terminology.getTerm(this.codeCriterion.codeTypeId);
    }

    private getOperators(): DataFilterModel.DataFilterOperator[]
    {
        return this.operators;
    }

    public clearSelectedValue()
    {
        this.valueCriteria.value = null;
    } 

    public getCodeCriterion(): CodeFilterCriterion
    {
        let codeCriterion: CodeFilterCriterion;

        codeCriterion = this.codeCriterion;
        codeCriterion.valueCriteria = this.valueCriteria;
        return codeCriterion;
    }
}
