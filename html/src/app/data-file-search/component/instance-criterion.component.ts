import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DrFilterCriteria } from '../../core/model/data-record-filter.model';
import { ModalDirective } from 'ngx-bootstrap';
import { DataFileSearchService } from '../service/data-file-search.service';
import { IdUtility } from '../../core/utility/id.utility';
import { TerminologySet } from '../../core/model/terminology.model';
import * as DataFilterModel from '../../core/model/data-filter.model';

@Component
({
    selector: 'instance-criterion',
    templateUrl: 'instance-criterion.component.html',
    styleUrls: ['instance-criterion.component.css']
})
export class InstanceCriterionComponent  
{   
    private drFilterCriteria: DrFilterCriteria;
    private isValueLookupShown: boolean;
    private searchString: string;
    private searchPattern: string;
    private loading: boolean;
    private dataFileSearchService: DataFileSearchService;
    private instanceLookupOptions: Array<Map<string,string>>;
    private instanceDisplayValue: string;
    private terminology: TerminologySet;

    // All operators supported by this component.
    private operators =
    [
        DataFilterModel.LIKE,
        DataFilterModel.NOT_LIKE,
        DataFilterModel.EQUAL,
        DataFilterModel.NOT_EQUAL,
        DataFilterModel.GREATER_THAN,
        DataFilterModel.GREATER_THAN_OR_EQUAL,
        DataFilterModel.LESS_THAN,
        DataFilterModel.LESS_THAN_OR_EQUAL,
        DataFilterModel.STARTS_WITH,
        DataFilterModel.ENDS_WITH,
        DataFilterModel.IS_NULL,
        DataFilterModel.IS_NOT_NULL
    ];
    
    constructor(dataFileSearchService: DataFileSearchService)
    {
        this.dataFileSearchService = dataFileSearchService;
        this.isValueLookupShown = false;
    }

    @ViewChild('autoShownModal') 
    private autoShownModal: ModalDirective;

    @Input()
    set inputDrCriteria(drFilterCriteria: any) 
    {
        this.drFilterCriteria = drFilterCriteria as DrFilterCriteria;
    }

    @Input()
    set inputTerminology(terminology: TerminologySet) 
    {
        this.terminology = terminology;

        let value = this.drFilterCriteria.instanceValue;
        if(value != null && value instanceof Array)
        {
            value = value[0];
            if(IdUtility.isId(value))
            {
                this.instanceDisplayValue = this.terminology.getTerm(value);
            }
            else
            {
                this.instanceDisplayValue = value;
            }
        }
    }

    private getOperators(): DataFilterModel.DataFilterOperator[]
    {
        return this.operators;
    }

    public onValueLookupHidden():void 
    {
        this.isValueLookupShown = false;
    }

    public hideValueLookup():void 
    {
        this.autoShownModal.hide();
    }

    public showValueLookup():void 
    {
        this.isValueLookupShown = true;
        this.reloadValues();
    }

    public clearSelectedValue()
    {
        this.drFilterCriteria.instanceValue= null;
        this.instanceDisplayValue = null;
    }

    reloadValues()
    {
        // Create a seach pattern from the terms specified.
        if (this.searchString)
        {
            let pattern: string;

            // Split the search terms on whitespace and add each term to the filter.
            for (let searchTerm of this.searchString.split(" "))
            {
                pattern += "|";
                pattern += searchTerm;
            }

            // Set the search pattern used for text highlighting.
            this.searchPattern = pattern;
        }
        else
        {
            // Reset the search pattern.
            this.searchPattern = "";
        }

        // Load the values using the search terms.
        this.loading = true;
        this.dataFileSearchService.retrieveInstanceLookupOptions(this.drFilterCriteria.drId, this.searchString)
            .subscribe
            (
                data => 
                {
                    this.loading = false;
                    console.log(data);
                    setTimeout(this.instanceLookupOptions = data);
                },
                error => 
                {
                    console.log("Error while retrieving instance lookup options: " + error);
                    this.loading = false;
                }
            );
    }

    private onInstanceSelection(selectedInstance: Map<string,string>)
    {
        this.hideValueLookup();
        this.drFilterCriteria.instanceValue = new Array(selectedInstance['instanceId']);
        this.instanceDisplayValue = selectedInstance['term'];
        this.searchString = null;
    }

    private onUserInput(input: any)
    {
        this.drFilterCriteria.instanceValue = new Array(input.target.value);
    }

    public getDrFilterCriteria(): DrFilterCriteria
    {
        return this.drFilterCriteria;
    }

    public addTerminology(terminology: TerminologySet)
    {  
        let value = this.drFilterCriteria.instanceValue;

        if (value != null && value instanceof Array)
        {
            value = value[0];
            if(IdUtility.isId(value))
            {
                terminology.setTerm(value, this.instanceDisplayValue);
            }
        }
    }
}
