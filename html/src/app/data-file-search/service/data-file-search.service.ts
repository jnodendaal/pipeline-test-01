import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApplicationSettings } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
 
@Injectable()
export class DataFileSearchService 
{
    private accessParameters: HttpParams;

    constructor(private http: HttpClient) 
    {
        console.log("DataFileSearchService");
        this.accessParameters = new HttpParams();
    }

    public retrieveOrgLookupOptions(orgTypeIds: string[], searchString: string): Observable<Array<Map<string,string>>>
    {
        const input = 
        { 
            '@OP_RETRIEVE_ORG_LOOKUP_OPTIONS$P_ORG_TYPE_IDS': orgTypeIds, 
            '@OP_RETRIEVE_ORG_LOOKUP_OPTIONS$P_SEARCH_STRING': searchString
        };

        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_RETRIEVE_ORG_LOOKUP_OPTIONS', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                return response['@OP_RETRIEVE_ORG_LOOKUP_OPTIONS$P_ORG_ID_LIST'];
            });
    }

    public retrieveControlledConceptLookupOptions(drInstanceId: string, drId: string, propertyId: string, searchString: string): Observable<Array<Map<string,string>>>
    {
        const input = 
        { 
            '@OP_RETRIEVE_CONTROLLED_CONCEPT_LOOKUP_OPTIONS$P_SEARCH_STRING': searchString, 
            '@OP_RETRIEVE_CONTROLLED_CONCEPT_LOOKUP_OPTIONS$P_DR_INSTANCE_ID': drInstanceId,
            '@OP_RETRIEVE_CONTROLLED_CONCEPT_LOOKUP_OPTIONS$P_DR_ID': drId,
            '@OP_RETRIEVE_CONTROLLED_CONCEPT_LOOKUP_OPTIONS$P_PROPERTY_ID': propertyId
        };

        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_RETRIEVE_CONTROLLED_CONCEPT_LOOKUP_OPTIONS', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                return response['@OP_RETRIEVE_CONTROLLED_CONCEPT_LOOKUP_OPTIONS$P_CONCEPT_LIST'];
            });
    }

    public retrieveInstanceLookupOptions(drId: string, searchString: string): Observable<Array<Map<string,string>>>
    {
        const input = 
        { 
            '@OP_RETRIEVE_INSTANCE_LOOKUP_OPTIONS$P_DR_ID': drId, 
            '@OP_RETRIEVE_INSTANCE_LOOKUP_OPTIONS$P_SEARCH_STRING': searchString
        };

        return this.http.post(ApplicationSettings.API_URL + '/api/operation/@OP_RETRIEVE_INSTANCE_LOOKUP_OPTIONS', JSON.stringify(input), { withCredentials: true })
            .map((response: Response) => 
            {
                return response['@OP_RETRIEVE_INSTANCE_LOOKUP_OPTIONS$P_INSTANCE_LIST'];
            });
    }
}
