import { NgModule, APP_INITIALIZER }  from '@angular/core';
import { APP_BASE_HREF }              from '@angular/common';
import { BrowserModule }              from '@angular/platform-browser';
import { FormsModule }                from '@angular/forms';
import { RouterModule }               from '@angular/router';
import { HttpClientModule }           from '@angular/common/http';
import { BrowserAnimationsModule }    from '@angular/platform-browser/animations';
import { CoreModule }                 from './core/core.module';
import { SharedModule }               from './core/shared.module';
import { FrameModule }                from './frame/frame.module';
import { DatepickerModule  }          from 'ngx-bootstrap';
import { TypeaheadModule }            from 'ngx-bootstrap/typeahead';
import { TabsModule }                 from 'ngx-bootstrap/tabs';
import { BsDropdownModule }           from 'ngx-bootstrap/dropdown';
import { ModalModule }                from 'ngx-bootstrap/modal';
import { ProgressbarModule }          from 'ngx-bootstrap/progressbar';
import { ToastrModule }               from 'ngx-toastr';
import { DataFileEditorModule }       from './data-file-editor/data-file-editor.module';

import { AppComponent }               from './app.component';
import { AppRoutingModule }           from './app-routing.module';
import { AppConfigService }           from './app-config.service';
import { DataFileSearchModule }       from './data-file-search/data-file-search.module';

/*
  Included Modules:
  - npm install chart.js --save // For charting components.
  - npm install ng2-charts --save // For angular charting components.
  - npm install ngx-bootstrap --save // For angular bootstrap components.
  - npm install ngx-contextmenu --save // For context menu popup.
  - npm install angular-router-loader --save-dev // For lazy loading angular modules using webpack.
  - npm install ngx-toastr --save // For popup toast alerts: https://github.com/scttcper/ngx-toastr
  - npm install --save file-saver // Used to download files from url to local machine.
  - npm install @types/file-saver --save // Types for file-saver.
*/

@NgModule
({
  imports:      
  [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    FrameModule,
    ToastrModule.forRoot(),
    TypeaheadModule.forRoot(),
    DatepickerModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    DataFileEditorModule,
    DataFileSearchModule
  ],
  providers: 
  [
    //AlertService,
    AppConfigService,
    { 
      provide: APP_INITIALIZER, // This allows the AppConfigService to be loaded during bootstrapping before any other components or services are initialize.
      useFactory: (config: AppConfigService) => () => config.load(),
      deps: [AppConfigService],
      multi: true 
    },
    {
      provide: APP_BASE_HREF, // This provides the baseHref that is normally included in the index.html dynamically to all angular modules, specifically the router.
      useFactory: (config: AppConfigService) => config.getDefaultContextUrl(),
      deps: [AppConfigService]
    }
  ],
  declarations: 
  [ 
    AppComponent
  ],
  bootstrap:    
  [
    AppComponent
  ]
})
export class AppModule { }
