import { Component, ViewChild, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DataFileEditorComponent } from '../../data-file-editor/component/data-file-editor.component';
import { DataQualityService } from '../../core/service/data-quality.service';
import { ReportDefinition } from './mat-data-quality-report.component';
import { GroupedDataQualityReport, DataQualityMetricsSummary } from '../../core/model/data-record-quality.model';
import { BarChartComponent, BarChartModel, BarChartCategoryModel } from '../../core/component/charts/bar-chart.component';
import { DoughnutChartComponent, DoughnutChartDefinition } from '../../core/component/charts/doughnut-chart.component';
import { DataQualityTableComponent, RecordSelectedEvent } from './mat-data-quality-table.component';
import { NumberUtility } from '../../core/utility/number.utility';

@Component
({
    selector: 'mat-data-quality-report',
    templateUrl: 'mat-data-quality-report.component.html',
    styleUrls: [ 'mat-data-quality-report.component.css' ],
    providers: [ DataQualityService ]
})
export class MaterialDataQualityReportComponent  
{   
    private definition: DataQualityAspectReportDefinition;
    private dqService: DataQualityService;
    private dqReport: GroupedDataQualityReport;
    private dqSummary: DataQualityMetricsSummary;
    private selectedBreakdownCategory: BarChartCategoryModel;
    private selectedRecord: RecordDetails;
    private loading: boolean;

    @ViewChild('completenessChart')
    private completenessChart: DoughnutChartComponent;

    @ViewChild('validityChart')
    private validityChart: DoughnutChartComponent;

    @ViewChild('breakdownChart')
    private breakdownChart: BarChartComponent;

    @ViewChild('recordTable')
    private recordTable: DataQualityTableComponent;

    @ViewChild('dataFileEditor')
    private datFileEditor: DataFileEditorComponent;

    @ViewChild('autoShownModal') public autoShownModal:ModalDirective;
    public isModalShown:boolean = false;
    
    constructor(dqService: DataQualityService)
    {
        this.dqService = dqService;
        this.dqReport = null;
        this.loading = false;
    }

    ngOnInit() 
    {
        let definition: DataQualityAspectReportDefinition;
        
        definition = new DataQualityAspectReportDefinition("2", "Material Type Breakdown");
        definition.rootDrInstanceId = "CB9F1459E3D5425F97D4BB710206B380";
        definition.groupByTargetDrInstanceId = "669B002BDD63485E9C10F56947A42176";
        definition.groupByTargetPropertyId = "23A5CDF339114E3E9901FE4627CC5508";
        this.setDefinition(definition);
        this.refresh();
    }

    public setDefinition(definition: DataQualityAspectReportDefinition)
    {
        this.definition = definition;
    }

    public setDataQualityReport(dqReport: GroupedDataQualityReport)
    {
        let breakdownChartDefinition: BarChartModel;
        let completenessChartDefinition: DoughnutChartDefinition;
        let validityChartDefinition: DoughnutChartDefinition;

        // Update the local reference to the new report. 
        this.dqReport = dqReport;
        this.dqSummary = dqReport.getSummary();

        // Create the completeness chart definition.
        completenessChartDefinition = new DoughnutChartDefinition();
        completenessChartDefinition.addValue("Complete Properties", NumberUtility.round(this.dqSummary.characteristicCompleteness, -2));
        completenessChartDefinition.addValue("Incomplete Properties", NumberUtility.round(100 - this.dqSummary.characteristicCompleteness, -2));
        completenessChartDefinition.legendEnabled = false;
        this.completenessChart.setDefinition(completenessChartDefinition);

        // Create the validity chart definition.
        validityChartDefinition = new DoughnutChartDefinition();
        validityChartDefinition.addValue("Valid Properties", NumberUtility.round(this.dqSummary.characteristicValidity, -2));
        validityChartDefinition.addValue("Invalid Properties", NumberUtility.round(100 - this.dqSummary.characteristicValidity, -2));
        validityChartDefinition.legendEnabled = false;
        this.validityChart.setDefinition(validityChartDefinition);

        // Update the breakdown chart definition.
        breakdownChartDefinition = new BarChartModel();
        breakdownChartDefinition.addSeries("COMPLETENESS", "Characteristic Completeness");
        breakdownChartDefinition.addSeries("VALIDITY", "Characteristic Validity");
        for (let group of dqReport.groups)
        {
            breakdownChartDefinition.addCategory(group.id, group.term + " (" + group.metrics.recordCount + ")");
            breakdownChartDefinition.addData("COMPLETENESS", Math.round(group.metrics.getCharacteristicCompleteness()));
            breakdownChartDefinition.addData("VALIDITY", Math.round(group.metrics.getCharacteristicValidity()));
        }
        this.breakdownChart.setModel(breakdownChartDefinition);
    }

    public refresh()
    {
        this.loading = true;
        this.dqService.retrieveRootDataQualityReportGroupedByValueConcept(this.definition.rootDrInstanceId, this.definition.groupByTargetDrInstanceId, this.definition.groupByTargetPropertyId)
            .subscribe
            (
                data => 
                {
                    // Set the loading indicator to false.
                    // setTimeout is then required so that the UI update happens after the results of 'loading' set to false have taken place.
                    this.loading = false;
                    setTimeout(() => this.setDataQualityReport(data)); 
                },
                error => 
                {
                    console.log("Error while retrieving data quality report: " + error);
                    this.loading = false;
                }
            );
    }

    onBreakdownChartClicked(event: any)
    {
        console.log(event);
        this.selectedBreakdownCategory = event.category;
        this.recordTable.setFilterConceptId(this.selectedBreakdownCategory.id);
        this.recordTable.reloadData();
    }

    onRecordSelected(event: RecordSelectedEvent)
    {
        console.log("Record Selected: " + event.recordId);
        this.selectedRecord = new RecordDetails(event.recordId, event.corporateNumber, event.descriptor);
        this.showModal();
        setTimeout(() => this.datFileEditor.openFile(event.recordId));
    }
 
    public showModal():void 
    {
        this.isModalShown = true;
    }

    public hideModal():void 
    {
        this.autoShownModal.hide();
    }

    public onHidden():void 
    {
        this.isModalShown = false;
    }    
}

export interface ReportDefinition
{
    id: string;
    type: string;
    title: string;
}

export class DataQualityAspectReportDefinition implements ReportDefinition
{
    id: string;
    type: string;
    title: string;
    rootDrInstanceId: string;
    groupByTargetDrInstanceId: string;
    groupByTargetPropertyId: string;

    constructor(id: string, title: string)
    {
        this.id = id;
        this.title = title;
        this.type = 'ASPECT';
    }
}

class RecordDetails
{
    recordId: string;
    corporateNumber: string;
    descriptor: string;

    constructor(recordId: string, corporateNumber: string, descriptor: string)
    {
        this.recordId = recordId;
        this.corporateNumber = corporateNumber;
        this.descriptor = descriptor;
    }
}