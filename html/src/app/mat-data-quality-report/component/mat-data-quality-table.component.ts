import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DataTableFilter } from '../../data-table/model/data-table.model';
import { DataTableComponent } from '../../data-table/component/data-table.component';
import { DataFilter } from '../../core/model/data-filter.model';

@Component
({
    selector: 'mat-dq-data-table',
    templateUrl: 'mat-data-quality-table.component.html',
    styleUrls: ['mat-data-quality-table.component.css']
})
export class DataQualityTableComponent  
{   
    data: Object[];
    dataCount: number;
    loading: boolean;
    searchTerms: string;
    searchPattern: string;
    filterConceptId: string;

    @Input() searchString: string;

    @ViewChild('dqTable')
    dataTable: DataTableComponent;

    constructor()
    {
        console.log("DQ table constructor executing...");
        this.data = new Array();
        this.dataCount = 0;
        this.loading = false;
    }

    reloadData()
    {
        console.log("Reloading table data 1...");
    }

    setTableFilter(tableFilter: DataTableFilter) 
    {
        console.log("Reloading table data 2...");
        let dataFilter: DataFilter;

        dataFilter = this.createDataFilter(tableFilter);
        // Retrieve data.
    }

    @Output() recordSelected: EventEmitter<RecordSelectedEvent> = new EventEmitter<RecordSelectedEvent>();
    rowClick(rowEvent: any) 
    {
        console.log('Clicked: ' + rowEvent.row.item.name);
        this.recordSelected.emit(new RecordSelectedEvent(rowEvent.row.item.$RECORD_ID, rowEvent.row.item.$CORPORATE_NUMBER, rowEvent.row.item.$DESCRIPTOR));
    }

    rowDoubleClick(rowEvent: any) 
    {
        alert('Double clicked: ' + rowEvent.row.item.name);
    }

    rowTooltip(item: any) 
    { 
        return item.jobTitle; 
    }

    public setFilterConceptId(filterConceptId: string)
    {
        this.filterConceptId = filterConceptId;
    }

    private createDataFilter(tableFilter: DataTableFilter): DataFilter
    {
        let filter: DataFilter;

        // Create the new filter.
        filter = new DataFilter(null, "@E_MMM_CATALOGUE_SEARCH_CORPORATE");
        filter.pageOffset = tableFilter.pageOffset;
        filter.pageSize = tableFilter.pageSize;
        filter.fields = ["$RECORD_ID", "$CORPORATE_NUMBER", "$LONG_DESCRIPTION", "$DESCRIPTOR", "$CHRCTRSTC_COMPLETENESS", "$CHRCTRSTC_VALIDITY"];

        // Add the default filter criteria.
        filter.addCriterion("AND", "$ORG_ID", "EQUAL", "56D59F9D41E94DEE981BA26869B0EF41");
        filter.addCriterion("AND", "$LANGUAGE_ID", "EQUAL", "CC0092ADF415443DB36744B5D9EF96E9");

        // Add the filter concept id if specified.
        if (this.filterConceptId)
        {
            filter.addCriterion("AND", "$MATERIAL_TYPE_CONCEPT_ID", "EQUAL", this.filterConceptId);
        }

        // Add the search terms if specified.
        if (this.searchTerms)
        {
            let pattern: string;

            // Split the search terms on whitespace and add each term to the filter.
            for (let searchTerm of this.searchTerms.split(" "))
            {
                pattern += "|";
                pattern += searchTerm;
                filter.addCriterion("AND", "$SEARCH_TERMS", "LIKE", searchTerm);
            }

            // Set the search pattern used for text highlighting.
            this.searchPattern = pattern;
        }
        else
        {
            // Reset the search pattern.
            this.searchPattern = "";
        }

        // Add the ordering if specified.
        if (tableFilter.orderByFieldId)
        {
            filter.addFieldOrdering(tableFilter.orderByFieldId, tableFilter.orderAscending ? "ASCENDING" : "DESCENDING");
        }

        return filter;
    }
}

export class RecordSelectedEvent
{
    recordId: string;
    corporateNumber: string;
    descriptor: string;

    constructor(recordId: string, corporateNumber: string, descriptor: string)
    {
        this.recordId = recordId;
        this.corporateNumber = corporateNumber;
        this.descriptor = descriptor;
    }
}
