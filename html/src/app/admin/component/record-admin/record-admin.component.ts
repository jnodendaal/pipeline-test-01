import { Component, ViewChild, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableFilter } from '../../../data-table/model/data-table.model';
import { DataTableComponent } from '../../../data-table/component/data-table.component';
import { FunctionalityHandle, FunctionalityGroupHandle, FunctionalityAccessHandle } from '../../../core/model/functionality.model';
import { DataFileEditorComponent } from '../../../data-file-editor/component/data-file-editor.component';
import { FunctionalityManagerService } from '../../../core/service/functionality-manager.service';
import { DataFilter } from '../../../core/model/data-filter.model';

@Component
({
    selector: 'admin-admin-editor',
    templateUrl: 'record-admin.component.html',
    styleUrls: ['record-admin.component.css']
})
export class RecordAdminComponent implements OnInit, OnDestroy
{   
    private activatedRoute: ActivatedRoute;
    private functionalityManager: FunctionalityManagerService;
    private accessHandle: FunctionalityAccessHandle;
    private parameters: any;
    private data: Object[];
    private dataCount: number;
    private loading: boolean;
    private searchTerms: string;
    private searchPattern: string;
    private selectedFileId: string;
    private recordEditorView: boolean;

    @Input() searchString: string;

    @ViewChild('dqTable')
    dataTable: DataTableComponent;

    @ViewChild('dataFileEditor')
    private dataFileEditor: DataFileEditorComponent;

    @ViewChild('autoShownModal') public autoShownModal:ModalDirective;
    public isModalShown:boolean = false;

    constructor(activatedRoute:ActivatedRoute, functionalityManager: FunctionalityManagerService)
    {
        console.log("RecordEditorComponent");
        this.activatedRoute = activatedRoute;
        this.functionalityManager = functionalityManager;
        this.data = new Array();
        this.dataCount = 0;
        this.loading = false;
        this.parameters = new Object(); // Initialize the object to prevent undefined references.
        this.recordEditorView = false;
    }

    ngOnInit() 
    {
        this.activatedRoute.queryParams
            .map(params => params['functionality_iid'])
            .subscribe((functionalityIid) => 
            {
                console.log("Functionality Iid: " + functionalityIid);
                this.accessHandle = this.functionalityManager.getFunctionalityAccessHandle(functionalityIid);
                this.parameters = this.accessHandle.componentParameters;
                console.log(this.accessHandle);
            })
    }

    ngOnDestroy()
    {
        this.functionalityManager.releaseFunctionality(this.accessHandle.iid);
    }
}