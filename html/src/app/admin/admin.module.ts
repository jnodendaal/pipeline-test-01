import { NgModule }                               from '@angular/core';
import { Routes, RouterModule }                   from '@angular/router';
import { CommonModule }                           from '@angular/common';
import { FormsModule }                            from '@angular/forms';
import { SharedModule }                           from '../core/shared.module';
import { DataFileEditorModule }                   from '../data-file-editor/data-file-editor.module';
import { RecordAdminComponent }                  from './component/record-admin/record-admin.component';

@NgModule
({
  imports:      
  [ 
    CommonModule, 
    FormsModule,
    SharedModule,
    DataFileEditorModule,
    RouterModule.forChild(
    [
        { path: '', component: RecordAdminComponent, pathMatch: 'full' }
    ])
  ],
  declarations: 
  [ 
    RecordAdminComponent
  ],
  exports:      [ RecordAdminComponent ],
  providers:    [ ]
})
export class AdminModule 
{
}